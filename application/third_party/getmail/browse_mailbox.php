<?php
/*
 * browse_mailbox.php
 *
 * @(#) $Header: /opt2/ena/metal/pop3/browse_mailbox.php,v 1.2 2014/01/27 10:53:45 mlemos Exp $
 *
 */


	require('mime_parser.php');
	require('rfc822_addresses.php');
	require("pop3.php");
	
	stream_wrapper_register('mlpop3', 'pop3_stream');  /* Register the pop3 stream handler class */


	
	$pop3=new pop3_class;
	$pop3->hostname=POP3_HOST;               /* POP 3 server host name   */
	$pop3->port=POP3_PORT;                   /* POP 3 server host port,
	                                            usually 110 but some servers use other ports
	                                            Gmail uses 995                              */
	$pop3->tls=1;                            /* Establish secure connections using TLS      */
	$user=POP3_EMAIL;                        /* Authentication user name  */
	$password=POP3_PWD;                      /* Authentication password                 */
	$pop3->realm="";                         /* Authentication realm or domain              */
	$pop3->workstation="";                   /* Workstation for NTLM authentication         */
	$apop=0;                                 /* Use APOP authentication                     */
	$pop3->authentication_mechanism="USER";  /* SASL authentication mechanism               */
	$pop3->debug=0;                          /* Output debug information                    */
	$pop3->html_debug=0;                     /* Debug information is in HTML                */
	$pop3->join_continuation_header_lines=1; /* Concatenate headers split in multiple lines */
	$data = array();
	if(($error=$pop3->Open())=="")
	{
		if(($error=$pop3->Login($user,$password,$apop))=="")
		{
					
			if(($error=$pop3->Statistics($messages,$size))=="")
			{
				echo $messages;
				if($messages>0)
				{
					$pop3->GetConnectionName($connection_name);
					
					$num = $messages > 10 ? 15 : $messages; /*No of mails increase here*/
					
					for($i=0; $i<= $num; $i++){ 					
						$message= $messages - $i ;
						
						$message_file='mlpop3://'.$connection_name.'/'.$message;
						$mime=new mime_parser_class;
					   
						/*
						* Set to 0 for not decoding the message bodies
						*/
						echo "<hr>";
						$mime->decode_bodies =1;
	
						$parameters =  parameters($message_file); 
						$success = $mime->Decode($parameters, $decoded);
						if ($success)
						{
						if ($mime->Analyze($decoded[0], $results))
						 {
							 
						   if (($results['From'][0]['name']) == 'IndiaMART Enquiry')
						    {	 
							 $exp = break_fields(clean($results['Alternative'][0]['Data']));
							 if($exp){
							   $descrip = clean($exp[0]);	 
							   $address= clean($exp[1]);
							   $mobile = clean($exp[2]);
							   $email = clean($exp[3]);
							   $email = clean( str_replace('Buyers Requirement Details','',$email ));
							 
							   if(preg_match('/@.+\./', $email))
							      $email = $email;
								else 
								  $email = '';
							 
							 
						      $data[] =array(
							            
										 'mobile'=>"'".str_replace('Email','',$mobile)."'",
										 'address'=>"'".str_replace('Mobile','',$address)."'",
										 'header'=>"'".$results['From'][0]['name']."'",
										 'lead_title' =>"'".clean($results['Subject'])."'",
										 'lead_email'=>"'".$email."'", 
										 'lead_description'=>"'".$descrip."'",
										 'lead_modidate'=>"'".date("Y-m-d h:m:s", strtotime($results['Date']))."'",
										 'lead_regdate' => "'".date('Y-m-d h:i:s')."'"
				                       );
									  // print_r($data);
							 }
							}
						 }
						}
				    }
				  	print_r($data);
				}
				
				return $data;
			}
		}
	}
	
function parameters($message_file)
{

  $param = array('File'=>$message_file,
			/* Read a message from a string instead of a file */
			/* 'Data'=>'My message data string',              */

			/* Save the message body parts to a directory     */
			/* 'SaveBody'=>'/tmp',                            */

			/* Do not retrieve or save message body parts     */
				'SkipBody'=>'',
   );
   return $param;
   

}


 
function clean($string) 
{  
  $string = str_replace("'", '', $string);
   $string = str_replace('"', '', $string); // Replaces all spaces with hyphens.
  return preg_replace('/[^A-Za-z0-9\-@_:,][ ]/', '', $string);// Removes special chars.
}



function break_fields($fields)
{
 $d = explode(':',$fields);
return $d;

}

?>
