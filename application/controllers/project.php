<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Project extends CI_Controller 
	{
		
		/*-----------------------------Project Constructor-----------------------------*/
		
		public function __construct() 
		{
			parent::__construct();
            $timezone = new DateTimeZone("Asia/Kolkata" );
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
            $this->hour=$date->format('H');
            $this->_is_logged_in();
			$this->load->model('project_model');
			$this->data1=$this->crm_model->assign_menu();
		}
		
		/*Session Check Function */ 
		public function _is_logged_in() 
		{
		    $is_logged_in = $this->session->userdata('profile_id');
			if ($is_logged_in=="") 
			{
				redirect('auth/index/1');
				die();
			}
		}
		
		public function index()
		{
			$this->load->view('header.php');
			$this->load->view('menu.php');
			$this->load->view('footer.php');
		}
		
		/*-----------------------------Start View Project Here-----------------------------*/
		
		public function view_project()
		{
			$this->load->helper('form');
			$data['rec']=$this->db->get('m42_project');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Project/view_project',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		/*-----------------------------Start View Project After Load Js Here-----------------------------*/
		
		public function after_view_project()
		{
			$data['rec']=$this->db->get('m42_project');
			$this->load->view('project/view_project',$data);
			$this->load->view('footer_init');
		}
		
		/*-----------------------------Start Insert Project Here-----------------------------*/
		
		public function insert_project()
		{
			$this->project_model->insert_project();
			
			$p_name=$this->input->post('txtpname');
			$upload_config = array
			(
			'upload_path' => './project/',
			);
			
			$this->load->library('upload', $upload_config);
			if (!is_dir('project/'.$p_name))
			{
				mkdir('./project/'.$p_name, 0777, true);
			}
		}
		
		// -----------------------------Upload File in Project-------------------------------------
		
		public function uploadfile()
		{
			$config['upload_path']   =   "application/projectdoc/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png|pdf|doc|docx|xlsx|xml|zip|txt"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "1960";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$fileupload=($finfo['raw_name'].$finfo['file_ext']);
			echo $fileupload;
		}
		
		// -----------------------------Edit Project View here-------------------------------------
		
		public function edit_project()
		{
			$id=$this->uri->segment(3);
			$this->db->where('m_project_id',$this->uri->segment(3));
			$data['rec']=$this->db->get('m42_project');
			$this->load->view('Project/view_edit_project',$data);
			$this->load->view('footer_init');
		}
		
		// -----------------------------Update Project Here-------------------------------------
		
		public function update_project()
		{
			$this->project_model->update_project();
			echo "true";
		}
		
		// -----------------------------Status Change Of Project Here------------------------------------
		
		public function project_status()
		{
			$this->project_model->project_status();
			$this->db->free_db_resource();
			$data['rec']=$this->db->get('m42_project');
			$this->load->view('Project/view_project',$data);
			$this->load->view('footer_init');
		}
		
		/*---------------------------End Current Follow Up Report Here-----------------------------*/
		
		public function view_module()
		{
			$data['projid']=$this->uri->segment(3);
			$this->db->where("m_module_project_id",$this->uri->segment(3));
			$data['rec']=$this->db->get("m59_project_module");
            $this->load->view('Project/view_add_modules',$data);
			$this->load->view('footer_init');
		}
		
		public function upload_submodule()
		{
			$data['projectid']=$this->uri->segment(3);
			$this->load->view('Project/view_add_submodules',$data);
		}
		
		public function insert_module()
		{
			$this->project_model->insert_module();
		}
		public function edit_module()
		{
			$data['projid']=$this->uri->segment(3);
			$this->db->where('m_module_project_id',$this->uri->segment(3));
			$data['rec']=$this->db->get('m59_project_module');
			$this->load->view('Project/view_edit_module',$data);
			$this->load->view('footer_init');
		}
		
		public function module_edit_details()
		{
			$data['mod_id']=$this->uri->segment(3);
			$data['proj_id']=$this->uri->segment(4);
			$this->db->where("m_module_id",$this->uri->segment(3));
			$data['rec']=$this->db->get("m59_project_module");
			$this->load->view('Project/view_edit_module_form',$data);
		}
		
		public function update_module()
		{
			$this->project_model->update_module();
		}
		/*--------------------Upload Project Module Doc----------------------*/
		
		public function moduledoc()
		{
			$config['upload_path']   =   "application/moduledoc/";
			$config['allowed_types'] =   "jpg|jpeg|png|pdf|doc|xlsx|txt";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$fileupload=($finfo['raw_name'].$finfo['file_ext']);
			echo $fileupload;
		}
		
		/*-----------------All participants View here-------------------*/
		
		public function view_paricipants()
		{
			$data['rec']=$this->db->query("SELECT * FROM view_participant_on_project WHERE `view_participant_on_project`.`account_type`=11 AND  (CASE WHEN(".$this->session->userdata('user_type')."=2) THEN (`view_participant_on_project`.`account_owner` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`= ".$this->session->userdata('affid')." ) AND `view_participant_on_project`.`account_owned_by`= 1) OR `view_participant_on_project`.`account_owner`= ".$this->session->userdata('profile_id')." AND `view_participant_on_project`.`account_owned_by`= 2 WHEN(".$this->session->userdata('user_type')."=1) THEN (`view_participant_on_project`.`account_owner`= ".$this->session->userdata('profile_id')." AND `view_participant_on_project`.`account_owned_by`= 1) ELSE 1 END)  ");
         	$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Project/view_all_participants',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		// -----------------------------Adding Participants after load js View Here-------------------------------------
		
		public function after_view_project_participants()
		{
			$data['rec']=$this->db->get("view_participant_on_project");
			$this->load->view('Project/view_all_participants',$data);
			$this->load->view('footer_init');
		}
		
		/*-----------------Adding participants View here-------------------*/
		
		public function view_project_participants()
		{
			$data['project']=$this->db->query("SELECT * FROM view_client_project_detail WHERE `view_client_project_detail`.`account_type`=11 AND  (CASE WHEN(".$this->session->userdata('user_type')."=2) THEN (`view_client_project_detail`.`account_owner` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`= ".$this->session->userdata('affid')." ) AND `view_client_project_detail`.`account_owned_by`= 1) OR `view_client_project_detail`.`account_owner`= ".$this->session->userdata('profile_id')." AND `view_client_project_detail`.`account_owned_by`= 2 WHEN(".$this->session->userdata('user_type')."=1) THEN (`view_client_project_detail`.`account_owner`= ".$this->session->userdata('profile_id')." AND `view_client_project_detail`.`account_owned_by`= 1) ELSE 1 END) AND ACCP_STATUS=1 GROUP BY `view_client_project_detail`.`m_acc_project` ");		
$desig=array('2','3');
			$this->db->where_in('or_m_designation',$desig);
            $this->db->where('or_m_aff_id',$this->session->userdata('affid'));
			$this->db->where('or_m_status',1);
			$data['emp']=$this->db->get('m06_user_detail');
			$this->load->view('Project/view_add_participants',$data);
			$this->load->view('footer_init');
		}
		
		/*-----------------Adding participants  here-------------------*/
		
		public function add_participants()
		{
			$this->project_model->participants_add();
		}
		
		/*-----------------Edit participants View here-------------------*/
		
		public function edit_add_participants()
		{
			$this->db->where('ACCP_STATUS',1);
			$data['project']=$this->db->get('view_client_project_detail');
			
			$desig=array('2','3');
			$this->db->where_in('or_m_designation',$desig);
			$this->db->where('or_m_status',1);
			$data['emp']=$this->db->get('m06_user_detail');
			
			$this->db->where('parti_id',$this->uri->segment(3));
			$data['rec']=$this->db->get('tr22_project_participants');
			
			$this->load->view('Project/view_edit_participants',$data);
			$this->load->view('footer_init');
		}
		
		/*-----------------Updating participants here-------------------*/
		
		
		public function update_add_participants()
		{
			$this->project_model->participants_update();
			echo 'true';
		}
		
		
		/*-----------------Updating participants Status here-------------------*/
		
		
		public function participants_status()
		{
			$this->project_model->participants_status();
		    $data['rec']=$this->db->get("view_participant_on_project");
			$this->load->view('Project/view_all_participants',$data);
			$this->load->view('footer_init');
		}
		
		
		/*---------------------------View All Task For Projects-----------------------------*/
		
		
		public function view_all_task()
		{
			$todate=0;
			$fromdate=0;
			$condition='';
			
			if($this->input->post('txtfrom')!="")
			{
				$fromdate=$this->input->post('txtfrom');
			}
			
			if($this->input->post('txtto')!="")
			{
				$todate=$this->input->post('txtto');
			}
			
			if($todate!='0' && $fromdate!='0')
			{
				$condition=$condition."`m48_task_on_project`.`m_ptask_date` BETWEEN DATE_FORMAT('$fromdate','%Y-%m-%d') and DATE_FORMAT('$todate','%Y-%m-%d') and ";
			}
			
			if($this->input->post('txtassign_to')!="" && $this->input->post('txtassign_to')!="0")
			{
				$condition=$condition." `m06_user_detail`.`or_m_name`='".$this->input->post('txtassign_to')."'  and";
			} 
			
			if($this->input->post('txtproject')!="" && $this->input->post('txtproject')!="0")
			{
				$condition=$condition." `m42_project`.`m_project_name`='".$this->input->post('txtproject')."'  and";
			} 
              
            if($this->input->post('ddtasktype')==1)
			{
            $condition=$condition."`m48_task_on_project`.`m_ptask_assign_by`=".$this->session->userdata('profile_id')." AND `m48_task_on_project`.`m_task_usertype`=".$this->session->userdata('user_type')." AND ";
            }

            if($this->input->post('ddtasktype')==2)
			{
            $condition=$condition."`m48_task_on_project`.`m_ptask_assign_to`=".$this->session->userdata('profile_id')." AND ";
            }

            if($this->input->post('ddtasktype')==3)
			{
            $condition=$condition."`m48_task_on_project`.`m_ptask_follower`=".$this->session->userdata('profile_id')." AND ";
            }

			$condition=$condition." `m33_account`.`account_type`=11 AND  (CASE WHEN(".$this->session->userdata('user_type')."=2) THEN (`m33_account`.`account_owner` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`= ".$this->session->userdata('affid')." ) AND `m33_account`.`account_owned_by`= 1) OR `m33_account`.`account_owner`= ".$this->session->userdata('profile_id')." AND `m33_account`.`account_owned_by`= 2 WHEN(".$this->session->userdata('user_type')."=1) THEN (`m48_task_on_project`.`m_ptask_assign_by`= ".$this->session->userdata('profile_id')." OR `m48_task_on_project`.`m_ptask_follower`= ".$this->session->userdata('profile_id')." OR `m48_task_on_project`.`m_ptask_assign_to`=".$this->session->userdata('profile_id').") ELSE 1 END) AND `m48_task_on_project`.`m_ptask_id` !=0";
			$condition=$condition." GROUP BY `m48_task_on_project`.`m_ptask_id` ORDER BY `m48_task_on_project`.`m_ptask_id` DESC  ";
			
			
			$record=array(
			'query'=>$condition
			);
			$query = " CALL sp_view_project_task(?" . str_repeat(",?", count($record)-1) . ") ";
			$data['rec']=$this->db->query($query, $record);
		
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Project/view_all_task',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		/*---------------------------View All Task After load Js-----------------------------*/
		
		public function after_view_all_task()
		{
			$query = " CALL sp_view_project_task(1) ";
			$data['rec']=$this->db->query($query);
			$this->load->view('Project/view_all_task',$data);
			$this->load->view('footer_init');
		}
		
		/*---------------------------End OF View All Projects-----------------------------*/
		
		/*---------------------------Create Task For View Projects-----------------------------*/
		
		public function view_create_task()
		{
			$desig=array('2','3');
			$this->db->where_in('or_m_designation',$desig);
			$this->db->where('or_m_status',1);
			$data['follower'] = $this->db->get('m06_user_detail');
			
			
			$data['proj'] = $this->db->query("SELECT * FROM view_project_details WHERE ACCP_STATUS=1 AND `view_project_details`.`account_type`=11 AND  (CASE WHEN(".$this->session->userdata('user_type')."=2) THEN (`view_project_details`.`account_owner` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`= ".$this->session->userdata('affid')." ) AND `view_project_details`.`account_owned_by`= 1) OR `view_project_details`.`account_owner`= ".$this->session->userdata('profile_id')." AND `view_project_details`.`account_owned_by`= 2 WHEN(".$this->session->userdata('user_type')."=1) THEN (`view_project_details`.`PARICIPANT_USERID`= ".$this->session->userdata('profile_id')." ) ELSE 1 END)  GROUP BY `view_project_details`.`ACCP_ID`");
		
			$this->load->view('Project/view_create_task',$data);
			$this->load->view('footer_init');
		}
		
		public function get_task_assign()
		{
			$query=$this->db->query("SELECT
			`tr22_project_participants`.`parti_participants_id`	AS 	`USER_ID`,
			`m06_user_detail`.`or_m_name`						AS 	`USER_NAME`
			FROM
			`admin_appworks`.`tr22_project_participants`
			LEFT JOIN `admin_appworks`.`m06_user_detail` 
			ON (`tr22_project_participants`.`parti_participants_id` = `m06_user_detail`.`or_m_reg_id`) 
			WHERE `tr22_project_participants`.`parti_project_id`=".$this->input->post('txtprojectid')." AND (CASE WHEN (".$this->session->userdata('user_type')."=1) THEN `tr22_project_participants`.`parti_designation`>=(SELECT `tr22_project_participants`.`parti_designation` FROM `tr22_project_participants` WHERE `tr22_project_participants`.`parti_participants_id`=".$this->session->userdata('profile_id')." AND `tr22_project_participants`.`parti_project_id`=".$this->input->post('txtprojectid')." AND `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid').") ELSE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid')." END )");
			$json=json_encode($query->result());
			echo $json;
		}
		
		public function get_task_follower()
		{
			$query=$this->db->query("SELECT
			`tr22_project_participants`.`parti_participants_id`	AS 	`USER_ID`,
			`m06_user_detail`.`or_m_name`						AS 	`USER_NAME`
			FROM
			`admin_appworks`.`tr22_project_participants`
			LEFT JOIN `admin_appworks`.`m06_user_detail` 
			ON (`tr22_project_participants`.`parti_participants_id` = `m06_user_detail`.`or_m_reg_id`) 
			WHERE `tr22_project_participants`.`parti_project_id`='".$this->input->post('txtprojectid')."' AND `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid')." AND `tr22_project_participants`.`parti_designation`<=(SELECT `parti_designation` FROM `tr22_project_participants` WHERE `tr22_project_participants`.`parti_participants_id`=".$this->input->post('ddassignto')." AND `tr22_project_participants`.`parti_project_id`=".$this->input->post('txtprojectid').")");
			$json=json_encode($query->result());
			echo $json;
		}
		/*---------------------------Insert Task For Projects here -----------------------------*/
		
		public function insert_project_task()
		{
			$this->project_model->insert_project_task();
		}
		
		/*---------------------------Upload Task file-----------------------------*/
		
		public function uploadfile1()
		{
			$config['upload_path']   =   "application/libraries/taskdoc/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "1960";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$fileupload=($finfo['raw_name'].$finfo['file_ext']);
            
			echo $fileupload;
		}
		
		/*---------------------------End of Create Task For Projects-----------------------------*/
		
		/*---------------------------Compete, Enable And Disable Task For Projects-----------------------------*/
		
		public function view_task_complete()
		{
			$this->load->model('project_model');
			$this->project_model->view_task_complete();
			$query = " CALL sp_view_project_task(1) ";
			$data['rec']=$this->db->query($query);
			$this->load->view('Project/view_all_task',$data);
			$this->load->view('footer_init');
		}
		
		/*---------------------------Edit Task For Projects View Here-----------------------------*/
		
		public function view_task_edit()
		{
            $data['proj'] = $this->db->query("SELECT * FROM view_project_details WHERE ACCP_STATUS=1 AND `view_project_details`.`account_type`=11 AND  (CASE WHEN(".$this->session->userdata('user_type')."=2) THEN (`view_project_details`.`account_owner` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`= ".$this->session->userdata('affid')." ) AND `view_project_details`.`account_owned_by`= 1) OR `view_project_details`.`account_owner`= ".$this->session->userdata('profile_id')." AND `view_project_details`.`account_owned_by`= 2 WHEN(".$this->session->userdata('user_type')."=1) THEN (`view_project_details`.`PARICIPANT_USERID`= ".$this->session->userdata('profile_id')." ) ELSE 1 END)  GROUP BY `view_project_details`.`ACCP_ID`");
            $this->db->free_db_resource();
			$condition="";
			$condition="`m48_task_on_project`.`m_ptask_id`=".$this->uri->segment(3);
			$record=array(
			'query'=>$condition
			);
			$query = " CALL sp_view_project_task(?" . str_repeat(",?", count($record)-1) . ") ";
			$data['rec']=$this->db->query($query, $record);
			$this->load->view('Project/view_edit_task',$data);
			$this->load->view('footer_init');
		}
		
		/*---------------------------Updating Task For Projects Here-----------------------------*/
		
		public function update_project_task()
		{
			$this->project_model->update_project_task();
		}
		
		/*---------------------------End View All Task For Projects-----------------------------*/
		
		/*---------------------------Projects Report Here-----------------------------*/
		
		public function view_project_report()
		{
			$todate=0;
			$fromdate=0;
			$condition="";	
			if($this->input->post('txttodate')!="")
			{
				$todate=$this->input->post('txttodate');
			}
			if($this->input->post('txtfromdate')!="")
			{
				$fromdate=$this->input->post('txtfromdate');
			}
			if($fromdate!='0')
			{
				$condition=$condition." DATE_FORMAT(`m42_project`.`m_project_startdate`,'%Y-%m-%d' )>=DATE_FORMAT('$fromdate','%Y-%m-%d') AND ";
			}
			if($todate!='0')
			{
				$condition=$condition." DATE_FORMAT(`m42_project`.`m_project_startdate`,'%Y-%m-%d' )<=DATE_FORMAT('$todate','%Y-%m-%d') and ";
			}
			
			if($this->input->post('ddproject')!="" && $this->input->post('ddproject')!="-1")
			{
				$condition=$condition." `m42_project`.`m_project_id`= ".$this->input->post('ddproject')."  and";
			}
			
			if($this->input->post('ddleader')!="" && $this->input->post('ddleader')!="-1")
			{
				$condition=$condition." (SELECT `GetProjectLeader`(`m42_project`.`m_project_id`))= ".$this->input->post('ddleader')."  and";
			}
			
			if($this->input->post('ddparticipants')!="-1" && $this->input->post('ddparticipants')!="")
			{
				$condition=$condition." `tr22_project_participants`.`parti_participants_id`= ".$this->input->post('ddparticipants')."  and";
			}
			
			$condition=$condition." `m33_account`.`account_type`=11 AND  (CASE WHEN(".$this->session->userdata('user_type')."=2) THEN (`m33_account`.`account_owner` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`= ".$this->session->userdata('affid')." ) AND `m33_account`.`account_owned_by`= 1) OR `m33_account`.`account_owner`= ".$this->session->userdata('profile_id')." AND `m33_account`.`account_owned_by`= 2 WHEN(".$this->session->userdata('user_type')."=1) THEN (`tr22_project_participants`.`parti_participants_id`= ".$this->session->userdata('profile_id')." ) ELSE 1 END)  GROUP BY `tr22_project_participants`.`parti_project_id` ORDER BY `m43_acc_project`.`m_acc_project` DESC ";
			
			$record=array(
			'query'=>$condition
			);
			
			$query = " CALL sp_view_all_project(?) ";
			$data['rec']=$this->db->query($query, $record);
			
			$this->db->free_db_resource();
			
			$this->db->where('or_m_status',1);
			$this->db->where('or_m_designation',2);
			$data['user']=$this->db->get('m06_user_detail');
			
			$this->db->free_db_resource();
			
			$this->db->where('m_project_stauts',1);
			$data['proj']=$this->db->get('m42_project');
			
			$this->db->free_db_resource();
			
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Project/view_project_report',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		/*---------------------------End Projects Report Here-----------------------------*/
		
		/*---------------------------Project Task Report Here-----------------------------*/
		
		public function view_task_report()
		{
			$todate=0;
			$fromdate=0;
			$condition="";
			
			if($this->input->post('txttodate')!="")
			{
				$todate=$this->input->post('txttodate');
			}
			
			if($this->input->post('txtfromdate')!="")
			{
				$fromdate=$this->input->post('txtfromdate');
			}
			
			if($fromdate!='0')
			{
				$condition=$condition." DATE_FORMAT(`m48_task_on_project`.`m_ptask_start_date`,'%Y-%m-%d' )>=DATE_FORMAT('$fromdate','%Y-%m-%d') AND ";
			}
			
			if($todate!='0')
			{
				$condition=$condition." DATE_FORMAT(`m48_task_on_project`.`m_ptask_end_date`,'%Y-%m-%d' )<=DATE_FORMAT('$todate','%Y-%m-%d') and ";
			}
			
			if($this->input->post('ddproject')!="" && $this->input->post('ddproject')!="-1")
			{
				$condition=$condition." `m42_project`.`m_project_id`= ".$this->input->post('ddproject')."  and";
			}
			
			if($this->input->post('ddassignby')!="" && $this->input->post('ddassignby')!="-1")
			{
				$condition=$condition." `m48_task_on_project`.`m_ptask_assign_by`= ".$this->input->post('ddassignby')."  and";
			}
			
			if($this->input->post('ddassignto')!="-1" && $this->input->post('ddassignto')!="")
			{
				$condition=$condition." `m48_task_on_project`.`m_ptask_assign_to`= ".$this->input->post('ddassignto')."  and";
			}
			
			if($this->input->post('ddstatus')!="-1" && $this->input->post('ddstatus')!="")
			{
				$condition=$condition." `m48_task_on_project`.`m_ptask_status`= ".$this->input->post('ddstatus')."  and";
			}
			
			if($this->input->post('ddpriority')!="-1" && $this->input->post('ddpriority')!="")
			{
				$condition=$condition." `m48_task_on_project`.`m_ptask_priority`= ".$this->input->post('ddpriority')."  and";
			}
			
			$condition=$condition." `m33_account`.`account_type`=11 AND  (CASE WHEN(".$this->session->userdata('user_type')."=2) THEN (`m33_account`.`account_owner` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`= ".$this->session->userdata('affid')." ) AND `m33_account`.`account_owned_by`= 1) OR `m33_account`.`account_owner`= ".$this->session->userdata('profile_id')." AND `m33_account`.`account_owned_by`= 2 WHEN(".$this->session->userdata('user_type')."=1) THEN (`m48_task_on_project`.`m_ptask_assign_by`= ".$this->session->userdata('profile_id')." AND `m48_task_on_project`.`m_task_usertype`=".$this->session->userdata('user_type').") ELSE 1 END) ";
			
			$record=array(
			'query'=>$condition
			);
			
			$query = " CALL sp_view_project_task(?) ";
			$data['rec']=$this->db->query($query, $record);
			
			$this->db->free_db_resource();
			
			
			$this->db->where('or_m_status',1);
			$this->db->where('or_m_designation',2);
			$data['user']=$this->db->get('m06_user_detail');
			
			$this->db->free_db_resource();
			
			$this->db->where('m_project_stauts',1);
			$data['proj']=$this->db->get('m42_project');
			
			
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Project/view_task_report',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		/*---------------------------End Projects Task Report Here-----------------------------*/
		
		/*---------------------------Add Task Followup in Projects Here-----------------------------*/
		
		public function view_task_follow_up()
		{
			$data=array(
			'queery'=>"`tr26_project_task_transaction`.`task_id`=".$this->uri->segment(3)
			);
			$query = "CALL sp_project_task_follow_up(?) ";
			$data['rec']=$this->db->query($query,$data);
			
			$this->db->free_db_resource();
			
			$query=$this->db->query("SELECT GetStatusOfTask(".$this->uri->segment(3).") AS taskstatus");
			$row=$query->row();
			$data['taskstatus']=$row->taskstatus;
			
			$this->db->free_db_resource();
			$data['id']=$this->uri->segment(3);
			
			$this->load->view('project/view_task_followup',$data);
			$this->load->view('footer_init');
		}
		
		/*---------------------------End Add Task Followup in Projects Here-----------------------------*/
		
		/*---------------------------Insert Task Followup in Projects Here-----------------------------*/
		
		public function insert_follow_up()
		{
			$this->project_model->insert_follow_up();
		}
		
		/*---------------------------End Insert Task Followup in Projects Here-----------------------------*/
		
		/*---------------------------Current Follow Up Report Here-----------------------------*/
		
		public function current_followup_report()
		{
			$condition="`tr26_project_task_transaction`.`task_trans_trid`!='' AND `m33_account`.`account_type`=11 AND  (CASE WHEN(".$this->session->userdata('user_type')."=2) THEN (`m33_account`.`account_owner` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`= ".$this->session->userdata('affid')." ) AND `m33_account`.`account_owned_by`= 1) OR `m33_account`.`account_owner`= ".$this->session->userdata('profile_id')." AND `m33_account`.`account_owned_by`= 2 WHEN(".$this->session->userdata('user_type')."=1) THEN (`m48_task_on_project`.`m_ptask_assign_by`= ".$this->session->userdata('profile_id')." AND `m48_task_on_project`.`m_task_usertype`=".$this->session->userdata('user_type')." OR `m48_task_on_project`.`m_ptask_follower`= ".$this->session->userdata('profile_id').") ELSE 1 END) ORDER BY `tr26_project_task_transaction`.`task_response_date` desc limit 100";
			$query = 'CALL sp_current_followup("'.$condition.'")';
			$data['rec']=$this->db->query($query);
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Project/view_current_followup_report',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		/*---------------------------End Current Follow Up Report Here-----------------------------*/
		
		/*---------------------------Current Follow Up Report Here-----------------------------*/
		
		public function view_project_flow_report()
		{
			/*$clientid=$this->uri->segment(3);
			$proid=$this->uri->segment(4);
            $pro_manager_id=$this->uri->segment(5);
            $account_id=$this->uri->segment(4);
			$query="CALL sp_view_project_flow('1','".$clientid."','".$proid."','".$pro_manager_id."')";
			$data['rec']=$this->db->query($query);
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Project/view_project_flow_report',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');*/

            $clientid=$this->uri->segment(3);
			$proid=$this->uri->segment(4);
			$pro_manager_id=$this->uri->segment(5);
            $accprj_id=$this->uri->segment(4);
			$query="CALL sp_view_project_flow(1, $clientid, $proid, $pro_manager_id,$accprj_id)";
			$data['rec']=$this->db->query($query);
			$this->db->free_db_resource();
			$query1="CALL sp_view_all_project('`m43_acc_project`.`m_acc_id`=$clientid AND `m43_acc_project`.`m_acc_project`=$proid')";
			$data['users']=$this->db->query($query1);
			$this->db->free_db_resource();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Project/view_project_flow_report',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');

		}
		
		public function update_project_flow()
		{
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			$data=array(
			'tr_flow_completed_by'=>$this->session->userdata('profile_id'),
			'tr_flow_description'=>$this->input->post('txtdescription'),
			'tr_flow_status'=>1,
			'tr_flow_completed_date'=>$date->format('Y-m-d H:i:s')
			);
			$this->db->where('tr_flow_id',$this->input->post('process_id'));
			$this->db->update('tr29_project_flow_report',$data);
			header("Location:".base_url()."project/view_project_flow_report/".$this->input->post('txt3')."/".$this->input->post('txt4')."/".$this->input->post('txt5'));
		}
		/*---------------------------End Current Follow Up Report Here-----------------------------*/
		
		
		/*-------------------Account panel---------------------*/
		public function view_all_project()
		{			
			
			$query = "CALL sp_view_all_project('1')";			
			$data['rec']=$this->db->query($query);
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Project/view_all_project',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}

        
        public function update_project_assign()
		{
			$data=array(
             'tr_flow_assign_id'=>$this->input->post('ddreassign'),
             'tr_next_assigndate'=>date('Y-m-d')
              );
			$this->db->where('tr_flow_id',$this->input->post('process_id1'));
			$this->db->update('tr29_project_flow_report',$data);
			header("location:".base_url()."project/view_project_flow_report/".$this->input->post('txtacc')."/".$this->input->post('txtpro')."/".$this->input->post('txtmanage'));
			
		}

        
        /*----------------------- Start Project FollowUp ----------------------*/
        public function view_followup()
        {
           $p_id=$this->uri->segment(3);
		   $data['project_id']=$this->uri->segment(3);
		   $datas=array(
			'proc'=>2,'project_id'=>0,'tr_proj_description'=>'','tr_responce_by'=>$this->session->userdata('profile_id'),'tr_responce_type'=>'','tr_responce_date'=>'','tr_user_type'=>$this->session->userdata('user_type'),'tr_userfile'=>'','querey'=>"tr_accproject_id='".$p_id."'"
			);
		   $query = "CALL sp_project_followup(?" . str_repeat(",?", count($datas)-1) .") ";
		   $data['rec']=$this->db->query($query,$datas);
          
		   $this->db->free_db_resource();
            
		   $this->load->view('header');
		   $this->load->view('menu1',$this->data1);
		   $this->load->view('Project/view_followup',$data);
		   $this->load->view('sidebar');
		   $this->load->view('footer');
        } 

        
        public function insert_project_followup()
        { 
            $task_no=$this->uri->segment(3);
			$task_reply=$this->input->post('task_description');
			$taskdata=array(
			'proc'=>1,
            'project_id'=>$task_no,
            'tr_proj_description'=>$task_reply,
            'tr_responce_by'=>$this->session->userdata('profile_id'),
            'tr_responce_type'=>1,
            'tr_responce_date'=>$this->curr,
            'tr_user_type'=>$this->session->userdata('user_type'),
            'tr_userfile'=>$this->input->post('userfile'),
            'querey'=>'1'
			);
			$query = "CALL sp_project_followup(?" . str_repeat(",?", count($taskdata)-1) .") ";
			$data['rec']=$this->db->query($query,$taskdata);
			$this->db->free_db_resource();
			echo 'true';
        }
       /*----------------------- End Project FollowUp ----------------------*/
	}
	
?>															