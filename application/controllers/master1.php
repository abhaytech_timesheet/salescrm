<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Master extends CI_Controller {
		
		public function index()
		{
			$this->load->helper('url');
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				header("Location:".base_url()."index.php/master/view_mainconfig");
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
	    public function logout()
	    {
			$this->load->helper('url');
			$this->load->library('session');
			$this->session->unset_userdata('profile_id');
			$this->session->unset_userdata('user_id');
			$this->session->unset_userdata('e_email');
			$this->session->unset_userdata('name');
			$this->session->unset_userdata('designation');
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
			$this->load->view('Auth/header');
			$this->load->view('Auth/login');
			$this->load->view('Auth/footer');
		}
		
		//Config
		public function view_mainconfig()
		{
			$this->load->helper('url');
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$data['form_name']="Here you can set Configuration";
				$this->load->model('mastermodel');
				$data['config']= $this->mastermodel->select_config();
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_mainconfig',$data);
				$this->load->view('sidebar');
				$this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		public function update_mainconfig()
		{
			$this->load->helper('url');
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$data['form_name']="Here you can set Configuration";
				$this->load->model('mastermodel');
				$this->mastermodel->update_config();
				header("Location:".base_url()."index.php/master/view_mainconfig");
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		//Manage City Add,Update,Status Change Function 
		public function view_city()
		{
			$this->load->helper('url');
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$data['form_name']="Manage City";
				$this->load->model('mastermodel');
				$data['city']=$this->mastermodel->select_state(1);
				$data['allcity']=$this->mastermodel->select_state(0);
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_city',$data);
				$this->load->view('sidebar');
				$this->load->view('footer');
			}
			else
			{ 
				header("Location:".base_url()."index.php/master/logout");
			}	
		}	
		public function select_city()
		{
			$this->load->helper('url');
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->load->model('mastermodel');
				$data['city']=$this->mastermodel->select_state($this->uri->segment(3));
				$this->load->view('Master/view_select_city',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		public function select_aff_city()
		{
			$this->load->helper('url');
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->load->model('mastermodel');
				$data['city']=$this->mastermodel->select_state($this->uri->segment(3));
				$data['city_id']=$this->uri->segment(4);
				$this->load->view('Master/view_select_branch_city',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		public function add_city()
		{
			$this->load->helper('url');
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->load->model('mastermodel');
				$this->mastermodel->add_city();
				header("Location:".base_url()."index.php/master/view_city");
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		
		public function update_status_city()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$id=$this->uri->segment(3);
				$this->db->where('m_loc_id',$id);
				$city=array(
				'm_status'=>$this->uri->segment(4)
				);
				$this->db->update('m05_location',$city);
				header("Location:".base_url()."index.php/master/view_city");
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		public function edit_city()
		{
			$this->load->helper('url');
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$data['form_name']="Manage City";
				$id=$this->uri->segment(3);
				$data['id']=$id;	
				$this->load->model('mastermodel');
				$data['edit_city']=$this->mastermodel->select_city($id);
				$data['city']=$this->mastermodel->select_state(1);
				$data['allcity']=$this->mastermodel->select_state(0);
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_edit_city',$data);
				$this->load->view('sidebar');
			    $this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		
		public function update_city()
		{
			$this->load->helper('url');
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$id=$this->uri->segment(3);
				$data['id']=$id;
				$this->load->model('mastermodel');
				$this->mastermodel->update_city($id);
				header("Location:".base_url()."index.php/master/edit_city/$id/");
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		//End City Master All Action related 
		
		//Manage Role /Designation
		public function view_designation()
		{
			$this->load->helper('url');
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$data['form_name']="Manage Designation/Rank";
				$this->load->model('mastermodel');
				$data['desig']=$this->mastermodel->select_designation();
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_designation',$data);
				$this->load->view('Master/footer',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}	
		
		public function add_designation()
		{
			$this->load->helper('url');
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->load->model('mastermodel');
				$this->mastermodel->add_designation();
				header("Location:".base_url()."index.php/master/view_designation");
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		public function edit_designation()
		{
			$this->load->helper('url');
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$data['form_name']="Manage City";
				$id=$this->uri->segment(3);
				$data['id']=$id;	
				$this->load->model('mastermodel');
				$data['rank']=$this->mastermodel->select_rank($id);
				$data['desig']=$this->mastermodel->select_designation();
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_edit_designation',$data);
				$this->load->view('Master/footer',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		public function update_designation()
		{
			$this->load->helper('url');
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$id=$this->uri->segment(3);
				$data['id']=$id;
				$this->load->model('mastermodel');
				$this->mastermodel->update_designation($id);
				header("Location:".base_url()."index.php/master/edit_designation/$id/");
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		public function stch_designation()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$id=$this->uri->segment(3);
				$this->db->where('m_des_id',$id);
				$desig=array(
				
				'm_des_status'=>$this->uri->segment(4)
				);
				$this->db->update('m03_designation',$desig);
				header("Location:".base_url()."index.php/master/view_designation");
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		//End Rank related all action here
		//Manage Branch 
		public function view_affiliate()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$data['form_name']="Manage Affiliate";
				$this->load->model('mastermodel');
				$data['state']=$this->mastermodel->select_state(1);
				$data['affiliate']=$this->mastermodel->select_affiliate();
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_affiliate',$data);
				$this->load->view('Master/footer',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		public function add_affiliate()
		{
			$this->load->helper('url');
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->load->model('mastermodel');
				$this->mastermodel->add_affiliate();
				header("Location:".base_url()."index.php/master/view_affiliate");
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		
		public function edit_affiliate()
		{
			$this->load->helper('url');
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$data['form_name']="Manage City";
				$id=$this->uri->segment(3);
				$data['id']=$id;	
				$this->load->model('mastermodel');
				$data['state']=$this->mastermodel->select_state(1);
				$data['branch']=$this->mastermodel->select_branch($id);
				$data['affiliate']=$this->mastermodel->select_affiliate();
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_edit_affiliate',$data);
				$this->load->view('Master/footer',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		public function update_affiliate()
		{
			$this->load->helper('url');
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$id=$this->uri->segment(3);
				$data['id']=$id;
				$this->load->model('mastermodel');
				$this->mastermodel->update_affiliate($id);
				header("Location:".base_url()."index.php/master/edit_affiliate/$id/");
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		public function stch_affiliate()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$id=$this->uri->segment(3);
				$this->db->where('affiliate_id',$id);
				$affiliate=array(
				'affiliate_status'=>$this->uri->segment(4)
				);
				$this->db->update('m22_affiliate',$affiliate);
				header("Location:".base_url()."index.php/master/view_affiliate");
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		
		
		//Manage View PlanMode 
		public function view_planmode()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$data['form_name']="Manage City";
				$this->db->where('p_plantype_status',1);
				$data['plan']=$this->db->get('p01_plan_type');
				$data['mstplan']=$this->db->get('p02_master_plan');
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_planmode',$data);
				$this->load->view('Master/footer',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		public function add_planmodemaster()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$data['form_name']="Manage City";
				$plnmst=array(
				'p_mst_pl_name'=>trim($this->input->post('txtplanmstname')) ,
				'p_mst_pl_depositterm'=>$this->input->post('txtplanmstdepoterm') ,
				'p_mst_pl_maturityterm'=>$this->input->post('txtplanmstmatuterm') ,
				'p_plantype_id'=>$this->input->post('ddplanmstplntyp') ,
				'p_mst_pl_weight'=>trim($this->input->post('txtplanmstwet')) ,
				'p_mst_pl_level' =>trim($this->input->post('txtplanmstlvl')),
				'p_mst_pl_paymentmode'=>trim($this->input->post('ddplanmstplntyp')) ,
				'p_mst_pl_subplanid'=>0 ,
				'p_mst_pl_spot'=>trim($this->input->post('txtplanmstspot')) ,
				'p_mst_pl_status'=>trim($this->input->post('txtstatus'))
				);
				$this->db->insert('p02_master_plan', $plnmst);
				$this->db->where('p_plantype_status',1);
				$data['plan']=$this->db->get('p01_plan_type');
				$data['mstplan']=$this->db->get('p02_master_plan');
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_planmode',$data);
				$this->load->view('Master/footer',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		//Manage View PlanMode 
		public function view_plan()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$data['form_name']="Manage City";
				$this->db->where('p_plantype_status',1);
				$data['plan_mode']=$this->db->get('p01_plan_type');
				//$data['plan_mode1']=$this->db->get('p01_plan_type');
				$data['mstplan']=$this->db->get('p02_master_plan');
				$data['rlst_plan']=$this->db->get('p03_realestate_plan');
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_plan',$data);
				$this->load->view('Master/footer',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		//Select Plan according mode
		public function select_plan()
		{
			$this->load->helper('url');
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->load->model('mastermodel');
				$data['plan']=$this->mastermodel->select_plan($this->uri->segment(3));
				$this->load->view('Master/view_select_plan',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		public function add_plan()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$plnmst=array(
				'p_mst_pl_id'=>trim($this->input->post('ddpltype')),
				'p_rlstplan_unit'=>trim($this->input->post('txtlandunit')), 
				'p_rlstplan_monthly'=>trim($this->input->post('txtmtlyamt')),
				'p_rlstplan_quaterly'=>trim($this->input->post('txtqtlyamt')),
				'p_rlstplan_halfyearly'=>trim($this->input->post('txthlfylyamt')),
				'p_rlstplan_yearly'=>trim($this->input->post('txtylyamt')), 
				'p_rlstplan_considerationvalue'=>trim($this->input->post('txtcnsdamt')),
				'p_rlstplan_estimatevalue'=>trim($this->input->post('txtetmdamt')), 
				'p_rlstplan_status'=>trim($this->input->post('txtstatus'))
				
				);
				$this->db->insert('p03_realestate_plan', $plnmst);
				header("Location:".base_url()."index.php/master/view_plan");
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		
		
		
		public function customer_reg()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->db->where('m_parent_id',1);
				$this->db->where('m_status',1);
				$data['state']=$this->db->get('m05_location');
				$data['allstate']=$this->db->get('m05_location');
				$this->db->where('p_plantype_status',1);
				$data['plan_mode']=$this->db->get('p01_plan_type');
				$data['mstplan']=$this->db->get('p02_master_plan');
				$data['rlst_plan']=$this->db->get('p03_realestate_plan');
				$data['branch']=$this->db->get('m22_affiliate');
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_customer_reg',$data);
				$this->load->view('Master/footer');
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		
		public function associate_reg()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->db->where('m_parent_id',1);
				$this->db->where('m_status',1);
				$data['state']=$this->db->get('m05_location');
				$data['allstate']=$this->db->get('m05_location');
				$data['branch']=$this->db->get('m22_affiliate');
				$data['rank']=$this->db->get('m03_designation');
				$data['bank']=$this->db->get('m04_bank');
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_associate_reg',$data);
				$this->load->view('Master/footer');
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		public function get_brcnhadd()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$branch=$this->input->post('txtintuserid');
			$this->db->where('affiliate_id',$branch);
			$data['rec']=$this->db->get('m22_affiliate');
			foreach($data['rec']->result() as $row)
			{
				break;
			}
			echo $row->affiliate_address;
		}
		
		
		public function get_intr_det()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$intr_id=$this->uri->segment(3);
			$this->db->where('or_m_user_id',$intr_id);
			$data['rec']=$this->db->get('m06_user_detail');
			$data['rank']=$this->db->get('m03_designation');
			$this->load->view('Master/view_select_introducer',$data);
		}
		
		
		public function get_intr_rank()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$intr_id=$this->input->post('txtintid');
			$this->db->where('or_m_user_id',$intr_id);
			$data['rec']=$this->db->get('m06_user_detail');
			$data['rec1']=$this->db->get('m03_designation');
			$rank="";
			foreach($data['rec']->result() as $row1)
			{
				foreach($data['rec1']->result() as $row2)
				{
					if($row1->or_m_user_id==$row2->m_des_id)
					{
						$rank=$row2->m_des_name;
						break;
					}
				}
				
			}
			
			
			
			echo $rank;
		}
		
		
		public function get_duration()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$p_id=$this->uri->segment(3);
			$this->db->where('p_mst_pl_id',$p_id);
			$data['month']=$this->db->get('p02_master_plan');
			$this->load->view('Master/duration',$data);
		}
		public function get_land_unit()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$p_id=$this->uri->segment(3);
			$this->db->where('p_mst_pl_id',$p_id);
			$data['unit']=$this->db->get('p03_realestate_plan');
			$this->load->view('Master/view_land_unit',$data);
		}
		public function get_payment_value()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$p_id=$this->uri->segment(3);
			$this->db->where('p_mst_pl_id',$p_id);
			$data['unit']=$this->db->get('p03_realestate_plan');
			$this->load->view('Master/view_land_unit',$data);
		}
		public function redem_value()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$p_id=$this->uri->segment(3);
			$this->db->where('p_rlstplan_id',$p_id);
			$data['val']=$this->db->get('p03_realestate_plan');
			$this->load->view('Master/value',$data);
		}
		public function cheque()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$this->load->view('Master/get_cheque_no');
		}
		
		public function validateUser()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			if($this->session->userdata('user_id')!="")
			{
				$this->load->model('users_records');
				$this->users_records->introducerid();
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		/*----------------Add Main Menu--------------------*/
		
		public function main_menu()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$data['main_menu']=$this->db->get('m01_menu');
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_main_menu',$data);
				$this->load->view('sidebar');
			    $this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		
		public function add_mainmenu()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$timezone = new DateTimeZone("Asia/Kolkata" );
				$date = new DateTime();
				$date->setTimezone($timezone);
				$mmenu=array(
				'm_menu_name'=>$this->input->post('txtmname'),
				'm_menu_desc'=>$this->input->post('txtdesc'),
				'm_menu_status'=>$this->input->post('txtstatus'),
				'm_entrydate'=>$date->format( 'Y-m-d H-i-s' )
				);
				$this->db->insert('m01_menu',$mmenu);
				redirect('master/main_menu');
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		public function stch_menu()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$id=$this->uri->segment(3);
				$mmenu=array(
				'm_menu_status'=>$this->uri->segment(4)
				);
				$this->db->where('m_menu_id',$id);
				$this->db->update('m01_menu',$mmenu);
				$data['main_menu']=$this->db->get('m01_menu');
				redirect('master/main_menu');
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		
		public function edit_menu()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$id=$this->uri->segment(3);
			if($this->session->userdata('user_id')!="")
			{
				$this->db->where('m_menu_id',$id);
				$data['menu']=$this->db->get('m01_menu');
				$data['id']=$id;
				$data['main_menu']=$this->db->get('m01_menu');
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_edit_main_menu',$data);
				$this->load->view('sidebar');
			    $this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		public function update_menu()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$id=$this->uri->segment(3);
				$mmenu=array(
				'm_menu_name'=>$this->input->post('txtmname')
				);
				$this->db->where('m_menu_id',$id);
				$this->db->update('m01_menu',$mmenu);
				redirect('master/main_menu');
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		
		/*----------------Add Sub Menu--------------------*/
		
		
		public function sub_menu()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->db->where('m_menu_status',1);
				$data['menu']=$this->db->get('m01_menu');
				$data['submenu']=$this->db->get('m02_submenu');
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_sub_menu',$data);
				$this->load->view('sidebar');
			     $this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		
		public function add_submenu()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$timezone = new DateTimeZone("Asia/Kolkata" );
				$date = new DateTime();
				$date->setTimezone($timezone);
				$smenu=array(
				'm_submenu_name'=>$this->input->post('txtsname'),
				'm_submenu_desc'=>$this->input->post('txtsdesc'),
				'm_sbcatlink'=>$this->input->post('txtslink'),
				'm_menu_id'=>$this->input->post('main_menu'),
				'm_entrydate'=>$date->format( 'Y-m-d H-i-s' ),
				'm_submenu_status'=>$this->input->post('txtstatus')
				);
				$this->db->insert('m02_submenu',$smenu);
				redirect('master/sub_menu');
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		public function st_chsubmst()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$smenu=array(
				'm_submenu_status'=>$this->uri->segment(4)
				);
				$this->db->where('m_submenu_id',$this->uri->segment(3));
				$this->db->update('m02_submenu',$smenu);
				$this->db->where('m_menu_status',1);
				$data['menu']=$this->db->get('m01_menu');
				$data['submenu']=$this->db->get('m02_submenu');
				redirect('Master/sub_menu');	
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		public function select_submenu()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$id=$this->uri->segment(3);
				$this->db->where('m_menu_id',$id);
				$data['submenu']=$this->db->get('m02_submenu');
				$this->load->view('Master/viewassign_smenu',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		
		public function edit_smenu()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$id=$this->uri->segment(3);
				$this->db->where('m_submenu_id',$id);
				$data['smenu']=$this->db->get('m02_submenu');
				$data['menu']=$this->db->get('m01_menu');
				$data['id']=$id;
				$data['submenu']=$this->db->get('m02_submenu');
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_edit_sub_menu',$data);
				$this->load->view('sidebar');
			    $this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		
		public function edit_submenu()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$smenu=array(
				'm_submenu_name'=>$this->input->post('txtsname'),
				'm_sbcatlink'=>$this->input->post('txtslink')
				);
				$this->db->where('m_submenu_id',$this->uri->segment(3));
				$this->db->update('m02_submenu',$smenu);
				redirect('master/sub_menu');
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		
		public function assign_apps()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->db->where('m_menu_status',1);
				$data['menu']=$this->db->get('m01_menu');
				$data['submenu']=$this->db->get('m02_submenu');
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_assign_apps',$data);
				$this->load->view('Master/footer');
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		
		public function target_plan()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$data['target']=$this->db->get('p05_plan_target');
				$this->db->where('m_des_pat_id','0');
				$this->db->where('m_des_status','1');
				$data['desig']=$this->db->get('m03_designation');
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_targetplan',$data);
				$this->load->view('Master/footer');
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		
		public function add_target_plan()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$data=array(
				'target_cadername'=>$this->input->post('ddcader_name'),
				'target_bussiness'=>$this->input->post('txtbussiness'),
				'target_team'=>$this->input->post('txtteam'),
				'target_advisor'=>$this->input->post('txtadvisor'),
				'target_promoter'=>$this->input->post('txtpromoter'),
				'target_organiser'=>$this->input->post('txtorganiser'),
				'target_motivator'=>$this->input->post('txtmotivator'),
				'target_co_ordinator'=>$this->input->post('txtco_ordnator'),
				'target_s_co_ordinator'=>$this->input->post('txts_coordinator'),
				'target_regional_manager'=>$this->input->post('txtregional_manager'),
				'target_zonal_manager'=>$this->input->post('txtzonal_manager'),
				'target_executive_manager'=>$this->input->post('txtexecutive_manager'),
				'target_promotion'=>$this->input->post('ddpromotion'),
				'target_status'=>'1',
				);
				$this->db->insert('p05_plan_target',$data);
				header("Location:".base_url()."index.php/master/target_plan");
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		
		public function view_plan_insentive()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->db->where('p_plantype_status',1);
				$data['plan_mode']=$this->db->get('p01_plan_type');
				$data['mstplan']=$this->db->get('p02_master_plan');
				$data['insentive']=$this->db->get('p04_plan_insentive');
				$this->db->where('m_des_pat_id',0);
				$data['designation']=$this->db->get('m03_designation');
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_plan_insentive',$data);
				$this->load->view('Master/footer',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		
		
		
		public function add_plan_insentive()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$data=array(
				'instvplan_designation'=>$this->input->post('ddcaders'),
				'instvplan_mode'=>$this->input->post('ddplnmode'),
				'instvplan_type'=>$this->input->post('ddpltype'),
				'instvplan_first'=>$this->input->post('txtfirst'),
				'instvplan_second'=>$this->input->post('txtsecond'),
				'instvplan_third'=>$this->input->post('txtthird'),
				'instvplan_last'=>$this->input->post('txtlast'),
				'instvplan_status'=>'1',
				);
				$this->db->insert('p04_plan_insentive',$data);
				header("Location:".base_url()."index.php/master/view_plan_insentive");
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		
		
		
		
		
		/*------------------Assign Apps To user-------------------*/
		
		public function view_assign_app()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->db->where('m_menu_status',1);
				$data['menu']=$this->db->get('m01_menu');
				
				$des=array('-1','-2');
				$this->db->where_in('m_des_pat_id',$des);
				$data['designation']=$this->db->get('m03_designation');
				
				$data['assign']=$this->db->get('m19_des_menu');
				
				
				$data['assign']=$this->db->get('m19_des_menu');
				$data['desig']=$this->db->get('m03_designation');
				$data['desig1']=$this->db->get('m03_designation');
				$this->db->where('m_menu_status',1);
				$data['menu']=$this->db->get('m01_menu');
				$this->db->where('m_menu_status',1);
				$data['menu1']=$this->db->get('m01_menu');
				$this->load->view('Admin/header');
				
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_assign_app',$data);
				$this->load->view('Master/footer');
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		
		
		public function add_assign_menu()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$gp=array(
				'm_des_id'=>$this->input->post('dddesig'),
				'm_menu_id'=>$this->input->post('main_menu'),
				'm_dm_status'=>$this->input->post('txtstatus')
				);
				$this->db->insert('m19_des_menu',$gp);
				$data['form_name']="Assign Menu to Designation";
				$data['assign']=$this->db->get('m19_des_menu');
				$data['desig']=$this->db->get('m03_designation');
				$data['desig1']=$this->db->get('m03_designation');
				$this->db->where('m_menu_status',1);
				$data['menu1']=$this->db->get('m01_menu');
				$this->db->where('m_menu_status',1);
				$data['menu']=$this->db->get('m01_menu');
				
				header("Location:".base_url()."index.php/master/view_assign_app");
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		
		public function stch_asmenu()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$gp=array(
				'm_dm_status'=>$this->uri->segment(4)
				);
				$this->db->where('m_dm_id',$this->uri->segment(3));
				$this->db->update('m19_des_menu',$gp);
				header("Location:".base_url()."index.php/master/view_assign_app");
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		
		
		public function view_assign_subapp()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->db->where('m_menu_status',1);
				$data['menu']=$this->db->get('m01_menu');	
				$des=array('-1','-2');
				$this->db->where_in('m_des_pat_id',$des);
				$data['designation']=$this->db->get('m03_designation');	
				$data['submenu']=$this->db->get('m20_des_smenu');
				
				$data['assign']=$this->db->get('m19_des_menu');
				$data['assigns']=$this->db->get('m20_des_smenu');
				$des=array('-1','-2');
				$this->db->where_in('m_des_pat_id',$des);
				$data['desig']=$this->db->get('m03_designation');
				$des=array('-1','-2');
				$this->db->where_in('m_des_pat_id',$des);
				$data['desig1']=$this->db->get('m03_designation');
				
				$this->db->where('m_menu_status',1);
				$data['menu1']=$this->db->get('m01_menu');
				$data['smenu']=$this->db->get('m02_submenu');
				
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/view_assign_subapp',$data);
				$this->load->view('Master/footer');
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		
		
		public function select_menu()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$id=$this->uri->segment(3);
				$this->db->where('m_des_id',$id);
				$data['assign']=$this->db->get('m19_des_menu');
				$this->db->where('m_menu_status',1);
				$data['menu']=$this->db->get('m01_menu');
				$this->load->view('Master/view_menu',$data);
				
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		
		
		
		public function select_submenu2()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->db->where('m_menu_id',$this->uri->segment(3));
				$data['submenu']=$this->db->get('m02_submenu');
				
				$this->load->view('Master/view_submenu',$data);
				
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		
		
		
		public function add_assign_smenu()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$gp=array(
				'm_dm_id'=>$this->input->post('main_menu'),
				'm_submenu_id'=>$this->input->post('sub_menu'),
				'm_dsm_status'=>$this->input->post('txtstatus'),
				'm_dsm_islink'=>$this->input->post('txtislink')
				);
				$this->db->insert('m20_des_smenu',$gp);
				header("Location:".base_url()."index.php/master/view_assign_subapp");
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}	
		}
		
		
		
		public function stch_assmenu()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->db->where('m_des_id',$this->input->post('desig'));
				$this->db->where('m_menu_id',$this->input->post('main_menu'));
				$data['dm']=$this->db->get('m19_des_menu');
				foreach($data['dm']->result() as $row)
				{
					break;
				}
				$gp=array(
				'm_dsm_status'=>$this->uri->segment(4)
				);
				$this->db->where('m_dsm_id',$this->uri->segment(3));
				$this->db->update('m20_des_smenu',$gp);
				header("Location:".base_url()."index.php/master/view_assign_subapp");
			}
			else
			{
				header("Location:".base_url()."index.php/master/logout");
			}
		}
		
		public function account()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->db->where('m_acc_status',1);
				//$this->db->where('m_ic_parent',0);
				$data['menu']=$this->db->get('m47_account');
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Master/accounts',$data);
				$this->load->view('Master/footer');
			}
			else
			{
				header("Location:".base_url()."index.php/welcome/index");
			}
		}
		
		public function account_load()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$id=$this->uri->segment(3);
				$this->db->where('m_acc_id',$id);
				$data['details']=$this->db->get('m47_account');
				$this->load->view('Master/accounts_load',$data);
				
			}
			else
			{
				header("Location:".base_url()."index.php/welcome/index");
			}
		}
		
		public function insert_account()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$account = array
				(
				'm_acc_name'=>$this->input->post('txtsubledname'),
				'm_acc_num'=>$this->input->post('txtsubled'),
				'm_acc_descrip'=>$this->input->post('txtsubledname'),
				'm_acc_parent'=>$this->input->post('m_acc_id'),
				'm_acc_for'=>1,
				'm_acc_status'=>1,
				'm_main_acc_id'=>$this->input->post('m_main_acc_id')
				);
				$this->db->insert('m47_account',$account);
				echo "true";
			}
			
			else
			{
				header("Location:".base_url()."index.php/welcome/index");
			}
		}
		
		
		
	}
?>																																			