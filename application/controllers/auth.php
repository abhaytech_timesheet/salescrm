<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Auth extends CI_Controller 
	{
		public function __construct() 
		{
			parent::__construct();
			$timezone = new DateTimeZone("Asia/Kolkata");
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
		    $this->curryear=$date->format('Y');
		    $this->currmonth=$date->format('m');
		    $this->currday=$date->format('d');
		    $this->currhour=$date->format('H');
		    $this->currminute=$date->format('i');
		    $this->currsecond=$date->format('s');
			//$this->_is_logged_in();
		}
		//Check Affiliate Registered Or Not on Portal
		
		public function is_affiliate()
		{
			if($this->uri->segment(3)=="")
			{
				$data['info']="";
			}
			else
			{
				$data['info']=$this->uri->segment(3);
			}
			if(base_url()=="http://innovativetech.tech/sales/CRM/")
			{
				$data['aff_id']=$this->db->query("SELECT GetAffiliateId('".base_url()."',1) as Aff_id");
				foreach($data['aff_id']->result() as $row1) {break;}
				if($row1->Aff_id==base_url())
				{
					echo $row1->Aff_id.'-'."THIS URL IS NOT REGISTERED .Please Contact Admin";
				}
				else
				{
					$data['affiliates']=$row1->Aff_id;
					$this->db->where('affiliate_id','1');
					$data['aff_detail']=$this->db->get('m22_affiliate');
				}
			}
			else
			{
				$data['affiliates']=0;
				$this->db->where('affiliate_id',0);
			    $data['aff_detail']=$this->db->get('m22_affiliate');
			}
			return $data;
		}
		
		
		
		public function get_affiliate()
		{
			$data['aff_id']=$this->db->query("SELECT GetAffiliateId('".base_url()."',1) as Aff_id");
			foreach($data['aff_id']->result() as $row1) {break;}
			if($row1->Aff_id==base_url())
			{
				return $row1->Aff_id.'/'."THIS URL IS NOT REGISTERED . Please Contact Admin";
			}
			else 
			{
				return "true";	
			}
		}
		
		/*Auth First Action */
		public function index()
		{
			$this->destroy_session(); 
			$data=$this->is_affiliate();
			$data['info']=$this->uri->segment(3);
			$this->load->view('Auth/header',$data);
			$this->load->view('Auth/login',$data);
			$this->load->view('Auth/footer',$data);
		}
		
		/* Super Admin Login here !*/
		public function login()
		{
			if($this->session->userdata('user_id')=="" )
			{
				if($this->input->post('txtlogin')!="" && $this->input->post('txtpwd')!="" )
				{
					$query00 = $this->db->get_where('m00_setconfig',array('m00_username'=>$this->input->post('txtlogin'),'m00_password'=>$this->input->post('txtpwd'), 'm00_status'=>1));
					$row00 = $query00->row();
					if($query00->num_rows()==1)
					{
						if(sendemail=='1')
						{
							$this->send_alert_email(ucwords("Super Admin"),$row00->m00_username);
						}
						if(sendsms=='1')
						{
							$this->send_alert_sms($row00->m00_primary_contact);
						}
						if(sendotp=='1')
						{
							$this->send_otp($row00->m00_primary_contact,$row00->m00_username,1);
						}
						else
						{
							$sessiondata=array(
							'user_id'=>"9999999999",
							'profile_id'=>"0",
							'e_email'=>$this->input->post('txtlogin'),
							'name'=>"Super Admin",
							'designation'=>0,
							'affid'=>'0',
							'logged_in'=>TRUE,
							'user_type'=>0
							);
							$this->session->set_userdata($sessiondata);
							header("Location:".base_url()."report/dashboard/");
						}
					}
					else
					{
						$this->destroy_session();
						header("Location:".base_url()."auth/index/10");
					}
				}
				else
				{
					$this->destroy_session();
					header("Location:".base_url()."auth/index/1");
				}
			}
			else
			{
				$this->destroy_session();
				header("Location:".base_url()."auth/index/6");
			}
		}
		/* Employee Login here !*/
		public function employee_login()
		{
			if($this->session->userdata('user_id')=="")
			{
				if($this->input->post('txtlogin')!="" && $this->input->post('txtpwd')!="" )
				{
					$query = $this->db->get_where('tr04_login',array('tr04_login_id'=>$this->input->post('txtlogin'),'tr04_login_pwd'=>$this->input->post('txtpwd')));
					$row2 = $query->row();
					if($query->num_rows()==1)
					{
						$query1 = $this->db->get_where('m06_user_detail',array('or_m_email'=>$this->input->post('txtlogin')));
						$row = $query1->row();
						if($row->or_m_status!=0)
						{
							$sessiondata=array(
							'user_id'=>$row->or_m_mobile_no,
							'profile_id' => $row->or_m_reg_id,
							'e_email'  => $row->or_m_email,
							'name'     => ucwords($row->or_m_name),
							'designation' =>$row->or_m_designation,
							'affid' =>$row->or_m_aff_id,
							'logged_in' => "TRUE",
                            'user_image'=>$row->or_m_userimage,
							'user_type'=>1,
							'owned_by'=>$row->or_m_aff_id
							);
							$this->session->set_userdata($sessiondata);
							$att_pr='L';
							$data['att_pr']=$this->db->query("SELECT GetAttendanceProcess() as Att_pr");
							foreach($data['att_pr']->result() as $attrow){}
							$att_pr=$attrow->Att_pr;
							if($att_pr=='L')
							{
								$atten_data=array (
								'proc'=>1,
								'or_m_reg_id'=>$row->or_m_reg_id,
								'year_of_attendance'=>$this->curryear,
								'month_of_attendance'=>$this->currmonth,
								'day_of_attendance'=>$this->currday,
								'hour_of_attendance'=>$this->currhour,
								'min_of_attendance'=>$this->currminute,
								'sft_strttime_of_attendance'=>$this->curryear,
								'sft_endtime_of_attendance'=>$this->curryear,
								'atten_date'=>$this->curr,
								'entry_date'=>$this->curr,
								'or_m_designation'=>$row->or_m_designation,
								'sms_alert'=>0,
								'attn_flage'=>0, 
								'is_upload'=>0,
								'or_m_aff_id'=>$row->or_m_aff_id
								);
								$queryy = "CALL sp_attendance(?" . str_repeat(",?", count($atten_data)-1) .")";
								$data['rec']=$this->db->query($queryy,$atten_data);
								$this->db->free_db_resource();
							}
							header("Location:".base_url()."report/acc_dashboard/");
						}
						else
						{
							$this->destroy_session();
							header("location:".base_url()."index.php/auth/index/4");	
						}
					}
					else
					{
						$this->destroy_session();
						header("location:".base_url()."index.php/auth/index/5");
					}
				}
				else
				{
					$this->destroy_session();
					header("Location:".base_url()."index.php/auth/index/1");
				}
			}
			else
			{
				$this->destroy_session();
				header("Location:".base_url()."index.php/auth/index/6");
			}
		}
		/* Affiliate Login here !*/
		public function affiliate_login()
		{
			if($this->session->userdata('user_id')=="")
			{
				if($this->input->post('txtlogin')!="" && $this->input->post('txtpwd')!="" )
				{
					$query = $this->db->get_where('tr04_login',array('tr04_login_id'=>$this->input->post('txtlogin'),'tr04_login_pwd'=>$this->input->post('txtpwd')));
					$row2 = $query->row();
					if($query->num_rows()==1)
					{
						if($row2->tr04_login_type!=0)
						{
							$query1 = $this->db->get_where('m34_contact',array('contact_email'=>$this->input->post('txtlogin'),'contact_status'=>1));
							$row = $query1->row();
							if($row->is_account!=0)
							{
								$query11 =$this->db->get_where('m33_account',array('account_id'=>$row->account_id,'account_status'=>1));
								$row3 =$query11->row();
								if($query11->num_rows()==1)
								{
									if(sendemail=='1')
									{
										$this->send_alert_email(ucwords($row->contact_name),$row->contact_email);
									}
									if(sendemail=='1')
									{
										$this->send_alert_sms($row->contact_mobile);
									}
									if(sendotp=='1')
									{
										$this->send_otp($row->contact_mobile,$row->contact_email,3);
									}
									else
									{
										$sessiondata=array(
										'user_id'=>$row->contact_mobile,
										//'profile_id'=>$row->contact_id,
										'profile_id' => $row2->tr04_user_id,
										'e_email'=>$row->contact_email,
										'name'=>ucwords($row->contact_name),
										'designation'=>$row3->account_type,
										'affid'=>$row->account_id,
										'logged_in'=>"TRUE",
                                        'user_image'=>$row->contact_image,
										'user_type'=>2,
										'owned_by'=>$row3->account_owned_by
										);
										$this->session->set_userdata($sessiondata);
								
										header("Location:".base_url()."report/dashboard/");
									}
								}
								else
								{
									$this->destroy_session();
									header("Location:".base_url()."auth/index/7");
								}
							}
							else
							{
								$this->destroy_session();
								header("location:".base_url()."auth/index/1");	
							}
						}
						else
						{
							$query1 = $this->db->get_where('m06_user_detail',array('or_m_email'=>$this->input->post('txtlogin')));
							$row = $query1->row();
							if($row->or_m_status!=0)
							{
								
								if(sendemail=='1')
								{
									//$this->send_alert_email(ucwords($row->or_m_name),$row->or_m_email);
								}
								if(sendsms=='1')
								{
									//$this->send_alert_sms($row->or_m_mobile_no);	
								}
								if(sendotp=='1')
								{
									//$this->send_otp($row->or_m_mobile_no,$row->or_m_email,2);
								}
								else
								{
									$sessiondata=array(
									'user_id'=>$row->or_m_mobile_no,
									'profile_id' => $row->or_m_reg_id,
									'e_email'  => $row->or_m_email,
									'name'     => ucwords($row->or_m_name),
									'designation' =>$row->or_m_designation,
									'affid' =>$row->or_m_aff_id,
									'logged_in' => "TRUE",
									'user_image'=>$row->or_m_userimage,
							        'user_type'=>1
									);
									$this->session->set_userdata($sessiondata);
									$att_pr='L';
									$data['att_pr']=$this->db->query("SELECT GetAttendanceProcess() as Att_pr");
									foreach($data['att_pr']->result() as $attrow){}
									$att_pr=$attrow->Att_pr;
									if($att_pr=='L')
									{
										$atten_data=array (
										'proc'=>1,
										'or_m_reg_id'=>$row->or_m_reg_id,
										'year_of_attendance'=>$this->curryear,
										'month_of_attendance'=>$this->currmonth,
										'day_of_attendance'=>$this->currday,
										'hour_of_attendance'=>$this->currhour,
										'min_of_attendance'=>$this->currminute,
										'sft_strttime_of_attendance'=>$this->curryear,
										'sft_endtime_of_attendance'=>$this->curryear,
										'atten_date'=>$this->curr,
										'entry_date'=>$this->curr,
										'or_m_designation'=>$row->or_m_designation,
										'sms_alert'=>0,
										'attn_flage'=>0, 
										'is_upload'=>0,
										'or_m_aff_id'=>$row->or_m_aff_id
										);
										$queryy = "CALL sp_attendance(?" . str_repeat(",?", count($atten_data)-1) .")";
										$data['rec']=$this->db->query($queryy,$atten_data);
										$this->db->free_db_resource();
									}
									header("Location:".base_url()."report/dashboard/");
								}
							}
							else
							{
								$this->destroy_session();
								header("location:".base_url()."auth/index/4");	
							}
						}
					}
					else
					{
						$this->destroy_session();
						header("location:".base_url()."auth/index/5");
					}
				}
				else
				{
					$this->destroy_session();
					header("Location:".base_url()."auth/index/1");
				}
			}
			else
			{
				$this->destroy_session();
				header("Location:".base_url()."auth/index/6");
			}
		}
		
		public function logout()
		{
			$att_pr='L';
			$data['att_pr']=$this->db->query("SELECT GetAttendanceProcess() as Att_pr");
			foreach($data['att_pr']->result() as $attrow){}
			$att_pr=$attrow->Att_pr;
			if($att_pr=='L' && $this->session->userdata('user_type')==1)
			{
				if($this->session->userdata('profile_id')!="" && $this->session->userdata('profile_id')!=0 )
				{
					$atten_data=array (
					'proc'=>2,
					'or_m_reg_id'=>$this->session->userdata('profile_id'),
					'year_of_attendance'=>$this->curryear,
					'month_of_attendance'=>$this->currmonth,
					'day_of_attendance'=>$this->currday,
					'hour_of_attendance'=>$this->currhour,
					'min_of_attendance'=>$this->currminute,
					'sft_strttime_of_attendance'=>$this->curryear,
					'sft_endtime_of_attendance'=>$this->curryear,
					'atten_date'=>$this->curr,
					'entry_date'=>$this->curr,
					'or_m_designation'=>$this->session->userdata('designation'),
					'sms_alert'=>0,
					'attn_flage'=>'0', 
					'is_upload'=>0,
					'or_m_aff_id'=>$this->session->userdata('affid')
					);
					$queryy = "CALL sp_attendance(?" . str_repeat(",?", count($atten_data)-1) .")";
					$data['rec']=$this->db->query($queryy,$atten_data);
					$this->db->free_db_resource();
				}
			}
			header("Location:".base_url()."auth/index/2/");
		}
		
		public function destroy_session()
		{
			$this->session->unset_userdata('profile_id');
			$this->session->unset_userdata('user_id');
			$this->session->unset_userdata('e_email');
			$this->session->unset_userdata('name');
			$this->session->unset_userdata('designation');
			$this->session->unset_userdata('logged_in');
            $this->session->unset_userdata('user_image');
            $this->session->unset_userdata('user_type');
			$this->session->sess_destroy();
		}
		
		
		//reset password
		public function resetpassword()
		{
			$query0 = $this->db->get_where('m34_contact',array('contact_email'=>$this->input->post('email'), 'contact_status'=>1));
			$row = $query0->row();
			if($query0->num_rows()>0)
			{
				$this->load->helper('string');
				$loginpass=random_string('numeric',7);
				$reset=array(
				'or_login_pwd'=>$loginpass
				);
				$this->db->where('or_login_id',$this->input->post('email'));
				$this->db->update('tr04_login',$reset);
				//$msg="Your Password Reset.Your UID:".$this->input->post('email').",Pass:".$loginpass.".";
				$mob=$this->input->post('email');
				$message = "Your Password Reset.Your UID:".$this->input->post('email').",Pass:".$loginpass.".";
				$this->load->library('email');
				$this->email->set_newline("\r\n");
				$this->email->from(EMAIL,'Support APPWORKS'); // change it to yours
				$this->email->to($txtemail);// change it to yours
				$this->email->subject('APPWORKS Reset Password here');
				$this->email->message($message);
				if($this->email->send())
				{
					//header("Location:".base_url()."auth/index/2");
				}
				else
				{
					show_error($this->email->print_debugger());
				}
				//$this->send_sms($mob,$msg);
				header("Location:".base_url()."auth/index/2");
			}
			else
			{
				$data=$this->is_affiliate();
				$data['aff_id']=$this->db->query("SELECT GetAffiliateId('".base_url()."',1) as Aff_id");	
				$data['info']=$this->uri->segment(3);
				$this->load->view('Auth/header',$data);
				$this->load->view('Auth/login',$data);
				$this->load->view('Auth/footer',$data);
			}
			
		}
		public function send_otp($contact,$email,$type)
		{
			$message=rand(10000,999999);
			$data['msg']=$message;
			$msg="One Time Password on APPWORKS is ".$message." This step is important for the safety of your account.";
			$mob=$contact;
			//$this->send_sms($mob,$msg);
			$this->load->library('email');
			$config = array (
			'charset'  => 'utf-8',
			'protocol' => 'sendmail',
			'priority' => '1'
			);
			$this->email
			->set_newline("\r\n")
			->from(EMAIL,'Otp Mail')			 			// change it to yours
			->to($email)								    // change it to yours
			->subject('One Time Password For APPWORKS')
			->message($msg)
			->set_mailtype('html');
			$this->email->send();
			$data['type']=$type;
			$data['txtlogin']=$this->input->post('txtlogin');
			$this->load->view('auth/header');
			$this->load->view('auth/otp_login',$data);
			$this->load->view('auth/footer',$data);
		}
		
		public function authentcate_otppassword()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$msg=$this->input->post('msg');
			if($this->input->post('pinpassword')==$msg)
			{
				if($this->input->post('type')=='1')
				{
					$id=$this->input->post('login');
					$sessiondata=array(
					'user_id'=>"9999999999",
					'profile_id'=>"0",
					'e_email'=>$id,
					'name'=>"Super Admin",
					'designation'=>"Super Admin",
					'affid'=>'0',
					'logged_in'=>TRUE,
					'user_type'=>0
					);
					$this->session->set_userdata($sessiondata);
					echo 'true';
					
				}
				if($this->input->post('type')=='2')
				{
					$query1 = $this->db->get_where('m06_user_detail',array('or_m_email'=>$this->input->post('txtlogin')));
					$row = $query1->row();
					$sessiondata=array(
					'user_id'=>$row->or_m_mobile_no,
					'profile_id' => $row->or_m_reg_id,
					'e_email'  => $row->or_m_email,
					'name'     => ucwords($row->or_m_name),
					'designation' =>$row->or_m_designation,
					'affid' =>$row->or_m_aff_id,
					'logged_in' => "TRUE",
					'user_image'=>$row->or_m_userimage,
					'user_type'=>1
					);
					//var_dump($sessiondata);
					$this->session->set_userdata($sessiondata);
					$att_pr='L';
					$data['att_pr']=$this->db->query("SELECT GetAttendanceProcess() as Att_pr");
					foreach($data['att_pr']->result() as $attrow){}
					$att_pr=$attrow->Att_pr;
					if($att_pr=='L')
					{
						$atten_data=array (
						'proc'=>1,
						'or_m_reg_id'=>$row->or_m_reg_id,
						'year_of_attendance'=>$this->curryear,
						'month_of_attendance'=>$this->currmonth,
						'day_of_attendance'=>$this->currday,
						'hour_of_attendance'=>$this->currhour,
						'min_of_attendance'=>$this->currminute,
						'sft_strttime_of_attendance'=>$this->curryear,
						'sft_endtime_of_attendance'=>$this->curryear,
						'atten_date'=>$this->curr,
						'entry_date'=>$this->curr,
						'or_m_designation'=>$row->or_m_designation,
						'sms_alert'=>0,
						'attn_flage'=>'O', 
						'is_upload'=>0,
						'or_m_aff_id'=>$row->or_m_aff_id
						);
						$queryy = "CALL sp_attendance(?" . str_repeat(",?", count($atten_data)-1) .")";
						$data['rec']=$this->db->query($queryy,$atten_data);
						$this->db->free_db_resource();
					}
					echo 'true';
					
				}
				if($this->input->post('type')=='3')
				{
					$query1 = $this->db->get_where('m34_contact',array('contact_email'=>$this->input->post('txtlogin'),'contact_status'=>1));
					$row = $query1->row();
					$query11 =$this->db->get_where('m33_account',array('account_id'=>$row->account_id,'account_status'=>1));
					$row3 =$query11->row();
					$sessiondata=array(
					'user_id'=>$row->contact_mobile,
					'profile_id'=>$row->contact_id,
					//'profile_id' => $row2->or_user_id,
					'e_email'=>$row->contact_email,
					'name'=>ucwords($row->contact_name),
					'designation'=>$row3->account_type,
					'affid'=>$row->account_id,
					'image' =>$row3->account_image,
					'logged_in'=>"TRUE",
					'user_image'=>$row->contact_image,
					'user_type'=>2
					);
					$this->session->set_userdata($sessiondata);
					echo 'true';
				}
				
			}
			else
			{
				echo 'false';
			}
		}
		
		public function send_alert_email($txtname,$txtemail)
		{
			$txtsubject="Login at ERP 1.0";
			$ip=$_SERVER['REMOTE_ADDR'];
			$message = '<p>Dear: '.$txtname.',<br><br>
			Thank you for Login. You will be notified when a response is made by email.<br><br>
			Subject: '. $txtsubject.'<br><br>
			Your IP: '. $ip.'<br><br></p>';
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL,'Support APPWORKS'); // change it to yours
			$this->email->to($txtemail);// change it to yours
			$this->email->subject('ERP 1.0 Login');
			$this->email->message($message);
			if($this->email->send())
			{
				//header("Location:".base_url()."direct_ticket/index");
			}
			else
			{
				//show_error($this->email->print_debugger());
			}
		}
		
		public function send_alert_sms($contact)
		{
			$msg="You have at login ferryipl portal";
			$mob=$contact;
		
			//$this->send_sms($mob,$msg);
		}
			public function send_alert_emaildemo()
		{
		
		echo $this->send_alert_email(ucwords("VIVEK"),"con2ser@gmail.com");
			//$this->send_sms($mob,$msg);
		}
		
		public function send_sms($mob,$msg)
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$this->db->where('m_api_url_for',11);
			$data['apiurl']=$this ->db->get('m16_api_url');
			foreach($data['apiurl']->result() as $row)
			{
				if($row->m_api_url_id=="9" && $row->m_api_url_status=="1")
				{
					$url=$row->m_api_url_address;
					$params = array (
					'userName'=>'ferryinfotech',
					'pwd'=>'qwerty',
					'mobile'=>$mob,
					'message'=>$msg,
					'root'=>'Premium',
					'type'=>'Transactional',
					'senderID'=>'ETOPUP'
					);
					$options = array(
					CURLOPT_SSL_VERIFYHOST => 0,
					CURLOPT_SSL_VERIFYPEER => 0
					);
					
					$defaults = array(
					CURLOPT_URL => $url. (strpos($url, '?') 
					=== FALSE ? '?' : ''). http_build_query($params),
					CURLOPT_HEADER => 0,
					CURLOPT_RETURNTRANSFER => TRUE,
					CURLOPT_TIMEOUT =>56
					);
					
					$ch = curl_init();
					curl_setopt_array($ch, ($options + $defaults));
					$result = curl_exec($ch);
					if(!$result)
					{
						trigger_error(curl_error($ch));
						$flag=0;
					}
					else
					{	                
						$flag=1;
					}
					curl_close($ch);
					return $result; 
				}
				if($row->m_api_url_id=="10" && $row->m_api_url_status=="1")
				{
					$url=$row->m_api_url_address;
					$params = array (
					'uname'=>'ferry123',
					'pass'=>'727510',
					'send'=>'ETOPUP',
					'dest'=>$mob,
					'msg'=>$msg
					);
					$options = array(
					CURLOPT_SSL_VERIFYHOST => 0,
					CURLOPT_SSL_VERIFYPEER => 0
					);
					
					$defaults = array(
					CURLOPT_URL => $url. (strpos($url, '?') 
					=== FALSE ? '?' : ''). http_build_query($params),
					CURLOPT_HEADER => 0,
					CURLOPT_RETURNTRANSFER => TRUE,
					CURLOPT_TIMEOUT =>56
					);
					
					$ch = curl_init();
					curl_setopt_array($ch, ($options + $defaults));
					$result = curl_exec($ch);
					if(!$result)
					{
						trigger_error(curl_error($ch));
						$flag=0;
					}
					else
					{	                
						$flag=1;
					}
					curl_close($ch);
					return $result; 
				}	
			}
		}
		
	}
?>