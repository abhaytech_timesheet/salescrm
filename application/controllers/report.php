<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Report extends CI_Controller {
		
		public function __construct() 
		{
			parent::__construct();
			$timezone = new DateTimeZone("Asia/Kolkata");
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
			$this->_is_logged_in();
			$this->data1=$this->crm_model->assign_menu();
		}
		
		/*Custom Function here*/
		
		/*Session Check Function */ 
		public function _is_logged_in() 
		{
		    $is_logged_in = $this->session->userdata('profile_id');
			if ($is_logged_in=="") 
			{
				redirect('auth/index/1');
				die();
			}
		}
		
		/*Dashboard */
		public function dashboard()
		{  
			$data['new']=$this->db->query("SELECT COUNT(DISTINCT(tkt_ref_no)) AS new_tic FROM m46_submit_ticket WHERE tkt_status=1 AND affiliate_id =".$this->session->userdata('affid')." AND DATE_FORMAT(tkt_sub_date,'%y-%m-%d')=CURDATE()");
            $data['acount_ticket']=$this->db->query("SELECT COUNT(DISTINCT(tkt_ref_no)) AS new_tic FROM m46_submit_ticket WHERE tkt_status=1 AND affiliate_id =".$this->session->userdata('affid')." AND DATE_FORMAT(tkt_sub_date,'%y-%m-%d')=CURDATE() AND emp_id IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid').") ")->row();
			$data['complete']=$this->db->query("SELECT COUNT(DISTINCT(tkt_ref_no)) as complete FROM m46_submit_ticket WHERE tkt_status='0' AND affiliate_id =".$this->session->userdata('affid')." ");
			$data['pending']=$this->db->query("SELECT COUNT(DISTINCT(tkt_ref_no)) AS pending FROM m46_submit_ticket WHERE tkt_status=1 AND affiliate_id =".$this->session->userdata('affid')." AND DATE_FORMAT(tkt_sub_date,'%y-%m-%d')<CURDATE()");
			$data['curr']=$this->curr;
			$data['lead1']=$this->db->query("SELECT COUNT(*) AS lead_no FROM vw_lead WHERE Lead_Status1 NOT IN(7,8) AND (CASE WHEN(".$this->session->userdata('user_type')."=2) THEN (lead_assign_id IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid')." ) AND Lead_Created_By=1) OR (lead_assign_id=".$this->session->userdata('profile_id')." AND Lead_Created_By=2) WHEN(".$this->session->userdata('user_type')."=1) THEN lead_assign_id=".$this->session->userdata('profile_id')." AND Lead_Created_By=1 ELSE 1 END) ORDER BY Lead_Created_On  DESC")->row()->lead_no;
			$data['account1']=$this->db->query("SELECT COUNT(*) AS account_no FROM view_account WHERE (CASE WHEN(".$this->session->userdata('user_type')."=2) THEN (`account_owner` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`= ".$this->session->userdata('affid')." ) AND `account_owned_by`= 1) OR `account_owner`= ".$this->session->userdata('profile_id')." AND `account_owned_by`= 2 WHEN(".$this->session->userdata('user_type')."=1) THEN (`account_owner`= ".$this->session->userdata('profile_id')." AND `account_owned_by`= 1) ELSE 1 END) ORDER BY `contact_id` DESC")->row()->account_no;
					$data['opp']=$this->db->query("SELECT COUNT(*) AS OPP_COUNT FROM m35_opportunity WHERE m35_opportunity.opportunity_stage !=5 AND `opportunity_owner` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid').") ")->row()->OPP_COUNT;
			$data['anounc']=$this->db->get('tr19_anoncement');
		    $data['task_info']=$this->db->query("SELECT * FROM m38_task WHERE m38_task.task_assignto_id IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid').") ORDER BY m38_task.task_reminder");
			
		    $data['lead']=$this->db->query("SELECT * FROM vw_lead WHERE Lead_Status1!=8 AND (CASE WHEN(".$this->session->userdata('user_type')."=2) THEN (lead_assign_id IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid')." ) AND Lead_Created_By=1) OR (lead_assign_id=".$this->session->userdata('profile_id')." AND Lead_Created_By=2) WHEN(".$this->session->userdata('user_type')."=1) THEN lead_assign_id=".$this->session->userdata('profile_id')." AND Lead_Created_By=1 ELSE 1 END)
            ORDER BY Lead_Created_On  DESC");
			$data['account']=$this->db->query("SELECT * FROM view_account WHERE (CASE WHEN(".$this->session->userdata('user_type')."=2) THEN (`account_owner` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`= ".$this->session->userdata('affid')." ) AND `account_owned_by`= 1) OR `account_owner`= ".$this->session->userdata('profile_id')." AND `account_owned_by`= 2 WHEN(".$this->session->userdata('user_type')."=1) THEN (`account_owner`= ".$this->session->userdata('profile_id')." AND `account_owned_by`= 1) ELSE 1 END) ORDER BY `contact_id` DESC");
			$data['contact']=$this->db->query("SELECT * FROM view_contact WHERE (CASE WHEN(".$this->session->userdata('user_type')."=2) THEN (`account_owner` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`= ".$this->session->userdata('affid')." ) AND `account_owned_by`= 1) OR `account_owner`= ".$this->session->userdata('profile_id')." AND `account_owned_by`= 2 WHEN(".$this->session->userdata('user_type')."=1) THEN (`account_owner`= ".$this->session->userdata('profile_id')." AND `account_owned_by`= 1) ELSE 1 END) ORDER BY `contact_id` DESC");
            if($this->session->userdata('affid')==1)     
            {       
            			$this->db->where('event_status',1);
            			$data['event']=$this->db->get('view_event');
            }
            else
            {
            			$this->db->where('event_status',-1);            			
            			$data['event']=$this->db->get('view_event');
            }
            if($this->session->userdata('affid')==1)
            {
            			$this->db->where('task_status',0);
            			$data['task']=$this->db->get('m38_task');
            }
            else
            {
            			$this->db->where('task_status',-2);
            			$data['task']=$this->db->get('m38_task');
            }
            if($this->session->userdata('affid')==1)
            			$data['task_mail']=$this->db->query("SELECT COUNT(*) AS t_mail FROM m38_task WHERE task_type=2");
            else
            			$data['task_mail']=$this->db->query("SELECT 0 AS t_mail ");
            if($this->session->userdata('affid')==1)		
            			$data['task_call']=$this->db->query("SELECT COUNT(*) AS t_call FROM m38_task WHERE task_type=1");
            else
			$data['task_call']= $this->db->query("SELECT 0 AS t_call ");
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
            if($this->session->userdata('user_type')!=2)
            {
			$this->load->view('Report/acc_dashboard',$data);
            }
            else
            { 
             $this->load->view('Report/acc_dashboard',$data);
            }
			$this->load->view('footer');
			
		}


        public function acc_dashboard()
		{   
			$data['new']=$this->db->query("SELECT COUNT(DISTINCT(tkt_ref_no)) AS new_tic FROM m46_submit_ticket WHERE tkt_status=1 AND DATE_FORMAT(tkt_sub_date,'%y-%m-%d')=CURDATE()");
            $data['acount_ticket']=$this->db->query("SELECT COUNT(DISTINCT(tkt_ref_no)) AS new_tic FROM m46_submit_ticket WHERE tkt_status=1 AND DATE_FORMAT(tkt_sub_date,'%y-%m-%d')=CURDATE() AND emp_id IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid').") ")->row();
			$data['complete']=$this->db->query("SELECT COUNT(DISTINCT(tkt_ref_no)) as complete FROM m46_submit_ticket WHERE tkt_status='0' AND emp_id IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid').")  ");
			$data['pending']=$this->db->query("SELECT COUNT(DISTINCT(tkt_ref_no)) AS pending FROM m46_submit_ticket WHERE tkt_status=1 AND affiliate_id =".$this->session->userdata('affid')." AND DATE_FORMAT(tkt_sub_date,'%y-%m-%d')<CURDATE() AND emp_id IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid').") ");
			$data['curr']=$this->curr;
			$data['lead1']=$this->db->query("SELECT COUNT(*) AS lead_no FROM vw_lead WHERE Lead_Status1!=8 AND (CASE WHEN(".$this->session->userdata('user_type')."=2) THEN (lead_assign_id IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid')." ) AND Lead_Created_By=1) OR (lead_assign_id=".$this->session->userdata('profile_id')." AND Lead_Created_By=2) WHEN(".$this->session->userdata('user_type')."=1) THEN lead_assign_id=".$this->session->userdata('profile_id')." AND Lead_Created_By=1 ELSE 1 END)
ORDER BY Lead_Created_On  DESC")->row()->lead_no;
$data['account1']=$this->db->query("SELECT COUNT(*) AS acc_count FROM m33_account WHERE m33_account.account_status=1 AND m33_account.account_owner IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid').") ")->row()->acc_count;
			$data['opp']=$this->db->query("SELECT COUNT(*) AS OPP_COUNT FROM m35_opportunity WHERE m35_opportunity.opportunity_stage !=5 AND `opportunity_owner` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid').") ")->row()->OPP_COUNT;
			$data['anounc']=$this->db->get('tr19_anoncement');
			$data['task_info']=$this->db->query("SELECT * FROM m38_task WHERE m38_task.task_assignto_id IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid').") ORDER BY m38_task.task_reminder");
			$data['lead']=$this->db->query("SELECT * FROM vw_lead WHERE Lead_Status1!=8 AND (CASE WHEN(".$this->session->userdata('user_type')."=2) THEN (lead_assign_id IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid')." ) AND Lead_Created_By=1) OR (lead_assign_id=".$this->session->userdata('profile_id')." AND Lead_Created_By=2) WHEN(".$this->session->userdata('user_type')."=1) THEN lead_assign_id=".$this->session->userdata('profile_id')." AND Lead_Created_By=1 ELSE 1 END)
ORDER BY Lead_Created_On  DESC");
$data['account']=$this->db->query("SELECT * FROM m33_account WHERE m33_account.account_owner IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid').") ");
			$data['contact']=$this->db->query("SELECT * FROM m34_contact WHERE m34_contact.account_id IN (SELECT  account_id FROM m33_account WHERE m33_account.account_owner IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid').")) ");
			
			$data['event']=$this->db->query("SELECT * FROM m41_event WHERE m41_event.event_status=1 AND m41_event.event_assignto_id =". $this->session->userdata('profile_id') ."");
			$data['task']=$this->db->query("SELECT * FROM m38_task WHERE m38_task.task_status=0 AND m38_task.task_assignto_id IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid').") ");
			$data['task_mail']=$this->db->query("SELECT COUNT(*) AS t_mail FROM m38_task WHERE task_type=2 AND m38_task.task_assignto_id IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid').") ");
			$data['task_call']=$this->db->query("SELECT COUNT(*) AS t_call FROM m38_task WHERE task_type=1 AND m38_task.task_assignto_id IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid').") ");
	
         	$this->load->view('header');
			$this->load->view('menu1',$this->data1);
            $this->load->view('Report/acc_dashboard',$data);
			$this->load->view('footer');
			
		}

		public function pyramid(){
			if($this->session->userdata('user_type')==1)
			{
				$this->db->where('opportunity_owner',$this->session->userdata('profile_id'));
				$data=$this->db->get('m35_opportunity');
			}
			if($this->session->userdata('user_type')==2)
			{
				$data=$this->db->query("SELECT * FROM (`m35_opportunity`) WHERE (`opportunity_owner`=".$this->session->userdata('profile_id')." AND `m35_opportunity`.`opportunity_owned_by`=".$this->session->userdata('user_type')." ) OR (`opportunity_owner` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid')." and `or_m_designation`=4) AND `opportunity_owned_by`=1)");
			}
			$lead=$this->db->query("SELECT COUNT(*) AS lead_no FROM vw_lead WHERE Lead_Status1 NOT IN(7,8) AND (CASE WHEN(".$this->session->userdata('user_type')."=2) THEN (lead_assign_id IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid')." ) AND Lead_Created_By=1) OR (lead_assign_id=".$this->session->userdata('profile_id')." AND Lead_Created_By=2) WHEN(".$this->session->userdata('user_type')."=1) THEN lead_assign_id=".$this->session->userdata('profile_id')." AND Lead_Created_By=1 ELSE 1 END)
			ORDER BY Lead_Created_On  DESC")->row()->lead_no;

			$opportunity =$qualification = $analysis = $proposition = $won = 0;

			foreach($data->result() as $row){
				if($row->opportunity_stage==1)
				{
					$opportunity = $opportunity + 1;
				}
				if($row->opportunity_stage==2)
				{
					$qualification = $qualification + 1;
				}
				if($row->opportunity_stage==3)
				{
					$analysis = $analysis + 1;
				}
				if($row->opportunity_stage==4)
				{
					$proposition = $proposition + 1;
				}
				if($row->opportunity_stage==5)
				{
					$won = $won + 1;
				}
			}
			$data= [];
			if($lead != 0){
				$data[] = [
					'label'=> 'Leads',
					'name'=> 'Leads',
					'y' => $lead
				];
			}
			if($opportunity != 0){
				$data[] = [
					'label'=> 'Opportunity',
					'name'=> 'Opportunity',
					'y' => $opportunity
				];
			}
			if($qualification != 0){
				$data[] = [
					'label'=> 'Qualification',
					'name'=> 'Qualification',
					'y' => $qualification
				];
			}
			if($analysis != 0){
				$data[] = [
					'label'=> 'Analysis',
					'name'=> 'Analysis',
					'y' => $analysis
				];
			}
			if($proposition != 0){
				$data[] = [
					'label'=> 'Proposition',
					'name'=> 'Proposition',
					'y' => $proposition
				];
			}
			if($won != 0){
				$data[] = [
					'label'=> 'Won',
					'name'=> 'Won',
					'y' => $won
				];
			}
			echo json_encode($data,JSON_NUMERIC_CHECK);
		}

		public function loc(){
			//print_r($this->session);die;
			$y = date('Y');
			$dataPoints1 = $dataPoints2 =[];
			if($this->session->userdata('user_type')==1)
			{
				$data=$this->db->query("SELECT COUNT(opportunity_id) as opp_count,opportunity_create_date
				FROM (`m35_opportunity`) 
				WHERE `opportunity_owner`=".$this->session->userdata('profile_id')." AND `m35_opportunity`.`opportunity_owned_by`=".$this->session->userdata('user_type')." 
				GROUP BY MONTH(opportunity_create_date), YEAR(opportunity_create_date)=".$y." ORDER BY MONTH(opportunity_create_date)  ASC");

				$conver=$this->db->query("SELECT COUNT(opportunity_id) as opp_count,opportunity_create_date
				FROM (`m35_opportunity`) 
				WHERE `opportunity_stage` = 5 AND (`opportunity_owner`=".$this->session->userdata('profile_id')." AND `m35_opportunity`.`opportunity_owned_by`=".$this->session->userdata('user_type')." ) 
				GROUP BY MONTH(opportunity_create_date), YEAR(opportunity_create_date)=".$y." ORDER BY MONTH(opportunity_create_date)  ASC");
			}
			if($this->session->userdata('user_type')==2)
			{
				$data=$this->db->query("SELECT COUNT(opportunity_id) as opp_count,opportunity_create_date
				FROM (`m35_opportunity`) 
				WHERE (`opportunity_owner`=".$this->session->userdata('profile_id')." AND `m35_opportunity`.`opportunity_owned_by`=".$this->session->userdata('user_type')." ) 
				OR (`opportunity_owner` IN (
					SELECT `or_m_reg_id` 
					FROM `m06_user_detail` 
					WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid')." and `or_m_designation`=4) AND `opportunity_owned_by`=1) 
				GROUP BY MONTH(opportunity_create_date), YEAR(opportunity_create_date)=".$y." ORDER BY MONTH(opportunity_create_date)  ASC");

				$conver=$this->db->query("SELECT COUNT(opportunity_id) as opp_count,opportunity_create_date
				FROM (`m35_opportunity`) 
				WHERE `opportunity_stage` = 5
				GROUP BY MONTH(opportunity_create_date), YEAR(opportunity_create_date)=".$y." ORDER BY MONTH(opportunity_create_date)  ASC");
			}
			$rec=$this->crm_model->show_lead($this->session->userdata('user_type'));
			$lead=$rec['rec']->num_rows();
			
			$row=$data->result();
			$account=$conver->result();
			$i=0;
			for( $j= 0;$j<12;$j++){
				$dateObj   = DateTime::createFromFormat('!m', $j+1);
				$monthName = $dateObj->format('M');
				if(isset($row[$i]->opportunity_create_date)){
					if($j+1 == date('m',strtotime($row[$i]->opportunity_create_date))){
						$dataPoints1[] = [
							'label' => $monthName,
							'y' => round(($row[$i]->opp_count / $lead) * 100,2)
						];
						if(isset($account[$i]->opp_count)){
							$dataPoints2[] = [
								'label' => $monthName,
								'y' => round(($account[$i]->opp_count / $row[$i]->opp_count) * 100,2)
							];
						}
						else{
							$dataPoints2[] = [
								'label' => $monthName,
								'y' => 0
							];
						}
						
						$i=$i+1;
					}
					else{
						$dataPoints1[] = [
							'label' => $monthName,
							'y' => 0
						];
						$dataPoints2[] = [
							'label' => $monthName,
							'y' => 0
						];
					}
				}
				else{
					$dataPoints1[] = [
						'label' => $monthName,
						'y' => 0
					];
					$dataPoints2[] = [
						'label' => $monthName,
						'y' => 0
					];
				}
			}
		
			$result = [
				'dataPoints1' => $dataPoints1,
				'dataPoints2' => $dataPoints2,
			];
			echo json_encode($result,JSON_NUMERIC_CHECK);
		}

		public function winLost(){
			if($this->session->userdata('user_type')==1)
			{
				$this->db->where('opportunity_owner',$this->session->userdata('profile_id'));
				$data=$this->db->get('m35_opportunity');
			}
			if($this->session->userdata('user_type')==2)
			{
				$data=$this->db->query("SELECT * FROM (`m35_opportunity`) WHERE (`opportunity_owner`=".$this->session->userdata('profile_id')." AND `m35_opportunity`.`opportunity_owned_by`=".$this->session->userdata('user_type')." ) OR (`opportunity_owner` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid')." and `or_m_designation`=4) AND `opportunity_owned_by`=1)");
			}
			
			$lost = $won = 0;

			foreach($data->result() as $row){
				if($row->opportunity_stage==5)
				{
					$won = $won + 1;
				}
				if($row->opportunity_stage==6)
				{
					$lost = $lost + 1;
				}
			}
			$data = [
				'lost' => $lost,
				'won' => $won,
			];
			echo json_encode($data);
		}

		public function typeOfLeads(){
			if($this->session->userdata('user_type')==1){
				$data=$this->db->query("SELECT COUNT(`m32_lead`.`lead_id`) as y,m29_lead_status.status_name as 'name',m29_lead_status.status_name as 'label' FROM (`m32_lead`)
			JOIN m29_lead_status ON m32_lead.lead_status=m29_lead_status.status_id
			WHERE `m32_lead`.`lead_status` NOT IN(7,8) AND `m32_lead`.`lead_owner`=".$this->session->userdata('profile_id')." GROUP BY lead_status");
			}
			if($this->session->userdata('user_type')==2){
				$data=$this->db->query("SELECT COUNT(`m32_lead`.`lead_id`) as y,m29_lead_status.status_name as label,m29_lead_status.status_name as 'name' FROM (`m32_lead`)
			JOIN m29_lead_status ON m32_lead.lead_status=m29_lead_status.status_id
			WHERE `m32_lead`.`lead_status` NOT IN(7,8) GROUP BY `m32_lead`.`lead_status`");
			}
			echo json_encode($data->result());
		}

		public function callToLead(){
			$y = date('Y');
			$dataPoints1 = $dataPoints2 =[];
			if($this->session->userdata('user_type')==1)
			{
				$data=$this->db->query("SELECT COUNT(opportunity_id) as opp_count,opportunity_create_date
				FROM (`m35_opportunity`) 
				WHERE (`opportunity_owner`=".$this->session->userdata('profile_id')." AND `m35_opportunity`.`opportunity_owned_by`=".$this->session->userdata('user_type')." )
				GROUP BY MONTH(opportunity_create_date), YEAR(opportunity_create_date)=".$y." ORDER BY MONTH(opportunity_create_date)  ASC");

				$conver=$this->db->query("SELECT COUNT(`m32_lead`.`lead_id`) as opp_count,m32_lead.lead_regdate as opportunity_create_date FROM (`m32_lead`)
				JOIN m29_lead_status ON m32_lead.lead_status=m29_lead_status.status_id
				WHERE `lead_status` NOT IN(7,8) AND `m32_lead`.`lead_owner`=".$this->session->userdata('profile_id')." GROUP BY MONTH(lead_regdate), YEAR(lead_regdate)=".$y." ORDER BY MONTH(opportunity_create_date)  ASC");
			}
			if($this->session->userdata('user_type')==2)
			{
				$data=$this->db->query("SELECT COUNT(opportunity_id) as opp_count,opportunity_create_date
				FROM (`m35_opportunity`) 
				WHERE (`opportunity_owner`=".$this->session->userdata('profile_id')." AND `m35_opportunity`.`opportunity_owned_by`=".$this->session->userdata('user_type')." ) 
				OR (`opportunity_owner` IN (
					SELECT `or_m_reg_id` 
					FROM `m06_user_detail` 
					WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid')." and `or_m_designation`=4) AND `opportunity_owned_by`=1) 
				GROUP BY MONTH(opportunity_create_date), YEAR(opportunity_create_date)=".$y." ORDER BY MONTH(opportunity_create_date)  ASC");

				$conver=$this->db->query("SELECT COUNT(`m32_lead`.`lead_id`) as opp_count,m32_lead.lead_regdate as opportunity_create_date FROM (`m32_lead`)
				JOIN m29_lead_status ON m32_lead.lead_status=m29_lead_status.status_id
				WHERE `lead_status` NOT IN(7,8) GROUP BY MONTH(lead_regdate), YEAR(lead_regdate)=".$y." ORDER BY MONTH(opportunity_create_date)  ASC");
			}
			
			
			$row=$data->result();
			$i=0;
			for( $j= 1;$j<=12;$j++){
				$dateObj   = DateTime::createFromFormat('!m', $j);
				$monthName = $dateObj->format('M');
				if(isset($row[$i]->opportunity_create_date)){
					if($j == date('m',strtotime($row[$i]->opportunity_create_date))){
						$dataPoints1[] = [
							'label' => $monthName,
							'y' => $row[$i]->opp_count
						];
						$i = $i+1;
					}
					else{
						$dataPoints1[] = [
							'label' => $monthName,
							'y' => 0
						];
					}
				}
				else{
					$dataPoints1[] = [
						'label' => $monthName,
						'y' => 0
					];
				}
			}
			
			$row=$conver->result();
			$i=0;
			for( $j= 1;$j<=12;$j++){
				$dateObj   = DateTime::createFromFormat('!m', $j);
				$monthName = $dateObj->format('M');
				if(isset($row[$i]->opportunity_create_date)){
					if($j == date('m',strtotime($row[$i]->opportunity_create_date))){
						$dataPoints2[] = [
							'label' => $monthName,
							'y' => $row[$i]->opp_count
						];
						$i= $i+1;
					}
					else{
						$dataPoints2[] = [
							'label' => $monthName,
							'y' => 0
						];
					}
				}
				else{
					$dataPoints2[] = [
						'label' => $monthName,
						'y' => 0
					];
				}
			}
			$result = [
				'dataPoints1' => $dataPoints1,
				'dataPoints2' => $dataPoints2,
			];
			echo json_encode($result,JSON_NUMERIC_CHECK);
		}
	}
