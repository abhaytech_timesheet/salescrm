<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Corporate extends CI_Controller{
		
		public function __construct()
		{
			parent::__construct();			
			// Your own constructor code
			if($this->session->userdata('user_id')!="")
			{
				$this->load->model('users_records');
				$this->load->model('mastermodel');
				
				$timezone = new DateTimeZone("Asia/Kolkata" );
				$date = new DateTime();
				$date->setTimezone($timezone );	
				$this->dates = $date->format('Y-m-d');
                $this->dates1 = $date->format('Y-m-d H:i:s');
				$aff =0;
				if($this->session->userdata('affid') ==0)
				{
					$aff = 1;
				}
				$this->db->where('affiliate_id',$aff);
				$this->data['aff_name'] = $this->db->get("m22_affiliate");
				$this->db->where('m_menu_status',1);
				$this->data['menu']=$this->db->get('m58_apps');
			}
			else
			{
				header("location:".base_url()."index.php/auth/logout");
			}
		}
		
		
		public function view_plot_booking()
		{
			$data['bank']=$this->db->get('m04_bank');
		    $this->db->where('m_site_point_status',1);
			$data['site']=$this->db->get('m68_site_master');
			$this->load->view('header');
			$this->load->view('menu1',$this->data);
			$this->load->view('corporate1/view_plot_booking',$data);
			$this->load->view('footer');
			$this->load->view('corporate1/plot_script');
		}
		
		//Get member name 
		public function get_member_name()
		{
			$mname="";
			$id="";
			$id=$this->input->post('txtintuserid');
			$query=$this->db->get_where('m06_user_detail',array('or_m_user_id'=>trim($id) ));
			$row = $query->row();
			if($query->num_rows()==1)
			{		// validate??
				echo trim($row->or_m_name);	
			}
			else
			{
				echo "-1";		// RETURN ARRAY WITH ERROR
			}
		}
		
		public function getplot_size()
		{
			$name="";
			$site_id=$this->input->post('site_id');
			$query = $this->db->query("SELECT
			`m67_plot_size`.`m_plot_size_id`    AS `m_plot_size_id`,
			`m67_plot_size`.`m_plot_width_ft`    AS `m_plot_width_ft`,
			`m67_plot_size`.`m_plot_width_inch`  AS `m_plot_width_inch`,
			`m67_plot_size`.`m_plot_height_ft`   AS `m_plot_height_ft`,
			`m67_plot_size`.`m_plot_height_inch` AS `m_plot_height_inch`,
			`m67_plot_size`.`m_plot_Area`        AS `m_plot_Area`
			FROM
			`admin_appworks`.`m69_site_plot`
			LEFT JOIN `admin_appworks`.`m67_plot_size` 
			ON (`m69_site_plot`.`m_sp_plot_size` = `m67_plot_size`.`m_plot_size_id`) where m_sp_site='$site_id'");
			
			$json=json_encode($query->result());
			echo $json;
		}
		
		
		public function getplot_no()
		{
			$plot_size = $this->input->post('plot_size');
			$site_id   = $this->input->post('site_id');
			$query = $this->db->query("SELECT 
			`m69_site_plot`.`m_sp_id`       AS  `ID`,
			`m69_site_plot`.`m_sp_plot_no`  AS  `PLOT_NO`
			FROM
			`admin_appworks`.`m69_site_plot` WHERE `m69_site_plot`.`m_sp_site`='$site_id' AND `m69_site_plot`.`m_sp_plot_size`='$plot_size' and `m69_site_plot`.`m_sp_status`=1");
			
			$json=json_encode($query->result());
			echo $json;
		}
		
		public function getplot_amt()
		{
			$ddplot_no=$this->input->post('ddplot_no');
			$query = $this->db->query("SELECT
			`m69_site_plot`.`m_sp_plot_amount`  AS  `PLOT_AMT`
			FROM
			`admin_appworks`.`m69_site_plot` WHERE `m69_site_plot`.`m_sp_id`='$ddplot_no'");
			
			$json=json_encode($query->result());
			echo $json;
		}
		
		public function getplan()
		{
			$site_id   = $this->input->post('site_id');
			//$site_id   = $this->uri->segment(3);
			$query = $this->db->query("SELECT `m_pm_id`,`m_pm_planname` FROM `m70_plan_master` WHERE `m_pm_site_id` ='$site_id'");
			$json=json_encode($query->result());
			echo $json;
		}	
		
		public function getbook_amnt()
		{
			$site_id   = $this->input->post('site_id');
			$plan_id   = $this->input->post('plan_id');
			//$site_id   = $this->uri->segment(3);
			$query = $this->db->query("SELECT * FROM `m70_plan_master` WHERE `m_pm_site_id` ='$site_id' AND `m_pm_id` = '$plan_id'");
			$json=json_encode($query->result());
			echo $json;
		}
		
		public function insert_plot_book()
		{
			if($this->session->userdata('user_id')!="")
			{
				$this->load->model('corporate_model');
				$this->corporate_model->insert_plot_book();			
				header("Location:".base_url()."corporate/view_plot_report");
			}
			else
			{
				header("Location:".base_url()."index.php/corporate/logout");
			}
		}
		
		public function view_plot_report()
		{
		    $this->db->where('m_site_point_status',1);
			$data['site']=$this->db->get('m68_site_master');
			$data['plot_report']=$this->db->query("CALL sp_plot_book_report(1,'1','')");
			$this->load->view('header');
			$this->load->view('menu1',$this->data);
			$this->load->view('corporate1/view_plot_report',$data);
			$this->load->view('footer');
		}
		
		
		public function edit_plotbooking()
		{
		    $this->db->where('m_site_point_status',1);
			$data['site']=$this->db->get('m68_site_master');
			
			$this->db->where('m_plot_status',1);
			$data['plotsize']=$this->db->get('m67_plot_size');
			
			$this->db->where('m_sp_status',1);
			$data['siteplot']=$this->db->get('m69_site_plot');
			
			$data['bank']=$this->db->get('m04_bank');
			
			$id = $this->uri->segment(3);
			$sparr = array(
			'proc'=>2,
			'querey'=>'',
			'id'=>$id
			);
			//var_dump($sparr);
			$query = "CALL sp_plot_book_report(?" . str_repeat(",?", count($sparr)-1) . ") ";
			$data['resul'] = $this->db->query($query,$sparr);
			$this->db->free_db_resource();
			foreach($data['resul']->result() as $rowins)
			{
				break;
			}
			if($data['resul']->num_rows() >0)
			$this->db->where('payment_id',$rowins->ins_id);
			$data['ins_data'] = $this->db->get('m72_installment_info');
			$this->load->view('header');
			$this->load->view('menu1',$this->data);
			$this->load->view('corporate1/edit_plot_booking',$data);
			$this->load->view('footer');
			$this->load->view('corporate1/plot_script');
		}
		
		public function loadplot()
		{
			
			$site = $this->uri->segment(3);
		    $plotsite = $this->uri->segment(4);
			$query = "SELECT
			`m69_site_plot`.`m_sp_id`				 AS	Plot_id,
			`m69_site_plot`.`m_sp_plot_no`		     AS Plot_no,
			`m67_plot_size`.`m_plot_size_id`    	 AS `m_plot_size_id`,
			`m67_plot_size`.`m_plot_width_ft`   	 AS `m_plot_width_ft`,
			`m67_plot_size`.`m_plot_width_inch` 	 AS `m_plot_width_inch`,
			`m67_plot_size`.`m_plot_height_ft`  	 AS `m_plot_height_ft`,
			`m67_plot_size`.`m_plot_height_inch`	 AS `m_plot_height_inch`,
			`m67_plot_size`.`m_plot_Area`       	 AS `m_plot_Area`
			FROM
			`admin_appworks`.`m69_site_plot`
			LEFT JOIN `admin_appworks`.`m67_plot_size` 
			ON (`m69_site_plot`.`m_sp_plot_size` = `m67_plot_size`.`m_plot_size_id`) where `m_sp_plot_size` = '$plotsite' AND  m_sp_site='$site' ";
			$data['plotsize'] = $this->db->query($query);
			
			$this->load->view('corporate1/hid_plotsize',$data);
			
		}
		
		
		public function loadpaymentplan()
		{
			$paymentplanid = $this->uri->segment(3);
			$this->db->where('m_pm_id',$paymentplanid);	
			$data['paymentplan'] = $this->db->get('m70_plan_master');
			$this->load->view('corporate1/hid_payment',$data);
		}
		
		public function update_plot_book()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$this->load->model('corporate_model');
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			
			$pay_status = 0;
			$booking_amnt = $this->input->post('txtbooking_amt');
			$allotment_amnt = $this->input->post('txtallot_amt');
			$payment_amnt = $this->input->post('txtpay_amt');
			
			$data=array(
			'proc'=>'2',
			'plot_book_id'=>$this->input->post('txtplan_book_id'),				
			'book_ref_by'=>'',
			'or_m_reg_id'=>$this->corporate_model->get_uid($this->input->post('txtmember_id')),
			'site_id'=>$this->input->post('ddsite'),
			'plot_size'=>$this->input->post('ddplot_size'),
			'plot_no'=>$this->input->post('ddplot_no'),
			'payment_plan'=>$this->input->post('txtpayment_plan'),
			'is_late'=>$this->input->post('ddlate'),
			'late_days'=>$this->input->post('txtlate_day'),
			'late_rate'=>$this->input->post('txtlate_rate'),
			'bookdates'=>'',				
			'paymentamnt'=>$this->input->post('txtpay_amt'),
			'paymentmode'=>$this->input->post('txtpayment_mode'),
			'paymentother'=>0,				
			'paymentcheckno'=>$this->input->post('txtcheck_no'),
			'paymentbank'=>$this->input->post('ddbank'),
			'paymentcheckdate'=>$this->input->post('txtcheck_date'),
			'paymentcheckstatus'=>0,
			'paystatus'=>'',
			'booking_status'=>1	
			);
			$query = " CALL sp_plot_booking(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query,$data);
			header("Location:".base_url()."corporate/view_plot_report");
		}
	}
?>		