<?php

class Cms extends CI_Controller {


		public function index()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$this->load->view('CMS/header');
		$this->load->view('CMS/menu');
		$this->load->view('CMS/index');
		$this->load->view('CMS/footer');
	}
	
	public function home_tab_id()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$aid=0;
		$data['id']=$this->uri->segment(3);
		$this->db->where('affiliate_id',$aid);
		$this->db->where('widget_index',$this->uri->segment(3));
		$data['content']=$this->db->get('m26_cms_widgets');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('CMS/home_tab',$data);
		$this->load->view('admin/footer');
	}
	public function update_home_tab()
   {
		$this->load->library('session');
		$this->load->database();
		$this->load->helper('url');
		$aid=0;
		$data=array
		(
			'widget_title'=>$this->input->post('title'),
			'widget_content'=>$this->input->post('content')
		);
		$flg=$this->input->post('flg');
		$this->db->where('widget_index',$flg);
		$this->db->where('affiliate_id',$aid);
		$this->db->update('m26_cms_widgets',$data);
		echo "Widget Update Successfully";
    }
	
	public function left_tab_id()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$aid=0;
		$data['id']=$this->uri->segment(3);
		$this->db->where('affiliate_id',$aid);
		$this->db->where('widget_index',$this->uri->segment(3));
		 $this->db->limit(2);
		$data['content']=$this->db->get('m26_cms_widgets');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('CMS/left_tab',$data);
		$this->load->view('admin/footer');
	}
	public function update_left_tab()
   {
		$this->load->library('session');
		$this->load->database();
		$this->load->helper('url');
		$aid=0;
		$data=array
		(
			'widget_title'=>$this->input->post('title'),
			'widget_content'=>$this->input->post('content')
		);
		$flg=$this->input->post('flg');
		$this->db->where('widget_index',$flg);
		$this->db->where('affiliate_id',$aid);
		$this->db->update('m26_cms_widgets',$data);
		echo "Widget Update Successfully";
    }
	
	public function right_tab_id()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$aid=0;
		$data['id']=$this->uri->segment(3);
		$this->db->where('affiliate_id',$aid);
		$this->db->where('widget_index',$this->uri->segment(3));
		 $this->db->limit(3);
		$data['content']=$this->db->get('m26_cms_widgets');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('CMS/right_tab',$data);
		$this->load->view('admin/footer');
	}
	public function update_right_tab()
   {
		$this->load->library('session');
		$this->load->database();
		$this->load->helper('url');
		$aid=0;
		$data=array
		(
			'widget_title'=>$this->input->post('title'),
			'widget_content'=>$this->input->post('content')
		);
		$flg=$this->input->post('flg');
		$this->db->where('widget_index',$flg);
		$this->db->where('affiliate_id',$aid);
		$this->db->update('m26_cms_widgets',$data);
		echo "Widget Update Successfully";
    }
	
	public function center_tab_id()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$aid=0;
		$data['id']=$this->uri->segment(3);
		$this->db->where('affiliate_id',$aid);
		$this->db->where('widget_index',$this->uri->segment(3));
		 $this->db->limit(4);
		$data['content']=$this->db->get('m26_cms_widgets');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('CMS/center_tab',$data);
		$this->load->view('admin/footer');
	}
	public function update_center_tab()
   {
		$this->load->library('session');
		$this->load->database();
		$this->load->helper('url');
		$aid=0;
		$data=array
		(
			'widget_title'=>$this->input->post('title'),
			'widget_content'=>$this->input->post('content')
		);
		$flg=$this->input->post('flg');
		$this->db->where('widget_index',$flg);
		$this->db->where('affiliate_id',$aid);
		$this->db->update('m26_cms_widgets',$data);
		echo "Widget Update Successfully";
    }
	
	public function event_tab()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$data['content']=$this->db->get('manage_events');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('CMS/event_tab',$data);
		$this->load->view('admin/footer');
	}
	public function event_insert()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$aid=0;
		$data=array
		(
			'event_name'=>$this->input->post('title'),
			'event_date'=>$this->input->post('date'),
			'status'=>$this->input->post('status'),
			'affiliate_id'=>$aid,
			'event_description'=>$this->input->post('content')			 
		);
		$this->db->insert('manage_events', $data); 
		echo "Event Inserted Successfully";
	}
	
	public function delete_event()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$id=$this->input->post('id');
		$this->db->where('event_id',$id);
		$this->db->delete('manage_events');
		echo "Event Deleted Successfully";
		//header("location:".base_url()."index.php/cms/image_upload");
	}
	public function edit_event()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$id=$this->uri->segment(3);
		$this->db->where('event_id',$id);
		$data['id']=$id;
		$data['content']= $this->db->get('manage_events');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('CMS/event_edit_tab',$data);
		$this->load->view('admin/footer');
	}
	
	public function update_event()
   {
		$this->load->library('session');
		$this->load->database();
		$this->load->helper('url');
		$aid=0;
		$id=$this->input->post('id');
		$data=array
		(
			'event_name'=>$this->input->post('title'),
			'event_date'=>$this->input->post('date'),
			'status'=>$this->input->post('status'),
			'affiliate_id'=>$aid,
			'event_description'=>$this->input->post('content')	
		);
		$this->db->where('event_id',$id);
		$this->db->where('affiliate_id',$aid);
		$this->db->update('manage_events',$data);
		echo "Event Updated Successfully";
    }
	public function event_image()
   {
		$this->load->library('session');
		$this->load->database();
		$this->load->helper('url');
		$aid=0;
		$id=$this->uri->segment(3);
		$data['aid']=$aid;
		$data['id']=$id;
		$this->db->where('affiliate_id',$aid);
		$this->db->where('event_id',$id);
		$data['content']=$this->db->get('m36_cms_event_image');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('CMS/event_image',$data);
		$this->load->view('admin/footer');
    }
	public function delete_event_images()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		
		$id=$this->input->post('id');
		$this->db->where('image_id',$id);
		$this->db->delete('m36_cms_event_image');
		@unlink('application/libraries/event_Images/'.$this->input->post('imge'));
		echo "Image Deleted Successfully";
		//header("location:".base_url()."index.php/cms/image_upload");
	}
	
	public function image_upload()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$this->db->where('status',1);
		$data['content']=$this->db->get('m34_cms_manage_image');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('CMS/image_upload',$data);
		$this->load->view('admin/footer');
	}
	public function delete_banner_images()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		
		$id=$this->input->post('id');
		$this->db->where('image_id',$id);
		$this->db->delete('m34_cms_manage_image');
		@unlink('application/libraries/banner_Images/'.$this->input->post('imge'));
		echo "Image Deleted Successfully";
		//header("location:".base_url()."index.php/cms/image_upload");
	}
	
	public function delete_affiliate_images()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		
		$id=$this->input->post('id');
		$this->db->where('image_id',$id);
		$this->db->delete('m35_cms_affiliate_image');
		@unlink('application/libraries/affiliate_Images/'.$this->input->post('imge'));
		echo "Image Deleted Successfully";
		//header("location:".base_url()."index.php/cms/image_upload");
	}
	
	public function affiliate_picture()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$this->db->where('status',1);
		$data['content']=$this->db->get('m35_cms_affiliate_image');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('CMS/affiliate_picture',$data);
		$this->load->view('admin/footer');
	}
	
	public function email_template()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$aid=0;
		$this->db->where('affiliate_id',$aid);
		$this->db->where('m_template_category','1');
		$data['content']=$this->db->get('m37_email_temp');
		$this->db->where('m_type','1');
		$data['cate']=$this->db->get('m38_email_template_category');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('CMS/email_template',$data);
		$this->load->view('admin/footer');
	}
	
	
	public function insert_email_temp()
   {
		$this->load->library('session');
		$this->load->database();
		$this->load->helper('url');
		$aid=0;
		$data=array
		(
			'affiliate_id'=>0,
			'm_email_temp_title'=>$this->input->post('title'),
			'm_email_temp_description'=>$this->input->post('content'),
			'm_category'=>$this->input->post('category'),
			'm_template_category'=>$this->input->post('opttyp')
		);
		$this->db->insert('m37_email_temp',$data);
		echo "Email Inserted Successfully";
    }
	
	
	public function edit_email()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$aid=0;
		$id=$this->uri->segment(3);
		$data['id']=$id;
		$this->db->where('m_email_temp_id',$id);
		$this->db->where('affiliate_id',$aid);
		$data['content']= $this->db->get('m37_email_temp');
		$data['cate']=$this->db->get('m38_email_template_category');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('CMS/email_edit_template',$data);
		$this->load->view('admin/footer');
	}
	
	public function update_email()
   {
		$this->load->library('session');
		$this->load->database();
		$this->load->helper('url');
		$aid=0;
		$id=$this->input->post('id');
		$data=array
		(
			'm_email_temp_title'=>$this->input->post('title'),
			'm_email_temp_description'=>$this->input->post('content'),
			'category'=>$this->input->post('category')
		);
		$this->db->where('m_email_temp_id',$id);
		$this->db->where('affiliate_id',$aid);
		$this->db->update('m37_email_temp',$data);
		echo "Email Updated Successfully";
    }
	public function delete_email()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$id=$this->input->post('id');
		$this->db->where('m_email_temp_id',$id);
		$this->db->delete('m37_email_temp');
		echo "Email Deleted Successfully";
		//header("location:".base_url()."index.php/cms/image_upload");
	}
	
	public function view_email_template()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$data['content']=$this->db->get('m37_email_temp');
		$this->db->where('m_type','1');
		$data['cate']=$this->db->get('m38_email_template_category');
		$data['info']='';
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('CMS/view_email_template',$data);
		$this->load->view('admin/footer');
	}
	
	
	public function after_view_email_template()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$data['content']=$this->db->get('m37_email_temp');
		$data['cate']=$this->db->get('m38_email_template_category');
		$data['info']='';
		$this->load->view('CMS/view_email_template',$data);
	}
	
	
	public function send_template_mail()
	{
		$config = array();
        $config['mailtype'] = 'html';
        $this->load->library('email', $config);
        $this->email->from($this->input->post('txtfrom'),'Pramotional Template');
        $this->email->to($this->input->post('txtto')); 
        $this->email->subject($this->input->post('txtsubject'));
        $this->email->message($this->input->post('txtdescription'));
	    $this->email->send();
		echo 'true';
	}
	
	public function email_template_display()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$id=$this->uri->segment(3);	
		$this->db->where('m_category',$id);
		$this->db->where('m_template_category','1');
		$data['content']=$this->db->get('m37_email_temp');
		$data['cate']=$this->db->get('m38_email_template_category');
		$this->load->view('CMS/inbox_inbox',$data);
	}
	
	
	public function compose()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$this->load->view('CMS/inbox_compose');
	}
	
	public function template1()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$id=$this->uri->segment(3);
		
		$aid=0;
		$this->db->where('affiliate_id',$aid);
		$this->db->limit(5);
		$data['top_menu']=$this->db->get('m26_cms_widgets');
		$this->db->where('affiliate_id',$aid);
		$this->db->limit(2,5);
		$data['left_menu']=$this->db->get('m26_cms_widgets');
		$this->db->where('affiliate_id',$aid);
		$this->db->limit(3,7);
		$data['right_menu']=$this->db->get('m26_cms_widgets');
		$this->db->where('affiliate_id',$aid);
		$this->db->limit(4,10);
		$data['center_menu']=$this->db->get('m26_cms_widgets');
		if($id==1)
		{
			$this->db->where('widget_index',$id);
			$data['content']=$this->db->get('m26_cms_widgets');
		$this->load->view('template1/index',$data);
		}
			else
			{
				$this->db->where('widget_index',$id);
			$data['content']=$this->db->get('m26_cms_widgets');
			$this->load->view('template1/page',$data);}
	}
	
	public function template2()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		
		$aid=0;
		
		$id=$this->uri->segment(3);
		
		$this->db->where('widget_index',$id);
		$data['id']=$this->db->get('m26_cms_widgets');
		
		$this->db->where('affiliate_id',$aid);
		$this->db->limit(5);
		
		$data['top_menu']=$this->db->get('m26_cms_widgets');
		$this->db->where('affiliate_id',$aid);
		$this->db->limit(2,5);
		
		$data['left_menu']=$this->db->get('m26_cms_widgets');
		$this->db->where('affiliate_id',$aid);
		$this->db->limit(3,7);
		
		$data['right_menu']=$this->db->get('m26_cms_widgets');
		$this->db->where('affiliate_id',$aid);
		$this->db->limit(4,10);
		
		$data['center_menu']=$this->db->get('m26_cms_widgets');
		$this->load->view('template2/index',$data);
	}
	
	public function dashboard()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$this->load->view('Registration/header');
		$this->load->view('Registration/menu');
		$this->load->view('Registration/customer_registration');
	}
	
	public function view_news()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$aid=0;
		$this->db->where('m_affid',$aid);
		$data['content']=$this->db->get('m24_news');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('CMS/view_news',$data);
		$this->load->view('admin/footer');
	}
	
	public function manage_news()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$aid=0;
		$this->db->where('m_affid',$aid);
		$data['content']=$this->db->get('m24_news');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('CMS/manage_news',$data);
		$this->load->view('admin/footer');
	}
	
	public function update_news()
   {
		$this->load->library('session');
		$this->load->database();
		$this->load->helper('url');
		$aid=0;
		$id = $this->input->post('id');
		$data=array
		(
			'm_news_title'=>$this->input->post('title'),
			'm_news_des'=>$this->input->post('content'),
			'm_news_status'=>$this->input->post('status'),
			'm_affid'=>$aid,
			'm_entrydate'=>$this->input->post('news_date')
		);
		$this->db->where('m_affid',$aid);
		$this->db->where('m_news_id',$id);
		$this->db->update('m24_news',$data);
		echo "News Update Successfully";
    }
	
	
	public function get_details_email()
		{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		if($this->session->userdata('user_id')!="")
		{
			$sou=$this->uri->segment(3);
			$cat=$this->uri->segment(4);
			$data['info']="";
			$data['acc']="";
			$data['opp']="";
			if($sou=='1')
			{
				$data['info']=$this->db->query("SELECT * FROM m32_lead WHERE lead_status!=7");
			}
			if($sou=='2' && $cat==1)
			{
				$data['acc']=$this->db->query("SELECT * FROM m34_contact WHERE is_account=1");
			}
			if($sou=='2' && $cat==2)
			{
				$data['opp']=$this->db->query("SELECT * FROM m34_contact WHERE is_account=0");
			}
			$this->load->view('Master/get_campaign',$data);
		}
		else
		{
			header("Location:".base_url()."index.php/crm/logout");
		}
		}
		
		public function get_template()
		{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		if($this->session->userdata('user_id')!="")
		{
			$data['content']=$this->db->get('m37_email_temp');
			$data['cate']=$this->db->get('m38_email_template_category');
			$this->db->where('m_email_temp_id',$this->uri->segment(3));
			$data['info']=$this->db->get('m37_email_temp');
			$this->load->view('admin/header');
			$this->load->view('admin/menu');
			$this->load->view('CMS/view_email_template',$data);
			$this->load->view('admin/footer');
		}
		else
		{
			header("Location:".base_url()."index.php/crm/logout");
		}
		}
		
}
?>
