<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Group extends CI_Controller {
		
		/*-----------------------------Project Constructor-----------------------------*/
		
		public function __construct() 
		{
			parent::__construct();
            $this->_is_logged_in();
			//$this->load->model('project_model');
			$this->data1=$this->crm_model->assign_menu();
		}
		
		/*Session Check Function */ 
		public function _is_logged_in() 
		{
		    $is_logged_in = $this->session->userdata('profile_id');
			if ($is_logged_in=="") 
			{
				redirect('auth/index/1');
				die();
			}
		}
		
		public function index()
		{
			$this->load->view('header.php');
			$this->load->view('menu.php');
			$this->load->view('footer.php');
		}
		
		/*-----------------------------Start View group HERE-----------------------------*/
		
		public function manage_group()
		{
			$data['ddgroup']=$this->db->get_where('m18_group',array('m_gr_status' => 1));
			$this->db->reconnect();
			
			$data['all_oper']=$this->db->query("Select * from vw_sp");
			$this->db->reconnect();
			
			$x=$this->input->post('ddgrp');
			$data['grp_dd']=$x;
			$data['groupmargin']=$this->db->query("CALL sp_assign_group('$x')");
			$this->db->reconnect();
			
			$data['group']=$this->db->query("CALL sp_select_group()");
			
			//print_r($data);die;
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Group/manage_group',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function add_group()
		{
			$d=CURR_DATE;
			$gp=array(
			'm_gr_name'=>$this->input->post('txtgrp'),
			'm_gr_user_id'=>$this->session->userdata('profile_id'),
			'm_gr_status'=>1,
			'm_gr_endate'=>$d
			);
			$this->db->insert('m18_group',$gp);
			
			$this->db->where('m_gr_endate',$d);
			$data['group']=$this->db->get('m18_group');
			foreach($data['group']->result() as $row1)
			{
				break;
			}
			//$this->db->where('m_sp_status',1);
			$data['op']=$this->db->get('m17_service_provider');
			foreach($data['op']->result() as $row)
			{
				$gpop=array(
				'm_service_id'=>$row->m_sp_type_id,
				'm_so_name'=>$row->m_sp_code,
				'm_so_code'=>'',
				'm_api_id'=>'',
				'm_gr_id'=>$row1->m_gr_id,
				'm_gro_entrydate'=>$d,
				'm_gro_status'=>1
				);
				$this->db->insert('tr06_group_op',$gpop);
				$mrop=array(
				'm_service_id'=>$row->m_sp_type_id,
				'm_so_name'=>$row->m_sp_code,
				'm_op_margin'=>'',
				'm_gr_id'=>$row1->m_gr_id,
				'm_opmr_entrydate'=>$d,
				'm_opmr_status'=>1
				);
				$this->db->insert('tr07_op_margin',$mrop);
			}
			header("location:".base_url()."index.php/group/manage_group");
		}
		
		
		
		//Assign Operator to Group
		public function add_op_to_group()
		{
			$sic=$this->input->post('api_opid');
			
			$quisic=explode('-',$sic);
			$stack=$quisic;
			
			foreach($stack as $qu)
			{	
					$soc="";
					$sid="";
					$sn="";
					$apiid="";
					$quesy=explode(',',$qu);
					for($y=0;$y<count($quesy);$y++)
					{
						if($y==0)
						$soc=$quesy[$y];
						if($y==1)
						$sid=$quesy[$y];
						if($y==2)
						$sn=$quesy[$y];
						if($y==3)
						$apiid=$quesy[$y];
					}
					
					$gp=array(
					'm_service_id' =>$sid,
					'm_so_name' =>$sn,
					'm_so_code'=>$soc,
					'm_api_id'=>$apiid,
					'm_gr_id' =>$this->input->post('group')
					);
					//print_r($gp);die;
					$q=" CALL sp_group_op(?" . str_repeat(",?", count($gp)-1) . ",@msg) ";
					$this->db->query($q,$gp);
					$msg=$this->db->query("SELECT @msg AS message");
			}
			echo 'Insert successfully';
		}
		
		/*-----------------------------Start Assign Group HERE-----------------------------*/
		
		public function assign_group()
		{
			$data['record']=$this->db->get_where('tr15_group_user',array('m_gu_status'=>1));
			$data['group']=$this->db->get_where('m18_group',array('m_gr_status'=>1));
			$data['user']=$this->db->query("SELECT * FROM view_member WHERE or_m_designation IN(22,23,24)");
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Group/assign_group',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function assign_groupuser()
		{
			$sic=$this->input->post('gp_userid');
			$stack=explode('-',$sic);
			$cnt=0;
			foreach($stack as $qu)
			{	
				$quesy=explode(',',$qu);
				if(!empty($quesy[0]) && !empty($quesy[1]) && $quesy[0] !=0)
				{
					$memid=$quesy[0];
					$grpid=$quesy[1];
					$cnt=$this->db->query("SELECT * FROM tr15_group_user WHERE tr15_group_user.m_u_id=".$memid)->num_rows();
					if($cnt ==0)
					{
						$data=array(
						'm_u_id'=>$memid,
						'm_group_id'=>$grpid,
						'm_aff_id'=>0,
						'm_gu_entrydate'=> CURR_DATE,
						'm_gu_status'=>1
						);
						$this->db->insert('tr15_group_user',$data);
					}
					else if($cnt >0)
					{
						$data1=array(
							'm_group_id'=>$grpid
						);
					$this->db->where('m_u_id',$memid);
					$this->db->update('tr15_group_user',$data1);
					}	
				}
			}
			echo 'Assigned successfully';
		}
		/*-----------------------------Start Assign Margin  HERE-----------------------------*/
		
		public function assign_margin()
		{
			$data['ddgroup']=$this->db->get_where('m18_group',array('m_gr_status' => 1));
			$x=$this->input->post('ddgrp');
			$data['grp_dd']=$x;
			
			$data['all_oper']=$this->db->query("Select * from vw_sp");
			
			$data['margin']=$this->db->get_where('tr07_op_margin',array('m_gr_id'=> $x));
			
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Group/assign_margin',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		//Assign Margin to Group
	public function add_mar_to_group()
	{
		$timezone = new DateTimeZone("Asia/Kolkata" );
		$date = new DateTime();
		$date->setTimezone($timezone);
		$sic=$this->input->post('marg_id');
		$quisic=explode('-',$sic);
		foreach($quisic as $qu)
		{
			$quesy=explode(',',$qu);
			$soc="";
			$sid="";
			$sn="";
			$quesy=explode(',',$qu);
			for($y=0;$y<count($quesy);$y++)
			{
			if($y==0)
			$soc=$quesy[$y];
			if($y==1)
			$sid=$quesy[$y];
			if($y==2)
			$sn=$quesy[$y];
			}
			$gp=array(
			'm_service_id' => $sid,
			'm_so_name' => $sn,
			'm_op_margin' => $soc,
			'm_gr_id' => $this->input->post('group')
			);
			//print_r($gp);die;
			$q=" CALL sp_assign_margin(?" . str_repeat(",?", count($gp)-1) . ") ";
			$this->db->query($q,$gp);
		}
		echo 'Insert successfully';
	}
	
		
}
