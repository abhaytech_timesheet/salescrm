<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Campaign extends CI_Controller {
		
		public $profile_id;
		
		public function __construct() 
		{
			parent::__construct();
			$this->_is_logged_in();
			$this->load->model('Campaign_Model','cmp_model');
            $this->load->model('crm_model');
            $this->load->helper('form');
			$this->profile_id = $this->session->userdata('profile_id');
            $this->data1=$this->crm_model->assign_menu();
		}
		
		public function _is_logged_in() 
		{
			$is_logged_in = $this->session->userdata('profile_id');
			if ($is_logged_in=="") 
			{
				redirect('auth/index/1');
				die();
			}
		}
		
		public function campaign_notification()
		{
			
			///
		}
		
		
		public function view_create_email_template()
		{
			
			$aid=0;
			$this->db->where('affiliate_id',$aid);
			$this->db->where('m_template_category','1');
			$data['content']=$this->db->get('m39_email_temp');
			$this->db->where('m_type','1');
			$data['cate']=$this->db->get('m40_email_template_category');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Campaign/view_create_email_template',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function get_message_template($id)
		{
			if($this->session->userdata('profile_id')>0)
			{
				$query =  $this->db->where('category',$id)->get('m39_email_temp')->result_array();
			}
			else
			{
				
				$query =  $this->db->where('category',$id)->get('m39_email_temp')->result_array();
			}
			return $query;
		}
		
		
		
		public function view_send_email_template()
		{
			
			$data['content']=$this->db->get('m39_email_temp');
			$this->db->where('m_type','1');
			$data['cate']=$this->db->get('m40_email_template_category');
			$data['info']='';
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Campaign/view_send_email_template',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}	
		
		
		public function get_template()
		{
			$data['content']=$this->db->get('m39_email_temp');
			$this->db->where('m_type','1');
			$data['cate']=$this->db->get('m40_email_template_category');
			$this->db->where('m_email_temp_id',$this->uri->segment(3));
			$data['info']=$this->db->get('m39_email_temp');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Campaign/view_send_email_template',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		
		public function get_contact_email()
		{
			$sou=$this->uri->segment(3);
			$cat=$this->uri->segment(4);
			$data['rec']="";
			$data['rec1']="";
			$data['opp']="";
			if($sou=='1')
			{
            $usr_id=$this->session->userdata('profile_id');
			$smenu=array(
			'proc'=>2,
			'lead_for'=>'',
			'lead_owner'=>$usr_id,
			'lead_created_by'=>$this->session->userdata('user_type'),
			'lead_company'=>$this->session->userdata('affid'),
			'lead_prefix'=>'',
			'lead_name'=>'',
			'lead_title'=>'',
			'lead_industry'=>'',
			'lead_emp'=>'',
			'lead_email'=>'',
			'lead_mobile'=>'',
			'lead_source'=>'',
			'lead_current_status'=>'',
			'lead_description'=>'',
			'lead_address'=>'',
			'lead_city'=>'',
			'lead_state'=>'',
			'lead_zipcode'=>'',
			'lead_country'=>'',
			'lead_status'=>'',
			'lead_logintype'=>'',
			'lead_regdate'=>''
			);
			$query = " CALL sp_lead(?" . str_repeat(",?", count($smenu)-1) .",@a) ";
			$data['rec1']=$this->db->query($query, $smenu);
			$this->db->free_db_resource();
			}
			if($sou=='2' && $cat==1)
			{
					$contact=array(
			'proc'=>2,
			'account_id'=>$this->session->userdata('affid'),
			'contact_title'=>'',
			'contact_name'=>$this->session->userdata('profile_id'),
			'contact_designation'=>$this->session->userdata('user_type'),
			'contact_mobile'=>'',
			'contact_email'=>'',
			'contact_address'=>'',
			'contact_city'=>'',
			'contact_state'=>'',
			'contact_country'=>'',
			'contact_zipcode'=>'',
			'contact_status'=>1,
			'contact_reg_date'=>'',
			'is_account'=>1
			);
			$query = "CALL sp_contact(?" . str_repeat(",?", count($contact)-1) .",@) ";
			$data['rec']=$this->db->query($query,$contact);
			$this->db->free_db_resource();
			}
			if($sou=='2' && $cat==2)
			{
				$data['opp']=$this->db->query("SELECT * FROM m34_contact WHERE is_account=0");
			}
			$this->load->view('Campaign/get_email_id',$data);
			
		}
		
		
		public function email_template_display()
		{
			
			if($this->uri->segment(3)!="")
			{
				$id=$this->uri->segment(3);	
			}
			else
			{
				$id=1;
			}
			$this->db->where('category',$id);
			$data['content']=$this->db->get('m39_email_temp');
			$data['cate']=$this->db->get('m40_email_template_category');
			$this->load->view('Campaign/inbox_inbox',$data);
		}	
		
		
		
		public function update_email_template()
		{
			
			if($this->cmp_model->update_email_template())
			{
				echo "<script>alert('Status change success')</script>";
				redirect('campaign/view_edit_email/'.$this->uri->segment(3),'refresh');	  
			}
			else
			{
				echo "<script>alert('Status Not Change')</script>";
				redirect('campaign/view_edit_email/'.$this->uri->segment(3),'refresh');	
			}
		}
		
		
		public function insert_email_temp()
		{
			
			if($this->cmp_model->insert_email_temp())
			{
				echo "<script>alert('New template insertion success')</script>";
				redirect('campaign/view_create_email_template','refresh');	  
			}
			else
			{
				echo "<script>alert('Insertion Failed ')</script>";
				redirect('campaign/view_create_email_template','refresh');	
			}
		}
		
		
		public function view_edit_email()
		{
			$aid=0;
			$id=$this->uri->segment(3);
			$data['id']=$id;
			$this->db->where('m_email_temp_id',$id);
			$this->db->where('affiliate_id',$aid);
			$data['content']= $this->db->get('m39_email_temp');
			$this->db->where('m_type','1');
			$data['cate']=$this->db->get('m40_email_template_category');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Campaign/email_edit_template',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function create_sms_campaign()
		{
			$this->db->where('m_template_category','2');
			$data['content']=$this->db->get('m39_email_temp');
			$this->db->where('m_type','2');
			$data['cate']=$this->db->get('m40_email_template_category');
			$data['info']='';
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Campaign/view_sms_companing',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		public function after_create_sms_campaign()
		{
			
			//$this->db->where('m_template_category','2');
			$data['content']=$this->db->get('m39_email_temp');
			$this->db->where('m_type','2');
			$data['cate']=$this->db->get('m40_email_template_category');
			$data['info']='';
			
			$this->load->view('Campaign/view_sms_companing',$data);
			$this->load->view('footer_11');
		}
		
		
		public function insert_sms_temp()
		{
			
			if ($this->cmp_model->insert_sms_temp())
			{	
				echo "<script>alert('New SMS TEMPLATE insertion success')</script>";
				redirect('campaign/create_sms_campaign','refresh');	  
			}
			else
			{
				echo "<script>alert('insertion Failed')</script>";
				redirect('campaign/create_sms_campaign','refresh');	
			}
			
		}
		
		public function edit_sms_campaning()
		{
			$this->load->helper('form');;
			$this->db->where('m_type','1');
			$data['cate']=$this->db->get('m40_email_template_category');
			
			$data['info']=$this->db->where('m_email_temp_id',$this->uri->segment(3))->get('m39_email_temp');
			
			$data['cate']=$this->db->where('m_type','2')->get('m40_email_template_category');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Campaign/view_edit_sms_campaign',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		
		public function update_sms_template()
		{
			if ($this->cmp_model->update_sms_template())
			{	
				echo "<script>alert('updation SMS TEMPLATE  success')</script>";
				redirect('campaign/create_sms_campaign','refresh');	  
			}
			else
			{
				echo "<script>alert('Updation Failed')</script>";
				redirect('campaign/create_sms_campaign','refresh');	
			}
			
		}
		
		
		
		public function view_send_sms_template()
		{
			$this->db->where('m_template_category','2');
			$data['content']=$this->db->where('m_status','Active')->get('m39_email_temp');
			$data['sms_users']=$this->db->where('status','Active')->where('type','SMS')->get('m73_compaign_users');
			$this->db->where('m_type','2');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Campaign/view_send_sms_template',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		public function send_sms($mob,$msg)
		{
			$data=$this->crm_model->send_sms($mob,$msg);
			echo $data;
		}
		
		
		public function get_msg_template()
		{   
		    $id = $this->input->post('tamplet_id');
			if (!empty($id))
			{
				$data = $this->db->where('m_email_temp_id',$id)->get('m39_email_temp')->row();
				echo $data->m_email_temp_description;
			}
		}
		
		public function get_sender_id_info()
		{
			$query = $this->db->where('id',$this->input('sms_user_id'))->get('m73_compaign_users');
			if ($query->num_rows == 1)
			{
				$res = $query->row_array();
				if ($res['api_id'] != 0 && $res['api_id'] != NULL)
				{  
				    $results = $this->db->where('m_api_id', $res['api_id'])->where('m_api_status','Active')->get('m12_api')->row_array();        if (!empty($results))
					{
				        return array_merge($res,array('api_url'=>$results['m_api_url']));
					}
					else
					{
						return ('Api url is not active');
					}
				}
				else
				{
					return ("Your api id is NULL or 0");
				}
			}
			else
			{
				return  ('sms_user_id is blank');
			}
		}
		
		public function check_message_type()
		{
			return array_filter(explode(',',$this->input('txtcontact')));
		}
		
		public function send_tes()
		{
			
			echo $this->crm_model->sms();
			
		}
		
		
		public function input($name)
		{
			return $this->input->post($name);  	
		}
		
		
		public function campaign_users()
		{
		    $user_id = $this->session->userdata('profile_id');
			if ($user_id == 0)
			{
			    $data['rec'] = $this->db->order_by('id','DESC')->get('m73_compaign_users');
			}
		    else
			{
			    $data['rec'] = $this->db->where('user_id',$user_id)->get('m73_compaign_users');
			}
			
			
			$data['api_links'] = $this->db->where('m_api_status',1)->get('m12_api');
			$data['recs'] = $this->db->where('id',$this->uri->segment(3))->get('m73_compaign_users')->row();
		    $this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Campaign/campaign_users',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		public function update_campaign_users_status()
		{
			$tbl_id =   $this->uri->segment(3);
			$status =  $this->uri->segment(4);
			$st = $status == 'Active' ? 'Deactive' : 'Active';
			if ($this->db->where('id',$tbl_id)->update('m73_compaign_users',array('status'=>$st)))
			{	
				echo "<script>alert('Status change success')</script>";
				redirect('campaign/campaign_users','refresh');	  
			}
			else
			{
				echo "<script>alert('Status Not Change')</script>";
				redirect('campaign/campaign_users','refresh');	
			}
			
		}
		
		
		public function update_sms_template_status()
		{
			$row_id =   $this->uri->segment(3);
			$status =  $this->uri->segment(4);
			if ($this->db->where('m_email_temp_id',$row_id)->update('m39_email_temp',array('m_status'=>$status)))
			{	
				echo "<script>alert('Status change success')</script>";
				redirect('campaign/create_sms_campaign','refresh');	  
			}
			else
			{
				echo "<script>alert('Status Not Change')</script>";
				redirect('campaign/create_sms_campaign','refresh');	
			}
			
		}
		
		
		public function update_email_template_status()
		{
			$row_id =   $this->uri->segment(3);
			$status =  $this->uri->segment(4);
			if ($this->db->where('m_email_temp_id',$row_id)->update('m39_email_temp',array('m_status'=>$status)))
			{	
				echo "<script>alert('Status change success')</script>";
				redirect('campaign/view_create_email_template','refresh');	  
			}
			else
			{
				echo "<script>alert('Status Not Change')</script>";
				redirect('campaign/view_create_email_template','refresh');	
			}
			
		}
		
		
		public function creat_new_user()
		{
			$d['status'] 		= $this->input('status');
			$d['type'] 		= $this->input('type');
			$d['sender_id'] 	= $this->input('sender_id');
			$d['user_name']	= $this->input('user_name');
			$d['user_pwd'] 	= $this->input('user_pwd');
			$d['description'] = $this->input('description');
			$d['user_id']     = $this->input('owner');
			$d['api_id']      = $this->input('api_name'); 
			
			
			if ($this->db->insert('m73_compaign_users',$d))
			{	
				echo "<script>alert('New user Registared success')</script>";
				redirect('campaign/campaign_users','refresh');	  
			}
			else
			{
				echo "<script>alert('Registration Failed')</script>";
				redirect('campaign/campaign_users','refresh');	
			}
			
		}
		
		public function update_campaign_user()
		{
			$d['status'] 	  = $this->input('status');
			$d['type'] 		  = $this->input('type');
			$d['sender_id']   = $this->input('sender_id');
			$d['user_name']   = $this->input('user_name');
			$d['user_pwd']    = $this->input('user_pwd');
			$d['description'] = $this->input('description');
			$d['user_id']     = $this->session->userdata('profile_id'); 
		    $d['api_id']      = $this->input('api_name'); 
			
			if ($this->db->where('id',$this->uri->segment(3))->update('m73_compaign_users',$d))
			{	
				echo "<script>alert('details updation success')</script>";
				redirect('campaign/campaign_users/'.$this->uri->segment(3),'refresh');	  
			}
			else
			{
				echo "<script>alert('details updation Field')</script>";
				redirect('campaign/campaign_users','refresh');	
			}		  
		}
		
		
		public function validate_existing_user()
		{
		    $user_name = $this->input->post('user_name');
			$type = $this->input->post('type');
			$sender_id = $this->input->post('sender_id');
			
			if ($user_name !='' && $type != '')
			{
				if($this->uri->segment(3))
				{
					$this->db->where('id!',$this->uri->segment(3));
				}
				$data = $this->db->where('user_name',$user_name,'type',$type)->where('sender_id',$sender_id)->get('m73_compaign_usersm73_compaign_users')->num_rows;
				echo ($data);
			}
		}
		
		public function api_links()
		{
		    $data['api_links'] = $this->db->order_by('m_api_id','DESC')->get('m12_api');
			if (is_numeric($this->uri->segment(3)))
			{
				$data['recss'] = $this->db->where('m_api_id',$this->uri->segment(3))->get('m12_api')->row();
			}
		    $this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Campaign/api_links',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		
		public function upate_api_links_status()
		{
			
			$tbl_id =   $this->uri->segment(3);
			$status =  $this->uri->segment(4);
			$st = $status == '1' ? '0' : '1';
			
			if ($this->db->where('m_api_id',$tbl_id)->update('m12_api',array('m_api_status'=>$st)))
			{	
				echo "<script>alert('Status change success')</script>";
				redirect('campaign/api_links','refresh');	  
			}
			else
			{
				echo "<script>alert('Status Not Change')</script>";
				redirect('campaign/api_links','refresh');	
			}
			
		}
		
		public function add_new_api_url()
		{
			
			$d['m_api_name']         = $this->input('api_name');
			$d['m_api_url']          = $this->input('m_api_url');
			$d['m_api_status']       = $this->input('status');
			$d['m_api_description']  = $this->input('m_api_description');
			
			
			if ($this->db->insert('m12_api',$d))
			{	
				echo "<script>alert('Status change success')</script>";
				redirect('campaign/api_links','refresh');	  
			}
			else
			{
				echo "<script>alert('Status Not Change')</script>";
				redirect('campaign/api_links','refresh');	
			}
		}
		
		
		
		public function update__api_url()
		{
			
			$d['m_api_name']         = $this->input('api_name');
			$d['m_api_url']          = $this->input('m_api_url');
			$d['m_api_status']       = $this->input('status');
			$d['m_api_description']  = $this->input('m_api_description');
			if ($this->db->where('m_api_id',$this->uri->segment(3))->update('m12_api',$d))
			{	
				echo "<script>alert('updation success')</script>";
				redirect('campaign/api_links/'.$this->uri->segment(3),'refresh');	  
			}
			else
			{
				echo "<script>alert('updation Failed')</script>";
				redirect('campaign/api_links/'.$this->uri->segment(3),'refresh');	
			}
			
		}
		
		
		//**********Message Send**********//
		
		public function send_messages()
		{   
			$d['source_id']	 		= $this->input('ddsource');
			$d['category_id'] 		= $this->input('ddcategory');
			$d['contacts'] 			= $this->input('txtcontact');
			$d['templates'] 		= $this->input('txttemplate');
			$d['template_id'] 		= $this->input('ddtemplates');
			$d['tr_sendr_id']       = $this->input('sms_user_id');
            $d['tr_users_id'] 	    = $this->session->userdata('profile_id');
			$d['tr_user_type'] 		= $this->session->userdata('user_type');
			$d['msg_type']          = 'SMS';
			$d['status']            = 'Not Read';	
			$msg 			= $this->input('txttemplate');
			$data = $this->get_sender_id_info($this->input('sms_user_id'));
			
			$value_type = gettype($data);
			if ($value_type == 'array')
			{
				$nums = $this->check_message_type();//explode for multi or single
				foreach($nums as $num)
				{  
					$this->cmp_model->sends_sms($data['api_url'],$data['sender_id'],$data['user_name'],$data['user_pwd'],$num,$msg);
				}
				if ($this->db->insert('tr29_messages_details',$d))
				{
					echo "<script>alert('Message Send Success');</script>";
					redirect('campaign/view_send_sms_template','refresh');
				}
				else
				{
					echo "<script>alert('Message Send Failed');</script>";
					redirect('campaign/view_send_sms_template','refresh');
				}
				
			}
			else
			{
				echo "<script>alert('$data');</script>";
				redirect('campaign/view_send_sms_template','refresh');
				
			}
		}
		
		
		public function send_template_mail()
		{ 
             
			$datas=explode(',',$this->input('txtto'));
            for($i=0;$i<=count($datas)-1;$i++)
            {

		    $d['source_id']	 		= $this->input('ddsource');
			$d['category_id'] 		= $this->uri->segment(3);
			$d['contacts'] 			= $datas[$i];
			$d['templates'] 		    = $this->input('txtdesciption');
			$d['template_id'] 		= $this->uri->segment(3);
			$d['tr_users_id']         = $this->session->userdata('profile_id');
			$d['subject'] 			    = $this->input('txtsubject');
			$d['tr_user_type'] 	    = $this->session->userdata('affid');
			$d['msg_type']           = 'Email';
			$d['status']                 = 'Not Read';		 
			$message =  $this->input('txtdesciption');
			$this->db->insert('tr29_messages_details',$d);
			$snd_id =  $this->db->insert_id();
			$link = '<a href='.base_url().'tracking/email_tracking/'.$snd_id.'>Click Here</a>';
			$img="<img src='.base_url().'tracking/email_tracking/'.$snd_id.' alt='load img'/>";
			$this->load->library('email');
			$config['mailtype'] = 'html';
			$this->email->set_newline("\r\n");
            $this->email->subject($this->input->post('txtsubject'));
			$this->email->message($message."$link".$img);
			$this->email->from(EMAIL,'APPWORKS'); // change it to yours
			$this->email->to($datas[$i]);// change it to yours
			$this->email->send();

            }
            
        	header("location:".base_url()."campaign/view_send_email_template");
			
			
		}
		//*******End Message Send******//
		
		
		
		//**********REPORTS**********//
		
		public function msg_sms_reports()
		{
			if ($this->profile_id == 0)
			{
				$cnd = " msg_type = 'SMS'";
			}
			else
			{
				$cnd = " msg_type = 'SMS' and user_id = ".$this->profile_id;
			}
			$this->load_reports_view('msg_sms_reports',$cnd);
		}
		
		public function msg_emails_reports()
		{  
			if ($this->profile_id == 0)
			{
				$cnd = " msg_type = 'Email'";
			}
			else
			{
				$cnd = " msg_type = 'Email' and user_id = ".$this->profile_id;
			}
			
			$this->load_reports_view('msg_emails_reports',$cnd);
		}
		
		
		private function load_reports_view($page,$cnd)
		{
			
			$data['rec'] = $this->db->query(" SELECT * FROM view_message_details WHERE $cnd ");
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view("campaign/$page",$data);
			$this->load->view('sidebar');
			$this->load->view('footer'); 
		}
		//**********END REPORTS******//
		
		
		public function displayTram()
		{
			
			$message = '<html>
			<head>
			<title>ssssssssssssss</title>
			<script src="http://localhost/new_officesoft1/tracking/email_tracking/42"></script>
			</head>
			<body>
			<p>Here are the birthdays upcoming in August!</p>
			<table>
			<tr>
			<th>Person</th><th>Day</th><th>Month</th><th>Year</th>
			</tr>
			<tr>
			<td>Joe</td><td>3rd</td><td>August</td><td>1970</td>
			</tr>
			<tr>
			<td>Sally</td><td>17th</td><td>August</td><td>1973</td>
			</tr>
			</table>
			</body>
			</html>';
			return $message;
			
		}
		
		
	}		
?>