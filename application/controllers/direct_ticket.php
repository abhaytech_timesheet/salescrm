<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Direct_ticket extends CI_Controller {
		public function __construct() 
		{
			parent::__construct();
			$this->load->model('clientmodel');
			$timezone = new DateTimeZone("Asia/Kolkata" );
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
			//$this->_is_logged_in();
		}
		
		/*Session Check Function */ 
		public function _is_logged_in() 
		{
		    $is_logged_in = $this->session->userdata('profile_id');
			if ($is_logged_in=="") 
			{
				redirect('auth/index/1');
				die();
			}
			}
		
		public function is_affiliate()
		{
			if($this->uri->segment(3)=="")
			{
				$data['info']="";
			}
			else
			{
				$data['info']=$this->uri->segment(3);
			}
			if(base_url()!="http://208.115.226.18/")
			{
				$data['aff_id']=$this->db->query("SELECT GetAffiliateId('".base_url()."',1) as Aff_id");
				foreach($data['aff_id']->result() as $row1) {break;}
				if($row1->Aff_id==base_url())
				{
					echo $row1->Aff_id.'-'."THIS URL IS NOT REGISTERED .Please Contact Admin";
				}
				else
				{
					$data['affiliates']=$row1->Aff_id;
					$this->db->where('affiliate_id',$row1->Aff_id);
					$data['aff_detail']=$this->db->get('m22_affiliate');
				}
			}
			else
			{
				$data['affiliates']=0;
			}
			return $data;
		}
		
		public function index()
		{
		$data=$this->is_affiliate();
		$data['affiliates'];
       if($this->uri->segment(3)=="")
		{
		$data['info']="";
		}
		else
		{
		$data['info']=$this->uri->segment(3);
		$data['name']=$this->uri->segment(4);
	    $data['ticketno']=$this->uri->segment(5);
		$data['depart']=$this->uri->segment(6);
		}
			$this->load->view('header',$data);
			$this->load->view('outer_ticket/menu',$data);
			$this->load->view('outer_ticket/ticket_type',$data);
			$this->load->view('footer',$data);
		}
		
		public function submit_ticket()
		{
			$data=$this->is_affiliate();
			$p_id=$this->session->userdata('profile_id');
			$dep=$this->uri->segment(2);
			$data['dep']=$dep;
			$this->load->view('header',$data);
		    $this->load->view('outer_ticket/menu',$data);
			$this->load->view('outer_ticket/submit_ticket',$data);
			$this->load->view('footer');
		}
		//Outer Ticket Submit here 
		public function outer_ticket()
		{
			$data=$this->is_affiliate();
			$data['affiliates'];
			$sessiondata=array(
							'affid'=> $data['affiliates']
							);
			$this->session->set_userdata($sessiondata);
			if($data['affiliates']!=0 && $data['affiliates']!="")
			{
			$config['upload_path']   =   "application/uploadimage/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "1907";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$fileupload=($finfo['raw_name'].$finfo['file_ext']);
			$ticket_no=$this->clientmodel->add_new_outer_ticket($fileupload);
			$txtemail=$this->input->post('txtemail');
			$txtname=$this->input->post('txtname');
			$txtsubject=$this->input->post('txtsubject');
			$txtcomment=$this->input->post('txtdiscription');
			//$ticket_no=$this->input->post('txtticket');
			$department=$this->input->post('txtdepartment1');
			$status="";
			if($this->input->post('txturgency')==1) 
			{$status="High";}
			if($this->input->post('txturgency')==2)
			{$status="Medium";}
			if($this->input->post('txturgency')==3)
			{$status="Low";}
			$ip=$_SERVER['REMOTE_ADDR'];
			$message = '<p>Dear: '.$txtname.',<br><br>
		  	Thank you for contacting our support team. A support ticket has now been opened for your request. You will be notified when a response is made by email.
			The details of your ticket are shown below.<br><br>
			Subject: '. $txtsubject.'<br><br>
			Priority: '. $status .'<br><br>
			Status: Open<br><br>
			Your IP: '. $ip.'<br><br>
            For checking your Ticket- '.base_url().'direct_ticket/view_ticket/'.$ticket_no.'</p>';
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL,'Support Techaarjavam'); // change it to yours
			$this->email->to($txtemail);// change it to yours
			$this->email->subject('Techaarjavam Support Ticket[ID:'.$ticket_no.']');
			$this->email->message($message);
			if($this->email->send())
			{
				header("Location:".base_url()."direct_ticket/index/1/".$txtname."/".$ticket_no."/".$department."/");
			}
			else
			{
				show_error($this->email->print_debugger());
			}
          }
		}
		public function announcements()
		{
			$data=$this->is_affiliate();
			$t=array('2','3');
			$this->db->where_in('for',$t);
			$data['rec']=$this->db->get('tr19_anoncement');
			$this->load->view('header',$data);
			$this->load->view('outer_ticket/menu',$data);
			$this->load->view('outer_ticket/announcements',$data);
			$this->load->view('footer');
		}
		
		
		public function full_Announcements()
		{
			$data=$this->is_affiliate();
			$p_id=$this->uri->segment(3);
			$this->db->where('id',$p_id);
			$data['rec']=$this->db->get('tr19_anoncement');
			$this->load->view('header',$data);
			$this->load->view('outer_ticket/menu',$data);
			$this->load->view('outer_ticket/full_Announcements',$data);
			$this->load->view('footer');
		}
		
		public function view_ticket()
		{
			$data=$this->is_affiliate();
			$p_id=$this->uri->segment(3);
            $query=$this->db->query("SELECT GetTicketExist('$p_id') as rc");
            $row2 = $query->row();
            $data1['is_ticexist']=$row2->rc;
            if($row2->rc==1)
			{
			$condition='';
			$condition=$condition." m46_submit_ticket.tkt_ref_no='$p_id'";
			$clsdt1= array(
			'querey'=>$condition
			);
			$query1 = "CALL sp_view_ticket(?)";
			$data['rec']=$this->db->query($query1,$clsdt1);
			$this->db->free_db_resource();
			$this->db->where('tkt_ref_no',$p_id);
			$data['ticket']=$this->db->get('m46_submit_ticket');
			foreach($data['ticket']->result() as $row)
			{
				$id=$row->tkt_id;	
			}
			$condition = "tr24_ticket_transaction.tkt_id='$id'";
			$clsdt= array(
			'querey'=>$condition
			);
			$query = "CALL sp_view_ticket_reply(?)";
			$data['rec1']=$this->db->query($query,$clsdt);
			$this->db->free_db_resource();
			}
			$this->load->view('header',$data1);
			$this->load->view('outer_ticket/menu',$data);
			$this->load->view('outer_ticket/view_ticket',$data);
			$this->load->view('footer');
		}
		
		
		/* ----------------Resolve Problem on btn Click--------------- */
		public function resolve()
		{
		
			$ticket_no=$this->input->post('ticket_no');
			$data['rec']=$this->db->query("SELECT tkt_email,tkt_person_name FROM m46_submit_ticket WHERE tkt_ref_no='$ticket_no' GROUP BY tkt_email");
			
			foreach($data['rec']->result() as $rec)
			{
				$tic_owner=$rec->tkt_email;
				$txtname=$rec->tkt_person_name;
				break;
			}
			
			$data=array(       
		    'proc'=>3,'tkt_ref_no'=>$ticket_no,'tkt_person_name'=>'','tkt_email'=>'','tkt_department'=>'','emp_id'=>'','tkt_subject'=>'','tkt_urgency'=>'','tkt_discription'=>'','tkt_response_type'=>1,'response_by'=>'','tkt_userfile'=>'','tkt_status'=>0,'account_id'=>'','affiliate_id'=>'','tkt_sub_date'=>$this->curr,'trans_description'=>'','trans_response_date'=>'','trans_status'=>''
			);
			$query = "CALL sp_ticket_submission(?" . str_repeat(",?", count($data)-1) .",@a)";
			$data['rec']=$this->db->query($query,$data);
			$this->db->free_db_resource();
			
			$ip=$_SERVER['REMOTE_ADDR'];
			$message = '<p>Dear: '.$txtname.',<br><br>
			This is in response of your attempt that your all query has been resolved now. So, we are considering it true and here by changing the status of your ticket('.$ticket_no.') to closed .<br><br> Thanking you,<br><br>
			Keep visiting us!<br><br>
			Priority: High<br><br>
			Status: Closed<br><br>
			Your IP: '. $ip.'<br><br>
			For checking your Ticket- '.base_url().'direct_ticket/view_ticket/'.$ticket_no.'</p>';
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL,'Support Techaarjavam'); // change it to yours
			$this->email->to($tic_owner);// change it to yours
			$this->email->bcc('techaarjavam@gmail.com');
			$this->email->subject('Techaarjavam Support Ticket[ID:'.$ticket_no.']');
			$this->email->message($message);
			if($this->email->send())
			{
				header("Location:".base_url()."direct_ticket/view_ticket/$ticket_no");  
			}
			else
			{
                echo "Due to some error email have not been sent";
				
                header( "refresh:3;url=".base_url()."direct_ticket/view_ticket/$ticket_no");
                
			}
			
		}
		
	}
	
	
?>		