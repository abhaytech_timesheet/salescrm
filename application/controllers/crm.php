<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class CRM extends CI_Controller {
		public function __construct() 
		{
			parent::__construct();
            $timezone = new DateTimeZone("Asia/Kolkata" );
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
            $this->hour=$date->format('H');
			$this->_is_logged_in();
			$this->data1=$this->crm_model->assign_menu();
		}
		/*sp_lead*/
		/*
			CALL sp_lead(5,'','0','','','','','','','','','','','`m32_lead`.`lead_status`!=8 ORDER BY `m32_lead`.`lead_regdate` DESC','','','','','','','','',@a);
			SELECT @a AS A;
		*/
		/*Session Check Function */ 
		public function _is_logged_in() 
		{
		    $is_logged_in = $this->session->userdata('profile_id');
			if ($is_logged_in=="") 
			{
				redirect('auth/index/1');
				die();
			}
		}
		
		public function Get_user_auth()
		{
			$data=$this->router->fetch_class()."/".$this->router->fetch_method();
			
			$query=$this->db->query("SELECT Get_user_authorise('".$this->session->userdata('profile_id')."','".$this->session->userdata('designation')."','".$data."') as user_auth")->row();
			
			if($query->user_auth)
			{
				return 1;
			}
			else
			{
				
				redirect('auth/index/1');
				die();
			}
		}
		
		public function index()
		{
			$this->load->view('header.php');
			$this->load->view('menu.php');
			$this->load->view('CRM/footer.php');	
		}
		//Select State	
		public function select_state()
		{
			$datas=$this->mastermodel->select_state($this->uri->segment(3));
			$data['state_id']=$this->uri->segment(4);
			$json=json_encode($datas->result());
			echo $json;
		}
		// Select city	
		public function select_city()
		{
			$datas=$this->mastermodel->select_state($this->uri->segment(3));
			$data['city_id']=$this->uri->segment(4);
			$json=json_encode($datas->result());
			echo $json;
		}
		
		
		//<-------------------lead function start--------------------------------------->//		
		
		public function add_lead()
		{
		    $data=$this->crm_model->lead();
			$this->load->view('CRM/view_lead',$data);
			$this->load->view('footer_11');
		}
		
		
		//Add Lead Form
		public function view_lead1()
		{
			$data=$this->crm_model->lead();
			$this->load->view('CRM/view_lead',$data);
			$this->load->view('footer_11');
		}
		
		public function show_lead()
	    {
			$data=$this->crm_model->show_lead($this->session->userdata('user_type'));
			$data['id']=$this->uri->segment(3);
			$this->load->view('CRM/view_all_lead',$data);
			$this->load->view('footer_11');
			
		}
		
		
		public function view_lead()
	    {
			$this->load->model('users_records');
			$data=$this->crm_model->show_lead($this->session->userdata('user_type'));
			$data['id']=$this->uri->segment(3);
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/view_all_lead',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
	    public function edit_lead()
	    {
		    $data=$this->crm_model->lead();
			$data['lead_id']=$this->uri->segment(3);
			$this->db->where('lead_id',$this->uri->segment(3));
			$data['lead_info']=$this->db->get('m32_lead');
			$data['id']=$this->session->userdata('user_type');
			$this->load->view('CRM/view_edit_lead',$data);
			$this->load->view('footer_11');
		}
		
		
		public function update_lead()
	    {
			$ids=$this->crm_model->update_lead();
			echo $ids;
		}
		
		public function insert_lead()
		{
			$ids=$this->crm_model->insert_lead();
			echo $ids;
		}
		
		public function delete_lead()
	    {
			$data=$this->crm_model->delete_lead();
			$this->load->view('CRM/view_all_lead',$data);
			$this->load->view('footer_11');
		}
		
		public function lead_search_report()
	    {
			$data=$this->crm_model->lead_search_report();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/view_all_lead',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		public $data;
		public function view_csv_upload()
		{
			$this->load->view('CRM/view_csv_upload');
			$this->load->view('footer_11');
			
		}
		
		
		function upload_sampledata()
		{
			$this->load->model('crm_model');
			$data['result']=$this->crm_model->upload_sampledata_csv();
			header("Location:".base_url()."index.php/crm/view_lead/0");
		}
		
		public function view_modified_leads()
		{
			$this->db->order_by('lead_modidate','DESC');
			$this->db->where('lead_modidate !=','');
			$data['lead']=$this->db->get('m32_lead');
			$this->load->view('CRM/view_modified_leads',$data);
			$this->load->view('footer_11');
		}
		//<------------------------lead function end-------------------->//
		
		//<------------------------account start------------------------>//	
		public function get_account_detail()
		{
			//$this->db->select('account_id,account_name');
			$query=$this->crm_model->view_account();
			$json=json_encode($query['account']->result());
			echo $json;
		}
		public function view_account()
		{
			$data=$this->crm_model->view_account();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/view_all_account',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		public function show_account()
		{
			$data=$this->crm_model->show_account();
			$this->load->view('CRM/view_all_account',$data);
			$this->load->view('footer_11');
		}
		
		public function add_account()
		{
			$data=$this->crm_model->account();
			$this->load->view('CRM/view_account',$data);
			$this->load->view('footer_11');
		}
		
		public function edit_account()
		{
			$data=$this->crm_model->account();
			$this->db->free_db_resource();
			$data['account_id']=$this->uri->segment(3);
			$this->db->where('account_id',$this->uri->segment(3));
			$data['info']=$this->db->get('m33_account');
			$this->load->view('CRM/view_edit_aacount',$data);
			$this->load->view('footer_11');
		}
		
		public function insert_account()
		{	
			$data=$this->crm_model->insert_account();
			echo $data;
		}
		
		
		public function update_account()
		{
			$data=$this->crm_model->update_account();
		    echo $data;
		}
		//<----------------account end------------------------>//
		//<----------------add project to account------------------------>//
		
		public function view_account_project()
		{ 
			$this->db->where('ACCP_ACC_ID',$this->uri->segment(3));
			$data['proj']=$this->db->get('view_project_details');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/view_add_account_project',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		public function insert_acc_proj()
		{
			$data=$this->crm_model->insert_acc_proj();
		    echo $data;
		}
		
		//<----------------add project to account end------------------------>//
		/*---------------This Action is Use For Profile Menu-----------------*/
		
		public function view_account_profile()
		{
			
            if($this->uri->segment(3)!='' || $this->uri->segment(3)!=0)
            {
				if($this->uri->segment(3)==$this->session->userdata('affid') || $this->uri->segment(4)==$this->session->userdata('affid'))
				{
					$this->db->where('account_id',$this->uri->segment(3));
					$this->db->where('contact_id',$this->session->userdata('profile_id'));
					$data['acc']=$this->db->get('view_account');
					$data['loc']=$this->db->get('m05_location');
					$query = " CALL sp_view_all_project('`m43_acc_project`.`m_acc_id`=".$this->uri->segment(3)." GROUP BY `tr22_project_participants`.`parti_project_id` ORDER BY `m43_acc_project`.`m_acc_project` DESC')";
					$data['pro']=$this->db->query($query);
					$this->db->free_db_resource();
					$data['ticket']=$this->db->get('m46_submit_ticket');
					$t=array('2','3');
					$this->db->where_in('for',$t);
					$data['anonce']=$this->db->get('tr19_anoncement');
					$data['state']=$this->db->query("SELECT * FROM m05_location WHERE m05_location.m_parent_id=1");
					$this->load->view('header');
					$this->load->view('menu1',$this->data1);
					$this->load->view('CRM/view_accountprofile',$data);
					$this->load->view('sidebar');
					$this->load->view('footer');
				}
				else
				{
					header("Location:".base_url()."report/dashboard/");
				}
			} 
			else
            {
				header("Location:".base_url()."report/dashboard/");
			}
		}
		
		public function change_account_image()
		{
			$id=$this->crm_model->change_account_image();
			header("Location:".base_url()."crm/view_account_profile/".$id);
		}
		
        public function change_contact_image()
		{
			$id=$this->crm_model->change_contact_image();
			header("Location:".base_url()."crm/view_account_profile/".$id);
		}
		
		//<----------------contact start---------------------->//
		
		public function view_contact()
		{
			$data=$this->crm_model->view_contact();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/view_all_contact',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function show_contact()
		{
			$data=$this->crm_model->view_contact();
			$this->load->view('CRM/view_all_contact',$data);
			$this->load->view('footer_11');
		}
		
		public function create_contact()
		{
			$data=$this->crm_model->contact();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/view_contact',$data);
			$this->load->view('footer_11');
		}
		
		public function add_contact()
		{
			$data=$this->crm_model->contact();
			$this->load->view('CRM/view_contact',$data);
			$this->load->view('footer_11');
		}
		
		/*insert contact*/
		public function insert_contact()
		{
			$data=$this->crm_model->insert_contact();
			echo $data;
		}
		
		/*Edit Contact*/
		public function edit_contact()
		{   
			$data=$this->crm_model->contact();
			$data['contact_id']=$this->uri->segment(3);
			$this->db->where('contact_id',$this->uri->segment(3));
			$data['cont']=$this->db->get('m34_contact');
			
			$data['info']=$this->db->get('m35_opportunity');
			$this->load->view('CRM/view_edit_contact',$data);
			$this->load->view('footer_11');
		}
		/*Edit Contact after Opportunity */
		public function edit_contact_afteropp()
		{   
			$data=$this->crm_model->contact();
			$data['contact_id']=$this->uri->segment(3);
			$this->db->where('account_id',$this->uri->segment(3));
            $this->db->where('is_account',0);
			$data['cont']=$this->db->get('m34_contact');
			$this->db->where('opportunity_id',$this->uri->segment(3));
			$data['info']=$this->db->get('m35_opportunity');
			$this->load->view('CRM/view_edit_contact',$data);
			$this->load->view('footer_11');
		}
		
		/*Update Contact*/
		public function update_contact()
		{
			$data=$this->crm_model->update_contact();
			echo $data;
		}
		
		//<------------------------contact end------------------>//
		//<------------------------task start----------------->//
		public function get_detail_for_task()
		{
			if($this->input->post('for')==1)
			{
				$query=$this->db->query("SELECT `vw_lead`.`lead_id` as account_id, `vw_lead`.`lead_name` as account_name FROM `vw_lead` WHERE `vw_lead`.`Lead_Status1` NOT IN (7,8) AND (CASE WHEN(".$this->session->userdata('user_type')."=2) THEN (`vw_lead`.`lead_owner1` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid')." AND `m06_user_detail`.`or_m_designation`=4) AND `vw_lead`.`lead_created_by`=1) OR (`vw_lead`.`lead_owner1`=".$this->session->userdata('profile_id')." AND `vw_lead`.`lead_created_by`=2) WHEN(".$this->session->userdata('user_type')."=1) THEN `vw_lead`.`lead_owner1`=".$this->session->userdata('profile_id')." AND `vw_lead`.`lead_created_by`=1 ELSE 1 END)");
			}
			if($this->input->post('for')==2)
			{
				$query=$this->db->query("SELECT `view_contact`.`contact_id` as account_id, `view_contact`.`contact_name` as account_name  FROM view_contact WHERE (CASE WHEN(".$this->session->userdata('user_type')."=2) THEN (`opportunity_owner` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`= ".$this->session->userdata('affid')." ) OR `account_owner` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`= ".$this->session->userdata('affid')." ) AND (`account_owned_by`= 1 OR `opportunity_owned_by`=1) OR ((`account_owner`= ".$this->session->userdata('profile_id')." OR `opportunity_owner`=".$this->session->userdata('profile_id').") AND (`account_owned_by`= 2 OR `opportunity_owned_by`=2))) WHEN(".$this->session->userdata('user_type')."=1) THEN ((`account_owner`= ".$this->session->userdata('profile_id')." AND `account_owned_by`= 1) OR (`opportunity_owner`= ".$this->session->userdata('profile_id')." AND `opportunity_owned_by`=1)) ELSE 1 END) ORDER BY `contact_id` DESC");
			}
          
			$json=json_encode($query->result());
			echo $json;
		}

		public function project_category1()
		{
			$query=$this->db->query("SELECT `m42_project`.`m_project_id`, `m42_project`.`m_project_name`FROM `m42_project` WHERE `m42_project`.`m_project_stauts`=1 and `m42_project`.`m_project_type`=".$this->input->post('ddrelate'));
			$json=json_encode($query->result());
			echo $json;
		}
		public function create_task()
		{
			$this->load->view('CRM/view_task');
			$this->load->view('footer_11');
		}
		public function after_task_followup()
		{
			header("Location:".base_url()."index.php/crm/view_task/0");
		}
		
		public function assign_task()
		{
			$data['is_lead']=$this->uri->segment(4);
			$data['id']=$this->uri->segment(3);
			$this->load->view('CRM/view_assign_task',$data);
			$this->load->view('footer_11');
		}
		
		public function get_detail()
		{
			$lid=$this->input->post('id');
			echo $lid;
			if($this->uri->segment(3)==1)
			{
				$this->db->where('lead_id',$lid);
				$data['info']=$this->db->get('m32_lead');
				foreach($data['info']->result() as $row)
				{
					echo $row->lead_name;
				}
			}
			if($this->uri->segment(3)==2)
			{
				$this->db->where('contact_id',$lid);
				$data['info']=$this->db->get('m34_contact');
				foreach($data['info']->result() as $row)
				{
					echo $row->contact_name;
				}
			}
		}
		
		
		public function insert_task()
		{
			$data=$this->crm_model->insert_task();
			echo $data;
		}
		
		public function edit_task()
		{
			$data=$this->crm_model->edit_task();
			$this->load->view('CRM/view_edit_task',$data);
			$this->load->view('footer_11');
		}
		
		public function update_task()
		{
			$data=$this->crm_model->update_task();
			echo $data;
		}
		
		public function complete_task()
		{
			$data=$this->crm_model->complete_task();
			echo $data;
		}
		
		
		public function show_task()
		{	
			$data=$this->crm_model->show_task();
			$this->load->view('CRM/view_all_task',$data);
			$this->load->view('footer_11');
		}
		
		public function view_task()
		{
		
		    $data=$this->crm_model->show_task();
			$this->db->free_db_resource();
			$st=$this->uri->segment(3);
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/view_all_task',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		public function view_task_history()
		{
			$data=$this->crm_model->view_task_history();
            
			$this->db->free_db_resource();
			
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
            
			$this->load->view('CRM/view_task_history',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		/* Follow Up on Task assign on User.*/
		
		public function insert_follow_up()
		{
			$data=$this->crm_model->insert_follow_up();
			echo $data;
			//header("Location:".base_url()."index.php/crm/view_task_history/$task_no");
		}
		
		public function admin_response()
		{
			$task_no=$this->crm_model->insert_follow_up();
			header("Location:".base_url()."index.php/crm/view_task_history/$task_no");
			
		}
		
		
		/*Start Opportunity */
		
		public function project_category()
		{
			$json=$this->crm_model->project_category();
			echo $json;
		}
		
		public function services_category()
		{
			$json=$this->crm_model->services_category();
			echo $json;
		}
		
		public function view_opportunity()
		{
            if($this->session->userdata('user_type')==1)
			{
				$this->db->where('opportunity_owner',$this->session->userdata('profile_id'));
				$data['info']=$this->db->get('m35_opportunity');
			}
			if($this->session->userdata('user_type')==2)
			{
				$data['info']=$this->db->query("SELECT * FROM (`m35_opportunity`) WHERE (`opportunity_owner`=".$this->session->userdata('profile_id')." AND `m35_opportunity`.`opportunity_owned_by`=".$this->session->userdata('user_type')." ) OR (`opportunity_owner` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`=".$this->session->userdata('affid')." and `or_m_designation`=4) AND `opportunity_owned_by`=1)");
			}
			
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/view_all_opportunity',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		public function show_opportunity()
		{
			$data['info']=$this->db->get('m35_opportunity');
			$this->load->view('CRM/view_all_opportunity',$data);
		}
		
		/*CALL sp_opportunity(1,'FIPL0116LD001/P0','2','1',0,'0.00','0','','vbb','0','0','Ferry Infotech Pvt Ltd',1,'0000-00-00 00:00:00',1,1,'','2016-01-05 17:10:36','0000-00-00 00:00:00','0000-00-00 00:00:00',)*/
		/*Convert Opportunity*/
		
		public function convert_opportunity()
		{
			$data=$this->crm_model->convert_opportunity();
			$this->load->view('CRM/view_edit_opportunity',$data);
			$this->load->view('footer_11');
		}
		
		public function add_opportunity()
		{
			$this->db->where('m_project_stauts',1);
			$data['project']=$this->db->get('m42_project');
			$this->load->view('CRM/view_opportunity',$data);
			$this->load->view('footer_11');
		}
		
		
		public function edit_opportunity()
		{
			$data=$this->crm_model->edit_opportunity();
			$this->load->view('CRM/view_edit_opportunity',$data);
			$this->load->view('footer_11');
		}
		
		public function insert_opportunity_file()
		{
			$data=$this->crm_model->insert_opportunity_file();
			echo $data;
		}
		
		public function insert_opportunity()
		{
			$data=$this->crm_model->insert_opportunity();
			echo $data;
		}
		
		public function update_opportunity()
		{
			$data=$this->crm_model->update_opportunity();
			echo $data;
		}
		
		public function conert_opp_to_acc()
		{
			$opportunity=array(
			'proc'=>3,
			'lead_id'=>0,
			'account_id'=>$this->uri->segment(3),
			'opportunity_sno'=>'P',
			'opportunity_for'=>1,
			'opportunity_website_id'=>'1',
			'opportunity_project_id'=>'1',
			'opportunity_service_id'=>'1',
			'expected_revenue'=>0.0,
			'probability'=>0.00,
			'opportunity_proposal_copy'=>'',
			'opportunity_subject'=>'',
			'opportunity_owner'=>1,
			'opportunity_owned_by'=>1,
			'opportunity_account_name'=>'',
			'opportunity_priority'=>1,
			'opportunity_close_date'=>$this->curr,
			'opportunity_type'=>1,
			'opportunity_stage'=>5,
			'opportunity_description'=>'',
			'opportunity_create_date'=>$this->curr,
			'opportunity_proposal_date'=>'0000-00-00 00:00:00',
			'opportunity_workorder_date'=>'0000-00-00 00:00:00'
			);
			$query1 ="CALL sp_opportunity(?" . str_repeat(",?", count($opportunity)-1) .",@a) ";
			$data['rec1']=$this->db->query($query1, $opportunity);
            //echo $this->db->last_query();
			$this->db->free_db_resource();
			$query1=$this->db->query("SELECT @a as message");
			$row = $query1->row();
            if($row->message!="")	
            {        
            echo $row->message."Please back and Refresh once";
             die();
            }
            else
            {
			header("location:".base_url()."crm/view_opportunity/0");
            }
		}
		
		public function view_event()
		{
			$data['curr']=$this->curr;
			$profile_id = $this->session->userdata('profile_id');
			if($this->session->userdata('user_type')==2){
				$query = " CALL sp_event() ";
			}
			else{
				$query = " CALL sp_event_emp($profile_id) ";
			}
			$data['rec']=$this->db->query($query);
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/view_all_event',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		
		public function after_view_event()
		{
			$data['curr']=$this->curr;
			$query = " CALL sp_event() ";
			$data['rec']=$this->db->query($query);
			$this->load->view('CRM/view_all_event',$data);
			$this->load->view('footer_11');
		}
		
		public function insert_event()
		{
			$data=$this->crm_model->insert_event();
			echo $data;
			
		}
		
		
		
		public function view_edit_event()
		{
			$data['id']=$this->uri->segment(3);
			$data['lead']=$this->db->get('m32_lead');
			$data['cont']=$this->db->get('m34_contact');
			$this->db->where('event_id',$this->uri->segment(3));
			$data['rec']=$this->db->get('m41_event');
			$this->load->view('CRM/view_edit_event',$data);
			$this->load->view('footer_11');
		}

		public function view_edit_call_event()
		{
			$data['id']=$this->uri->segment(3);
			$data['lead']=$this->db->get('m32_lead');
			$data['cont']=$this->db->get('m34_contact');
			$this->db->where('event_id',$this->uri->segment(3));
			$data['rec']=$this->db->get('m41_event');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/view_edit_event_cc',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function convert_lead_to_event()
		{
			if($this->session->userdata('user_id')!="")
			{
				$data['id']=$this->uri->segment(3);
				$this->db->where('lead_id',$this->uri->segment(3));
				$data['rec']=$this->db->get('m32_lead');
				$rec = $data['rec']->result()[0];
				$this->load->view('header');
				$this->load->view('menu1',$this->data1);
				$this->load->view('CRM/view_lead_event',$rec);
				$this->load->view('sidebar');
				$this->load->view('footer');
			}
		}
		
		public function update_event()
		{
			
			$data=$this->crm_model->update_event();
			echo $data;
			
		}
		
		public function create_event()
		{
			if($this->session->userdata('user_id')!="")
			{
				$this->load->view('CRM/view_event');
				$this->load->view('footer_11');
			}
		}
		
		
		
		public function taskattachment()
		{
			$data=$this->crm_model->taskattachment();
			echo $data;
		}
		
		
		//<-------------------------end task---------------->//
		//<-------------------------Create SMS Compaining---------------->//
		
		public function create_sms_campaign()
		{
			$this->db->where('m_template_category','2');
			$data['content']=$this->db->get('m39_email_temp');
			$this->db->where('m_type','2');
			$data['cate']=$this->db->get('m40_email_template_category');
			$data['info']='';
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/view_sms_companing',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		public function after_create_sms_campaign()
		{
			
			//$this->db->where('m_template_category','2');
			$data['content']=$this->db->get('m39_email_temp');
			$this->db->where('m_type','2');
			$data['cate']=$this->db->get('m40_email_template_category');
			$data['info']='';
			
			$this->load->view('CRM/view_sms_companing',$data);
			$this->load->view('footer_11');
		}
		
		
		
		public function insert_sms_temp()
		{
			$data=$this->crm_model->insert_sms_temp();
			echo $data;
		}
		
		
		public function edit_sms_campaning()
		{
			
			$this->db->where('m_template_category','2');
			$this->db->where('m_email_temp_id',$this->uri->segment(3));
			$data['info']=$this->db->get('m39_email_temp');
			$this->db->where('m_type','2');
			$data['cate']=$this->db->get('m40_email_template_category');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/view_edit_sms_campaign',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		
		public function update_companing()
		{
			
			$data=$this->crm_model->update_companing();
			echo $data;
			
		}
		
		
		
		public function view_send_sms_template()
		{
			
			
			$this->db->where('m_template_category','2');
			$data['content']=$this->db->get('m39_email_temp');
			$this->db->where('m_type','2');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/view_send_sms_template',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		
		
		public function get_mobile_no()
		{
			
			
			$sou=$this->uri->segment(3);
			$cat=$this->uri->segment(4);
			$data['info']="";
			$data['acc']="";
			$data['opp']="";
			if($sou=='1')
			{
				$data['info']=$this->db->query("SELECT * FROM m32_lead WHERE lead_status!=7");
			}
			if($sou=='2' && $cat==1)
			{
				//$data=$this->db->query("SELECT account_id FROM m34_contact WHERE is_account=1");
				$data['acc']=$this->db->query("SELECT * FROM m34_contact WHERE is_account=1");
			}
			if($sou=='2' && $cat==2)
			{
				//$data=$this->db->query("SELECT contact_id FROM m34_contact WHERE is_account=0");
				$data['opp']=$this->db->query("SELECT * FROM m34_contact WHERE is_account=0");
			}
			$this->load->view('CRM/get_mobile_no',$data);
			
		}
		
		
		
		public function send_sms($mob,$msg)
		{
			$data=$this->crm_model->send_sms($mob,$msg);
			echo $data;
		}
		
		
		//-------------------------End SMS compaining---------------------------------
		
		
		//-------------------------Get Email compaining-------------------------------
		
		
		public function view_create_email_template()
		{
			
			$aid=0;
			$this->db->where('affiliate_id',$aid);
			$this->db->where('m_template_category','1');
			$data['content']=$this->db->get('m39_email_temp');
			$this->db->where('m_type','1');
			$data['cate']=$this->db->get('m40_email_template_category');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/view_create_email_template',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function after_view_create_email_template()
		{
			
			$aid=0;
			$this->db->where('affiliate_id',$aid);
			$this->db->where('m_template_category','1');
			$data['content']=$this->db->get('m39_email_temp');
			$this->db->where('m_type','1');
			$data['cate']=$this->db->get('m40_email_template_category');
			//$this->load->view('CRM/header');
			//$this->load->view('CRM/menu');
			$this->load->view('CRM/view_create_email_template',$data);
			$this->load->view('footer_11');
			//$this->load->view('CRM/new_footer');
		}
		
		
		public function insert_email_temp()
		{
			$data=$this->crm_model->insert_email_temp();
			echo $data;
		}
		
		
		
		
		public function view_edit_email()
		{
			$aid=0;
			$id=$this->uri->segment(3);
			$data['id']=$id;
			$this->db->where('m_email_temp_id',$id);
			$this->db->where('affiliate_id',$aid);
			$data['content']= $this->db->get('m39_email_temp');
			$this->db->where('m_type','1');
			$data['cate']=$this->db->get('m40_email_template_category');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/email_edit_template',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function update_email_template()
		{
			$data=$this->crm_model->update_email_template();
			echo $data;
		}
		
		
		public function delete_email()
		{
			$data=$this->crm_model->delete_email();
			echo $data;
		}
		
		
		public function view_send_email_template()
		{
			
			$data['content']=$this->db->get('m39_email_temp');
			$this->db->where('m_type','1');
			$data['cate']=$this->db->get('m40_email_template_category');
			$data['info']='';
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/view_send_email_template',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}	
		
		
		
		public function get_contact_email()
		{
			
			
			$sou=$this->uri->segment(3);
			$cat=$this->uri->segment(4);
			$data['info']="";
			$data['acc']="";
			$data['opp']="";
			if($sou=='1')
			{
				$data['info']=$this->db->query("SELECT * FROM m32_lead WHERE lead_status!=7");
			}
			if($sou=='2' && $cat==1)
			{
				$data['acc']=$this->db->query("SELECT * FROM m34_contact WHERE is_account=1");
			}
			if($sou=='2' && $cat==2)
			{
				$data['opp']=$this->db->query("SELECT * FROM m34_contact WHERE is_account=0");
			}
			$this->load->view('CRM/get_email_id',$data);
			
		}
		
		
		
		public function email_template_display()
		{
			
			if($this->uri->segment(3)!="")
			{
				$id=$this->uri->segment(3);	
			}
			else
			{
				$id=1;
			}
			$this->db->where('category',$id);
			$data['content']=$this->db->get('m39_email_temp');
			$data['cate']=$this->db->get('m40_email_template_category');
			$this->load->view('CRM/inbox_inbox',$data);
		}
		
		
		public function get_template()
		{
			
			
			$data['content']=$this->db->get('m39_email_temp');
			$this->db->where('m_type','1');
			$data['cate']=$this->db->get('m40_email_template_category');
			$this->db->where('m_email_temp_id',$this->uri->segment(3));
			$data['info']=$this->db->get('m39_email_temp');
			$this->load->view('header');
			$this->load->view('menu');
			$this->load->view('CRM/view_send_email_template',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		
		public function send_template_mail()
		{
			
			$this->load->library('email');
			$message=$this->input->post('txtdesciption');
			$config['mailtype'] = 'html';
			$this->email->set_newline("\r\n");
			$this->email->subject($this->input->post('txtsubject'));
			$this->email->message($message);
			$this->email->from(EMAIL,'APPWORKS'); // change it to yours
			$this->email->to($this->input->post('txtto'));// change it to yours
		    if($this->email->send())
			{
			    header("Location:".base_url()."email_marketing");
			}
			else
			{
			    show_error($this->email->print_debugger());
			}
			
			
		}
		/*-------------Make A Call-----------------*/
		
		
		public function view_makecall()
		{
			$this->load->view('CRM/view_make_call');
			$this->load->view('footer_11');
		}
		
		
		
		public function make_message()
		{
			$this->db->where('m_template_category','2');
			$data['content']=$this->db->get('m39_email_temp');
			$this->db->where('m_type','2');
			$this->load->view('CRM/view_send_sms_template',$data);
			$this->load->view('footer_11');
		}
		
		public function make_email()
		{
			$data['content']=$this->db->get('m39_email_temp');
			$this->db->where('m_type','1');
			$data['cate']=$this->db->get('m40_email_template_category');
			$data['info']='';
			$this->load->view('CRM/view_send_email_template',$data);
			$this->load->view('footer_11');
		}
        
        
        public function assign_lead()
		{
			$data=$this->crm_model->assign_lead();
			$data['id']=$this->uri->segment(3);
			$data['assign_id']=$this->uri->segment(4);
			$this->load->view('CRM/view_assign_lead',$data);
			$this->load->view('footer_11');
		}
		
		public function reassign_lead_update()
		{
			$preval=$this->db->get_where('m32_lead', array('lead_id'=>$this->input->post('txtuserid')))->row();
if($preval->lead_created_by==2)
{
			$data=array(
			'lead_owner'=>$this->input->post('ddassignid'),
			'lead_created_by'=>1
			);
}
else
{
			$data=array(
						'lead_owner'=>$this->input->post('ddassignid')
			);
}
			$this->db->where('lead_id',$this->input->post('txtuserid'));
			$this->db->update('m32_lead',$data);
			if($preval->lead_owner==0)
			{
				$nam="No-one";
			}
			else
			{
				$nam=$this->db->query("SELECT GetNameById('".$preval->lead_owner."') AS nam")->row()->nam;
			}
			$nam1=$this->db->query("SELECT GetNameById('".$this->input->post('ddassignid')."') AS nam1")->row()->nam1;
			$val="Lead No. '".$preval->lead_userid."' is assign to $nam1 which was previously assign to $nam";
			$datasa=array(
			'm_ar_tblnm'=>'m32_lead/lead_owner',
			'm_ar_old_value'=>$preval->lead_owner,
			'm_ar_new_value'=>$this->input->post('ddassignid'),
			'm_ar_uid'=>$this->session->userdata('profile_id'),
			'm_ar_description'=>$val,
			'm_ar_date'=>''
			);
			$query="CALL sp_maintain_history(?" . str_repeat(",?", count($datasa)-1) .")";
			$this->db->query($query,$datasa);
			echo "Successfull";
		}
        
		
        public function export_data()
		{
			$fp = fopen($_FILES['userfile']['tmp_name'],'r') or die("can't open file");
			while($csv_line = fgetcsv($fp)) 
			{
				for ($i = 0, $j = count(fgetcsv($fp)); $i < $j; $i++) 
				{
					$insert_csv['Date'] = $csv_line[0];
					$insert_csv['Trader'] = $csv_line[1];
					$insert_csv['Exch'] = $csv_line[2];
					$insert_csv['Algo'] = $csv_line[3];
					$insert_csv['Instrument'] = $csv_line[4];
					$insert_csv['FCM']= $csv_line[5];
					$insert_csv['Size'] = $csv_line[6];
					$insert_csv['Filled'] = $csv_line[7];
					$insert_csv['Volume'] = $csv_line[8];
					$insert_csv['Passive'] = $csv_line[9];
					$insert_csv['Cleanup'] = $csv_line[10];
					$insert_csv['AP'] = $csv_line[11];
					$insert_csv['STF'] = $csv_line[12];
				}
				
				$data = array(
				'Date' =>$insert_csv['Date'],
				'Trader'=>$insert_csv['Trader'],
				'Exch'=>$insert_csv['Exch'],
				'Algo'=>$insert_csv['Algo'],
				'Instrument' =>$insert_csv['Instrument'],
				'FCM' =>$insert_csv['FCM'],
				'Size' =>$insert_csv['Size'],
				'Filled'=>$insert_csv['Filled'],
				'Volume' =>$insert_csv['Volume'],
				'Passive' =>$insert_csv['Passive'],
				'Cleanup' =>$insert_csv['Cleanup'],
				'AP' =>$insert_csv['AP'],
				'STF' =>$insert_csv['STF']
				);
				
				$this->db->insert('tr29_mock_data',$data);
			}
            header("location:".base_url()."crm/view_export_data");
			
		}
		
		public function view_export_data()
		{
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/view_upload_data');
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
        public function view_mockdata_reports()
        {
			$data['mock']=$this->db->get('tr29_mock_data');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/view_mock_data',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
        
		
		///////////////////////////////////////MAILS COLLECT////////////////////////////////////////////		
		
		public function get_mail_inbox()
		{       
			
			
			$row = array();
			$reg_id =  $this->session->userdata('profile_id');
			$row =  $this->db->select('lead_title')->get_where('m32_lead',['lead_owner'=>$reg_id])->result_array();         	
			$subjects = [];
			if(!empty($row)){ 
				foreach($row as $value)
				$subjects[] = str_replace("'",'',trim($value['lead_title']));
			}   
			
			
			// $inbox = (include APPPATH.'third_party/getmail/browse_mailbox.php');
			
			$inbox = $this->browse_mailbox();
			$rwData = array();
			if(!empty($inbox)){
			    $ii=0;
				foreach($inbox as $key=>$value){
					$ii++;
					$user_lead = $this->db->query("SELECT GetAutoId(3) as userid")->row()->userid.$ii.uniqid();
					if(!in_array(str_replace("'",'',$value['lead_title']),$subjects)){
						$rwData[] =  "(".implode(',' ,array_merge( [ 
						'lead_userid'=>"'".$user_lead."'",
						'lead_owner'=> $reg_id ,
						'lead_status'=>7,
						],$value)).")";               
					}
				}
				if(!empty($rwData)){	   
					$rw = implode(',',$rwData);	
					$this->db->query( "INSERT INTO `m32_lead`   
					(
					`lead_userid`,
					`lead_owner`,
					`lead_status`,
					
					`lead_mobile`,
					`lead_address`,
					
					`message_title`,
					`lead_title`,
					`lead_email`,
					`lead_description`,
					`lead_modidate`,
					`lead_regdate`
					)
					VALUES {$rw}");
				}
				
			}
		}
		
		public function requires($file_name)
		{
			require( APPPATH."third_party/getmail/{$file_name}.php");
		}
		
		/**
			* NOTE : Becareful use this function browse_mailbox
			// ------------------------------------------------------------------------
			
			/**
			* browse_mailbox library this function
			*
			* This function enables for inbox  get 
			*
		*/	
		public function browse_mailbox() 
		{ 
			
			$this->requires('mime_parser');
			$this->requires('rfc822_addresses');
			$this->requires("pop3");
			stream_wrapper_register('mlpop3', 'pop3_stream');  /* Register the pop3 stream handler class */
			
			$pop3=new pop3_class;
			$pop3->hostname=POP3_HOST;               /* POP 3 server host name   */
			$pop3->port=POP3_PORT;                   /* POP 3 server host port,
				usually 110 but some servers use other ports
			Gmail uses 995                              */
			$pop3->tls=1;                            /* Establish secure connections using TLS      */
			$user=PO3_EMAIL;                        /* Authentication user name  */
			$password=POP3_PWD;                      /* Authentication password                 */
			$pop3->realm="";                         /* Authentication realm or domain              */
			$pop3->workstation="";                   /* Workstation for NTLM authentication         */
			$apop=0;                                 /* Use APOP authentication                     */
			$pop3->authentication_mechanism="USER";  /* SASL authentication mechanism               */
			$pop3->debug=0;                          /* Output debug information                    */
			$pop3->html_debug=0;                     /* Debug information is in HTML                */
			$pop3->join_continuation_header_lines=1; /* Concatenate headers split in multiple lines */
			$data = array();
			
			if(($error=$pop3->Open())=="")// chaech connection
			{
				if(($error=$pop3->Login($user,$password,$apop))=="") // check login
				{
					
					if(($error=$pop3->Statistics($messages,$size))=="") // count total message;
					{
						if($messages>0)
						{
							$pop3->GetConnectionName($connection_name);
							
							$num = $messages > 10 ? 15 : $messages; /*No of mails increase here*/
							
							for($i=0; $i<= $num; $i++){ 					
								$message= $messages - $i ;
								
								$message_file='mlpop3://'.$connection_name.'/'.$message;
								$mime=new mime_parser_class;
								
								/*
									* Set to 0 for not decoding the message bodies
								*/
								$mime->decode_bodies =1;
								
								$parameters =  $this->parameters($message_file); 
								$success = $mime->Decode($parameters, $decoded);
								if ($success)
								{
									if ($mime->Analyze($decoded[0], $results))
									{
										
										if (($results['From'][0]['name']) == 'IndiaMART Enquiry')
										{	 
											$exp = $this->break_fields($this->clean($results['Alternative'][0]['Data']));
											if($exp){
												$descrip = $this->clean($exp[0]);	 
												$address= $this->clean($exp[1]);
												$mobile = $this->clean($exp[2]);
												$email = $this->clean($exp[3]);
												$email = $this->clean( str_replace('Buyers Requirement Details','',$email ));
												
												if(preg_match('/@.+\./', $email))
												$email = $email;
												else 
												$email = '';
												
												
												$data[] =array(
												
												'mobile'=>"'".str_replace('Email','',$mobile)."'",
												'address'=>"'".str_replace('Mobile','',$address)."'",
												'header'=>"'".$results['From'][0]['name']."'",
												'lead_title' =>"'".$this->clean($results['Subject'])."'",
												'lead_email'=>"'".$email."'", 
												'lead_description'=>"'".$descrip."'",
												'lead_modidate'=>"'".date("Y-m-d h:m:s", strtotime($results['Date']))."'",
												'lead_regdate' => "'".date('Y-m-d h:i:s')."'"
												);
												// print_r($data);
											}
										}
									}
								}
							}
							//print_r($data);
						}
						
						return $data;
					}
				}
			}
		}
		
		// breaked fields	
		public 	function break_fields($fields)
		{
			$d = explode(':',$fields);
			return $d;
		}
		
		// break add parameters
		public function parameters($message_file)
		{
			
			$param = array('File'=>$message_file,
			/* Read a message from a string instead of a file */
			/* 'Data'=>'My message data string',              */
			
			/* Save the message body parts to a directory     */
			/* 'SaveBody'=>'/tmp',                            */
			
			/* Do not retrieve or save message body parts     */
			'SkipBody'=>'',
			);
			return $param;
		}
		// filter for spacail charactor delete
		public function clean($string) 
		{  
			$string = str_replace("'", '', $string);
			$string = str_replace('"', '', $string); // Replaces all spaces with hyphens.
			return preg_replace('/[^A-Za-z0-9\-@_:,][ ]/', '', $string);// Removes special chars.
		}
		
		////////////////// UPDATE MAIL INFORAMATION\\\\\\\\\\\\\\\\\\\\
		public function mail_config()
		{  
			
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('master/mail_config');
			$this->load->view('sidebar');
			$this->load->view('footer');        
		}
		
		
		public function update_mail_config()
		{
			$host = $this->input->post('txthost_name');
			$root = $this->input->post('txtportadd');
			$email = $this->input->post('txtpopemail');
			$pwd = $this->input->post('txtpwd1');
			
			$data = array('m00_pop3_host'=>$host,'m00_pop3_port'=>$root,'m00_pop3_email'=>$email,'m00_pop3_pwd'=>$pwd,);
			
			$this->db->where('m00_id',1)->update('m00_setconfig',$data);
			
			redirect('configuration');
		}
		
		
		public function  get_mail_records()
		{
			$res =   $this->get_mail_inbox();
			if($res == null)
		    echo  "<script>alert('Mail Setting is Invalid ')</script>";
			else 
		    echo "<script>alert('$res')</script>"; 
			
			redirect('CRM/mail_config','refresh');   
		}
		
        /*----------------- Manage Account Detail --------------------*/ 
		public function update_account_detail()
		{
			
			$data=array(
			'account_name'=>$this->input->post('txtname'),
			'account_phone'=>$this->input->post('txtmobile'),
			'account_state'=>$this->input->post('ddstate'),
			'account_city'=>$this->input->post('ddcity'),
			'account_address'=>$this->input->post('txtaddress'),
			'account_zipcode'=>$this->input->post('txtpincode')
			);
			$this->db->where('account_id',$this->uri->segment(3));
			$this->db->update('m33_account',$data);
			header("location:".base_url()."crm/view_account_profile/".$this->uri->segment(4));
		}
		
        
		public function update_contact_detail()
		{
			
			$data=array(
			'contact_name'=>$this->input->post('txtcontname'),
			'contact_mobile'=>$this->input->post('txtcontmobile'),
			'contact_state'=>$this->input->post('ddcontstate'),
			'contact_city'=>$this->input->post('ddcontcity'),
			'contact_address'=>$this->input->post('txtcontaddress'),
			'contact_zipcode'=>$this->input->post('txtcontpincode')
			);
			$this->db->where('contact_id',$this->uri->segment(3));
			$this->db->update('m34_contact',$data);
			header("location:".base_url()."crm/view_account_profile/".$this->uri->segment(4));
		}
		
		/*---------------------- End Manage Account Detail ------------------*/
		
		
		/*-----------------------Manage User Profile ------------------------*/ 
		
		public function view_user_profile()
		{
			$data = $this->crm_model->view_user_profile();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/view_userprofile',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		
		public function update_profile_details()
		{
			$data=$this->crm_model->update_profile_details();
            echo $data;
		}
		
		
		public function change_profile_image()
		{
			$this->crm_model->change_profile_image();
			header("Location:".base_url()."crm/view_user_profile");
		}
		
		
		public function update_profile_password()
		{
			$data=$this->crm_model->update_profile_password();
            echo $data;
		}	
		
        /*---------------------- End Manage User Profile ------------------*/
		
		
		/*----------------------- Start Manage Lead Followup ----------------*/
		
        public function view_lead_history()
		{
			$p_id=$this->uri->segment(3);
			$data['task_id']=$this->uri->segment(3);
			$taskdata=array(
			'proc'=>2,'enquiry_id'=>0,'enq_foll_response_by'=>'','enq_foll_description'=>'','enq_foll_response_date'=>'','lead_foll_usertype'=>$this->session->userdata('user_type'),'querey'=>'lead_id='.$p_id
			);
			$query = "CALL sp_lead_followup(?" . str_repeat(",?", count($taskdata)-1) .") ";
			$data['rec']=$this->db->query($query,$taskdata);
			$this->db->free_db_resource();
            
			$this->load->view('header');
			$data['module']='crm';
			$this->load->view('menu1',$this->data1);
			$this->load->view('CRM/view_lead_history',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		/* Follow Up on Task assign on User.*/
		public function insert_lead_follow_up()
		{
			$task_no=$this->uri->segment(3);
			$task_reply=$this->input->post('task_description');
			$taskdata=array(
			'proc'=>1,
            'enquiry_id'=>$task_no,
            'enq_foll_description'=>$task_reply,
            'enq_foll_response_by'=>$this->session->userdata('profile_id'),
            'enq_foll_response_date'=>$this->curr,
            'lead_foll_usertype'=>$this->session->userdata('user_type'),
            'querey'=>'1'
			);
			$query = "CALL sp_lead_followup(?" . str_repeat(",?", count($taskdata)-1) .") ";
			$data['rec']=$this->db->query($query,$taskdata);
			$this->db->free_db_resource();
			echo 'true';
			//header("Location:".base_url()."index.php/crm/view_lead_history/$task_no");
		}
		/*----------------------- End Manage Lead Followup ----------------*/
		
		/*------------------------- Send Task Mail -------------------------*/
		
        public function send_task_mail()  
        {
            $data=$this->db->query("CALL sp_due_task()");
			$this->load->library('email');
            foreach($data->result() AS $row)
            {
				$message="Dear ".$row->task_assignto_name."
				<br/> 
				This is to inform, that your coordination is required in resolving the Task assigned to you.
				<br/> You have ".$row->task_status." task which priority is ".$row->task_priority." to ".$row->type." to ".$row->name." 
				<br/>
				Thank You.";
				$config['mailtype'] = 'html';
				$this->email->set_newline("\r\n");
				$this->email->subject("TechAarjavam Sales Task".$row->subject);
				$this->email->message($message);
				$this->email->from(EMAIL,'APPWORKS'); // change it to yours
				$this->email->to($row->email_id);// change it to yours
			    if($this->email->send())
				{
				    echo $this->email->send();
                    echo $row->email_id;
				}
				else
				{
				    show_error($this->email->print_debugger());
				}
			}
		}
		/*-------------------------- Send Task Mail -------------------------*/
		
	}
?>