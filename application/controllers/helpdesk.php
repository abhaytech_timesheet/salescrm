<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Helpdesk extends CI_Controller {
		
		
		public function __construct() 
		{
			parent::__construct();
			$timezone = new DateTimeZone("Asia/Kolkata");
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
			$this->_is_logged_in();
		}
		
		/*Session Check Function */ 
		public function _is_logged_in() 
		{
		    $is_logged_in = $this->session->userdata('profile_id');
			if ($is_logged_in=="") 
			{
				redirect('auth/index/1');
				die();
			}
		}
		public function index()
		{
			$data=$this->help_model->get_submit_ticket_detail();
			$this->load->view('Admin/header');
			$this->load->view('Admin/menu');
			$this->load->view('Help_Desk/index',$data);
			$this->load->view('Admin/footer');	
		}
		public function view_ticket()
		{
            $data=[];
			$p_id=$this->uri->segment(3);
            $query=$this->db->query("SELECT GetTicketExist('$p_id') as rc");
            $row2 = $query->row();
            $data1['is_ticexist']=$row2->rc;
            if($row2->rc==1)
			{
			$data=$this->help_model->view_ticket($p_id);
            }
			$this->load->view('header');
			$this->load->view('menu',$data1);
			$this->load->view('Admin/view_ticket',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			}
		public function after_index()
		{
			$data=$this->help_model->get_submit_ticket_detail();
			$this->load->view('Help_Desk/index',$data);
		}
		public function assign()
		{
			$affid=$this->session->userdata["affid"];
			$data=$this->help_model->assign($affid);
			$this->load->view('Help_Desk/assign_ticket',$data);
		}
		/*Affiliate Wise Submit Ticket Assign to their employees according their designation*/ 
		public function task_assign()
		{
			$response=$this->help_model->task_assign();
			echo $response;	
		}
		//Reassign Ticket 
		public function view_reassign()
		{
			$affid=$this->session->userdata["affid"];
			$data=$this->help_model->aff_emp($affid);
           // echo $this->db->last_query();
             //die();
			$data['id']=$this->uri->segment(3);
			$data['assign_id']=$this->uri->segment(4);
			$data['tickt_type']=$this->uri->segment(5);
			$this->load->view('Help_Desk/reassign',$data);
			
		}
		public function task_again_assign()
	    {
			$response=$this->help_model->task_assign();
			echo $response;	
		}
		
	
		
		
	}
?>