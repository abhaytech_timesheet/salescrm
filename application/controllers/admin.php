<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Admin extends CI_Controller 
	{
		public function __construct() 
		{
			parent::__construct();
		    $timezone = new DateTimeZone("Asia/Kolkata");
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
			$this->_is_logged_in();
			$this->data1=$this->crm_model->assign_menu();
		}
		
		/*Custom Function here*/
		
		/*Session Check Function */ 
		public function _is_logged_in() 
		{
		    $is_logged_in = $this->session->userdata('profile_id');
			if ($is_logged_in=="") 
			{
				redirect('auth/index/1');
				die();
			}
		}
		
		/*Index Function */
		/*### {admin/index} View All Ticket ####*/
		public function index()
		{
			$data=$this->help_model->get_submit_ticket_detail();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Help_Desk/index',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function after_index()
		{
			$data=$this->help_model->get_submit_ticket_detail();
			$this->load->view('Help_Desk/index',$data);
			$this->load->view('footer_11');
		}
		
		/*Affiliate Wise Submit Ticket Assign to their employees according their designation*/ 
	    public function task_assign()
		{
			$data=$this->help_model->task_assign();
			echo $data;
		}
		
		/*Just Replace action name on controller */
		
		/*### {admin/view_assign_ticket} Assign Ticket ####*/
		
		public function view_assign_ticket()
		{
			$affid=$this->session->userdata["affid"];
			$data=$this->help_model->assign($affid);
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Help_Desk/assign_ticket',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function view_ticket()
		{
            $data=[];
			$p_id=$this->uri->segment(3);
            $query=$this->db->query("SELECT GetTicketExist('$p_id') as rc");
            $row2 = $query->row();
            $data1['is_ticexist']=$row2->rc;
            if($row2->rc==1)
			{
				$data=$this->help_model->view_ticket();
			}
            $data=array_merge($data1,$data);
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Admin/view_ticket',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		/*Search Ticket*/
		public function search_ticket()
		{
			$data=$this->help_model->ticket_search();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Help_Desk/search_ticket_res',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		/*According Employee Wise Ticket Report*/
		public function employee_ticket_report()
		{
			$data=$this->help_model->ticket_search_report();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Help_Desk/emp_tkt_report',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		
		/* ----------------Generate Ticket--------------- */	
		public function ticket()
		{
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Admin/ticket_type');
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function submit_ticket()
		{
			$p_id=$this->uri->segment(4);
			$this->db->where('account_id',$p_id);
			$data['rec']=$this->db->get('m33_account');
			$dep=$this->uri->segment(3);
			$data['dep']=$dep;
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Admin/submit_ticket',$data);
			$this->load->view('footer');
		}
		/*--------------------REPLY BY Admin--------------------*/
		public function Admin_response()
		{
		    $data=$this->help_model->response();
			$tic_no=$this->input->post('txtticket');
			header("Location:".base_url()."admin/view_ticket/$tic_no");	
		}
		
		
		/* ----------------Resolve Problem on btn Click--------------- */
		public function resolve()
		{
			$data=$this->help_model->resolve();
			$ticket_no=$this->input->post('ticket_no');	
			header("Location:".base_url()."admin/view_ticket/$ticket_no");
		}
		
		
		
		
		
		/* ----------------Announcements View ,Update, Delete, Edit--------------- */	
		public function announcements()
		{
			$data['act']='anouncement';
			$data['rec']=$this->db->get('tr19_anoncement');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Admin/announcements',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		
		public function announce_delete()
		{
			
			$id=$this->uri->segment(3);
			$data1=array(
			'txtstatus'=>0
			);	  
			$this->db->where('id',$id);
			$this->db->update('tr19_anoncement',$data1);
			
			header("Location:".base_url()."admin/announcements");
			
		}
		
		public function announce_enable()
		{
			$id=$this->uri->segment(3);
			$data1=array(
			'txtstatus'=>1
			);	  
			$this->db->where('id',$id);
			$this->db->update('tr19_anoncement',$data1);
			header("Location:".base_url()."admin/announcements");
			
		}
		
		public function edit_anouncement()
		{
			$data['id']=$this->uri->segment(3);
			$id=$this->uri->segment(3);
			$this->db->where('id',$id);
			$data['rec']=$this->db->get('tr19_anoncement');
			$data['rec1']=$this->db->get('tr19_anoncement');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Admin/edit_anouncement',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
	}
?>																	