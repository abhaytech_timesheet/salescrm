<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Support extends CI_Controller {
		
		public function __construct() 
		{
			parent::__construct();
			$this->_is_logged_in();
			$this->load->model('Support_model');
			$this->load->model('project_model');
			$this->load->model('crm_model');
		}
		
		public function _is_logged_in() 
		{
			$is_logged_in = $this->session->userdata('profile_id');
			if ($is_logged_in==""  || $is_logged_in=="0") 
			{
				redirect('auth/index/1');
				die();
			}
		}
		
		public function dashboard()
		{
			$data=$this->Support_model->get_submit_ticket_detail();
			$data1=$this->Support_model->assign_menu();
			$this->load->view('header');
			$this->load->view('support/menu',$data1);
			$this->load->view('support/index',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		/*Support Menu */
		public function index()
		{
			$data1=$this->Support_model->assign_menu();
			$data=$this->Support_model->get_submit_ticket_detail();
			$this->load->view('header');
			$this->load->view('support/menu',$data1);
			$this->load->view('support/index',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function view_ticket()
		{
            $data=[];
			$data1=$this->Support_model->assign_menu();
			$p_id=$this->uri->segment(3);
            $query=$this->db->query("SELECT GetTicketExist('$p_id') as rc");
            $row2 = $query->row();
            $data1['is_ticexist']=$row2->rc;
            if($row2->rc==1)
			{
		  
			$data=$this->Support_model->view_ticket(); 
            }
			$this->load->view('header');
			$this->load->view('support/menu',$data1);
			$this->load->view('support/view_ticket',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		/*--------------------REPLY BY Affiliates--------------------*/
		public function affiliates_response()
		{
			$tic=$this->Support_model->affiliates_response();
			header("Location:".base_url()."support/view_ticket/$tic");
		}
		
		
		/* ----------------Resolve Problem on btn Click--------------- */
		public function resolve()
		{
			$ticket_no=$this->Support_model->resolve();
			header("Location:".base_url()."support/view_ticket/$ticket_no");
		}
		
		/*----------------------Notification Area-----------------------*/
		public function announcements()
		{
			$data1=$this->Support_model->assign_menu();
			$this->db->free_db_resource();
			$data=$this->Support_model->announcements();
			$this->load->view('header');
			$this->load->view('support/menu',$data1);
			$this->load->view('support/Announcements',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		public function full_Announcements()
		{
			$data1 = $this->Support_model->assign_menu();
			$this->db->free_db_resource();
			$data=$this->Support_model->full_Announcements();
			$this->load->view('header');
			$this->load->view('support/menu',$data1);
			$this->load->view('support/full_Announcements',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		/*----------------------Customer Relationship Management----------------------------*/
		public function view_lead()
		{
			$data1 = $this->Support_model->assign_menu();
			$this->db->free_db_resource();
			$data=$this->Support_model->view_lead();
			$this->load->view('header');
			$this->load->view('support/menu',$data1);
			$this->load->view('CRM/view_all_lead',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		public function view_opportunity()
		{
			$data1 = $this->Support_model->assign_menu();
			$data = $this->Support_model->view_opportunity();
			$this->load->view('header');
			$this->load->view('support/menu',$data1);
			$this->load->view('CRM/view_all_opportunity',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function view_account()
		{
			$data1 = $this->Support_model->assign_menu();
			$data = $this->crm_model->view_account();
			$this->load->view('header');
			$this->load->view('support/menu',$data1);
			$this->load->view('CRM/view_all_account',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		public function view_contact()
		{
			$data1 = $this->Support_model->assign_menu();
			$data=$this->crm_model->view_contact();
			$this->load->view('header');
			$this->load->view('support/menu',$data1);
			$this->load->view('CRM/view_all_contact',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function view_task()
		{		    
			$data1 = $this->Support_model->assign_menu();
		    $data=$this->crm_model->show_task();
			$this->db->free_db_resource();
			$st=$this->uri->segment(3);
			$this->load->view('header');
			$this->load->view('support/menu',$data1);
			$this->load->view('CRM/view_all_task',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function project()
		{
			$data1 =$this->Support_model->assign_menu();
			$data = $this->Support_model->project();
			$this->load->view('header');
			$this->load->view('support/menu',$data1);
			$this->load->view('support/project',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		
		public function project_cost()
		{
			$data1 = $this->Support_model->assign_menu();
			$data = $this->Support_model->project_cost();
			$this->load->view('header');
			$this->load->view('support/menu',$data1);
			$this->load->view('support/project_cost',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		
		public function update_project_cost()
		{
			$this->Support_model->update_project_cost();
			header("Location:".base_url()."support/project/".$this->uri->segment(3));		
			
		}	
		
		
		public function payment()
		{
			$data1 = $this->Support_model->assign_menu();
			$data= $this->Support_model->payment();
			$this->load->view('header');
			$this->load->view('support/menu',$data1);
			$this->load->view('support/payment',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		
		public function select_category()
		{
			$data= $this->Support_model->payment();
			echo $data;
		}
		
		
		public function insert_payment()
		{
			$this->Support_model->insert_payment();
			header("Location:".base_url()."support/project_cost/".$this->uri->segment(3));
		}
		
		
		public function update_pay()
		{     
			$data=$this->Support_model->insert_payment();
			echo $data;
			
		}
		
		
		//<-------------------------end task---------------->//
		
		public function view_services()
		{
			$data1 = $this->Support_model->assign_menu();
			$data = $this->Support_model->view_services();
			$this->load->view('header');
			$this->load->view('support/menu',$data1);
			$this->load->view('support/view_services_form',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		public function insert_service()
		{
			$this->Support_model->insert_service();
			header("Location:".base_url()."support/view_services/".$this->uri->segment(3));
			
		}
		
		
		public function service_payment()
		{
			
			$data1=$this->Support_model->assign_menu();
			$data=$this->Support_model->service_payment();
			$this->load->view('header');
			$this->load->view('support/menu',$data1);
			$this->load->view('support/service_payment',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		
		public function insert_service_payment()
		{
			$this->Support_model->insert_service_payment();
			header("Location:".base_url()."support/view_services/".$this->uri->segment(3));
			
		}
		
		
		/*---------------This Action is Use For Profile Menu-----------------*/
		
		public function view_user_profile()
		{
			$data1 = $this->Support_model->assign_menu();
			$data = $this->Support_model->view_user_profile();
			$this->load->view('header');
			$this->load->view('support/menu',$data1);
			$this->load->view('Support/view_userprofile',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		public function after_profile_update()
		{
			$data = $this->Support_model->after_profile_update();
			$this->load->view('Support/view_userprofile',$data);
		}
		
		public function change_profile_image()
		{
			$this->Support_model->change_profile_image();
			header("Location:".base_url()."support/view_user_profile");
		}
		
		
		public function update_profile_details()
		{
			$data=$this->Support_model->update_profile_details();
		    header("Location:".base_url()."support/view_user_profile");
		}
		
		
		public function update_profile_password()
		{
			$data=$this->Support_model->update_profile_password();
			header("Location:".base_url()."support/view_user_profile");
		}	
		
		/*-------------------Account panel---------------------*/
		public function view_all_project()
		{			
			$data1=$this->Support_model->assign_menu();
			$query = "CALL sp_view_all_project('`tr22_project_participants`.`parti_participants_id`=".$this->session->userdata('profile_id')."')";			
			$data['rec']=$this->db->query($query);
			$this->load->view('header');
			$this->load->view('support/menu',$data1);
			$this->load->view('support/view_all_project',$data);
			$this->load->view('footer');
		}
		public function after_view_all_task()
		{
			$query = "CALL sp_view_project_task(".$this->session->userdata('profile_id').")";
			$data['rec']=$this->db->query($query);
			$this->load->view('Support/view_all_task',$data);
			$this->load->view('footer_init');
		}
		public function view_create_task()
		{
			if($this->session->userdata('user_id')!="")
			{	
				$this->db->where('PARICIPANT_USERID',$this->session->userdata('profile_id'));
				$this->db->where('ACCP_STATUS',1);
				$data['proj'] = $this->db->get('view_project_details');
				$this->load->view('Support/view_create_task',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/support/logout");
			}
		}
		
		public function get_task_assign()
		{
			$query=$this->db->query("SELECT
					`tr22_project_participants`.`parti_participants_id`	AS 	`USER_ID`,
					`m06_user_detail`.`or_m_name`						AS 	`USER_NAME`
						FROM
					`admin_metro_new`.`tr22_project_participants`
					LEFT JOIN `admin_metro_new`.`m06_user_detail` 
					ON (`tr22_project_participants`.`parti_participants_id` = `m06_user_detail`.`or_m_reg_id`) 
					WHERE `tr22_project_participants`.`parti_project_id`='".$this->input->post('txtprojectid')."' AND `tr22_project_participants`.`parti_designation`>=(SELECT `parti_designation` FROM `tr22_project_participants` WHERE `tr22_project_participants`.`parti_participants_id`=".$this->session->userdata('profile_id').")");
			$json=json_encode($query->result());
			echo $json;
		}
		
		public function get_task_follower()
		{
			$query=$this->db->query("SELECT
					`tr22_project_participants`.`parti_participants_id`	AS 	`USER_ID`,
					`m06_user_detail`.`or_m_name`						AS 	`USER_NAME`
						FROM
					`admin_metro_new`.`tr22_project_participants`
					LEFT JOIN `admin_metro_new`.`m06_user_detail` 
					ON (`tr22_project_participants`.`parti_participants_id` = `m06_user_detail`.`or_m_reg_id`) 
					WHERE `tr22_project_participants`.`parti_project_id`='".$this->input->post('txtprojectid')."' AND `tr22_project_participants`.`parti_designation`<(SELECT `parti_designation` FROM `tr22_project_participants` WHERE `tr22_project_participants`.`parti_participants_id`=".$this->input->post('ddassignto').")");
			$json=json_encode($query->result());
			echo $json;
		}
		
		public function insert_project_task()
		{
			$this->project_model->insert_project_task();
		}
		/*---------------------------Upload Task file-----------------------------*/
		
		public function uploadfile1()
		{
			$config['upload_path']   =   "application/taskdoc/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "1960";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$fileupload=($finfo['raw_name'].$finfo['file_ext']);
			echo $fileupload;
		}
		
		/*-------------Project Management Module Start Here----------------------  */
		
		/*-------------Project Management Module End Here------------------------  */
		
		public function logout()
		{
			$this->load->helper('url');
			$this->load->library('session');
			$this->session->unset_userdata('profile_id');
			$this->session->unset_userdata('user_id');
			$this->session->unset_userdata('e_email');
			$this->session->unset_userdata('name');
			$this->session->unset_userdata('designation');
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
			$data['info']=0;
			$this->load->view('auth/header');
			$this->load->view('auth/login',$data);
			$this->load->view('auth/footer');
		}
	}
?>																																																																								