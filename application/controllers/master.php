<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Master extends CI_Controller {
		
		public function __construct() 
		{
			parent::__construct();
            $timezone = new DateTimeZone("Asia/Kolkata" );
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
			$this->_is_logged_in();
			$this->load->model('mastermodel');
            $this->data1=$this->crm_model->assign_menu();
		}
		
		public function _is_logged_in() 
		{
		    $is_logged_in = $this->session->userdata('profile_id');
			if ($is_logged_in=="") 
			{
				redirect('auth/index/1');
				die();
			}
		}
		
		public function index()
		{
			
			header("Location:".base_url()."master/view_mainconfig");
			
		}
		
		//Config
		public function view_mainconfig()
		{
			$data['form_name']="Here you can set Configuration";
			$data['config']= $this->mastermodel->select_config();
			$this->load->view('header',$data);
			$this->load->view('menu1',$this->data1);
			$this->load->view('Master/view_mainconfig',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		public function update_mainconfig($param='')
		{
			
			$data['form_name']="Here you can set Configuration";
			$this->mastermodel->update_config($param);
			header("Location:".base_url()."master/view_mainconfig#tab_1_1_".$param);
		}
		
		//Manage City Add,Update,Status Change Function 
		public function view_city()
		{
			$data['form_name']="Manage City";
			$data['city']=$this->mastermodel->select_state(1);
			$data['allcity']=$this->mastermodel->select_state(0);
			$this->load->view('header',$data);
			$this->load->view('menu1',$this->data1);
			$this->load->view('Master/view_city',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}	
		public function select_city()
		{
			$data['city']=$this->mastermodel->select_state($this->uri->segment(3));
			$this->load->view('Master/view_select_city',$data);
			
		}
		public function select_aff_city()
		{
			$data['city']=$this->mastermodel->select_state($this->uri->segment(3));
			$data['city_id']=$this->uri->segment(4);
			$this->load->view('Master/view_select_branch_city',$data);
			
		}
		public function add_city()
		{
			$this->mastermodel->add_city();
			header("Location:".base_url()."master/view_city");
			
		}
		
		public function update_status_city()
		{
			$this->mastermodel->update_status_city();
			header("Location:".base_url()."master/view_city");
			
		}
		
		
		public function edit_city()
		{
			$data['form_name']="Manage City";
			$id=$this->uri->segment(3);
			$data['id']=$id;	
			$data['edit_city']=$this->mastermodel->select_city($id);
			$data['city']=$this->mastermodel->select_state(1);
			$data['allcity']=$this->mastermodel->select_state(0);
			$this->load->view('header',$data);
			$this->load->view('menu1',$this->data1);
			$this->load->view('Master/view_edit_city',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		public function update_city()
		{
			$id=$this->uri->segment(3);
			$data['id']=$id;
			$this->mastermodel->update_city($id);
			header("Location:".base_url()."master/edit_city/$id/");
			
		}
		//End City Master All Action related 
		
		
		
		/*--------------------------Start News Master--------------------------*/
		
		public function view_news()
		{
			$data=$this->mastermodel->view_news();
			$this->load->view('header',$data);
			$this->load->view('menu1',$this->data1);
			$this->load->view('master/view_news',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		public function add_news()
		{
			$data=$this->mastermodel->add_news();
			$this->load->view('header',$data);
			$this->load->view('menu1',$this->data1);
			$this->load->view('master/add_news');
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		
		public function insert_news()
		{
			$data=$this->mastermodel->insert_news();
			header("Location:".base_url()."Master/add_news");
			
		}
		
		
		public function edit_news()
		{
			$data=$this->mastermodel->edit_news();
			$this->load->view('header',$data);
			$this->load->view('menu1',$this->data1);
			$this->load->view('master/view_edit_news',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function update_news()
		{
			$this->mastermodel->update_news();
			header("Location:".base_url()."Master/view_news");
		}
		
		public function delete_news()
		{
			$this->mastermodel->delete_news();
			header("Location:".base_url()."Master/view_news");
		}
		
		/*--------------------------End News Master--------------------------*/
		
		public function account()
		{
			$data=$this->mastermodel->account();
			$this->load->view('header',$data);
			$this->load->view('menu1',$this->data1);
			$this->load->view('Master/accounts',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function account_load()
		{
			$data=$this->mastermodel->account_load();
			$this->load->view('Master/accounts_load',$data);
		}
		
		public function insert_account()
		{
			$data=$this->mastermodel->insert_account();
			echo $data;
		}
		
		
		/*--------------------View App And Sub App---------------------------*/
		
		public function view_add_apps()
		{
			$data=$this->mastermodel->view_add_apps();
			$this->load->view('header',$data);
			$this->load->view('menu1',$this->data1);
			$this->load->view('Master/view_add_apps_subapps',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		/*--------------------View Sub App---------------------------*/
		
		public function view_add_subapps()
		{
			$data['parent_id']=$this->uri->segment(3);
			$this->load->view('Master/view_add_subapps',$data);
			
		}
		
		/*--------------------Add App And Sub App---------------------------*/
		
		public function insert_apps()
		{
			$data=$this->mastermodel->insert_apps();
			redirect('master/view_add_apps');
		}
		
		/*--------------------Disable Apps---------------------------*/
		
		
		public function view_change_status_apps()
		{
			$data=$this->mastermodel->view_change_status_apps();
			$this->load->view('header',$data);
			$this->load->view('menu1',$this->data1);
			$this->load->view('Master/view_stautus_apps_subapps',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		public function view_edit_apps()
		{
			$data=$this->mastermodel->view_edit_apps();
			$this->load->view('Master/view_edit_apps_subapps',$data);
			$this->load->view('footer_11');
		}
		
		public function update_apps()
		{
			$this->mastermodel->update_apps();
			redirect('master/view_change_status_apps');
		}
		
		public function update_status_apps()
		{
			$this->mastermodel->update_status_apps();
			redirect('master/view_change_status_apps');
		}
		
		public function update_menu()
		{
			$data=$this->mastermodel->update_menu();
			echo $data;
		}
		
		/*--------------------View App And Sub App---------------------------*/
		
		public function view_user_assign()
		{
			$this->load->view('header');
			$this->load->view('menu');
			$this->load->view('Master/view_assign_user');
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function check_user()
		{

			/*$data=$this->db->query("SELECT GetUseridByEmail('".$this->input->post('emailid')."',".$this->session->userdata('affid').") AS user_reg_id")->row();
			echo $data->user_reg_id;
            $datass=$this->db->query("SELECT IFNULL(`m06_user_detail`.`or_m_designation`,0) AS desig  FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_reg_id`=".$data->user_reg_id." ");
            if($datass->num_rows()>0)
            {
            echo "/".$datass->row()->desig;
            }*/
echo 1;
		}
		public function view_assign_apps()
		{
			$data=$this->mastermodel->view_add_apps();
			$data['user_mail']=$this->input->post('emailid');
			/*$this->load->view('header',$data);
			$this->load->view('menu1',$this->data1);
			$this->load->view('Master/view_assign_apps',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');*/
			$this->load->view('Master/view_assign_apps',$data);
			$this->load->view('footer_11');
		}
		
		/*--------------------Assign Apps to user---------------------------*/
		
		public function assign_apps_to_user()
		{
			$data=$this->mastermodel->assign_apps_to_user();
			header("Location:".base_url()."app_allocation");
		}
		
		/*--------------------View Assign App And Sub App---------------------------*/
		
		public function view_assign_apps_status()
		{
			$data=$this->mastermodel->view_assign_apps_status();
			$this->load->view('header',$data);
			$this->load->view('menu1',$this->data1);
			$this->load->view('Master/view_assign_apps_status',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		/*--------------------Update Assign App And Sub App---------------------------*/
		
		public function update_assign_status_apps()
		{
			$data=$this->mastermodel->update_assign_status_apps();
			header("Location:".base_url()."master/view_assign_apps_status");
		}
		
		public function validateUser()
		{
			$this->load->model('users_records');
			$this->users_records->introducerid();
		}

/*11/04/2016 */
/*--------------------Manage Services---------------------------*/
		
		public function manage_services()
		{
			$data['services']=$this->db->get('view_service');
			$this->load->view('header',$data);
			$this->load->view('menu1',$this->data1);
			$this->load->view('Master/view_manage_services',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function add_service()
		{
			$this->mastermodel->add_service();
			header("Location:".base_url()."master/manage_services");
		}
		
		public function update_service()
		{
			$this->mastermodel->update_service();
			header("Location:".base_url()."master/manage_services");
		}
		
		public function edit_service()
		{
			$id=$this->uri->segment(3);
			$this->db->where('Service_type_id',$id);
			$data['update']=$this->db->get('view_service')->row();
			
			$this->load->view('header',$data);
			$this->load->view('menu1',$this->data1);
			$this->load->view('Master/view_edit_services',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function stch_service()
		{
			$data=$this->mastermodel->stch_service();
			header("location:".base_url()."index.php/master/manage_services");
		}
		
		//End Service
		//---Manage Service Provider----//
		public function manage_service_provider()
		{
			$data['services']=$this->db->get('view_service');
			$data['sp']=$this->db->get('view_serv_pro_report');
			
			$this->load->view('header',$data);
			$this->load->view('menu1',$this->data1);
			$this->load->view('Master/view_service_provider',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		public function add_srv_provider()
		{
			$this->mastermodel->add_srv_provider();
			header("location:".base_url()."index.php/master/manage_service_provider");
		}
		
		public function edit_service_opt()
		{
			$this->db->where('Sp_id',$this->uri->segment(3));
			$data['update']=$this->db->get('view_serv_pro_report')->row();
		
			$this->load->view('header',$data);
			$this->load->view('menu1',$this->data1);
			$this->load->view('Master/edit_service_provider',$data);
				$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function stch_sp()
		{
			$data=$this->mastermodel->stch_sp();
			header("location:".base_url()."index.php/master/manage_service_provider");
		}
		
		public function update_srv_provider()
		{
			$this->mastermodel->update_srv_provider();
			header("location:".base_url()."index.php/master/manage_service_provider");
		}
		//End Service Provider
		
	}
?>