<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Employee extends CI_Controller {
		
		public function __construct() 
		{
			parent::__construct();
		    $timezone = new DateTimeZone("Asia/Kolkata");
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
			$this->_is_logged_in();
			$this->load->model('employee_model');
			//$this->load->model('employee_model');
			$this->data1=$this->crm_model->assign_menu();
		  // echo $this->curr;
		}
		
		/*Custom Function here*/
		
	/*Session Check Function */ 
		public function _is_logged_in() 
		{
		    $is_logged_in = $this->session->userdata('profile_id');
			if ($is_logged_in=="") 
			{
				redirect('auth/index/1');
				die();
			}
		}
		//Select State	
		public function select_state()
		{
			$datas=$this->employee_model->select_state($this->uri->segment(3));
			$data['state_id']=$this->uri->segment(4);
			$json = json_encode($datas->result());
			echo $json;	
		}
		public function select_city()
		{
			$data['city_id']=$this->uri->segment(4);
			$datas=$this->employee_model->select_state($this->uri->segment(3));
			$json = json_encode($datas->result());
			echo $json;
		}
		public function spcl()
		{
			$datas=$this->employee_model->spcl();
			$json = json_encode($datas->result());
			echo $json;
		}
		
		public function select_shift()
		{
			$datas=$this->employee_model->select_shift();
			$json = json_encode($datas->result());
			echo $json;
		}
		
		/*Index Function */
		public function index()
		{
		    $data=$this->employee_model->index();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Employee/view_all_employee',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		public function view_employee()
	    {
			$data=$this->employee_model->index();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Employee/view_all_employee',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function create_employee()
		{
			$data=$this->employee_model->emp();
			$this->load->view('Employee/create_employee',$data);
			$this->load->view('footer_11');
		} 
		
		public function insert_employee()
		{
			$data=$this->employee_model->insert_employee();
			echo $data;
		}
		
		public function after_create_employee()
		{
			$data=$this->employee_model->emp();
			$this->load->view('Employee/create_employee',$data);		
		}
		
		// Start Profile Pic, Signature Pic and Document Upload
		
		public function multimedia_upload()
		{
			$data['id']=$this->uri->segment(3);
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Employee/view_emp_multimedia',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}	
		
		public function uploadfile()
		{
			$data=$this->employee_model->uploadfile();
			echo $data;
		}
		
		public function insert_profile_pic()		
		{
			$data=$this->employee_model->insert_profile_pic();
			echo $data;
		}
		
		// Signature Pic
		public function view_sign_pic()
		{
			$this->load->view('Employee/hid_view_sign_pic');
			$this->load->view('footer_11');
		}
		
		public function uploadfile1()
		{
			$data=$this->employee_model->uploadfile1();
			echo $data;
		}
		
		public function insert_sign_pic()		
		{			  
			$data=$this->employee_model->insert_sign_pic();
			echo $data;
		}
		
		
		// End Profile Pic, Signature Pic and Document Upload
		
		//Start  Edit Employee Registration
		public function view_edit_employee()
		{
            /*$data['id']=$this->uri->segment(3);
			$id = $this->uri->segment(3);*/
			$data=$this->employee_model->view_edit_employee();
			$this->db->free_db_resource();
			$this->load->view('Employee/view_edit_employee',$data);
			$this->load->view('footer_11');
			//$this->load->view('Employee/js');
		}
		
		public function update_employee()
		{
			$data=$this->employee_model->update_employee();
			echo $data;
		}
		//End  Edit Employee Registration
		
		public function after_view_employee()
	    {
			$data=$this->employee_model->index();
			$this->load->view('Employee/view_all_employee',$data);
			$this->load->view('footer_11');
		}
		
		
		public function view_employee_report()
		{	
			$data=$this->employee_model->view_employee_report();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Employee/view_emp_report',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function view_employee_attendence_report()
		{	
			$data=$this->employee_model->view_employee_attendence_report();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Employee/view_emp_attendence_report',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function employee_attendance()
		{
			$data=$this->employee_model->employee_attendance();
			$this->load->view('Employee/view_full_attendance',$data);
			$this->load->view('footer_11');
			
		}
		
		
		/*Upload Document Like Pancard user here*/
		public function view_upload_doccument()
	    {
			$data=$this->employee_model->view_upload_doccument();
			$this->load->view('Employee/view_upload_document',$data);
			$this->load->view('footer_11');
		}
		
		public function upload_document()		
		{
			$data=$this->employee_model->upload_document();
			echo $data;
		}
		
		public function insert_uploaded_Document()		
		{
			$data=$this->employee_model->insert_uploaded_Document();
			echo $data;
		}
		/*Image Upload Here*/
		public function view_upload_profile_image()
	    {
			$data=$this->employee_model->view_upload_profile_image();
			$this->load->view('Employee/view_upload_image',$data);
			$this->load->view('footer_11');
		}
		
		public function upload_image()		
		{ 
		    $data=$this->employee_model->upload_image();
			echo $data;
		}
		
		public function update_uploaded_image()		
		{
			$data=$this->employee_model->update_uploaded_image();
			echo $data;
		}
		
	}?>