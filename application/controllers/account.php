<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Account extends CI_Controller {
		public function __construct() 
		{
			parent::__construct();
		    $timezone = new DateTimeZone("Asia/Kolkata");
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
			$this->_is_logged_in();
			$this->data1=$this->crm_model->assign_menu();
		}
		
		/*Session Check Function */ 
		public function _is_logged_in() 
		{
		    $is_logged_in = $this->session->userdata('profile_id');
			if ($is_logged_in=="") 
			{
				redirect('auth/index/1');
				die();
			}
		}
		
		public function index()
	    {
			header("Location:".base_url()."index.php/master/view_mainconfig");
		}
	    
		
		/*-------------------Account panel---------------------*/
		public function account_details()
		{
			$data=array(
			'proc'=>'2','Account_owner'=>$this->session->userdata('profile_id'),'Account_owned_by'=>$this->session->userdata('user_type'),'Account_name'=>'','Account_industry'=>$this->session->userdata('affid'),'Account_emp'=>'','Account_website' =>'','Account_phone'=>'','Account_email'=>'','Account_image'=>'','Account_fax'=>'','Account_type'=>'','Account_ownership'=>'','Account_city'=>'','Account_state'=>'','Account_zipcode'=>'','Account_country'=>'','Account_address'=>'','Account_desc'=>'','Account_status'=>'','logintype'=>'','Account_regdate'=>'','Account_pwd'=>'','Account_pinpwd'=>''
			);
			$query = "CALL sp_account(?" . str_repeat(",?", count($data)-1) . ",@a) ";
			$data['rec']=$this->db->query($query,$data);
			$this->db->free_db_resource();
			$data['id']=$this->uri->segment(3);
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Account/account',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}

        
        public function view_month_sales()
		{
            $condition='';
            $fromdate=date('Y-m-01');
            $todate=date('Y-m-01');
            if($this->input->post('txtempid')!='' && $this->input->post('txtempid')!='0')
            {
             $condition="`m06_user_detail`.`or_m_user_id`='".$this->input->post('txtempid')."' AND ";
            }
             
            if($this->input->post('txtempname')!='' && $this->input->post('txtempname')!='0')
            {
             $condition=$condition."`m06_user_detail`.`or_m_name` LIKE %'".$this->input->post('txtempid')."'% AND ";
            }
             
            if($this->input->post('ddduration')!='' && $this->input->post('ddduration')!='0' && $this->input->post('ddduration')!='-1')
            {
              $fromdate=$this->input->post('ddduration');
              $condition=$condition."`tr21_project_payment`.`m_pay_date` BETWEEN DATE_FORMAT('".$fromdate."','%Y-%m-%d') AND LAST_DAY('".$fromdate."')  AND ";
            }
            
            
			$data=array(
			'proc'=>'1',
            'from_date'=>$fromdate,
            'last_date'=>$fromdate,
            'status'=>$this->input->post('ddstatus'),
            'querys'=>$condition." `m33_account`.`account_type`=11 AND  (CASE WHEN(".$this->session->userdata('user_type')."=2) THEN (`m33_account`.`account_owner` IN (SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_aff_id`= ".$this->session->userdata('affid')." ) AND `m33_account`.`account_owned_by`= 1) OR `m33_account`.`account_owner`= ".$this->session->userdata('profile_id')." AND `m33_account`.`account_owned_by`= 2 WHEN(".$this->session->userdata('user_type')."=1) THEN (`m33_account`.`account_owner`= ".$this->session->userdata('profile_id')." AND `m33_account`.`account_owned_by`= 1) ELSE 1 END) GROUP BY `m43_acc_project`.`m_acc_project` ORDER BY  `m43_acc_project`.`m_acc_project` DESC"
			);
			$query = "CALL sp_month_sales(?" . str_repeat(",?", count($data)-1) . ") ";
			$data['rec']=$this->db->query($query,$data);
			//echo $this->db->last_query();
			$this->db->free_db_resource();
			$data['id']=$this->uri->segment(3);
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Account/view_month_sales',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}

		//Select Office Account /Client Project Here 
		public function project()
		{
			$data=array(
			'proc'=>3,'m_acc_id'=>$this->uri->segment(3),'m_project_sno'=>'','m_acc_owner'=>'','m_project_name'=>'','m_project_description'=>'','m_unit_price'=>'0','m_actual_price'=>'','m_commision'=>'','m_tax'=>'','m_project_stauts'=>'','m_project_create'=>''
			);
			$query = "CALL sp_add_project(?" . str_repeat(",?", count($data)-1).")";
			$data['pro']=$this->db->query($query,$data);
			$this->db->free_db_resource();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Account/project',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		public function project_cost()
		{
			$project_id=$this->uri->segment(3);
			$data=array(
			'proc'=>4,'m_acc_id'=>$this->uri->segment(3),'m_project_sno'=>'','m_acc_owner'=>'','m_project_name'=>'','m_project_description'=>'','m_unit_price'=>'0','m_actual_price'=>'','m_commision'=>'','m_tax'=>'','m_project_stauts'=>'','m_project_create'=>''
			);
			$query = "CALL sp_add_project(?" . str_repeat(",?", count($data)-1).")";
			$data['pro']=$this->db->query($query,$data);
			$this->db->free_db_resource();
			$data['tax']=$this->db->get('m44_tax');
			$this->db->where('m_project_id',$project_id);
			$this->db->where('m_pay_type','1');
			$data['cost']=$this->db->get('tr21_project_payment');
			$data['count']=$this->db->query("SELECT COUNT(*) as co FROM tr21_project_payment WHERE m_project_id='$project_id' and m_pay_status=0");
			$data['total_paid']=$this->db->query("SELECT SUM(m_amonut) as total FROM tr21_project_payment WHERE m_project_id='$project_id' and m_pay_status=1");
			$data['project_id']=$this->uri->segment(3);
			$data['curr']=$this->curr;
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Account/project_cost',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		
		public function update_project_cost()
	    {
			$smenu=array(
			'proc'=>2,
			'm_acc_id'=>$this->uri->segment(3),
			'm_project_sno'=>'',
			'm_acc_owner'=>'',
			'm_project_name'=>'',
			'm_project_description'=>'',
			'm_unit_price'=>$this->input->post('txtunitprc'),
			'm_actual_price'=>$this->input->post('txtact_prc'),
			'm_commision'=>$this->input->post('txtcommission'),
			'm_tax'=>$this->input->post('ddtax'),
			'm_project_stauts'=>1,
			'm_project_create'=>''
			);
			$query = "CALL sp_add_project(?,?,?,?,?,?,?,?,?,?,?,?)";
			$data['rec']=$this->db->query($query,$smenu);
			$this->db->free_db_resource();
			header("Location:".base_url()."index.php/Account/project_cost/".$this->uri->segment(3));		
		}	
		
		//Project Payment
		public function payment()
		{
			$project_id=$this->uri->segment(3);
			$pay_id=$this->uri->segment(4);
			$data['bank']=$this->db->get('m04_bank');
			$data['user_bank']=$this->db->get('m25_user_bank');
			$data=array(
			'proc'=>4,'m_acc_id'=>$this->uri->segment(3),'m_project_sno'=>'','m_acc_owner'=>'','m_project_name'=>'','m_project_description'=>'','m_unit_price'=>'0','m_actual_price'=>'','m_commision'=>'','m_tax'=>'','m_project_stauts'=>'','m_project_create'=>''
			);
			$query = "CALL sp_add_project(?" . str_repeat(",?", count($data)-1).")";
			$data['pro']=$this->db->query($query,$data);
			$this->db->free_db_resource();
			$this->db->where('m_pm_parent',0);
			$data['payment_mode']=$this->db->get('m23_payment_mode');
			$data['count']=$this->db->query("SELECT COUNT(*) as co FROM tr21_project_payment WHERE m_project_id='$project_id' and m_pay_status=0");
			$data['installment']=$this->db->query("SELECT m_amonut,`m_due_amount` FROM `tr21_project_payment` WHERE m_pay_id=(SELECT MIN(m_pay_id) FROM tr21_project_payment WHERE m_pay_status='0' AND m_project_id='$project_id')");
			$data['remain']=$this->db->query("SELECT `m_due_amount` FROM `tr21_project_payment` WHERE m_pay_id=(SELECT MAX(m_pay_id) FROM tr21_project_payment WHERE m_pay_status='1' AND m_project_id='$project_id')");
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Account/payment',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		//Receipt
		public function customer_receipt()
		{		
		    $data['date']=$this->curr;	
			$id1=$this->uri->segment(3);
			$id2=$this->uri->segment(4);
			$id3=$this->uri->segment(5);
		    $clsdt= array(
			'acc_id'=>$id1,
			'proj_id'=>$id2,
			'pay_id'=>$id3
			);
			$query = "CALL sp_generate_reciept (?,?,?)";
			$data['bond_data1']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('Account/view_customer_receipt',$data);
		}
		
		public function select_category()
		{
			$data['id']=$this->uri->segment(3);
			$data['user_bank']=$this->db->get('view_user_bank');
			$this->db->where('m_pm_status',1);
			$this->db->where('m_pm_parent',$this->uri->segment(3));
			$data['category']=$this->db->get('m23_payment_mode');
			$this->load->view('Account/select_category',$data);
		}
		// For Service Select Category 
		public function select_category1()
		{
			$data['id']=$this->uri->segment(3);
			$data['bank']=$this->db->get('m04_bank');
			$data['user_bank']=$this->db->get('m25_user_bank');
			$this->db->where('m_pm_status',1);
			$this->db->where('m_pm_parent',$this->uri->segment(3));
			$data['category']=$this->db->get('m23_payment_mode');
			$this->load->view('Account/service_select_category',$data);
			$this->load->view('footer_11');
		}
		
	 	public function insert_payment()
		{
			$rem_amt=$this->input->post('txtrem_amt');
			$pay_amt=$this->input->post('txtpay_amt');
			$new_rem_bal=$rem_amt-$pay_amt;
			$data=array(
			'proc'=>1,
			'm_project_id'=>$this->uri->segment(3),
			'm_bank_id'=>$this->input->post('bank'),
			'm_paymode'=>$this->input->post('mode'),
			'm_Account_no'=>$this->input->post('detail'),
			'm_ddno'=>$this->input->post('transid'),
			'm_amonut'=>$this->input->post('txtpay_amt'),
			'm_due_amount'=>$new_rem_bal,	
			'm_subdate'=>$this->curr,
			'm_pay_description'=>$this->input->post('txtdescription'),	
			'm_pay_status'=>'1',
			'm_pay_reminder'=>'0',
			'm_pay_user_id'=>$this->session->userdata('profile_id'),
			'm_pay_date'=>$this->curr,
			'm_pay_type'=>'1',
			'no_of_emi'=>$this->input->post('txtno_emi'),
			'emi_charge'=>$this->input->post('txtemi_charge')
			);
			$query = "CALL sp_project_payment(?" . str_repeat(",?", count($data)-1) . ") ";
	        $data['rec']=$this->db->query($query,$data);
            $this->db->free_db_resource();
			header("Location:".base_url()."index.php/Account/project_cost/".$this->uri->segment(3));
			
		}
		
		
		// Update Project Payment
		public function update_pay()
	    {
			if($this->input->post('txtpay_amt')!=0 || $this->input->post('txtpay_amt')!="")
			{
				$smenu=array(
				'proc'=>2,
				'm_project_id'=>$this->uri->segment(3),
				'm_bank_id'=>$this->input->post('bank'),
				'm_paymode'=>$this->input->post('mode'),
				'm_Account_no'=>$this->input->post('detail'),
				'm_ddno'=>$this->input->post('transid'),
				'm_amonut'=>$this->input->post('txtpay_amt'),
				'm_due_amount'=>'',	
				'm_subdate'=>'',
				'm_pay_description'=>$this->input->post('txtdescription'),
				'm_pay_status'=>'1',
				'm_pay_reminder'=>'0',
				'm_pay_user_id'=>'',
				'm_pay_date'=>$this->curr,
				'm_pay_type'=>'',
				'no_of_emi'=>'',
				'emi_charge'=>''
				);
				$query = "CALL sp_project_payment(?" . str_repeat(",?", count($smenu)-1) . ") ";
				$data['rec']=$this->db->query($query,$smenu);
				$this->db->free_db_resource();
				echo "true";
				
			}
			
		}
		
		
		//<-------------------------end task---------------->//
		
	    public function view_services()
		{
			$data['id']=$this->uri->segment(3);
			$data['tax']=$this->db->get('m44_tax');
			$data['servicetype']=$this->db->get('m10_service_type');
			$this->db->where('Account_type','21');
			$data['acc']=$this->db->get('m33_Account');
			$this->db->where('Account_id',$this->uri->segment(3));
			$data['acccount']=$this->db->get('m33_Account');
			$smenu=array(
			'proc'=>'2','m_acc_id'=>$this->uri->segment(3),'m_project_id'=>"",'m_service_id'=>'','m_service_desc'=>'','m_vendor_acc_id'=>'','m_unit'=>'','m_unit_price'=>'','m_tax'=>'','m_payment_type'=>'','m_expiry_date'=>'','m_total'=>'','m_acc_ser_status'=>''
			);
			$query = "CALL sp_acc_rel_ser(?" . str_repeat(",?", count($smenu)-1) . ") ";
			$data['payment']=$this->db->query($query,$smenu);
			$this->db->free_db_resource();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Account/view_services_form',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		
		public function insert_service()
		{
			$expiry=$this->input->post('txtexpiry');
			$data=array(
			'proc'=>'1',
			'm_acc_id'=>$this->uri->segment(3),
			'm_project_id'=>"",
			'm_service_id'=>$this->input->post('ddservice'),
			'm_service_desc'=>$this->input->post('txtservdsc'),
			'm_vendor_acc_id'=>$this->input->post('ddvendor'),
			'm_unit'=>$this->input->post('txtunit'),
			'm_unit_price'=>$this->input->post('txtprice'),
			'm_tax'=>$this->input->post('ddtax'),	
			'm_payment_type'=>$this->input->post('ddpayment_type'),	
			'm_expiry_date'=>$expiry,	
			'm_total'=>$this->input->post('txttotal'),
			'm_acc_ser_status'=>1
			);
			$query = "CALL sp_acc_rel_ser(?" . str_repeat(",?", count($data)-1) . ") ";
			$data['rec']=$this->db->query($query,$data);
			$this->db->free_db_resource();
			header("Location:".base_url()."index.php/Account/view_services/".$this->uri->segment(3));
			
		}
		
		public function service_payment()
		{
			
			$data['id']=$this->uri->segment(3);
			$data['bank']=$this->db->get('m04_bank');
			$data['user_bank']=$this->db->get('m25_user_bank');
			$this->db->where('m_pm_parent','0');
			$data['payment_mode']=$this->db->get('m23_payment_mode');
			$this->db->where('m_acc_project',$this->uri->segment(4));
			$data['service']=$this->db->get('m45_acc_rel_service');
			$data['service_type']=$this->db->get('m10_service_type');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Account/service_payment',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		
		// Insert Service Payment
		public function insert_service_payment()
		{
			$rem_amt=$this->input->post('txtrem_amt');
			$pay_amt=$this->input->post('txtpay_amt');
			$new_rem_bal=$rem_amt-$pay_amt;
			$smenu=array(
			'proc'=>1,
			'm_project_id'=>$this->uri->segment(4),
			'm_bank_id'=>$this->input->post('bank'),
			'm_paymode'=>$this->input->post('mode'),
			'm_Account_no'=>$this->input->post('detail'),
			'm_ddno'=>$this->input->post('transid'),
			'm_amonut'=>$this->input->post('txtpay_amt'),
			'm_due_amount'=>$new_rem_bal,
			'm_subdate'=>$this->curr,
			'm_pay_description'=>$this->input->post('txtdescription'),
			'm_pay_status'=>'1',
			'm_pay_reminder'=>'0',
			'm_pay_user_id'=>$this->session->userdata('profile_id'),
			'm_pay_date'=>$this->curr,
			'm_pay_type'=>'2',
			'no_of_emi'=>'',
			'emi_charge'=>''
			);
			$query = "CALL sp_project_payment(?" . str_repeat(",?", count($smenu)-1) . ") ";
			$data['rec']=$this->db->query($query,$smenu);
			$this->db->free_db_resource();
			$smenu=array(
			'proc'=>'3','m_acc_id'=>$this->uri->segment(4),'m_project_id'=>"",'m_service_id'=>'','m_service_desc'=>'','m_vendor_acc_id'=>'','m_unit'=>'','m_unit_price'=>'','m_tax'=>'','m_payment_type'=>'','m_expiry_date'=>'','m_total'=>'','m_acc_ser_status'=>''
			);
			$query = "CALL sp_acc_rel_ser(?" . str_repeat(",?", count($smenu)-1) . ") ";
			$data['payment']=$this->db->query($query,$smenu);
			header("Location:".base_url()."index.php/Account/view_services/".$this->uri->segment(3));
			
		}	
		
		public function view_amc_report()
		{
			$data=array(
			'queryy'=>'1','proc'=>'1'
			);
			$query = "CALL sp_amc_report(?" . str_repeat(",?", count($data)-1) . ") ";
			$data['rec']=$this->db->query($query,$data);
			$this->db->free_db_resource();
			$data1=array(
			'queryy'=>'1','proc'=>'2'
			);
			$query1 = "CALL sp_amc_report(?" . str_repeat(",?", count($data1)-1) . ") ";
			$data['services']=$this->db->query($query1,$data1);
			$this->db->free_db_resource();
			$data['id']=$this->uri->segment(3);
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Account/view_amc_report',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
        
        public function get_serv()
		{
			$id=$this->uri->segment(3);
			$this->db->where('m_service_type_id',$id);
			$data=$this->db->get('m10_service_type');
			$json=json_encode($data->result());
			echo $json;
		}
		

        /*-------------------Project Payment Report Installmetn Wise -----------------*/

        public function payment_report()
		{
			$project_id=$this->uri->segment(3);
			$data=array(
			'proc'=>5,
			'm_acc_id'=>$this->uri->segment(3),
			'm_project_sno'=>'',
			'm_acc_owner'=>$this->session->userdata('profile_id'),
			'm_project_name'=>$this->session->userdata('affid'),
			'm_project_description'=>$this->session->userdata('user_type'),
			'm_unit_price'=>'0',
			'm_actual_price'=>'',
			'm_commision'=>'',
			'm_tax'=>'',
			'm_project_stauts'=>'',
			'm_project_create'=>''
			);
			$query = "CALL sp_add_project(?" . str_repeat(",?", count($data)-1).")";
			$data['pro']=$this->db->query($query,$data);
			$this->db->free_db_resource();
			$data['tax']=$this->db->get('m44_tax');
			$this->db->where('m_project_id',$project_id);
			$this->db->where('m_pay_type','1');
			$data['cost']=$this->db->get('tr21_project_payment');
			$data['total_paid']=$this->db->query("SELECT SUM(m_amonut) as total FROM tr21_project_payment WHERE m_project_id='$project_id' and m_pay_status=1");
			$data['count']=$this->db->query("SELECT COUNT(*) as co FROM tr21_project_payment WHERE m_project_id='$project_id' and m_pay_status=0");
			$data['project_id']=$this->uri->segment(3);
		
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Account/view_payment_report',$data);
			$this->load->view('footer');
		}

       
        public function project_status()
        {
            
          $this->db->where('m_acc_project',$this->uri->segment('3'));
          $this->db->update('m43_acc_project',array('m_project_stauts'=>$this->uri->segment('4')));
          header("location:".base_url()."Account/payment_report");
        }

        public function add_to_amc()
        {
          $this->db->where('m_acc_project',$this->uri->segment('3'));
          $this->db->update('m43_acc_project',array('m_amc_status'=>1));
          header("location:".base_url()."Account/payment_report");
        }

	}
?>				