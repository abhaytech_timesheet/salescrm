<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Affiliates extends CI_Controller {
		
		public function __construct() 
		{
			parent::__construct();
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$this->_is_logged_in();
			$this->load->model('affiliates_model');
		}
		
		
		public function _is_logged_in() 
		{
			$is_logged_in = $this->session->userdata('profile_id');
			if ($is_logged_in==""  || $is_logged_in=="0") 
			{
				redirect('auth/index/1');
				die();
			}
		}
		public function index()
		{
			$data=$this->affiliates_model->get_submit_ticket_detail();
			$data1=$this->affiliates_model->assign_menu();
			$this->load->view('header');
			$this->load->view('Affiliates/menu',$data1);
			$this->load->view('Help_Desk/index',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		public function after_index()
		{
			$data=$this->affiliates_model->get_submit_ticket_detail();
			$this->load->view('Help_Desk/index',$data);
		}
		/*Just Replace action name on controller */
		public function view_assign_ticket()
		{
			
			$data=$this->affiliates_model->assign_menu();
			$data1=$this->affiliates_model->assign();
			$this->load->view('header');
			$this->load->view('Affiliates/menu',$data);
			$this->load->view('Help_Desk/assign_ticket',$data1);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		/*Affiliate Wise Submit Ticket Assign to their employees according their designation*/ 
	    public function task_assign()
		{
			$data=$this->affiliates_model->task_assign();
			echo $data;
		}
		
		public function view_reassign()
		{
			$data=$this->affiliates_model->view_reassign();
			$this->load->view('Affiliates/reassign',$data);
		}
		
        public function task_again_assign()
	    {
		    $this->affiliates_model->task_again_assign();
		    header("Location:".base_url()."affiliates/view_assign_ticket");
		}
		
		public function view_ticket()
		{
			$data1=$this->affiliates_model->assign_menu();
			$data=$this->affiliates_model->view_ticket();
			$this->load->view('header');
			$this->load->view('Affiliates/menu',$data1);
			$this->load->view('Affiliates/view_ticket',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		/*--------------------REPLY BY Affiliates--------------------*/
		public function affiliates_response()
		{
			$tic=$this->affiliates_model->affiliates_response();
			header("Location:".base_url()."affiliates/view_ticket/$tic");
			
		}
		
		
		/* ----------------Resolve Problem on btn Click--------------- */
		public function resolve()
		{
			$ticket_no=$this->affiliates_model->resolve();
			header("Location:".base_url()."affiliates/view_ticket/$ticket_no");
		}
		
		/*Custom Function here*/
		
		/*Session Check Function */ 
		
		/* ----------------Generate Ticket--------------- */	
		public function ticket()
		{
		    $data1=$this->affiliates_model->assign_menu();
			$this->load->view('header');
			$this->load->view('Affiliates/menu',$data1);
			$this->load->view('Affiliates/ticket_type');
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function submit_ticket()
		{
			$data1=$this->affiliates_model->assign_menu();
			$data=$this->affiliates_model->submit_ticket();
			$this->load->view('header');
			$this->load->view('Affiliates/menu',$data1);
			$this->load->view('Affiliates/submit_ticket',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function reassign()
		{
			$this->affiliates_model->reassign();
			redirect('Affiliates/assign', 'refresh');
		}
		
		/*Profile */
		public function view_account_profile()
		{
			$data1=$this->affiliates_model->assign_menu();
			$data=$this->affiliates_model->view_account_profile();
			$this->load->view('header');
			$this->load->view('Affiliates/menu',$data1);
			$this->load->view('Affiliates/view_accountprofile',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function change_account_image()
		{
			$id=$this->affiliates_model->change_account_image();
			header("Location:".base_url()."affiliates/view_account_profile/".$id);
		}
		
		public function details()
		{
		    $data1=$this->affiliates_model->assign_menu();
		    $data=$this->affiliates_model->details();
			$this->load->view('header');
			$this->load->view('Affiliates/menu',$data1);
			$this->load->view('Affiliates/profile',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		public function change_password()
		{
			$data1=$this->affiliates_model->assign_menu();
			$data=$this->affiliates_model->change_password();
			$this->load->view('header');
			$this->load->view('Affiliates/menu',$data1);
			$this->load->view('Affiliates/change_password',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		public function password_update()
		{
			$this->affiliates_model->password_update();
			header("Location:".base_url()."affiliates/change_password");
		}
		
		
		public function mydetail_update()
		{
			$this->affiliates_model->mydetail_update();
			header("Location:".base_url()."affiliates/index");
			
		}
		
		
		/*SALES BOOSTER*/
		
		public function view_lead()
		{
			$data1=$this->affiliates_model->assign_menu();
			$data=$this->affiliates_model->view_lead();
			$this->load->view('header');
			$this->load->view('Affiliates/menu',$data1);
			$this->load->view('CRM/view_all_lead',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function view_account()
		{
			$data1=$this->affiliates_model->assign_menu();
			$data=$this->affiliates_model->view_account();
			$this->load->view('header');
			$this->load->view('Affiliates/menu',$data1);
			$this->load->view('CRM/view_all_account',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
        
		public function view_contact()
		{
			$data1=$this->affiliates_model->assign_menu();
			$data=$this->affiliates_model->view_contact();
			$this->load->view('header');
			$this->load->view('Affiliates/menu',$data1);
			$this->load->view('CRM/view_all_contact',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function logout()
		{
			header("Location:".base_url()."auth/index/2/");
		}
		
		public function destroy_session()
		{
			$this->session->unset_userdata('profile_id');
			$this->session->unset_userdata('user_id');
			$this->session->unset_userdata('e_email');
			$this->session->unset_userdata('name');
			$this->session->unset_userdata('designation');
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
		}
		
	}
	
?>													