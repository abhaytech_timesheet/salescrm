<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Real_master extends CI_Controller {
		
		public function __construct()
		{
			parent:: __construct();		
			$this->load->database();
			$this->load->library('session');
			$this->load->helper('url');
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			$this->dates = $date->format('Y-m-d');
			$this->curr=$date->format('Y-m-d H:i:s');
			$aff =0;
			if($this->session->userdata('user_id') ==0)
			{
				$aff = 1;
			}
			$this->load->model('mastermodel');
			$this->data1=$this->crm_model->assign_menu();
		}
		public function index()
		{
			if($this->session->userdata('user_id')!="") {
				header("Location:".base_url()."index.php/view_mainconfig");
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
	    public function logout()
		{
			$this->session->unset_userdata('profile_id');
			$this->session->unset_userdata('user_id');
			$this->session->unset_userdata('e_email');
			$this->session->unset_userdata('name');
			$this->session->unset_userdata('designation');
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
			$data['info']='2';
			
			$this->load->view('Auth/header');
			$this->load->view('Auth/login',$data);
			$this->load->view('Auth/footer',$data);
			
		}
		
		
		//--------------------Config---------------------
		public function view_mainconfig()
		{
			if($this->session->userdata('user_id')!="") {
				$data['form_name']="Here you can set Configuration";
				$this->load->model('real_mastermodel');
				$data['config']= $this->real_mastermodel->select_config();
				$this->load->view('header');
				$this->load->view('menu1',$this->data1);
				$this->load->view('Real_master/view_mainconfig',$data);
				$this->load->view('footer',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		public function update_mainconfig()
		{
			if($this->session->userdata('user_id')!="") {
				$data['form_name']="Here you can set Configuration";
				$this->load->model('real_mastermodel');
				$this->real_mastermodel->update_config();
				header("Location:".base_url()."index.php/view_mainconfig");
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		//Manage City Add,Update,Status Change Function 
		public function view_city()
		{
			if($this->session->userdata('user_id')!="") {
				$data['form_name']="Manage City";
				$this->load->model('real_mastermodel');
				$data['city']=$this->real_mastermodel->select_state(1);
				$data['allcity']=$this->real_mastermodel->select_state(0);
				$this->load->view('header');
				$this->load->view('menu1',$this->data1);
				$this->load->view('Real_master/view_city',$data);
				$this->load->view('footer',$data);
			}
			else
			{ 
				header("Location:".base_url()."index.php/logout");
			}	
		}
		
		
		public function select_city()
		{
			if($this->session->userdata('user_id')!="") {
				$this->load->model('real_mastermodel');
				$data['city']=$this->real_mastermodel->select_state($this->uri->segment(3));
				$this->load->view('view_select_city',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		
		
		public function add_city()
		{
			if($this->session->userdata('user_id')!="") {
				$this->load->model('real_mastermodel');
				$this->real_mastermodel->add_city();
				header("Location:".base_url()."index.php/view_city");
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}	
		}
		
		
		public function update_status_city()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="") {
				$id=$this->uri->segment(3);
				$this->db->where('m_loc_id',$id);
				$city=array(
				'm_status'=>$this->uri->segment(4)
				);
				$this->db->update('m02_location',$city);
				header("Location:".base_url()."index.php/view_city");
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}	
		}
		
		
		public function edit_city()
		{
			if($this->session->userdata('user_id')!="") {
				$data['form_name']="Manage City";
				$id=$this->uri->segment(3);
				$data['id']=$id;	
				$this->load->model('real_mastermodel');
				$data['edit_city']=$this->real_mastermodel->select_city($id);
				$data['city']=$this->real_mastermodel->select_state(1);
				$data['allcity']=$this->real_mastermodel->select_state(0);
				$this->load->view('header');
				$this->load->view('menu1',$this->data1);
				$this->load->view('view_edit_city',$data);
				$this->load->view('footer',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}	
		}
		
		public function update_city()
		{
			if($this->session->userdata('user_id')!="") {
				$id=$this->uri->segment(3);
				$data['id']=$id;
				$this->load->model('real_mastermodel');
				$this->real_mastermodel->update_city($id);
				header("Location:".base_url()."index.php/edit_city/$id/");
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}	
		}
		//End City Real_master All Action related 
		
		
		
		
		/*--------------------------Start Bank Real_master--------------------------*/
		
		public function view_bank()
		{
			if($this->session->userdata('user_id')!="") {
				$data=array(
				'm_bank_id'=>'',
				'm_bank_name'=>'',
				'm_bank_status'=>'1',
				'proc'=>'2'
				);
				$query = " CALL sp_bank(?" . str_repeat(",?", count($data)-1) . ")";
				$data['info']=$this->db->query($query, $data);
				$this->load->view('header');
				$this->load->view('menu1',$this->data1);
				$this->load->view('Real_master/view_bank_master',$data);
				$this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		
		public function inser_bank_master()
		{
			if($this->session->userdata('user_id')!="") {
				$this->load->model('real_mastermodel');
				$this->real_mastermodel->insert_bank();
				header("Location:".base_url()."index.php/real_master/view_bank");
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		
		public function view_edit_bank()
		{
			if($this->session->userdata('user_id')!="") {
				$this->db->where('m_bank_id',$this->uri->segment(3));
				$data['info']=$this->db->get('m13_bank');
				$this->load->view('header');
				$this->load->view('menu1',$this->data1);
				$this->load->view('Real_master/view_edit_bank',$data);
				$this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		
		public function view_update_bank()
		{
			if($this->session->userdata('user_id')!="") {
				$this->load->model('real_mastermodel');
				$this->real_mastermodel->view_update_bank();
				header("Location:".base_url()."real_master/view_bank");
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		
		public function bank_status()
		{
			if($this->session->userdata('user_id')!="") {
				$this->load->model('real_mastermodel');
				$this->real_mastermodel->bank_status();
				header("Location:".base_url()."real_master/view_bank");
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		
		/*--------------------------End Bank Real_master--------------------------*/
		
		
		/*--------------------------Start News Real_master--------------------------*/
		
		public function view_news()
		{
			if($this->session->userdata('user_id')!="") {
				$aid=0;
				$this->db->where('m_affid',$aid);
				$data['content']=$this->db->get('m08_news');
				$this->load->view('header');
				$this->load->view('menu1',$this->data1);
				$this->load->view('view_news',$data);
				$this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		
		public function add_news()
		{
			if($this->session->userdata('user_id')!="") {
				$this->load->view('header');
				$this->load->view('menu1',$this->data1);
				$this->load->view('add_news');
				$this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		
		public function insert_news()
		{
			if($this->session->userdata('user_id')!="") {
				$colo=rand(1,5);
				$data=array
				(
				'm_news_id'=>'',
				'm_news_title'=>$this->input->post('txttitle'),
				'm_news_des'=>$this->input->post('txtdescription'),
				'm_news_status'=>$this->input->post('ddstatus'),
				'm_affid'=>$this->session->userdata('profile_id'),
				'proc_id'=>'1'
				);
				$query = " CALL sp_news(?" . str_repeat(",?", count($data)-1) . ")";
				$this->db->query($query, $data);
				header("Location:".base_url()."index.php/add_news");
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		
		public function edit_news()
		{
			if($this->session->userdata('user_id')!="") {
				$aid=0;
				$this->db->where('m_affid',$aid);
				$this->db->where('m_news_id',$this->uri->segment(3));
				$data['content']=$this->db->get('m08_news');
				$this->load->view('header');
				$this->load->view('menu1',$this->data1);
				$this->load->view('view_edit_news',$data);
				$this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		public function update_news()
		{
			if($this->session->userdata('user_id')!="") {
				$data=array
				(
				'm_news_id'=>$this->uri->segment(3),
				'm_news_title'=>$this->input->post('txttitle'),
				'm_news_des'=>$this->input->post('txtdescription'),
				'm_news_status'=>$this->input->post('ddstatus'),
				'm_affid'=>$this->session->userdata('profile_id'),
				'proc_id'=>'2'
				);
				$query = " CALL sp_news(?" . str_repeat(",?", count($data)-1) . ")";
				$this->db->query($query, $data);
				header("Location:".base_url()."index.php/view_news");
				
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		public function delete_news()
		{
			if($this->session->userdata('user_id')!="") {
				$data=array
				(
				'm_news_id'=>$this->uri->segment(3),
				'm_news_title'=>'',
				'm_news_des'=>'',
				'm_news_status'=>$this->uri->segment(4),
				'm_affid'=>'',
				'proc_id'=>'3'
				);
				$query = " CALL sp_news(?" . str_repeat(",?", count($data)-1) . ")";
				$this->db->query($query, $data);
				header("Location:".base_url()."index.php/view_news");
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		/*--------------------------End News Real_master--------------------------*/
		
		
		/*--------------------------Start Plan Real_master--------------------------*/
		
		public function plan_master()
		{
			if($this->session->userdata('user_id')!="") {
				$plan_data = array('proc'=>1,'id'=>0);
				$query = "CALL sp_view_plans (?" . str_repeat(",?", count($plan_data)-1) . ") ";
				$data['resul'] = $this->db->query($query,$plan_data);
				$this->db->free_db_resource();
				$data['sites'] = $this->db->get('m68_site_master');
				$this->load->view('header');
				$this->load->view('menu1',$this->data1);
				$this->load->view('Real_master/view_plan_master',$data);
				$this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		public function insert_plan_master()
		{
			if($this->session->userdata('user_id')!="") {
				$registry = 0.00;
				$refund = 0.00;
				if($this->input->post('txtregistry')!=0 || $this->input->post('txtregistry')!='')
				{
					$registry = $this->input->post('txtregistry');
				}
				
				if($this->input->post('txtrefund')!=0 || $this->input->post('txtrefund')!='')
				{
					$refund = $this->input->post('txtrefund');
				}
				
				$plan_data = array(
				'proc'=>1,
				'pm_id'=>0,
				'pm_planname'=>$this->input->post('txtplanname'),
				'pm_duration'=>$this->input->post('txtduration'),
				'pm_site_id'=>$this->input->post('ddsitename'),
				'pm_booking_amount'=>$this->input->post('txtbookamnt'),
				'pm_allotment'=>$this->input->post('txtallotment'),
				'pm_allot_days'=>$this->input->post('txtallot_days'),
				'installment_type'=>$this->input->post('ddinstallment_type'),
				'pm_installment'=>$this->input->post('txtinstallment'),
				'pm_install_days'=>$this->input->post('txtins_days'),
				'pm_discount'=>$this->input->post('txtdiscount'),
				'pm_interest'=>$this->input->post('txtinterest'),
				'pm_is_registry'=>$this->input->post('txtregistrystatus'),
				'pm_registry'=>$registry,
				'pm_is_refundable'=>$this->input->post('txtrefundstatus'),
				'pm_refund'=>$refund,
				'pm_description'=>$this->input->post('txtdesc'),
				'pm_status'=>1,
				'pm_date'=>$this->dates
				);
				$query = " CALL sp_insert_plan (?" . str_repeat(",?", count($plan_data)-1) . ")";
				$data['resul'] = $this->db->query($query,$plan_data);
				$this->db->free_db_resource();
				header("Location:".base_url()."index.php/real_master/plan_master");
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		
		/*--------------------------End Plan Real_master--------------------------*/	
		
		/*--------------------------Start Unit Real_master--------------------------*/
		
		public function unit_master()
		{
			if($this->session->userdata('user_id')!="") {
				$data['info']=$this->db->get('m66_unit');
				$this->load->view('header');
				$this->load->view('menu1',$this->data1);
				$this->load->view('Real_master/view_unit_master',$data);
				$this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		
		public function inser_unit_master()
		{
			if($this->session->userdata('user_id')!="") {
				$this->load->model('real_mastermodel');
				$this->real_mastermodel->insert_unit_master();
				header("Location:".base_url()."real_master/unit_master");
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		public function view_edit_unit_master()
		{
			if($this->session->userdata('user_id')!="") {
				$data=array(
				'm_unit_id'=>$this->uri->segment(3),
				'm_unit_name'=>'',
				'm_unit_description'=>'',
				'm_unit_status'=>'1',
				'm_unit_value'=>'',
				'proc'=>'2'
				);
				$query = " CALL sp_unit_master(?" . str_repeat(",?", count($data)-1) . ")";
				$data['info']=$this->db->query($query, $data);
				$this->load->view('header');
				$this->load->view('menu1',$this->data1);
				$this->load->view('Real_master/view_edit_unit_master',$data);
				$this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		
		public function update_unit_master()
		{
			if($this->session->userdata('user_id')!="") {
				$this->load->model('real_mastermodel');
				$this->real_mastermodel->update_unit_master();
				header("Location:".base_url()."index.php/real_master/unit_master");
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		
		
		public function unit_master_status()
		{
			if($this->session->userdata('user_id')!="") {
				$this->load->model('real_mastermodel');
				$this->real_mastermodel->unit_master_status();
				header("Location:".base_url()."index.php/real_master/unit_master");
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		
		/*--------------------------End Unit Real_master--------------------------*/
		
		
		/*--------------------------Start Site Real_master--------------------------*/
		
		public function site_master()
		{
			if($this->session->userdata('user_id')!="") {
				$data=array(
				'm_site_id'=>'',
				'm_site_name'=>'',
				'm_site_location'=>'',
				'm_site_rate'=>'',
				'm_site_rate_per'=>'',
				'm_site_corner_rate'=>'',
				'm_site_park_facing'=>'',
				'm_site_corner_park'=>'',
				'm_site_commercial'=>'',
				'm_site_main_raod'=>'',
				'm_site_other'=>'',
				'm_site_commercial_facing'=>'',
				'm_site_dev_charge_method'=>'',
				'm_site_dev_charge'=>'',
				'm_site_dev_per'=>'',
				'm_site_point_value'=>'',
				'm_site_point_status'=>'',
				'proc'=>'2'
				);
				$query = " CALL sp_site_master(?" . str_repeat(",?", count($data)-1) . ")";
				$data['info']=$this->db->query($query, $data);
				$this->db->free_db_resource();
				$data['unit']=$this->db->get('m66_unit');
				
				$this->load->view('header');
				$this->load->view('menu1',$this->data1);
				$this->load->view('Real_master/view_site_master',$data);
				$this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/real_master/logout");
			}
		}
		
		
		public function insert_site_master()
		{
			if($this->session->userdata('user_id')!="") {
				$this->load->model('real_mastermodel');
				$this->real_mastermodel->insert_site_master();
				header("Location:".base_url()."index.php/real_master/site_master");
			}
			else
			{
				header("Location:".base_url()."index.php/real_master/logout");
			}
		}
		
		
		
		public function view_edit_site_master()
		{
			if($this->session->userdata('user_id')!="") {
				$this->db->where('m_site_id',$this->uri->segment(3));
				$data['info']=$this->db->get('m68_site_master');
				$this->db->free_db_resource();
				$data['unit']=$this->db->get('m66_unit');
				$this->load->view('header');
				$this->load->view('menu1',$this->data1);
				$this->load->view('Real_master/view_edit_site_master',$data);
				$this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/real_master/logout");
			}
		}
		
		
		public function update_site_master()
		{
			if($this->session->userdata('user_id')!="") {
				$this->load->model('real_mastermodel');
				$this->real_mastermodel->update_site_master();
				header("Location:".base_url()."index.php/real_master/site_master");
			}
			else
			{
				header("Location:".base_url()."index.php/real_master/logout");
			}
		}
		
		public function site_master_status()
		{
			if($this->session->userdata('user_id')!="") {
				$this->load->model('real_mastermodel');
				$this->real_mastermodel->site_master_status();
				header("Location:".base_url()."index.php/real_master/site_master");
			}
			else
			{
				header("Location:".base_url()."index.php/real_master/logout");
			}
		}
		
		
		/*--------------------------End Site Real_master--------------------------*/
		
		/*--------------------------Start Plot Size--------------------------*/
		
		public function plot_size_master()
		{
			if($this->session->userdata('user_id')!="") {
				$data=array(
				'm_plot_size_id'=>'',
				'm_plot_width_ft'=>'',
				'm_plot_width_inch'=>'',
				'm_plot_height_ft'=>'',
				'm_plot_height_inch'=>'',
				'm_plot_Area'=>'',
				'm_plot_per'=>'',
				'm_plot_booking_amt'=>'',
				'm_plot_status'=>'',
				'proc'=>'2'
				);
				$query = " CALL sp_plot_size_master(?" . str_repeat(",?", count($data)-1) . ")";
				$data['info']=$this->db->query($query, $data);
				$this->db->free_db_resource();
				$data['unit']=$this->db->get('m66_unit');
				$this->load->view('header');
				$this->load->view('menu1',$this->data1);
				$this->load->view('Real_master/view_plot_size_master',$data);
				$this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/real_master/logout");
			}
		}
		
		public function insert_plot_size()
		{
			if($this->session->userdata('user_id')!="") {
				$this->load->model('real_mastermodel');
				$this->real_mastermodel->insert_plot_size();
				header("Location:".base_url()."index.php/real_master/plot_size_master");
			}
			else
			{
				header("Location:".base_url()."index.php/real_master/logout");
			}
		}
		
		
		
		public function view_edit_plot_size()
		{
			if($this->session->userdata('user_id')!="") {
				$this->db->where('m_plot_size_id',$this->uri->segment(3));
				$data['info']=$this->db->get('m67_plot_size');
				$this->db->free_db_resource();
				$data['unit']=$this->db->get('m66_unit');
				$this->load->view('header');
				$this->load->view('menu1',$this->data1);
				$this->load->view('Real_master/view_edit_plot_size',$data);
				$this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/real_master/logout");
			}
		}
		
		
		public function update_plot_size()
		{
			if($this->session->userdata('user_id')!="") {
				$this->load->model('real_mastermodel');
				$this->real_mastermodel->update_plot_size();
				header("Location:".base_url()."index.php/real_master/plot_size_master");
			}
			else
			{
				header("Location:".base_url()."index.php/real_master/logout");
			}
		}
		
		
		public function plot_size_status()
		{
			if($this->session->userdata('user_id')!="") {
				$this->load->model('real_mastermodel');
				$this->real_mastermodel->plot_size_status();
				header("Location:".base_url()."index.php/real_master/plot_size_master");
			}
			else
			{
				header("Location:".base_url()."index.php/real_master/logout");
			}
		}
		
		/*--------------------------End Plot Size--------------------------*/
		
		
		/*--------------------------Start Site Plot Size--------------------------*/
		
		public function site_plot_master()
		{
			if($this->session->userdata('user_id')!="") {
				$this->db->where('m_site_point_status','1');
				$data['site']=$this->db->get('m68_site_master');
				$this->db->where('m_plot_status','1');
				$data['plot']=$this->db->get('m67_plot_size');
				$this->db->free_db_resource();
				$data['unit']=$this->db->get('m66_unit');
				$this->load->view('header');
				$this->load->view('menu1',$this->data1);
				$this->load->view('Real_master/view_site_plot_master',$data);
				$this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/real_master/logout");
			}
		}
		
		public function view_plot_charges()
		{
			if($this->session->userdata('user_id')!="") {
				$this->db->where('m_site_id',$this->uri->segment(3));
				$data['site']=$this->db->get('m68_site_master');
				$this->load->view('Real_master/view_plot_charges',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/real_master/logout");
			}
		}
		
		public function fill_amount()
		{
			if($this->session->userdata('user_id')!="") {
				$this->db->where('m_plot_size_id',$this->uri->segment(3));
				$data['plot']=$this->db->get('m67_plot_size');
				$this->load->view('Real_master/fill_amt',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		
		public function insert_site_plot()
		{
			if($this->session->userdata('user_id')!="") {
				$this->load->model('real_mastermodel');
				$this->real_mastermodel->insert_site_plot();
				header("Location:".base_url()."index.php/real_master/site_plot_master");
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		public function search_plot()
		{
			if($this->session->userdata('user_id')!="") {
				$condition='';
			    $site_search=$this->input->post('txtsite_search');
			    $plotno_search=$this->input->post('txtplotno_search');
				
				if($this->input->post('txtsite_search')!="-1" && $this->input->post('txtsite_search')!="")
				{
					$condition=$condition." m69_site_plot.m_sp_site= ".$this->input->post('txtsite_search')."  and";
				}
				
				if($this->input->post('txtplotno_search')!="" && $this->input->post('txtplotno_search')!="0")
				{
					$condition=$condition." m69_site_plot.m_sp_plot_no=".$this->input->post('txtplotno_search')."  and";
				}
				
				$condition=$condition." m69_site_plot.m_sp_id !=0 ";
				$condition=$condition." ORDER BY m69_site_plot.m_sp_id ASC ";
				$record=array(
				'query'=>$condition
				);
				$query = " CALL sp_search_plot(?" . str_repeat(",?", count($record)-1) . ") ";
				$data['info']=$this->db->query($query, $record);
				$this->db->free_db_resource();
				
				$this->db->where('m_site_point_status','1');
				$data['site']=$this->db->get('m68_site_master');
				$this->db->where('m_plot_status','1');
				$data['plot']=$this->db->get('m67_plot_size');
				$data['unit']=$this->db->get('m66_unit');
				
				$this->load->view('header');
				$this->load->view('menu1',$this->data1);
				$this->load->view('Real_master/view_site_plot_master_search',$data);
				$this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/logout");
			}
		}
		
		
		public function view_value()
		{
			echo $this->db->query("SELECT * FROM m66_unit WHERE m_unit_id='".$this->input->post('txtper')."' ")->row()->m_unit_value;
		}
		
		/*--------------------------End Site Plot Size--------------------------*/
		
	}
?>																