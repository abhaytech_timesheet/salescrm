<?php 
	class Client extends CI_Controller {
		
		public function __construct() 
		{
			parent::__construct();
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$this->_is_logged_in();
		}
		public function assign_menu()
		{
			$desig=11;
			$data['menu']=$this->db->query("SELECT MENU_NAME,MENU_ICON FROM `vw_menu` WHERE DESIGNATIONM='$desig' AND MENU_STATUS='1' AND ASSIGN_SMENU_STATUS='1' GROUP BY MENU_NAME");
			$data['sub']=$this->db->query("SELECT * FROM `vw_menu` WHERE DESIGNATIONM='$desig' AND SUBM_STATUS='1' AND ASSIGN_SMENU_STATUS='1' GROUP BY SUBMENU");
			return $data;
		}
		public function _is_logged_in() 
		{
			$is_logged_in = $this->session->userdata('profile_id');
			if ($is_logged_in==""  || $is_logged_in=="0") 
			{
				redirect('auth/index/1');
				die();
			}
		}
		
		public function index()
		{
			$email=$this->session->userdata('e_email');
			$cid=$this->input->post('txtcategory');
			$data1=$this->assign_menu();
			$this->load->view('client/header');
			$this->load->view('client/menu',$data1);
			//$this->load->view('client/index',$data);
			$this->load->view('client/footer');
		}
		public function dashboard()
		{
			$data1=$this->assign_menu();
			$data['new']=$this->db->query("SELECT * FROM tr20_submit_ticket WHERE DATE_FORMAT(adddate,'%y-%m-%d')=DATE_FORMAT(now(),'%y-%m-%d')");
			$data['complete']=$this->db->query("SELECT * FROM tr20_submit_ticket WHERE txtstatus=0");
			$data['pending']=$this->db->query("SELECT * FROM tr20_submit_ticket WHERE DATE_FORMAT(adddate,'%y-%m-%d')!=DATE_FORMAT(now(),'%y-%m-%d') group by ticket_no");
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			$data['curr']=$date->format('Y-m-d H:i:s');
			$this->db->where('lead_status !=',7);
			$data['lead1']=$this->db->count_all_results('m32_lead');
			$this->db->where('account_status',1);
			$data['account1']=$this->db->count_all_results('m33_account');
			$data['opp']=$this->db->count_all('m35_opportunity');
			$data['anounc']=$this->db->get('tr19_anoncement');
			$this->db->order_by('task_reminder','asc');
			$data['task_info']=$this->db->get('m38_task');
			$data['lead']=$this->db->get('m32_lead');
			$data['account']=$this->db->get('m33_account');
			$data['contact']=$this->db->get('m34_contact');
			$this->load->view('client/header');
			$this->load->view('client/menu',$data1);
			$this->load->view('client/dashboard',$data);
			$this->load->view('client/footer');
			
		}
		
		public function assign()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('profile_id')!="0")
			{
				$affid=$this->session->userdata["affid"];
				$condition='';
				$condition = $condition." affiliate_id='$affid' and  (emp_id!=''||emp_id!=0) GROUP BY tkt_ref_no order by tkt_id desc";
				$clsdt= array(
				'querey'=>$condition
				);
				$query = "CALL sp_view_ticket(?)";
				$data['rec']=$this->db->query($query,$clsdt);
				$this->db->free_db_resource();
				$data['emp']=$this->db->get('m06_user_detail');
				$this->db->where('m_des_id',$this->session->userdata('designation'));
				$this->db->where('m_dm_status',1);
				$data['assign']=$this->db->get('m19_des_menu');
				$this->db->where('m_dsm_status',1);
				$data['smenu']=$this->db->get('m20_des_smenu');
				$this->db->where('m_menu_status',1);
				$data['menu1']=$this->db->get('m01_menu');
				$this->db->where('m_submenu_status',1);
				$data['menu2']=$this->db->get('m02_submenu');
				$this->load->view('client/header');
				$this->load->view('client/menu',$data);
				$this->load->view('Help_Desk/assign_ticket',$data);
				$this->load->view('client/footer');
			}
			else
			{
				header("Location:".base_url()."index.php/client/logout");
				
			}
		}
		
		public function view_ticket()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('profile_id')!="0")
			{
				$p_id=$this->uri->segment(3);
				$condition='';
				$condition=$condition."m46_submit_ticket.tkt_ref_no='$p_id'";
				$clsdt1= array(
				'querey'=>$condition
				);
				$query1 = "CALL sp_view_ticket(?)";
				$data['rec']=$this->db->query($query1,$clsdt1);
				$this->db->free_db_resource();
				$this->db->where('tkt_ref_no',$p_id);
				$data['ticket']=$this->db->get('m46_submit_ticket');
				foreach($data['ticket']->result() as $row)
				{
					$id=$row->tkt_id;	
				}
				$condition = "tr24_ticket_transaction.tkt_id='$id'";
				$clsdt= array(
				'querey'=>$condition
				);
				$query = "CALL sp_view_ticket_reply(?)";
				$data['rec1']=$this->db->query($query,$clsdt);
				$this->db->free_db_resource();
				$data['emp']=$this->db->get('m06_user_detail');
				$this->db->where('m_des_id',$this->session->userdata('designation'));
				$this->db->where('m_dm_status',1);
				$data['assign']=$this->db->get('m19_des_menu');
				$this->db->where('m_dsm_status',1);
				$data['smenu']=$this->db->get('m20_des_smenu');
				$this->db->where('m_menu_status',1);
				$data['menu1']=$this->db->get('m01_menu');
				$this->db->where('m_submenu_status',1);
				$data['menu2']=$this->db->get('m02_submenu');
				$this->load->view('client/header');
				$this->load->view('client/menu',$data);
				$this->load->view('client/view_ticket',$data);
				$this->load->view('client/footer');
				
			}
			else
			{
				header("Location:".base_url()."index.php/client/logout");
			}
		}
		
		public function search_ticket()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('e_email')=="")
			{
				header("Location:".base_url()."index.php/admin/logout");
			}
			else
			{
				$txtdepartment=$this->input->post('txtdepartment');
				$data['depart']=$this->input->post('txtdepartment');
				
				$data['emp']=$this->db->get('m06_user_detail');
				$this->db->where('m_des_id',$this->session->userdata('designation'));
				$this->db->where('m_dm_status',1);
				$data['assign']=$this->db->get('m19_des_menu');
				$this->db->where('m_dsm_status',1);
				$data['smenu']=$this->db->get('m20_des_smenu');
				$this->db->where('m_menu_status',1);
				$data['menu1']=$this->db->get('m01_menu');
				$this->db->where('m_submenu_status',1);
				$data['menu2']=$this->db->get('m02_submenu');
				
				$this->load->view('client/header');
				$this->load->view('client/menu',$data);
				$this->load->view('client/view_search_ticket',$data);
				$this->load->view('client/footer');
			}
		}
		
		/*--------------------REPLY BY ADMIN--------------------*/
		function admin_response()
		{
			$config['upload_path']   =   "application/uploadimage/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "1907";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$this->load->helper('url');
			$this->load->library('session');
			$this->load->database();
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			$tic=$this->input->post('txtticket');
			$fileupload=$finfo['raw_name'].$finfo['file_ext'];
			if($fileupload=="")
			{
				$fi="";
			}
			else
			{
				$fi=$fileupload;
			}
			$data=array(       'proc'=>2,
			'ticket_no'=>$this->input->post('txtticket'),
			'tkt_person_name'=>'',
			'tkt_email'=>'',
			'tkt_department'=>'',
			'emp_id'=>'',
			'tkt_subject'=>'',
			'tkt_urgency'=>'',
			'tkt_discription'=>'',
			'tkt_response_type'=>1,
			'response_by'=>$this->session->userdata('name'),
			'tkt_userfile'=>$fi,
			'tkt_status'=>1,
			'account_id'=>'',
			'affiliate_id'=>'',
			'tkt_sub_date'=>$date->format('Y-m-d H:i:s'),
			'trans_description'=>$this->input->post('txtdiscription'),
			'trans_response_date'=>$date->format('Y-m-d H:i:s'),
			'trans_status'=>1
			);
			$query = "CALL sp_ticket_submission(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			$data['rec']=$this->db->query($query,$data);
			$this->db->free_db_resource();
			
			
			
			/*$tic_no=$this->input->post('txtticket');
				$txtsubject=$this->input->post('txtsubject');
				$description=$this->input->post('txtdiscription');
				$res=$this->input->post('txtemail');
				$data['rec']=$this->db->query("SELECT txtemail FROM tr20_submit_ticket WHERE ticket_no='$tic_no' GROUP BY ticket_no");
				foreach($data['rec']->result() as $rec)
				{
				$tic_owner=$rec->txtemail;
				break;
				}
				
				$ip=$_SERVER['REMOTE_ADDR'];
				$message = '<p>Dear: '.$txtname.',<br><br>
				This is a notification to let you know that we have responded back to your query with reference to the ticket no #'.$this->input->post('txtticket').'. 
				Please re-open the ticket and respond accordingly.<br><br>
				Message: '. $description.'<br><br>
				Priority: High<br><br>
				Status: Open<br><br>
				Your IP: '. $ip.'<br><br>
				For checking your Ticket- '.base_url().'index.php/direct_ticket/view_ticket/'.$tic_no.'</p>';
				$this->load->library('email');
				$this->email->set_newline("\r\n");
				$this->email->from('office@metroheights.co.in','Support Ferryipl'); // change it to yours
				$this->email->to($tic_owner);// change it to yours
				$this->email->cc($res);// change it to yours
				$this->email->subject('Ferry Support Ticket[ID:'.$tic_no.']');
				$this->email->message($message);
				$this->email->send();
			*/
			
			
			header("Location:".base_url()."index.php/client/view_ticket/$tic");
			
		}
		
		
		/* ----------------Resolve Problem on btn Click--------------- */
		
		
		public function resolve()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			
			if($this->session->userdata('profile_id')!="0")
			{
				$timezone = new DateTimeZone("Asia/Kolkata" );
				$date = new DateTime();
				$date->setTimezone($timezone);
				$ticket_no=$this->input->post('ticket_no');
				$data=array(  
				'proc'=>3,
				'ticket_no'=>$ticket_no,
				'tkt_person_name'=>'',
				'tkt_email'=>'',
				'tkt_department'=>'',
				'emp_id'=>'',
				'tkt_subject'=>'',
				'tkt_urgency'=>'',
				'tkt_discription'=>'',
				'tkt_response_type'=>1,
				'response_by'=>'',
				'tkt_userfile'=>'',
				'tkt_status'=>0,
				'account_id'=>'',
				'affiliate_id'=>'',
				'tkt_sub_date'=>$date->format('Y-m-d H:i:s'),
				'trans_description'=>'',
				'trans_response_date'=>'',
				'trans_status'=>''
				);
				$query = "CALL sp_ticket_submission(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				$data['rec']=$this->db->query($query,$data);
				$this->db->free_db_resource();
				/*$data['rec']=$this->db->query("SELECT tkt_email FROM m46_submit_ticket WHERE tkt_ref_no='$ticket_no' GROUP BY tkt_email");
					foreach($data['rec']->result() as $rec)
					{
					$tic_owner=$rec->tkt_email;
					break;
					}
					$ip=$_SERVER['REMOTE_ADDR'];
					$message = '<p>Dear: '.$tic_owner.',<br><br>
					This is in response of your attempt that your all query has been resolved now. So, we are considering it true and here by changing the status of your ticket(5464) to closed .<br><br> Thanking you,<br><br>
					Keep visiting us!<br><br>
					Priority: High<br><br>
					Status: Closed<br><br>
					Your IP: '. $ip.'<br><br>
					For checking your Ticket- '.base_url().'index.php/direct_ticket/view_ticket/'.$ticket_no.'</p>';
					$this->load->library('email');
					$this->email->set_newline("\r\n");
					$this->email->from('office@metroheights.co.in','Support Ferryipl'); // change it to yours
					$this->email->to($tic_owner);// change it to yours
					$this->email->subject('Ferry Support Ticket[ID:'.$ticket_no.']');
					$this->email->message($message);
				$this->email->send();*/
				header("Location:".base_url()."index.php/client/view_ticket/$ticket_no");
			}
			else
			{
				header("Location:".base_url()."index.php/client/logout");
			}
		}
		
		
		public function search_ticket_report()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			
			$fromdate='';
			$todate='';
			$fromdate=$this->input->post('txtfromdate');
			$todate=$this->input->post('txttodate');
			
			$affid=$this->session->userdata("affid");
			$condition='';
			
			$data['depart']=$this->input->post('txtdepartment');
			$data['ddactype']=$this->input->post('ddactype');
			if($todate!="" && $fromdate!="")
			{
				$condition = $condition." `m46_submit_ticket`.`tkt_sub_date` BETWEEN DATE_FORMAT('$fromdate','%Y-%m-%d') AND DATE_FORMAT('$todate','%Y-%m-%d') and";
			}
			if($this->input->post('ddstatus')!="" && $this->input->post('ddstatus')!="-1")
			{
				$condition=$condition." `m46_submit_ticket`.`tkt_status`='".$this->input->post('ddstatus')."' and";
			}
			if($this->input->post('ddassign')!="" && $this->input->post('ddassign')!="-1")
			{
				$condition=$condition." `m46_submit_ticket`.`emp_id`='".$this->input->post('ddassign')."' and";
			}
			if($this->input->post('txtdepartment')!="" && $this->input->post('txtdepartment')!="-1" )
			{
				$condition = $condition." `m46_submit_ticket`.`tkt_department`='".$this->input->post('txtdepartment')."' and";
			}
			if($this->input->post('ddactype')!="" && $this->input->post('ddactype')!="-1")
			{
				$condition = $condition." `m46_submit_ticket`.`affiliate_id`='".$this->input->post('ddactype')."' and";
			}
			
			$condition = $condition." `m46_submit_ticket`.`tkt_id`!='0' ";
			$condition=$condition." ORDER BY `m46_submit_ticket`.`tkt_id` DESC ";
			
			$clsdt= array(
			'querey'=>$condition
			);
			$queery = "CALL sp_view_ticket(?)";
			$query1=$this->db->query($queery, $clsdt);
			
			$json=json_encode($query1->result());
			echo $json;
			
		}
		public function status_report()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('e_email')=="")
			{
				header("Location:".base_url()."index.php/admin/logout");
			}
			else
			{
				$txtdepartment=$this->input->post('txtdepartment');
				$data['depart']=$this->input->post('txtdepartment');
				
				$data['emp']=$this->db->get('m06_user_detail');
				$this->db->where('m_des_id',$this->session->userdata('designation'));
				$this->db->where('m_dm_status',1);
				$data['assign']=$this->db->get('m19_des_menu');
				$this->db->where('m_dsm_status',1);
				$data['smenu']=$this->db->get('m20_des_smenu');
				$this->db->where('m_menu_status',1);
				$data['menu1']=$this->db->get('m01_menu');
				$this->db->where('m_submenu_status',1);
				$data['menu2']=$this->db->get('m02_submenu');
				$this->load->view('client/header');
				$this->load->view('client/menu',$data);
				$this->load->view('client/view_ticket_status_report',$data);
				$this->load->view('client/footer');
			}
		}
		
		public function view_lead()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->db->where('m_des_id',$this->session->userdata('designation'));
				$this->db->where('m_dm_status',1);
				$data['assign']=$this->db->get('m19_des_menu');
				$this->db->where('m_dsm_status',1);
				$data['smenu']=$this->db->get('m20_des_smenu');
				$this->db->where('m_menu_status',1);
				$data['menu1']=$this->db->get('m01_menu');
				$this->db->where('m_submenu_status',1);
				$data['menu2']=$this->db->get('m02_submenu');
				/*$this->db->where('lead_owner',$this->session->userdata('profile_id'));
					$this->db->where('lead_created_by',2);
					$this->db->where_not_in('lead_status','0');
					$this->db->order_by('lead_id','desc');
					$data['rec']=$this->db->get('m32_lead');
					$this->db->where('contact_id',$this->session->userdata('profile_id'));
				$data['con']=$this->db->get('m34_contact');*/
				$smenu=array(
				'proc'=>2,'lead_owner'=>$this->session->userdata('profile_id'),'lead_created_by'=>$this->uri->segment(3),'lead_company'=>'','lead_prefix'=>'','lead_name'=>'','lead_title'=>'','lead_industry'=>'',
				'lead_emp'=>'','lead_email'=>'','lead_mobile'=>'','lead_source'=>'','lead_current_status'=>'','lead_description'=>'','lead_address'=>'','lead_city'=>'',
				'lead_state'=>'','lead_zipcode'=>'','lead_country'=>'','lead_status'=>'','lead_logintype'=>'','lead_regdate'=>'');
				$query = " CALL sp_lead(?" . str_repeat(",?", count($smenu)-1) .") ";
				$data['rec']=$this->db->query($query, $smenu);
				$this->db->free_db_resource();
				$data['id']=$this->uri->segment(3);
				$this->load->view('client/header');
				$this->load->view('client/menu',$data);
				$this->load->view('CRM/view_all_lead',$data);
				$this->load->view('client/footer');
			}
			else
			{
				header("Location:".base_url()."index.php/client/logout");
			}
		}
		public function details()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('e_email')=="")
			{
				header("Location:".base_url()."index.php/client/logout");
			}
			else
			{
				$id = $this->session->userdata('profile_id');
				$this->db->where('contact_id',$id);
				$data['rec2']=$this->db->get('m34_contact');
				$data['rec1']=$this->db->get('m05_location');
				$this->db->where('m_parent_id',0);
				$data['rec']=$this->db->get('m05_location');
				$this->db->where('m_des_id',$this->session->userdata('designation'));
				$this->db->where('m_dm_status',1);
				$data['assign']=$this->db->get('m19_des_menu');
				$this->db->where('m_dsm_status',1);
				$data['smenu']=$this->db->get('m20_des_smenu');
				$this->db->where('m_menu_status',1);
				$data['menu1']=$this->db->get('m01_menu');
				$this->db->where('m_submenu_status',1);
				$data['menu2']=$this->db->get('m02_submenu');
				$this->load->view('client/header');
				$this->load->view('client/menu',$data);
				$this->load->view('client/profile',$data);
				$this->load->view('client/footer');
			}
		}
		
		public function change_password()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('e_email')=="")
			{
				header("Location:".base_url()."index.php/client/logout");
			}
			else
			{
		        $data=$this->assign_menu();
				$id=$this->session->userdata('profile_id');
				$this->db->where('or_user_id',$id);
				$data['rec']=$this->db->get('tr04_login');
				$this->db->where('m_des_id',$this->session->userdata('designation'));
				$this->db->where('m_dm_status',1);
				$data['assign']=$this->db->get('m19_des_menu');
				$this->db->where('m_dsm_status',1);
				$data['smenu']=$this->db->get('m20_des_smenu');
				$this->db->where('m_menu_status',1);
				$data['menu1']=$this->db->get('m01_menu');
				$this->db->where('m_submenu_status',1);
				$data['menu2']=$this->db->get('m02_submenu');
				$this->load->view('header');
				$this->load->view('client/menu',$data);
				$this->load->view('client/change_password',$data);
				$this->load->view('sidebar');
			    $this->load->view('footer');
			}
		}
		
		
		public function password_update()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('e_email')=="")
			{
				header("Location:".base_url()."index.php/client/logout");
			}
			else
			{
				$pid=$this->session->userdata('profile_id');
				$login=array(
				
				'or_login_pwd'=>trim($this->input->post('oldpassword'))
				);
				$this->db->where('or_user_id',$pid);
				//$this->db->update('tr04_login',$login);
				header("Location:".base_url()."index.php/client/change_password");
			}
		}
		
		
		public function mydetail_update()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('e_email')=="")
			{
				header("Location:".base_url()."index.php/client/logout");
			}
			else
			{
				$pid=$this->session->userdata('profile_id');
				$data=array(
				'txtfname'=>$this->input->post('txtfname'),
				'txtlname'=>$this->input->post('txtlname'),
				'txtcompany'=>$this->input->post('txtcompany'),
				'txtemail'=>$this->input->post('txtemail'),
				'txtcountry'=>$this->input->post('txtcountry'),
				'txtstate'=>$this->input->post('txtstate'),
				'txtcity'=>$this->input->post('txtcity'),
				'txtlocality'=>$this->input->post('txtlocality'),
				'txtzip'=>$this->input->post('txtzip'),
				'txtphone'=>$this->input->post('txtphone'),
				'txtcurrency'=>$this->input->post('txtcurrency'),
				'txtpaymentmethod'=>$this->input->post('txtpaymentmethod'),
				'txtbillingcid'=>$this->input->post('txtbillingcid')
				);
				
				$data1=array(
				'txtemail'=>$this->input->post('txtemail')
				);
				$this->db->where('m02_id',$pid);
				$this->db->update('m06_user_detail',$data);
				$this->db->where('tr01_id',$pid);
				//$this->db->update('tr04_login',$data1);
				header("Location:".base_url()."index.php/client/index");
			}
		}
		
		
		public function ticket()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$this->db->where('m_des_id',$this->session->userdata('designation'));
			$this->db->where('m_dm_status',1);
			$data['assign']=$this->db->get('m19_des_menu');
			$this->db->where('m_dsm_status',1);
			$data['smenu']=$this->db->get('m20_des_smenu');
			$this->db->where('m_menu_status',1);
			$data['menu1']=$this->db->get('m01_menu');
			$this->db->where('m_submenu_status',1);
			$data['menu2']=$this->db->get('m02_submenu');
			$this->load->view('client/header');
			$this->load->view('client/menu',$data);
			$this->load->view('client/ticket_type');
			$this->load->view('client/header');
		}
		
		
		public function submit_ticket()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('e_email')=="")
			{
				header("Location:".base_url()."index.php/client/logout");
			}
			else
			{
				$p_id=$this->session->userdata('profile_id');
				$this->db->where('contact_id',$p_id);
				$data['rec']=$this->db->get('m34_contact');
				$dep=$this->uri->segment(3);
				$data['dep']=$dep;
				
				$this->db->where('m_des_id',$this->session->userdata('designation'));
				$this->db->where('m_dm_status',1);
				$data['assign']=$this->db->get('m19_des_menu');
				$this->db->where('m_dsm_status',1);
				$data['smenu']=$this->db->get('m20_des_smenu');
				$this->db->where('m_menu_status',1);
				$data['menu1']=$this->db->get('m01_menu');
				$this->db->where('m_submenu_status',1);
				$data['menu2']=$this->db->get('m02_submenu');
				$this->load->view('client/header');
				$this->load->view('client/menu',$data);
				$this->load->view('client/submit_ticket',$data);
				$this->load->view('client/footer');
			}
		}
		
		
		
		
		
		
		/*public function resolve()
			{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('e_email')=="")
			{
			header("Location:".base_url()."index.php/client/logout");
			}
			else
			{
			$ticket_no=$this->input->post('ticket_no');
			$data=array(
			'txtstatus'=>$this->input->post('txtstatus'),
			);
			$this->db->where('ticket_no',$ticket_no);
			$this->db->update('tr20_submit_ticket',$data);;
			header("Location:".base_url()."index.php/client/index");
			}
		}*/
		
		
		public function announcements()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('e_email')=="")
			{
				header("Location:".base_url()."index.php/client/logout");
			}
			else
			{
				$t=array('2','3');
				$this->db->where_in('for',$t);
				$data['rec']=$this->db->get('tr19_anoncement');
				$this->db->where('m_des_id',$this->session->userdata('designation'));
				$this->db->where('m_dm_status',1);
				$data['assign']=$this->db->get('m19_des_menu');
				$this->db->where('m_dsm_status',1);
				$data['smenu']=$this->db->get('m20_des_smenu');
				$this->db->where('m_menu_status',1);
				$data['menu1']=$this->db->get('m01_menu');
				$this->db->where('m_submenu_status',1);
				$data['menu2']=$this->db->get('m02_submenu');
				$this->load->view('client/header');
				$this->load->view('client/menu',$data);
				$this->load->view('client/announcements',$data);
				$this->load->view('client/footer');
			}
		}
		
		
		public function full_Announcements()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('e_email')=="")
			{
				header("Location:".base_url()."index.php/client/logout");
			}
			else
			{
				$p_id=$this->uri->segment(3);
				$this->db->where('id',$p_id);
				$data['rec']=$this->db->get('tr19_anoncement');
				$this->db->where('m_des_id',$this->session->userdata('designation'));
				$this->db->where('m_dm_status',1);
				$data['assign']=$this->db->get('m19_des_menu');
				$this->db->where('m_dsm_status',1);
				$data['smenu']=$this->db->get('m20_des_smenu');
				$this->db->where('m_menu_status',1);
				$data['menu1']=$this->db->get('m01_menu');
				$this->db->where('m_submenu_status',1);
				$data['menu2']=$this->db->get('m02_submenu');
				$this->load->view('client/header');
				$this->load->view('client/menu',$data);
				$this->load->view('client/full_Announcements',$data);
				$this->load->view('client/footer');
			}
		}
		
		
		public function view_account_profile()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->db->where('m_des_id',$this->session->userdata('designation'));
				$this->db->where('m_dm_status',1);
				$data['assign']=$this->db->get('m19_des_menu');
				$this->db->where('m_dsm_status',1);
				$data['smenu']=$this->db->get('m20_des_smenu');
				$this->db->where('m_menu_status',1);
				$data['menu1']=$this->db->get('m01_menu');
				$this->db->where('m_submenu_status',1);
				$data['menu2']=$this->db->get('m02_submenu');
				
				$this->db->where('account_id',$this->session->userdata('profile_id'));
				$data['acc']=$this->db->get('m33_account');
				$data['loc']=$this->db->get('m05_location');
				$this->db->where('m_acc_id',$this->session->userdata('profile_id'));
				$data['pro']=$this->db->get('m43_acc_project');
				$data['ticket']=$this->db->get('tr20_submit_ticket');
				$t=array('2','3');
				$this->db->where_in('for',$t);
				$data['anonce']=$this->db->get('tr19_anoncement');
				$this->load->view('client/header');
				$this->load->view('client/menu',$data);
				$this->load->view('client/view_accountprofile',$data);
				$this->load->view('client/footer');
			}
			else
			{
				header("Location:".base_url()."index.php/client/logout");
			}
		}
		public function change_account_image()
		{
			$config['upload_path']   =   "application/uploadimage/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "1907";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
			
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$this->load->helper('url');
			$this->load->library('session');
			$this->load->database();
			$fileupload=$finfo['raw_name'].$finfo['file_ext'];
			$id=$this->uri->segment(3);
			if($fileupload=="")
			{
				$fi="";
			}
			else
			{
				$fi=$fileupload;
			}
			$data=array(
			'account_image'=>$fi,
			);
			$this->db->where('account_id',$this->session->userdata('profile_id'));
			$this->db->update('m33_account',$data);
			header("Location:".base_url()."index.php/client/view_account_profile/".$id);
			
		}
		
		
		
		public function logout()
		{
			$this->load->helper('url');
			$this->load->library('session');
			$this->session->unset_userdata('profile_id');
			$this->session->unset_userdata('user_id');
			$this->session->unset_userdata('e_email');
			$this->session->unset_userdata('name');
			$this->session->unset_userdata('designation');
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
			$data['info']=0;
			$this->load->view('auth/header');
			$this->load->view('auth/login',$data);
			$this->load->view('auth/footer');
		}
		
	}
	
?>																	