<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Welcome extends CI_Controller {
		
		public function __construct() 
		{
			parent::__construct();
			$timezone = new DateTimeZone("Asia/Kolkata");
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
		   	//$this->_is_logged_in();
		   	//echo base_url();die();
		}
		
		public function read_mail()
		{
			$this->load->library('email_reader');
			$er=new Email_reader();
			var_dump($er->connect());
		}
		
		public function is_affiliate()
		{
			if($this->uri->segment(3)=="")
			{
				$data['info']="";
			}
			else
			{
				$data['info']=$this->uri->segment(3);
			}
			if(base_url()!="http://innovativetech.tech/sales/CRM/")
			{
				$data['aff_id']=$this->db->query("SELECT GetAffiliateId('".base_url()."',1) as Aff_id");
				foreach($data['aff_id']->result() as $row1) {break;}
				echo $row1->Aff_id;die();
				if($row1->Aff_id==base_url())
				{
					echo $row1->Aff_id.'-'."THIS URL IS NOT REGISTERED .Please Contact Admin";
				}
				else
				{
					$data['affiliates']=$row1->Aff_id;
					$this->db->where('affiliate_id',$row1->Aff_id);
					$data['aff_detail']=$this->db->get('m22_affiliate');
				}
			}
			else
			{
				$data['affiliates']=0;
				$this->db->where('affiliate_id',0);
			    $data['aff_detail']=$this->db->get('m22_affiliate');
			}
			return $data;
		}
		
		public function index()
		{
			/*$this->load->view('Front/header');
		    $this->load->view('Front/menu');
			$this->load->view('Front/index');
			$this->load->view('Front/footer');*/
			//echo 'hello';die();
			header("Location:".base_url()."auth/index/");
		}
		public function help_desk()
		{
			$data=$this->is_affiliate();
			$this->load->view('header',$data);
			$this->load->view('Outer_Ticket/menu',$data);
			$this->load->view('Outer_Ticket/ticket_type',$data);
			$this->load->view('footer',$data);
		}
		/*Error Controller*/
		public function error_404()
		{
			$this->load->view('header');
			$this->load->view('menu_404');
			$this->load->view('404');
			$this->load->view('footer');
		}
		public function get_city()
		{
			$query=$this->db->query("SELECT * FROM `m05_location` WHERE `m05_location`.`m_status`='1' AND `m05_location`.`m_parent_id`='".$this->input->post('ddstate')."'");
			$json=json_encode($query->result());
			echo $json;
		}
		
		public function spcl()
		{
			$query=$this->db->query("SELECT * FROM `m03_designation` WHERE `m03_designation`.`m_des_status`='1' AND `m03_designation`.`m_des_pat_id`='".$this->input->post('dddesignation')."'");
			$json=json_encode($query->result());
			echo $json;
		}
		/*Front View*/
		public function about()
		{
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			$this->load->view('Front/about');
			$this->load->view('Front/footer');
		}
		
		public function services()
		{
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			$this->load->view('Front/services');
			$this->load->view('Front/footer');
		}
		
		public function case_studies()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			$this->load->view('Front/case-studies');
			$this->load->view('Front/footer');
		}
		public function contact()
		{
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			$this->load->view('Front/contact');
			$this->load->view('Front/footer');
		}
		public function aarjavam()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			$this->load->view('Front/aarjavam');
			$this->load->view('Front/footer');
		}
		public function techaarjavam()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			$this->load->view('Front/techaarjavam');
			$this->load->view('Front/footer');
		}
		public function leadership()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			$this->load->view('Front/contact');
			$this->load->view('Front/footer');
		}
		public function solution()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			//$this->load->view('Front/contact');
			$this->load->view('Front/footer');
		}
		public function mobolitysolution()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			$this->load->view('Front/mobile-app');
			$this->load->view('Front/footer');
		}
		public function analytics()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			$this->load->view('Front/analytics');
			$this->load->view('Front/footer');
		}
		public function erp()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			$this->load->view('Front/erp');
			$this->load->view('Front/footer');
		}
		public function enterprises()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			$this->load->view('Front/enterprise');
			$this->load->view('Front/footer');
		}
		public function digitaltrans()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			$this->load->view('Front/digital-transformation');
			$this->load->view('Front/footer');
		}
		public function outsourcing()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			//$this->load->view('Front/contact');
			$this->load->view('Front/footer');
		}
		public function servicegroup()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			$this->load->view('Front/business-group');
			$this->load->view('Front/footer');
		}
		public function platforms()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			//$this->load->view('Front/contact');
			$this->load->view('Front/footer');
		}
		public function salesbooster()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			//$this->load->view('Front/contact');
			$this->load->view('Front/footer');
		}
		public function braandmaker()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			//$this->load->view('Front/contact');
			$this->load->view('Front/footer');
		}
		public function metroheights()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			//$this->load->view('Front/contact');
			$this->load->view('Front/footer');
		}
		public function onebox()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			//$this->load->view('Front/contact');
			$this->load->view('Front/footer');
		}
		
		public function offerings()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			//$this->load->view('Front/contact');
			$this->load->view('Front/footer');
		}
		public function banking()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			//$this->load->view('Front/contact');
			$this->load->view('Front/footer');
		}
		public function retail()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			//$this->load->view('Front/contact');
			$this->load->view('Front/footer');
		}
		public function manufacturing()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			//$this->load->view('Front/contact');
			$this->load->view('Front/footer');
		}
		public function technomedia()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			//$this->load->view('Front/contact');
			$this->load->view('Front/footer');
		}
		public function travel()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			//$this->load->view('Front/contact');
			$this->load->view('Front/footer');
		}
		public function psu()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			//$this->load->view('Front/contact');
			$this->load->view('Front/footer');
		}
		
		
		public function get_freequote()
		{
			$data=array(
			'tr_qoute_user_name'=>$this->input->post('txtname'),
			'tr_qoute_mobile'=>$this->input->post('txtmobile'),
			'tr_qoute_query'=>$this->input->post('txtmsgquery'),
			'tr_quote_stataus'=>1,
			'tr_qoute_date'=>$this->curr
			);
			$this->db->insert('tr30_get_quotesdetails',$data);
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL,'Techaarjavam'); // change it to yours
			$this->email->to('');// change it to yours
			$this->email->subject('Free Quote Request');
			$this->email->message("My Name is '".$this->input->post('txtname')."' and my contact no is '".$this->input->post('txtmobile')."' And query is '".$this->input->post('txtmsgquery')."' ");
			if($this->email->send())
			{
				echo "Your Query is recieved . We will responce soon";
			}
			else
			{
				echo show_error($this->email->print_debugger());
               //echo "There have some error . Please Try latter";
			}
		}

         

        public function get_freelead()
        {
				if($this->input->post('txtname')!="")
				{
				 $smenu=array(
					'proc'=>1,
					'lead_for'=>1,
					'lead_owner'=>0,
					'lead_created_by'=>0,
					'lead_company'=>'N/A',
					'lead_prefix'=>1,
					'lead_name'=>$this->input->post('txtname'),
					'lead_title'=>$this->input->post('txtsubject'),
					'lead_industry'=>0,
					'lead_emp'=>0,
					'lead_email'=>$this->input->post('txtemail'),
					'lead_mobile'=>$this->input->post('txtmobile'),
					'lead_source'=>7,
					'lead_current_status'=>3,
					'lead_description'=>$this->input->post('txtmessage'),
					'lead_address'=>'',
					'lead_city'=>'',
					'lead_state'=>'',
					'lead_zipcode'=>226041,
					'lead_country'=>'',
					'lead_status'=>1,
					'lead_logintype'=>'4',
					'lead_regdate'=>$this->curr
					);
					$query = " CALL sp_lead(?" . str_repeat(",?", count($smenu)-1) .",@a) ";
					$data['lead']=$this->db->query($query, $smenu);
					$this->db->free_db_resource();
					$data['response']=$this->db->query("SELECT @a as resp");
					foreach($data['response']->result() as $rows)
					{
					}
					$ids =$rows->resp;
					echo $ids;
				  }
        }

        
        /*------------------------- Send Task Mail -------------------------*/
		
        public function send_task_mail()  
        {
            $data=$this->db->query("CALL sp_due_task()");
			$this->load->library('email');
            foreach($data->result() AS $row)
            {
				$message="Dear ".$row->task_assignto_name."
				<br/> 
				This is to inform, that your coordination is required in resolving the Task assigned to you.
				<br/> You have ".$row->task_status." task which priority is ".$row->task_priority." to ".$row->type." to ".$row->name." 
				<br/>
				Thank You.";
				$config['mailtype'] = 'html';
				$this->email->set_newline("\r\n");
				$this->email->subject("TechAarjavam Sales Task".$row->subject);
				$this->email->message($message);
				$this->email->from(EMAIL,'Support TechAarjavam'); // change it to yours
				$this->email->to($row->email_id);// change it to yours
			    if($this->email->send())
				{
				    //echo $this->email->send();
                    echo $row->email_id;
				}
				else
				{
				    show_error($this->email->print_debugger());
				}
			}
		}
		/*-------------------------- Send Task Mail -------------------------*/
		

/*-------------------------------------------Term of use---------------------------*/
        public function termofuse()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			$this->load->view('Front/termofuse');
			$this->load->view('Front/footer');
		}
/*-------------------------------------Privacy Policy------------------------------*/
		public function privacypolicy()
		{
			
			$this->load->view('Front/header');
			$this->load->view('Front/menu');
			$this->load->view('Front/privacypolicy');
			$this->load->view('Front/footer');
		}	
}?>