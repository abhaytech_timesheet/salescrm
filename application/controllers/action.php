<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Action extends CI_Controller {
		
		/*-----------------------------Project Constructor-----------------------------*/
		
		public function __construct() 
		{
			parent::__construct();
            $this->_is_logged_in();
			//$this->load->model('project_model');
			$this->data1=$this->crm_model->assign_menu();
		}
		
		/*Session Check Function */ 
		public function _is_logged_in() 
		{
		    $is_logged_in = $this->session->userdata('profile_id');
			if ($is_logged_in=="") 
			{
				redirect('auth/index/1');
				die();
			}
		}
		
		public function index()
		{
			$this->load->view('header.php');
			$this->load->view('menu.php');
			$this->load->view('footer.php');
		}
		
		/*-----------------------------Start Action API HERE-----------------------------*/
		
		public function change_password()
		{
			$data['red_page']='fetch_password'; //redirect to page
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Action/change_credit',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function get_member()
		{
			$id=$this->input->post('txtid');
			$data['red_page']=$page=$this->input->post('page');
			$query=$this->db->query("SELECT * FROM view_member WHERE or_m_user_id='".$id."'");
			if($query->num_rows()>0)
			{
				$data['record']=$query->row();
				$this->load->view('action/'.$page,$data);
			}
			else
			{
				echo $msg=0;
			}
		}
		
		public function update_password()
		{
			$id=$this->input->post('txtid');
			$regid=base64_decode($this->uri->segment(3));
			$data=array(
			'or_login_pwd'=>$this->input->post('txtnewpass')
			);
			$this->db->where('or_user_id',$regid);
			//$this->db->update('tr04_login',$data);
			$data['record']=$this->db->query("SELECT * FROM view_member WHERE or_m_user_id='".$id."'")->row();
			echo $msgs = true;
		}
		
		public function change_mobile()
		{
			$data['red_page']='fetch_mobile'; //redirect to page
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Action/change_credit',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function update_mobile()
		{
			$id=$this->input->post('txtid');
			$regid=base64_decode($this->uri->segment(3));
			$data=array(
			'or_m_mobile_no'=>$this->input->post('txtmob')
			);
			$this->db->where('or_m_reg_id',$regid);
			$this->db->update('m06_user_detail',$data);
			$data['record']=$this->db->query("SELECT * FROM view_member WHERE or_m_user_id='".$id."'")->row();
			echo $msgs = true;
		}
		
		public function change_intro()
		{
			$data['red_page']='fetch_intro'; //redirect to page
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Action/change_credit',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function update_intro()
		{
			$id=$this->input->post('txtid');
			$introid=$this->input->post('txtintro');
			if($introid != 0 || $introid !="")
			{
				$intro=$this->db->query("SELECT `or_m_reg_id` FROM `m06_user_detail` WHERE `or_m_user_id`='".$introid."'")->row()->or_m_reg_id;
				$intro_name=$this->db->query("SELECT `or_m_name` FROM `m06_user_detail` WHERE `or_m_user_id`='".$introid."'")->row()->or_m_name;
			}
			else
			{
				$intro_name= $this->session->userdata('name');
			}
			$data=array(
			'or_m_intr_id'=>$intro,
			'or_m_intr_name'=> $intro_name
			);

			$this->db->where('or_m_user_id',$id);
			$this->db->update('m06_user_detail',$data);
			$data['record']=$this->db->query("SELECT * FROM view_member WHERE or_m_user_id='".$id."'")->row();
			echo 1;
		}
		
		
		
		public function active_mem()
		{
			$data['page_name']='Active';
			$condition=' ';
			if($this->input->post('txtid')!="")
			{
				$condition=$condition."or_m_user_id= '".$this->input->post('txtid')."' and ";
			}
			if($this->input->post('dddes')!="-1" && $this->input->post('dddes')!="")
			{
				$condition=$condition."or_m_designation= ".$this->input->post('dddes')." and ";
			}
			$condition=$condition.'`or_m_status`=1';
			
			$data['desig']=$this->db->get_where('m03_designation',array('m_des_status'=>1));
			$data['record']=$this->db->query("SELECT * FROM view_member WHERE".$condition);
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Action/active_mem',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		public function deactive_mem()
		{
			$data['page_name']='Deactive';
			$condition=' ';
			if($this->input->post('txtid')!="")
			{
				$condition=$condition."or_m_user_id= '".$this->input->post('txtid')."' and ";
			}
			if($this->input->post('dddes')!="-1" && $this->input->post('dddes')!="")
			{
				$condition=$condition."or_m_designation= ".$this->input->post('dddes')." and ";
			}
			$condition=$condition.'`or_m_status`=0';
			
			$data['desig']=$this->db->get_where('m03_designation',array('m_des_status'=>1));
			$data['record']=$this->db->query("SELECT * FROM view_member WHERE".$condition);
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Action/active_mem',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		public function change_memst()
		{
			$chk=trim($this->input->post('chkid'),',');
			$status=$this->uri->segment(3);
			if($chk != '')
			{
				$this->db->query("UPDATE `m06_user_detail` SET `m06_user_detail`.`or_m_status`=".$status." WHERE `m06_user_detail`.`or_m_reg_id` IN (".$chk.")");
			}
		}
		public function allow_capping()
		{
			$condition=' ';
			if($this->input->post('txtid')!="")
			{
				$condition=$condition."or_m_user_id= '".$this->input->post('txtid')."' and ";
			}
			if($this->input->post('dddes')!="-1" && $this->input->post('dddes')!="")
			{
				$condition=$condition."or_m_designation= ".$this->input->post('dddes')." and ";
			}
			$condition=$condition.'`or_m_status`=1';
			
			$data['desig']=$this->db->get_where('m03_designation',array('m_des_status'=>1));
			$data['record']=$this->db->query("SELECT * FROM view_member WHERE".$condition);
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Action/allow_capping',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function update_cap()
		{
			$cid=$this->input->post('txtcap');
			$stack = explode('-',$cid);
			foreach($stack as $id)
			{
				if($id != "")
				{
					$cap = explode(',',$id);
					$data=array(
					'm_cap_amt'=>$cap[0]
					);
					$this->db->where('m_curru_id',$cap[1]);
					$this->db->update('tr12_curr_bal',$data);
				}
			}
			echo "Updated Successfully";
		}
	}