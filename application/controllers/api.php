<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Api extends CI_Controller {
		
		/*-----------------------------Project Constructor-----------------------------*/
		
		public function __construct() 
		{
			parent::__construct();
            $this->_is_logged_in();
			//$this->load->model('project_model');
			$this->data1=$this->crm_model->assign_menu();
		}
		
		/*Session Check Function */ 
		public function _is_logged_in() 
		{
		    $is_logged_in = $this->session->userdata('profile_id');
			if ($is_logged_in=="") 
			{
				redirect('auth/index/1');
				die();
			}
		}
		
		public function index()
		{
			$this->load->view('header.php');
			$this->load->view('menu.php');
			$this->load->view('footer.php');
		}
		
		/*-----------------------------Start View API HERE-----------------------------*/
		
		public function manage_api()
		{
			
			$data['rec']=$this->db->get('m12_api');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Api/manage_api',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function add_api()
		{
			$this->apimodel->add_api();
			header("location:".base_url()."index.php/api/manage_api");
		}
		
		public function edit_api()
		{
			$data['rec']=$this->db->get_where('m12_api',array('m_api_id' => $this->uri->segment(3)))->row();
			//print_r($data);die;
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Api/edit_api',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function change_status()
		{
			$this->apimodel->change_status();
			header("location:".base_url()."index.php/api/manage_api");
		}
		
		public function update_api()
		{
			$this->apimodel->update_api();
			header("location:".base_url()."index.php/api/manage_api");
		}
		
		public function manage_api_uri()
		{
			
			$data['rec']=$this->db->get('m12_api');
			$data['api']=$this->db->get('view_api_url');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Api/manage_api_uri',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function add_api_url()
		{
			$this->apimodel->add_api_url();
			header("location:".base_url()."index.php/api/manage_api_uri");
		}	
		public function stch_api_url()
		{
			$this->apimodel->stch_api_url();
			header("location:".base_url()."index.php/api/manage_api_uri");
		}	
		public function edit_api_uri()
		{
			
			$data['rec']=$this->db->get('m12_api');
			$data['api']=$this->db->get_where('m16_api_url',array('m_api_url_id' => $this->uri->segment(3)))->row();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Api/edit_api_uri',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function update_api_url()
		{
			$data=$this->apimodel->update_api_url();
			header("location:".base_url()."index.php/api/manage_api_uri");
		}
		
		public function manage_api_services()
		{
			
			$data['rec']=$this->db->get_where('m12_api',array('m_api_status' => 1));
			
			$this->db->where('Service_status',1);
			$data['services']=$this->db->get('view_service');
			$data['api_service']=$this->db->get('view_api_service');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Api/manage_api_services',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function add_api_service()
		{ 
			$data=$this->apimodel->add_api_service();
			echo $data;
		}
		
		public function edit_api_services()
		{
			$this->db->where('Service_id',$this->uri->segment(3));
			$this->db->where('Service_status',1);
			$data['apiservice']=$this->db->get('view_api_service')->row();
			
			$data['rec']=$this->db->get_where('m12_api',array('m_api_status' => 1));
			
			$this->db->where('Service_status',1);
			$data['services']=$this->db->get('view_service');
			//print_r($data);die;
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Api/edit_api_services',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function update_api_service()
		{
			$data=$this->apimodel->update_api_service();
			echo $data;
		}
		
		public function stch_api_services()
		{
			$this->apimodel->stch_api_services();
			header("location:".base_url()."index.php/api/manage_api_services");
		}
		
		public function manage_api_service_op()
		{
			
			$data['rec']=$this->db->get_where('m12_api',array('m_api_status' => 1));
			$data['services']=$this->db->get('view_service');
			$data['api_service']=$this->db->get('view_api_service');
			
			$data['api_operator']=$this->db->get('view_service_opt_report');
			$data['sp']=$this->db->get('view_service_provider');
			
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Api/manage_api_service_op',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function select_st()
		{
			$data=$this->apimodel->select_st();
			echo $data;
		}
		public function select_srv_provider()
		{
			$json=$this->apimodel->select_srv_provider();
			echo $json;
		}
		public function add_api_operator()
		{
			$this->apimodel->add_api_operator();
			header("location:".base_url()."index.php/api/manage_api_service_op");
		}
		
		public function edit_api_operator()
		{
			$id=$this->uri->segment(3);
			$this->db->where('So_id',$id);
			$data['api_operator']=$this->db->get('view_service_opt_report');
			$data['id']=$id;
			$this->load->view('Api/edit_api_ser_op',$data);
		}
		
		public function st_chserop()
		{
			$data=$this->apimodel->st_chserop();
			header("location:".base_url()."index.php/api/manage_api_service_op");
		}
		
		public function update_service_opt()
		{
			$data=$this->apimodel->update_service_opt();
			echo $data;
		}
		
		public function genrate_api()
		{
			$data['record']=$this->db->query("SELECT * FROM `view_member` WHERE `view_member`.`or_m_status`=1 AND or_m_designation IN (23,24)");
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Api/genrate_api',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function generate_apiuser()
		{
			$id=$this->uri->segment(3);
			$data['record']=$this->db->query("SELECT * FROM `view_member` WHERE or_m_designation IN (23,24) AND `view_member`.or_m_reg_id=".$id)->row();
			$query="SELECT * FROM m22_affiliate WHERE affiliate_mreg_id=".$id;
			if($query->num_rows())
			{
				$data['aff']=$this->db->query();
			}
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('Api/edit_generate_api',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
	}
?>