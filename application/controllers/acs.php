<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Acs extends CI_Controller {
		
		public function __construct() 
		{
			parent::__construct();
			
		
		    $timezone = new DateTimeZone("Asia/Kolkata");
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
		    
			$this->load->model('acs_model');
			
			$this->_is_logged_in();
			$this->data1=$this->crm_model->assign_menu();
			
		}
		
		/*Session Check Function */ 
		public function _is_logged_in() 
		{
		    $is_logged_in = $this->session->userdata('profile_id');
			if ($is_logged_in=="") 
			{
				redirect('auth/index/1');
				die();
			}
		}
		
		
		public function index()
		{
			header("Location:".base_url()."index.php/ACS/view_mainconfig");
		}
		
		//Config
		public function view_mainconfig()
		{
			$data['form_name']="Here you can set Configuration";
			$data['config']= $this->Acs_model->select_config();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('ACS/view_mainconfig',$data);
			$this->load->view('ACS/footer',$data);
		}
		
		public function update_mainconfig()
		{
			$data['form_name']="Here you can set Configuration";
			$this->Acs_model->update_config();
			header("Location:".base_url()."index.php/ACS/view_mainconfig");
		}
		
		//Manage City Add,Update,Status Change Function 
		public function view_city()
		{
			$data['form_name']="Manage City";
			$data['city']=$this->Acs_model->select_state(1);
			$data['allcity']=$this->Acs_model->select_state(0);
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('ACS/view_city',$data);
			$this->load->view('ACS/footer',$data);	
		}	
		public function select_city()
		{
			$data['city_id']=$this->uri->segment(4);
			$data['city']=$this->Acs_model->select_state($this->uri->segment(3));
			$this->load->view('ACS/view_select_city',$data);
		}
		public function select_aff_city()
		{
			$data['city']=$this->Acs_model->select_state($this->uri->segment(3));
			$data['city_id']=$this->uri->segment(4);
			$this->load->view('ACS/view_select_branch_city',$data);
		}
		public function add_city()
		{
			$this->Acs_model->add_city();
			header("Location:".base_url()."index.php/ACS/view_city");
			
		}
		
		public function update_status_city()
		{
			$id=$this->uri->segment(3);
			$this->db->where('m_loc_id',$id);
			$city=array(
			'm_status'=>$this->uri->segment(4)
			);
			$this->db->update('m05_location',$city);
			header("Location:".base_url()."index.php/ACS/view_city");
		}
		public function edit_city()
		{
			$data['form_name']="Manage City";
			$id=$this->uri->segment(3);
			$data['id']=$id;	
			$data['edit_city']=$this->Acs_model->select_city($id);
			$data['city']=$this->Acs_model->select_state(1);
			$data['allcity']=$this->Acs_model->select_state(0);
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('ACS/view_edit_city',$data);
			$this->load->view('ACS/footer',$data);
		}
		
		public function update_city()
		{
			$id=$this->uri->segment(3);
			$data['id']=$id;
			$this->Acs_model->update_city($id);
			header("Location:".base_url()."index.php/ACS/edit_city/$id/");
		}
		//End City ACS All Action related 
		
		//Start Session Panel 
		public function view_session()
		{
		   
			//$this->db->where('m_session_status',1);
			$data['sessionss']=$this->db->get('m49_session');
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('ACS/view_session',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function view_session1()
		{
			
			//$this->db->where('m_session_status',1);
			$data['sessionss']=$this->db->get('m49_session');
			$this->load->view('ACS/view_session',$data);
			$this->load->view('footer_11');
			
		}
		
		public function add_session()
		{
			
			$from = explode("-",$this->input->post('txtfromdate'));
			$to = explode("-",$this->input->post('txttodate'));
			$session = $from[0]."-".$to[0];
			$query = $this->db->query("SELECT YEAR(STR_TO_DATE(`m_session_end`, '%Y-%m-%d')) AS endss FROM `m49_session` WHERE `m_session_id` = (SELECT MAX(`m_session_id`) FROM `m49_session`)");
		
			if($query->num_rows()==0)
			{
				$clsdt = array(
                'proc'=>1,
				'm_session'=>$session,
				'm_session_start'=>$this->input->post('txtfromdate'),
				'm_session_end'=>$this->input->post('txttodate'),
				'm_session_affid'=>1,
				'm_session_status'=>$this->input->post('rbrank')
				);	
               //var_dump($clsdt);
				$query ="CALL sp_insert_session(?" . str_repeat(",?", count($clsdt)-1) . ") ";
				$data['cl_dt']=$this->db->query($query,$clsdt);	
				$this->db->free_db_resource();
				echo 1;
			}
			else
			{
				if($from[0] == $end)
				{
					$clsdt = array(
					'proc'=>1,
					'm_session'=>$session,
					'm_session_start'=>$this->input->post('txtfromdate'),
					'm_session_end'=>$this->input->post('txttodate'),
					'm_session_affid'=>1,
					'm_session_status'=>$this->input->post('rbrank')
					);	
					$query ="CALL sp_insert_session(?" . str_repeat(",?", count($clsdt)-1) . ") ";
					$data['cl_dt']=$this->db->query($query,$clsdt);	
					$this->db->free_db_resource();
					echo 1;
				}
				else 
				{
					echo 0;
				}
			}
			
			
		}
		
		public function edit_session()
		{
			$id=$this->uri->segment(3);
			$sess=$this->uri->segment(4);
			$clsdt = array(
			'proc'=>2,
			'm_session'=>$id,
			'm_session_start'=>'',
			'm_session_end'=>'',
			'm_session_affid'=>1,
			'm_session_status'=>$sess
			);
			$query ="CALL sp_insert_session(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$data['sessionss']=$this->db->get('m49_session');
			$this->load->view('ACS/view_session',$data);
			$this->load->view('footer_11');
		}
		//End Session Panel 
		
		//Start Department Panel 	
		
		public function view_manage_department()
		{
			
			$data['form_name']="Manage Designation/Rank";
			$this->load->model('Acs_model');
			$data['desig']=$this->Acs_model->select_designation();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('ACS/view_designation',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}	
		
		public function view_manage_department1()
		{
			$data['desig']=$this->Acs_model->select_designation();
			$this->load->view('ACS/view_designation',$data);
			$this->load->view('footer_11');
			
		}	
		
		public function add_designation()
		{
			$clsdt=array(
			'proc'=>1,
			'm_des_id'=>'',
			'm_des_name'=>trim($this->input->post('txtdesignation')), 
			'm_des_pat_id'=>0, 
			'm_des_user_id'=>0,
			'm_des_orid'=>0, 
			'm_des_status'=>trim($this->input->post('txtstatus'))
			);
			$query ="CALL sp_insert_department(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			echo 1;
		}
		
		public function edit_designation()
		{
			$id=$this->uri->segment(3);
			$data['id']=$id;	
			$data['rank']=$this->Acs_model->select_rank($id);
			$data['desig']=$this->Acs_model->select_designation();
			$this->load->view('ACS/view_edit_designation',$data);
			$this->load->view('footer_11');
			
		}
		
		public function update_designation()
		{
			
			$id=$this->input->post('id');
			$clsdt=array(
			'proc'=>2,
			'm_des_id'=>$id,
			'm_des_name'=>trim($this->input->post('txtdesignation')), 
			'm_des_pat_id'=>'', 
			'm_des_user_id'=>'',
			'm_des_orid'=>'', 
			'm_des_status'=>trim($this->input->post('txtstatus'))
			);
			$query ="CALL sp_insert_department(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			echo 1;
			
		}
		public function stch_designation()
		{
			
			$id = $this->uri->segment(3);
			$status = $this->uri->segment(4);
			$clsdt=array(
			'proc'=>3,
			'm_des_id'=>$id,
			'm_des_name'=>'', 
			'm_des_pat_id'=>'', 
			'm_des_user_id'=>'',
			'm_des_orid'=>'', 
			'm_des_status'=>$status
			);
			$query ="CALL sp_insert_department(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->model('Acs_model');
			$data['desig']=$this->Acs_model->select_designation();
			$this->load->view('ACS/view_designation',$data);
			$this->load->view('footer_11');
			
		}
		
		public function duplicate_department()
		{
			
			$name = $this->input->post('txtdesignation');
			$query = "Select count(m_des_id) as counts from m03_designation where m_des_pat_id = '-2' AND m_des_name = '$name'";
			$data['val'] = $this->db->query($query);
			foreach($data['val']->result() as $row)
			{
				break;
			}
			echo $row->counts;
			
		}
		
		//Start Roles
		public function view_manage_role()
		{
			
			$this->load->model('Acs_model');
			$data['department']=$this->Acs_model->select_designation();
			$data['roles']=$this->Acs_model->select_roles();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('ACS/view_roles',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}	
		
		public function view_manage_role1()
		{
			$this->load->model('Acs_model');
			$data['department']=$this->Acs_model->select_designation();
			$data['roles']=$this->Acs_model->select_roles();
			$this->load->view('ACS/view_roles',$data);
			$this->load->view('footer_11');
		}	
		public function add_roles()
		{
			$clsdt=array(
			'proc'=>1,
			'm_des_id'=>'',
			'm_des_name'=>$this->input->post('txtrole'), 
			'm_des_pat_id'=>$this->input->post('dddepartment'), 
			'm_des_user_id'=>0,
			'm_des_orid'=>0, 
			'm_des_status'=>$this->input->post('txtstatus')
			);
			$query ="CALL sp_insert_roles(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			echo 1;
		}
		
		public function edit_roles()
		{
			$id=$this->uri->segment(3);
			$data['id']=$id;	
			$data['rank']=$this->Acs_model->select_rank($id);
			$data['department']=$this->Acs_model->select_designation();
			$data['roles']=$this->Acs_model->select_roles();
			$this->load->view('ACS/view_edit_roles',$data);
			$this->load->view('footer_11');
		}
		
		public function update_roles()
		{
			
			$id=$this->input->post('id');
			$data['id']=$id;
			$clsdt=array(
			'proc'=>2,
			'm_des_id'=>$id,
			'm_des_name'=>$this->input->post('txtrole'), 
			'm_des_pat_id'=>$this->input->post('dddepartment'),
			'm_des_user_id'=>0,
			'm_des_orid'=>0, 
			'm_des_status'=>$this->input->post('txtstatus')
			);
			$query ="CALL sp_insert_roles(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			echo 1;
		}
		
		public function stch_roles()
		{
			
			$id=$this->uri->segment(3);
			$status = $this->uri->segment(4);
			$clsdt=array(
			'proc'=>3,
			'm_des_id'=>$id,
			'm_des_name'=>'', 
			'm_des_pat_id'=>'',
			'm_des_user_id'=>0,
			'm_des_orid'=>0, 
			'm_des_status'=>$status
			);
			$query ="CALL sp_insert_roles(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$data['department']=$this->Acs_model->select_designation();
			$data['roles']=$this->Acs_model->select_roles();
			$this->load->view('ACS/view_roles',$data);
			$this->load->view('footer_11');
			
		}
		
		
		public function duplicate_roles()
		{
			$department = $this->input->post('dddepartment');
			$name = $this->input->post('txtrole');
			$query = "Select count(m_des_id) as counts from m03_designation where m_des_pat_id = '$department' AND m_des_name = '$name' ";
			$data['val'] = $this->db->query($query);
			foreach($data['val']->result() as $row)
			{
				break;
			}
			echo $row->counts;
		}
		//End Roles
		
		//Start Allowance
		public function view_add_allowance()
		{
			$this->db->where('m_session_status',1);
			$data['session']=$this->db->get('m49_session');
			$clsdt= array(
			'proc'=>1,
			'allowance_id'=>1,
			'affid'=>1
			);
			$query ="CALL sp_allowance_on_session_basis(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['allowance']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('ACS/view_add_allowance',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
			
		}
		
		public function view_add_allowance1()
		{
			$this->db->where('m_session_status',1);
			$data['session']=$this->db->get('m49_session');
			$clsdt= array(
			'proc'=>1,
			'allowance_id'=>1,
			'affid'=>1
			);
			$query ="CALL sp_allowance_on_session_basis(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['allowance']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('ACS/view_add_allowance',$data);
			$this->load->view('footer_11');
		}
		
		public function add_allowances()
		{
			$parent = $this->input->post('ddsession');
			$query['check'] = $this->db->query("Select COUNT(m_allowance_sessionid) AS counts from m50_allowance  where m_allowance_sessionid='$parent'") ;
			foreach($query['check']->result() as $rows)
			{
				$val=$rows->counts;
			}
			if($val==0)
			{
				$clsdt = array(
				'proc'=>1,
				'm_allowance_id'=>0,
				'm_allowance_sessionid'=>$this->input->post('ddsession'),
				'm_allowance_da'=>$this->input->post('txtda'),
				'm_allowance_hra'=>$this->input->post('txthra'),
				'm_allowance_status'=>$this->input->post('txtstatus'),
				'm_allowance_affid'=>1,
				);	
				//var_dump($clsdt);
				$query ="CALL sp_insert_allowance(?" . str_repeat(",?", count($clsdt)-1) . ") ";
				$data['cl_dt']=$this->db->query($query,$clsdt);	
				$this->db->free_db_resource();
				echo 1;
			}
			else 
			{
				echo "false";
			}
			
		}
		
		public function edit_view_allowance()
		{
			$id=$this->uri->segment(3);
			$data['id']=$id;
			$this->db->where('m_session_status',1);
			$data['session']=$this->db->get('m49_session');
			$id=$this->uri->segment(3);
			$clsdt= array(
			'proc'=>2,
			'allowance_id'=>$id,
			'affid'=>1
			);
			$query ="CALL sp_allowance_on_session_basis(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['allowance']=$this->db->query($query,$clsdt);
			$this->db->free_db_resource();
			$clsdt= array(
			'proc'=>1,
			'allowance_id'=>1,
			'affid'=>1
			);
			$query ="CALL sp_allowance_on_session_basis(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['get_allowance']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('ACS/edit_view_allowance',$data);
			$this->load->view('footer_11');
		}
		
		public function update_allowance()
		{
			$id=$this->input->post('id');
			$data['id']=$id;
			$clsdt = array(
			'proc'=>2,
			'm_allowance_id'=>$id,
			'm_allowance_sessionid'=>$this->input->post('ddsession'),
			'm_allowance_da'=>$this->input->post('txtda'),
			'm_allowance_hra'=>$this->input->post('txthra'),
			'm_allowance_status'=>$this->input->post('txtstatus'),
			'm_allowance_affid'=>1,
			);	
			$query ="CALL sp_insert_allowance(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			echo 1;	
		}
		
		//End Allowance
		
		
		//Start Holidays
		public function view_add_holiday()
		{
			$this->db->where('m_session_status',1);
			$data['session']=$this->db->get('m49_session');
			$clsdt= array(
			'proc'=>1,
			'holiday_id'=>1,
			'affid'=>1
			);
			$query ="CALL sp_holiday_on_session_basis(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['holiday']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('ACS/view_add_holidays',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function view_add_holiday1()
		{
			$this->db->where('m_session_status',1);
			$data['session']=$this->db->get('m49_session');
			$clsdt= array(
			'proc'=>1,
			'holiday_id'=>1,
			'affid'=>1
			);
			$query ="CALL sp_holiday_on_session_basis(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['holiday']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('ACS/view_add_holidays',$data);
			$this->load->view('footer_11');
		}
		
		public function add_holidays()
		{
			$clsdt = array(
			'proc'=>1,
			'holiday_id'=>0,
			'session'=>$this->input->post('ddsession'),
			'name'=>$this->input->post('txtholidayname'),
			'begin'=>$this->input->post('txtstart'),
			'end'=>$this->input->post('txtend'),
			'amount'=>$this->input->post('txtamount'),
			'status'=>$this->input->post('txtstatus'),
			'affid'=>1,
			);	
			$query ="CALL sp_insert_holidays(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			echo 1;
			
		}
		
		public function hid_amount_of_holiday()
		{
			$start = $this->uri->segment(3);
			$end = $this->uri->segment(4);
			$date1 = date_create($start);
			$date2 = date_create($end);
			$diff=date_diff($date1,$date2);
			$data['date']=$diff->format("%R".","."%a");
			$this->load->view('ACS/hid_amount_of_holiday',$data);
			
		}
		
		public function view_edit_holiday()
		{
			$id=$this->uri->segment(3);
			$data['id']=$id;
			$this->db->where('m_session_status',1);
			$data['session']=$this->db->get('m49_session');
			$id=$this->uri->segment(3);
			$clsdt= array(
			'proc'=>2,
			'allowance_id'=>$id,
			'affid'=>1
			);
			$query ="CALL sp_holiday_on_session_basis(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['holiday']=$this->db->query($query,$clsdt);
			$this->db->free_db_resource();
			$clsdt= array(
			'proc'=>1,
			'allowance_id'=>1,
			'affid'=>1
			);
			$query ="CALL sp_holiday_on_session_basis(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['get_holiday']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('ACS/view_edit_holidays',$data);
			$this->load->view('footer_11');
		}
		
		public function update_holidays()
		{
			$id=$this->input->post('id');
			$data['id']=$id;
			$clsdt = array(
			'proc'=>2,
			'holiday_id'=>$id,
			'session'=>$this->input->post('ddsession'),
			'name'=>$this->input->post('txtholidayname'),
			'begin'=>$this->input->post('txtstart'),
			'end'=>$this->input->post('txtend'),
			'amount'=>$this->input->post('txtamount'),
			'status'=>$this->input->post('txtstatus'),
			'affid'=>1,
			);	
			$query ="CALL sp_insert_holidays(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			echo 1;
		}
		
		public function stch_holiday()
		{
			$id = $this->uri->segment(3);
			$status = $this->uri->segment(4);
			$clsdt=array(
			'proc'=>3,
			'holiday_id'=>$id,
			'session'=>'',
			'name'=>'',
			'begin'=>'',
			'end'=>'',
			'amount'=>'',
			'status'=>$status,
			'affid'=>1
			);
			$query ="CALL sp_insert_holidays(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->db->where('m_session_status',1);
			$data['session']=$this->db->get('m49_session');
			$clsdt= array(
			'proc'=>1,
			'holiday_id'=>1,
			'affid'=>1
			);
			$query ="CALL sp_holiday_on_session_basis(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['holiday']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('ACS/view_add_holidays',$data);
			$this->load->view('footer_11');
		}
		//End Holidays
		
		
		//Start Define Salary Scale
		public function view_define_salary_scale()
		{
			$this->db->where('m_session_status',1);
			$data['session']=$this->db->get('m49_session');
			$clsdt= array(
			'proc'=>1,
			'salsc_id'=>1,
			'affid'=>1
			);
			$query ="CALL sp_salary_scale_on_session_basis(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['salary_scale']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('ACS/view_manage_salary_scale',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function view_define_salary_scale1()
		{
			$this->db->where('m_session_status',1);
			$data['session']=$this->db->get('m49_session');
			$clsdt= array(
			'proc'=>1,
			'salsc_id'=>1,
			'affid'=>1
			);
			$query ="CALL sp_salary_scale_on_session_basis(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['salary_scale']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('ACS/view_manage_salary_scale',$data);
			$this->load->view('footer_11');
		}
		
		public function add_salary_scale()
		{
			$clsdt = array(
			'proc'=>1,
			'salsc_id'=>0,
			'salsc_sessionid'=>$this->input->post('ddsession'),
			'salsc_salary_by'=>$this->input->post('txtsalby'),
			'salsc_scale'=>$this->input->post('txtsalary'),
			'salsc_desc'=>$this->input->post('txtscaledesc'),
			'salsc_affid'=>1,
			'salsc_status'=>$this->input->post('txtstatus')
			);	
			$query ="CALL sp_insert_salary_scale(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			echo 1;
		}
		
		
		public function view_edit_salary_scale()
		{
			$id=$this->uri->segment(3);
			$data['id']=$id;
			$this->db->where('m_session_status',1);
			$data['session']=$this->db->get('m49_session');
			$id=$this->uri->segment(3);
			$clsdt= array(
			'proc'=>2,
			'salsc_id'=>$id,
			'affid'=>1
			);
			$query ="CALL sp_salary_scale_on_session_basis(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['sal']=$this->db->query($query,$clsdt);
			$this->db->free_db_resource();
			$clsdt= array(
			'proc'=>1,
			'salsc_id'=>1,
			'affid'=>1
			);
			$query ="CALL sp_salary_scale_on_session_basis(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['salary_scale']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('ACS/view_edit_salary_scale',$data);
			$this->load->view('footer_11');
		}
		
		public function update_salary_scale()
		{
			$id=$this->input->post('id');
			$data['id']=$id;
			$clsdt = array(
			'proc'=>2,
			'salsc_id'=>$id,
			'salsc_sessionid'=>$this->input->post('ddsession'),
			'salsc_salary_by'=>$this->input->post('txtsalby'),
			'salsc_scale'=>$this->input->post('txtsalary'),
			'salsc_desc'=>$this->input->post('txtscaledesc'),
			'salsc_affid'=>1,
			'salsc_status'=>$this->input->post('txtstatus')
			);	
			$query ="CALL sp_insert_salary_scale(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			echo 1;
		}
		
		public function stch_salscale()
		{
			$id = $this->uri->segment(3);
			$status = $this->uri->segment(4);
			$data['id']=$id;
			$clsdt = array(
			'proc'=>3,
			'salsc_id'=>$id,
			'salsc_sessionid'=>'',
			'salsc_salary_by'=>'',
			'salsc_scale'=>'',
			'salsc_desc'=>'',
			'salsc_affid'=>1,
			'salsc_status'=>$status 
			);	
			$query ="CALL sp_insert_salary_scale(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->db->where('m_session_status',1);
			$data['session']=$this->db->get('m49_session');
			$clsdt= array(
			'proc'=>1,
			'salsc_id'=>1,
			'affid'=>1
			);
			$query ="CALL sp_salary_scale_on_session_basis(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['salary_scale']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('ACS/view_manage_salary_scale',$data);
			$this->load->view('footer_11');
		}
		//End Define Salary Scale
		
		//Start Salary
		
		public function view_add_salary()
		{
			$this->db->where('m_des_pat_id',0);
			$data['department']=$this->db->get('m03_designation');
			$this->db->where('m_session_status',1);
			$clsdt= array(
			'proc'=>1,
			'sal_id'=>0,
			'department_id'=>0,
			'role_id'=>0,
			'affid'=>1
			);
			$query ="CALL sp_view_salary(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['salary']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('ACS/view_add_salary',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function view_add_salary1()
		{
			$this->db->where('m_des_pat_id',-2);
			$data['department']=$this->db->get('m03_designation');
			$clsdt= array(
			'proc'=>1,
			'sal_id'=>0,
			'department_id'=>0,
			'role_id'=>0,
			'affid'=>1
			);
			$query ="CALL sp_view_salary(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['salary']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('ACS/view_add_salary',$data);
			$this->load->view('footer_11');	
		}
		
		public function hid_get_roles()
		{
			$depart = $this->uri->segment(3);
			$datas= $this->db->query("Select * from m03_designation where m03_designation.m_des_pat_id='$depart'");
			$json=json_encode($datas->result());
			echo $json;
			//$this->load->view('ACS/hid_get_roles',$data);	
		}
		
		public function add_salary()
		{
			$clsdt = array(
			'proc'=>1,
			'sal_id'=>0,
			'sal_departmentid'=>$this->input->post('dddepartment'),
			'sal_role_id'=>$this->input->post('ddroles'),
			'sal_salsc_by'=>$this->input->post('txtsalby'),
			'sal_salaryscid'=>$this->input->post('ddsalary'),
			'sal_da'=>$this->input->post('txtda'),
			'sal_hra'=>$this->input->post('txthra'),
			'sal_ta'=>0,
			'sal_description'=>$this->input->post('txtscaledesc'),
			'sal_affid'=>1,
			'sal_status'=>$this->input->post('txtstatus'),
			'sal_date'=>$thius->curr
			);	
			$query ="CALL sp_insert_salary(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			echo 1;
		}
		public function checksal()
		{
			$id = $this->uri->segment(3);
			$session = 6;
			$this->db->where('m_salsc_salary_by',$id);
			$this->db->where('m_salsc_sessionid',$session);
			$datas=$this->db->get('m48_salary_scale');
			$json=json_encode($datas->result());
			echo $json;
			//$this->load->view('ACS/hid_get_salary',$data);
		}
		
		public function edit_salary()
		{
			$this->db->where('m_des_pat_id',-2);
			$data['department']=$this->db->get('m03_designation');
			$id=$this->uri->segment(3);
			$data['id']=$id;
			$clsdt= array(
			'proc'=>2,
			'sal_id'=>$id,
			'department_id'=>0,
			'role_id'=>0,
			'affid'=>1
			);
			$query ="CALL sp_view_salary(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['sal']=$this->db->query($query,$clsdt);
			$this->db->free_db_resource();
			$clsdt= array(
			'proc'=>1,
			'sal_id'=>0,
			'department_id'=>0,
			'role_id'=>0,
			'affid'=>1
			);
			$query ="CALL sp_view_salary(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['salary']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('ACS/view_edit_salary',$data);
			$this->load->view('footer_11');
		}
		
		public function update_salary()
		{
			$id=$this->input->post('txtid');
			$clsdt = array(
			'proc'=>2,
			'sal_id'=>$id,
			'sal_departmentid'=>$this->input->post('dddepartment'),
			'sal_role_id'=>$this->input->post('ddroles'),
			'sal_salsc_by'=>$this->input->post('txtsalby'),
			'sal_salaryscid'=>$this->input->post('ddsalary'),
			'sal_da'=>$this->input->post('txtda'),
			'sal_hra'=>$this->input->post('txthra'),
			'sal_ta'=>0,
			'sal_description'=>$this->input->post('txtscaledesc'),
			'sal_affid'=>1,
			'sal_status'=>$this->input->post('txtstatus'),
			'sal_date'=>$date->format('Y-m-d H:i:s')
			);	
			$query ="CALL sp_insert_salary(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			echo 1;
		}
		
		public function stch_salary()
		{
			$id=$this->uri->segment(3);
			$status=$this->uri->segment(4);
			$clsdt = array(
			'proc'=>3,
			'sal_id'=>$id,
			'sal_departmentid'=>0,
			'sal_role_id'=>0,
			'sal_salsc_by'=>0,
			'sal_salaryscid'=>0,
			'sal_da'=>0,
			'sal_hra'=>0,
			'sal_ta'=>0,
			'sal_description'=>0,
			'sal_affid'=>0,
			'sal_status'=>$status,
			'sal_date'=>0
			);	
			$query ="CALL sp_insert_salary(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->db->where('m_des_pat_id',0);
			$data['department']=$this->db->get('m03_designation');
			$clsdt= array(
			'proc'=>1,
			'sal_id'=>0,
			'department_id'=>0,
			'role_id'=>0,
			'affid'=>1
			);
			$query ="CALL sp_view_salary(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['salary']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('ACS/view_add_salary',$data);
			$this->load->view('footer_11');
		}
		
		//End Salary
		
		
		//Start Leave Master
		
		public function view_leave_master()
		{
			$clsdt= array(
			'proc'=>1,
			'leave_id'=>0,
			'affid'=>0
			);
			$query ="CALL sp_view_leave(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['leave']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('ACS/view_add_leaves',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		
		public function view_leave_master1()
		{
			
			$clsdt= array(
			'proc'=>1,
			'leave_id'=>0,
			'affid'=>0
			);
			$query ="CALL sp_view_leave(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['leave']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('ACS/view_add_leaves',$data);
			$this->load->view('footer_11');
		}
		
		public function add_leave()
		{
			$clsdt = array(
			'proc'=>1,
			'leave_id'=>0,
			'leave_name'=>$this->input->post('txtleavename'),
			'leave_type'=>$this->input->post('ddtype'),
			'leave_max'=>$this->input->post('txtmax'),
			'leave_status'=>$this->input->post('txtstatus'),
			'leave_affid'=>'1',
			'leave_date'=>$this->curr
			);	
			$query ="CALL sp_insert_leave(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			echo 1;
		}
		
		public function edit_leave()
		{
			$this->db->where('m_des_pat_id',0);
			$data['department']=$this->db->get('m03_designation');
			$id=$this->uri->segment(3);
			$data['id']=$id;
			$clsdt= array(
			'proc'=>2,
			'leave_id'=>$id,
			'affid'=>0
			);
			$query ="CALL sp_view_leave(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['leavess']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$clsdt= array(
			'proc'=>1,
			'leave_id'=>0,
			'affid'=>0
			);
			$query ="CALL sp_view_leave(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['leave']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('ACS/view_edit_leaves',$data);
			$this->load->view('footer_11');
		}
		
		public function update_leave()
		{
			$id=$this->input->post('txtid');
			$clsdt = array(
			'proc'=>2,
			'leave_id'=>$id,
			'leave_name'=>$this->input->post('txtleavename'),
			'leave_type'=>$this->input->post('ddtype'),
			'leave_max'=>$this->input->post('txtmax'),
			'leave_status'=>$this->input->post('txtstatus'),
			'leave_affid'=>'1',
			'leave_date'=>$this->curr
			);	
			$query ="CALL sp_insert_leave(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			echo 1;
		}
		
		public function stch_leave()
		{
			$id=$this->uri->segment(3);
			$status=$this->uri->segment(4);
			$clsdt = array(
			'proc'=>3,
			'leave_id'=>$id,
			'leave_name'=>'',
			'leave_type'=>'',
			'leave_max'=>'',
			'leave_status'=>$status,
			'leave_affid'=>'',
			'leave_date'=>$this->curr
			);	
			$query ="CALL sp_insert_leave(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$clsdt= array(
			'proc'=>1,
			'leave_id'=>0,
			'affid'=>0
			);
			$query ="CALL sp_view_leave(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['leave']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('ACS/view_add_leaves',$data);
			$this->load->view('footer_11');
		}
		
		//End Leave Master
		
		//Start Shift Master 	
		
		public function view_shift_master()
		{
			$clsdt= array(
			'proc'=>1,
			'shift_id'=>0,
			'affid'=>0
			);
			$query ="CALL sp_view_shift(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['shift']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('header');
			$this->load->view('menu1',$this->data1);
			$this->load->view('ACS/view_add_shift',$data);
			$this->load->view('sidebar');
			$this->load->view('footer');
		}
		
		public function view_shift_master1()
		{
			$clsdt= array(
			'proc'=>1,
			'shift_id'=>0,
			'affid'=>0
			);
			$query ="CALL sp_view_shift(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['shift']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$this->load->view('ACS/view_add_shift',$data);
			$this->load->view('footer_11');
		}
		
		public function add_shift()
		{
			$clsdt = array(
			'proc'=>1,
			'shift_id'=>0,
			'shift_name'=>$this->input->post('txtshiftname') ,
			'shift_starttime'=>$this->input->post('txtstarttime'),
			'shift_endtime'=>$this->input->post('txtendtime'),
			'shift_duration'=>$this->input->post('txtshift'),
			'shift_startdate'=>$this->input->post('txtstart'),
			'txtend'=>$this->input->post('txtend'),
			'shift_halfday'=>$this->input->post('txthalfday'),
			'shift_fullday'=>$this->input->post('txtfullday'),
			'shift_grace'=>$this->input->post('txtgrace'),
			'shift_affid'=>1,
			'shift_status'=>$this->input->post('txtstatus'),
			'shift_date'=>$this->curr
			);	
			$query ="CALL sp_insert_shift(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			echo 1;
		}
		
		
		public function edit_shift()
		{
			$this->db->where('m_des_pat_id',0);
			$data['department']=$this->db->get('m03_designation');
			$id=$this->uri->segment(3);
			$data['id']=$id;
			$clsdt= array(
			'proc'=>2,
			'shift_id'=>$id,
			'affid'=>0
			);
			$query ="CALL sp_view_shift(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['shiftss']=$this->db->query($query,$clsdt);
			$this->db->free_db_resource();
			$clsdt= array(
			'proc'=>1,
			'shift_id'=>0,
			'affid'=>0
			);
			$query ="CALL sp_view_shift(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['shift']=$this->db->query($query,$clsdt);
			$this->db->free_db_resource();
			$this->load->view('ACS/view_edit_shift',$data);
			$this->load->view('footer_11');	
		}
		
		public function update_shift()
		{
			$id=$this->input->post('id');
			$clsdt = array(
			'proc'=>2,
			'shift_id'=>$id,
			'shift_name'=>$this->input->post('txtshiftname') ,
			'shift_starttime'=>$this->input->post('txtstarttime'),
			'shift_endtime'=>$this->input->post('txtendtime'),
			'shift_duration'=>$this->input->post('txtshift'),
			'shift_startdate'=>$this->input->post('txtstart'),
			'txtend'=>$this->input->post('txtend'),
			'shift_halfday'=>$this->input->post('txthalfday'),
			'shift_fullday'=>$this->input->post('txtfullday'),
			'shift_grace'=>$this->input->post('txtgrace'),
			'shift_affid'=>1,
			'shift_status'=>$this->input->post('txtstatus'),
			'shift_date'=>$this->curr
			);	
			//var_dump($clsdt);
			$query ="CALL sp_insert_shift(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			echo 1;
			
		}
		
		public function stch_shift()
		{
			
			$id=$this->uri->segment(3);
			$status=$this->uri->segment(4);
			$clsdt = array(
			'proc'=>3,
			'shift_id'=>$id,
			'shift_name'=>0 ,
			'shift_starttime'=>0,
			'shift_endtime'=>0,
			'shift_duration'=>0,
			'shift_startdate'=>0,
			'txtend'=>0,
			'shift_halfday'=>0,
			'shift_fullday'=>0,
			'shift_grace'=>0,
			'shift_affid'=>1,
			'shift_status'=>$status,
			'shift_date'=>$this->curr
			);	
			$query ="CALL sp_insert_shift(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$clsdt= array(
			'proc'=>1,
			'shift_id'=>0,
			'affid'=>0
			);
			$query ="CALL sp_view_shift(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['shift']=$this->db->query($query,$clsdt);
			$this->db->free_db_resource();
			$this->load->view('ACS/view_add_shift',$data);
			$this->load->view('footer_11');
			
		}
	}
?>