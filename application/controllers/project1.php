<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Project extends CI_Controller 
	{
		
		public function index()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->load->view('CRM/header.php');
				$this->load->view('CRM/menu.php');
				$this->load->view('CRM/footer.php');
			}
			else
			{
				header("Location:".base_url()."index.php/crm/logout");
				
			}
		}
		
		
		
	    public function logout()
	    {
			$this->load->helper('url');
			$this->load->library('session');
			$this->session->unset_userdata('profile_id');
			$this->session->unset_userdata('user_id');
			$this->session->unset_userdata('e_email');
			$this->session->unset_userdata('name');
			$this->session->unset_userdata('designation');
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
			$data['info']=0;
			$this->load->view('Auth/header');
			$this->load->view('Auth/login',$data);
			$this->load->view('Auth/footer');
		}
		
		
		
		
		/*-----------------------------Create Project-----------------------------*/
		
		
		public function view_project()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$this->load->helper('form');
			if($this->session->userdata('user_id')!="")
			{
				$this->db->where('or_m_designation',2,3);
				$data['emp']=$this->db->get('m06_user_detail');
				$data['rec']=$this->db->get('m42_project');
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('project/view_project',$data);
				$this->load->view('sidebar');
			    $this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/project/logout");
			}
		}
		
		
		public function after_view_project()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->db->where('or_m_designation',2,3);
				$data['emp']=$this->db->get('m06_user_detail');
				$data['rec']=$this->db->get('m42_project');
				$this->load->view('project/view_project',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/project/logout");
			}
		}
		
		
		public function insert_project()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			if($this->session->userdata('user_id')!="")
			{
				$this->load->model('project_model');
				$this->project_model->insert_project();
				
				$p_name=$this->input->post('txtpname');
				$upload_config = array
				(
				'upload_path' => './project/',
				);
				
				$this->load->library('upload', $upload_config);
				if (!is_dir('project/'.$p_name))
				{
					mkdir('./project/'.$p_name, 0777, true);
				}
				
				
				
				echo 'true';
			}
			else
			{
				header("Location:".base_url()."index.php/project/logout");
			}
		}
		
		
		
		
		
		public function edit_project()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			if($this->session->userdata('user_id')!="")
			{
				$id=$this->uri->segment(3);
				$this->db->where('or_m_designation',2,3);
				$data['emp']=$this->db->get('m06_user_detail');
				$this->db->where('m_project_id',$this->uri->segment(3));
				$data['rec']=$this->db->get('m42_project');
				$data['participants']=$this->db->query("SELECT * FROM `tr22_project_participants` WHERE `parti_project_id`='$id' AND `parti_id`=(SELECT MIN(parti_id) FROM tr22_project_participants WHERE `parti_project_id`='$id')");
				//$this->load->view('project/header');
				//$this->load->view('project/menu');
				$this->load->view('project/view_edit_project',$data);
				//$this->load->view('project/footer');
			}
			else
			{
				header("Location:".base_url()."index.php/project/logout");
			}
		}
		
		public function update_project()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			if($this->session->userdata('user_id')!="")
			{
				$this->load->model('project_model');
				$this->project_model->update_project();
				echo "true";
			}
			else
			{
				header("Location:".base_url()."index.php/project/logout");
			}
		}
		
		public function project_status()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				
				$this->load->model('project_model');
				$this->project_model->project_status();
				
				
				$data['rec']=$this->db->get('m42_project');
				$this->load->view('Project/view_project',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/project/logout");
			}
		}
		
		
		
		
		/*-----------------Adding participants-------------------*/
		
		
		public function view_paricipants()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				
				$query = " CALL sp_sel_participants";			
				$data['rec']=$this->db->query($query);
				$this->load->view('header');
				$this->load->view('menu');
				$this->load->view('Project/view_all_participants',$data);
				$this->load->view('sidebar');
			    $this->load->view('footer');
			}
			else
			{
				header("Location:".base_url()."index.php/project/logout");
			}
		}
		
		
		public function after_view_project_participants()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$query = " CALL sp_sel_participants";			
				$data['rec']=$this->db->query($query);
				$this->load->view('Project/view_all_participants',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/project/logout");
			}
		}
		
		
		
		public function view_project_participants()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$data['project']=$this->db->get('m42_project');
				$this->db->where('or_m_designation',2,3);
				$data['emp']=$this->db->get('m06_user_detail');
				$this->load->view('Project/view_add_participants',$data);
				
			}
			else
			{
				header("Location:".base_url()."index.php/project/logout");
			}
		}
		
		
		
		
		public function add_participants()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			if($this->session->userdata('user_id')!="")
			{
				
				$this->load->model('project_model');
				$this->project_model->participants_add();
				
				$proj_id=$this->input->post('txtprojectid');
				$parti_id=$this->input->post('txtparticipant');
				$data['fol']=$this->db->query("SELECT m_project_name FROM m42_project WHERE m_project_id='$proj_id'");
				$fol="";
				foreach($data['fol']->result() as $row)
				{
					$fol=$row->m_project_name;
					break;
				}
				
				
				$upload_config = array
				(
				'upload_path' => './project/'.$fol.'/'
				);
				
				$this->load->library('upload', $upload_config);
				if (!is_dir('project/'.$fol.'/'.$parti_id))
				{
					mkdir('./project/'.$fol.'/'.$parti_id, 0777, true);
				}
				
				echo "true";
			}
			else
			{
				header("Location:".base_url()."index.php/project/logout");
			}
		}
		
		
		
		public function edit_add_participants()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$data['project']=$this->db->get('m42_project');
				$this->db->where('or_m_designation',2,3);
				$data['emp']=$this->db->get('m06_user_detail');
				$this->db->where('parti_id',$this->uri->segment(3));
				$data['rec']=$this->db->get('tr22_project_participants');
				$this->load->view('Project/view_edit_participants',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/project/logout");
			}
		}
		
		
		public function update_add_participants()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				$this->load->model('project_model');
				$this->project_model->participants_update();
				echo 'true';
			}
			else
			{
				header("Location:".base_url()."index.php/project/logout");
			}
		}
		
		
		public function participants_status()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			if($this->session->userdata('user_id')!="")
			{
				
				$this->load->model('project_model');
				$this->project_model->participants_status();
				
				
				
				$query = "CALL sp_sel_participants";			
				$data['rec']=$this->db->query($query);
				$this->load->view('Project/view_all_participants',$data);
			}
			else
			{
				header("Location:".base_url()."index.php/project/logout");
			}
		}
		
		
		
		public function mail()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			
			$this->load->model('send_mail');
			$this->send_mail->send_mail();
			$this->load->view('project/header');
			$this->load->view('project/menu');
			$this->load->view('project/view_project_task');
			$this->load->view('project/footer');
			
		}
		
		
	}
	
?>																						