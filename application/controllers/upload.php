<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Upload extends CI_Controller
	{
		public function __construct() 
		{
			parent::__construct();
			$this->load->model('clientmodel');
		    $timezone = new DateTimeZone("Asia/Kolkata" );
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
			//$this->_is_logged_in();
		}
		
		/*Custom Function here*/
		
		/*Session Check Function */ 
		public function _is_logged_in() 
		{
		    $is_logged_in = $this->session->userdata('profile_id');
			if ($is_logged_in=="" || $is_logged_in!= 0) 
			{
				redirect('auth/index/1');
				die();
			}
		} 
		
		//Upload Image function
		
		public function ticket_submit()
		{
			$config['upload_path']   =   "application/uploadimage/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "1907";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$fileupload=($finfo['raw_name'].$finfo['file_ext']);
			$txtemail=$this->input->post('txtemail');
			$txtname=$this->input->post('txtname');
			$txtsubject=$this->input->post('txtsubject');
			$txtcomment=$this->input->post('txtdiscription');
			$ticket_no=$this->clientmodel->add_new_ticket($fileupload);
			$status="";
			if($this->input->post('txturgency')==1) 
			{$status="High";}
			if($this->input->post('txturgency')==2)
			{$status="Medium";}
			if($this->input->post('txturgency')==3)
			{$status="Low";}
			
			$ip=$_SERVER['REMOTE_ADDR'];
			$message="<p>Dear: ".$txtname.",<br><br>
			Thank you for contacting our support team. A support ticket has now been opened for your request. You will be notified when a response is made by email.
			The details of your ticket are shown below.<br><br>
			Subject: ". $txtsubject."<br><br>
			Priority: ". $status ."<br><br>
			Status: Open<br><br>
			Your IP: ". $ip."<br><br>
			For checking your Ticket- ".base_url()."direct_ticket/view_ticket/".$ticket_no."</p>";
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL,'Support Techaarjavam');			 // change it to yours
			$this->email->to($txtemail);												 // change it to yours
            $this->email->bcc('techaarjavam@gmail.com');
			$this->email->subject('Techaarjavam Support Ticket[ID:'.$ticket_no.']');
			$this->email->message($message);
			$this->email->send();
			header("Location:".base_url()."client/ticket");
		}
		public function ticket_submit2()
		{
			$config['upload_path']   =   "application/uploadimage/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "1907";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$fileupload=($finfo['raw_name'].$finfo['file_ext']);
			//$this->clientmodel->add_new_client_ticket($fileupload);
			$txtemail=$this->input->post('txtemail');
			$txtname=$this->input->post('txtname');
			$txtsubject=$this->input->post('txtsubject');
			$txtcomment=$this->input->post('txtdiscription');
			$ticket_no=$this->clientmodel->add_new_client_ticket($fileupload);
			$status="";
			if($this->input->post('txturgency')==1) 
			{$status="High";}
			if($this->input->post('txturgency')==2)
			{$status="Medium";}
			if($this->input->post('txturgency')==3)
			{$status="Low";}
			
			$ip=$_SERVER['REMOTE_ADDR'];
			$message="<p>Dear: ".$txtname.",<br><br>
			Thank you for contacting our support team. A support ticket has now been opened for your request. You will be notified when a response is made by email.
			The details of your ticket are shown below.<br><br>
			Subject: ". $txtsubject."<br><br>
			Priority: ". $status ."<br><br>
			Status: Open<br><br>
			Your IP: ". $ip."<br><br>
			For checking your Ticket- ".base_url()."direct_ticket/view_ticket/".$ticket_no."</p>";
			
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL,'Support Techaarjavam');			 // change it to yours
			$this->email->to($txtemail);												 // change it to yours
            $this->email->bcc('techaarjavam@gmail.com');
			$this->email->subject('Techaarjavam Support Ticket[ID:'.$ticket_no.']');
			$this->email->message($message);
			$this->email->send();
			
			header("Location:".base_url()."client/ticket");
			
		}
		public function admin_ticket_submit()
		{
			$config['upload_path']   =   "application/uploadimage/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "1907";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$fileupload=($finfo['raw_name'].$finfo['file_ext']);
			//$this->clientmodel->add_new_ticket($fileupload);
			$txtemail=$this->input->post('txtemail');
			$txtname=$this->input->post('txtname');
			$txtsubject=$this->input->post('txtsubject');
			$txtcomment=$this->input->post('txtdiscription');
			$ticket_no=$this->clientmodel->add_new_ticket($fileupload);
			$status="";
			if($this->input->post('txturgency')==1) 
			{$status="High";}
			if($this->input->post('txturgency')==2)
			{$status="Medium";}
			if($this->input->post('txturgency')==3)
			{$status="Low";}
			
			$ip=$_SERVER['REMOTE_ADDR'];
			$message="<p>Dear: ".$txtname.",<br><br>
			Thank you for contacting our support team. A support ticket has now been opened for your request. You will be notified when a response is made by email.
			The details of your ticket are shown below.<br><br>
			Subject: ". $txtsubject."<br><br>
			Priority: ". $status ."<br><br>
			Status: Open<br><br>
			Your IP: ". $ip."<br><br>
			For checking your Ticket- ".base_url()."direct_ticket/view_ticket/".$ticket_no."</p>";
			
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL,'Support Techaarjavam');			 // change it to yours
			$this->email->to($txtemail);												 // change it to yours
            $this->email->bcc('techaarjavam@gmail.com');
			$this->email->subject('Techaarjavam Support Ticket[ID:'.$ticket_no.']');
			$this->email->message($message);
			$this->email->send();
			
			header("Location:".base_url()."crm/view_account");
		}
		
		/*Outer Ticket Submit Here*/
		public function outer_ticket()
		{
			$config['upload_path']   =   "application/uploadimage/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "1907";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$fileupload=($finfo['raw_name'].$finfo['file_ext']);
			$ticket_no=$this->clientmodel->add_new_outer_ticket($fileupload);
			$txtemail=$this->input->post('txtemail');
			$txtname=$this->input->post('txtname');
			$txtsubject=$this->input->post('txtsubject');
			$txtcomment=$this->input->post('txtdiscription');
			//$ticket_no=$this->input->post('txtticket');
            $department=$this->input->post('txtdepartment1');
			$status="";
			if($this->input->post('txturgency')==1) 
			{$status="High";}
			if($this->input->post('txturgency')==2)
			{$status="Medium";}
			if($this->input->post('txturgency')==3)
			{$status="Low";}
			
			$ip=$_SERVER['REMOTE_ADDR'];
			$message="<p>Dear: ".$txtname.",<br><br>
			Thank you for contacting our support team. A support ticket has now been opened for your request. You will be notified when a response is made by email.
			The details of your ticket are shown below.<br><br>
			Subject: ". $txtsubject."<br><br>
			Priority: ". $status ."<br><br>
			Status: Open<br><br>
			Your IP: ". $ip."<br><br>
			For checking your Ticket- ".base_url()."direct_ticket/view_ticket/".$ticket_no."</p>";
			
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL,'Support Techaarjavam');			 // change it to yours
			$this->email->to($txtemail);												 // change it to yours
            $this->email->bcc('techaarjavam@gmail.com');
			$this->email->subject('Techaarjavam Support Ticket[ID:'.$ticket_no.']');
			$this->email->message($message);
			$this->email->send();
			header("Location:".base_url()."direct_ticket/index/1/".$txtname."/".$ticket_no."/".$department."/");
			//header("Location:".base_url()."direct_ticket/index");
		}
		
		
		function client_response()
		{
			$config['upload_path']   =   "application/uploadimage/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "1907";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$tic=$this->input->post('txtticket');
			$fileupload=$finfo['raw_name'].$finfo['file_ext'];
			if($fileupload=="")
			{
				$fi="";
			}
			else
			{
				$fi=$fileupload;
			}
			$data=array(
			'txtemail'=>$this->input->post('txtemail'),
			'txtdepartment'=>$this->input->post('txtdepartment'),
			'assign_emp_id'=>$this->input->post('txtassign_emp_id'),
			'txtsubject'=>$this->input->post('txtsubject'),
			'txturgency'=>$this->input->post('txturgency'),
			'txtdiscription'=>$this->input->post('txtdiscription'),
			'userfile'=>$fi,
			'response_type'=>3,
			'response_by'=>$this->session->userdata('e_email'),
			'ticket_no'=>$this->input->post('txtticket'),
			'txtstatus'=>1,
			'adddate'=>$this->curr
			);
			$this->db->insert('tr20_submit_ticket',$data);
			$des=$this->input->post('txtdiscription');
			
			$ip=$_SERVER['REMOTE_ADDR'];
			$message="<p>This is a notification to let you know that we have responded back to your query with reference to the ticket no #".$this->input->post('txtticket').". 
			Please re-open the ticket and respond accordingly.<br><br>
			Message: ". $description."<br><br>
			Priority: High<br><br>
			Status: Open<br><br>
			Your IP: ". $ip."<br><br>
			For checking your Ticket- ".base_url()."direct_ticket/view_ticket/".$tic."</p>";
			
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL,'Support Techaarjavam');			 // change it to yours
			$this->email->to($this->input->post('txtemail'));												 // change it to yours
			$this->email->bcc('techaarjavam@gmail.com');
            $this->email->subject('Techaarjavam Support Ticket[ID:'.$tic.']');
			$this->email->message($message);
			$this->email->send();
			header("Location:".base_url()."client/view_ticket/$tic");  
		}
		
		
		function outer_response()
		{
			$tic_no=$this->input->post('txtticket');
			//$txtsubject=$this->input->post('txtsubject');
			$description=$this->input->post('txtdiscription');
			$res=$this->input->post('txtname');
			$tic_owner=$this->input->post('txtemail1');
			$config['upload_path']   =   "application/uploadimage/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "1907";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
		    $tic=$this->input->post('txtticket');
            $ip=$_SERVER['REMOTE_ADDR'];
		    $fileupload=$finfo['raw_name'].$finfo['file_ext'];
			if($fileupload=="")
			{
				$fi="";
			}
			else
			{
				$fi=$fileupload;
			}
			$data=array(
			'proc'=>2,'tkt_ref_no'=>$this->input->post('txtticket'),'tkt_person_name'=>'','tkt_email'=>$tic_owner,'tkt_department'=>'','emp_id'=>'','tkt_subject'=>'','tkt_urgency'=>'','tkt_discription'=>'','tkt_response_type'=>4,'response_by'=>$res,'tkt_userfile'=>$fi,'tkt_status'=>1,'account_id'=>'','affiliate_id'=>'','tkt_sub_date'=>$this->curr,'trans_description'=>$this->input->post('txtdiscription'),'trans_response_date'=>$this->curr,'trans_status'=>1
			);
			$query = "CALL sp_ticket_submission(?" . str_repeat(",?", count($data)-1) .",@ticketno)";
			$data['rec']=$this->db->query($query,$data);
			$this->db->free_db_resource();
			
			$message = '<p>Dear: '.$res.',<br><br>
		  	This is a notification to let you know that we have responded back to your query with reference to the ticket no #'.$this->input->post('txtticket').'. 
		    Please re-open the ticket and respond accordingly.<br><br>
			Message: '. $this->input->post('txtdiscription').'<br><br>
			Priority: High<br><br>
			Status: Open<br><br>
			Your IP: '. $ip.'<br><br>
            For checking your Ticket- '.base_url().'direct_ticket/view_ticket/'.$tic_no.'</p>';
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL,'Support Techaarjavam'); // change it to yours
			$this->email->to($tic_owner);// change it to yours
			$this->email->bcc('techaarjavam@gmail.com');
			$this->email->subject('Techaarjavam Support Ticket[ID:'.$tic_no.']');
			$this->email->message($message);
			if($this->email->send())
			{
				header("Location:".base_url()."direct_ticket/view_ticket/".$tic_no);
			}
			else
			{
				show_error($this->email->print_debugger());
			}
		}
		
		
		
		function admin_response()
		{
			$config['upload_path']   =   "application/uploadimage/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "1907";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
	  		$this->upload->do_upload();
			$finfo=$this->upload->data();
			$tic=$this->input->post('txtticket');
			$fileupload=$finfo['raw_name'].$finfo['file_ext'];
			if($fileupload=="")
			{
				$fi="";
			}
			else
			{
				$fi=$fileupload;
			}
			$id=$this->uri->segment(3);
			$data=array(
			'proc'=>2,
			'ticket_no'=>$this->input->post('txtticket'),
			'tkt_person_name'=>'',
			'tkt_email'=>'',
			'tkt_department'=>'',
			'emp_id'=>'',
			'tkt_subject'=>'',
			'tkt_urgency'=>'',
			'tkt_discription'=>'',
			'tkt_response_type'=>1,
			'response_by'=>$this->session->userdata('name'),
			'tkt_userfile'=>$fi,
			'tkt_status'=>1,
			'account_id'=>'',
			'affiliate_id'=>'',
			'tkt_sub_date'=>$this->curr,
			'trans_description'=>$this->input->post('txtdiscription'),
			'trans_response_date'=>$this->curr,
			'trans_status'=>1		  
			);
			$query = "CALL sp_ticket_submission(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	        $data['rec']=$this->db->query($query,$data);
			$this->db->free_db_resource();
			$tic_no=$this->input->post('txtticket');
			$txtsubject=$this->input->post('txtsubject');
			$description=$this->input->post('txtdiscription');
			$res=$this->input->post('txtemail');
			$data['rec']=$this->db->query("SELECT txtemail FROM tr20_submit_ticket WHERE ticket_no='$tic_no' GROUP BY ticket_no");
			foreach($data['rec']->result() as $rec)
			{
				$tic_owner=$rec->txtemail;
				break;
			}
			$ip=$_SERVER['REMOTE_ADDR'];
			$message="<p>Dear: ".$txtname.",<br><br>
			This is a notification to let you know that we have responded back to your query with reference to the ticket no #".$this->input->post('txtticket').". 
			Please re-open the ticket and respond accordingly.<br><br>
			Message: ". $description."<br><br>
			Priority: High<br><br>
			Status: Open<br><br>
			Your IP: ". $ip."<br><br>
			For checking your Ticket- ".base_url()."direct_ticket/view_ticket/".$tic_no."</p>";
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL,'Support Techaarjavam');			 // change it to yours
			$this->email->to($tic_owner);											 // change it to yours
			$this->email->cc($res);													 // change it to yours
            $this->email->bcc('techaarjavam@gmail.com');
			$this->email->subject('Techaarjavam Support Ticket[ID:'.$tic_no.']');
			$this->email->message($message);
			$this->email->send();
			header("Location:".base_url()."admin/view_ticket/$tic");
			
		}
		
		
		function support_reply()
		{
			$config['upload_path']   =   "application/uploadimage/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "1907";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$tic=$this->input->post('txtticket');
			$fileupload=$finfo['raw_name'].$finfo['file_ext'];
			if($fileupload=="")
			{
				$fi="";
			}
			else
			{
				$fi=$fileupload;
			}
			$data=array(
			'txtemail'=>$this->input->post('txtemail'),
			'txtdepartment'=>$this->input->post('txtdepartment'),
			'assign_emp_id'=>$this->input->post('txtassign_emp_id'),
			'txtsubject'=>$this->input->post('txtsubject'),
			'txturgency'=>$this->input->post('txturgency'),
			'txtdiscription'=>$this->input->post('txtdiscription'),
			'userfile'=>$fi,
			'response_type'=>2,
			'response_by'=>$this->session->userdata('name'),
			'ticket_no'=>$this->input->post('txtticket'),
			'txtstatus'=>1,
			'adddate'=>$this->curr
			);
			$this->db->insert('tr20_submit_ticket',$data);
			$tic_no=$this->input->post('txtticket');
			$txtsubject=$this->input->post('txtsubject');
			$description=$this->input->post('txtdiscription');
			$res=$this->input->post('txtemail');
			$data['rec']=$this->db->query("SELECT txtemail FROM tr20_submit_ticket WHERE ticket_no='$tic_no' GROUP BY ticket_no");
			foreach($data['rec']->result() as $rec)
			{
				$tic_owner=$rec->txtemail;
				break;
			}
			$ip=$_SERVER['REMOTE_ADDR'];
			$message="<p>Dear: ".$txtname.",<br><br>
			This is a notification to let you know that we have responded back to your query with reference to the ticket no #".$this->input->post('txtticket').". 
			Please re-open the ticket and respond accordingly.<br><br>
			Reply: ". $description."<br><br>
			Priority: High<br><br>
			Status: Open<br><br>
			Your IP: ". $ip."<br><br>
			For checking your Ticket- ".base_url()."direct_ticket/view_ticket/".$tic_no."</p>";
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL,'Support Techaarjavam');			 // change it to yours
			$this->email->to($tic_owner);												 // change it to yours
            $this->email->bcc('techaarjavam@gmail.com');
			$this->email->subject('Techaarjavam Support Ticket[ID:'.$tic_no.']');
			$this->email->message($message);
			$this->email->send();
			
			header("Location:".base_url()."support/view_ticket/$tic_no");
			
		}
		
		
		
		
		function anouncement()
		{
			$config['upload_path']   =   "application/uploadimage/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "1907";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			$tic=$this->input->post('txtticket');
			$fileupload=$finfo['raw_name'].$finfo['file_ext'];
			if($fileupload=="")
			{
				$fi="";
			}
			else
			{
				$fi=$fileupload;
			}
			$data=array(
			'txttitle'=>$this->input->post('txttitle'),
			'for'=>$this->input->post('ddannfor'),
			'txtdescription'=>$this->input->post('txtdiscription'),
			'userfile'=>$fi,
			'txtstatus'=>1,
			'adddate'=>$this->curr
			);
			$this->db->insert('tr19_anoncement',$data);
			header("Location:".base_url()."admin/announcements/");
			
		}
		
		
		function anouncement_edit()
		{
			$config['upload_path']   =   "application/uploadimage/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "1907";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$id=$this->uri->segment(3);
			$fileupload=$finfo['raw_name'].$finfo['file_ext'];
			if($fileupload=="")
			{
				$fi="";
			}
			else
			{
				$fi=$fileupload;
			}
			$data=array(
			'txttitle'=>$this->input->post('txttitle'),
			'txtdescription'=>$this->input->post('txtdescription'),
			'userfile'=>$fi,
			'adddate'=>$this->curr
			);
			$this->db->where('id',$id);
			$this->db->update('tr19_anoncement',$data);
			header("Location:".base_url()."admin/announcements/");
			
		}
		
		
		
		/*-------------------------------CMS FILE UPLOAD CONTROLER----------------------------------------------------*/
		
		function index()
		{    
			$this->load->view('CMS/image_upload', array('error' => ' ' ));
		}
		
		function do_upload()
		{
			$this->load->library('upload');
			$files = $_FILES;
			$cpt = count($_FILES['userfile']['name']);
			for($i=0; $i<$cpt; $i++)
			{
				$_FILES['userfile']['name']= $files['userfile']['name'][$i];
				$_FILES['userfile']['type']= $files['userfile']['type'][$i];
				$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
				$_FILES['userfile']['error']= $files['userfile']['error'][$i];
				$_FILES['userfile']['size']= $files['userfile']['size'][$i];    
				$data=array(
				'image_name'=>$_FILES['userfile']['name'],
				'image_type'=>$_FILES['userfile']['type'],
				'image_size'=> $_FILES['userfile']['size'],
				'status'=>'1'
				);
				$this->db->insert('m34_cms_manage_image',$data);
				$this->upload->initialize($this->set_upload_options());
				$this->upload->do_upload();
				header("location:".base_url()."cms/image_upload/");
			}
		}
		private function set_upload_options()
		{   
			//  upload an image options
			$config = array();
			$config['upload_path'] = 'application/libraries/banner_Images/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']      = '0';
			$config['overwrite']     = FALSE;
			return $config;
		}
		
		/*-------------------------------END CMS FILE UPLOAD CONTROLER----------------------------------------------------*/
		
	}
	
?>