DELIMITER $$

USE `admin_metro_new`$$

DROP PROCEDURE IF EXISTS `sp_account`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_account`(
    IN  proc INT(11),
    IN  account_owner INT(11),
    IN  account_owned_by INT(11),
    IN  account_name VARCHAR(200),
    IN  account_industry INT(11),
    IN  account_emp INT(11),
    IN  account_website VARCHAR(200),
    IN  account_phone  VARCHAR(200),
    IN  account_email VARCHAR(200),
    IN  account_image VARCHAR(200),
    IN  account_fax VARCHAR(200),
    IN  account_type INT(11),
    IN  account_ownership INT(11),
    IN  account_city INT(11),
    IN  account_state INT(11),
    IN  account_zipcode VARCHAR(20),
    IN  account_country INT(11),
    IN  account_address VARCHAR(2000),
    IN  account_desc VARCHAR(2000),
    IN  account_status INT(2),
    IN  logintype INT(2),
    IN  account_regdate DATETIME,
    IN  account_pwd VARCHAR(1000),
    IN  account_pinpwd VARCHAR(1000),
    OUT msg VARCHAR(1000)
)
BEGIN
  DECLARE user_id,rc VARCHAR(20);
  DECLARE acc_id INT(11);
  DECLARE con_id INT(11) ;
  DECLARE login_type INT(11);
DECLARE EXIT HANDLER FOR SQLEXCEPTION
BEGIN
SET msg="No data was affected due to SQLEXCEPTION";
ROLLBACK;
END;
DECLARE EXIT HANDLER FOR SQLWARNING
BEGIN
GET DIAGNOSTICS CONDITION 1
    @p2 = MESSAGE_TEXT;
SET msg=CONCAT("No data was affected due to SQLWARNING ",@p2);
/*SET msg="No data was affected due to SQLWARNING";*/
ROLLBACK;
END;
DECLARE EXIT HANDLER FOR 1318 
 BEGIN
 SET msg="Some Parameter missing";
 END;
 DECLARE EXIT HANDLER FOR 1062 
BEGIN
SET msg=CONCAT("Duplicate entry %s for key %d");
END;
DECLARE EXIT HANDLER FOR 1065 
BEGIN
SET msg="Some Error on Syntax";
END;
DECLARE EXIT HANDLER FOR SQLSTATE '23000' 
BEGIN
SET msg='SQLSTATE 23000';
END;
DECLARE EXIT HANDLER FOR 1048 
BEGIN
SET msg='SQLSTATE 1048';
END;
IF(proc=1)
THEN
START TRANSACTION;
SET user_id=(SELECT GetAutoId(2) AS Account_id);
INSERT INTO `admin_metro_new`.`m33_account` 
(
  `account_user_id`,
  `account_owner`,
  `account_owned_by`,
  `account_name`,
  `account_industry`,
  `account_emp`,
  `account_website`,
  `account_phone`,
  `account_email`,
  `account_image`,
  `account_fax`,
  `account_type`,
  `account_ownership`,
  `account_city`,
  `account_state`,
  `account_zipcode`,
  `account_country`,
  `account_address`,
  `account_desc`,
  `account_status`,
  `logintype`,
  `account_regdate`
) 
VALUES
  (
    user_id,
    account_owner,
    account_owned_by,
    account_name,
    account_industry,
    account_emp,
    account_website,
    account_phone,
    account_email,
    account_image,
    account_fax,
    account_type,
    account_ownership,
    account_city,
    account_state,
    account_zipcode,
    account_country,
    account_address,
    account_desc,
    account_status,
    logintype,
    account_regdate
  ) ;
 SET acc_id=(SELECT `m33_account`.`account_id` FROM m33_account WHERE `m33_account`.`account_user_id`=user_id);
 SET login_type=(SELECT `m33_account`.`logintype` FROM m33_account WHERE `m33_account`.`account_user_id`=user_id);
 INSERT INTO `admin_metro_new`.`m34_contact` 
 (
  `account_id`,
  `contact_title`,
  `contact_name`,
  `contact_designation`,
  `contact_mobile`,
  `contact_email`,
  `contact_address`,
  `contact_city`,
  `contact_state`,
  `contact_country`,
  `contact_zipcode`,
  `contact_status`,
  `contact_reg_date`,
  `is_account`
 ) 
VALUES
  (
   acc_id,
   '',
   '',
   '',
   account_phone,
   account_email,
   account_address,
   account_city,
   account_state,
   account_country,
   account_zipcode,
   account_status,
   account_regdate,
   1
  ) ;
  SET con_id=(SELECT `m34_contact`.`contact_id` FROM m34_contact WHERE `m34_contact`.`account_id`=acc_id);
  INSERT INTO `admin_metro_new`.`tr04_login`
  (
  `or_user_id`,
  `or_login_id`,
  `or_login_pwd`,
  `or_pin_pwd`,
  `or_account_id`,
  `or_login_type`
  ) 
  VALUES
  (
    con_id,
    account_email,
    account_pwd,
    account_pinpwd,
    acc_id,
    login_type 
  );
  
COMMIT;
 SET rc=(SELECT ROW_COUNT() AS c);
 IF(rc=0) THEN
 SET msg="No data was affected.";
 ELSE
 SET msg="You are Successfully Inserted";
 END IF;
END IF;  
  
IF(proc=2)
THEN
SELECT 
       `m33_account`.`account_id` AS 'ACC_ID',
       `m33_account`.`account_owner` AS 'ACC_OWNER',
       (CASE WHEN(`m33_account`.`account_owner`=0) THEN 'Super Admin' WHEN (`m33_account`.`account_owner`!=0 AND `m33_account`.`account_owned_by`=1) THEN `m06_user_detail`.`or_m_name` WHEN (`m33_account`.`account_owner`!=0 AND `m33_account`.`account_owned_by`=2) THEN m34_contact.contact_name  ELSE '' END) AS 'Owner',
       `m33_account`.`account_regdate` AS 'REG_DATE',
       `m33_account`.`account_user_id` AS 'USER_ID',
       `m33_account`.`account_name` AS 'ACC_NAME',
       `m33_account`.`account_phone` AS 'ACC_PHONE',
       `m33_account`.`account_email` AS 'ACC_EMAIL',
       `m33_account`.`account_address` AS 'ACC_ADDRESS',
       `m33_account`.`account_website` AS 'ACC_WEBSITE',
       `m03_designation`.`m_des_name` AS 'ACC_TYPE',
       (CASE WHEN(`m33_account`.`account_status`=0) THEN 'DEACTIVE' WHEN(`m33_account`.`account_status`=1) THEN 'ACTIVE' ELSE '' END) AS 'ACC_STATUS',
       `m05_location`.`m_loc_name` AS 'ACC_STATE', 
       `m05_location_1`.`m_loc_name` AS 'ACC_CITY' 
FROM
    `admin_metro_new`.`m33_account`
    LEFT JOIN `admin_metro_new`.`m06_user_detail` 
        ON (`m33_account`.`account_owner` = `m06_user_detail`.`or_m_reg_id`)
        LEFT JOIN `admin_metro_new`.`m34_contact` 
        ON (`m33_account`.`account_owner` = `m34_contact`.`contact_id`)
         LEFT JOIN `admin_metro_new`.`m03_designation` 
        ON (`m33_account`.`account_type` = `m03_designation`.`m_des_id`)
    LEFT JOIN `admin_metro_new`.`m05_location` 
        ON (`m33_account`.`account_state` = `m05_location`.`m_loc_id`)
    LEFT JOIN `admin_metro_new`.`m05_location` AS `m05_location_1`
        ON (`m33_account`.`account_city` = `m05_location_1`.`m_loc_id`)
         WHERE `m33_account`.`account_type`=11 AND `m33_account`.`account_owner`=account_owner AND `m33_account`.`account_owned_by`=account_owned_by;
 SET rc=(SELECT FOUND_ROWS() AS c);
 IF(rc=0 || rc=-1) THEN
 SET msg=rc;
 ELSE
 SET msg=rc;
 END IF;
END IF;
IF(proc=3)
THEN
START TRANSACTION;
UPDATE 
  `admin_metro_new`.`m33_account` 
SET
  `account_name` = account_name,
  `account_industry` = account_industry,
  `account_emp` = account_emp,
  `account_website` =account_website,
  `account_phone` = account_phone,
  `account_email` = account_email,
  `account_image` = account_image,
  `account_fax` = account_fax,
  `account_city` = account_city,
  `account_state` = account_state,
  `account_zipcode` = account_zipcode,
  `account_country` = account_country,
  `account_address` = account_address,
  `account_desc` = account_desc
WHERE `account_id` = account_owner ;
SET rc=(SELECT ROW_COUNT() AS c);
IF(rc=0) THEN
SET msg="No data was affected.";
ELSE
SET msg="You are Successfully Update";
END IF;
COMMIT;
END IF;
    END$$

DELIMITER ;