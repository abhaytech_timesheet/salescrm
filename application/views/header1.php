<!DOCTYPE html>
<html lang="en" class="no-js">
	<!-- BEGIN HEAD -->
	<head>
		
		<title>demo</title>
		
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
		<meta content="" name="description"/>
		<meta content="" name="author"/>
		
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
		<?php
			if($this->router->fetch_class()!='direct_ticket')
			{
			?>
			<?php
				if($this->router->fetch_method()=='dashboard')
				{
				?>
				<!-- BEGIN PAGE LEVEL PLUGINS -->
				<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
				<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
				<!-- END PAGE LEVEL PLUGINS -->
				<?php
				}
				if($this->router->fetch_method()=='view_opportunity')
				{
				?>
				<link href="<?php echo base_url() ?>application/libraries/assets/pages/css/pricing.min.css" rel="stylesheet" type="text/css" />
				<?php
				}
				if($this->router->fetch_method()=='view_send_email_template' || $this->router->fetch_method()=='get_template')
				{
				?>
				<link href="<?php echo base_url() ?>application/libraries/assets/apps/css/inbox.min.css" rel="stylesheet" type="text/css" />
				<?php
				}
			?>
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
			<!-- END PAGE LEVEL PLUGINS -->
			<?php
			}
		?>
		<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
		
		<!-- BEGIN THEME GLOBAL STYLES -->
		<link href="<?php echo base_url() ?>application/libraries/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
		<link href="<?php echo base_url() ?>application/libraries/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
		<!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url() ?>application/libraries/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url() ?>application/libraries/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
	</head>
<!-- END HEAD -->