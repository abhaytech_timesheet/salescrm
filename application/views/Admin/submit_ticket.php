<?php  					
	foreach($rec->result() as $row)
	{	
		$name=$row->account_name;
		$email=$row->account_email;
	}
	if($dep=='1')
	{
		$department='Support';
	}
	if($dep=='2')
	{
		$department='Sales';
	}
	if($dep=='3')
	{
		$department='Development';
	}
	if($dep=='4')
	{
		$department='Complaints';
	}									
?>
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Admin Ticket Panel for Account</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Account</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Submit Ticket</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Submit Ticket</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/upload/admin_ticket_submit" onsubmit="return check(conwv('myform'))" method="post" id="myform">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Name</label>
										<div class="col-md-9">
											<input type="text" name="txtname" id="txtname" value="<?php echo $name ?>" readonly="readonly" class="form-control input-sm" />
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Email Address</label>
										<div class="col-md-9">
											<input type="text" name="txtemail" id="txtemail" value="<?php echo $email ?>" readonly="readonly" class="form-control input-sm" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Department</label>
										<div class="col-md-9">
                                            <input type="text" name="txtdepartment" id="txtdepartment" value="<?php echo $department ?>" readonly="readonly" class="form-control input-sm" />
											<input type="hidden" name="txtdepartmentid" id="txtdepartmentid" value="<?php echo $dep;?>" />
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Subject
	                                        <span class="required">*</span>
										</label>
										<div class="col-md-9">
                                            <input type="text" name="txtsubject" id="txtsubject" class="form-control input-sm alpha_numeric" />
                                            <span id="divtxtsubject" style="color:red"></span>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Urgency</label>
										<div class="col-md-9">
                                            <select name="txturgency" id="txturgency" class="form-control input-sm">
                                                <option value="1">High</option>
                                                <option value="2" selected="selected">Medium</option>
                                                <option value="3">Low</option>
											</select>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Comment
                                        	<span class="required">
                                                *
											</span>
										</label>
										<div class="col-md-9">
                                            <textarea rows="7" name="txtdiscription" id="txtdiscription" cols="50" class="form-control input-sm alpha_numeric"></textarea>
                                            <span id="divtxtdiscription" style="color:red"></span>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Attachments</label>
										<div class="col-md-9">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
												<div class="input-group input-large">
													<div class="form-control uneditable-input span3" data-trigger="fileinput">
														<i class="fa fa-file fileinput-exists"></i>&nbsp;
														<span class="fileinput-filename">
														</span>
													</div>
													<span class="input-group-addon btn default btn-file">
														<span class="fileinput-new">
															Select file
														</span>
														<span class="fileinput-exists">
															Change
														</span>
														<input type="file"  name="userfile" id="userfile">
													</span>
													<a href="#" class="input-group-addon btn default fileinput-exists" data-dismiss="fileinput">
														Remove
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<?php
                                            $ticket_no=rand(100000, 999999);
										?>
										<input type="hidden" value="<?php echo $ticket_no ?>" name="txtticket" id="txtticket"/>
										<button type="submit" class="btn blue">Submit</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													
