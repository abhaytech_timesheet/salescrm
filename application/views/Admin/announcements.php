<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/index/">
							Help Desk
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/announcements/">
							Announcements
						</a>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Add Announcements</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-black"></i>
								<span class="caption-subject font-black bold uppercase">Add Announcements</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal" enctype="multipart/form-data" action="<?php echo base_url();?>upload/anouncement" method="post" id="myform" >
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Title
										</label>
										<div class="col-md-6">
											<input type="text" name="txttitle" id="txttitle" class="form-control input-sm empty" />
											<span id="divtxttitle" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">For
											
										</label>
										<div class="col-md-6">
											<select id="ddannfor" name="ddannfor" class="form-control input-sm opt">
												<option value="1">Employee's</option>
												<option value="2">Client's</option>
												<option value="3">For All</option>
											</select>
											<span id="divddannfor" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Comment
											
										</label>
										<div class="col-md-9">
											<textarea rows="5" name="txtdiscription" id="txtdiscription" class="form-control input-sm empty" data-provide="markdown" data-error-container="#editor_error"></textarea>
											<span id="divtxtdiscription" style="color:red"></span>
											<div id="editor_error">
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Attachments</label>
										<div class="col-md-6">
											<div class="fileinput fileinput-new" data-provides="fileinput">
												<div class="input-group">
													
													<div class="form-control uneditable-input input-fixed " data-trigger="fileinput">
														<i class="fa fa-file fileinput-exists"></i>&nbsp;
														<span class="fileinput-filename">
														</span>
													</div>
													
													<span class="input-group-addon btn default btn-file">
														<span class="fileinput-new"> Select file </span>
														<span class="fileinput-exists"> Change </span>
														<input type="file" class="emptyfile" name="userfile" id="userfile" >
													</span>
													<a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
													
													
												</div>
											</div>
											<span id="divuserfile" style="color:red"></span>
										</div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="button" onclick="conwv('myform')" class="btn green">Submit</button>
										<button type="button" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-settings font-dark"></i>
								
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>
											S. No.
										</th>
										<th>
											Title
										</th>
										<th>
											Action
										</th>
										
										
									</tr>
								</thead>
								<tbody>
									
									<?php  
										$i=1;
										foreach($rec->result() as $row)
										{
											if(($row->id)!=null)
											{
												
											?>
											
											<tr class="odd gradeX">
												<td>
													<?php echo $i ?>
												</td>
												<td>
													<?php echo $row->txttitle ?>
												</td>
												<td>
													<div class="btn-group">
														<button class="btn red btn-sm dropdown-toggle" data-toggle="dropdown">Action
															<i class="fa fa-angle-down"></i>
														</button>
														<ul class="dropdown-menu">
															<li>
																<a href="<?php echo base_url();?>admin/edit_anouncement/<?php echo $row->id ?>" ><i class="fa fa-edit"></i> Edit</a>
															</li>
															<li>
																<?php
																	if($row->txtstatus=='1')
																	{
																	?>
																	<a href="<?php echo base_url();?>admin/Announce_delete/<?php echo $row->id ?>" ><i class="fa fa-times"></i> Disable</a>
																	
																	<?php }
																	else
																	{
																	?>
																	<a href="<?php echo base_url();?>admin/announce_enable/<?php echo $row->id ?>" ><i class="fa fa-plus"></i> Enable</a>
																	<?php }
																?>
															</li>
															
														</ul>
													</div>
												</td>
											</tr>
											<?php
												$i++;
											}
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													


