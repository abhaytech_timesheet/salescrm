<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
						<a href="<?php echo base_url(); ?>crm/view_account/">
							CRM
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-bubbles"></i>
						<a href="<?php echo base_url(); ?>crm/view_account/0">Account</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-bubbles"></i>
						<a href="<?php echo base_url(); ?>crm/view_account/0">Ticket</a>
						<i class="fa fa-ticket"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">Admin Ticket Panel for Account</h3>
			<!-- END PAGE TITLE-->
			
			
			<div class="row">
				<div class="col-md-12 blog-page">
					
					<h1>Ticket Type</h1>
					<div class="row">
						<div class="col-md-3">
							
							<div class="top-news">
								<a href="<?php echo base_url();?>admin/submit_ticket/1/<?php echo $this->uri->segment(3);?>" class="btn red">
									<span>
										Support Ticket
									</span><br />
									<em>Technical Assistance Related<br>
										to your Domain Registration,<br />
									Web Hosting and Servers</em>
									<i class="fa fa-book top-news-icon"></i>
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="top-news">
								<a href="<?php echo base_url();?>admin/submit_ticket/2/<?php echo $this->uri->segment(3);?>" class="btn green">
									<span>
										Sales Ticket
									</span><br />
									<em>Pre Sales, Sales Assistance</em>
									<em>
										
										<br />
									</em>
									<i class="fa fa-book top-news-icon"></i>
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="top-news">
								<a href="<?php echo base_url();?>admin/submit_ticket/3/<?php echo $this->uri->segment(3);?>" class="btn blue">
									<span>
										Development Ticket
									</span><br />
									<em>Web Design,
									Web Development,<br /> SEO/SEM Queries</em>
									<em>
									</em>
									<i class="fa fa-book top-news-icon"></i>
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="top-news">
								<a href="<?php echo base_url();?>admin/submit_ticket/4/<?php echo $this->uri->segment(3);?>" class="btn yellow">
									<span>
										Complaints Ticket
									</span><br />
									<em>Abuse issues, complaints</em>
									<em>
										
										<br />
									</em>
									<i class="fa fa-book top-news-icon"></i>
								</a>
							</div>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->
