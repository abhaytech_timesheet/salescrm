<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		
		
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Affiliates Ticket Panel for Account
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo base_url();?>index.php/crm/view_account">
							Account
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">
							Submit Ticket
						</a>
						
					</li>
					
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12 blog-page">
				
				<h1>Ticket Type</h1>
				<div class="row">
					<div class="col-md-3">
                        
						<div class="top-news">
							<a href="<?php echo base_url();?>index.php/admin/submit_ticket/1/<?php echo $this->uri->segment(3);?>" class="btn red">
								<span>
									Support Ticket
								</span><br>
								<em>Technical Assistance Related<br />
									to your Domain Registration,<br />
								Web Hosting and Servers</em>
								<i class="fa fa-book top-news-icon"></i>
							</a>
						</div>
					</div>
					<div class="col-md-3">
						<div class="top-news">
							<a href="<?php echo base_url();?>index.php/admin/submit_ticket/2/<?php echo $this->uri->segment(3);?>" class="btn green">
								<span>
									Sales Ticket
								</span><br>
								<em>Pre Sales, Sales Assistance</em>
								<em>
									<br />
                                    <br />
								</em>
								<i class="fa fa-book top-news-icon"></i>
							</a>
						</div>
					</div>
					<div class="col-md-3">
						<div class="top-news">
							<a href="<?php echo base_url();?>index.php/admin/submit_ticket/3/<?php echo $this->uri->segment(3);?>" class="btn blue">
								<span>
									Development Ticket
								</span>
								<em>Web Design,<br />
								Web Development,<br /> SEO/SEM Queries</em>
								<em>
								</em>
								<i class="fa fa-book top-news-icon"></i>
							</a>
						</div>
					</div>
					<div class="col-md-3">
						<div class="top-news">
							<a href="<?php echo base_url();?>index.php/admin/submit_ticket/4/<?php echo $this->uri->segment(3);?>" class="btn yellow">
								<span>
									Complaints Ticket
								</span><br>
								<em>Abuse issues, complaints</em>
								<em>
									<br />
                                    <br />
								</em>
								<i class="fa fa-book top-news-icon"></i>
							</a>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->