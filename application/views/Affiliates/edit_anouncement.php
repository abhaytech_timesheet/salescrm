<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Admin Panel 
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="btn-group">
							<button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							<span>
								Actions
							</span>
							<i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="#">
										Action
									</a>
								</li>
								<li>
									<a href="#">
										Another action
									</a>
								</li>
								<li>
									<a href="#">
										Something else here
									</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">
										Separated link
									</a>
								</li>
							</ul>
						</li>
						<li>
							<i class="fa fa-home"></i>
							<a href="#">
								Dashboard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Edit Announcements
							</a>
							
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
			<!--row-->
            <div class="col-md-6">
            	<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Edit Announcements
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
                        <?php  
							
							foreach($rec->result() as $row)
							{
								if(($row->id)!=null)
								{
									 $id=$row->id;	
									 $title=$row->txttitle;
									 $des=$row->txtdescription;
								 }
							}
							
					  ?>
							<form role="form" class="form-horizontal" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/upload/anouncement_edit/<?php echo $id ?>" method="post">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Title</label>
										<div class="col-md-9">
											<input type="text" name="txttitle" id="txttitle" value="<?php echo $title ?>" class="form-control input-large" />
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Comment</label>
										<div class="col-md-8">
											<textarea name="txtdescription" id="txtdescription" class="form-control" data-provide="markdown" data-error-container="#editor_error">
												<?php echo $des ?>
                                            </textarea>
										</div>
									</div>
                                     <div class="form-group">
										<label class="col-md-3 control-label">Attachments</label>
										<div class="col-md-9">
											
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
												<div class="input-group input-large">
													<div class="form-control uneditable-input span3" data-trigger="fileinput">
														<i class="fa fa-file fileinput-exists"></i>&nbsp;
														<span class="fileinput-filename">
														</span>
													</div>
													<span class="input-group-addon btn default btn-file">
														<span class="fileinput-new">
															 Select file
														</span>
														<span class="fileinput-exists">
															 Change
														</span>
														<input type="file"  name="userfile" id="userfile">
													</span>
													<a href="#" class="input-group-addon btn default fileinput-exists" data-dismiss="fileinput">
														 Remove
													</a>
												</div>
											</div>
										</div>
									</div>
                                </div>
									<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Submit</button>
										<button type="button" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
					</div>
            <div class="col-md-6">
                	<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Managed Table
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							
								<table class="table table-striped table-bordered table-hover">
								<thead>
								<tr>
									<th>
										 S. No.
									</th>
									<th>
										 Title
									</th>
									<th>
										 Edit
									</th>
                                    <th>
										 Delete
									</th>
									
								</tr>
								</thead>
								<tbody>
                                
                                  <?php  
											$i=1;
										foreach($rec1->result() as $row)
										{
											if(($row->id)!=null)
											{
												
								  ?>
                                  
								<tr class="odd gradeX">
									<td>
										 <?php echo $i ?>
									</td>
									<td>
										 <?php echo $row->txttitle ?>
									</td>
									<td>
										 <a href="<?php echo base_url();?>index.php/admin/edit_anouncement/<?php echo $row->id ?>">
                       						<span class="btn btn-sm purple"><i class="fa fa-edit"></i> Edit</span></a>
									</td>
                                    <td>
                                    <?php
									if($row->txtstatus=='1')
									{
										?>
										 <a href="<?php echo base_url();?>index.php/admin/Announce_delete/<?php echo $row->id ?>">
                       					<span class="btn btn-sm red"><i class="fa fa-times"></i> Disable</span></a>
                                        
                                      <?php }
									  else
									  {
										  ?>
										  <a href="<?php echo base_url();?>index.php/admin/announce_enable/<?php echo $row->id ?>">
                       					<span class="btn btn-sm green"><i class="fa fa-plus"></i> Enable</span></a>
									  <?php }
									  ?>
									</td>
									
									</tr>
                                 <?php
								 $i++;
										  }
										}
								  ?>
								</tbody>
								</table>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
					
				</div>
			</div>
           
            
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
