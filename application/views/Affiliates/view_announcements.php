<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			
			
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Admin Panel 
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						
						<li>
							<i class="fa fa-home"></i>
							<a href="#">
								Dashboard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								All Announcements
							</a>
							
						</li>
							<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" >
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                	<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Managed Table
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							
								<table class="table table-striped table-bordered table-hover">
								<thead>
								<tr>
									<th>
										 S. No.
									</th>
									<th>
										 Title
									</th>
									<th>
										 Edit
									</th>
                                    <th>
										 Delete
									</th>
									
								</tr>
								</thead>
								<tbody>
                                
                                  <?php  
											$i=1;
										foreach($rec->result() as $row)
										{
											if(($row->id)!=null)
											{
												
								  ?>
                                  
								<tr class="odd gradeX">
									<td>
										 <?php echo $i ?>
									</td>
									<td>
										 <?php echo $row->txttitle ?>
									</td>
									<td>
										 <a href="<?php echo base_url();?>/index.php/admin/edit_anouncement/<?php echo $row->id ?>">
                       						Edit</a>
									</td>
                                    <td>
										 <a href="<?php echo base_url();?>/index.php/admin/Announce_delete/<?php echo $row->id ?>">
                       					Delete</a>
									</td>
									
									</tr>
                                 <?php
								 $i++;
										  }
										}
								  ?>
								</tbody>
								</table>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
					
				</div>
			</div>
            <!--/row-->
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
