<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="#.">Client Panel </a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Dashboard</span>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<?php 
				foreach($rec->result() as $row)
				{
					$p=$row->or_login_pwd;
					break;
				}
			?>
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Change Password</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-7">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>
								<span class="caption-subject bold uppercase">Change Password</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal" action="<?php echo base_url();?>index.php/affiliates/password_update/" method="post" id="myform" onsubmit="return check(conwv('myform'))">
								<input type="hidden" value="<?php echo $p;?>" name="op" id="op" />
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Existing Password</label>
										<div class="col-md-7">
											<input type="password" name="txtepassword" id="txtepassword" class="form-control input-sm empty" />
											<span id="divtxtepassword" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">New Password</label>
										<div class="col-md-7">
											<input type="password" name="txtpassword" id="txtpassword" class="form-control input-sm empty" />
											<span id="divtxtpassword" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Confirm New Password</label>
										<div class="col-md-7">
											<input type="password" name="txtcpassword" id="txtcpassword" class="form-control input-sm empty" />
											<span id="divtxtconfirm" style="color:red;"></span>
										</div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn blue">Submit</button>
										<button type="button" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													
