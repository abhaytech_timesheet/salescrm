	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN STYLE CUSTOMIZER -->
			<div class="theme-panel hidden-xs hidden-sm">
				<div class="toggler">
				</div>
				<div class="toggler-close">
				</div>
				<div class="theme-options">
					<div class="theme-option theme-colors clearfix">
						<span>
							 THEME COLOR
						</span>
						<ul>
							<li class="color-black current color-default" data-style="default">
							</li>
							<li class="color-blue" data-style="blue">
							</li>
							<li class="color-brown" data-style="brown">
							</li>
							<li class="color-purple" data-style="purple">
							</li>
							<li class="color-grey" data-style="grey">
							</li>
							<li class="color-white color-light" data-style="light">
							</li>
						</ul>
					</div>
					<div class="theme-option">
						<span>
							 Layout
						</span>
						<select class="layout-option form-control input-small">
							<option value="fluid" selected="selected">Fluid</option>
							<option value="boxed">Boxed</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
							 Header
						</span>
						<select class="header-option form-control input-small">
							<option value="fixed" selected="selected">Fixed</option>
							<option value="default">Default</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
							 Sidebar
						</span>
						<select class="sidebar-option form-control input-small">
							<option value="fixed">Fixed</option>
							<option value="default" selected="selected">Default</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
							 Sidebar Position
						</span>
						<select class="sidebar-pos-option form-control input-small">
							<option value="left" selected="selected">Left</option>
							<option value="right">Right</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
							 Footer
						</span>
						<select class="footer-option form-control input-small">
							<option value="fixed">Fixed</option>
							<option value="default" selected="selected">Default</option>
						</select>
					</div>
				</div>
			</div>
			<!-- END STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Customer Registration
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="btn-group">
							<button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							<span>
								Actions
							</span>
							<i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="#">
										Action
									</a>
								</li>
								<li>
									<a href="#">
										Another action
									</a>
								</li>
								<li>
									<a href="#">
										Something else here
									</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">
										Separated link
									</a>
								</li>
							</ul>
						</li>
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">
								Home
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Form Stuff
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Form Layouts
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom boxless tabbable-reversed">
						<div class="tab-content">
							  <div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Customer Registration
										</div>
										<div class="tools">
											<a href="javascript:;" class="collapse">
											</a>
											<a href="#portlet-config" data-toggle="modal" class="config">
											</a>
											<a href="javascript:;" class="reload">
											</a>
											<a href="javascript:;" class="remove">
											</a>
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<form action="#" class="form-horizontal">
											<div class="form-body">
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label class="control-label col-md-3">Applicant Form Number</label>
															<div class="col-md-9">
																<input type="text" class="form-control">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Branch Code For Payment</label>
															<div class="col-md-9">
																<select class="form-control">
																	<option value="">Select..</option>
                                                                    <option value="">Branch 1</option>
																	<option value="">Branch 2</option>
																</select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">CSS No.</label>
															<div class="col-md-9">
																<input type="text" class="form-control" value="1" readonly disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                <h3> Applicant Details </h3>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Date </label>
															<div class="col-md-9">
																<input type="text" class="form-control">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Applicant Name</label>
															<div class="col-md-9">
																<input type="text" class="form-control">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Father/Huband's Name</label>
															<div class="col-md-9">
																<input type="text" class="form-control">
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Gender </label>
															<div class="col-md-9">
																<select class="form-control">
																	<option value="">Select..</option>
                                                                    <option value="">Male</option>
																	<option value="">Female</option>
																</select>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Permanent Address</label>
															<div class="col-md-9">
																<input type="text" class="form-control">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">State</label>
															<div class="col-md-9">
																<select class="form-control">
                                                                    <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                                                    <option value="Andhra Pradesh">Andhra Pradesh</option>
                                                                    <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                                                    <option value="Assam">Assam</option>
                                                                    <option value="Bihar">Bihar</option>
                                                                    <option value="Chandigarh">Chandigarh</option>
                                                                    <option value="Chattisgarh">Chattisgarh</option>
                                                                    <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                                                    <option value="Daman and Diu">Daman and Diu</option>
                                                                    <option value="Delhi">Delhi</option>
                                                                    <option value="Goa">Goa</option>
                                                                    <option value="Gujarat">Gujarat</option>
                                                                    <option value="Haryana">Haryana</option>
                                                                    <option value="Himachal Pradesh">Himachal Pradesh</option>
                                                                    <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                                                                    <option value="Jharkhand">Jharkhand</option>
                                                                    <option value="Karnataka">Karnataka</option>
                                                                    <option value="Kerala">Kerala</option>
                                                                    <option value="Lakshadweep">Lakshadweep</option>
                                                                    <option value="Madhya Pradesh">Madhya Pradesh</option>
                                                                    <option value="Maharashtra">Maharashtra</option>
                                                                    <option value="Manipur">Manipur</option>
                                                                    <option value="Meghalaya">Meghalaya</option>
                                                                    <option value="Mizoram">Mizoram</option>
                                                                    <option value="Nagaland">Nagaland</option>
                                                                    <option value="Orissa">Orissa</option>
                                                                    <option value="Puducherry">Puducherry</option>
                                                                    <option value="Punjab">Punjab</option>
                                                                    <option value="Rajasthan">Rajasthan</option>
                                                                    <option value="Sikkim">Sikkim</option>
                                                                    <option value="Tamil Nadu">Tamil Nadu</option>
                                                                    <option value="Tripura">Tripura</option>
                                                                    <option selected="selected" value="Uttar Pradesh">Uttar Pradesh</option>
                                                                    <option value="Uttarakhand">Uttarakhand</option>
                                                                    <option value="West Bengal">West Bengal</option>
                                                            
                                                                </select>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">District </label>
															<div class="col-md-9">
																<select class="form-control">
                                                                    <option value="Select">Select</option>
                                                                    <option value="1">kanpr</option>
                                                                    <option value="2">Lucknow</option>
                                                                    <option value="26">Agra</option>
                                                                    <option value="27">Firozabad</option>
                                                                    <option value="28">Mainpuri</option>
                                                                    <option value="29">Mathura</option>
                                                                    <option value="30">Aligarh</option>
                                                                    <option value="31">Etah</option>
                                                                    <option value="32">Mahamaya Nagar</option>
                                                                    <option value="33">Kanshiram Nagar</option>
                                                                    <option value="34">Allahabad</option>
                                                                    <option value="35">Fatehpur</option>
                                                                    <option value="36">Kaushambi</option>
                                                                    <option value="37">Pratapgarh</option>
                                                                    <option value="38">Azamgarh</option>
                                                                    <option value="39">Ballia</option>
                                                                    <option value="40">Mau</option>
                                                                    <option value="41">Badaun</option>
                                                                    <option value="42">Bareilly</option>
                                                                    <option value="43">Pilibhit</option>
                                                                    <option value="44">Shahjahanpur</option>
                                                                    <option value="45">Basti</option>
                                                                    <option value="46">Sant Kabir Nagar</option>
                                                                    <option value="47">Siddharthnagar</option>
                                                                    <option value="48">Banda</option>
                                                                    <option value="49">Chitrakoot</option>
                                                                    <option value="50">Hamirpur</option>
                                                                    <option value="51">Mahoba</option>
                                                                    <option value="52">Bahraich</option>
                                                                    <option value="53">Balarampur</option>
                                                                    <option value="54">Gonda</option>
                                                                    <option value="55">Shravasti</option>
                                                                    <option value="56">Ambedkar Nagar</option>
                                                                    <option value="57">Barabanki</option>
                                                                    <option value="58">Faizabad</option>
                                                                    <option value="59">Sultanpur</option>
                                                                    <option value="60">Chhatrapati Shahuji Maharaj Nagar</option>
                                                                    <option value="61">Deoria</option>
                                                                    <option value="62">Gorakhpur</option>
                                                                    <option value="63">Kushinagar</option>
                                                                    <option value="64">Maharajganj</option>
                                                                    <option value="65">Jalaun</option>
                                                                    <option value="66">Jhansi</option>
                                                                    <option value="67">Lalitpur</option>
                                                                    <option value="68">Auraiya</option>
                                                                    <option value="69">Etawah</option>
                                                                    <option value="70">Farrukhabad</option>
                                                                    <option value="71">Kannauj</option>
                                                                    <option value="72">Kanpur Dehat</option>
                                                                    <option value="73">Kanpur Nagar</option>
                                                                    <option value="74">Hardoi</option>
                                                                    <option value="75">Lakhimpur Kheri</option>
                                                                    <option value="76">Raebareli</option>
                                                                    <option value="77">Sitapur</option>
                                                                    <option value="78">Unnao</option>
                                                                    <option value="79">Bagpat</option>
                                                                    <option value="80">Bulandshahr</option>
                                                                    <option value="81">Gautam Buddha Nagar</option>
                                                                    <option value="82">Ghaziabad</option>
                                                                    <option value="83">Meerut</option>
                                                                    <option value="84">Panchsheel Nagar (Hapur)</option>
                                                                    <option value="85">Mirzapur</option>
                                                                    <option value="86">Sant Ravidas Nagar</option>
                                                                    <option value="87">Sonbhadra</option>
                                                                    <option value="88">Sambhal</option>
                                                                    <option value="89">Bijnor</option>
                                                                    <option value="90">Jyotiba Phule Nagar</option>
                                                                    <option value="91">Moradabad</option>
                                                                    <option value="92">Rampur</option>
                                                                    <option value="93">Muzaffarnagar</option>
                                                                    <option value="94">Prabuddha Nagar (Shamli)</option>
                                                                    <option value="95">Saharanpur</option>
                                                                    <option value="96">Chandauli</option>
                                                                    <option value="97">Ghazipur</option>
                                                                    <option value="98">Jaunpur</option>
                                                                    <option value="99">Varanasi</option>
                                                                    <option value="226">Itawa</option>
                                                                    <option value="227">Lakhimpur</option>
                                                                    <option value="228">Kunda</option>
                                                                    <option value="229">raibarelly</option>
                                                                    <option value="231">fukabad</option>
                                                                    <option value="232">gkp5</option>
                                                                    <option value="233">gorakhpur golghar</option>
                                                            
                                                                </select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Pin Code</label>
															<div class="col-md-9">
																<input type="text" class="form-control">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Address Proof </label>
															<div class="col-md-9">
																<select class="form-control">
                                                                    <option value="--Select--">--Select--</option>
                                                                    <option value="Pan Card">Pan Card</option>
                                                                    <option value="Passport">Passport</option>
                                                                    <option value="Driving Licence">Driving Licence</option>
                                                                    <option value="Ration Card">Ration Card</option>
                                                                    <option value="Voter Id">Voter Id</option>
                                                                    <option value="Domicile Certificate">Domicile Certificate</option>
                                                                    <option value="Other">Other</option>
                                                            
                                                                </select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Mobile Number</label>
															<div class="col-md-9">
																<input type="text" class="form-control">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Date of Birth </label>
															<div class="col-md-9">
																<input type="text" class="form-control">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Age </label>
															<div class="col-md-9">
																<input type="text" class="form-control" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                <h3>Nominee Details</h3>
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Nominee Name </label>
															<div class="col-md-9">
																<input type="text" class="form-control">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Permanent Address</label>
															<div class="col-md-9">
																<textarea class="form-control"></textarea>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Gender </label>
															<div class="col-md-9">
																<select class="form-control">
																	<option value="">Select..</option>
                                                                    <option value="">Male</option>
																	<option value="">Female</option>
																</select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">State </label>
															<div class="col-md-9">
																<select class="form-control">
                                                                    <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                                                    <option value="Andhra Pradesh">Andhra Pradesh</option>
                                                                    <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                                                    <option value="Assam">Assam</option>
                                                                    <option value="Bihar">Bihar</option>
                                                                    <option value="Chandigarh">Chandigarh</option>
                                                                    <option value="Chattisgarh">Chattisgarh</option>
                                                                    <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                                                    <option value="Daman and Diu">Daman and Diu</option>
                                                                    <option value="Delhi">Delhi</option>
                                                                    <option value="Goa">Goa</option>
                                                                    <option value="Gujarat">Gujarat</option>
                                                                    <option value="Haryana">Haryana</option>
                                                                    <option value="Himachal Pradesh">Himachal Pradesh</option>
                                                                    <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                                                                    <option value="Jharkhand">Jharkhand</option>
                                                                    <option value="Karnataka">Karnataka</option>
                                                                    <option value="Kerala">Kerala</option>
                                                                    <option value="Lakshadweep">Lakshadweep</option>
                                                                    <option value="Madhya Pradesh">Madhya Pradesh</option>
                                                                    <option value="Maharashtra">Maharashtra</option>
                                                                    <option value="Manipur">Manipur</option>
                                                                    <option value="Meghalaya">Meghalaya</option>
                                                                    <option value="Mizoram">Mizoram</option>
                                                                    <option value="Nagaland">Nagaland</option>
                                                                    <option value="Orissa">Orissa</option>
                                                                    <option value="Puducherry">Puducherry</option>
                                                                    <option value="Punjab">Punjab</option>
                                                                    <option value="Rajasthan">Rajasthan</option>
                                                                    <option value="Sikkim">Sikkim</option>
                                                                    <option value="Tamil Nadu">Tamil Nadu</option>
                                                                    <option value="Tripura">Tripura</option>
                                                                    <option selected="selected" value="Uttar Pradesh">Uttar Pradesh</option>
                                                                    <option value="Uttarakhand">Uttarakhand</option>
                                                                    <option value="West Bengal">West Bengal</option>
                                                            
                                                                </select>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">District </label>
															<div class="col-md-9">
																<select class="form-control">
                                                                    <option value="Select">Select</option>
                                                                    <option value="1">kanpr</option>
                                                                    <option value="2">Lucknow</option>
                                                                    <option value="26">Agra</option>
                                                                    <option value="27">Firozabad</option>
                                                                    <option value="28">Mainpuri</option>
                                                                    <option value="29">Mathura</option>
                                                                    <option value="30">Aligarh</option>
                                                                    <option value="31">Etah</option>
                                                                    <option value="32">Mahamaya Nagar</option>
                                                                    <option value="33">Kanshiram Nagar</option>
                                                                    <option value="34">Allahabad</option>
                                                                    <option value="35">Fatehpur</option>
                                                                    <option value="36">Kaushambi</option>
                                                                    <option value="37">Pratapgarh</option>
                                                                    <option value="38">Azamgarh</option>
                                                                    <option value="39">Ballia</option>
                                                                    <option value="40">Mau</option>
                                                                    <option value="41">Badaun</option>
                                                                    <option value="42">Bareilly</option>
                                                                    <option value="43">Pilibhit</option>
                                                                    <option value="44">Shahjahanpur</option>
                                                                    <option value="45">Basti</option>
                                                                    <option value="46">Sant Kabir Nagar</option>
                                                                    <option value="47">Siddharthnagar</option>
                                                                    <option value="48">Banda</option>
                                                                    <option value="49">Chitrakoot</option>
                                                                    <option value="50">Hamirpur</option>
                                                                    <option value="51">Mahoba</option>
                                                                    <option value="52">Bahraich</option>
                                                                    <option value="53">Balarampur</option>
                                                                    <option value="54">Gonda</option>
                                                                    <option value="55">Shravasti</option>
                                                                    <option value="56">Ambedkar Nagar</option>
                                                                    <option value="57">Barabanki</option>
                                                                    <option value="58">Faizabad</option>
                                                                    <option value="59">Sultanpur</option>
                                                                    <option value="60">Chhatrapati Shahuji Maharaj Nagar</option>
                                                                    <option value="61">Deoria</option>
                                                                    <option value="62">Gorakhpur</option>
                                                                    <option value="63">Kushinagar</option>
                                                                    <option value="64">Maharajganj</option>
                                                                    <option value="65">Jalaun</option>
                                                                    <option value="66">Jhansi</option>
                                                                    <option value="67">Lalitpur</option>
                                                                    <option value="68">Auraiya</option>
                                                                    <option value="69">Etawah</option>
                                                                    <option value="70">Farrukhabad</option>
                                                                    <option value="71">Kannauj</option>
                                                                    <option value="72">Kanpur Dehat</option>
                                                                    <option value="73">Kanpur Nagar</option>
                                                                    <option value="74">Hardoi</option>
                                                                    <option value="75">Lakhimpur Kheri</option>
                                                                    <option value="76">Raebareli</option>
                                                                    <option value="77">Sitapur</option>
                                                                    <option value="78">Unnao</option>
                                                                    <option value="79">Bagpat</option>
                                                                    <option value="80">Bulandshahr</option>
                                                                    <option value="81">Gautam Buddha Nagar</option>
                                                                    <option value="82">Ghaziabad</option>
                                                                    <option value="83">Meerut</option>
                                                                    <option value="84">Panchsheel Nagar (Hapur)</option>
                                                                    <option value="85">Mirzapur</option>
                                                                    <option value="86">Sant Ravidas Nagar</option>
                                                                    <option value="87">Sonbhadra</option>
                                                                    <option value="88">Sambhal</option>
                                                                    <option value="89">Bijnor</option>
                                                                    <option value="90">Jyotiba Phule Nagar</option>
                                                                    <option value="91">Moradabad</option>
                                                                    <option value="92">Rampur</option>
                                                                    <option value="93">Muzaffarnagar</option>
                                                                    <option value="94">Prabuddha Nagar (Shamli)</option>
                                                                    <option value="95">Saharanpur</option>
                                                                    <option value="96">Chandauli</option>
                                                                    <option value="97">Ghazipur</option>
                                                                    <option value="98">Jaunpur</option>
                                                                    <option value="99">Varanasi</option>
                                                                    <option value="226">Itawa</option>
                                                                    <option value="227">Lakhimpur</option>
                                                                    <option value="228">Kunda</option>
                                                                    <option value="229">raibarelly</option>
                                                                    <option value="231">fukabad</option>
                                                                    <option value="232">gkp5</option>
                                                                    <option value="233">gorakhpur golghar</option>
                                                            
                                                                </select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Age </label>
															<div class="col-md-9">
																<input type="text" class="form-control">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Relationship </label>
															<div class="col-md-9">
																<input type="text" class="form-control">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">NomineeFather/Husband's Name</label>
															<div class="col-md-9">
																<input type="text" class="form-control">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                <h3>Plan Details</h3>
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Plan No </label>
															<div class="col-md-9">
																<select class="form-control">
                                                                    <option selected="selected" value="Select">Select</option>
                                                                    <option value="PLAN(F-1)">PLAN(F-1)</option>
                                                                    <option value="PLAN(F-2)">PLAN(F-2)</option>
                                                                    <option value="PLAN(F-3)">PLAN(F-3)</option>
                                                                    <option value="PLAN(F-4)">PLAN(F-4)</option>
                                                                    <option value="PLAN(F-5)">PLAN(F-5)</option>
                                                                    <option value="PLAN(F-6)">PLAN(F-6)</option>
                                                                    <option value="PLAN(F-7)">PLAN(F-7)</option>
                                                                    <option value="PLAN(F-8)">PLAN(F-8)</option>
                                                                    <option value="PLAN(F-9)">PLAN(F-9)</option>
                                                                    <option value="PLAN(M-1)">PLAN(M-1)</option>
                                                                    <option value="PLAN(M-2)">PLAN(M-2)</option>
                                                                    <option value="PLAN(M-3)">PLAN(M-3)</option>
                                                                    <option value="PLAN(M-4)">PLAN(M-4)</option>
                                                                    <option value="PLAN(R-1)">PLAN(R-1)</option>
                                                                    <option value="PLAN(R-10)">PLAN(R-10)</option>
                                                                    <option value="PLAN(R-2)">PLAN(R-2)</option>
                                                                    <option value="PLAN(R-3)">PLAN(R-3)</option>
                                                                    <option value="PLAN(R-4)">PLAN(R-4)</option>
                                                                    <option value="PLAN(R-5)">PLAN(R-5)</option>
                                                                    <option value="PLAN(R-6)">PLAN(R-6)</option>
                                                                    <option value="PLAN(R-7)">PLAN(R-7)</option>
                                                                    <option value="PLAN(R-8)">PLAN(R-8)</option>
                                                                    <option value="PLAN(R-9)">PLAN(R-9)</option>
                                                            
                                                                </select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Plan Name</label>
															<div class="col-md-9">
																<input type="text" class="form-control" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Term of Plan (months)</label>
															<div class="col-md-9">
																<input type="text" class="form-control" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Land Unit </label>
															<div class="col-md-9">
																<select class="form-control">
																	<option>Select..</option>
																</select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Total Consideration Value </label>
															<div class="col-md-9">
																<input type="text" class="form-control" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Redemption Realizable Value </label>
															<div class="col-md-9">
																<input type="text" class="form-control" disabled="disabled">
															</div>
														</div>
													</div>
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Mode </label>
															<div class="col-md-9">
																<select class="form-control" disabled="disabled">
																	<option>Select..</option>
																	<option>Country 2</option>
																</select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Payment(1st Installment) </label>
															<div class="col-md-9">
																<input type="text" class="form-control" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Amount in Words  </label>
															<div class="col-md-9">
																<textarea class="form-control" disabled="disabled"></textarea>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Installment Due Date </label>
															<div class="col-md-9">
																<input type="text" class="form-control" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Date of Last Installment</label>
															<div class="col-md-9">
																<input type="text" class="form-control" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Date Of Redemption Value </label>
															<div class="col-md-9">
																<input type="text" class="form-control" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
													
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Application Charges </label>
															<div class="col-md-9">
																<input type="text" class="form-control">
															</div>
														</div>
													</div>
													
												</div>
												<!--/row-->
                                                <h3>Payment Details</h3>
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Mode of Deposit </label>
															<div class="col-md-9">
																<select class="form-control">
                                                                    <option selected="selected" value="Cash">Cash</option>
                                                                    <option value="Cheque">Cheque</option>
                                                                    <option value="DD">DD</option>
                                                            
                                                                </select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Payment Date </label>
															<div class="col-md-9">
																<input type="text" class="form-control">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Amount </label>
															<div class="col-md-9">
																<input type="text" class="form-control" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Amount in Words</label>
															<div class="col-md-9">
																<textarea class="form-control" disabled="disabled"></textarea>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                <h3>Agent Details</h3>
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Agency Code </label>
															<div class="col-md-9">
																<input type="text" class="form-control">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Agency  Name </label>
															<div class="col-md-9">
																<input type="text" class="form-control" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Branch Name </label>
															<div class="col-md-9">
																<input type="text" class="form-control" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Branch Code</label>
															<div class="col-md-9">
																<input type="text" class="form-control" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                <h3>Branch Details</h3>
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Branch Name </label>
															<div class="col-md-9">
																<select class="form-control">
                                                                    <option selected="selected" value="Select">Select</option>
                                                                    <option value="K1-0512">Branch1</option>
                                                                    <option value="L1-0522">Branch2</option>
                                                            
                                                                </select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Branch Address </label>
															<div class="col-md-9">
																<textarea class="form-control" disabled="disabled"></textarea>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Spot Income </label>
															<div class="col-md-9">
																<input type="text" class="form-control" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Membership</label>
															<div class="radio-list">
																<label class="radio-inline">
																<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked> Paid </label>
																<label class="radio-inline">
																<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2"> Due </label>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
											</div>
											
										</form>
										<!-- END FORM-->
									</div>
								</div>
						  <div class="tab-pane" id="tab_4">
							  <div class="portlet box blue"></div>
							</div>
						  <div class="tab-pane" id="tab_6">
							  <div class="portlet box blue "></div>
							</div>
							<div class="tab-pane" id="tab_7">
								<div class="portlet box blue ">
									<div class="portlet-title">
										<div class="caption">
										<i class="fa fa-reorder"></i></div>
										<div class="tools">
											<a href="javascript:;" class="collapse">
											</a>
											<a href="#portlet-config" data-toggle="modal" class="config">
											</a>
											<a href="javascript:;" class="reload">
											</a>
											<a href="javascript:;" class="remove">
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
