	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Admin Panel 
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="btn-group">
							<button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							<span>
								Actions
							</span>
							<i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="#">
										Action
									</a>
								</li>
								<li>
									<a href="#">
										Another action
									</a>
								</li>
								<li>
									<a href="#">
										Something else here
									</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">
										Separated link
									</a>
								</li>
							</ul>
						</li>
						<li>
							<i class="fa fa-home"></i>
							<a href="#">
								Dashboard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Assign Ticket
							</a>
							
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                	<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Ticket
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
                        
							<table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
							<thead>
							<tr>
								<th>
									 #
								</th>
								<th>
									 Department
								</th>
								<th>
									 Subject
								</th>
								<th>
									 Status
								</th>
								<th>
									 Urgency
								</th>
                                <th>
									 Date
								</th>
                                <th>
									 Assign to Employee
								</th>
                                <th class="ignore">
									 Reassign Ticket
								</th>
							</tr>
							</thead>
                            <tbody>
                           <?php  
							$sn=1;
									foreach($rec->result() as $row)
									{
										
										
							?>
							<tr>
                            	<td>
									 <?php echo $sn; ?>
								</td>
								<td>
                                	<?php echo $row->Department ?>
								</td>
								<td>
									<a href="<?php echo base_url();?>index.php/admin/view_ticket/<?php echo $row->Ticket_reference_no ?>" style="color:#0066FF; text-decoration:none;">
										<?php echo '#'.($row->Ticket_reference_no).' - '.$row->Subject ?>
                                        <input type="hidden" name="hd" id="hd" value="<?php echo $row->Ticket_reference_no ?>" />
                        			</a>
								</td>
								<td>
                                	 <?php echo $row->STATUS; ?>
								</td>
								<td>
									<?php echo $row->Urgency;?>
								</td>
								<td>
									 <?php echo substr($row->Sub_date,0,10) ?>
								</td>
                                
                                <td>
									 
									<?php
                                   
												echo $row->User_name;
										
                                        ?>
									
								</td>
                                <td>
								<div id="ignore<?php echo $row->Ticket_reference_no; ?>">
								<?php 
								if($row->STATUS=='ACTIVE')
								{
								?>
                                	<a class="btn green" onclick="popup(<?php echo $row->Ticket_reference_no; ?>,<?php echo $row->Assign_to_id; ?>,<?php echo $row->Dep_id; ?>)">
										 Reassign <i class="fa fa-mail-reply-all"></i>
									</a>
								<?php
								}
								?>
								</div>
                                </td>
							</tr>
							<?php
								$sn++;
								
										
								
									}
									
							  ?>
                              </tbody>
							</table>
						</div>
					</div>
					
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>

<script>
function popup(id,assign_id,tickt_type)
{
	$("#ignore"+id).html('<div><img src ="<?php echo base_url();?>application/libraries/assets/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
	$("#ignore"+id).load("<?php echo base_url();?>index.php/admin/view_reassign/"+id+"/"+assign_id+"/"+tickt_type);
}
</script>