<?php 
$new_ticket=0;
$complete_ticket=0;
$pending_ticket=0;
foreach($new->result() as $row)
{
	$new_ticket=$row->new_tic;
}
foreach($complete->result() as $row1)
{
	$complete_ticket=$row1->complete;
}
foreach($pending->result() as $row3)
{
	$pending_ticket=$row3->pending;
}
?>
<?php
$task_today=0;
$task_coming=0;
$task_completed=0;
foreach($task_info->result() as $row2)
{
	$date=date_create($row2->task_reminder);
	$d=date_format($date,'Y-m-d');
	$date1=date_create($curr);
	$today=date_format($date1,'Y-m-d');
	if($d < $today)
	{
		$task_completed=$task_completed+1;
	}
	if($d > $today)
	{
		$task_coming=$task_coming+1;
	}
	if($d == $today)
	{
		$task_today=$task_today+1;
	}
}
?>
<?php
$event_call=0;
$event_email=0;
$event_meeting=0;
foreach($event->result() as $row5)
{
	if($row5->event_type=='2')
	{
		$event_call=$event_call+1;
	}
	if($row5->event_type=='3')
	{
		$event_email=$event_email+1;
	}
	if($row5->event_type=='4')
	{
		$event_meeting=$event_meeting+1;
	}
}
?>
    <!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Dashboard <small>statistics and more</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="#">
								Home
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Dashboard
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN DASHBOARD STATS -->
			<div class="row">
				<div class="col-lg-2">
					<div class="dashboard-stat green">
						<div class="visual">
							<i class="fa fa-ticket"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php echo $new_ticket; ?>
							</div>
							<div class="desc">
								 New
							</div>
						</div>
						
					</div>
				</div>
				<div class="col-lg-2">
					<div class="dashboard-stat green">
						<div class="visual">
							<i class="fa fa-ticket"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php echo $complete_ticket; ?>
							</div>
							<div class="desc">
								 Completed
							</div>
						</div>
						
					</div>
				</div>
				<div class="col-lg-2">
					<div class="dashboard-stat green">
						<div class="visual">
							<i class="fa fa-ticket"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php echo $pending_ticket; ?>
							</div>
							<div class="desc">
								 Pending
							</div>
						</div>
						
					</div>
				</div>
				<div class="col-lg-2">
					<div class="dashboard-stat yellow">
						<div class="visual">
							<i class="fa fa-envelope"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php echo $event_email; ?>
							</div>
							<div class="desc">
								 Mail
							</div>
						</div>
						
					</div>
				</div>
                <div class="col-lg-2">
					<div class="dashboard-stat yellow">
						<div class="visual">
							<i class="fa fa-phone-square"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php echo $event_call; ?>
							</div>
							<div class="desc">
								 Call
							</div>
						</div>
						
					</div>
				</div>
                <div class="col-lg-2">
					<div class="dashboard-stat yellow">
						<div class="visual">
							<i class="fa fa-users"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php echo $event_meeting; ?>
							</div>
							<div class="desc">
								 Meeting
							</div>
						</div>
						
					</div>
				</div>
                
                
                
			</div>
			<!-- END DASHBOARD STATS -->
            
            <!-- START DASHBOARD STATS -->
            <div class="row">
            <div class="col-lg-2">
					<div class="dashboard-stat purple">
						<div class="visual">
							<i class="fa fa-tasks"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php echo $task_today; ?>
							</div>
							<div class="desc">
								 Today
							</div>
						</div>
						
					</div>
				</div>
                <div class="col-lg-2">
					<div class="dashboard-stat purple">
						<div class="visual">
							<i class="fa fa-tasks"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php echo $task_coming; ?>
							</div>
							<div class="desc">
								 Coming Soon
							</div>
						</div>
						
					</div>
				</div>
				<div class="col-lg-2">
					<div class="dashboard-stat purple">
						<div class="visual">
							<i class="fa fa-tasks"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php echo $task_completed; ?>
							</div>
							<div class="desc">
								 Pending
							</div>
						</div>
						
					</div>
				</div>
				<div class="col-lg-2">
					<div class="dashboard-stat blue">
						<div class="visual">
							<i class="fa fa-tags"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php echo $lead1; ?>
							</div>
							<div class="desc">
								 Leads
							</div>
						</div>
						
					</div>
				</div>
				<div class="col-lg-2">
					<div class="dashboard-stat blue">
						<div class="visual">
							<i class="fa fa-money"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php echo $account1; ?>
							</div>
							<div class="desc">
								 Accounts
							</div>
						</div>
						
					</div>
				</div>
				<div class="col-lg-2">
					<div class="dashboard-stat blue">
						<div class="visual">
							<i class="fa fa-dot-circle-o"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php echo $opp; ?>
							</div>
							<div class="desc">
								 Opportunity
							</div>
						</div>
						
					</div>
				</div>
			</div>
                    
			<!-- END DASHBOARD STATS -->
            <div class="clearfix">
			</div>
			<div class="row ">
            <div class="col-md-6 col-sm-6">
					<div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-bell-o"></i>Announcement
							</div>
                            <div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div id="piechart_3d" style="width: 600px; height: 300px;"></div>
						</div>
					</div>
				</div>
                
                <div class="col-md-6 col-sm-6">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-bell-o"></i>Announcement
							</div>
                            <div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div id="chart_div" style="width: 600px; height: 300px;"></div>
						</div>
					</div>
				</div>
            </div>
            
            
            
            
            
			<div class="clearfix">
			</div>
			<div class="row ">
				<div class="col-md-6 col-sm-6">
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-bell-o"></i>Announcement
							</div>
                            <div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
								<ul class="feeds">
                                <?php
								foreach($anounc->result() as $row1)
								{
									?>
									<li>
										<div class="col1">
											<div class="cont">
												<div class="cont-col1">
													<div class="label label-sm label-info">
														<i class="fa fa-user"></i>
													</div>
												</div>
												<div class="cont-col2">
													<div class="desc">
														 <?php echo $row1->txttitle; ?>
													</div>
												</div>
											</div>
										</div>
									</li>
                                <?php 
								}
								?>
                                </ul>
							</div>
							<div class="scroller-footer">
								<div class="pull-right">
									<a href="<?php echo base_url(); ?>index.php/admin/view_announcements">
										 See All Announcement <i class="m-icon-swapright m-icon-gray"></i>
									</a>
									 &nbsp;
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="portlet box yellow tasks-widget">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-check"></i>Tasks
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="task-content">
								<div class="scroller" style="height: 305px;" data-always-visible="1" data-rail-visible1="1">
									<!-- START TASK LIST -->
									<ul class="feeds">
                                    <?php
									foreach($task_info->result() as $row2)
									{
										$date=date_create($row2->task_reminder);
										$d=date_format($date,'Y-m-d');
										$date1=date_create($curr);
										$today=date_format($date1,'Y-m-d');
										if($d > $today)
										{	
										?>
                                    <li>
										<div class="col1">
											<div class="cont">
												<div class="cont-col1">
													<div class="label label-sm label-info">
														<i class="fa fa-tasks"></i>
													</div>
												</div>
												<div class="cont-col2">
													<div class="desc">
														 
                                                         <?php 
																echo $row2->task_comment;
																echo "<button type='button' class='btn btn-xs green'>Task Coming Soon</button>";
														?>
                                                      </div>
												</div>
											</div>
										</div>
                                        <div class="col2">
														<div class="date">
															 <?php echo substr($row2->task_reminder,0,10)?>
														</div>
													</div>
										</li>
                                        <?php
										}
										if($d == $today)
										{
											?>
											<li>
										<div class="col1">
											<div class="cont">
												<div class="cont-col1">
													<div class="label label-sm label-info">
														<i class="fa fa-tasks"></i>
													</div>
												</div>
												<div class="cont-col2">
													<div class="desc">
														 
                                                         <?php 
																echo $row2->task_comment;
																echo "<button type='button' class='btn btn-xs purple'>Do your Task Today</button>";
														?>
                                                      </div>
												</div>
											</div>
										</div>
                                        <div class="col2">
														<div class="date">
															 <?php echo substr($row2->task_reminder,0,10)?>
														</div>
													</div>
										</li>
										<?php
                                          }					
										}
										?>
									</ul>
									<!-- END START TASK LIST -->
								</div>
							</div>
							<div class="task-footer">
								<span class="pull-right">
									<a href="<?php echo base_url() ?>index.php/crm/view_task">
										 See All Tasks <i class="m-icon-swapright m-icon-gray"></i>
									</a>
									 &nbsp;
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix">
			</div>
			
			<div class="clearfix">
			</div>
			<div class="row ">
				
				<div class="col-md-6 col-sm-6">
					<!-- BEGIN PORTLET-->
					<div class="portlet box yellow">
						<div class="portlet-title line">
							<div class="caption">
								<i class="fa fa-bell-o"></i>Feeds
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<!--BEGIN TABS-->
							<div class="tabbable tabbable-custom">
								<ul class="nav nav-tabs">
									<li class="active">
										<a href="#tab_1_1" data-toggle="tab">
											 Lead
										</a>
									</li>
									<li>
										<a href="#tab_1_2" data-toggle="tab">
											 Accounts
										</a>
									</li>
									<li>
										<a href="#tab_1_3" data-toggle="tab">
											 Contacts
										</a>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="tab_1_1">
										<div class="scroller" style="height: 425px;" data-always-visible="1" data-rail-visible1="1">
											<div class="row">
                                            <?php
											foreach($lead->result() as $row3)
											{
											?>
												<div class="col-md-6 user-info">
													<img alt="" src="<?php echo base_url(); ?>application/libraries/assets/img/avatar.png" class="img-responsive"/>
													<div class="details">
														<div>
															<a href="#">
																 <?php echo $row3->lead_company; ?>
															</a>
															<span class="label label-sm label-info">
																 <?php echo $row3->lead_name; ?>
															</span>
														</div>
														<div>
															 <?php echo $row3->lead_email; ?>
														</div>
													</div>
												</div>
                                                <?php
												}
												?>
											</div>
                                        </div>
									</div>
									<div class="tab-pane" id="tab_1_2">
										<div class="scroller" style="height: 425px;" data-always-visible="1" data-rail-visible1="1">
											<div class="row">
                                             <?php
											foreach($account->result() as $row4)
											{
											?>
												<div class="col-md-6 user-info">
													<img alt="" src="<?php echo base_url();?>application/libraries/assets/img/avatar.png" class="img-responsive"/>
													<div class="details">
														<div>
															<a href="#">
																 <?php echo $row4->account_name; ?>
															</a>
															<span class="label label-sm label-success label-mini">
																 <?php echo $row4->account_phone; ?>
															</span>
														</div>
														<div>
															 <?php echo $row4->account_email; ?>
														</div>
													</div>
												</div>
                                                <?php
												}
												?>
												
											</div>
										</div>
									</div>
									<div class="tab-pane" id="tab_1_3">
										<div class="scroller" style="height: 425px;" data-always-visible="1" data-rail-visible1="1">
											<div class="row">
                                            <?php
											foreach($contact->result() as $row5)
											{
											?>
												<div class="col-md-6 user-info">
													<img alt="" src="<?php echo base_url(); ?>application/libraries/assets/img/avatar.png" class="img-responsive"/>
													<div class="details">
														<div>
															<a href="#">
																 <?php echo $row5->contact_name; ?>
															</a>
															<span class="label label-sm label-danger">
																 <?php echo $row5->contact_mobile; ?>
															</span>
														</div>
														<div>
															  	 <?php echo $row5->contact_email; ?>
														</div>
													</div>
												</div>
                                                <?php
												}
												?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--END TABS-->
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
                
                <div class="col-md-6 col-sm-6">
					<!-- BEGIN PORTLET-->
					<div class="portlet box blue calendar">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-calendar"></i>Calendar
							</div>
						</div>
						<div class="portlet-body light-grey">
							<div id="calendar">
							</div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
			</div>
			
			
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->



<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);

        var options = {
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>
    
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Lead', 'Oppurtunity', 'Account'],
		  
          ['<?php echo Date('Y') ?>',  <?php echo $lead1; ?>,    <?php echo $opp; ?>,   <?php echo $account1; ?>]
        ]);

        var options = {
          title: 'Company Performance',
          hAxis: {title: 'Year',  titleTextStyle: {color: 'red'}}
        };

        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));

        chart.draw(data, options);
      }
    </script>
    
   <script>
   var Index = function () {

    return {
		
		//main function
        init: function () {
            App.addResponsiveHandler(function () {
                jQuery('.vmaps').each(function () {
                    var map = jQuery(this);
                    map.width(map.parent().width());
                });
            });
        },

           initCalendar: function () {
            if (!jQuery().fullCalendar) {
                return;
            }

            var date = new Date();
            

            var h = {};

            if ($('#calendar').width() <= 400) {
                $('#calendar').addClass("mobile");
                h = {
                    left: 'title, prev, next',
                    center: '',
                    right: 'today,month,agendaWeek,agendaDay'
                };
            } else {
                $('#calendar').removeClass("mobile");
                if (App.isRTL()) {
                    h = {
                        right: 'title',
                        center: '',
                        left: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                } else {
                    h = {
                        left: 'title',
                        center: '',
                        right: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                }               
            }

            $('#calendar').fullCalendar('destroy'); // destroy the calendar
            $('#calendar').fullCalendar({ //re-initialize the calendar
                disableDragging: false,
                header: h,
                editable: false,
                events: [
					<?php 
					foreach($task->result() as $row)
					{
					?>
					{
                        title: 'Time - <?php echo substr($row->task_reminder,11,8); echo '\n'; ?>Task - <?php echo $row->subject; ?>',                        
                        start: new Date(<?php echo substr($row->task_reminder,0,4); ?>, <?php echo substr($row->task_reminder,5,2)-1; ?>, <?php echo substr($row->task_reminder,8,2); ?>),
                        backgroundColor: App.getLayoutColorCode('purple'),
                    },
					<?php
					}
					?>
					
					
					
					
					
					<?php 
					foreach($event->result() as $row1)
					{
					?>
					{
                        title: 'Time - <?php echo substr($row1->event_start_date,11,8); echo '\n'; ?>Event - <?php echo $row1->subject; ?>',                        
                        start: new Date(<?php echo substr($row1->event_start_date,0,4); ?>, <?php echo substr($row1->event_start_date,5,2)-1; ?>, <?php echo substr($row1->event_start_date,8,2); ?>),
						end: new Date(<?php echo substr($row1->event_end_date,0,4); ?>, <?php echo substr($row1->event_end_date,5,2)-1; ?>, <?php echo substr($row1->event_end_date,8,2); ?>),
                        backgroundColor: App.getLayoutColorCode('green'),
					},
                    <?php
					}
					?>
					
					
					
					
                ]
            });
        },
		
		

        initDashboardDaterange: function () {
        }
		
		};
		

}();

   </script>
    
