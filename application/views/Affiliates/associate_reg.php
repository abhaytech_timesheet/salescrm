<script>
function fill_city()
{

var state_id=$("#txtstate").val();
$("#city").html('<div><img src ="<?php echo base_url();?>application/lib_images/loading.gif" alt="Loading....." title="Loading...."></div>');
$("#city").load("<?php echo base_url();?>index.php/master/select_city/"+state_id);
}
</script>
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Registration
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="btn-group">
							<button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							<span>
								Actions
							</span>
							<i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="#">
										Download As PDF
									</a>
								</li>
								<li>
									<a href="#">
										Download As Excel
									</a>
								</li>
								
							</ul>
						</li>
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">
								Registration
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Associate Registration
							</a>
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom boxless tabbable-reversed">
						<div class="tab-content">
							  <div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Associate Registration
										</div>
										<div class="tools">
											<a href="javascript:;" class="collapse">
											</a>
											<a href="#portlet-config" data-toggle="modal" class="config">
											</a>
											<a href="javascript:;" class="reload">
											</a>
											<a href="javascript:;" class="remove">
											</a>
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<form action="#" class="form-horizontal">
											<div class="form-body">
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Application Charge</label>
															<div class="col-md-9">
																<input type="text" class="form-control input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Branch Name</label>
															<div class="col-md-9">
																<select class="form-control input-medium">
																	<option value="">Select..</option>
                                                                    <option value="">Branch 1</option>
																	<option value="">Branch 2</option>
																</select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Branch Address</label>
															<div class="col-md-9">
																<textarea class="form-control input-medium" value="1" readonly disabled="disabled"></textarea>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                <div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Associate Name </label>
															<div class="col-md-9">
																<input type="text" class="form-control input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Introducer Associate Id</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                <div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Introducer Associate Name</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium" disabled="disabled">
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Introducer Rank</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium" disabled="disabled">
															</div>
														</div>
													</div>
												</div>
                                                <h3>Personal Details</h3>
                                                <div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3"> Applicant Rank</label>
															<div class="col-md-9">
																<select class="form-control input-medium">
                                                                    <option value="-1">--Select--</option>
                                                                </select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3"> Father/Husband's Name</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3"> Applicant Rank</label>
															<div class="col-md-9">
																<select class="form-control input-medium">
                                                                    <option value="-1">--Select--</option>
                                                                </select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3"> Father/Husband's Name</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Date of Birth </label>
															<div class="col-md-9">
																<input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value=""/>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Age</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Joining Date </label>
															<div class="col-md-9">
																<input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value=""/>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Address</label>
															<div class="col-md-9">
																<textarea class="form-control input-medium" value="1"></textarea>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
                                                <div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">State</label>
															<div class="col-md-9">
                                                           		<select id="txtstate" name="txtstate" class="form-control  input-medium" onChange="fill_city()">
                                                                    <option selected="selected" value="-1">Select State</option>
                                                                    <?php foreach($state->result() as $row)
                                                                    {
                                                                    ?><option value="<?php echo $row->m_loc_id;?>"><?php echo $row->m_loc_name;?></option>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </select>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">City</label>
															<div class="col-md-9">
                                                             <div id="city">
																	<select id="txtcity" name="txtcity" class="form-control  input-medium">
                                                                        <option selected="selected" value="-1">Select City</option>
                                                                    </select>
                                                              </div>
															</div>
														</div>
													</div>
													
													<!--/span-->
												</div>
												<!--/row-->
                                                <!--/row-->
												<div class="row">
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Pin/PO Box Number </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Pan No</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                <h3>Contact Details </h3>

                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Mobile </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Email </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                <h3>Nominee Details</h3>
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Nominee Name </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Relation </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Nominee Age </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													
												</div>
												<!--/row-->
                                                <h3>Bank Details</h3>

                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Bank Name </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Branch Name</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Account Number</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">IFSCode</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                               <div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Pancard Number</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													
													<!--/span-->
												</div>
											</div>
											<div class="form-actions fluid">
												<div class="row">
													<div class="col-md-6">
														<div class="col-md-offset-3 col-md-9">
															<button type="submit" class="btn blue">Submit</button>
															<button type="reset" class="btn default">Cancel</button>
														</div>
													</div>
													<div class="col-md-6">
													</div>
												</div>
											</div>
										</form>
										<!-- END FORM-->
									</div>
								</div>
						  <div class="tab-pane" id="tab_4">
							  <div class="portlet box blue"></div>
							</div>
						  <div class="tab-pane" id="tab_6">
							  <div class="portlet box blue "></div>
							</div>
							<div class="tab-pane" id="tab_7">
								<div class="portlet box blue ">
									<div class="portlet-title">
										<div class="caption">
										<i class="fa fa-reorder"></i></div>
										<div class="tools">
											<a href="javascript:;" class="collapse">
											</a>
											<a href="#portlet-config" data-toggle="modal" class="config">
											</a>
											<a href="javascript:;" class="reload">
											</a>
											<a href="javascript:;" class="remove">
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<script>
        jQuery(document).ready(function() {       
           // initiate layout and plugins
           App.init();
           ComponentsPickers.init();
        });   
    </script>