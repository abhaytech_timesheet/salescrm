<script>
function fill_city()
{

var state_id=$("#txtstate").val();
$("#city").html('<div><img src ="<?php echo base_url();?>application/lib_images/loading.gif" alt="Loading....." title="Loading...."></div>');
$("#city").load("<?php echo base_url();?>index.php/master/select_city/"+state_id);
}
</script>
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Registration
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="btn-group">
							<button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							<span>
								Actions
							</span>
							<i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="#">
										Download As PDF
									</a>
								</li>
								<li>
									<a href="#">
										Download As Excel
									</a>
								</li>
							</ul>
						</li>
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">
								Registration
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Customer Registration
							</a>
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom boxless tabbable-reversed">
						<div class="tab-content">
							  <div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Customer Registration
										</div>
										<div class="tools">
											<a href="javascript:;" class="collapse">
											</a>
											<a href="#portlet-config" data-toggle="modal" class="config">
											</a>
											<a href="javascript:;" class="reload">
											</a>
											<a href="javascript:;" class="remove">
											</a>
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<form action="#" class="form-horizontal">
											<div class="form-body">
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Applicant Form Number</label>
															<div class="col-md-9">
																<input type="text" class="form-control input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Branch Code For Payment</label>
															<div class="col-md-9">
																<select class="form-control  input-medium input-medium">
																	<option value="">Select..</option>
                                                                    <option value="">Branch 1</option>
																	<option value="">Branch 2</option>
																</select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">CSS No.</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium input-medium" value="1" readonly disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                <h3> Applicant Details </h3>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Date </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Applicant Name</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Father/Huband's Name</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Gender </label>
															<div class="col-md-9">
																<select class="form-control  input-medium">
																	<option value="">Select..</option>
                                                                    <option value="">Male</option>
																	<option value="">Female</option>
																</select>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Permanent Address</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">State</label>
															<div class="col-md-9">
																<select id="txtstate" name="txtstate" class="form-control  input-medium" onChange="fill_city()">
                                                                    <option selected="selected" value="-1">Select State</option>
                                                                    <?php foreach($state->result() as $row)
                                                                    {
                                                                    ?><option value="<?php echo $row->m_loc_id;?>"><?php echo $row->m_loc_name;?></option>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </select>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">District </label>
															<div class="col-md-9">
																<div id="city">
																	<select id="txtcity" name="txtcity" class="form-control  input-medium">
                                                                        <option selected="selected" value="-1">Select City</option>
                                                                    </select>
                                                              </div>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Pin Code</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Address Proof </label>
															<div class="col-md-9">
																<select class="form-control  input-medium">
                                                                    <option value="--Select--">--Select--</option>
                                                                    <option value="Pan Card">Pan Card</option>
                                                                    <option value="Passport">Passport</option>
                                                                    <option value="Driving Licence">Driving Licence</option>
                                                                    <option value="Ration Card">Ration Card</option>
                                                                    <option value="Voter Id">Voter Id</option>
                                                                    <option value="Domicile Certificate">Domicile Certificate</option>
                                                                    <option value="Other">Other</option>
                                                            
                                                                </select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Mobile Number</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Date of Birth </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Age </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                <h3>Nominee Details</h3>
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Nominee Name </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Permanent Address</label>
															<div class="col-md-9">
																<textarea class="form-control  input-medium"></textarea>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Gender </label>
															<div class="col-md-9">
																<select class="form-control  input-medium">
																	<option value="">Select..</option>
                                                                    <option value="">Male</option>
																	<option value="">Female</option>
																</select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">State </label>
															<div class="col-md-9">
																<select id="txtstate" name="txtstate" class="form-control  input-medium" onChange="fill_city()">
                                                                    <option selected="selected" value="-1">Select State</option>
                                                                    <?php foreach($state->result() as $row)
                                                                    {
                                                                    ?><option value="<?php echo $row->m_loc_id;?>"><?php echo $row->m_loc_name;?></option>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </select>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">District </label>
															<div class="col-md-9">
																<div id="city">
																	<select id="txtcity" name="txtcity" class="form-control  input-medium">
                                                                        <option selected="selected" value="-1">Select City</option>
                                                                    </select>
                                                              </div>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Age </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Relationship </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">NomineeFather/Husband's Name</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                <h3>Plan Details</h3>
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Plan No </label>
															<div class="col-md-9">
																<select class="form-control  input-medium">
                                                                    <option selected="selected" value="Select">Select</option>
                                                                    <option value="PLAN(F-1)">PLAN(F-1)</option>
                                                                    <option value="PLAN(F-2)">PLAN(F-2)</option>
                                                                    <option value="PLAN(F-3)">PLAN(F-3)</option>
                                                                    <option value="PLAN(F-4)">PLAN(F-4)</option>
                                                                    <option value="PLAN(F-5)">PLAN(F-5)</option>
                                                                    <option value="PLAN(F-6)">PLAN(F-6)</option>
                                                                    <option value="PLAN(F-7)">PLAN(F-7)</option>
                                                                    <option value="PLAN(F-8)">PLAN(F-8)</option>
                                                                    <option value="PLAN(F-9)">PLAN(F-9)</option>
                                                                    <option value="PLAN(M-1)">PLAN(M-1)</option>
                                                                    <option value="PLAN(M-2)">PLAN(M-2)</option>
                                                                    <option value="PLAN(M-3)">PLAN(M-3)</option>
                                                                    <option value="PLAN(M-4)">PLAN(M-4)</option>
                                                                    <option value="PLAN(R-1)">PLAN(R-1)</option>
                                                                    <option value="PLAN(R-10)">PLAN(R-10)</option>
                                                                    <option value="PLAN(R-2)">PLAN(R-2)</option>
                                                                    <option value="PLAN(R-3)">PLAN(R-3)</option>
                                                                    <option value="PLAN(R-4)">PLAN(R-4)</option>
                                                                    <option value="PLAN(R-5)">PLAN(R-5)</option>
                                                                    <option value="PLAN(R-6)">PLAN(R-6)</option>
                                                                    <option value="PLAN(R-7)">PLAN(R-7)</option>
                                                                    <option value="PLAN(R-8)">PLAN(R-8)</option>
                                                                    <option value="PLAN(R-9)">PLAN(R-9)</option>
                                                            
                                                                </select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Plan Name</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Term of Plan (months)</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Land Unit </label>
															<div class="col-md-9">
																<select class="form-control  input-medium">
																	<option>Select..</option>
																</select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Total Consideration Value </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Redemption Realizable Value </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium" disabled="disabled">
															</div>
														</div>
													</div>
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Mode </label>
															<div class="col-md-9">
																<select class="form-control  input-medium" disabled="disabled">
																	<option>Select..</option>
																	<option>Country 2</option>
																</select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Payment(1st Installment) </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Amount in Words  </label>
															<div class="col-md-9">
																<textarea class="form-control  input-medium" disabled="disabled"></textarea>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Installment Due Date </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Date of Last Installment</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Date Of Redemption Value </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
													
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Application Charges </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													
												</div>
												<!--/row-->
                                                <h3>Payment Details</h3>
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Mode of Deposit </label>
															<div class="col-md-9">
																<select class="form-control  input-medium">
                                                                    <option selected="selected" value="Cash">Cash</option>
                                                                    <option value="Cheque">Cheque</option>
                                                                    <option value="DD">DD</option>
                                                            
                                                                </select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Payment Date </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Amount </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Amount in Words</label>
															<div class="col-md-9">
																<textarea class="form-control  input-medium" disabled="disabled"></textarea>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                <h3>Agent Details</h3>
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Agency Code </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Agency  Name </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Branch Name </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Branch Code</label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                <h3>Branch Details</h3>
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Branch Name </label>
															<div class="col-md-9">
																<select class="form-control  input-medium">
                                                                    <option selected="selected" value="Select">Select</option>
                                                                    <option value="K1-0512">Branch1</option>
                                                                    <option value="L1-0522">Branch2</option>
                                                            
                                                                </select>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Branch Address </label>
															<div class="col-md-9">
																<textarea class="form-control  input-medium" disabled="disabled"></textarea>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
                                                
                                                <!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Spot Income </label>
															<div class="col-md-9">
																<input type="text" class="form-control  input-medium" disabled="disabled">
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Membership</label>
															<div class="radio-list">
																<label class="radio-inline">
																<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked> Paid </label>
																<label class="radio-inline">
																<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2"> Due </label>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
											</div>
											<div class="form-actions fluid">
												<div class="row">
													<div class="col-md-6">
														<div class="col-md-offset-3 col-md-9">
															<button type="submit" class="btn blue">Submit</button>
															<button type="reset" class="btn default">Cancel</button>
														</div>
													</div>
													<div class="col-md-6">
													</div>
												</div>
											</div>
										</form>
										<!-- END FORM-->
									</div>
								</div>
						  <div class="tab-pane" id="tab_4">
							  <div class="portlet box blue"></div>
							</div>
						  <div class="tab-pane" id="tab_6">
							  <div class="portlet box blue "></div>
							</div>
							<div class="tab-pane" id="tab_7">
								<div class="portlet box blue ">
									<div class="portlet-title">
										<div class="caption">
										<i class="fa fa-reorder"></i></div>
										<div class="tools">
											<a href="javascript:;" class="collapse">
											</a>
											<a href="#portlet-config" data-toggle="modal" class="config">
											</a>
											<a href="javascript:;" class="reload">
											</a>
											<a href="javascript:;" class="remove">
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
