<?php 
	foreach($acc->result() as $row)
	{
		break;
	}
?>
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="#.">View Account Profile</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Affiliates</span>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View Account Profile</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row profile">
				<div class="col-md-12">
					<!--BEGIN TABS-->
					<div class="tabbable tabbable-custom tabbable-full-width">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_1_1" data-toggle="tab">
									Overview
								</a>
							</li>
							<li>
								<a href="#tab_1_3" data-toggle="tab">
									Account
								</a>
							</li>
							<li>
								<a href="#tab_1_4" data-toggle="tab">
									Projects
								</a>
							</li>
							
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1_1">
								<div class="row">
									<div class="col-md-3">
										<ul class="list-unstyled profile-nav">
											<li>
												<?php if($row->account_image!="")
													{
													?>
													<img src="<?php echo base_url() ?>application/uploadimage/<?php echo $row->account_image; ?>" class="img-responsive" alt=""/>
													<?php 
													} 
													else
													{
													?>
													<img src="<?php echo base_url() ?>application/uploadimage/default_avatar_large.png" class="img-responsive" alt=""/>
												<?php } ?>
												
											</li>
											
										</ul>
									</div>
									<div class="col-md-9">
										<div class="row">
											<div class="col-md-8 profile-info">
												<h1><?php echo $row->account_name; ?></h1>
												<p>
													<?php echo $row->account_desc; ?>
												</p>
												<p>
													<a href="#">
														<?php echo $row->account_email; ?>
													</a>
												</p>
												<ul class="list-inline">
													<li>
														<i class="fa fa-phone"></i> <?php echo $row->account_phone; ?>
													</li>
													<li>
														<i class="fa fa-map-marker"></i>  
														<?php 
															foreach($loc->result() as $l)
															{
																if($row->account_city==$l->m_loc_id)
																{
																	echo $l->m_loc_name;
																}
															}
														echo '&nbsp; , '.$row->account_address; ?>
													</li>
													<li>
														<i class="fa fa-calendar"></i> <?php echo $row->account_regdate; ?>
													</li>
													
													
													
													<!--
														<li>
														<i class="fa fa-star"></i> Top Seller
														</li>
														<li>
														<i class="fa fa-heart"></i> BASE Jumping
														</li>
													-->
												</ul>
											</div>
											<!--end col-md-8-->
											<div class="col-md-4">
												<div class="portlet sale-summary">
													<div class="portlet-title">
														<div class="caption">
															Sales Summary
														</div>
														<div class="tools">
															<a class="reload" href="javascript:;">
															</a>
														</div>
													</div>
													<div class="portlet-body">
														<ul class="list-unstyled">
															<li>
																<span class="sale-info">
																	TODAY SOLD <i class="fa fa-img-up"></i>
																</span>
																<span class="sale-num">
																	23
																</span>
															</li>
															<li>
																<span class="sale-info">
																	WEEKLY SALES <i class="fa fa-img-down"></i>
																</span>
																<span class="sale-num">
																	87
																</span>
															</li>
															<li>
																<span class="sale-info">
																	TOTAL SOLD
																</span>
																<span class="sale-num">
																	2377
																</span>
															</li>
															<li>
																<span class="sale-info">
																	EARNS
																</span>
																<span class="sale-num">
																	$37.990
																</span>
															</li>
														</ul>
													</div>
												</div>
											</div>
											<!--end col-md-4-->
										</div>
										<!--end row-->
										<div class="tabbable tabbable-custom tabbable-custom-profile">
											<ul class="nav nav-tabs">
												<li class="active">
													<a href="#tab_1_11" data-toggle="tab">
														Generated Ticket
													</a>
												</li>
												<li>
													<a href="#tab_1_22" data-toggle="tab">
														Announcements
													</a>
												</li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane active" id="tab_1_11">
													<div class="portlet-body">
														<table class="table table-striped table-bordered table-advance table-hover">
															<thead>
																<tr>
																	<th>
																		<i class="fa fa-ticket"></i> Ticket No
																	</th>
																	<th>
																		Subject
																	</th>
																	
																	<th>
																		Action
																	</th>
																</tr>
															</thead>
															<tbody>
																<?php
																	foreach($ticket->result() as $tic)
																	{
																		if($tic->txtemail==$row->account_email)
																		{
																		?>
																		<tr>
																			<td>
																				<a href="#">
																					<?php
																						echo '#'.$tic->ticket_no;
																					?>
																				</a>
																			</td>
																			<td>
																				<a href="#">
																					<?php
																						echo $tic->txtsubject;
																					?>
																				</a>
																			</td>
																			<td>
																				<a class="btn default btn-xs blue-stripe" href="<?php echo base_url();?>index.php/admin/view_ticket/<?php echo $tic->ticket_no ?>">
																					View
																				</a>
																			</td>
																		</tr>
																		<?php
																		}
																	}
																?>
															</tbody>
														</table>
													</div>
												</div>
												<!--tab-pane-->
												<div class="tab-pane" id="tab_1_22">
													<div class="tab-pane active" id="tab_1_1_1">
														<div class="scroller" data-height="200px" data-always-visible="1" data-rail-visible1="1">
															<ul class="feeds">
																<?php  
																	foreach($anonce->result() as $row12)
																	{	
																	?>
																	<li>
																		<div class="col1">
																			<div class="cont">
																				<div class="cont-col1">
																					<div class="label label-success">
																						<i class="fa fa-bullhorn"></i>
																					</div>
																				</div>
																				<div class="cont-col2">
																					<div class="desc">
																						<?php echo $row12->txttitle; ?>
																					</div>
																				</div>
																			</div>
																		</div>
																	</li>
																	<?php
																	}
																?>
															</ul>
														</div>
													</div>
												</div>
												<!--tab-pane-->
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--tab_1_2-->
							<div class="tab-pane" id="tab_1_3">
								<div class="row profile-account">
									<div class="col-md-3">
										<ul class="ver-inline-menu tabbable margin-bottom-10">
											
											<li class="active">
												<a data-toggle="tab" href="#tab_2-2">
													<i class="fa fa-picture-o"></i> Change Account Image
												</a>
											</li>
											
										</ul>
									</div>
									<div class="col-md-9">
										<div class="tab-content">
											
											<div id="tab_2-2" class="tab-pane active">
												<h4>
													Add a Profile Picture
												</h4>
												<form role="form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/affiliates/change_account_image/" method="post">
													<div class="form-group">
														<div class="fileinput fileinput-new" data-provides="fileinput">
															<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
																<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
															</div>
															<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
															</div>
															<div>
																<span class="btn default btn-file">
																	<span class="fileinput-new">
																		Select image
																	</span>
																	<span class="fileinput-exists">
																		Change
																	</span>
																	<input type="file" name="userfile" id="userfile">
																</span>
																<a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
																	Remove
																</a>
															</div>
														</div>
														
													</div>
													<div class="margin-top-10">
														<input type="submit" name="submit" value="Submit" class="btn green" />
														<a href="#" class="btn default">
															Cancel
														</a>
													</div>
												</form>
											</div>
											
											
										</div>
									</div>
									<!--end col-md-9-->
								</div>
							</div>
							<!--end tab-pane-->
							<div class="tab-pane" id="tab_1_4">
								<div class="row">
									<div class="col-md-12">
										<div class="add-portfolio">
											<span>
												View All Projects Here
											</span>
										</div>
									</div>
								</div>
								<!--end add-portfolio-->
								<?php 
									foreach($pro->result() as $row4)
									{ ?>
									<div class="row portfolio-block">
										<div class="col-md-3 portfolio-text">
											<div class="portfolio-text-info">
												<h4><?php echo $row4->m_project_name; ?></h4>
												<p>
													<?php echo substr($row4->m_project_name,0,50); ?>
												</p>
											</div>
										</div>
										<div class="col-md-7 portfolio-stat">
											<div class="portfolio-info">
												Project Id
												<span>
													<?php echo $row4->m_project_sno; ?>
												</span>
											</div>
											
											<div class="portfolio-info">
												Cost
												<span>
													<?php echo $row4->m_actual_price; ?> <del>&#2352;</del>
												</span>
											</div>
										</div>
										<div class="col-md-2 portfolio-btn">
											<a href="#" class="btn bigicn-only">
												<span>
													Manage
												</span>
											</a>
										</div>
									</div>
									
								<?php } ?>
								<!--end row-->
							</div>
							<!--end tab-pane-->
						</div>
						<!--END TABS-->
					</div>
				</div>					
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END CONTENT -->
			
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													
