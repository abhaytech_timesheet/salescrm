<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="#">Action</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Change User Credentials</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Change User Credentials</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Change User Credentials</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" id="insert_data" action="javascript: get_mem('<?php echo $red_page;?>')" method="post">
								<div class="form-body" id="city">
									<div class="form-group">
										<label class="col-md-4 control-label">User ID</label>
										<div class="col-md-6">
											<input type="text" id="txtid" name="txtid" class="form-control empty" placeholder="Enter User ID" value="<?php echo $record->or_m_user_id;?>">
											<span id="divtxtid" style="color:red;"></span>
										</div>
									</div>
									
									
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-4 col-md-8">
										<button type="submit" class="btn green" onclick="return check('insert_data')">Search</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			
				<div class="col-md-6">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-settings font-dark"></i>
								<span class="caption-subject bold uppercase">View & Modify</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" id="change_pass" action="javascript: update_pass()" method="post">
								<div class="form-body" id="change_password">
									<div class="form-group">
										<label class="col-md-4 control-label">User ID</label>
										<div class="col-md-6">
											<input type="text" id="txtid1" name="txtid" class="form-control" value="<?php echo $record->or_m_user_id;?>" readonly>
											<span id="divtxtid1" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">User Name</label>
										<div class="col-md-6">
											<input type="text" id="txtname" name="txtname" class="form-control" value="<?php echo $record->or_m_name;?>" disabled>
											<span id="divtxtname" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">Old Password</label>
										<div class="col-md-6">
											<input type="text" id="txtoldpass" name="txtoldpass" class="form-control" value="<?php echo $record->or_login_pwd;?>" disabled>
											<span id="divtxtoldpass" style="color:red;"></span>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-md-4 control-label">New Password</label>
										<div class="col-md-6">
											<input type="text" id="txtnewpass" name="txtnewpass" class="form-control empty" placeholder="Enter New Password">
											<span id="divtxtnewpass" style="color:red;"></span>
										</div>
									</div>
									
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-5 col-md-7">
										<button type="submit" class="btn green" onclick="return check('change_pass')">Edit</button>
										<a type="button" class="btn default" href="<?php echo base_url().'index.php/action/change_password'?>">Cancel<a>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
					
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													
<script>
	function update_pass()
	{
		var txtid=$("#txtid1").val();
		var txtnewpass=$("#txtnewpass").val();
		$.ajax({
			type:"POST",
			url:"<?php echo base_url().'index.php/action/update_password/'.base64_encode($record->or_m_reg_id)?>",
			data:{'txtid':txtid,'txtnewpass':txtnewpass},
			success: function(msgs) {
				 if (msgs == true) 
				 {
                    alert("Changed Successfully");
                    window.location = "<?php echo base_url();?>index.php/action/change_password";
				}
			}
			});
		
	}
</script>
<script>
	function get_mem(page)
	{
		var txtid=$("#txtid").val();
		$.ajax(
		{
			type:"POST",
			url:"<?php echo base_url();?>index.php/action/get_member",
			data:{'txtid':txtid,'page':page},
			success: function(msg) {
				if(msg==0)
				{
					$("#txtid").val('');
					alert("This User ID dosen't exist");
				}
				else
				{
					$('#stylized').html(msg);
				}
			}
		});
	}
</script>