<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="#">Action</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Allow Capping</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Allow Capping</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Search a Member</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" id="insert_data" action="<?php echo base_url();?><?php echo $this->uri->segment(1);?>" method="post">
								<div class="form-body" id="city">
									<div class="form-group">
										<label class="col-md-1 control-label">User ID</label>
										<div class="col-md-4">
											<input type="text" id="txtid" name="txtid" class="form-control" placeholder="Enter User ID">
											<span id="divtxtid" style="color:red;"></span>
										</div>
									
										<label class="col-md-2 control-label">Designation</label>
										<div class="col-md-4">
											<select class="form-control" name="dddes" id="dddes">
												<option value="-1">Select User Designation</option>
												<?php 
												foreach($desig->result() as $rdes)
												{?>
													<option value="<?php echo $rdes->m_des_id;?>"><?php echo $rdes->m_des_name;?></option>
												<?php
												}?>
											</select>
											<span id="divdddes" style="color:red;"></span>
										</div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-4 col-md-8">
										<button type="submit" class="btn green" onclick="return check('insert_data')">Search</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-settings font-dark"></i>
								<span class="caption-subject bold uppercase">View & Modify</span>
							</div>
							<div class="tools">
								
								<button class="btn btn-md green" onclick="updatecap()"> Update <i class="fa fa-upload"></i></button>
								
							</div>
						</div>
						<table class="table table-striped table-bordered table-hover table-checkable  dt-responsive order-column" id="sample_2">
							<thead>
								<tr>
									<th>
										Select
									</th>
									<th>SNo.</th>
									<th>User Id</th>
									<th>User Name</th>
									<th>Designation</th>
									<th>Mobile</th>
									<th>Introducer Name</th>
									<th>Location</th>
									<th>Capping Amount</th>
								</tr>
							</thead>
							<tbody id="userid">
								<?php
									$sn=1;
									foreach($record->result() as $rows)
									{?>
									
									<tr>
										<td>
											<input type="checkbox" class="checkboxes" id="<?php echo $rows->or_m_reg_id;?>" onclick="chbchecksin()" >
										</td>
										<td><?php echo $sn++;?></td>
										<td><?php echo $rows->or_m_user_id;?></td>
										<td><?php echo $rows->or_m_name;?></td>
										<td><?php echo $rows->m_des_name;?></td>
										<td><?php echo $rows->or_m_mobile_no;?></td>
										<td><?php echo $rows->or_m_intr_name;?></td>
										<td><?php echo $rows->city.', '.$rows->state;?></td>
										<td>
											<input type="text" value="<?php echo $rows->m_cap_amt;?>" id="txtcap<?php echo $rows->or_m_reg_id;?>" name="txtcap<?php echo $rows->or_m_reg_id;?>" onkeyup="changecap('<?php echo $rows->or_m_reg_id;?>');">
											<input type="hidden"  id="txthidcap<?php echo $rows->or_m_reg_id;?>" name="txthidcap<?php echo $rows->or_m_reg_id;?>" value="<?php echo $rows->m_cap_amt;?>,<?php echo $rows->or_m_reg_id;?>">
										</td>
									</tr>   
								<?php 
								}?>                   
							</tbody>
						</table>
						<input type="hidden" id="txtquid" name="txtquid">
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
					
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->
<script>
	function changecap(id)
	{
		var capid=$("#txtcap"+id+"").val();
		var value=capid+","+id;
		$("#txthidcap"+id+"").val(value);
	}
</script>
<script>
	function updatecap()
	{
		var txtquid=$("#txtquid").val();
		var txtcap="";
		var cap="";
		var status=0;
		var collection=$("#userid");
		var inputs=collection.find("input[type=hidden]");
		var regid=txtquid.split(',');
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			for(var i=0; i<regid.length-1; i++)
			{
				cap=$("#"+id+"").val();
				var hidval=cap.split(',');
				if(hidval[1] == regid[i])
				{
					txtcap=cap+"-"+txtcap;
					status=1;
				}
			}
		}
		if(status==0)
		{
			alert("Please select a checkbox");
		}
		else
		{
			$.ajax(
				{
					type:"POST",
					url:"<?php echo base_url();?>index.php/action/update_cap",
					data:"txtcap="+txtcap,
					success: function(msg)
					{
						alert(msg.trim());
						location.reload();
					}
				});
		}
	}
</script>
<script src="<?php echo base_url() ?>application/libraries/js/checkboxscipt.js"></script>