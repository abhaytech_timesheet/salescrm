<?php 
	$new_ticket=0;
	$complete_ticket=0;
	$pending_ticket=0;
	foreach($new->result() as $row)
	{
		$new_ticket=$row->new_tic;
	}
	foreach($complete->result() as $row1)
	{
		$complete_ticket=$row1->complete;
	}
	foreach($pending->result() as $row3)
	{
		$pending_ticket=$row3->pending;
	}
?>
<?php
	$task_today=0;
	$task_coming=0;
	$task_completed=0;
	foreach($task_info->result() as $row2)
	{
		$date=date_create($row2->task_reminder);
		$d=date_format($date,'Y-m-d');
		$date1=date_create($curr);
		$today=date_format($date1,'Y-m-d');
		if($d < $today)
		{
			$task_completed=$task_completed+1;
		}
		if($d > $today)
		{
			$task_coming=$task_coming+1;
		}
		if($d == $today)
		{
			$task_today=$task_today+1;
		}
	}
?>
<?php
	$event_call=0;
	$event_email=0;
	$event_meeting=0;
	foreach($event->result() as $row5)
	{
		if($row5->event_type=='4')
		{
			$event_meeting=$event_meeting+1;
		}
	}
	foreach($task_mail->result() as $row6)
	{
		$event_email=$row6->t_mail;
	}
	foreach($task_call->result() as $row7)
	{
		$event_call=$row7->t_call;
	}
?>




<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		
		<!-- BEGIN PAGE BAR -->
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/">Report</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/dashboard/">Dashboard</a>
				</li>
			</ul>
			<div class="page-toolbar">
				<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
					<i class="icon-calendar"></i>&nbsp;
					<span class="thin uppercase hidden-xs"></span>&nbsp;
					<i class="fa fa-angle-down"></i>
				</div>
			</div>
		</div>
		<!-- END PAGE BAR -->
		<!-- BEGIN PAGE TITLE-->
		<h3 class="page-title"> Dashboard
			<small>dashboard & statistics</small>
		</h3>
		<!-- END PAGE TITLE-->
		<!-- BEGIN DASHBOARD STATS -->
		<div class="row">
			<div class="col-md-4">
				<div class="dashboard-stat green">
					<div class="visual">
						<i class="fa fa-ticket"></i>
					</div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="<?php echo $new_ticket; ?>">0</span>
						</div>
						<div class="desc"> New </div>
					</div>
					<a class="more" href="<?php echo base_url();?>index.php/admin/index"> View more
						<i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="dashboard-stat green">
					<div class="visual">
						<i class="fa fa-ticket"></i>
					</div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="<?php echo $complete_ticket; ?>">0</span>
						</div>
						<div class="desc"> Completed </div>
					</div>
					<a class="more" href="<?php echo base_url();?>index.php/admin/view_assign_ticket/0"> View more
						<i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="dashboard-stat green">
					<div class="visual">
						<i class="fa fa-ticket"></i>
					</div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="<?php echo $pending_ticket; ?>">0</span>
						</div>
						<div class="desc"> Pending </div>
					</div>
					<a class="more" href="<?php echo base_url();?>index.php/admin/view_assign_ticket/1"> View more
						<i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			
			
		</div>
		<!-- END DASHBOARD STATS -->
		
		<div class="row">
			
			<div class="col-md-4">
				<div class="dashboard-stat blue">
					<div class="visual">
						<i class="fa fa-tags"></i>
					</div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="<?php echo $lead1; ?>">0</span>
						</div>
						<div class="desc"> Leads </div>
					</div>
					<a class="more" href="<?php echo base_url();?>index.php/crm/view_task/0"> View more
						<i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="dashboard-stat blue">
					<div class="visual">
						<i class="fa fa-money"></i>
					</div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="<?php echo $account1; ?>">0</span>
						</div>
						<div class="desc"> Accounts </div>
					</div>
					<a class="more" href="<?php echo base_url();?>index.php/crm/view_account/0"> View more
						<i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="dashboard-stat blue">
					<div class="visual">
						<i class="fa fa-dot-circle-o"></i>
					</div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="<?php echo $account1; ?>">0</span>
						</div>
						<div class="desc"> Opportunity </div>
					</div>
					<a class="more" href="<?php echo base_url();?>index.php/crm/view_opportunity/0"> View more
						<i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			
		</div>
		
		<div class="clearfix"></div>
		<!-- END DASHBOARD STATS 1-->
		
		
		
		
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-cursor font-black"></i>
							<span class="caption-subject font-black bold uppercase">Announcement</span>
						</div>
						<div class="actions">
							<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
								<i class="icon-cloud-upload"></i>
							</a>
							<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
								<i class="icon-wrench"></i>
							</a>
							<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
							<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
								<i class="icon-trash"></i>
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
							<ul class="feeds">
                                <?php
									foreach($anounc->result() as $row1)
									{
									?>
									<li>
										<div class="col1">
											<div class="cont">
												<div class="cont-col1">
													<div class="label label-sm label-info">
														<i class="fa fa-user"></i>
													</div>
												</div>
												<div class="cont-col2">
													<div class="desc">
														<?php echo $row1->txttitle; ?>
													</div>
												</div>
											</div>
										</div>
									</li>
									<?php 
									}
								?>
							</ul>
						</div>
						<div class="scroller-footer">
							<div class="pull-right">
								<a href="<?php echo base_url(); ?>index.php/admin/view_announcements">
									See All Announcement <i class="m-icon-swapright m-icon-gray"></i>
								</a>
								&nbsp;
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="col-md-6 col-sm-6">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-equalizer font-yellow"></i>
							<span class="caption-subject font-yellow bold uppercase">Tasks</span>
						</div>
						<div class="actions">
							<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
								<i class="icon-cloud-upload"></i>
							</a>
							<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
								<i class="icon-wrench"></i>
							</a>
							<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
							<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
								<i class="icon-trash"></i>
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="scroller" style="height: 305px;" data-always-visible="1" data-rail-visible1="1">
							<!-- START TASK LIST -->
							<ul class="feeds">
								<?php
									foreach($task_info->result() as $row2)
									{
										$date=date_create($row2->task_reminder);
										$d=date_format($date,'Y-m-d');
										$date1=date_create($curr);
										$today=date_format($date1,'Y-m-d');
										if($d > $today)
										{	
										?>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-info">
															<i class="fa fa-tasks"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc">
															
															<?php 
																echo $row2->task_comment;
																echo "<button type='button' class='btn btn-xs green'>Task Coming Soon</button>";
															?>
														</div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date">
													<?php echo substr($row2->task_reminder,0,10)?>
												</div>
											</div>
										</li>
										<?php
										}
										if($d == $today)
										{
										?>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-info">
															<i class="fa fa-tasks"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc">
															
															<?php 
																echo $row2->task_comment;
																echo "<button type='button' class='btn btn-xs purple'>Do your Task Today</button>";
															?>
														</div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date">
													<?php echo substr($row2->task_reminder,0,10)?>
												</div>
											</div>
										</li>
										<?php
										}					
									}
								?>
							</ul>
							<!-- END START TASK LIST -->
						</div>
						
						<div class="scroller-footer">
							<span class="pull-right">
								<a href="<?php echo base_url() ?>index.php/crm/view_task">
									See All Tasks <i class="m-icon-swapright m-icon-gray"></i>
								</a>
								&nbsp;
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-cursor font-black"></i>
							<span class="caption-subject font-black bold uppercase">Feeds</span>
						</div>
						<div class="actions">
							<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
								<i class="icon-cloud-upload"></i>
							</a>
							<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
								<i class="icon-wrench"></i>
							</a>
							<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
							<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
								<i class="icon-trash"></i>
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="tabbable tabbable-custom">
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#tab_1_1" data-toggle="tab">
										Lead
									</a>
								</li>
								<li>
									<a href="#tab_1_2" data-toggle="tab">
										Accounts
									</a>
								</li>
								<li>
									<a href="#tab_1_3" data-toggle="tab">
										Contacts
									</a>
								</li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="tab_1_1">
									<div class="scroller" style="height: 425px;" data-always-visible="1" data-rail-visible1="1">
										<div class="row">
                                           <?php
												foreach($lead->result() as $row3)
												{
												?>
												<div class="col-md-6 user-info">
													<img alt="" src="<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/avatar.png" class="img-responsive"/>
													<div class="details">
														<div>
															<a href="#">
																<?php echo $row3->Lead_Company; ?>
															</a>
															<span class="label label-sm label-info">
																<?php echo $row3->Lead_Name; ?>
															</span>
														</div>
														<div>
															<?php echo $row3->Lead_Email; ?>
														</div>
													</div>
												</div>
                                                <?php
												}
											?>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="tab_1_2">
									<div class="scroller" style="height: 425px;" data-always-visible="1" data-rail-visible1="1">
										<div class="row">
											<?php
												foreach($account->result() as $row4)
												{
												?>
												<div class="col-md-6 user-info">
													<img alt="" src="<?php echo base_url();?>application/libraries/assets/layouts/layout/img/avatar.png" class="img-responsive"/>
													<div class="details">
														<div>
															<a href="#">
																<?php echo $row4->account_name; ?>
															</a>
															<span class="label label-sm label-success label-mini">
																<?php echo $row4->account_phone; ?>
															</span>
														</div>
														<div>
															<?php echo $row4->account_email; ?>
														</div>
													</div>
												</div>
                                                <?php
												}
											?>
											
										</div>
									</div>
								</div>
								<div class="tab-pane" id="tab_1_3">
									<div class="scroller" style="height: 425px;" data-always-visible="1" data-rail-visible1="1">
										<div class="row">
                                            <?php
												foreach($contact->result() as $row5)
												{
												?>
												<div class="col-md-6 user-info">
													<img alt="" src="<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/avatar.png" class="img-responsive"/>
													<div class="details">
														<div>
															<a href="#">
																<?php echo $row5->contact_name; ?>
															</a>
															<span class="label label-sm label-danger">
																<?php echo $row5->contact_mobile; ?>
															</span>
														</div>
														<div>
															<?php echo $row5->contact_email; ?>
														</div>
													</div>
												</div>
                                                <?php
												}
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="col-md-6 col-sm-6">
				<!-- BEGIN PORTLET-->
				<div class="portlet light calendar bordered">
					<div class="portlet-title ">
						<div class="caption">
							<i class="icon-calendar font-green-sharp"></i>
							<span class="caption-subject font-green-sharp bold uppercase">Feeds</span>
						</div>
					</div>
					<div class="portlet-body">
						<div id="calendar"> </div>
					</div>
				</div>
				<!-- END PORTLET-->
			</div>
		</div>
		
		
		
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->


<script>
 $(document).ready(function ()
 {
  randomToast();
 });
 
 function randomToast ()
 {
  var priority = 'success';
  var title    = 'HELLO'+' <?php echo $this->session->userdata('name');?>'+' ';
  var message  = '<br/>Have! A Nice Day.';
  
  $.toaster({ priority : priority, title : title, message : message });
 }

 
</script>