<?php
$new_ticket = 0;
$complete_ticket = 0;
$pending_ticket = 0;
foreach ($new->result() as $row) {
	$new_ticket = $row->new_tic;
}
foreach ($complete->result() as $row1) {
	$complete_ticket = $row1->complete;
}
foreach ($pending->result() as $row3) {
	$pending_ticket = $row3->pending;
}
?>
<?php
$task_today = 0;
$task_coming = 0;
$task_completed = 0;
foreach ($task_info->result() as $row2) {
	$date = date_create($row2->task_reminder);
	$d = date_format($date, 'Y-m-d');
	$date1 = date_create($curr);
	$today = date_format($date1, 'Y-m-d');
	if ($d < $today) {
		$task_completed = $task_completed + 1;
	}
	if ($d > $today) {
		$task_coming = $task_coming + 1;
	}
	if ($d == $today) {
		$task_today = $task_today + 1;
	}
}
?>
<?php
$event_call = 0;
$event_email = 0;
$event_meeting = 0;
$events=[];

foreach ($event->result() as $row5) {
	// if ($row5->event_type == '4') {
	// 	$event_meeting = $event_meeting + 1;
	// }
	$events[]=[
		'title' => $row5->subject,
		'start'=> $row5->event_start_date,
		'end'=> $row5->event_end_date,
		'url'=> base_url()."index.php/crm/view_edit_call_event/".$row5->event_id
	];
}
foreach ($task_mail->result() as $row6) {
	$event_email = $row6->t_mail;
}
foreach ($task_call->result() as $row7) {
	$event_call = $row7->t_call;
}
?>




<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<!-- BEGIN CONTENT BODY -->
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->

		<!-- BEGIN PAGE BAR -->
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<?php if ($this->session->userdata('owned_by') == 0) {
					?> <a href="<?php echo base_url(); ?><?php echo $this->router->fetch_class(); ?>/">Report</a>
					<?php } else {
					?>
						Report
					<?php }

					?>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<?php if ($this->session->userdata('owned_by') == 0) {
					?>
						<a href="<?php echo base_url(); ?><?php echo $this->router->fetch_class(); ?>/dashboard/">Dashboard</a>
					<?php } else {
					?>
						Dashboard
					<?php }

					?>
				</li>
			</ul>
			<div class="page-toolbar">
				<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
					<i class="icon-calendar"></i>&nbsp;
					<span class="thin uppercase hidden-xs"></span>&nbsp;
					<i class="fa fa-angle-down"></i>
				</div>
			</div>
		</div>
		<!-- END PAGE BAR -->
		<!-- BEGIN PAGE TITLE-->
		<h3 class="page-title"> Dashboard
			<!-- <small>dashboard & statistics</small> -->
		</h3>
		<!-- END PAGE TITLE-->
		<!-- BEGIN DASHBOARD STATS -->
		<div class="row">
			<div class="col-md-4">
				<div class="dashboard-stat green">
					<div class="visual">
						<i class="fa fa-ticket"></i>
					</div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="<?php echo $new_ticket; ?>">0</span>
						</div>
						<div class="desc"> New </div>
					</div>
					<?php if ($this->session->userdata('owned_by') == 0) {
					?>
						<a class="more" href="<?php echo base_url(); ?>index.php/admin/index"> View more
							<i class="m-icon-swapright m-icon-white"></i>
						</a>
					<?php }			?>
				</div>
			</div>

			<div class="col-md-4">
				<div class="dashboard-stat green">
					<div class="visual">
						<i class="fa fa-ticket"></i>
					</div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="<?php echo $complete_ticket; ?>">0</span>
						</div>
						<div class="desc"> Completed </div>
					</div>
					<?php
					if ($this->session->userdata('owned_by') == 0) {
					?>
						<a class="more" href="<?php echo base_url(); ?>index.php/admin/view_assign_ticket/0"> View more
							<i class="m-icon-swapright m-icon-white"></i>
						</a>
					<?php }

					?>
				</div>
			</div>

			<div class="col-md-4">
				<div class="dashboard-stat green">
					<div class="visual">
						<i class="fa fa-ticket"></i>
					</div>
					<div class="details">
						<div class="number">
							<span data-counter="counterup" data-value="<?php echo $pending_ticket; ?>">0</span>
						</div>
						<div class="desc"> Pending </div>
					</div>
					<?php if ($this->session->userdata('owned_by') == 0) {
					?>
						<a class="more" href="<?php echo base_url(); ?>index.php/admin/view_assign_ticket/1"> View more
							<i class="m-icon-swapright m-icon-white"></i>
						</a>
					<?php } ?>
				</div>
			</div>

		</div>
		<!-- END DASHBOARD STATS -->

		<div class="row">
			<?php
			if ($this->session->userdata('owned_by') == 0) {
			?>
				<div class="col-md-4">
					<div class="dashboard-stat blue">
						<div class="visual">
							<i class="fa fa-tags"></i>
						</div>
						<div class="details">
							<div class="number">
								<span data-counter="counterup" data-value="<?php echo $lead1; ?>">0</span>
							</div>
							<div class="desc"> Leads </div>
						</div>
						<a class="more" href="<?php echo base_url(); ?>index.php/crm/view_lead/0"> View more
							<i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>

				<div class="col-md-4">
					<div class="dashboard-stat blue">
						<div class="visual">
							<i class="fa fa-money"></i>
						</div>
						<div class="details">
							<div class="number">
								<span data-counter="counterup" data-value="<?php echo $account1; ?>">0</span>
							</div>
							<div class="desc"> Accounts </div>
						</div>
						<a class="more" href="<?php echo base_url(); ?>index.php/crm/view_account/0"> View more
							<i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>

				<div class="col-md-4">
					<div class="dashboard-stat blue">
						<div class="visual">
							<i class="fa fa-dot-circle-o"></i>
						</div>
						<div class="details">
							<div class="number">
								<span data-counter="counterup" data-value="<?php echo $opp; ?>">0</span>
							</div>
							<div class="desc"> Opportunity </div>
						</div>
						<a class="more" href="<?php echo base_url(); ?>index.php/crm/view_opportunity/0"> View more
							<i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
			<?php }

			?>
		</div>

		<div class="clearfix"></div>
		<!-- END DASHBOARD STATS 1-->


		<div class="row">
			<div class="col-md-6 col-sm-6">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-cursor font-black"></i>
							<span class="caption-subject font-black bold uppercase">Sales Funnel</span>
						</div>
					</div>
					<div class="portlet-body">
						<div id="pyramid" style="height: 475px; max-width: 100%;"></div>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">

				<div class="portlet light calendar bordered">
					<div class="portlet-title ">
						<div class="caption">
							<i class="icon-calendar font-green-sharp"></i>
							<span class="caption-subject font-green-sharp bold uppercase">Events</span>
						</div>
					</div>
					<div class="portlet-body">
						<div id="calendar"> </div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-cursor font-black"></i>
							<span class="caption-subject font-black bold uppercase">Leads and Opportunities</span>
						</div>
					</div>
					<div class="portlet-body">
						<div id="calls-and-lead" style="height: 380px; max-width: 100%; margin: 0px auto;"></div>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-cursor font-black"></i>
							<span class="caption-subject font-black bold uppercase">Lead To Opportunity Conversion</span>
						</div>
					</div>
					<div class="portlet-body">
						<div id="calls-and-lead-conversion" style="height: 380px; max-width: 100%; margin: 0px auto;"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-cursor font-black"></i>
							<span class="caption-subject font-black bold uppercase">Leads Won v/s Lead Lost</span>
						</div>
					</div>
					<div class="portlet-body">
						<div id="lead-won-lost" style="height: 380px; max-width: 100%; margin: 0px auto;"></div>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-cursor font-black"></i>
							<span class="caption-subject font-black bold uppercase">Type Of Leads</span>
						</div>
					</div>
					<div class="portlet-body">
						<div id="types-of-leads" style="height: 380px; max-width: 100%; margin: 0px auto;"></div>
					</div>
				</div>
			</div>
		</div>



	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->


<script>
	$(document).ready(function() {
		randomToast();
	});

	function randomToast() {
		var priority = 'success';
		var title = 'HELLO' + ' <?php echo $this->session->userdata('name'); ?>' + ' ';
		var message = '<br/>Have! A Nice Day.';

		$.toaster({
			priority: priority,
			title: title,
			message: message
		});
	}
</script>
<script src="<?php echo base_url() ?>assets/canvasjs.min.js"></script>
<script>
	window.onload = function() {
		pyramid();
		loc();
		winlost();
		typeleads();
		opptoleads();

		var chart6 = new CanvasJS.Chart("average-handling", {
			animationEnabled: true,
			theme: "light2",
			data: [{
				type: "line",
				indexLabelFontSize: 16,
				dataPoints: [{
						y: 30,
						label: "Ankit"
					},
					{
						y: 20,
						label: "Abhishek"
					},
					{
						y: 50,
						label: "Total result"
					}
				]
			}]
		});
		chart6.render();

	}
</script>

<script type="text/javascript">
	function toolTipFormatter(e) {
		var str = "";
		var total = 0;
		var str3;
		var str2;
		for (var i = 0; i < e.entries.length; i++) {
			var str1 = "<span style= \"color:" + e.entries[i].dataSeries.color + "\">" + e.entries[i].dataSeries.name + "</span>: <strong>" + e.entries[i].dataPoint.y + "</strong> <br/>";
			total = e.entries[i].dataPoint.y + total;
			str = str.concat(str1);
		}
		str2 = "<strong>" + e.entries[0].dataPoint.label + "</strong> <br/>";
		str3 = "";
		return (str2.concat(str)).concat(str3);
	}

	function toolTipFormatter1(e) {
		var str = "";
		var total = 0;
		var str3;
		var str2;
		for (var i = 0; i < e.entries.length; i++) {
			var str1 = "<span style= \"color:" + e.entries[i].dataSeries.color + "\">" + e.entries[i].dataSeries.name + "</span>: <strong>" + e.entries[i].dataPoint.y + "%</strong> <br/>";
			total = e.entries[i].dataPoint.y + total;
			str = str.concat(str1);
		}
		str2 = "<strong>" + e.entries[0].dataPoint.label + "</strong> <br/>";
		str3 = "";
		return (str2.concat(str)).concat(str3);
	}

	function toggleDataSeries(e) {
		if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
			e.dataSeries.visible = false;
		} else {
			e.dataSeries.visible = true;
		}
		chart.render();
	}

	function explodePie(e) {
		if (typeof(e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
			e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
		} else {
			e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
		}
		e.chart.render();

	}
</script>
<script>
	function pyramid() {
		var link = "<?= base_url() ?>report/pyramid";
		var d = new Date();
		var currentYear = d.getFullYear();
		$.ajax({
			type: "POST",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: link,
			// data: {
			// 	month_val: month_val,
			// 	year_val: year_val,
			// 	grade: grade,
			// 	location: location
			// },
			dataType: 'json',
			success: function(response) {
				//alert(response);
				var chart = new CanvasJS.Chart("pyramid", {
					animationEnabled: true,
					theme: "light2",
					data: [{
						type: "funnel",
						// height: 260,
						showInLegend: true,
						indexLabelPlacement: "inside",
						indexLabelFontColor: "white",
						indexLabelFontSize: 12,
						indexLabelOrientation: "horizontal",
						toolTipContent: "<b>{label}</b>: {y} <b>({percentage}%)</b>",
						indexLabel: " ", //{label} ({y})
						dataPoints: response
					}]
				});
				calculatePercentage();
				chart.render();

				function calculatePercentage() {
					var dataPoint = chart.options.data[0].dataPoints;
					var total = dataPoint[0].y;
					for (var i = 0; i < dataPoint.length; i++) {
							chart.options.data[0].dataPoints[i].percentage = 25;
						//if (i == 0) {
						//	chart.options.data[0].dataPoints[i].percentage = 100;
						//} else {
							//chart.options.data[0].dataPoints[i].percentage = ((dataPoint[i].y / total) * 100).toFixed(2);
						//}
					}
				}
			},
			error: function(log) {
				//alert(response);
				console.log(log);
			}
		});

	}

	function loc() {
		var link = "<?= base_url() ?>report/loc";
		var d = new Date();
		var currentYear = d.getFullYear();
		$.ajax({
			type: "POST",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: link,
			// data: {
			// 	month_val: month_val,
			// 	year_val: year_val,
			// 	grade: grade,
			// 	location: location
			// },
			dataType: 'json',
			success: function(data) {
				var chart2 = new CanvasJS.Chart("calls-and-lead-conversion", {
					animationEnabled: true,
					axisX: {
						labelAngle: 50,
					},
					legend: {
						cursor: "pointer",
						itemclick: toggleDataSeries
					},
					toolTip: {
						shared: true,
						content: toolTipFormatter1
					},
					data: [{
							type: "column",
							showInLegend: true,
							color: "#4aacc5",
							name: "Leads to Opportunity Conversion",
							dataPoints: data.dataPoints1
						},
						{
							type: "column",
							showInLegend: true,
							color: "#f79647",
							name: "Opportunity to Account Conversion",
							dataPoints: data.dataPoints2
						}
					]
				});
				chart2.render();
			},
			error: function(log) {
				console.log(log);
			}
		});

	}

	function winlost() {
		var link = "<?= base_url() ?>report/winLost";

		var d = new Date();
		var currentYear = d.getFullYear();

		$.ajax({
			type: "POST",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: link,
			data: {
				// month_val: month_val,
				// year_val: year_val,
				// grade: grade,
				// location: location
			},
			dataType: 'json',
			success: function(data) {
				var chart10 = new CanvasJS.Chart("lead-won-lost", {
					animationEnabled: true,
					backgroundColor: "#ecf0f5",
					colorSet: "",
					title: {
						fontFamily: "tahoma",
						fontSize: 18,
						fontColor: "#484848",
						fontWeight: "bold",
						padding: {
							top: 1,
							bottom: 20
						},
						text: "     "
					},
					data: [{
						type: "pie",
						startAngle: 240,
						yValueFormatString: "",
						indexLabel: "{label} {y}",
						dataPoints: [{
								y: data.won,
								label: "Won"
							},
							{
								y: data.lost,
								label: "Lost"
							},
						]
					}]
				});


				chart10.render();

			}
		});
	}

	function typeleads() {
		var link = "<?= base_url() ?>report/typeOfLeads";

		var d = new Date();
		var currentYear = d.getFullYear();

		$.ajax({
			type: "POST",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: link,
			data: {
				// month_val: month_val,
				// year_val: year_val,
				// grade: grade,
				// location: location
			},
			dataType: 'json',
			success: function(response) {
				//console.log(response);
				var chart4 = new CanvasJS.Chart("types-of-leads", {
					animationEnabled: true,
					backgroundColor: "#ecf0f5",
					colorSet: "",
					title: {
						fontFamily: "tahoma",
						fontSize: 16,
						fontColor: "#484848",
						fontWeight: "bold",
						padding: {
							top: 1,
							bottom: 20
						},
						text: "     "
					},
					data: [{
						type: "pie",
						startAngle: 360,
						showInLegend: true,
						yValueFormatString: "",
						indexLabel: "{label} {y}",
						dataPoints: response
					}]
				});


				chart4.render();

			},
			error: function(log) {
				//alert(response);
				console.log(log);
			}
		});
	}

	function opptoleads() {
		var link = "<?= base_url() ?>report/callToLead";
		var d = new Date();
		var currentYear = d.getFullYear();
		$.ajax({
			type: "POST",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: link,
			// data: {
			// 	month_val: month_val,
			// 	year_val: year_val,
			// 	grade: grade,
			// 	location: location
			// },
			dataType: 'json',
			success: function(data) {
				var chart2 = new CanvasJS.Chart("calls-and-lead", {
					animationEnabled: true,
					axisX: {
						labelAngle: 50,
					},
					legend: {
						cursor: "pointer",
						itemclick: toggleDataSeries
					},
					toolTip: {
						shared: true,
						content: toolTipFormatter
					},
					data: [{
							type: "column",
							showInLegend: true,
							color: "#4aacc5",
							name: "Leads",
							dataPoints: data.dataPoints2
						},
						{
							type: "column",
							showInLegend: true,
							color: "#f79647",
							name: "Opportunities",
							dataPoints: data.dataPoints1
						}
					]
				});
				chart2.render();
			},
			error: function(log) {
				console.log(log);
			}
		});

	}
</script>
<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/jquery.min.js"></script>
<script>
var AppCalendar = function() {

    return {
        //main function to initiate the module
        init: function() {
            this.initCalendar();
        },

         initCalendar: function() {
            if (!jQuery().fullCalendar) {
                return;
            }

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            var h = {};

            if ($('#calendar').width() <= 400) {
                $('#calendar').addClass("mobile");
                h = {
                    left: 'title, prev, next',
                    center: '',
                    right: 'today,month,agendaWeek,agendaDay'
                };
            } else {
                $('#calendar').removeClass("mobile");
                if (App.isRTL()) {
                    h = {
                        right: 'title',
                        center: '',
                        left: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                } else {
                    h = {
                        left: 'title',
                        center: '',
                        right: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                }
            }



            $('#calendar').fullCalendar('destroy'); // destroy the calendar
            $('#calendar').fullCalendar({ //re-initialize the calendar
                disableDragging: false,
                header: h,
                editable: true,
                events: <?php echo json_encode($events)?>
            });
        },


    };

}();

jQuery(document).ready(function() {    
   AppCalendar.init(); 
});
</script>