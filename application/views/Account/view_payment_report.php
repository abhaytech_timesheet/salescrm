<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/payment_report/"><i class="icon-wallet"></i> Accounts</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/payment_report/"><i class="fa fa-credit-card"></i> Project Payment Report</a>
						<i class="fa fa-angle-right"></i>
					</li>
					
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Project Payment Report </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="glyphicon glyphicon-envelope font-dark"></i>
								<span class="caption-subject bold uppercase">View Project Payment Report</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
								<tr>
									<th>S No.</th>
									<th class="ignore">Action</th>
									<th>Project S. No.</th>
									<th>Project Name</th>
									<th>Project Unit Price</th>
									<th>Project Actual Price</th>
									<th>Total Pay Payment </th>
									<th>Due Payment</th>
									<th>Next Installment date</th>
									<th>Project Tax</th>
									<th>Name</th>
									<th>Contact</th>
									<th>Email</th>
									<th>Project Commision</th>
								</tr>
							</thead>
							
							<tbody>
								<?php
									$sn=1;
									
									foreach($pro->result() as $row)
									{
										if($row->pay_amount!=0.00 && $row->Due_amount !=0.00 )
{
									?>
									<tr class="<?php echo $row->Due_status; ?>">
										<td><?php echo $sn;?></td>
										<td>
                                           <a href="<?php echo base_url();?>index.php/account/project_cost/<?php echo $row->Project_Id;?>" title="Add Project Cost" class="label label-sm label-primary">
												<?php 
													if($row->Project_UnitPrice=='0.00')
													{
														echo "Fill Project Cost";
													}
													else
													{
														echo "Payment<del>&#2352; </del>";
													}
												?>
											</a>
                                           <p style="margin:0">&nbsp;</p>
                                            <a href="<?php echo base_url();?>index.php/account/project_status/<?php echo $row->Project_Id;?>/0" title="Add Project Cost" class="label label-sm label-danger">
												Close Project
											</a>
                                              
                                            <?php if($row->AMC_STATUS==0) { ?>
                                                <p style="margin:0">&nbsp;</p>
                                                <a href="<?php echo base_url();?>index.php/account/add_to_amc/<?php echo $row->Project_Id;?>" title="Add Project Cost" class="label label-sm label-success">
													Add To AMC
												</a>
                                             <?php }?>
										</td>
										<td><?php echo $row->Project_SNo; ?></td>                                                                
										<td><?php echo $row->Account_Name.'/'.$row->Website_id.'/'.$row->Project_id; ?></td>  
										<td><?php echo $row->Project_UnitPrice; ?></td> 
										<td><?php echo $row->Project_Actual_Price; ?></td>
										<td><?php echo $row->pay_amount; ?> </td>
										<td><?php echo round(($row->Project_UnitPrice-$row->pay_amount),2); ?></td>
										<td><?php echo $row->Due_date; ?></td>
										<td><?php echo $row->Project_Tax; ?></td>
										<td><?php echo $row->ACC_NAME; ?></td>
										<td><?php echo $row->ACC_PHONE; ?></td>
										<td><?php echo $row->ACC_EMAIL; ?></td>
										
										<td><?php echo $row->Project_Commision; ?></td>
									</tr>
									<?php
									$sn++;	
}	
}
?>
<tr>
										<td>Total</td>
										<td></td>
										<td></td>                                                                
										<td></td>  
										<td>0.00</td> 
										<td>0.00</td>
										<td>0.00 </td>
										<td>0.00</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
							</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	