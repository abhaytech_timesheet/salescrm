<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>AppWorks Technologies Pvt Ltd</title>
	</head>
	
	<body>
		<?php foreach( $bond_data1->result() as $rows)
		{break;}?>
		<div style="width: 100%;
		background-color: #FFF;
		margin-top: 10px;">
			<!--content1 start-->
			<div style="width: 100%;background-color: #FFF; float: right;">
				<div style="float:left"><img src="<?php echo base_url();?>application/logo/final logo-03-03.png" ></div>
				<div style="float: right;  margin-top: -25px;"><h2 style="font-size:30px;"><strong style="color:#00afd8;">AppWorks Technologies</strong> <strong style="color:#000;">Pvt. Ltd.</strong></h2>
					<p style="margin-top: -25px;font-size: 15px;">B 116/5 Shekhar Hospital Indira Nagar, Lucknow 226016.</p>
					<p style="margin-top: -14px;font-size: 15px;"><strong>GSTIN: 09AAPCA1540H1ZO</strong></p>
<p style="margin-top: -14px;font-size: 15px;">Phone:(+91) 991-911-1099&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Web-www.appworkstech.com</strong></p>
					
					
				</div>
				
			</div>
			
			
			
			<!--content1 Close-->
			
			
			
			
			
			<!--content2 start-->
			
			
			
			<div style="width: 100%;background-color: #FFF;float: right;  margin-top: 11px;">
				
				<div style="float:left;  font-size: 15px;"><strong>REC NO- <?php echo $rows->recno;?></strong></div>
				
				<div style="float: left;   margin-left: 525px; margin-top: 0px; font-size: 15px;"><strong><U>RECEIPT</U></strong></div>
				<div style="    float: right;margin-left: 179px;margin-top: -2px; font-size: 15px;"><strong>Date:</strong><?php echo $rows->pay_date;?></div>
				
			</div>
			
			
			
			<!--content2 Close-->
			<br />
			<br />
			
			
			<!--content3 start-->
			
			<div style="width: 100%;background-color: #FFF;float: right;margin-top: 18px;">
				
				<div style="float:left; font-size: 15px;"><strong>Mr/Miss</strong>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $rows->acc_name;?></div>
				<div style="clear:both;"></div>
				
				
				<div style="float:left;padding-top: 6px; font-size: 15px;"><strong>Address:&nbsp;&nbsp;&nbsp;&nbsp;</strong><?php echo $rows->address;?>&nbsp;&nbsp;,<?php echo $rows->state;?>&nbsp;&nbsp;-&nbsp;&nbsp;<?php echo $rows->city;?></div>
				<div style="clear:both;"></div>
			<div style="float:left; font-size: 15px;padding-top: 6px;"><strong>Contact No:&nbsp;&nbsp;</strong><?php echo $rows->phone;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>GSTINo-</strong><strong><?php echo $rows->gstin;?></strong></div>
			
			
			
			<!--content3 close-->
			
			
			
			<!--content4 start-->
			
			<div style="width: 100%;background-color: #FFF;float: right;margin-top: 18px;">
				
				<table width="100%" border="1" style="border-color: rgb(255, 254, 254);">
					<tr>
						<th width="6%" scope="col">S.No</th>
						<th width="34%" scope="col">DESCRIPTION</th>
						<th width="32%" scope="col">AMOUNT</th>
						<th width="23%" scope="col">PAID Rs.</th>
						<th width="5%" scope="col">P.</th>
					</tr>
					<tr>
						<td height="39" align="center">1.</td>
						<td><?php echo $rows->acc_name.'/'.$rows->Website_id.'/'.$rows->Project_id; ?></td>
						<td align="right"><?php echo $rows->amt;?></td>
						<td align="right"><?php echo $rows->amt;?></td>
						<td>&nbsp;</td>
					</tr>
				</table>
				
				<table  id="tax" width="100%" border="1" style="border-color: rgb(255, 254, 254);  margin-top: -3px;">

					<tr>
						<td width="46%" rowspan="3">&nbsp;</td>
						<td width="26%" align="right">Gross Total</td>
						<td width="23%"  align="right"><?php if($rows->tax!=0.00) { echo round((100*$rows->amt/118.00),2); } else { echo round((100*$rows->amt),2);}?></td>
						<td width="5%">&nbsp;</td>
					</tr>
<?php if($rows->tax!=0.00)
{
      if($rows->state=="Uttar Pradesh")
{?>
					<tr>
						<td align="right">CGST(09.00%).</td>
						<td  align="right"><span ><?php echo round((($rows->amt-(100*$rows->amt/118.00))/2),2);?></span></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="right">SGST(09.00%).</td>
						<td  align="right"><span ><?php echo round((($rows->amt-(100*$rows->amt/118.00))/2),2);?></span></td>
						<td>&nbsp;</td>
					</tr>
<?php }

else
{?>
<tr>
						<td align="right">IGST(18.00%).</td>
						<td  align="right"><span ><?php echo round(($rows->amt-(100*$rows->amt/118.00)),2);?></span></td>
						<td>&nbsp;</td>
					</tr>
<?php }
}
else 
{?>
<tr>
						<td align="right">TAX.</td>
						<td  align="right"><span ><?php echo round((0.00),2);?></span></td>
						<td>&nbsp;</td>
					</tr>
<? }?>
					<tr>
						<td width="46%" rowspan="3">&nbsp;</td>
						<td height="23" align="right"><strong>TOTAL</strong></td>
						<td  align="right"><?php echo $rows->amt;?></td>
						<td>&nbsp;</td>
					</tr>
				</table>
				
				
			</div>
			
			<!--content4 close-->
			<!--content5 start-->
			
			<div style="width: 100%;background-color: #FFF;float: right;margin-top: 18px;">
				
				<div style="float:left"><strong style="font-size: 17px;">Terms & Condition</strong>
					
					<p style="margin-top:3px;">1- Customer is advised to ensure that receipt is signed by authorized representative</p>
					<p style="  margin-top: -11px;">2- All disputes Subjects to Lucknow jurisdiction only</p>
					
					
				</div>
				
				<div style="float: right;
				margin-right: 56px;
				margin-top: 0px;
				font-size: 18px;"><strong>FOR AppWorks Technologies PVT. LTD</strong>
				</div>
					
					
			</div>
			
			<!--content5 close-->
			
			<!--content6 start-->
			
			<div style="width: 100%;background-color: #FFF;float: right;">
				
				<div style="float:left;font-size: 18px;"><strong>Customer's Signature</strong>

				</div>
				
				<div style="  float: right;
				margin-left: 207px;
				
				font-size: 18px;"><strong>Authrorized Signature</strong><br />
				</div>
				
				
			</div>
			
			<!--content6 close-->
			
			
			<div style="clear:both;"></div>
			
			
			
			
		</div>
	</div>
</body>
</html>