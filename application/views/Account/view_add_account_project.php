<!-- BEGIN CONTENT -->
<div id="stylized">
	<div class="page-content-wrapper">
		<div class="page-content" >		
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Create Project 
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo base_url();?>index.php/crm/view_Project">CRM</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?php echo base_url();?>index.php/crm/view_Project"> Create Project</a>
						</li>
					<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
            
            <div class="row">
			<!--row-->
            <div class="col-md-6">
            	<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Add Project
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal" method="post">
								<div class="form-body">
									
									<div class="form-group">
										<label class="col-md-4 control-label">Account Name</label>
										<div class="col-md-8">
                                        <?php
										foreach($acc->result() as $account)
										{
										if($account->account_id==$this->uri->segment(3))
										{
										?>
										<input type="text" name="txtaccount" id="txtaccount" class="form-control input-large" value="<?php echo $account->account_name; ?>" readonly />
										<?php 
										}
										} 
										?>
										</div>
									</div>
									
                                    <div class="form-group">
										<label class="col-md-4 control-label">Owner Name</label>
										<div class="col-md-8">
                                        <input type="text" name="txtowner" id="txtowner" class="form-control input-large" readonly="readonly" />
										<input type="hidden" class="form-control" name="txtacc_owner" id="txtacc_owner" value="<?php echo $this->session->userdata('profile_id')?>">										
										</div>
									</div>
									
									
									<div class="form-group">
										<label class="col-md-4 control-label">Project Cost</label>
										<div class="col-md-8">
											<input type="text" name="txtprojeccost" id="txtprojectcost" class="form-control input-large" />												
										</div>
									</div>
									
									
									
									<div class="form-group">
										<label class="col-md-4 control-label">Project Name</label>
										<div class="col-md-8">
											<input type="text" name="txtprojectname" id="txtprojectname" class="form-control input-large" />												
										</div>
									</div>
                                    
									
									<div class="form-group">
										<label class="col-md-4 control-label">Project Description</label>
										<div class="col-md-8">
											<textarea name="txtdescription" id="txtdescription" rows="3" class="form-control input-large"></textarea>
										</div>
									</div>

																		
									<div class="form-actions fluid">
									<div class="col-md-offset-4 col-md-8">
										<button type="button" onclick="insert()" class="btn blue">Submit</button>
										<button type="button" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
            </div>
            <!-- END PAGE CONTENT-->
        </div>
		
		
		<div class="col-md-6">
                	<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>View Added Projects
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
                            	<th width="33">
									 #
								</th>
								<th width="126">
									Created On
								</th>
								<th width="126">
									Project Name
								</th>
                                <th width="126">
									Project Description
								</th>
								<th width="126">
									Owner Alias
								</th>
								
							</tr>
							</thead>
							<tbody>
                            <?php
							$serial = 1;
							 foreach($proj->result() as $row5)
							{
												if($row5->m_acc_owner==0)
																{
																$owner="SuperAdmin";
																}
																else
																{
																foreach($emp->result() as $emprow)
                                                                  {
										                         	if($row5->m_acc_owner==$emprow->or_m_reg_id)
                                                                      	{
											                         	$owner= $emprow->or_m_name;
											                             }
                                                                   }
																   }
								?>
                            <tr class="odd gradeX">
								<td><?php echo $serial; ?></td>
								<td><?php echo substr($row5->m_project_create,0,10); ?></td>
                               	<td><?php echo $row5->m_project_name; ?></td>
                                <td><?php echo substr($row5->m_project_description,0,40); ?></td>
                                <td><?php echo $owner; ?></td>
                                
							</tr>
                            <?php
							 $serial++;
							 }
							?>
							</tbody>
							</table>
						</div>
					</div>
			</div>
		
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->


<script>
fill_userid();
function fill_userid()
{
   var user_id=<?php echo $this->session->userdata('user_id')?>;
  if(user_id!="9999999999")
  {
  $.ajax(
  {
	type:"POST",
	url:"<?php echo base_url();?>index.php/master/validateUser/0",
	data:"txtintuserid="+user_id,
	success: function(msg) {
	if(msg!="false")
	{
	}
	else
	{
    document.getElementById("txtowner").value=msg;                   
	alert('No User in this Id');
	}
  }
  }
	)

}
else
{
 document.getElementById("txtowner").value="SuperAdmin";                    
}	
}
</script>

<script>
function insert()
{
alert('kk');
	var data={
	txtpro_owner:$('#txtacc_owner').val(),
	txtprojectcost:$('#txtprojectcost').val(),
	txtprojectname:$('#txtprojectname').val(),
    txtdescription:$('#txtdescription').val()
	};
	$.ajax(
        {
 	type: "POST",
 	url:"<?php echo base_url();?>index.php/crm/insert_acc_proj/<?php echo $this->uri->segment(3); ?>",
 	dataType:'json',
	data: data,
    success: function(msg) {
 	      if(msg==true)
		    {
			$(".page-content").html("<center><h2>Project Added Successfully!</h2></center>")
			.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/img/loading-spinner-blue.gif' /></center>")
			.hide()
			.fadeIn(1000,function()
			{
			$("#stylized").load("<?php echo base_url().'index.php/crm/show_account'?>");
			}
			);

	}	  
         }
       });
}
</script>