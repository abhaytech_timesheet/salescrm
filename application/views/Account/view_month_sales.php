<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/account_details/"><i class="icon-wallet"></i> Accounts</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<span><i class="fa fa-credit-card"></i> Month Sales Report</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View Month Sales Report</h3>
			<!-- END PAGE TITLE-->
			
            <div class="row">
				<div class="col-md-12">
					
					<div class="table-responsive" id="data1">
						<table class="table table-striped table-bordered table-hover table-full-width" id="datatable_products">
							<thead>
								<tr role="row" class="heading">
									
									<th  width="20%">
										Employee ID
									</th>
									<th>
										Employee Name
									</th>
									<th width="20%">
										Ticket Duration
									</th>
									<th width="20%">
										Status
									</th>
									<th width="20%">
										Actions
									</th>
								</tr>
								
								<form method="post" action="<?php echo base_url()?>account/view_month_sales" >
									<tr role="row" class="filter">
										
										
										<td>
											<input type="text" class="form-control form-filter input-sm" name="txtempid" id="txtempid">
											
										</td>
										<td>
											<input type="text" class="form-control form-filter input-sm" name="txtempname" id="txtempname">
											
										</td>
										
										
										<td>
											<select name="ddduration" id="ddduration" class="form-control form-filter input-sm">
												<option value="-1">Select...</option>
												<option value="<?php echo date('Y-01-01'); ?>">January </option>
												<option value="<?php echo date('Y-02-01'); ?>">February</option>
												<option value="<?php echo date('Y-03-01'); ?>">March</option>
												<option value="<?php echo date('Y-04-01'); ?>">April</option>
                                                <option value="<?php echo date('Y-05-01'); ?>">May</option>
												<option value="<?php echo date('Y-06-01'); ?>">June</option>
												<option value="<?php echo date('Y-07-01'); ?>">July</option>
												<option value="<?php echo date('Y-08-01'); ?>">August</option>
                                                <option value="<?php echo date('Y-09-01'); ?>">September</option>
												<option value="<?php echo date('Y-10-01'); ?>">October</option>
                                                <option value="<?php echo date('Y-11-01'); ?>">November </option>
												<option value="<?php echo date('Y-12-01'); ?>">December</option>
											</select>
										</td>
										<td>
											<select name="ddstatus" id="ddstatus" class="form-control form-filter input-sm">
												<option value="-1">Select...</option>
												<option value="2">UPCOMING <b>/</b> DUE</option>
												<option value="1">PAY</option>
												
												
											</select>
										</td>
										<td>
											<div class="margin-bottom-5">
												<input type="submit" name="submit" id="submit" value="Search" class="btn green"/>
											</div>
											
										</td>
									</tr>
								</form>
							</thead>
						</table>
						
					</div>
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="glyphicon glyphicon-envelope font-dark"></i>
								<span class="caption-subject bold uppercase">View Month Sales Report</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead><tr>
									<th>S No.</th>
									<th class="ignore">Action</th>
                                    <th>Employee Name / Employee ID</th>
									<th>Project S. No.</th>
									<th>Project Name</th>
									<th>Received Payment </th>
									<th>Due Payment</th>
                                    <th>Upcoming Payment</th>
									<th>Next Installment date</th>
									<th>Name</th>
									<th>Contact</th>
									<th>Email</th>
								</tr>
							</thead>
							<tbody>
								<?php
$Totalpay_amount=0.00;
$TotalDue_amount=0.00;
$TotalUpcoming_amount=0.00;
									$sn=1;
									foreach($rec->result() as $row)
									{
	if($row->pay_amount!=0.00 )
{
									?>
									<tr class="<?php echo $row->Due_status; ?>">
										<td><?php echo $sn;?></td>
										<td>
											<a href="<?php echo base_url();?>index.php/account/project_cost/<?php echo $row->Project_Id;?>" title="Add Project Cost" class="label label-sm label-danger">
												<?php 
													if($row->Project_UnitPrice=='0.00')
													{
														echo "Fill Project Cost";
													}
													else
													{
														echo "Payment<del>&#2352; </del>";
													}
												?>
											</a>
										</td>
                                       	<td><?php echo $row->Employee_id." / ".$row->Employee_name; ?></td>   
										<td><?php echo $row->Project_SNo; ?></td>                                                                
										<td><?php echo $row->Account_Name.'/'.$row->Website_id.'/'.$row->Project_id; ?></td>  
										<td><?php echo $row->pay_amount; ?> </td>
										<td><?php echo $row->Due_amount; ?></td>
                                        <td><?php echo $row->Upcoming_amount; ?></td>
										<td><?php echo $row->Due_date; ?></td>
										<td><?php echo $row->ACC_NAME; ?></td>
										<td><?php echo $row->ACC_PHONE; ?></td>
										<td><?php echo $row->ACC_EMAIL; ?></td>
									</tr>
									<?php
$Totalpay_amount=$Totalpay_amount+$row->pay_amount;
$TotalDue_amount=$TotalDue_amount+$row->Due_amount;
$TotalUpcoming_amount=$TotalUpcoming_amount+$row->Upcoming_amount;
										$sn++;
}
									}
								?>
									<tr>
									<td>Total</td>
									<td></td>
                                    <td></td>   
									<td></td>                                                                
									<td></td>  
									<td><?php echo $Totalpay_amount; ?> </td>
									<td><?php echo $TotalDue_amount; ?></td>
                                    <td><?php echo $TotalUpcoming_amount; ?></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				
			</div>
		</div>
		
	</div>
	<!-- END CONTENT BODY -->
</div>
</div>
<!-- END CONTENT -->	