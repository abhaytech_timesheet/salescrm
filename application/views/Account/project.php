<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/account_details/"><i class="icon-wallet"></i> Accounts</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/account_details/"><i class="fa fa-credit-card"></i> Billing</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/project/<?php echo $this->uri->segment(3);?>"><i class="fa fa-sitemap"></i> Acccount Projects </a>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View All Acccount Projects </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="glyphicon glyphicon-envelope font-dark"></i>
								<span class="caption-subject bold uppercase">View and Modify All Projects on Account</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S No.</th>
										<th class="ignore">Action</th>
										<th>Project No</th>
										<th>Project Name</th>
										<th>Project Cost</th>
										<th>Actual Cost</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										$sn=1;
										foreach($pro->result() as $row)
										{
										?>
										<tr>
											<td><?php echo $sn; ?></td>
											<td>
												<a href="<?php echo base_url();?>account/project_cost/<?php echo $row->Project_Id;?>" title="Add Project Cost" class="label label-sm label-primary">
													<?php 
														if($row->Project_UnitPrice=='0.00')
														{
															echo "Fill Project Cost";
														}
														else
														{
															echo "Payment<del>&#2352; </del>";
														}
													?>
												</a>
                                                <?php if($row->AMC_STATUS==0) { ?>
                                                &nbsp;&nbsp;
                                                <a href="<?php echo base_url();?>account/add_to_amc/<?php echo $row->Project_Id;?>" title="Add Project Cost" class="label label-sm label-success">
													Add To AMC
												</a>
                                               <?php }?>
                                               &nbsp;&nbsp;
                                               <a href="<?php echo base_url();?>account/project_status/<?php echo $row->Project_Id;?>/0" title="Add Project Cost" class="label label-sm label-danger">
												Close Project
										    	</a>
											</td>
											<td><?php echo $row->Project_SNo;?></td>
									    	<td><?php echo $row->Account_Name.'/'.$row->Website_id.'/'.$row->Project_id; ?></td>
											<td><?php echo $row->Project_UnitPrice;?> <del>&#2352; </del></td>
											<td><?php echo $row->Project_Actual_Price;?> <del>&#2352; </del></td>
										</tr>
										<?php 
											$sn++;
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	
