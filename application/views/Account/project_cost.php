<?php 
	foreach($pro->result() as $row)
	{
		break;
	}
?>
<script>
	function get_amount()
	{
		var amt=document.getElementById('txtunitprc').value;
		var tax=document.getElementById('ddtax').value;
		var amt1=parseFloat(amt);
		var tax1=parseFloat(tax);
		if(tax!=-1)
		{
			var comon=(100*amt1)/(100+tax1);
			document.getElementById('txtact_prc').value=parseFloat(comon).toFixed(2);
		}
	}
</script>
<script>
	function get_amount1()
	{
		var unit=$("#txtunitprc").val();
		var tax=$("#ddtax").val();
		document.getElementById('txtact_prc').value=unit;
		if(tax!=-1)
		{
			get_amount();
		}
	}
</script>
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/account_details/"><i class="icon-wallet"></i> Accounts</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/account_details/"><i class="fa fa-credit-card"></i> Billing</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/project/<?php echo $this->uri->segment(3);?>"><i class="fa fa-sitemap"></i> Projects</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<span><i class="fa fa-rupee"></i> Payment</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Estimate Project cost </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-money font-black"></i>
								<span class="caption-subject font-black bold uppercase">Cost Estimated </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal" action="<?php echo base_url();?>index.php/account/update_project_cost/<?php echo $row->Project_Id;?>" method="post" onsubmit="return conwv('stylized')">
								<div class="form-body">
									<h4 class="caption-subject font-blue bold uppercase"> Project Information </h4>
									<div class="form-group">
										<label class="col-md-3 control-label">Project Name</label>
										<div class="col-md-9">
											<input type="text" placeholder="Enter Project Name" class="form-control input-sm empty" name="txtpro_name" id="txtpro_name" value="<?php echo $row->Account_Name.'/'.$row->Website_id.'/'.$row->Project_id; ?>" disabled>
											<span id="divtxtpro_name" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Project Owner </label>
										<div class="col-md-9">
											<input type="text" placeholder="Enter Project Ownername" class="form-control input-sm empty" name="txtpro_owner" id="txtpro_owner" value="<?php echo $row->Project_Owner;?>" disabled />
											<span id="divtxtpro_owner" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Project Id </label>
										<div class="col-md-9">
											<input type="text" placeholder="Project no"name="txtemp" id="txtemp" class="form-control input-sm empty" value="<?php echo $row->Project_SNo;?>" disabled / >
											<span id="divtxtemp" style="color:red;"></span>
										</div>
									</div>
									<h4 class="caption-subject font-blue bold uppercase"> Price Information </h4>
									<div class="form-group">
										<label class="col-md-3 control-label">Unit Price </label>
										<div class="col-md-9">
											<input type="text" placeholder="Enter Unit Price" class="form-control input-sm empty" name="txtunitprc" id="txtunitprc" value="<?php echo $row->Project_UnitPrice;?>" onblur="get_amount1()" />
											<span id="divtxtunitprc" style="color:red;"></span>
										</div>
									</div>
									
									
									<div class="form-group">
										<label class="col-md-3 control-label">Select Tax</label>
										<div class="col-md-9">
											<select class="form-control input-sm opt" name="ddtax" id="ddtax" onChange="get_amount1()">
												<option value="-1">Select Tax</option>
												<?php
													foreach($tax->result() as $row12)
													{
													?>
													<option value="<?php echo $row12->m_tax_value; ?>"  <?php echo ($row->Project_Tax ==$row12->m_tax_value ? 'selected=selected':'')?>><?php echo $row12->m_tax_name; ?> ( <?php echo $row12->m_tax_value; ?> )</option>
												<?php } ?>
											</select>
											<span id="divddtax" style="color:red;"></span>
										</div>
									</div>
									
									
									<div class="form-group">
										<label class="col-md-3 control-label">Commission</label>
										<div class="col-md-9">
											<input type="text"placeholder="Enter Commission %" class="form-control input-sm empty" name="txtcommission" id="txtcommission" value="<?php echo $row->Project_Commision;?>" />
											<span id="divtxtcommission" style="color:red;"></span>
										</div>
									</div>
									
									
									<div class="form-group">
										<label class="col-md-3 control-label">Actual Price</label>
										<div class="col-md-9">
											<input type="text" class="form-control input-sm empty" PLACEHOLDER="Enter Actual price" name="txtact_prc" id="txtact_prc" value="<?php echo $row->Project_Actual_Price;?>" />
											<span id="divtxtact_prc" style="color:red;"></span>
										</div>
									</div>
									
									
								</div>
								
								<?php
									$tot_paid=0.00;
									foreach($total_paid->result() as $record1)
									{
										$tot_paid=$record1->total;
										break;
									}
									foreach($count->result() as $record)
									{
										if($record->co < 1 && $tot_paid!=$row->Project_UnitPrice)
										{
										?>
										<div class="form-actions fluid">
											<div class="col-md-offset-3 col-md-9">
												<?php 
													if($row->Project_UnitPrice=='' || $row->Project_UnitPrice==0.00)
													{
													?>
													<button type="submit" class="btn green"><i class="fa fa-check"></i> Save</button>
													<button type="button" onclick="exit()" class="btn default">Cancel</button>
													<?php
													}
													else
													{
														
													?>
													<a href="<?php echo base_url();?>index.php/account/payment/<?php echo $this->uri->segment(3);?>/1/" class="btn green"><i class="fa fa-check"></i>Accept Payment</a>
													<?php 
													}
												?>
											</div>
										</div>
										
										<?php
										}
									}
								?>
							</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="glyphicon glyphicon-th-list font-dark"></i>
								<span class="caption-subject bold uppercase"></span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S No.</th>
										<th>Project Name</th>
										<th>Project Price</th>
										<th>Project Serial No</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$sn=1;
										foreach($pro->result() as $row)
										{
										?>					 
										<tr>
											<td><?php echo $sn;?></td>
											<td><?php echo $row->Account_Name.'/'.$row->Website_id.'/'.$row->Project_id; ?></td>
											<td><?php echo $row->Project_UnitPrice;?> <del>&#2352; </del></td>
											<td><?php echo $row->Project_SNo;?></td>
										</tr>
									<?php $sn++; }?>
								</tbody>
							</table>
						</div>
					</div>
					
					
					
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="glyphicon glyphicon-th-list font-dark"></i>
								<span class="caption-subject bold uppercase">Payment Installment Histroy</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S No.</th>
										<th>Installment Date</th>
										<th>Installment Price</th>
										<th>Due Price</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								
							</thead>
							<tbody>
								<?php 
									$sn=1;
									$sn1=1;
									foreach($cost->result() as $co)
									{
									?>
									<tr>
										<td>
											<?php echo $sn; ?>
										</td>
										<td><?php echo substr($co->m_subdate,0,10);?></td>
										<td><?php echo $co->m_amonut; ?> <del>&#2352; </del></td>
										<td><?php echo $co->m_due_amount; ?> <del>&#2352; </del></td>
										<td>
											<?php 
												if($co->m_pay_status==1)
												{
													echo 'Paid';
												}
												else
												{
													echo 'Unpaid';
												}
											?>
										</td>
										<td>
											<?php 
												if($co->m_pay_status==0)
												{
													$date=date_create($co->m_subdate);
													$d=date_format($date,'Y-m-d');
													$date1=date_create($curr);
													$today=date_format($date1,'Y-m-d');
													
													if($d<$today )
													
													{
													?>
													<a href="<?php echo base_url(); ?>index.php/account/payment/<?php echo $this->uri->segment(3); ?>/<?php echo $co->m_pay_id; ?>" title="Accept Payment" class="label label-sm label-danger">
														<del>&#2352; </del>&nbsp;&nbsp;Arrer
													</a>
													<?php
														
													}
													if($sn1==1 && $d>$today)
													{
													?>
													<a href="<?php echo base_url(); ?>index.php/account/payment/<?php echo $this->uri->segment(3); ?>/<?php echo $co->m_pay_id; ?>" title="Accept Payment" class="label label-sm label-danger">
														<del>&#2352; </del>&nbsp;&nbsp;Due
													</a>
													<?php
														$sn1++;
													}
												}
												else
												{
												?>
												
												<a href="<?php echo base_url();?>index.php/account/customer_receipt/<?php echo $row->Account_Id;?>/<?php echo $co->m_project_id;?>/<?php echo $co->m_pay_id; ?>" target="_blank" title="Accept Payment" class="label label-sm label-success">
													<i class="glyphicon glyphicon-th-list"></i> Receipt
												</a>
												
												<?php
												}
											?>
										</td>
									</tr>
									<?php 
										$sn++;
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<!-- END CONTENT BODY -->
</div>
</div>
<!-- END CONTENT -->													


<?php
	if($row->Project_UnitPrice!='0.00')
	{
	?>
	
	<script>
		//document.getElementById('ddtax').disabled = true;
		//document.getElementById('txtcommission').disabled = true;
		//document.getElementById('txtact_prc').disabled = true;
	</script>
	
	<?php
	}
	?>																																												