<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/account_details/"><i class="icon-wallet"></i> Accounts</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/account_details/"><i class="fa fa-credit-card"></i> Billing</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<span><i class="fa fa-hospital-o"></i> Services </span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Services Cost</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-rupee font-black"></i>
								<span class="caption-subject font-black bold uppercase">Services Cost</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal" action="<?php echo base_url();?>account/insert_service/<?php echo $this->uri->segment(3); ?>" method="post" id="">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Account Name</label>
										<div class="col-md-9">
											<?php
												foreach($acccount->result() as $accc)
												{
												?>
												<input type="text" name="txtaccount" id="txtaccount" disabled value="<?php echo $accc->account_name; ?>" class="form-control input-large" />
												<?php
												}
											?>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Select Service </label>
										<div class="col-md-9">
											<select id="ddservice" name="ddservice" class="form-control input-large" onchange="service()">
												<option value="-1">Select</option>
												<?php
													foreach($servicetype->result() as $row1)
													{
													?>
													<option value="<?php echo $row1->m_service_type_id; ?>"><?php echo $row1->m_service_type; ?></option>
													<?php 
													} 
												?>
											</select>
										</div>
									</div>
									
									<div class="form-group" id="service" style="display:none">
										<label class="col-md-3 control-label" id="label_name"></label>
										<div class="col-md-9">
											<input type="" name="txtservdsc" id="txtservdsc" class="form-control input-large" onblur="gettotal1()" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Select Vendor </label>
										<div class="col-md-9">
											<select id="ddvendor" name="ddvendor" class="form-control input-large">
												<option value="-1">Select</option>
												<?php 
													foreach($acc->result() as $ac)
													{	
													?>
													<option value="<?php echo $ac->account_id; ?>"><?php echo $ac->account_name; ?></option>
													<?php
													}
												?>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Unit </label>
										<div class="col-md-9">
											<input type="text" name="txtunit" id="txtunit" class="form-control input-large" onblur="gettotal()" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Unit Price</label>
										<div class="col-md-9">
											<input type="text" name="txtprice" id="txtprice" class="form-control input-large" onblur="gettotal1()" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Tax</label>
										<div class="col-md-9">
											<select id="ddtax" name="ddtax" class="form-control input-large" onchange="totalcheck()">
												<option value="-1">Select</option>
												<?php
													foreach($tax->result() as $row)
													{
													?>
													<option value="<?php echo $row->m_tax_value; ?>"><?php echo $row->m_tax_name; ?> ( <?php echo $row->m_tax_value; ?> )</option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Payment Type</label>
										<div class="col-md-9">
											<select id="ddpayment_type" name="ddpayment_type" class="form-control input-large" onchange="expirycheck()">
												<option value="-1">Select</option>
												<option value="1">Monthly</option>
												<option value="3">Quarterly</option>
												<option value="6">Half yearly</option>
												<option value="12">Yearly</option>
												<option value="11">One Time Payment</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Expiry Date</label>
										<div class="col-md-9">
											<input type="text" name="txtexpiry" id="txtexpiry" class="form-control input-large" readonly />
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Total</label>
										<div class="col-md-9">
											<input type="text" name="txttotal" id="txttotal" class="form-control input-large" />
										</div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Submit</button>
										<button type="button" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-wallet font-dark"></i>
								<span class="caption-subject bold uppercase">Service Info</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S. No.</th>
										<th>Service Name</th>
										<th>Description</th>
										<th>Account Name</th>
										<th>Vender Name</th>
										<th>Expiry Date</th>
										<th>Total Cost</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										$sn=1;
										foreach($payment->result() as $services)
										{
										?>
										<tr>
											<td><?php echo $sn; ?></td>
											<td><?php echo $services->SERVICE; ?></td>
											<td><?php echo $services->DESCRIPTION; ?></td>
											<td><?php echo $services->CLIENT; ?></td>
											<td><?php echo $services->VENDOR; ?></td>
											<td><?php echo $services->EXPIRY_DATE; ?></td>
											<td><?php echo $services->TOTAL_COST; ?></td>
											<td><?php if($services->STATUS==1)
												{
												?>
												<a href="javascript:void(0)" title="Receipt" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-th-list"></i>Receipt</a>
												<?php 
												}
												if($services->STATUS==0)
												{
												?>
												<a href="<?php echo base_url();?>account/service_payment/<?php echo $id;?>/<?php echo $services->ID?>" title="Accept Payment" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-th-list"></i>Accept Payment</a>
												<?php 
												}
											?> </td>
										</tr>
										<?php
											$sn=$sn+1;
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													
<script>
	function expirycheck()
	{
		var payment_type=$('#ddpayment_type').val();
		var now = new Date();
		var start = new Date();
		switch(payment_type)
		{
			case '12':
			start.setFullYear(start.getFullYear()+1,start.getMonth(),start.getDate()-1);
			break;
			case '6':
			start.setFullYear(start.getFullYear(), start.getMonth()+6,start.getDate()-1);
			break;
			case '3':
			start.setFullYear(start.getFullYear(), start.getMonth()+3,start.getDate()-1);
			break;
			case '1':
			start.setFullYear(start.getFullYear(), start.getMonth()+1,start.getDate()-1);
			break;
		}
		if(payment_type!='-1')
		{
			$('#txtexpiry').val(start.toLocaleDateString("ja-JP"));
		}
		else
		{
			$('#txtexpiry').attr("value","");
		}
	}
</script>

<script>
	function totalcheck()
	{
		var unit=$('#txtunit').val();
		var price=$('#txtprice').val();
		var tax=$('#ddtax').val();
		if(tax!="-1")
		{
			var multi=unit * price * tax;
			var divi=multi/100;
			var tot=(parseFloat(divi) + parseFloat(unit * price));
			$('#txttotal').val(tot);
		}
	}
</script>

<script>
	function gettotal()
	{
		var unit=$('#txtunit').val();
		var price=$('#txtprice').val();
		var tax=$('#ddtax').val();
		if(price!="" && tax!="-1")
		{
			totalcheck();
		}
		else
		{
			var multi=(unit * price);
			var divi=(multi);
			var tot=parseFloat(unit * price);
			$('#txttotal').val(tot);
		}
	}
</script>


<script>
	function gettotal1()
	{
		var unit=$('#txtunit').val();
		var price=$('#txtprice').val();
		var tax=$('#ddtax').val();
		if(unit!="" && tax!=="-1")
		{
			totalcheck();
		}
		else
		{
			var multi=(unit * price);
			var divi=(multi);
			var tot=parseFloat(unit * price);
			$('#txttotal').val(tot);
		}
	}
</script>
<script>
	function service()
	{
		var ddservice=$("#ddservice").val();
		if(ddservice!='-1' && ddservice!='')
		{
			$("#service").css('display','block');
			$.blockUI({ message: '<h3>Please Wait...</h3>' });
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>account/get_serv/"+ddservice,
				dataType: "JSON",
				data:{'ddservice':ddservice},
				success:function(data){
					setTimeout($.unblockUI, 01);
					$.each(data,function(i,item)
					{   
						$("#label_name").html(item.m_service_desc);
						$("#txtservdsc").attr('type',item.m_service_input_type);
					});
				}
			});
		}
		else
		{
			$("#service").css('display','none');
		}
	}
</script>