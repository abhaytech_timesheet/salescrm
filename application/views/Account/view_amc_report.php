<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_amc_report/"><i class="fa fa-bar-chart-o"></i> AMC</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<span><i class="fa fa-folder-o"></i> AMC Dashboard</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View all Record</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-recycle font-dark"></i>
								<span class="caption-subject bold uppercase">View Project Report</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<th>S No.</th>
									<th>Client ID</th>
									<th>Client Name</th>
									<th>Project Id</th>
									<th>Project Name</th>
									<th>AMC&nbsp;Due&nbsp;Date</th>
									<th>Expiry&nbsp;Status</th>
									<th>Phone No.</th>
									<th>Email</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$sn=1;
									foreach($rec->result() as $row)
									{
									?>
									<tr>
									   <td><?php echo $sn;?></td>
										<td><?php echo $row->USER_ID; ?></td>                                                                
										<td><?php echo $row->ACC_NAME; ?></td>  
										<td><?php echo $row->m_project_sno; ?></td> 
										<td><?php echo $row->Project_name; ?></td>
										<td><?php echo $row->Expiry_date; ?></td>
										<td><span class="label label-success"><?php echo $row->Expiry_status; ?></span></td>
										<td><?php echo $row->ACC_PHONE; ?></td>
										<td><?php echo $row->ACC_EMAIL; ?></td>
									</tr>
									<?php
										$sn++;
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
				
			</div>
		</div>
		
		<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-recycle font-dark"></i>
								<span class="caption-subject bold uppercase">View Services Report</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover dataTable no-footer" id="sample_2">
								<thead>
									<th>S No.</th>
									<th>Client Name</th>
									<th>Service Name</th>
									<th>Due&nbsp;Date</th>
									<th>Expiry&nbsp;Status</th>
									<th>Phone No.</th>
									<th>Email</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$sn=1;
									foreach($services->result() as $row)
									{
									?>
									<tr>
									   <td><?php echo $sn;?></td>                                                              
										<td><?php echo $row->CLIENT; ?></td>  
										<td><?php echo $row->SERVICE; ?></td>
										<td><?php echo $row->EXPIRY_DATE; ?></td>
										<td><span class="label label-success"><?php echo $row->EXPIRY_STATUS; ?></span></td>
										<td><?php echo $row->ACC_PHONE; ?></td>
										<td><?php echo $row->ACC_EMAIL; ?></td>
									</tr>
									<?php
										$sn++;
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
				
			</div>
		</div>
		
	</div>
	<!-- END CONTENT BODY -->
</div>
</div>
<!-- END CONTENT -->	
