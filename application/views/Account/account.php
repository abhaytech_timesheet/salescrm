<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
						<li>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/account_details/"><i class="icon-wallet"></i> Accounts</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<span><i class="fa fa-credit-card"></i> Billing</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View All Account</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="glyphicon glyphicon-envelope font-dark"></i>
								<span class="caption-subject bold uppercase">View all Accounts</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<th>S No.</th>
									<th class="ignore">Action</th>
									<th>Created On</th>
									<th>Type</th>
									<th>ID</th>
									<th>Name</th>
									<th>Contact</th>
									<th>Email</th>
									<th>Address</th>										
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$sn=1;
									foreach($rec->result() as $row)
									{
									?>
									<tr><td><?php echo $sn;?></td>
										<td class="ignore">
											<div class="btn-group">
												<button class="btn red btn-sm dropdown-toggle" data-toggle="dropdown">Action
													<i class="fa fa-angle-down"></i>
												</button>
												<ul class="dropdown-menu" style="position: relative;">
													<li>
														<a href="<?php echo base_url();?>index.php/account/project/<?php echo $row->ACC_ID;?>" title="View Project">
														<i class="fa fa-sitemap"></i> Projects</a>
													</li>
													<li>
														<a href="<?php echo base_url();?>index.php/account/view_services/<?php echo $row->ACC_ID; ?>" title="Add Services"><i class="fa fa-hospital-o"></i> Services</a>
													</li>
												</ul>
											</div>
										</td>
										<td><?php $date=date_create($row->REG_DATE); echo date_format($date,'d-m-Y') ?></td>                                                                
										<td><?php echo $row->ACC_TYPE; ?></td>
										<td><?php echo $row->USER_ID; ?></td>                                                                
										<td><?php echo $row->ACC_NAME; ?></td>                                                                
										<td><?php echo $row->ACC_PHONE; ?></td>
										<td><?php echo $row->ACC_EMAIL; ?></td>
										<td><?php echo $row->ACC_ADDRESS;?></td>
										<td><?php echo $row->ACC_STATUS; ?></td>
									</tr>
									<?php
										$sn++;
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
				
			</div>
		</div>
		
	</div>
	<!-- END CONTENT BODY -->
</div>
</div>
<!-- END CONTENT -->	
