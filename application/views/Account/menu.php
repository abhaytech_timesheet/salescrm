<!-- BEGIN BODY -->

<body class="page-header-fixed page-sidebar-closed">
<!-- BEGIN HEADER -->
<div class="header navbar navbar-fixed-top"> 
  <!-- BEGIN TOP NAVIGATION BAR -->
  <div class="header-inner"> 
    <!-- BEGIN LOGO -->
    <div class="navbar-brand" style="margin-top: -30px; margin-left: 33px; width:400px;">
      <h1 style="font-size: 30px;" class="fontsforweb_fontid_17281"> <a href="http://www.metroheights.co.in" title="Metro Heights" style="text-decoration:none;"> <span style="color:#FFFFFF;">Metroheights</span> </a> </h1>
      <h6 style="margin-top:-20px; font-size:9px; color:#FFF;">India's Leading Portal.</h6>
    </div>
    <!-- END LOGO --> 
    <!-- BEGIN RESPONSIVE MENU TOGGLER --> 
    <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <img src="<?php echo base_url() ?>application/libraries/assets/img/menu-toggler.png" alt=""/> </a> 
    <!-- END RESPONSIVE MENU TOGGLER --> 
    <!-- BEGIN TOP NAVIGATION MENU -->
    <ul class="nav navbar-nav pull-right">
      <!-- BEGIN USER LOGIN DROPDOWN -->
      <li class="dropdown user"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> <img alt="" src="<?php echo base_url() ?>application/libraries/assets/img/avatar1_small.jpg"/> <span class="username"> <?php echo $this->session->userdata('e_email'); ?> </span> <i class="fa fa-angle-down"></i> </a>
        <ul class="dropdown-menu">
          <li> <a href="extra_profile.html"> <i class="fa fa-user"></i> My Profile </a> </li>
          <li> <a href="<?php echo base_url() ?>index.php/auth/logout"> <i class="fa fa-key"></i> Log Out </a> </li>
        </ul>
      </li>
      <!-- END USER LOGIN DROPDOWN -->
    </ul>
    <!-- END TOP NAVIGATION MENU --> 
  </div>
  <!-- END TOP NAVIGATION BAR --> 
</div>
<!-- END HEADER -->
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
  <div id="menu" class="page-sidebar navbar-collapse collapse"> 
    <!-- BEGIN SIDEBAR MENU -->
    <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
      <li class="sidebar-toggler-wrapper"> 
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="sidebar-toggler hidden-phone"> </div>
        <!-- BEGIN SIDEBAR TOGGLER BUTTON --> 
      </li>
      <li class="sidebar-search-wrapper"> 
        <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
        <form class="sidebar-search" action="extra_search.html" method="POST">
          <div class="form-container">
            <div class="input-box"> <a href="javascript:;" class="remove"></a>
              <input type="text" placeholder="Search..."/>
              <input type="button" class="submit" value=" "/>
            </div>
          </div>
        </form>
        <!-- END RESPONSIVE QUICK SEARCH FORM --> 
      </li>
      <li class="start"><a href="<?php echo base_url();?>index.php/admin/dashboard"><i class="fa fa-home"></i><span class="title">Dashboard</span></a></li>
      <li> <a href="javascript:;"> <i class="fa fa-ticket"></i> <span class="title"> Announcements </span> <span class="arrow "> </span> </a>
        <ul class="sub-menu">
          <li> <a href="<?php echo base_url();?>index.php/admin/index"> View All Ticket </a> </li>
          <li> <a href="<?php echo base_url();?>index.php/admin/assign"> Assign Ticket </a> </li>
          <li> <a href="<?php echo base_url();?>index.php/admin/announcements"> Add Announcements </a> </li>
        </ul>
      </li>
      <li> <a href="javascript:;"> <i class="fa fa-users"></i> <span class="title">Registration</span> <span class="arrow "></span> </a>
        <ul class="sub-menu">
          <li><a href="<?php echo base_url() ?>index.php/crm/view_employee/">View/Modify Employee</a></li>
        </ul>
      </li>
      <li > <a href="javascript:;"> <i class="fa fa-bar-chart-o"></i> <span class="title">Sales Booster</span> </a>
        <ul class="sub-menu">
          <li><a href="<?php echo base_url() ?>index.php/crm/view_lead/0">Lead</a></li>
          <li><a href="<?php echo base_url() ?>index.php/crm/view_opportunity/0">Opportunity</a></li>
          <li><a href="<?php echo base_url() ?>index.php/crm/view_account/0">Account</a></li>
          <li><a href="<?php echo base_url() ?>index.php/crm/view_contact/0">Contact</a></li>
          <li><a href="<?php echo base_url() ?>index.php/crm/view_task/0">Task</a></li>
          <li><a href="<?php echo base_url() ?>index.php/crm/view_event/0">Event</a></li>
        </ul>
      </li>
      <li> <a href="javascript:;"><i class="fa fa-cogs"></i><span class="title">Master</span><span class="arrow "></span></a>
        <ul class="sub-menu">
          <li><a href="<?php echo base_url() ?>index.php/master/view_mainconfig"><i class="fa fa-bullhorn"></i>Manage Configuration</a></li>
          <li><a href="<?php echo base_url() ?>index.php/master/view_city"><i class="fa fa-bullhorn"></i>Manage City</a></li>
          <li><a href="<?php echo base_url() ?>index.php/master/view_designation"><i class="fa fa-shopping-cart"></i>Manage Rank</a></li>
          <li><a href="<?php echo base_url() ?>index.php/master/view_affiliate"><i class="fa fa-tags"></i>Manage Branch</a></li>
          <li><a href="<?php echo base_url() ?>index.php/master/view_planmode"><i class="fa fa-sitemap"></i>Manage PlanMode</a></li>
          <li><a href="<?php echo base_url() ?>index.php/master/view_plan"><i class="fa fa-file-o"></i>Manage Plan</a></li>
          <li><a href="<?php echo base_url() ?>index.php/master/target_plan"><i class="fa fa-sitemap"></i>Manage Target</a></li>
          <li><a href="<?php echo base_url() ?>index.php/master/view_plan_insentive"><i class="fa fa-sitemap"></i>Manage Incentive</a></li>
          <li><a href="<?php echo base_url() ?>index.php/master/main_menu"><i class="fa fa-file-o"></i>Manage Apps</a></li>
          <li><a href="<?php echo base_url() ?>index.php/master/sub_menu"><i class="fa fa-file-o"></i>Manage Sub Apps</a></li>
          <li><a href="<?php echo base_url() ?>index.php/master/view_project"><i class="fa fa-file-o"></i>Manage Project</a></li>
        </ul>
      </li>
      <li> <a href="javascript:;"><i class="fa fa-envelope-o"></i><span class="title">Manage Compaign</span><span class="arrow "></span></a>
        <ul class="sub-menu">
          <li> <a href="javascript:;"> <span class="title"> Create Campaign </span> <span class="arrow "> </span> </a>
            <ul class="sub-menu">
              <li><a href="<?php echo base_url() ?>index.php/crm/create_sms_campaign">SMS Campaign</a></li>
              <li><a href="<?php echo base_url() ?>index.php/crm/view_create_email_template">Email Campaign</a></li>
            </ul>
          </li>
          <li> <a href="javascript:;"> <span class="title"> Send Campaign </span> <span class="arrow "> </span> </a>
            <ul class="sub-menu">
              <li><a href="<?php echo base_url() ?>index.php/crm/view_send_sms_template">Send SMS Campaign</a></li>
              <li><a href="<?php echo base_url() ?>index.php/crm/view_send_email_template">Send Email Campaign</a></li>
            </ul>
          </li>
        </ul>
      </li>
      <li> <a href="javascript:;"> <i class="fa fa-desktop"></i> <span class="title">MANAGE CMS</span> <span class="arrow "></span> </a>
        <ul class="sub-menu">
          <li><a href="javascript:;"><span class="title">Home Panel</span><span class="arrow "></span></a>
            <ul class="sub-menu">
              <li><a href="<?php echo base_url() ?>index.php/cms/home_tab_id/<?php echo '1' ?>"><i class="fa  fa-book"></i>TAB 1</a></li>
              <li><a href="<?php echo base_url() ?>index.php/cms/home_tab_id/<?php echo '2' ?>"><i class="fa  fa-book"></i>TAB 2</a></li>
              <li><a href="<?php echo base_url() ?>index.php/cms/home_tab_id/<?php echo '3' ?>"><i class="fa  fa-book"></i>TAB 3</a></li>
              <li><a href="<?php echo base_url() ?>index.php/cms/home_tab_id/<?php echo '4' ?>"><i class="fa  fa-book"></i>TAB 4</a></li>
              <li><a href="<?php echo base_url() ?>index.php/cms/home_tab_id/<?php echo '5' ?>"><i class="fa  fa-book"></i>TAB 5</a></li>
            </ul>
          </li>
          <li><a href="javascript:;"><span class="title">Home Page Content</span><span class="arrow "></span></a>
            <ul class="sub-menu">
              <li><a href="javascript:;"> <span class="title">Left Panel</span><span class="arrow "></span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo base_url() ?>index.php/cms/left_tab_id/<?php echo '6' ?>">Left Widget 1</a></li>
                  <li><a href="<?php echo base_url() ?>index.php/cms/left_tab_id/<?php echo '7' ?>">Left Widget 2</a></li>
                  <li><a href="<?php echo base_url() ?>index.php/cms/left_tab_id/<?php echo '8' ?>">Left Widget 3</a></li>
                </ul>
              </li>
              <li><a href="javascript:;"><span class="title">Right Panel </span><span class="arrow "></span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo base_url() ?>index.php/cms/right_tab_id/<?php echo '9' ?>">Information Center</a></li>
                  <li><a href="<?php echo base_url() ?>index.php/cms/right_tab_id/<?php echo '10' ?>">Widget 2</a></li>
                  <li><a href="<?php echo base_url() ?>index.php/cms/right_tab_id/<?php echo '11' ?>">Widget 3</a></li>
                </ul>
              </li>
              <li><a href="javascript:;"><span class="title">Central Panel</span><span class="arrow "></span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo base_url() ?>index.php/cms/center_tab_id/<?php echo '12' ?>">Tab 1</a></li>
                  <li><a href="<?php echo base_url() ?>index.php/cms/center_tab_id/<?php echo '13' ?>">Tab 2</a></li>
                  <li><a href="<?php echo base_url() ?>index.php/cms/center_tab_id/<?php echo '14' ?>">Tab 3</a></li>
                  <li><a href="<?php echo base_url() ?>index.php/cms/center_tab_id/<?php echo '15' ?>">Tab 4</a></li>
                </ul>
              </li>
              <li><a href="javascript:;"> <span class="title">Events</span><span class="arrow "></span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo base_url() ?>index.php/cms/event_tab/">Add Events</a></li>
                  <li><a href="<?php echo base_url() ?>index.php/cms/manage_events/">Manage Events</a></li>
                </ul>
              </li>
              <li><a href="javascript:;"><span class="title">News</span><span class="arrow "></span></a>
                <ul class="sub-menu">
                  <li><a href="<?php echo base_url() ?>index.php/cms/view_news">Add News</a></li>
                  <li><a href="<?php echo base_url() ?>index.php/cms/manage_news">Manage News</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="javascript:;"><span class="title">Image Upload</span><span class="arrow"></span></a>
            <ul class="sub-menu">
              <li class="tooltips" data-container="body" data-placement="right" data-html="true"><a href="<?php echo base_url() ?>index.php/cms/image_upload"><span class="title">Banner Images</span></a></li>
              <li class="tooltips" data-container="body" data-placement="right" data-html="true" ><a href="<?php echo base_url() ?>index.php/cms/affiliate_picture"><span class="title">Affiliate Pictures</span></a></li>
            </ul>
          </li>
        </ul>
      </li>
      <li class="active"> <a href="javascript:;"><i class="fa fa-inr"></i><span class="title">Accounting</span><span class="arrow "></span></a>
        <ul class="sub-menu">
          <li><a href="<?php echo base_url() ?>index.php/account/account_details"><i class="fa fa-rupee"></i>Manage Payment</a></li>
        </ul>
      </li>
      <li> <a href="javascript:;"><i class="fa fa-sitemap"></i><span class="title">Manage Project</span><span class="arrow "></span></a>
        <ul class="sub-menu">
          <li><a href="<?php echo base_url() ?>index.php/project/view_project"><i class="fa fa-rupee"></i>Add New Project</a></li>
        </ul>
      </li>
    </ul>
    
    <!-- END SIDEBAR MENU --> 
  </div>
</div>
<!-- END SIDEBAR -->