	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/account_details/"><i class="icon-wallet"></i> Accounts</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/account_details/"><i class="fa fa-credit-card"></i> Billing</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<span><i class="fa fa-hospital-o"></i> Services </span>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<span><i class="fa fa-hospital-o"></i> Payment </span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Services Payment</h3>
			<!-- END PAGE TITLE-->
		<!-- END PAGE HEADER--> 
		<!-- BEGIN PAGE CONTENT-->
		<div class="row"> 
			<!--row-->
			<div class="col-md-6"> 
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption"> 	<i class="fa fa-rupee font-black"></i>
								<span class="caption-subject font-black bold uppercase">Services Payment</span></div>
						<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
					</div>
					<div class="portlet-body form">
						<form role="form" class="form-horizontal" action="<?php echo base_url();?>index.php/account/insert_service_payment/<?php echo $this->uri->segment(3);?>/<?php echo $this->uri->segment(4);?>" method="post" id="">
							<div class="form-body">
								
								<h3 class="form-section">Service Information</h3>
								<?php 
									foreach($service->result() as $ser)
									{
										foreach($service_type->result() as $ser_type)
										{
											if($ser_type->m_service_type_id==$ser->m_service_id)
											{
											?>
											<div class="form-group">
												<label class="col-md-3 control-label">Service Name</label>
												<div class="col-md-9">
													<input type="text" placeholder="Enter Service Name" class="form-control input-large" name="txtpro_name" id="txtacc_name" value="<?php echo $ser_type->m_service_type; ?>" disabled>
												</div>
											</div>
											
											
											
											<?php
											}
										}
									}
								?>
								
								
								<h3 class="form-section">Payment Details</h3>
								
								
								<div class="form-group">
									<label class="col-md-3 control-label">Remaining Amount </label>
									<div class="col-md-9">
										<input type="text" placeholder="Enter Remaining amount" class="form-control input-large" name="txtrem_amt" id="txtrem_amt" value="<?php echo $ser->m_total; ?>" readonly />
									</div>
								</div>
								
								
								
								<div class="form-group">
									<label class="col-md-3 control-label">Payment Amount</label>
									<div class="col-md-9">
										<input type="text" placeholder="Enter payment amount" class="form-control input-large" name="txtpay_amt" id="txtpay_amt" value="<?php echo $ser->m_total; ?>" />
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Payment Mode</label>
									<div class="col-md-9">
										<Select id="mode" name="mode" class="form-control input-large" onChange="get_payment(0)">
											<option value="-1">Select</option>
											<?php
												foreach($payment_mode->result() as $mode)
												{
												?>
												<option value="<?php echo $mode->m_pm_id; ?>"><?php echo $mode->m_pm_name; ?></option>
												<?php 
												}
											?>
										</select>
									</div>
								</div>
								
								
								<div id="category"></div>
								
								
								<div class="form-group">
									<label class="col-md-3 control-label">Payment Description</label>
									<div class="col-md-9">
										<textarea class="form-control input-large" rows="3" name="txtdescription" id="txtdescription"></textarea>
									</div>
								</div>
								
								
							</div>
							
							
							<div class="form-actions fluid">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn green"><i class="fa fa-check"></i>Collect Payment</button>
									<button type="reset" class="btn default" onClick="get_payment(1)">Cancel</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET--> 
			</div>
			<!-- END FORM CONTENT-->
			
			
			<div class="col-md-6">
				<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-wallet font-dark"></i>
								<span class="caption-subject bold uppercase">Payment Info</span>
							</div>
							<div class="tools"> </div>
						</div>
					<div class="portlet-body">
						
						<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead> 
								
								<tr>
									<th>Req Id</th>
									<th>Request Date</th>
									<th>Approval Date</th>
									<th>Amount</th>
									<th>Mode</th>
									<th>Remark</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								
							</tbody>
							
							
						</table>
					</div>
				</div>
				
				
			</div>
			
			
			
			
			
			<!-- END PAGE CONTENT--> 
		</div>
	</div>
	<!-- END CONTENT --> 
</div>
<!-- END CONTAINER --> 


<script>
	fill_userid();
	function fill_userid()
	{
		var user_id=document.getElementById("txtpro_owner").value;
		if(user_id!="0")
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/master/validateUser/0/",
				data:"txtintuserid="+user_id,
				success: function(msg) {
					if(msg!="false")
					{
						document.getElementById("txtpro_owner").value=msg; 
					}
					else
					{
						document.getElementById("txtpro_owner").value=msg;                   
						alert('No User in this Id');
					}
				}
			}
			)
			
		}
		else
		{
			document.getElementById("txtpro_owner").value="SuperAdmin";                    
		}	
	}
</script>

<script>
	function get_payment(id)
	{
		var amt=document.getElementById("mode").value;
         if(id!=0)
         {
            amt==-1;
         }
		if(amt!=-1)
		{
			$("#category").html('<div><img src ="<?php echo base_url();?>application/libraries/assets/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
			$("#category").load('<?php echo base_url() ;?>index.php/account/select_category1/'+amt);
		}
		if(amt==-1)
		{
			$("#category").html('<div><img src ="<?php echo base_url();?>application/libraries/assets/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
			$("#category").html('<p></p>');
		}
	}
</script>