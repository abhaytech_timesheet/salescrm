<div class="page-footer">
	<div class="page-footer-inner">  
		Copyright &copy;  <?php echo date('Y'); ?> <b><a href="http://techaarjavam.com">TechAarjavam</a></b>. All Rights Reserved. &nbsp;&nbsp;&nbsp;&nbsp;Terms of use&nbsp;&nbsp;&nbsp;&nbsp;Privacy Policy&nbsp;&nbsp;&nbsp;&nbsp;Site Map
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>

<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>application/libraries/js/check.js"></script>
<script src="<?php echo base_url() ?>application/libraries/js/insert_data.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/jquery.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/jquery.blockui.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/morris/morris.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/moment.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootbox/bootbox.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script src="<?php echo base_url() ?>application/libraries/js/jquery.toaster.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/fullcalendar/fullcalendar.min.js"></script>

<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<?php
	if($this->router->fetch_class()!='direct_ticket')
	{
		if($this->router->fetch_method()=='dashboard')
		{
		?>
		<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/morris/raphael-min.js"></script>
		<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/counterup/jquery.waypoints.min.js"></script>
		<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/counterup/jquery.counterup.min.js"></script>
		<?php
		}
		if($this->router->fetch_method()=='view_send_email_template' || $this->router->fetch_method()=='get_template' || $this->router->fetch_method()=='view_create_email_template' || $this->router->fetch_method()=='view_edit_email')
		{
		?>
		<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
		<?php
		}
	?>
	<script src="<?php echo base_url() ?>application/libraries/assets/global/scripts/datatable.js"></script>
	<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/datatables/datatables.min.js"></script>
	<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>
	<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
	<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
	<?php
	}
	?>
	<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>

	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS

	
	<script src="<?php echo base_url() ?>application/libraries/assets/layouts/global/scripts/quick-sidebar.min.js"></script>
 -->
	<script src="<?php echo base_url() ?>application/libraries/assets/global/scripts/app.min.js"></script>
<?php
		if($this->router->fetch_method()=='view_add_apps' || $this->router->fetch_method()=='account' || $this->router->fetch_method()=='view_module' || $this->router->fetch_method()=='edit_module')   
		{
		?>
    <script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
<?php 
}?>
	<?php
		if($this->router->fetch_class()!='direct_ticket')
		{
		?>
		<script src="<?php echo base_url() ?>application/libraries/assets/pages/scripts/table-datatables-buttons.min.js"></script>
		<script src="<?php echo base_url() ?>application/libraries/assets/pages/scripts/components-date-time-pickers.min.js"></script>
		<script src="<?php echo base_url() ?>application/libraries/assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>application/libraries/assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>application/libraries/assets/pages/scripts/components-bootstrap-switch.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>application/libraries/assets/pages/scripts/dashboard.js"></script>
		<?php
		}
	?>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script src="<?php echo base_url() ?>application/libraries/assets/layouts/layout/scripts/layout.min.js"></script>
	<script src="<?php echo base_url() ?>application/libraries/assets/layouts/layout/scripts/demo.min.js"></script>

	<!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>