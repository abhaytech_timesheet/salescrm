<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
	<!-- BEGIN HEADER -->
	<div class="page-header navbar navbar-fixed-top">
		<!-- BEGIN HEADER INNER -->
		<div class="page-header-inner ">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="#.">
				<img src="<?php if(sitelogo=='') { echo base_url().'application/libraries/assets/layouts/layout/img/company_logo_1582267357-1.png'; } else { echo base_url().'application/logo/86x14/'.sitelogo; } ?>" alt="logo" class="logo-default" style="width:138px;height:38px;margin-top:2px;" /> </a>
				<div class="menu-toggler sidebar-toggler"> </div>
			</div>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
					<li class="dropdown dropdown-user">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<img alt="" class="img-circle" src="<?php echo base_url() ?>application/libraries/assets/layouts/layout/img/avatar3_small.jpg" />
							<span class="username username-hide-on-mobile"><?php echo $this->session->userdata('name'); ?></span>
							<i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="<?php echo base_url();?>support/view_user_profile">
								<i class="icon-user"></i> Profile </a>
							</li>
							
							<li class="divider"> </li>
							
							<li>
								<a href="<?php echo base_url();?>auth/logout">
								<i class="icon-key"></i> Log Out </a>
							</li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
					<!-- BEGIN QUICK SIDEBAR TOGGLER -->
					<li class="dropdown dropdown-quick-sidebar-toggler">
						<a href="javascript:;" class="dropdown-toggle">
							<i class="icon-logout"></i>
						</a>
					</li>
					<!-- END QUICK SIDEBAR TOGGLER -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
		<!-- END HEADER INNER -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN HEADER & CONTENT DIVIDER -->
	<div class="clearfix"> </div>
	<!-- END HEADER & CONTENT DIVIDER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar-wrapper">
			<!-- BEGIN SIDEBAR -->
			<div class="page-sidebar navbar-collapse collapse">
				<!-- BEGIN SIDEBAR MENU -->
				<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
					<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
					<li class="sidebar-toggler-wrapper hide">
						<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
						<div class="sidebar-toggler"> </div>
						<!-- END SIDEBAR TOGGLER BUTTON -->
					</li>
					<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
					<li class="sidebar-search-wrapper">
						<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
						<form class="sidebar-search" action="#." method="POST">
							<a href="javascript:;" class="remove">
								<i class="icon-close"></i>
							</a>
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Search...">
								<span class="input-group-btn">
									<a href="javascript:;" class="btn submit">
										<i class="icon-magnifier"></i>
									</a>
								</span>
							</div>
						</form>
						<!-- END RESPONSIVE QUICK SEARCH FORM -->
					</li>
					
					<li class="nav-item start">
						<a href="<?php echo base_url();?>support/index">
							<i class="icon-home"></i>
							<span class="title">Dashboard</span>
							<span class="selected"></span>
						</a>
					</li>
					
					<li class="nav-item">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="fa fa-shopping-cart"></i>
							<span class="title">Announcements</span>
							<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item ">
								<a href="<?php echo base_url();?>support/Announcements" class="nav-link ">
									<i class="icon-bell"></i>
									<span class="title">View Announcement</span>
								</a>
							</li>
							
						</ul>
					</li>
					<?php
						foreach($menu->result() as $m)
						{
							if($m->m_menu_parentid=='0')
							{
							?>
							<li class="nav-item">
								<a href="javascript:;" class="nav-link nav-toggle">
									<i class="<?php echo $m->m_menu_icon; ?>"></i>
									<span class="title"><?php echo $m->m_menu_name; ?></span>
									<span class="arrow"></span>
								</a>
								<ul class="sub-menu">
									<?php
										foreach($menu->result() as $s)
										{
											if($s->m_menu_parentid==$m->m_menu_id)
											{
												
											?>
											<li class="nav-item">
												<a href="<?php echo base_url() ?>support/<?php echo $s->m_menu_url; ?>" class="nav-link">
													<i class="<?php echo $s->m_menu_icon; ?>"></i>
													<span class="title"><?php echo $s->m_menu_name; ?></span>
												</a>
												
											</li>
											<?php
											}
										}
									?>
								</ul>
							</li>
							<?php
							}
						}
					?>
					
				</ul>
				<!-- END SIDEBAR MENU -->
			</div>
			<!-- END SIDEBAR -->
		</div>
		<!-- END SIDEBAR -->						
		
		