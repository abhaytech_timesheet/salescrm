<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="#."> Service Payment </a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Service</span>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">  Service Payment  </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>
								<span class="caption-subject bold uppercase">Payment </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal" action="<?php echo base_url();?>index.php/support/insert_service_payment/<?php echo $this->uri->segment(3); ?>" method="post" id="myform" onsubmit="return check(conwv('myform'))">
								<div class="form-body">
									<h4 class="caption-subject font-blue bold uppercase">Service Information</h4>
									
									<?php 
										foreach($service->result() as $ser)
										{
											foreach($service_type->result() as $ser_type)
											{
												if($ser_type->m_service_type_id==$ser->m_service_id)
												{
												?>
												<div class="form-group">
													<label class="col-md-3 control-label">Service Name</label>
													<div class="col-md-7">
														<input type="text" placeholder="Enter Service Name" class="form-control input-sm empty" name="txtpro_name" id="txtpro_name" value="<?php echo $ser_type->m_service_type; ?>" disabled>
														<span id="divtxtpro_name" style="color:red"></span>
													</div>
												</div>
												
												<?php
												}
											}
										}
									?>
									
									<h4 class="caption-subject font-blue bold uppercase">Payment Details</h4>
									<div class="form-group">
										<label class="col-md-3 control-label">Remaining Amount </label>
										<div class="col-md-7">
											<input type="text" placeholder="Enter Remaining amount" class="form-control input-sm empty" name="txtrem_amt" id="txtrem_amt" value="<?php echo $ser->m_total; ?>" readonly />
											<span id="divtxtrem_amt" style="color:red"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Payment Amount</label>
										<div class="col-md-7">
											<input type="text" placeholder="Enter payment amount" class="form-control input-sm empty" name="txtpay_amt" id="txtpay_amt" value="<?php echo $ser->m_total; ?>" />
											<span id="divtxtpay_amt" style="color:red"></span>
										</div>
									</div>
									
									
									<div class="form-group">
										<label class="col-md-3 control-label">Select Bank</label>
										<div class="col-md-7">
											<Select id="bank" name="bank" class="form-control input-sm opt">
												<option value="-1">Select Bank</option>		
												<?php
													foreach($user_bank->result() as $rows)
													{
														foreach($bank->result() as $row)
														{
															if($rows->or_m_b_name==$row->m_bank_id)
															{
															?>	 
															<option value="<?php echo $rows->or_m_bid;?>"> <?php echo $row->m_bank_name.' - ['.$rows->or_m_b_cbsacno.'] '.'';?></option>   
															<?php
															}
														}
													}
												?>			 
											</select>
											<span id="divbank" style="color:red"></span>
										</div>
									</div>
									
									
									<div class="form-group">
										<label class="col-md-3 control-label">Payment Mode</label>
										<div class="col-md-7">
											<Select id="mode" name="mode" class="form-control input-sm opt" onChange="get_payment()">
												<option value="-1">Select</option>
												<?php
													foreach($payment_mode->result() as $mode)
													{
													?>
													<option value="<?php echo $mode->m_pm_id; ?>"><?php echo $mode->m_pm_name; ?></option>
													<?php 
													}
												?>
											</select>
											<span id="divmode" style="color:red"></span>
										</div>
									</div>
									
									
									<div id="category" style="display:none">
										
										<div class="form-group">
											<label class="col-md-3 control-label" id="title"></label>
											<div class="col-md-7">
												<input type="text" placeholder="" id="detail" name="detail" class="form-control input-sm empty"/>
												<span id="divdetail" style="color:red"></span>	
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-md-3 control-label">Transaction ID/Cheque/DD Number</label>
											<div class="col-md-7">
												<input type="text" placeholder="Your Transaction ID/Cheque/DD Number" id="transid" name="transid"  class="form-control input-sm empty" />
												<span id="divtransid" style="color:red"></span>	
											</div>
										</div>
										
									</div>
									
									
									<div class="form-group">
										<label class="col-md-3 control-label">Payment Description</label>
										<div class="col-md-7">
											<textarea class="form-control input-sm empty" rows="3" name="txtdescription" id="txtdescription"></textarea>
											<span id="divtxtdescription" style="color:red"></span>
										</div>
									</div>
									
									
								</div>
								
								
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green"><i class="fa fa-check"></i>Collect Payment</button>
										<button type="submit" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-globe"></i>
								<span class="caption-subject bold uppercase"></span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body" >
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead> 
									
									<tr>
										<th>Req Id</th>
										<th>Request Date</th>
										<th>Approval Date</th>
										<th>Amount</th>
										<th>Mode</th>
										<th>Remark</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	


<script>
	fill_userid();
	function fill_userid()
	{
		var user_id=document.getElementById("txtpro_owner").value;
		if(user_id!="0")
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/master/validateUser/0/",
				data:"txtintuserid="+user_id,
				success: function(msg) {
					if(msg!="false")
					{
						document.getElementById("txtpro_owner").value=msg.trim(); 
					}
					else
					{
						document.getElementById("txtpro_owner").value=msg.trim();                   
						alert('No User in this Id');
					}
				}
			}
			);
			
		}
		else
		{
			document.getElementById("txtpro_owner").value="SuperAdmin";                    
		}	
	}
</script>

<script>
	function get_payment()
	{
		var amt=document.getElementById("mode").value;
		
		$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url() ;?>index.php/support/select_category/"+amt,
				dataType: "JSON",
				data:{'amt':amt},
				success: function(msg) {
					$("#category").css('display','block');
					
					$.each(msg,function(i,item)
					{
					    $("#title").html(item.m_pm_name);
						$("#detail").attr('Placeholder',item.m_pm_name);
					});
				}
			});
	}
</script>
