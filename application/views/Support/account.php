<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					View Account
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<!-- <i class="fa fa-angle-down"></i> -->
							</div>
						</li>
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo base_url();?>index.php/crm/view_account">
								CRM
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?php echo base_url();?>index.php/crm/view_account">
								View All Account
							</a>
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
						
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>View & Modify View all Accounts
							</div>
														
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
									<div class="btn-group pull-right">
									<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
									</button>
									<ul class="dropdown-menu pull-right">
										<li>
											<a href="#">
												 Print
											</a>
										</li>
										<li>
											<a href="#">
												 Save as PDF
											</a>
										</li>
										<li>
											<a href="#" onclick="tableToExcel('sample_2', 'W3C Example Table')">
												 Export to Excel
											</a>
										</li>
									</ul>
								</div>
							</div>
                            <div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
											        <th>S No.</th>
													<th class="ignore">Action</th>
													<th>Created On</th>
                                                    <th>Type</th>
                                                    <th>ID</th>
													<th>Name</th>
                                                    <th>Contact</th>
                                                    <th>Email</th>
													
													<th>Address</th>										
                                                    <th>Status</th>
                                                    
                                                    
												</tr>
							</thead>
							<tbody>
                                               		<?php
													$sn=1;
													foreach($rec->result() as $row)
													{
													$type1="";
													$owner="";
													foreach($type->result() as $tyrow)
													{
													if($row->account_type==$tyrow->m_des_id)
													{
														$type1=$tyrow->m_des_name;
													}
													}
													if($row->account_status=='1')
																{
																$s='Active';
																}
																if($row->account_status=='2')
																{
																$s='Deactive';
																}
																if($row->account_owner==0)
																{
																$owner="SuperAdmin";
																$owner_type=$row->account_owned_by;
																}
																else
																{
																if($id==1||$id==0 && $row->account_owned_by==1)
																{
																foreach($emp->result() as $emprow)
                                                                  {
										                         	if($row->account_owner==$emprow->or_m_reg_id)
                                                                      	{
											                         	$owner= $emprow->or_m_name;
																		$owner_type=$row->account_owned_by;
											                             }
                                                                   }
																	}
																	if($id==2||$id==0 && $row->account_owned_by==2)
																	{
																	foreach($con->result() as $conrow)
                                                                  {
										                         	if($row->account_owner==$conrow->contact_id)
                                                                      	{
											                         	$owner= $conrow->contact_name;
																		$owner_type=$row->account_owned_by;
											                             }
                                                                   }
																	}
																}
													 ?>
															<tr><td><?php echo $sn;?></td>
                                                           <td class="ignore">
														   <a href="<?php echo base_url();?>index.php/support/project/<?php echo $row->account_id;?>" title="View Project" class="label label-sm label-danger">
															<i class="fa fa-sitemap"></i> Projects</a>&nbsp;
															
															<a href="<?php echo base_url();?>index.php/support/view_services/<?php echo $row->account_id; ?>" title="Add Services" class="label label-sm label-danger"><i class="fa fa-hospital-o"></i> Services</a>&nbsp;
															
															  <td><?php $date=date_create($row->account_regdate); echo date_format($date,'d-m-Y') ?></td>                                                                
                                                                <td><?php echo $type1; ?></td>
																<td><?php echo $row->account_user_id; ?></td>                                                                
																<td><?php echo $row->account_name; ?></td>                                                                
                                                                <td><?php echo $row->account_phone; ?></td>
                                                                <td><?php echo $row->account_email; ?></td>
																
																<td><?php echo $row->account_address; ?></td>
																 <td><?php echo $s; ?></td>
															</tr>
                                                            
                                                            <?php
															$sn++;
													}
													?>
							</tbody>
							</table>
                            </div>
						</div>
					</div>				
			<!-- END PAGE CONTENT-->
		
	</div>
	<!-- END CONTENT -->
   </div>
</div>
<!-- END CONTAINER -->