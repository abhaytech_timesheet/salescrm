<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="#">View Projects </a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>CRM</span>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">  View All Projects</h3>
			<!-- END PAGE TITLE-->
			
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-ticket font-dark" ></i>
								<span class="caption-subject bold uppercase">View and modify all projects</span>
							</div>
							
							<div class="tools"> </div>
						</div>
						<div class="portlet-body" style="overflow-x: auto;">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<h5> &nbsp;</h5>
										</div>
									</div>
									<div class="col-md-6">
										
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S No.</th>
										<th class="ignore">Action</th>
										<th>Project No</th>
										<th>Project Name</th>
										<th>Project Cost</th>
										<th>Actual Cost</th>
									</tr>
								</thead>
								
								<tbody>
									<?php 
										$sn=1;
										foreach($pro->result() as $row)
										{
										?>
										<tr>
											<td><?php echo $sn; ?></td>
											<td>
												<a href="<?php echo base_url();?>index.php/support/project_cost/<?php echo $row->m_acc_project;?>" title="Add Project Cost" class="label label-sm label-danger">
													<?php 
														if($row->m_unit_price=='0.00')
														{
														?>
														Fill Project Cost
														<?php
														}
														else
														{
														?>
														Payment <del>&#2352; </del>
														<?php
														}
													?>
												</a>
											</td>
											<td><?php echo $row->m_project_sno;?></td>
											<td><?php echo $row->m_project_name;?></td>
											<td><?php echo $row->m_unit_price;?> <del>&#2352; </del></td>
											<td><?php echo $row->m_actual_price;?> <del>&#2352; </del></td>
										</tr>
										<?php 
											$sn++;
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->
