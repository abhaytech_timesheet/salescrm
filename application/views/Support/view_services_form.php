<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="#."> Account Panel </a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Dashboard</span>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">  Services Cost  </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>
								<span class="caption-subject bold uppercase">Services Cost </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal" action="<?php echo base_url();?>index.php/support/insert_service/<?php echo $this->uri->segment(3); ?>" method="post" id="myform" onsubmit="return check(conwv('myform'))">
								<div class="form-body">
									
									<div class="form-group">
										<label class="col-md-3 control-label">Account Name</label>
										<div class="col-md-7">
											<?php
												foreach($acccount->result() as $accc)
												{
												?>
												<input type="text" name="txtaccount" id="txtaccount" disabled value="<?php echo $accc->account_name; ?>" class="form-control input-sm empty" />
												<?php
												}
											?>
											<span id="divtxtaccount" style="color:red"></span>
										</div>
									</div>
									
									
									
									<div class="form-group">
										<label class="col-md-3 control-label">Select Service </label>
										<div class="col-md-7">
											<select id="ddservice" name="ddservice" class="form-control input-sm opt">
												<option value="-1">Select</option>
												<?php
													foreach($servicetype->result() as $row1)
													{
													?>
													<option value="<?php echo $row1->m_service_type_id; ?>"><?php echo $row1->m_service_type; ?></option>
												<?php } ?>
											</select>
											<span id="divddservice" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Select Vendor </label>
										<div class="col-md-7">
											<select id="ddvendor" name="ddvendor" class="form-control input-sm opt">
												<option value="-1">Select</option>
												<?php 
													foreach($acc->result() as $ac)
													{	
													?>
													<option value="<?php echo $ac->account_id; ?>"><?php echo $ac->account_name; ?></option>
													<?php
													}
												?>
											</select>
											<span id="divddvendor" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Unit </label>
										<div class="col-md-7">
											<input type="text" name="txtunit" id="txtunit" class="form-control input-sm empty" onblur="gettotal()" />
											<span id="divtxtunit" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Unit Price</label>
										<div class="col-md-7">
											<input type="text" name="txtprice" id="txtprice" class="form-control input-sm empty" onblur="gettotal1()" />
											<span id="divtxtprice" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Tax</label>
										<div class="col-md-7">
											<select id="ddtax" name="ddtax" class="form-control input-sm opt" onchange="totalcheck()">
												<option value="-1">Select</option>
												<?php
													foreach($tax->result() as $row)
													{
													?>
													<option value="<?php echo $row->m_tax_value; ?>"><?php echo $row->m_tax_name; ?> ( <?php echo $row->m_tax_value; ?> )</option>
												<?php } ?>
											</select>
											<span id="divddtax" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Payment Type</label>
										<div class="col-md-7">
											<select id="ddpayment_type" name="ddpayment_type" class="form-control input-sm opt" onchange="expirycheck()">
												<option value="-1">Select</option>
												<option value="1">Monthly</option>
												<option value="3">Quarterly</option>
												<option value="6">Half yearly</option>
												<option value="12">Yearly</option>
											</select>
											<span id="divddpayment_type" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Expiry Date</label>
										<div class="col-md-7">
											<input type="text" name="txtexpiry" id="txtexpiry" class="form-control input-sm empty" readonly />
											<span id="divtxtexpiry" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Total</label>
										<div class="col-md-7">
											<input type="text" name="txttotal" id="txttotal" class="form-control input-sm empty" />
											<span id="divtxttotal" style="color:red"></span>
										</div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Submit</button>
										<button type="button" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-globe"></i>
								<span class="caption-subject bold uppercase"></span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body" >
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S. No.</th>
										<th>Service Name</th>
										<th>Vender Name</th>
										<th>Expiry Date</th>
										<th>Total Cost</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										$sn=1;
										foreach($service->result() as $services)
										{
										?>
										<tr>
											<td><?php echo $sn; ?></td>
											<td>
												<?php 
													foreach($servicetype->result() as $row11)
													{
														if($services->m_service_id==$row11->m_service_type_id)
														{
															echo $row11->m_service_type;
														}
													}
												?>
											</td>
											<td>
												<?php 
													foreach($acc->result() as $ac)
													{
														if($services->m_vendor_acc_id==$ac->account_type)
														{
															echo $ac->account_name;
														}
													}						
												?>
											</td>
											<td><?php echo $services->m_expiry_date; ?></td>
											<td><?php echo $services->m_total; ?></td>
											<td>
												<?php
													foreach($payment->result() as $rec12)
													{
														if($rec12->m_pay_status=='0')
														{
														?>
														<a href="<?php echo base_url(); ?>index.php/support/service_payment/<?php echo $services->m_acc_id; ?>" class="btn btn-sm btn-danger">
															
															<del>&#2352; </del>&nbsp;&nbsp;Due
															
														</a>
														<?php
														}
														else
														{
														?>
														<a href="javascript:void(0)" title="Accept Payment" class="btn btn-sm btn-success">
															<i class="glyphicon glyphicon-th-list"></i> Recipt
														</a>
														<?php
														}
													}
												?>
											</td>
										</tr>
										<?php
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	

<script>
	function expirycheck()
	{
		var payment_type=$('#ddpayment_type').val();
		var now = new Date();
		var start = new Date();
		switch(payment_type)
		{
			case '12':
			start.setFullYear(start.getFullYear()+1,start.getMonth(),start.getDate()-1);
			break;
			case '6':
			start.setFullYear(start.getFullYear(), start.getMonth()+6,start.getDate()-1);
			break;
			case '3':
			start.setFullYear(start.getFullYear(), start.getMonth()+3,start.getDate()-1);
			break;
			case '1':
			start.setFullYear(start.getFullYear(), start.getMonth()+1,start.getDate()-1);
			break;
		}
		if(payment_type!='-1')
		{
			$('#txtexpiry').val(start.toLocaleDateString("ja-JP"));
		}
		else
		{
			$('#txtexpiry').attr("value","");
		}
	}
</script>

<script>
	function totalcheck()
	{
		var unit=$('#txtunit').val();
		var price=$('#txtprice').val();
		var tax=$('#ddtax').val();
		if(tax!="-1")
		{
			var multi=unit * price * tax;
			var divi=multi/100;
			var tot=(parseFloat(divi) + parseFloat(unit * price));
			$('#txttotal').val(tot);
		}
	}
</script>

<script>
	function gettotal()
	{
		var unit=$('#txtunit').val();
		var price=$('#txtprice').val();
		var tax=$('#ddtax').val();
		if(price!="" && tax!=="-1")
		{
			totalcheck();
		}
	}
</script>


<script>
	function gettotal1()
	{
		var unit=$('#txtunit').val();
		var price=$('#txtprice').val();
		var tax=$('#ddtax').val();
		
		if(unit!="" && tax!=="-1")
		{
			totalcheck();
		}
	}
</script>																						