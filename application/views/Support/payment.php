<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="#.">Project Payment </a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span> Project </span>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Project Payment </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Create New Employee </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" action="<?php echo base_url();?>index.php/support/insert_payment/<?php echo $this->uri->segment(3); ?>" method="post" id="myform" onsubmit="return check(conwv('myform'))">
								<div class="form-body">
									
									
									<h4 class="caption-subject font-blue bold uppercase">Project Information</h4>
									<div class="row">
										<?php 
											foreach($pro->result() as $project)
											{
											?>
											
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Project Name</label>
													<input type="text" placeholder="Enter Project Name" class="form-control input-sm empty" name="txtpro_name" id="txtpro_name" value="<?php echo $project->m_project_name; ?>" disabled>
													<span id="divtxtpro_name" style="color:red"></span>
												</div>
											</div>
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Project Owner</label>
													<input type="text" placeholder="Enter Project Ownername" class="form-control input-sm empty" name="txtpro_owner" id="txtpro_owner" value="<?php echo $project->m_acc_owner;?>" disabled>
													<span id="divtxtpro_owner" style="color:red"></span>
												</div>
											</div>
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Project Id</label>
													<input type="text" placeholder="Project no"name="txtemp" id="txtemp" class="form-control input-sm empty" value="<?php echo $project->m_project_sno;?>" disabled>
													<span id="divtxtemp" style="color:red"></span>
												</div>
											</div>
											
											
										</div>
										
										<h4 class="caption-subject font-blue bold uppercase">Payment Details</h4>
										<?php
											if($this->uri->segment(4)=="")
											{
												$unit_price=$project->m_unit_price;
											}
											else
											{
												foreach($remain->result() as $rem)
												{
													$unit_price=$rem->m_due_amount;
													break;
												}
											}
										?>
										
										<div class="row">
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Remaining Amount</label>
													<input type="text" placeholder="Enter Remaining amount" class="form-control input-sm empty" name="txtrem_amt" id="txtrem_amt" value="<?php echo $unit_price; ?>" />
													<span id="divtxtrem_amt" style="color:red"></span>
												</div>
											</div>
											
											
											
											<?php
											}
										?>
										
										
										<?php
											if($this->uri->segment(4)!="")
											{
												foreach($installment->result() as $install)
												{
													$installment_amm=$install->m_amonut;
													break;
												}
											}
											else
											{
												$installment_amm="";
											}
										?>
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Payment Amount</label>
												<input type="text" placeholder="Enter payment amount" class="form-control input-sm empty" name="txtpay_amt" id="txtpay_amt" value="<?php echo $installment_amm; ?>" onblur="calculate1()" />
												<span id="divtxtpay_amt" style="color:red"></span>
											</div>
										</div>
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Select Bank</label>
												<Select id="bank" name="bank" class="form-control input-sm opt">
													<option value="-1">Select Bank</option>		
													<?php
														foreach($user_bank->result() as $rows)
														{
															foreach($bank->result() as $row)
															{
																if($rows->or_m_b_name==$row->m_bank_id)
																{
																?>	 
																<option value="<?php echo $rows->or_m_bid;?>"> <?php echo $row->m_bank_name.' - ['.$rows->or_m_b_cbsacno.'] '.'';?></option>   
																<?php
																}
															}
														}
													?>			 
												</select>
												<span id="divbank" style="color:red"></span>
											</div>
										</div>
										
									</div>
									
									<div class="row">
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Payment Mode</label>
												<Select id="mode" name="mode" class="form-control input-sm opt" onChange="get_payment()">
													<option value="-1">Select</option>
													<?php
														foreach($payment_mode->result() as $mode)
														{
														?>
														<option value="<?php echo $mode->m_pm_id; ?>"><?php echo $mode->m_pm_name; ?></option>
														<?php 
														}
													?>
												</select>
												<span id="divmode" style="color:red"></span>
											</div>
										</div>
										
										<div id="category" style="display:none">
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label" id="title"></label>
													<input type="text" placeholder="" id="detail" name="detail" class="form-control input-sm empty"/>
													<span id="divdetail" style="color:red"></span>	
												</div>
											</div>
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Transaction ID/Cheque/DD Number</label>
													<input type="text" placeholder="Your Transaction ID/Cheque/DD Number" id="transid" name="transid"  class="form-control input-sm empty" />
													<span id="divtransid" style="color:red"></span>	
												</div>
											</div>
										</div>
										
									</div>
									
									<div class="row">
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Payment Description</label>
												<textarea class="form-control input-sm empty" rows="3" name="txtdescription" id="txtdescription"></textarea>
												<span id="divtxtdescription" style="color:red"></span>
											</div>
										</div>
										
										<?php
											foreach($count->result() as $record)
											{
												if($record->co <= 1)
												{
													
												?>
												
												<div class="col-md-4">
													<div class="form-group">
														<label class="control-label">Generate EMI</label>
														<div class="radio-list">
															<label class="radio-inline">
															<input type="radio" name="optionsRadios" id="optionsRadios25" value="option1" checked="" onclick="get_form(1)"> YES</label>
															<label class="radio-inline">
															<input type="radio" name="optionsRadios" id="optionsRadios26" value="option2" onclick="get_form(0)" checked=""> NO </label>
														</div>
														<input type="hidden" name="txthd" id="txthd" />
													</div>
												</div>
												
												
											</div>
											
											<?php 
											}
										}
									?>
									
									<div id="genrate_emi" style="display:none;">
										
										<div class="row">
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">No. Of EMI</label>
													<input type="text" placeholder="No. Of EMI" class="form-control input-sm" name="txtno_emi" id="txtno_emi" onblur="calc()" />
													<span id="divtxtno_emi" style="color:red"></span>
												</div>
											</div>
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Per EMI Charge</label>
													<input type="text" placeholder="Per EMI Charge" class="form-control input-sm" name="txtemi_charge" id="txtemi_charge" readonly />
													<span id="divtxtemi_charge" style="color:red"></span>
												</div>
											</div>
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">EMI First Instalment Date</label>
													<input type="text" placeholder="EMI First Instalment Date" name="txtfst_date" id="txtfst_date" class="form-control input-sm " disabled />
													<span id="divtxtfst_date" style="color:red"></span>
												</div>
											</div>
											
										</div>
										
										<div class="row">
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">EMI Last Instalment Date</label>
													<input type="text" placeholder="EMI Last Instalment Date" class="form-control input-sm" name="txtlast_date" id="txtlast_date" disabled />
													<span id="divtxtlast_date" style="color:red"></span>
												</div>
											</div>
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Reminder days</label>
													<input type="text"placeholder="Reminder days" class="form-control input-sm" name="txtreminder_date" id="txtreminder_date" />
													<span id="divtxtreminder_date" style="color:red"></span>
												</div>
											</div>
											
											<script>
												function calc()
												{
													var rem1=$('#txthd').val();
													if(rem1=="")
													{
														var rem=0;
														var no_emi=$('#txtno_emi').val();
														var per_emi=parseFloat(rem)/no_emi;
														var peremi=parseFloat(per_emi).toFixed(2);
														$('#txtemi_charge').val(peremi);
														
														var now = new Date();
														var start = new Date();
														start.setFullYear(start.getFullYear(),start.getMonth()+1,start.getDate()-1);
														now.setFullYear(now.getFullYear(),now.getMonth()+parseInt(no_emi),now.getDate()-1);
														$('#txtfst_date').val(start.toLocaleDateString("ja-JP"));
														$('#txtlast_date').val(now.toLocaleDateString("ja-JP"));
													}
													else
													{
														var rem=rem1;
														var no_emi=$('#txtno_emi').val();
														var per_emi=parseFloat(rem)/no_emi;
														var peremi=parseFloat(per_emi).toFixed(2);
														$('#txtemi_charge').val(peremi);
														
														var now = new Date();
														var start = new Date();
														start.setFullYear(start.getFullYear(),start.getMonth()+1,start.getDate()-1);
														now.setFullYear(now.getFullYear(),now.getMonth()+parseInt(no_emi),now.getDate()-1);
														$('#txtfst_date').val(start.toLocaleDateString("ja-JP"));
														$('#txtlast_date').val(now.toLocaleDateString("ja-JP"));
													}
												}				
											</script>
											
										</div>
										
									</div>
									
								</div>
								
								<div id="function" class="form-actions left">
									<?php
										foreach($count->result() as $record)
										{
											if($record->co <= 1)
											{
												
											?>  
											<button type="submit" class="btn green"><i class="fa fa-check"></i>Collect Payment</button>
											<button type="submit" class="btn default">Cancel</button>
											<?php
											}
											else
											{
											?>
											<label class="btn green" onclick="update_pay(<?php echo $this->uri->segment(4); ?>)"><i class="fa fa-check"></i> Recieve Installment Payment</label>
											<?php
											}
										}
									?>
								</div>
							</form>
							<!-- END FORM--> 
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	

<script>
	fill_userid();
	function fill_userid()
	{
		var user_id=document.getElementById("txtpro_owner").value;
		if(user_id!="0")
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/master/validateUser/0/",
				data:"txtintuserid="+user_id,
				success: function(msg) {
					if(msg!="false")
					{
						document.getElementById("txtpro_owner").value=msg; 
					}
					else
					{
						document.getElementById("txtpro_owner").value=msg;                   
						alert('No User in this Id');
					}
				}
			}
			)
			
		}
		else
		{
			document.getElementById("txtpro_owner").value="SuperAdmin";                    
		}	
	}
</script>

<script>
	function get_payment()
	{
		var amt=document.getElementById("mode").value;
		
		$.ajax(
		{
			type:"POST",
			url:"<?php echo base_url() ;?>index.php/support/select_category/"+amt,
			dataType: "JSON",
			data:{'amt':amt},
			success: function(msg) {
				$("#category").css('display','block');
				
				$.each(msg,function(i,item)
				{
					$("#title").html(item.m_pm_name);
					$("#detail").attr('Placeholder',item.m_pm_name);
				});
			}
		});
	}
</script>
<script>
	function get_form(id)
	{
		if(id==1)
		{
			$("#genrate_emi").removeAttr( 'style' );
			var rem_amt=$("#txtrem_amt").val();
			var pay_amt1=$("#txtpay_amt").val();
			if(pay_amt1!="")
			{
				var pay_amt=pay_amt1;
			}
			else
			{
				var pay_amt=0;
			}
			
			var rem=parseFloat(rem_amt) - parseFloat(pay_amt);
			$('#txthd').val(rem);
		}
		if(id==0)
		{
			document.getElementById("genrate_emi").style.display='none';
		}
	}
</script>

<script>
	function calculate1()
	{
		var rem_amt=$("#txtrem_amt").val();
		var pay_amt1=$("#txtpay_amt").val();
		if(pay_amt1!="")
		{
			var pay_amt=pay_amt1;
		}
		else
		{
			var pay_amt=0;
		}
		var rem=parseFloat(rem_amt) - parseFloat(pay_amt);
		$('#txthd').val(rem);
		calc();
	}						
</script>


<script>
	function update_pay(id)
	{
		alert('dsfds');
		var bank = $('#bank').val();
		var mode= $('#mode').val();
		var detail = $('#detail').val();
		var transid = $('#transid').val();
		var txtdescription = $('#txtdescription').val();
		var txtpay_amt = $('#txtpay_amt').val();
		var txtrem_amt = $('#txtrem_amt').val();
		$.ajax(
		{
			type: "POST",
			url:"<?php echo base_url();?>index.php/support/update_pay/"+id,
			data:"bank="+bank+"&mode="+mode+"&detail="+detail+"&transid="+transid+"&txtdescription="+txtdescription+"&txtpay_amt="+txtpay_amt+"&txtrem_amt="+txtrem_amt,
			success: function(msg) {
				if(msg=="true")
				{
					$(".page-content").html("<center><h2>Payment Was Successfully Paid!</h2></center>")
					.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
					.hide()
					.fadeIn(1000,function()
					{
						window.location.href = "<?php echo base_url(); ?>index.php/support/project_cost/<?php echo $this->uri->segment(3) ?>";
					}
					);
					
				}	  
			}
		});
	}
</script>																																																								