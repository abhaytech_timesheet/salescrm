<!-- BEGIN CONTENT -->
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="<?php echo base_url();?>index.php/<?php echo $this->router->fetch_class();?>/dashboard/">HELP DESK </a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Help Desk </span>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> All Ticket </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-ticket font-dark" ></i>
								<span class="caption-subject bold uppercase">Ticket</span>
							</div>
							
							<div class="tools"> </div>
						</div>
						<div class="portlet-body" id="data1" style="overflow-x:auto">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<h5> &nbsp;</h5>
										</div>
									</div>
									<div class="col-md-6">
										
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								
								<thead>
									<tr>
										<th>
											S No.
										</th>
										<th>
											Department
										</th>
										<th>
											Subject
										</th>
										<th>
											Status
										</th>
										<th>
											Urgency
										</th>
										<th>
											Date
										</th>
										
									</tr>
								</thead>
								<tbody>
									<?php  
										$sn=1;
										foreach($rec->result() as $row)
										{
											if(($row->C_Status)!="DEACTIVE")
											{
												
											?>
											<tr>
												<td>
													<?php echo $sn; ?>
												</td>
												<td>
													<?php echo $row->Department ?>
												</td>
												<td>
													<a href="<?php echo base_url();?>index.php/<?php echo $this->router->fetch_class();?>/view_ticket/<?php echo $row->Ticket_reference_no ?>" style="color:#0066FF; text-decoration:none;">
														<?php echo '#'.($row->Ticket_reference_no).' - '.$row->Subject ?>
														<input type="hidden" name="hd<?php echo $row->Ticket_id; ?>" id="hd<?php echo $row->Ticket_id; ?>" value="<?php echo $row->Ticket_reference_no?>" />
													</a>
												</td>
												<td>
													<?php echo $row->C_Status; ?>
												</td>
												<td>
													<?php 
														echo $row->Urgency;
													?>
												</td>
												<td>
													<?php echo substr($row->Sub_date,0,10) ?>
												</td>
												
												
											</tr>
											
											
											<?php
												$sn++;
												
											}
											
										}
										
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->		

<script>
	function save(id)
	{ 
		
		if($('#ddemp_name'+id).val()!="-1")
		{
			var hidden = $('#hd'+id).val();
			var empname = $('#ddemp_name'+id).val();
			$.ajax(
			{
				type: "POST",
				url:"<?php echo base_url();?>index.php/help_desk/task_assign/",
				data:"&hd="+hidden+"&ddemp_name="+empname,
				success: function(msg){
					
					if(msg==1){
						$(".page-content").html("<center><h2>Ticket Assign Successfully!</h2></center>")
						.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/img/loading-spinner-blue.gif' /></center>")
						.hide()
						.fadeIn(1000,function()
						{
							
							$("#stylized").load("<?php echo base_url().'index.php/help_desk/after_index/-1'?>");
							
						}
						);
					}
					else
					{
						alert("Some Error on this page");
					}
				}
			});
		}
		else
		{
			alert('Please Select Employee!!');
			$('#ddemp_name'+id).focus();
		}
		
	}
	
	
</script>

<script>
	function display_ticket()
	{
		var dept=$('#txtdepartment').val();
		$('#stylized').html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/img/loading-spinner-blue.gif' /></center>");
		$('#stylized').load('<?php echo base_url();?>index.php/help_desk/after_index/'+dept);
		
	}
</script>	