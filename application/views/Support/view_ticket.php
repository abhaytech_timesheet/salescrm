<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="#">Support Panel</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Dashboard</span>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<a href="#">
							<?php
                              if($is_ticexist==1)
							{
								foreach($rec->result() as $row)
								{
									if(($row->Mail)!=null)
									{
										$tic=($row->Ticket_reference_no);
										break; 
									} 
								}
							}
							else
							{
								$tic="<span style='color:red'>".$this->uri->segment(3) ."---"."This Ticket is not exist</span>";
							}
							?>
							<strong><?php echo '#'.$tic; ?></strong>
						</a>
						
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">View Ticket </h3>
			<!-- END PAGE TITLE-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-ticket font-dark" ></i>
								<span class="caption-subject bold uppercase">View Ticket Detail</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body" >
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>
											Department
										</th>
										<th>
											Subject
										</th>
										<th>
											Status
										</th>
										<th>
											Urgency
										</th>
										<th>
											Date
										</th>
										
									</tr>
								</thead>
								<tbody>
									
									<?php  
										if($is_ticexist==1)
										{
										foreach($rec->result() as $row)
										{
										}
										if(($row->Mail)!=null)
										{
											
											
										?>
										
										<tr class="odd gradeX">
											<td>
												<?php echo $row->Department  ?>
											</td>
											<td>
												<?php echo $row->Subject ?>
											</td>
											<td>
												<?php echo $row->C_Status; ?>
											</td>
											<td>
												<?php 
													echo $row->Urgency;
												?>
											</td>
											<td>
												<?php echo $row->Sub_date; ?>
											</td>
										</tr>
										<?php
										} }
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<?php
if($is_ticexist==1)
										{
						if($row->C_Status !="DEACTIVE")
						{
						?>
						<center><form method="post" action="<?php echo base_url();?>index.php/support/resolve" name="resolved" id="resolved" >
							<input type="hidden" value="<?php echo $tic ?>" id="ticket_no" name="ticket_no" />
							<input type="hidden" value="0" id="txtstatus" name="txtstatus" />
							<input type="submit" name="submit" value="Click Here If Problem Is Resolved" class="btn green" style="width:300px;" />
						</form></center>
						<?php
						}}
					?>
				</div>
			</div>
			
			<div class="row">
				<br>
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-comments"></i>
								<span class="caption-subject bold uppercase">Message</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="scroller">
								<ul class="chats">
									<?php
if($is_ticexist==1)
										{
										foreach($rec1->result() as $reply_row)
										{
											if(($reply_row->Reply_by)!=null)
											{
												if(($reply_row->Reply_type)==1)
												{
													$rep=$reply_row->Reply_by;
													$type='out';
												}
												if(($reply_row->Reply_type)==2)
												{
													$rep=$reply_row->Reply_by;
													$type='out';
												}
												if(($reply_row->Reply_type)==3)
												{
													$rep=$reply_row->Reply_by;
													$type='in';
												}
												if(($reply_row->Reply_type)==4)
												{
													$rep=$reply_row->Reply_by;
													$type='in';
												}
											?>
											<li class="<?php echo $type; ?>">
												
												<div class="message">
													<span class="arrow">
													</span>
													<a href="#" class="name">
														<?php echo $rep; ?>
													</a>
													<span class="datetime">
														at <?php echo $reply_row->Reply_date ?>
													</span>
													<span class="body">
														<?php echo $reply_row->Reply_desc ?>
														<br />
														<a href="<?php echo base_url();?>application/uploadimage/<?php echo $reply_row->Reply_file ?>" style="color:#0066CC" target="_blank">
															<?php echo $reply_row->Reply_file ?>
														</a>
													</span>
												</div>
											</li>
											<?php 
											} 
										}}
									?>
								</ul>
							</div>
							
						</div>
					</div>
				</div>
				<?php
if($is_ticexist==1)
										{
					if(($row->C_Status)!="DEACTIVE")
					{
					?>
					<!--row-->
					<div class="col-md-6">
						<!-- BEGIN SAMPLE FORM PORTLET-->
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-cursor"></i>
									<span class="caption-subject bold uppercase">Reply From</span>
								</div>
								
								<div class="actions">
									<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
										<i class="icon-cloud-upload"></i>
									</a>
									<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
										<i class="icon-wrench"></i>
									</a>
									<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
									<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
										<i class="icon-trash"></i>
									</a>
								</div>
							</div>
							<div class="portlet-body form">
								<form role="form" class="form-horizontal" id="insert_data" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/support/affiliates_response"  method="post" >
									<div class="form-body">
										
										<div class="form-group">
											<label class="col-md-3 control-label">User</label>
											<div class="col-md-9">
												<input type="text" name="txtname" id="txtname" style="border:none;" value="<?php echo $this->session->userdata('name'); ?>" class="form-control input-large" readonly="readonly" />
	                                            <span id="divtxtemail" style="color:red;"></span>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Comment
												<span class="required">
													*
												</span>
											</label>
											<div class="col-md-9">
												<textarea rows="5" name="txtdiscription" id="txtdiscription" data-provide="markdown"  class="form-control input-sm empty"></textarea>
												<span id="divtxtdiscription" style="color:red;"></span>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Attachments</label>
											<div class="col-md-9">
												
												<div class="fileinput fileinput-new" data-provides="fileinput">
													<div class="input-group input-large">
														<div class="form-control uneditable-input span3" data-trigger="fileinput">
															<i class="fa fa-file fileinput-exists"></i>&nbsp;
															<span class="fileinput-filename">
															</span>
														</div>
														<span class="input-group-addon btn default btn-file">
															<span class="fileinput-new">
																Select file
															</span>
															<span class="fileinput-exists">
																Change
															</span>
															<input type="file" name="userfile" id="userfile"  accept=".gif,.jpg,.jpeg,.png,.pdf,.doc,.xlsx,.xml,.zip,.txt">
														</span>
														<a href="#" class="input-group-addon btn default fileinput-exists" data-dismiss="fileinput">
															Remove
														</a>
														<span id="divuserfile" style="color:red;"></span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-actions fluid">
										<div class="col-md-offset-3 col-md-9">
											<input type="hidden" value="<?php echo $row->Ticket_reference_no ?>" name="txtticket" id="txtticket" />
											<input type="hidden" value="<?php echo $row->Department ?>" name="txtdepartment" id="txtdepartment" />
											<input type="hidden" value="<?php echo $row->Subject ?>" name="txtsubject" id="txtsubject" />
											<input type="hidden" value="<?php echo $row->Urgency ?>" name="txturgency" id="txturgency" />
											<input type="hidden" value="<?php echo $row->Assign_to_id; ?>" name="txtassign" id="txtassign" />
											<button type="button" onclick="conwv('insert_data')" class="btn blue">Submit</button>
											<button type="reset" class="btn default">Cancel</button>
										</div>
									</div>
								</form>
							</div>
						</div>
						<!-- END SAMPLE FORM PORTLET-->
					</div>
					<?php  
						
					}}
				?>
				
			</div>
			
			
		</div>
		
	</div>
	<!-- END CONTENT BODY -->
</div>
</div>