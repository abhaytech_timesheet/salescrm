<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="#">PROJECT MODULE</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Project</span>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View All Project</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-link font-dark" ></i>
								<span class="caption-subject bold uppercase">View All Project</span>
							</div>
							<div class="actions">
								<div class="btn-group btn-group-devided" data-toggle="buttons">
									<a href="javascript:void(0)" >
										<button class="btn green btn-sm blue-stripe" onclick="gotopage()">Create Task on Project
										</button>
									</a>
									
								</div>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body" style="overflow-x: auto;">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<h5> &nbsp;</h5>
										</div>
									</div>
									<div class="col-md-6">
										
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								
								<thead>
									<tr>
										<th>S.No</th>
										<th>Project Name</th>
										<th>Project Leader</th>
										<th>Participant Name</th>
										<th>Project Start Date</th>
										<th>Project End Date</th>
										<th>Admin Rights</th>
										<th>Add Participants</th>
										<th>View Right</th>
										<th>Update Right</th>
										<th>Project Notification</th>
										<th>Task Notification</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$sn=1;
										foreach($rec->result() as $row)
										{
										?>
										<tr>
											<td><?php echo $row->SN; ?></td>
											<td align="center"><?php echo $row->Account_name.'<span style="color:red">/</span>'.$row->Website_name.'<span style="color:red">/</span>'.$row->PROJECT_NAME; ?></td>
											<td align="center"><?php echo strtoupper($row->PROJ_LEADER); ?></td>
											<td align="center"><?php echo strtoupper($row->USER_NAME); ?></td>
											<td align="center"><?php echo $row->PROJ_STARTDATE; ?></td>
											<td align="center"><?php echo $row->PROJ_ENDDATE; ?></td>
											<td align="center"><?php echo $row->admin_right; ?></td>
											<td align="center"><?php echo $row->parti_add_participant; ?></td>
											<td align="center"><?php echo $row->parti_view_right; ?></td>
											<td align="center"><?php echo $row->parti_update_right; ?></td>
											<td align="center"><?php echo $row->parti_project_noti; ?></td>
											<td align="center"><?php echo $row->parti_task_noti; ?></td>
											<?php 
												$sn++;
											}
										?>    
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->

<script>
	function gotopage()
	{
		$("#stylized").html("<center><h2>Create Task On Project will Loading!</h2></center>")
		.append("<center><i class='fa fa-cog fa-spin' style='font-size:100px; color:#d84a38; margin-top:30px'></i></center>");
		$("#stylized").load("<?php echo base_url();?>support/view_create_task/");
	}
</script>
