<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-archive"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_all_task/<?php echo $this->uri->segment(3);?>">PROJECT </a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-list"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_all_task/<?php echo $this->uri->segment(3);?>">View All Task  </a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-road"></i>
						<span> Add Followup  </span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View Task Followup</h3>
			<!-- END PAGE TITLE-->
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">View Task Followup</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="scroller" style="height: 435px; overflow-y: scroll auto;" data-always-visible="1" data-rail-visible1="0">
								<ul class="chats">
									<?php
										foreach($rec->result() as $row)
										{
											if($row->or_m_userimage!="")
											{
												$img=base_url()."application/uploadimage/".$row->or_m_userimage;
											}
											else
											{
												$img=base_url()."application/libraries/noimage.jpg";
											}
										?>
										<li class="<?php echo $row->msg; ?>">
											<img class="avatar img-responsive" alt="" src="<?php echo $img; ?>" width="45px" height="45px"/>
											<div class="message">
												<span class="arrow">
												</span>
												<a href="javascript:void(0);" class="name">
													<?php echo $row->or_m_name; ?>
												</a>
												<span class="datetime">
													at <?php echo $row->task_response_date1; ?>
												</span>
												<span class="body">
													<?php echo $row->task_description; ?>
												</span>
											</div>
										</li>
										<?php 
										} 
									?>
								</ul>
							</div>
							<?php
								if($taskstatus==1)
								{
								?>
								<div class="chat-form">
									<form id="insert_data" name="insert_data" method="post">
										<div class="input-cont">
											<input class="form-control empty" type="text" name="txtdescription" id="txtdescription" placeholder="Type a message here..."/>
											<input type="hidden" name="txttaskid" id="txttaskid" value="<?php echo $id; ?>" />
										</div>
										
										<div class="btn-cont">
											<span class="arrow">
											</span>
											<a href="javascript:void(0)" onclick="insert()" class="btn green icn-only">
												<i class="fa fa-check icon-white"></i>
											</a>
										</div>
										
									</form>
									
								</div>
								<?php
								}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<!-- END CONTENT BODY -->
</div>
</div>

<script>
	$("#txtdescription").keypress(TriggerSearch);
	
	function search_product() {
		insert()
	}
	
	function TriggerSearch(e) {
		var search_string = $("#txtdescription").val();
		e = e || window.event;
		var keycode;
		if (window.event) {
			keycode = e.which ? window.event.which : window.event.keyCode;
		}
		var key = e.which;
		switch (key) {
			case 38:
            break;
			case 40:
            break;
			case 13:
            e.preventDefault();
            search_product();
            break;
			default:
            break;
		}
	}
</script>

<script>
	function insert()
	{
		if(check('insert_data'))
		{
			formData=$("#insert_data").serialize();
			$.ajax(
			{
				type: "POST",
				url:"<?php echo base_url();?>project/insert_follow_up/",
				data:formData,
				success: function(msg)  {
					$(".page-content").html("<center><h2>Add Task Followup Successfully!</h2></center>")
					.append("<center><i class='fa fa-cog fa-spin' style='font-size:100px; color:#d84a38; margin-top:30px'></i></center>")
					.hide()
					.fadeIn(1000,function()
					{
						$("#stylized").load("<?php echo base_url(); ?>support/view_task_follow_up/"+msg);
					});
				}	
			});
		}
	}
	</script>					