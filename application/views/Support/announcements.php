<div id="stylized">
	<!-- BEGIN CONTAINER -->
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="#.">Sales Booster</a>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View Announcements </h3>
			<!-- END PAGE TITLE-->
			<div class="row">
				
				<div class="col-md-12">
					<div class="portlet light portlet-fit bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-microphone font-green"></i>
								<span class="caption-subject bold font-green uppercase"> Announcements</span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="timeline">
								<?php
									foreach($rec->result() as $row)
									{
										if(($row->id)!=null)
										{
										?>
										<!-- TIMELINE ITEM -->
										<div class="timeline-item">
											<div class="timeline-badge">
												<div class="timeline-icon">
													<i class="icon-user-following font-green-haze"></i>
												</div>
											</div>
											<div class="timeline-body">
												<div class="timeline-body-arrow"> </div>
												<div class="timeline-body-head">
													<div class="timeline-body-head-caption">
														<span class="timeline-body-alerttitle font-red-intense"><?php echo $row->txttitle ?></span>
														<span class="timeline-body-time font-grey-cascade">at <?php echo substr($row->adddate,0,10) ?> <?php echo substr($row->adddate,11,20) ?> </span>
													</div>
												</div>
												<div class="timeline-body-content">
													<span class="font-grey-cascade"> <?php echo substr($row->txtdescription,0,300) ?></span>
													<a href="<?php echo base_url();?>index.php/support/full_Announcements/<?php echo $row->id ?>" class="nav-link">
														Read more <i class="m-icon-swapright m-icon-white"></i>
													</a>
												</div>
											</div>
										</div>
										<!-- END TIMELINE ITEM -->
										<?php
										}
									}
								?>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
</div>

<script type="text/javascript">
	function MM_jumpMenu(targ,selObj,restore){ //v3.0
		eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
		if (restore) selObj.selectedIndex=0;
	}
</script>		