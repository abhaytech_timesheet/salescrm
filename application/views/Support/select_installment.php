			   <div class="form-group">
                  <label class="col-md-3 control-label">No. Of EMI</label>
                  <div class="col-md-9">
				    <input type="text" placeholder="No. Of EMI" class="form-control input-large" name="txtno_emi" id="txtno_emi" onblur="calc()" />
				  </div>
                </div>
			  
			  
			  
                <div class="form-group">
                  <label class="col-md-3 control-label">Per EMI Charge</label>
                  <div class="col-md-9">
                    <input type="text" placeholder="Per EMI Charge" class="form-control input-large" name="txtemi_charge" id="txtemi_charge" disabled />
                  </div>
                </div>
				
				
                <div class="form-group">
                  <label class="col-md-3 control-label">EMI First Instalment Date</label>
                  <div class="col-md-9">
                    <input type="text" placeholder="EMI First Instalment Date" name="txtfst_date" id="txtfst_date" class="form-control input-large" disabled />
                  </div>
                </div>
				
				
                <div class="form-group">
                  <label class="col-md-3 control-label">EMI Last Instalment Date</label>
                  <div class="col-md-9">
                    <input type="text" placeholder="EMI Last Instalment Date" class="form-control input-large" name="txtlast_date" id="txtlast_date" disabled />
                  </div>
                </div>
				
				
                <div class="form-group">
                  <label class="col-md-3 control-label">Reminder days</label>
                  <div class="col-md-9">
                    <input type="text"placeholder="Reminder days" class="form-control input-large" name="txtreminder_date" id="txtreminder_date" />
                  </div>
                </div>
				
<script>
function calc()
{
var rem=$('#txthd').val();
var no_emi=$('#txtno_emi').val();
var per_emi=parseFloat(rem)/no_emi;
var peremi=parseFloat(per_emi).toFixed(2);
$('#txtemi_charge').val(peremi);
	
	var now = new Date();
    var start = new Date();
	start.setFullYear(start.getFullYear(),start.getMonth()+1,start.getDate()-1);
	now.setFullYear(now.getFullYear(),now.getMonth()+parseInt(no_emi),now.getDate()-1);
	$('#txtfst_date').val(start.toLocaleDateString("ja-JP"));
	$('#txtlast_date').val(now.toLocaleDateString("ja-JP"));
}				
</script>