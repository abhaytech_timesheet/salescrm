<!-- BEGIN CONTAINER -->
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE BAR -->
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="#.">View Announcement</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Support</span>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">Announcement</h3>
			
			<!-- END PAGE TITLE-->
			<div class="row">
				
				<div class="col-md-12">
					<div class="portlet light portlet-fit bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-microphone font-green"></i>
								<span class="caption-subject bold font-green uppercase"> Full Announcement</span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="row">
								<div class="col-md-9 col-sm-8 article-block">
									<?php  
										
										foreach($rec->result() as $row)
										{
											if(($row->id)!=null)
											{
											?>
											<h3>
												<a href="#">
													<?php echo $row->txttitle ?>
												</a>
											</h3>
											<p>
												<i class="fa fa-calendar"></i>
												<a href="#">
													<?php echo $row->adddate ?>
												</a>
											</p>
											<p>
												<?php if($row->userfile!=""){ ?>
													<a href="<?php echo base_url();?>application/uploadimage/<?php echo $row->userfile ?>">
														<i class="fa fa-tags"></i>
														<?php echo $row->userfile ?>
													</a>
												<?php } ?>
											</p>
											<p align="justify">
												<?php echo $row->txtdescription ?>
											</p>
											
											<?php
											}
										}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
</div>
