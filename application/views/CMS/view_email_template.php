<?php
$temp="";
if($info!='')
{
	foreach($info->result() as $row123)
	{
		$temp=$row123->m_email_temp_description;
	}
}
?>
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Email Template
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								
							</div>
						</li>
						
						<li>
							<i class="fa fa-home"></i>
							<a href="#">
								Home
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Multiple Template for Email
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<div class="row inbox">
				<div class="col-md-2">
					<ul class="inbox-nav margin-bottom-10">
						
						<li class="sent active" id="mail">
							<a class="btn" href="<?php echo base_url() ?>index.php/cms/view_email_template" data-title="Sent">
								 Compose Mail
							</a>
							<b></b>
						</li>
					</ul>
				
                    <h3>Categories</h3>
                   <ul class="inbox-nav margin-bottom-10">
                   <?php 
				   foreach($cate->result() as $row)
				   	{
				    ?>
						<li class="<?php echo $row->m_email_name; ?>" id="inbox<?php echo $row->m_email_id; ?>">
							<a href="javascript:void(0)" class="btn"  onclick="follow('<?php echo $row->m_email_id; ?>');" data-title="<?php echo $row->m_email_name; ?>">
								 <?php echo $row->m_email_name; ?>
							</a>
							<b></b>
						</li>
                    <?php 
					}
					?>
					</ul>
				</div>
				<div class="col-md-10">
					<div class="inbox-header">
						<h1 class="pull-left">Compose</h1>
					</div>
					
					<div class="inbox-content" id="content_email">
                       <form class="inbox-compose form-horizontal" id="fileupload" action="#" method="POST" enctype="multipart/form-data">
                            <div class="inbox-compose-btn">
                                
                                <span class="btn green" onclick="send_mail()"><span class="fa fa-mail-forward"></span>Send</span>
                                <span class="btn yellow" onclick="get_details()">Get Email Id</span>
                                <a href="<?php echo base_url() ?>index.php/cms/view_email_template">
                                	<span class="btn red">Discard</span>
                                </a>
                            </div>
                            <div class="inbox-form-group mail-to">
                                <label class="control-label">Source:</label>
                                <div class="controls controls-to">
                                    <select id="ddsource" name="ddsource" class="form-control opt" onchange="enable()">
											<option value="-1" selected="selected">Select</option>
                                            <option value="1">Lead</option>
											<option value="2">Contact</option>
									</select>
                                    <span id="divddsource" class="help-inline" style="color:red"></span>
                                </div>
                            </div>
                            <div class="inbox-form-group mail-to">
                                <label class="control-label">Category:</label>
                                <div class="controls controls-to">
                                    <select id="ddcategory" name="ddcategory" class="form-control" disabled="disabled">
                                            <option value="1">Account</option>
											<option value="2">Opportunity</option>
									</select> 
                                    
                                </div>
                            </div>
                            <div class="inbox-form-group mail-to">
                                <label class="control-label">To:</label>
                                <div class="controls">
                                <div id="contact">
                                    <input type="text" class="form-control" name="txtto" id="txtto">
                                </div>
                                </div>
                            </div>
                            <div class="inbox-form-group">
                                <label class="control-label">From:</label>
                                <div class="controls">
                                	<div>
                                    	<input type="text" class="form-control" name="txtfrom" id="txtfrom">
                                    </div>
                                </div>
                            </div>
                            <div class="inbox-form-group">
                                <label class="control-label">Subject:</label>
                                <div class="controls">
                                	<div>
                                    	<input type="text" class="form-control" name="txtsubject" id="txtsubject">
                                    </div>
                                </div>
                            </div>
                            <div class="inbox-form-group">
                                <textarea class="ckeditor form-control" name="txtdesciption" id="txtdesciption" rows="12"><?php if($temp!=""){echo $temp;} ?></textarea>
                            </div>
                            
                            
                        
                            <div class="inbox-compose-btn">
                                <span class="btn green" onclick="send_mail()"><span class="fa fa-mail-forward"></span>Send</span>
                                <span class="btn yellow" onclick="get_details()">Get Email Id</span>
                                <a href="<?php echo base_url() ?>index.php/cms/view_email_template">
                                	<span class="btn red">Discard</span>
                                </a>
                            </div>
                        </form>
                        

					</div>
				</div>
			</div>			
            </div>
</div>
<script>
function follow(id)
{	
	
	
	for(i=0; i<8; i++)
	{
	$("#inbox"+i).removeAttr("class","active");
	}
	$("#mail").removeAttr("class","active");
	$("#inbox"+id).attr("class","active");
	var flg=id;
	$("#content_email").html('<div><img src ="<?php echo base_url();?>application/libraries/assets/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
	$('#content_email').load("<?php echo base_url();?>index.php/cms/email_template_display/"+flg);
}
</script>


<script>
function enable()
{
	
	var sour=$('#ddsource').val();
	var cate=$('#ddcategory').val();
	if(sour==2)
	{
		$('#ddcategory').removeAttr("disabled");	
	}
	else
	{
		$('#ddcategory').attr( "disabled", "disabled" );
	}
	
}
</script>
<script>
function get_details()
{
	if(check())
	{
	
	var sour=$('#ddsource').val();
	var cate=$('#ddcategory').val();
		$("#contact").html('<div><img src ="<?php echo base_url();?>application/libraries/assets/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
		$("#contact").load("<?php echo base_url() ?>index.php/cms/get_details_email/"+sour+"/"+cate);
	}
	
}
</script>


<script>
function send_mail()
{
	alert('hello');
	var from=$('#txtfrom').val();
	var to=$('#txtto').val();
	var subject=$('#txtsubject').val();
	var description=$('#txtdescription').val();
	$.ajax(
        {
 	type: "POST",
 	url:"<?php echo base_url();?>index.php/cms/send_template_mail/",
 	data:"txtfrom="+from+"&txtto="+to+"&txtsubject="+subject+"&txtdescription="+description,
 	success: function(msg) {
 	      if(msg="true")
		    {
			$(".page-content").html("<center><h2>Template Send Successfully!</h2></center>")
			.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/img/loading-spinner-blue.gif' /></center>")
			.hide()
			.fadeIn(1000,function()
			{
			$("#stylized").load("<?php echo base_url().'index.php/cms/after_view_email_template'?>");
			}
			);

	}	  
         }
       });
	   
}
</script>



<script>

function check()
{
var collection=$("#stylized");
var mark=0;
var obtainmark=0;
var vali=false;
var inputs=collection.find("input[type=text],select,textarea");
for(var x=0;x<inputs.length;x++)
{
var id=inputs[x].id;
var name=inputs[x].name;
var value=$("#"+id+"").val();
var type=inputs[x].type;
if($("#"+id+"").attr('class')=="form-control aplha_only" || $("#"+id+"").attr('class')=="form-control input-medium input-inline aplha_only")
{
if((type=="text" || type=="textarea") && value!="" && value!=0 )
{
  var pattern=/^[a-zA-Z ]*$/;
  if($("#"+id+"").val().match(pattern))  
        {  
		$("#div"+id+"").html('');
        }  
        else
        {
		$("#"+id+"").focus();
		$("#div"+id+"").html("*Only alpha characters and space allowed. You have entered an invalid input in this field!");
        return false;  
        }
 }
 else
 {
 $("#"+id+"").focus();
 $("#div"+id+"").html("This Feild is required.You can't leave this empty");
 return false;
 }
 }
 if($("#"+id+"").attr('class')=="form-control alpha_numeric" || $("#"+id+"").attr('class')=="form-control input-medium input-inline alpha_numeric")
{
if((type=="text" || type=="textarea") && value!="" && value!=0 )
{
  var pattern=/^[a-zA-Z0-9 ]*$/;
  if($("#"+id+"").val().match(pattern))  
        {  
		$("#div"+id+"").html('');
        }  
        else
        {
		$("#"+id+"").focus();
		$("#div"+id+"").html("*Only alpha numeric characters and space allowed. You have entered an invalid input in this field!");
        return false;  
        }
 }
 else
 {
 $("#"+id+"").focus();
 $("#div"+id+"").html("This Feild is required.You can't leave this empty");
 return false;
 }
 }
 if($("#"+id+"").attr('name')=="txtmail")
 {
 if(type=="text" && value!="" && value!=0 )
 {
 var pattern=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
 if($("#"+id+"").val().match(pattern))  
     {  
	 $("#div"+id+"").html('');
     }  
     else
     {
	  $("#"+id+"").focus();
	 $("#div"+id+"").html("*You have entered an invalid Email!,Please Fill Email in this format xyz@domain.com");
      return false;  
     } 
	 }
 else
 {
 $("#"+id+"").focus();
$("#div"+id+"").html("This Feild is required.You can't leave this empty");
return false;
 }

 }
 if($("#"+id+"").attr('name')=="txtcontact")
 {
 if(type=="text" && value!="" && value!=0 )
 {
   var numbers = /^[0-9]+$/;  
   if($("#"+id+"").val().match(numbers))  
   {   
   var val=$("#"+id+"").val();
   if(val.charAt(0)!="0")
   {
   $("#"+id+"").html('');
   if(val.length == 10)
   {
   $("#div"+id+"").html(''); 
   }
   else
   {
   $("#"+id+"").focus();
   $("#div"+id+"").html("Mobile Phone must be 10 Digit numeric no!. ");
   return false;
   }
   }
   else
   {
    $("#"+id+"").focus();
    $("#div"+id+"").html("Mobile Phone should not start with zero!. ");

  return false;
   }
}   
   else  
   { 
   $("#"+id+"").focus();
   $("#div"+id+"").html("Mobile Phone must be numeric!.This Field contain only numbers. ");   
   return false;  
   }
}
 else
 {
 $("#"+id+"").focus();
$("#div"+id+"").html("This Feild is required.You can't leave this empty");
return false;
 }   
 }
 
 if($("#"+id+"").attr('class')=="form-control input-small input-inline opt"||$("#"+id+"").attr('class')=="form-control opt")
 {

if($("#"+id+"").val() == -1 || $("#"+id+"").val()=="" || $("#"+id+"").val()==0)
{
$("#"+id+"").focus();
$("#div"+id+"").html("This Feild is required.Please Select");
return false;
}
 }
 $("#div"+id+"").html('');
 vali=true;

}
return vali;
}

</script>