
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Event Images
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								
							</div>
						</li>
						
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">
								Home
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Event Image
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<div class="row inbox">
				
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Home Tab 1 Content
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
                     <div class="portlet-body form">
                    	<div class="custom-file-upload">
							<!--<?php echo $error;?>

							-->
                            <form name="image_form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/event_upload/do_upload/<?php echo $id ?>" method="post">
                            	<div class="fileinput fileinput-new" data-provides="fileinput">
												<span class="btn default btn-file">
													<span class="fileinput-new">
														 Select file
													</span>
													<span class="fileinput-exists">
														 Change
													</span>
													 <input type="file" multiple name="userfile[]" size="20" />
												</span>
												<span class="fileinput-filename">
												</span>
												 &nbsp;
												<a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">
												</a>
								</div>
								<input type="hidden" value="<?php echo $aid ?>" name="aid" />
                            <input type="submit" class="btn blue" value="Upload" />
                            
                            </form>
                          </div>
					  </div>
					</div>
					
					<?php foreach($content->result() as $row)
								{
								?>
					<div class="col-sm-12 col-md-2">
									<div class="thumbnail">
								<img src="<?php echo base_url();?>application/libraries/event_Images/<?php echo $row->image_name; ?>" alt="" style="width:100%; height: 100px;">
										<div class="caption">
											<h3><?php echo $row->image_name; ?></h3>
											<a href="#" onclick="del('<?php echo $row->image_id; ?>','<?php echo $row->image_name; ?>')" class="btn red">
													 Delete
												</a>
											</div>
									</div>
								</div>
                                 <?php } ?>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
					</div>
				</div>
			</div>
		</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<script>
function del(id,image_name)
{
	//alert("sdf");
	var image = id;
	var img_name = image_name;
	$.ajax(
	{
	type:"POST",
	url: "<?php echo base_url(); ?>index.php/cms/delete_event_images/",
	data: "id="+image+"&imge="+img_name,
	success: function(msg) {
 	location.reload();
	alert(msg);
         }
		  
	});
	
}
</script>