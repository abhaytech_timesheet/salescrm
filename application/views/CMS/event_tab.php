	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Add Events
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								
							</div>
						</li>
						
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">
								Home
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Add Your Events
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<div class="row">
				
				<div class="col-md-6">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Add Event Content
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
                       
						<div class="portlet-body form">
							<div class="form-body">
                       			<div class="form-group">
										<label>Title</label>
										<input type="text" class="form-control input-large" id="txttitle" name="txttitle" placeholder="Enter text"/>
										
									</div>
                                    
                                    <div class="form-group">
										<label>Date of Event</label>
                                        <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
												<input type="text" class="form-control" id="event_date" name="event_date" readonly>
												<span class="input-group-btn">
													<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
                                        </div>
                                    
                                    <div class="form-group">
										
										<div >
											<textarea class="ckeditor form-control" name="editor1"  id="editor1" rows="4"></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Status</label>
										<div class="col-md-4">
											<select class="bs-select form-control" name="status" id="status">
												<option value="-1">Select</option>
												<option value="1">Active</option>
												<option value="0">Inactive</option>
											</select>
										</div>
									</div>
								<div style="clear:both"></div>
								<div class="form-actions">
										<button  onclick="event_insert()" class="btn green"><i class="fa fa-check"></i> Insert</button>								
								</div>
							</div></div></div></div>
					
                    <!-- END SAMPLE FORM PORTLET-->
					
                	<div class="col-md-6">
                	<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>View And Modify Event Data
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
								<th>
									Event Title
								</th>
								<th>
									 Event Date
								</th>
								<th>
									 Status
								</th>
                                <th>
									 Description
								</th>
                                 <th>
									Images
								</th>
                                 <th>
									Edit
								</th>
                                 <th>
									 Delete
								</th>
							</tr>
							</thead>
							<tbody>
                            <?php foreach($content->result() as $row)
								{
								?>
							<tr class="odd gradeX">
								
								<td>
									<?php echo $row->event_name; ?>
								<td>
									 <?php echo $row->event_date; ?>
                                </td>
                               <td>
                                     <?php if ($row->status=='1')
									 { ?>
								
									<span class="label label-sm label-success"></span>
										<?php echo "Active"; ?>
                                     <?php } 
									 else
									 { ?>
                                     <span class="btn btn-xs yellow"></span>
                                     <?php echo "Inactive"; }?>
								</td>
                                <td>
                					<?php echo $row->event_description; ?>
                                </td>
                                 <td>
                                 <a href="javascript:void(0)" title="Upload Images" class="btn blue" onclick="event_image('<?php echo $row->event_id; ?>')"><span class="glyphicon glyphicon-picture"></span> </a>
                                </td>
                                 <td>
								<a href="javascript:void(0)" title="Edit Your Event Details" class="btn purple" onclick="eve_edit('<?php echo $row->event_id; ?>')"><i class="fa fa-edit"></i></a>
                                </td>
                                 <td>
                                 <a href="javascript:void(0)" title="Delete Event" class="btn red" onclick="del('<?php echo $row->event_id; ?>')"><i class="fa fa-times"></i></a>
                                </td>
							</tr>
							<?php } ?>
							</tr>
							</tbody>
							</table>
						</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
</div>
<script type="text/javascript">
function loadUrl(newLocation)
{
  window.location = newLocation;
  return false;
}
</script>
<script>
function event_insert()
{
	var title = $('#txttitle').val();
	var status = $('#status').val();
	var date = $('#event_date').val();
	var editor = CKEDITOR.instances.editor1;
	//alert(status);
	//alert( editor.getData() );
	
	var content=editor.getData();
 	$.ajax(
        {
 	type: "POST",
 	url:"<?php echo base_url(); ?>index.php/cms/event_insert/",
 	data: "title="+title+"&date="+date+"&status="+status+"&content="+content,
 	success: function(msg) {
 	     alert(msg);
		 location.reload();
         }
 
       });
	
}
</script>

<script>
function del(id)
{
	var eve_id = id;
	$.ajax(
	{
	type:"POST",
	url: "<?php echo base_url(); ?>index.php/cms/delete_event/",
	data: "id="+eve_id,
	success: function(msg) {
 	location.reload();
	alert(msg);
         }
		  
	});
	
}
</script>

<script>
function eve_edit(id)
{
	var eve_id = id;
	loadUrl("<?php echo base_url();?>index.php/cms/edit_event/"+id);
}
</script>
<script>
function event_image(id)
{
	var eve_id = id;
	loadUrl("<?php echo base_url();?>index.php/cms/event_image/"+id);
}
</script>
