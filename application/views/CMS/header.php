<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8"/>
<title>DEMO MASTER</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>application/libraries/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>application/libraries/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/typeahead/typeahead.css">
<link href="<?php echo base_url() ?>application/libraries/assets/css/style-metronic.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>application/libraries/assets/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>application/libraries/assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>application/libraries/assets/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>application/libraries/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?php echo base_url() ?>application/libraries/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-datepicker/css/datepicker.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/select2/select2-metronic.css"/>
<link rel="stylesheet" href="<?php echo base_url() ?>application/libraries/assets/plugins/data-tables/DT_bootstrap.css"/>
<link href="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css" rel="stylesheet"/>
<link href="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet"/>
<link href="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet"/>
<!-- END PAGE LEVEL STYLES -->
<link href="<?php echo base_url() ?>application/libraries/assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet"/>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>

<!-- END PAGE LEVEL STYLES -->
</head>
<!-- END HEAD -->