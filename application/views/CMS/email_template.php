
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Email Template
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								
							</div>
						</li>
						
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">
								Home
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Multiple Template for Email
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<!-- BEGIN PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Add Email Template
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<!--BEGIN TABS-->
							<div class="tabbable tabbable-custom">
							
                          	<div id="content_email">
								<div id="content_email_temp">
                                <div data-always-visible="1" data-rail-visible="0">	
                                
                                	<div class="form-group">                                     
                                            <label>Subject of Email</label>
                                            <input type="text" class="form-control input-large" id="txttitle" name="txttitle" placeholder="Enter text"/>
                                    </div>
                                    <div class="form-group">
										<label>Category Of Email</label>
											<select class="form-control input-large" name="category" id="category">
												<option value="-1">Select</option>
												<?php
												foreach($cate->result() as $row)
												{
												?>
                                                <option value="<?php echo $row->m_email_id; ?>"><?php echo $row->m_email_name; ?></option>
                                                <?php
												}
												?>
											</select>
                                      </div>
                                    <div style="clear:both"></div>                                  
                                    <div class="form-group">
										<div>
											<textarea class="ckeditor form-control" name="editor1"  id="editor1" rows="6"></textarea>
										</div>
									</div>
                             		<div class="form-actions">
                                        <button  onclick="insert()" class="btn green"><i class="fa fa-check"></i> Save</button>								
                                    </div>
									</div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
			<div class="row">
				<div class="col-md-6">
                	<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>View Email Template
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
                            	<th width="33">
									 #
								</th>
								<th width="126">
									Email Title
								</th>
                                <th width="126">
									Email Description
								</th>
                                 <th width="31">
									Edit
								</th>
                                 <th width="49">
									 Delete
								</th>
							</tr>
							</thead>
							<tbody>
                            <?php
							$serial = 1;
							 foreach($content->result() as $row)
							{
								?>
							<tr>
								
                                <td><?php echo $serial; ?></td>
                               	<td><?php echo $row->m_email_temp_title ?></td>
                                <td><?php echo substr($row->m_email_temp_description,0,20) ?></td>
                                 <td>
								<a href="javascript:void(0)" title="Edit Your Event Details" onclick="email_edit('<?php echo $row->m_email_temp_id; ?>')" class="btn btn-xs purple"><i class="fa fa-edit"></i> Edit</a>
                                 </td>
                                 <td>
                                 <a href="javascript:void(0)" title="Delete Event" onclick="del('<?php echo $row->m_email_temp_id; ?>')" class="btn btn-xs red"><i class="fa fa-times"></i> Delete</a>
                                 </td>
							</tr>
                            <?php $serial++; } ?>
							</tbody>
							</table>
						</div>
					</div>
			</div>
           </div>
                       
                          
                        </div>
                      </div>
                   </div>
                 </div>
              </div>
<script>
function insert()
{
	var title = $('#txttitle').val();
	var editor = CKEDITOR.instances.editor1;
	var content=editor.getData();
	var category =$('#category').val();
	var type = $('input:radio[name=opttype]:checked').val();
 	$.ajax(
        {
 	type: "POST",
 	url:"<?php echo base_url(); ?>index.php/cms/insert_email_temp/",
 	data: "title="+title+"&content="+content+"&category="+category+"&opttyp="+type,
 	success: function(msg) {
 	     alert(msg);
		 location.reload();
         }
 
       });
}
</script>

<script>
function del(id)
{
	var email_id = id;
	$.ajax(
	{
	type:"POST",
	url: "<?php echo base_url(); ?>index.php/cms/delete_email/",
	data: "id="+email_id,
	success: function(msg) {
 	location.reload();
	alert(msg);
         }
		  
	});
	
}
</script>

<script type="text/javascript">
function loadUrl(newLocation)
{
  window.location = newLocation;
  return false;
}
</script>

<script>
function email_edit(id)
{
	var eve_id = id;
	loadUrl("<?php echo base_url();?>index.php/cms/edit_email/"+id);
}
</script>