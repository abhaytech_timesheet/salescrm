 						<?php 
							
								foreach($content->result() as $row)
								{
									break;
									} 
                                ?>
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?php echo $row->widget_title; ?>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								
							</div>
						</li>
						
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">
								Home
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
							<?php echo $row->widget_title; ?>
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<div class="row inbox">
				
				<div class="col-md-6 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> <?php echo $row->widget_title; ?> Content
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
                       
						<div class="portlet-body form">
							<div class="form-body">
                       			<div class="form-group">
										<label>Title</label>
										<input type="text" class="form-control input-large" id="txttitle" name="txttitle" placeholder="Enter text" value="<?php  echo $row->widget_title; ?>" />
										
									</div>
                                    
                                    <div class="form-group">
										
										<div >
											<textarea class="ckeditor form-control" name="editor1"  id="editor1" rows="6"><?php  echo $row->widget_content; ?></textarea>
										</div>
									</div>
									
								
								<div class="form-actions">
								
									<button type="button" onclick="back_tab()" name="back" class="btn default"><i class="fa fa-angle-left"></i> Back</button>
									<button  onclick="update()" class="btn green"><i class="fa fa-check"></i> Save</button>
									<button  onclick="next_update()" class="btn green"><i class="fa fa-check-circle"></i> Save & Continue Edit</button>									
								</div>
							</div>
					</div>
                    
                    <input type="hidden" id="hidden" name="hidden" value="<?php echo $id; ?>" />
					<!-- END SAMPLE FORM PORTLET-->
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<!-- BEGIN PORTLET-->
					<div class="portlet paddingless">
						<div class="portlet-title line">
							<div class="caption">
								<i class="fa fa-bell-o"></i>Feeds
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<!--BEGIN TABS-->
							<div class="tabbable tabbable-custom">
								<ul class="nav nav-tabs">
									<li class="active">
										<a href="#tab_1_1" data-toggle="tab">
											 Template 1
										</a>
									</li>
									<li>
										<a href="#tab_1_2" data-toggle="tab">
											 Template 2
										</a>
									</li>
									
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="tab_1_1">
										<div class="scroller" style="height: 290px;" data-always-visible="1" data-rail-visible="0">
											<iframe src="<?php echo base_url(); ?>index.php/cms/template1" width="100%" height="300px" scrolling="auto" frameborder="0"></iframe>
										</div>
									</div>
									<div class="tab-pane" id="tab_1_2">
										<div class="scroller" style="height: 290px;" data-always-visible="1" data-rail-visible1="1">
											<iframe src="<?php echo base_url(); ?>index.php/cms/template2" width="100%" height="300px" scrolling="auto" frameborder="0"></iframe>
										</div>
									</div>
				`			</div>
					</div>
			</div></div>
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
</div>

<script>
function update()
{
	var title = $('#txttitle').val();
	var editor = CKEDITOR.instances.editor1;
	//alert( editor.getData() );
	var content=editor.getData();
	var flg=$('#hidden').val();
 	$.ajax(
        {
 	type: "POST",
 	url:"<?php echo base_url(); ?>index.php/cms/update_home_tab/",
 	data: "title="+title+"&content="+content+"&flg="+flg,
 	success: function(msg) {
 	      alert(msg);
         }
 
       });
	
}
</script>
<script type="text/javascript">
function loadUrl(newLocation)
{
  window.location = newLocation;
  return false;
}
</script>

<script>
function next_update()
{

	update();
	var flg=$('#hidden').val();
	if(flg!=5)
	{
	flg++;
	}
	loadUrl("<?php echo base_url();?>index.php/cms/home_tab_id/"+flg);
}
</script>

<script>

function back_tab()
{
	var flg=$('#hidden').val();
	if(flg!=1)
	{
	flg--;
	}
	loadUrl("<?php echo base_url();?>index.php/cms/home_tab_id/"+flg);
}
</script>