	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Edit Your Events
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								
							</div>
						</li>
						
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">
								Home
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Edit Your Events
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<div class="row inbox">
				
				<div class="col-md-6 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Edit Event Content
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
                       
						<div class="portlet-body form">
							<div class="form-body">
                       			<div class="form-group">
                                <?php foreach($content->result() as $row)
								{
									?>
        <label>Title</label>
        <input type="text" class="form-control input-large" id="txttitle" name="txttitle" value="<?php echo $row->event_name; ?>" placeholder="Enter text"/>
										
									</div>
                                    
                                    <div class="form-group">
    <label>Date of Event</label>
    <input type="text" class="form-control input-large" id="event_date" name="event_date" value="<?php echo $row->event_date; ?>" placeholder="Enter text"/>
										
									</div>
                                    
                                    <div class="form-group">
										
										<div >
		<textarea class="ckeditor form-control" name="editor1"  id="editor1" rows="6"><?php echo $row->event_description; ?></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Status</label>
										<div class="col-md-4">
											<select class="bs-select form-control" name="status" id="status">
												<option value="-1">Select</option>
												<option value="1">Active</option>
												<option value="0">Inactive</option>
											</select>
										</div>
									</div>
                                    <input type="text" id="hidden" value="<?php echo $id; ?>"  />
                                    <?php } ?>
								<div style="clear:both"></div>
								<div class="form-actions">
										<button  onclick="event_update()" class="btn green"><i class="fa fa-check"></i> Update Event</button>								
								</div>
							</div></div></div></div>
								<div class="col-md-6 col-sm-6">
					<!-- BEGIN PORTLET-->
					<div class="portlet paddingless">
						<div class="portlet-title line">
							<div class="caption">
								<i class="fa fa-bell-o"></i>Feeds
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
					<div class="portlet-body">
							<!--BEGIN TABS-->
							<div class="col-md-12 col-sm-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
							<tr>
								<th>
									Event Title
								</th>
								<th>
									 Event Date
								</th>
								<th>
									 Status
								</th>
                                <th>
									 Description
								</th>
                                
							</tr>
							</thead>
							<tbody>
                            <?php foreach($content->result() as $row)
								{
								?>
							<tr class="odd gradeX">
								
								<td>
									<?php echo $row->event_name; ?>
								<td>
									 <?php echo $row->event_date; ?>
                                </td>
                               <td>
                                     <?php if ($row->status=='1')
									 { ?>
								
									<span class="label label-sm label-success">
										<?php echo "Active"; ?></span>
                                     <?php } 
									 else
									 { ?>
                                     <span class="btn btn-xs yellow">
                                     <?php echo "Inactive"; }?>
								</td>
                                <td>
                					<?php echo $row->event_description; ?>
                                </td>
                                
							</tr>
							<?php } ?>
							</tr>
							</tbody>
							</table>
						</div>
					</div>
			</div>
             </div>       <!-- END SAMPLE FORM PORTLET-->
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
</div>
<script type="text/javascript">
function loadUrl(newLocation)
{
  window.location = newLocation;
  return false;
}
</script>
<script>
function event_update()
{
	var title = $('#txttitle').val();
	var status = $('#status').val();
	var date = $('#event_date').val();
	var id = $('#hidden').val();
	var editor = CKEDITOR.instances.editor1;
	var content=editor.getData();
 	$.ajax(
	
        {
 	type: "POST",
 	url:"<?php echo base_url(); ?>index.php/cms/update_event/",
 	data: "title="+title+"&date="+date+"&status="+status+"&content="+content+"&id="+id,
 	success: function(msg) {
 	     alert(msg);
		 loadUrl("<?php echo base_url();?>index.php/cms/event_tab/");
         }
 
       });
}
</script>
