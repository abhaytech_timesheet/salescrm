<?php

class Admin extends CI_Controller {

	public function index()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('admin/index');
		$this->load->view('admin/footer');
	}
	
	public function home_tab_id()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$aid=0;
		$data['id']=$this->uri->segment(3);
		$this->db->where('affiliate_id',$aid);
		$this->db->where('widget_index',$this->uri->segment(3));
		 $this->db->limit(5);
		$data['content']=$this->db->get('manage_widgets');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('admin/home_tab',$data);
		$this->load->view('admin/footer');
	}
	public function update_home_tab()
   {
		$this->load->library('session');
		$this->load->database();
		$this->load->helper('url');
		$aid=0;
		$data=array
		(
			'widget_title'=>$this->input->post('title'),
			'widget_content'=>$this->input->post('content')
		);
		$flg=$this->input->post('flg');
		$this->db->where('widget_index',$flg);
		$this->db->where('affiliate_id',$aid);
		$this->db->update('manage_widgets',$data);
		echo "Widget Update Successfully";
    }
	
	public function left_tab_id()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$aid=0;
		$data['id']=$this->uri->segment(3);
		$this->db->where('affiliate_id',$aid);
		$this->db->where('widget_index',$this->uri->segment(3));
		 $this->db->limit(2);
		$data['content']=$this->db->get('manage_widgets');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('admin/left_tab',$data);
		$this->load->view('admin/footer');
	}
	public function update_left_tab()
   {
		$this->load->library('session');
		$this->load->database();
		$this->load->helper('url');
		$aid=0;
		$data=array
		(
			'widget_title'=>$this->input->post('title'),
			'widget_content'=>$this->input->post('content')
		);
		$flg=$this->input->post('flg');
		$this->db->where('widget_index',$flg);
		$this->db->where('affiliate_id',$aid);
		$this->db->update('manage_widgets',$data);
		echo "Widget Update Successfully";
    }
	
	public function right_tab_id()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$aid=0;
		$data['id']=$this->uri->segment(3);
		$this->db->where('affiliate_id',$aid);
		$this->db->where('widget_index',$this->uri->segment(3));
		 $this->db->limit(3);
		$data['content']=$this->db->get('manage_widgets');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('admin/right_tab',$data);
		$this->load->view('admin/footer');
	}
	public function update_right_tab()
   {
		$this->load->library('session');
		$this->load->database();
		$this->load->helper('url');
		$aid=0;
		$data=array
		(
			'widget_title'=>$this->input->post('title'),
			'widget_content'=>$this->input->post('content')
		);
		$flg=$this->input->post('flg');
		$this->db->where('widget_index',$flg);
		$this->db->where('affiliate_id',$aid);
		$this->db->update('manage_widgets',$data);
		echo "Widget Update Successfully";
    }
	
	public function center_tab_id()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$aid=0;
		$data['id']=$this->uri->segment(3);
		$this->db->where('affiliate_id',$aid);
		$this->db->where('widget_index',$this->uri->segment(3));
		 $this->db->limit(4);
		$data['content']=$this->db->get('manage_widgets');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('admin/center_tab',$data);
		$this->load->view('admin/footer');
	}
	public function update_center_tab()
   {
		$this->load->library('session');
		$this->load->database();
		$this->load->helper('url');
		$aid=0;
		$data=array
		(
			'widget_title'=>$this->input->post('title'),
			'widget_content'=>$this->input->post('content')
		);
		$flg=$this->input->post('flg');
		$this->db->where('widget_index',$flg);
		$this->db->where('affiliate_id',$aid);
		$this->db->update('manage_widgets',$data);
		echo "Widget Update Successfully";
    }
	
	public function event_tab()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('admin/event_tab');
		$this->load->view('admin/footer');
	}
	public function event_insert()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$aid=0;
		$data=array
		(
			'event_name'=>$this->input->post('title'),
			'event_date'=>$this->input->post('date'),
			'status'=>$this->input->post('status'),
			'event_description'=>$this->input->post('content'),
			'affiliate_id'=>$aid
			 
		);
		$this->db->where('affiliate_id',$aid);
		$this->db->insert('manage_events', $data); 
		echo "Event Inserted Successfully";
	}
	public function view_events()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$aid=0;
		$this->db->where('affiliate_id',$aid);
		$data['content']=$this->db->get('manage_events');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('admin/center_tab',$data);
		$this->load->view('admin/footer');
	}
	
	public function image_upload()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('admin/image_upload');
		$this->load->view('admin/footer');
	}
	
	public function affiliate_picture()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('admin/affiliate_picture');
		$this->load->view('admin/footer');
	}
	
	public function email_template()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$data['id']=$this->uri->segment(3);
		$data['content']=$this->db->get('manage_events');
		$this->load->view('admin/header');
		$this->load->view('admin/menu');
		$this->load->view('admin/email_template',$data);
		$this->load->view('admin/footer');
	}
	
	public function dashboard()
	{
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('session');
		$this->load->view('Registration/header');
		$this->load->view('Registration/menu');
		$this->load->view('Registration/customer_registration');
	}
	
}

