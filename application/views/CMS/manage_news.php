<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Add Events
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					<li class="pull-right">
						<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="">
							<i class="fa fa-calendar"></i>
							<span>
							</span>
							
						</div>
					</li>
					
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">
							Home
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">
							Add Your Events
						</a>
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<div class="row inbox">
			
			<!-- END SAMPLE FORM PORTLET-->
			
			<div class="col-md-12">
				<!-- BEGIN PORTLET-->
				<div class="portlet paddingless">
					<div class="portlet-title line">
						<div class="caption">
							<i class="fa fa-bell-o"></i>Events
						</div>
						<div class="tools">
							<a href="" class="collapse">
							</a>
							<a href="#portlet-config" data-toggle="modal" class="config">
							</a>
							<a href="" class="remove">
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<!--BEGIN TABS-->
						<div class="col-md-12">
							<!-- BEGIN EXAMPLE TABLE PORTLET-->
							
							<div class="portlet-body">
								<table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										<tr>
											<th width="78">
												News Title
											</th>
											<th width="102">
												News Date
											</th>
											<th width="76">
												Status
											</th>
											<th width="81">
												Description
											</th>
											<!-- <th width="53">
												Images
											</th>-->
											<th width="31">
												Edit
											</th>
											<th width="49">
												Delete
											</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($content->result() as $row)
											{
											?>
											<tr class="odd gradeX">
												
												<td>
													<?php echo $row->m_news_title; ?>
													<td>
														<?php echo $row->m_entrydate; ?>
													</td>
													<td>
														<?php if ($row->m_news_status=='1')
															{ ?>
															
															<span class="label label-sm label-success">
															<?php echo "Active"; ?></span>
															<?php } 
															else
															{ ?>
															<span class="btn btn-xs yellow">
															<?php echo "Inactive"; }?>
														</td>
														<td>
															<?php echo $row->m_news_des; ?>
														</td>
														<!--  <td>
															<a href="javascript:void(0)" title="Upload Images" class="btn blue" onclick="event_image('<?php echo $row->m_news_id; ?>')"><span class="glyphicon glyphicon-picture"></span> </a>
														</td>-->
														<td>
															<a href="javascript:void(0)" title="Edit Your Event Details" class="btn purple" onclick="eve_edit('<?php echo $row->m_news_id; ?>')"><i class="fa fa-edit"></i></a>
														</td>
														<td>
															<a href="javascript:void(0)" title="Delete Event" class="btn red" onclick="del('<?php echo $row->m_news_id; ?>')"><i class="fa fa-times"></i></a>
														</td>
													</tr>
												<?php } ?>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div></div>
				</div>
			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->
</div>
<script type="text/javascript">
	function loadUrl(newLocation)
	{
		window.location = newLocation;
		return false;
	}
</script>
<script>
	function event_insert()
	{
		var title = $('#txttitle').val();
		var status = $('#status').val();
		var date = $('#event_date').val();
		var editor = CKEDITOR.instances.editor1;
		//alert(status);
		//alert( editor.getData() );
		
		var content=editor.getData();
		$.ajax(
        {
			type: "POST",
			url:"<?php echo base_url(); ?>index.php/cms/event_insert/",
			data: "title="+title+"&date="+date+"&status="+status+"&content="+content,
			success: function(msg) {
				alert(msg);
				location.reload();
			}
			
		});
		
	}
</script>

<script>
	function del(id)
	{
		var eve_id = id;
		$.ajax(
		{
			type:"POST",
			url: "<?php echo base_url(); ?>index.php/cms/delete_event/",
			data: "id="+eve_id,
			success: function(msg) {
				location.reload();
				alert(msg);
			}
			
		});
		
	}
</script>

<script>
	function eve_edit(id)
	{
		var eve_id = id;
		loadUrl("<?php echo base_url();?>index.php/cms/edit_event/"+id);
	}
</script>
<script>
	function event_image(id)
	{
		var eve_id = id;
		loadUrl("<?php echo base_url();?>index.php/cms/event_image/"+id);
	}
</script>
