F
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Banner Images
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								
							</div>
						</li>
						
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">
								Home
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Banner Images
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<div class="row inbox">
				
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Home Tab 1 Content
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
                     <div class="portlet-body form">
                    	<div class="custom-file-upload">
							<!--<?php echo $error;?>

							-->
                             <form name="image_form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/affiliate_upload/do_upload/" method="post">
                            	<div class="fileinput fileinput-new" data-provides="fileinput">
												<span class="btn default btn-file">
													<span class="fileinput-new">
														 Select file
													</span>
													<span class="fileinput-exists">
														 Change
													</span>
													 <input type="file" multiple name="userfile[]" size="20" />
												</span>
												<span class="fileinput-filename">
												</span>
												 &nbsp;
												<a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">
												</a>
								</div>
                              
                            <input type="submit" class="btn blue" value="Upload" />
                            
                            </form>
                          </div>
					  </div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
					<div class="row">
				<div class="col-md-9 col-sm-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
								<th>
									Image
								</th>
								<th>
									 Image Path
								</th>
								<th>
									 Status
								</th>
                                <th>
									 Action
								</th>
							</tr>
							</thead>
							<tbody>
                            <?php foreach($content->result() as $row)
								{
								?>
							<tr class="odd gradeX">
								
								<td>
									 <img src="<?php echo base_url();?>application/libraries/affiliate_Images/<?php echo $row->image_name; ?>" width="100px" height="100px" />
								</td>
								<td>
									 <?php echo base_url().'application/libraries/affiliate_Images/'.$row->image_name; ?>
                                </td>
                               <td>
                                     <?php if ($row->status=='1')
									 { ?>
								
									<span class="label label-sm label-success">
										<?php echo "Active"; ?></span>
                                     <?php } 
									 else
									 { ?>
                                     <span class="btn btn-xs yellow">
                                     <?php echo "Inactive"; }?>
								</td>
                               <input type="hidden" id="textimg"+<?php echo $row->image_id; ?>+"" value="<?php echo $row->image_name ?>" />
                                <td>
                 <button type="button" class="btn btn-danger" onclick="del('<?php echo $row->image_id; ?>','<?php echo $row->image_name; ?>')">Delete</button>
                                </td>
							</tr>
							<?php } ?>
							</tr>
							</tbody>
							</table>
						</div>
                          
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
					</div>
				</div>
			</div>
		</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<script>
function del(id,image_name)
{
	//alert("sdf");
	var image = id;
	var img_name = image_name;
	$.ajax(
	{
	type:"POST",
	url: "<?php echo base_url(); ?>index.php/cms/delete_affiliate_images/",
	data: "id="+image+"&imge="+img_name,
	success: function(msg) {
 	location.reload();
	alert(msg);
         }
		  
	});
	
}
</script>