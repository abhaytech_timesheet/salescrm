<!-- BEGIN FOOTER -->
<div class="footer">
	<div class="footer-inner">
		 <?php echo date('Y') ?> &copy; Ferry Info Pvt. Ltd.
	</div>
	<div class="footer-tools">
		<span class="go-top">
			<i class="fa fa-angle-up"></i>
		</span>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/respond.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-migrate-1.2.1.min.js"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery.blockui.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/uniform/jquery.uniform.min.js"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/data-tables/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/core/app.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/form-samples.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/table-managed.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/components-form-tools.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/fuelux/js/spinner.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/typeahead/handlebars.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/typeahead/typeahead.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/components-editors.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/components-pickers.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jqvmap/jqvmap/jquery.vmap.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/index.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-markdown/lib/markdown.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/tasks.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-file-upload/js/jquery.fileupload.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-file-upload/js/jquery.fileupload-ui.js"></script>
jQuery(document).ready(function() {    
   // initiate layout and plugins
   App.init();
   ComponentsEditors.init();
   FormSamples.init();
   TableManaged.init();
   ComponentsFormTools.init();
   ComponentsPickers.init();
   Index.initDashboardDaterange();
   Index.initCalendar();
   Index.initJQVMAP();
});
</script>
<!-- END JAVASCRIPTS -->

</body>
<!-- END BODY -->
</html>