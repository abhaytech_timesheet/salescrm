<script>
	function get_city()
	{
		var state_id=$("#ddstate").val();
		$("#ddcity").empty();
		if(state_id!='-1' && state_id!='')
		{
	        $.blockUI({ message: '<h3>Please Wait...</h3>' });
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>employee/select_city/"+state_id+"/0",
				dataType: "JSON",
				data:{'state_id':state_id},
				success:function(data){
				    setTimeout($.unblockUI, 01);
					$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
					$.each(data,function(i,item)
					{
						$("#ddcity").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
					});
				}
			});
		}
		else
		{
			$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
			alert("Please First Select The State");
		}
		
	}
</script>
<script>
	function get_spcl()
	{
		var spcl_id=$("#dddesignation").val();
		if(spcl_id!=1)
		{
			$('#1').show();
			$('#2').show();
			$('#3').show();
			$.blockUI({ message: '<h3>Please Wait...</h3>' });
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>employee/spcl/"+spcl_id,
				dataType: "JSON",
				data:{'spcl_id':spcl_id},
				success:function(data){
				    $("#ddspecialization").empty();
				    setTimeout($.unblockUI, 01);
					$("#ddspecialization").append("<option value=-1> ~~ Select ~~</option>");
					$.each(data,function(i,item)
					{
						$("#ddspecialization").append("<option value="+item.m_des_id+">"+item.m_des_name+"</option>");
					});
				}
			});
			
			//$("#spcl").load("<?php echo base_url();?>employee/spcl/"+spcl_id);
		}
		else
		{
			$('#1').hide();
			$('#2').hide();
			$('#3').hide();
		}
	}
</script>
<script>
	function get_shift()
	{
		var shift_id=$("#ddshift_type").val();
		$.blockUI({ message: '<h3>Please Wait...</h3>' });
		$.ajax(
		{
			type:"POST",
			url:"<?php echo base_url();?>employee/select_shift/"+shift_id,
			dataType: "JSON",
			data:{'shift_id':shift_id},
			success:function(data){
				if(data!='')
				{
					$("#shift_div").css('display','block');
					setTimeout($.unblockUI, 01);
					$.each(data,function(i,item)
					{
						$("#txtshift_strart_time").val(item.m_shift_starttime);
						$("#txtshift_end_time").val(item.m_shift_endtime);
						$("#txtshift_duration").val(item.m_shift_duration);
					});
				}
				else
				{
					$("#shift_div").css('display','none');
					setTimeout($.unblockUI, 01);
					$.each(data,function(i,item)
					{
						$("#txtshift_strart_time").val('');
						$("#txtshift_end_time").val('');
						$("#txtshift_duration").val('');
					});
				}
			}
		});
	}
</script>
<!-- BEGIN CONTENT -->
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="fa fa-user-plus"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/index/">Employee </a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li><i class="fa fa-user"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_employee/">
							View All Registered Employee 
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li><i class="fa fa-user"></i>
						<a href="javascript:void(0)">
							Create Employee Here
						</a>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Create Employee </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Create New Employee </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form method="post" id="record">
								<div class="form-body">
									<h4 class="caption-subject font-blue bold uppercase"> Basic Information </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Employee Name <span class="required"> * </span></label>
												<input type="text" class="form-control input-sm empty" placeholder="Enter Employee Name" name="txtemp_name" id="txtemp_name">
												<span id="divtxtemp_name" style="color:red"></span>		
											</div>
										</div>
									</div>
									<!--/row-->
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Gender</label>
												<div id="gender">
													<select class="form-control input-sm opt" name="ddgender" id="ddgender">
														<option value="-1">Select Gender</option>
														<option value="1">Male</option>
														<option value="2">Female</option>
													</select>
												</div>
												<span id="divddgender" style="color:red"></span>	
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Date Of Birth</label>
												<input type="text" class="form-control input-sm date-picker empty" placeholder="yyyy-mm-dd" name="txtdob" id="txtdob"  data-date-format="yyyy-mm-dd" data-date="1980-01-01" data-date-viewmode="years" data-date-minviewmode="months" />
												<span id="divtxtdob" style="color:red"></span>	
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label"> Email ID <span class="required"> * </span> </label>
												<input type="text" placeholder="Enter Your Email ID" name="txtmail" id="txtmail" class="form-control input-sm">
												<span id="divtxtmail" style="color:red"></span>	                  
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Contact Number<span class="required"> * </span> </label>
												<input type="text" placeholder="Enter Your Mobile No" class="form-control input-sm" maxlength="10" name="txtcontact" id="txtcontact">
												<span id="divtxtcontact" style="color:red"></span>	 
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">State <span class="required"> * </span></label>
												<select class="form-control input-sm opt" name="ddstate" id="ddstate" onchange="get_city()">
													<option value="">Select State</option>
													<?php
														foreach($loc->result() as $row)
														{
														?>
														<option value="<?php echo $row->m_loc_id?>"><?php echo $row->m_loc_name; ?></option>
														<?php
														}
													?>
												</select>
												<span id="divddstate" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">City <span class="required"> * </span> </label>
												<div id="city">
													<select class="form-control input-sm opt" name="ddcity" id="ddcity">
														<option value="">Select City</option>
													</select>
												</div>
												<span id="divddcity" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Pincode </label>
												<input type="text"placeholder="Enter Your Pincode" name="txtpincode" id="txtpincode" maxlength="6" class="form-control input-sm empty">
												<span id="divtxtpincode" style="color:red"></span>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-8">
											<div class="form-group">
												<label class="control-label">Address <span class="required"> * </span> </label>
												<textarea placeholder="Enter Your Address" name="txtaddress" id="txtaddress" class="form-control input-sm empty" col="3" style="font-size:20px;"></textarea>
												<span id="divtxtaddress" style="color:red"></span>
											</div>
										</div>
										
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Department <span class="required"> * </span></label>
												<select class="form-control input-sm opt" name="dddesignation" id="dddesignation" onchange="get_spcl()">
													<option value="-1">Select Department</option>
													<?php
														foreach($desig->result() as $row)
														{
														?>
														<option value="<?php echo $row->m_des_id ?>"><?php echo $row->m_des_name; ?></option>
														<?php
														}
													?>
												</select>
												<span id="divdddesignation" style="color:red"></span>
											</div>
										</div>
										
										<div class="col-md-4" id="1">
											<div class="form-group">
												<label class="control-label">Specialization</label>
												<div id="spcl">
													<select class="form-control input-sm" name="ddspecialization" id="ddspecialization">
														<option value="">Select Specialization</option>
													</select>
													<span id="divddspecialization" style="color:red"></span>
												</div>
											</div>
										</div>
										<div class="col-md-4" id="2">
											<div class="form-group">
												<label class="control-label">Work Experience <span class="required"> * </span></label>
												<select class="form-control input-sm" name="ddworkexp" id="ddworkexp">
													<option value="">Select Work Experience</option>
													<option value="0">Fresher</option>
													<option value="1">1 Year</option>
													<option value="2">2 Year</option>
													<option value="3">3 Year</option>
													<option value="4">4 Year</option>
													<option value="5">5 Year</option>
													<option value="more">More</option>
												</select>
												<span id="divddworkexp" style="color:red"></span>
											</div>
										</div>
									</div>
									<!--/span--> 
									<div class="row">
										<div class="col-md-4" id="3">
											<div class="form-group">
												<label class="control-label">Qualification <span class="required"> * </span></label>
												<select class="form-control input-sm" name="ddqualification" id="ddqualification">
													<option value="">Select Qualification</option>
													<?php
														foreach($quali->result() as $row)
														{
														?>
														<option value="<?php echo $row->quali_id?>"><?php echo $row->quali_name; ?></option>
														<?php
														}
													?>
												</select>
												<span id="divddqualification" style="color:red"></span>
											</div>
										</div>
										
									</div>
									<!--row--> 
									
									<h4 class="caption-subject font-blue bold uppercase"> Shift Information </h4>
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Select Shift Type </label>
												<select class="form-control input-sm opt" name="ddshift_type" id="ddshift_type" onchange="get_shift()">
													<option value="">Select Shift</option>
													<?php
														foreach($shift->result() as $rowsh)
														{
														?>
														<option value="<?php echo $rowsh->m_shift_id ?>"><?php echo $rowsh->m_shift_name; ?></option>
														<?php
														}
													?>
												</select>
												<span id="divddshift_type" style="color:red"></span>
											</div>
										</div>
										<!--row-->
										<div id="shift_div" style="display:none">
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">Shift Start Time </label>
													<input type="text"placeholder="Enter Your Pincode" name="txtshift_strart_time" id="txtshift_strart_time" class="form-control input-sm" readonly value="">
													<span id="divddshift_type" class="help-inline" style="color:red"></span>
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">Shift End Time </label>
													<input type="text"placeholder="Enter Your Pincode" name="txtshift_end_time" id="txtshift_end_time" class="form-control input-sm" readonly value="">
													<span id="divddshift_type" class="help-inline" style="color:red"></span>
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">Shift Duration </label>
													<input type="text"placeholder="Enter Your Pincode" name="txtshift_duration" id="txtshift_duration" class="form-control input-sm" readonly value="">
													<span id="divddshift_type" class="help-inline" style="color:red"></span>
												</div>
											</div>
										</div>
									</div>
									<h4 class="caption-subject font-blue bold uppercase"> Week Off </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Select Week Off Day </label>
												<select class="form-control input-sm opt" name="ddweekoff" id="ddweekoff">
													<option value="-1">Select Week Off</option>
													<option value="1">Monday</option>
													<option value="2">Tuesday</option>
													<option value="3">Wednesday</option>
													<option value="4">Thursday</option>
													<option value="5">Friday</option>
													<option value="6">Saturday</option>
													<option value="7">Sunday</option>                       
												</select>
												<span id="divddweekoff" style="color:red"></span>
											</div>
										</div>
										<!--row-->
									</div>
									
									<!--/span-->
									<h4 class="caption-subject font-blue bold uppercase"> Account Information </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Account Name <span class="required"> * </span> </label>
												<input type="text"placeholder="Enter Account Name" class="form-control input-sm empty" name="txtacc_name" id="txtacc_name">
												<span id="divtxtacc_name" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label"> Account Number <span class="required"> * </span> </label>
												<input type="text"placeholder="Enter Account No" class="form-control input-sm empty" name="txtacc_num" id="txtacc_num">
												<span id="divtxtacc_num" style="color:red"></span>
											</div>
										</div>
										
										<!--row-->
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Bank Name </label>
												<select class="form-control input-sm opt" name="ddbank_name" id="ddbank_name">
													<option value="">Select Bank Name</option>
													<?php
														foreach($bank->result() as $row)
														{
														?>
														<option value="<?php echo $row->m_bank_id ?>"><?php echo $row->m_bank_name; ?></option>
														<?php
														}
													?>
												</select>
												<span id="divddbank_name" style="color:red"></span>
												
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Bank Branch </label>
												<input type="text" class="form-control input-sm empty" placeholder="Enter Your Bank Branch"name="txtbank_branch" id="txtbank_branch"/>
												<span id="divtxtbank_branch" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Bank IFSC <span class="required"> * </span> </label>
												<input type="text" class="form-control input-sm empty" placeholder="Enter Bank IFSC Code"name="txtbank" id="txtbank"/>
												<span id="divtxtbank" style="color:red"></span>
											</div>
										</div>
										
										<!--row-->
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">MMID Code </label>
												<input type="text" class="form-control input-sm empty" placeholder="Enter MMID Code" name="txtmmid" id="txtmmid"/>
												<span id="divtxtmmid" style="color:red"></span>
											</div>
										</div>
									</div>
								</div>
								<div id="function" class="form-actions left">
									<div class="col-md-offset-3 col-md-9">
										<button type="button" onclick="save()"  class="btn blue"><i class="fa fa-check"></i>Save</button>
										<button type="button" onclick="exit()" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							<!-- END FORM--> 
							
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													

<script>
	function save()
	{
		if(check("record"))
		{
			bootbox.confirm('Are you sure to Submit from?', function(result){
				if(result==true)
				{
					$("#function").html("<div>Employee Registration has been processed.<img id='checkmark' src='<?php echo base_url(); ?>/application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></div>");
					var data = {
						txtemp_name:$('#txtemp_name').val(),
						txtemp_code:$('#txtemp_code').val(),
						dddesignation:$('#dddesignation').val(),
						ddspecialization:$('#ddspecialization').val(),
						txtdob:$('#txtdob').val(),
						ddqualification:$('#ddqualification').val(),
						ddworkexp:$('#ddworkexp').val(),
						ddgender:$('#ddgender').val(),
						txtmail:$('#txtmail').val(),
						txtcontact:$('#txtcontact').val(),
						ddstate:$('#ddstate').val(),
						ddcity:$('#ddcity').val(),
						txtaddress:$('#txtaddress').val(),
						txtpincode:$('#txtpincode').val(),
						txtacc_name:$('#txtacc_name').val(),
						txtacc_num:$('#txtacc_num').val(),
						ddbank_name:$('#ddbank_name').val(),
						txtbank_branch:$('#txtbank_branch').val(),
						txtbank:$('#txtbank').val(),
						txtmmid:$('#txtmmid').val(),
						ddshift_type:$('#ddshift_type').val(),
						ddweekoff:$('#ddweekoff').val()		
					};
					$.ajax(
					{
						type: "POST",
						url:"<?php echo base_url();?>employee/insert_employee/",
						data:data,
						success: function(msg){
							//$(".page-content").html(msg);
							if(msg!="")
							{
								$(".page-content").html("<center><h2>Employee registration has been processed.</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(1000,function()
								{
									loadUrl("<?php echo base_url()?>employee/multimedia_upload/"+msg);
									$("#stylized").load("<?php echo base_url()?>employee/multimedia_upload/"+msg);
								});
							}
							else
							{
								alert("Sorry,Registration Unsuccessful.");
							}
						}
					});
				}
			}); 
		}
		else
		{
			return false;
		}
	}
</script> 
<script type="text/javascript">
	function loadUrl(newLocation)
	{
		window.location = newLocation;
		return false;
	}
</script>
<script>
	function exit()
	{		
		if(confirm('Are you sure to Exit from Employee Registration?')==true)
		{
			$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
			$("#stylized").load("<?php echo base_url().'employee/after_view_employee'?>");
		}
	}
</script>

