
<!-- BEGIN CONTENT -->
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="fa fa-user-plus"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/index/">Employee </a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-user"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_employee_report/">
							View All Employee Attendance Here
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<span>  Registration </span>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Upload Document </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Upload Document </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" id="myform" action="#">
								<div class="form-body">
									
									<div class="form-group">
										<label class="col-md-3 control-label">Document Type</label>
										<div class="col-md-6">
											<select name="ddtype" id="ddtype" class="form-control input-sm opt">
												<option value="-1">Select</option>
												<?php 
													foreach($doc->result() as $row)
													{
													?>
													<option value="<?php echo $row->m_type_id; ?>"><?php echo $row->m_type_name; ?></option>
													<?php
													}
												?>
											</select>
                                            <span id="divddtype" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Upload Document</label>
										<div class="col-md-6">
										    <div class="fileinput fileinput-new" data-provides="fileinput">
												<div class="input-group">
													<div class="form-control uneditable-input input-fixed " data-trigger="fileinput">
														<i class="fa fa-file fileinput-exists"></i>&nbsp;
														<span class="fileinput-filename">
														</span>
													</div>
													<span class="input-group-addon btn default btn-file">
														<span class="fileinput-new"> Select file </span>
														<span class="fileinput-exists"> Change </span>
														<input type="file" class="emptyfile" id="userfile" name="userfile" accept='image/*'>
													</span>
													<a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
												</div>
											</div>
											<span id="divuserfile" style="color:red"></span>
											
											<input type="hidden" name="txtuserid" id="txtuserid" value="<?php echo $id; ?>" />
											<input type="hidden" name="txtpicname" id="txtpicname" />
										</div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9" id="btnsubmit">
										<button type="button" class="btn green" onclick="submitFile()" >Submit</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-globe font-dark"></i>
								<span class="caption-subject bold uppercase"></span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th width="10%">S No.</th>
										<th width="30%">User Id</th>
										<th width="30%">User Name</th>
										<th width="30%">Document type</th>
										<th width="30%">Uploaded Document</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										foreach($rec->result() as $row)
										{
										?>
										<tr>
											<td width="20%"><?php echo $row->SN;?></td>
											<td width="20%"><?php echo $row->or_m_user_id?></td>
											<td width="20%"><?php echo $row->or_m_name?></td>
											<td width="20%"><?php echo $row->m_type_name ?></td>
											<td width="20%"><a href="<?php echo base_url(); ?>application/user_document/<?php echo $row->m_doc_upload?>" target="_blank"><?php echo $row->m_doc_upload?></a></td>
										</tr>
										<?php
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	

<script>
	function submitFile()
	{
		
		var img=$("#userfile").val();
		if(img!="")
		{
			var filesize=$("#userfile")[0].files[0].size;
			if(filesize < 1024 * 1024 * 2)
			{
				if(check("myform"))
				{
					bootbox.confirm('Are you sure to Submit from?', function(result){
						if(result==true)
						{
					        $("#btnsubmit").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
							var formUrl = "<?php echo base_url(); ?>index.php/employee/upload_document";
							var formData = new FormData($('.form-horizontal')[0]);
							$.ajax({
								url: formUrl,
								type: 'POST',
								data: formData,
								mimeType: "multipart/form-data",
								contentType: false,
								cache: false,
								processData: false,
								success: function(data){
									if(data!='')
									{
										$('#txtpicname').val(data);
										
										save();
									}
									else
									{
										$('#userfile').val('');
										alert(data);
										
									}
								},
								error: function(jqXHR, textStatus, errorThrown){
									//handle here error returned
								}
							});
						}
					}); 
				}
				else
				{
					return false;
				}
			}
			else
			{
				alert("File Size must be less than 2 mb");
			}
		}
		else
		{
			alert("Please upload a image first");
		}
		
	}
</script> 
<script>
	function save()
	{
		formData=$("#myform").serialize();
		$.ajax(
		{
			type: "POST",
			url:"<?php echo base_url();?>index.php/employee/insert_uploaded_Document/",
			mimeType:"multipart/form-data",
			data:formData,
			success: function(msg){
				
				if(msg.trim()=='1')			
				{
					$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
					location.reload();
					//$("#stylized").load("<?php echo base_url().'index.php/employee/after_view_employee'?>");
				}
			}
		});	
		
	}
</script> 			

