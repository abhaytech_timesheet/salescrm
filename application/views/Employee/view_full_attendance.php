<div class="modal-dialog modal-full">
	<div class="modal-content">
		
		<div class="modal-body">
			<div class="row">
				<div class="col-md-12">
				
						
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
								
								<thead>
									<tr>
										<th> Login</th>
										<th>Logout</th>
										<th>Total Login of day</th>
										<th>Total Logout of day</th>
										<th>Login Duration</th>
										<th>Late </th>
										
										
									</tr>
								</thead>
								<tbody>
									
									<?php $logincount=0; $logout=0; $logoutcount=0; $login_hour=0; $login_min=0; $logout_hour=0; $logout_min=0;?>
									<tr>
										<td>
											<?php
												foreach($full_att->result() as $row)
												{
													if($row->emp_atten_status=='1') 
													{
														
														
														$login_hour=$row->hour_of_attendance;
														$login_min=$row->min_of_attendance; 
														break;
													} 
												}
												echo $login_hour.":".$login_min; 
											?>
											
										</td>
										<td>
											<?php 
												foreach($full_att->result() as $row1)
												{
													$shift_time=$row1->m_shift_starttime;
													
													
													if($row1->emp_atten_status=='0') 
													{
														
														$logoutcount=$logoutcount+1;
														$logout=$row1->hour_of_attendance.":".$row1->min_of_attendance; 
														$logout_hour=$row1->hour_of_attendance;
														$logout_min=$row1->min_of_attendance;
													} 
													
												}
												if($logout==0){} else {echo $row1->hour_of_attendance.":".$row1->min_of_attendance;} 
											?>
										</td>
										<td>
											<?php 
												foreach($full_att->result() as $row2)
												{
													if($row2->emp_atten_status=='1'){ $logincount=$logincount+1;}
												}
												echo $logincount;
											?>
										</td>
										<td><?php echo $logoutcount;?></td>
										<td> 
											<?php  
												if($logoutcount!=0) 
												{ 
													if($logout_min>$login_min) 
													{ 
														echo $logout_hour-$login_hour;  
														echo " hour "; 
														echo $logout_min-$login_min; 
														echo " min"; 
													} 
													else
													{
														echo $logout_hour-$login_hour-1; 
														echo " hour "; 
														echo $logout_min+60-$login_min; 
														echo " min"; 
													} 
												}   
											?>
										</td>
										<td>
											<?php 
												$shift=explode(':',$shift_time); 
												if($login_hour>=$shift[0]) 
												{ 
													if($login_min>$shift[1]) 
													{ 
														echo $login_hour-$shift[0]; 
														echo " hour "; 
														echo $login_min-$shift[1]; 
														echo " min"; 
													} 
													else 
													{ 
														echo $login_hour-$shift[0]-1; 
														echo " hour "; 
														echo $shift[1]+60-$login_min;
														echo " min"; 
													} 
												}
												else 
												{ 
													echo $shift[0]-$login_hour-1; 
													echo " hour "; 
													echo 60-$login_min; 
													echo " min Earlier"; 
												} 
											?>
										</td>
										
									</tbody>
								</table>
							
						</div>
						
					</div>
					<!-- END PAGE CONTENT-->
				</div>	
				<div class="modal-footer">
					<button type="button" class="btn red btn-sm " data-dismiss="modal">Close</button>
					
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>		
	