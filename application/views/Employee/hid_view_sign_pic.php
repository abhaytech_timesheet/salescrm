<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption"> 
			<i class="icon-cursor font-black"></i>
			<span class="caption-subject font-black bold uppercase">Upload Signature Image</span>
		</div>
		<div class="actions">
			<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
				<i class="icon-cloud-upload"></i>
			</a>
			<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
				<i class="icon-wrench"></i>
			</a>
			<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
			<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
				<i class="icon-trash"></i>
			</a>
		</div>
	</div>
	<div class="portlet-body form"> 
		<!-- BEGIN FORM-->
		<form action="#" class="horizontal-form" enctype="multipart/form-data" method="post" id="form_sample_1">
			<div class="form-body">
				<h3 class="form-section">Signature Image</h3>
				<div class="row">
                    <div class="col-md-6">
						<div class="form-group">
							<label class="control-label">Signature Image</label>
							<div style="clear:both;"></div>
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="input-group input-xx-large">
									<div class="form-control uneditable-input span3" data-trigger="fileinput"> <i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename"> </span> </div>
									<span class="input-group-addon btn default btn-file"> <span class="fileinput-new"> Select file </span> <span class="fileinput-exists"> Change </span>
										<input type="file" id="userfile" name="userfile" accept='image/*'>
									</span> <a href="#" class="input-group-addon btn default fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
							</div>
							<!--  <span class="help-block"> Allowed Extension are Pdf,Doc,Xlsx and Txt. </span><br />--> 
							<span class="help-block"> File Size Must Be Smaller Than 2 MB. </span>
						</div>
					</div>
				</div>                  
			</div>
			
			<div class="form-actions left">
                <button type="button" value="submit" onclick="submitFile1()" class="btn blue"><i class="fa fa-check"></i> Upload</button>
                <button type="button" onclick="exit()" class="btn default">Cancel</button>
			</div>
		</form>
		<!--/row--> 
		<!-- END FORM--> 
	</div>
</div>
