<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
						<li>
					    <i class="fa fa-user-plus"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/index/">Employee </a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					<i class="fa fa-user"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_employee_report/">
							View All Employee Attendance Here
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">Employee Attendance</h3>
			<!-- END PAGE TITLE-->
			
			
			<div class="row">
				<div class="col-md-12">
					
					<div class="table-responsive" id="data1">
						<form method="post" action="#">
							<table class="table table-striped table-bordered table-hover" id="datatable_products">
								<thead>
									<tr role="row" class="heading">
										<th width="10%">
											Employee Code
										</th>
										<th width="10%">
											Employee Name
										</th>
										<th width="10%">
											Year
										</th>
										<th width="10%">
											Month
										</th>
										
										<th width="10%">
											Department
										</th>
										<th width="10%">
											Specialization
										</th>
										<th width="10%">
											Action
										</th>
									</tr>
									<tr role="row" class="filter">
										<td>
											<input type="text" class="form-control input-sm" name="txtempcode" id="txtempcode" value="<?php echo $empcode;?>" placeholder="Employee Code">
										</td>
										
										<td>
											<select name="ddname" id="ddname" class="form-control input-sm opt">
												<option value="">Select...</option>
												<?php 
													foreach($user->result() as $row2)
													{
													?>
													<option value="<?php echo $row2->or_m_reg_id; ?>" <?php if($empregid==$row2->or_m_reg_id){ echo "selected"; } ?> ><?php echo $row2->or_m_name; ?></option>
													<?php
													}
												?>
											</select>
										</td>
										
										<td>
											<select name="ddyear" id="ddyear" class="form-control input-sm opt">
											    <option value="<?php echo date('Y')-2;?>" <?php if($year==date('Y')-2){echo "selected"; } ?> ><?php echo date('Y')-2;?></option>
											    <option value="<?php echo date('Y')-1;?>" <?php if($year==date('Y')-1){echo "selected"; } ?> ><?php echo date('Y')-1;?></option>
												<option value="<?php echo date('Y');?>" <?php if($year==date('Y')){echo "selected"; } ?> ><?php echo date('Y');?></option>
											</select>
										</td>
										
										<td>
											<select name="ddmonth" id="ddmonth" class="form-control input-sm opt">
												<option value="1" <?php if($month==1){echo "selected"; } ?>>January</option>
												<option value="2" <?php if($month==2){echo "selected"; } ?>>February</option>
												<option value="3" <?php if($month==3){echo "selected"; } ?>>March</option>
												<option value="4" <?php if($month==4){echo "selected"; } ?>>April </option>
												<option value="5" <?php if($month==5){echo "selected"; } ?> >May  </option>
												<option value="6" <?php if($month==6){echo "selected"; } ?> >June </option>
												<option value="7" <?php if($month==7){echo "selected"; } ?> >July </option>
												<option value="8" <?php if($month==8){echo "selected"; } ?> >August </option>
												<option value="9" <?php if($month==9){echo "selected"; } ?> >September </option>
												<option value="10" <?php if($month==10){echo "selected"; } ?> >October </option>
												<option value="11" <?php if($month==11){echo "selected"; } ?> >November </option>
												<option value="12" <?php if($month==12){echo "selected"; } ?> >December </option>
											</select>
											
										</td>
										
										<td>
											<select name="dddesignation" id="dddesignation" class="form-control input-sm opt" onchange="get_spcl()">
												<option value="">Select...</option>
												<?php 
													foreach($desig->result() as $row4)
													{
													?>
													<option value="<?php echo $row4->m_des_id; ?>" <?php if($empdesig==$row4->m_des_id){ echo "selected"; } ?> ><?php echo $row4->m_des_name; ?></option>
													<?php
													}
												?>
											</select>
										</td>
										
										<td>
											<select name="ddspcl" id="ddspcl" class="form-control input-sm select2me">
												<option value="">Select...</option>
											</select>
											<input type="hidden" name="txtddspcl" id="txtddspcl" value="<?php echo $empspecification; ?>">
										</td>
										
										<td>
											<button class="btn btn-sm green filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
											<button type="reset" class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
										</td>
									</tr>
								</thead>
							</table>
						</form>
						
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-calendar font-dark"></i>
								<span class="caption-subject bold uppercase"> Employee Attendance Report of <?php echo $month."/".$year; ?></span>
							</div>
							
						</div>
						<div class="portlet-body" style="overflow-x: auto;">
							
							<table class="table table-striped table-bordered table-hover" >
								
								<thead>
									<tr>
									    <th></th>
										<th>S No.</th>
										<th>Employee Code</th>
										<th>Employee Name</th>
										<th>Total Present</th>
										<th>Total Working Days</th>
										<?php  $dn=0;
											foreach($leave->result() as $row1) { ?>
											<th><?php echo $row1->m_leave_name; ?></th>
										<?php $dn=$dn+1; } ?>
										
									</tr>
									
								</thead>
								<tbody>
									
									<?php
										$sun=0;
										$Snn=1;
										foreach($rec->result() as $row5)
										{
										?>
										<tr>
										    <td><button class="label btn-xs label-success circle" onclick="show('<?php echo $row5->SN; ?>')"><i id="icon1<?php echo $row5->SN; ?>" style="display:block" class="fa fa-plus"></i><i id="icon2<?php echo $row5->SN; ?>" style="display:none" class="fa fa-minus"></i></button> </td>
											<td> <?php echo $Snn; ?></td>
											<td><?php echo $row5->Emp_Code; ?></td>
											<td><?php echo $row5->Emp_name; ?></td>
											<td><?php echo $row5->Present; ?></td>
											<td>
												<?php for($j=1;$j<=$lastday;$j++)
													{ 
														$c="a".$j;
														
														if($row5->$c!='P') 
														{ 
															if($row5->$c!=''){ $sun=$sun+1;  }
														} 
													} 
													echo $lastday-$sun;
												?>
											</td>
											<?php foreach($leave->result() as $row1) { ?>
												<td></td>
											<?php  } ?>
											
										</tr>
										<tr id="<?php echo $row5->SN; ?>" style="display:none">
											
											<td colspan="<?php echo 6+$dn; ?>" >
												<table class="table table-striped table-bordered table-hover" >
													<thead>
														<tr>
															<?php for($i=1;$i<=$lastday;$i++)
																{ ?>
																<th><?php echo $i."/".$month; ?></th>
															<?php } ?>
														</tr>
													</thead>
													<tbody>
														<tr >
															
															<?php for($i=1;$i<=$lastday;$i++)
																{ 
																	$b="a".$i;
																?>
																<td> 
																	<?php 
																		if($row5->$b=='P') 
																		{ 
																		?>
																		<a data-toggle="modal" href="#basic" onclick="view_full_attendance(<?php echo "'".$row5->Emp_reg_id."','".$row5->Year."','".$row5->Month."','".$i."'"; ?>  )">
																			<?php echo $row5->$b; ?> 
																		</a>
																		<?php 
																		} 
																		else 
																		{  
																	      if($row5->$b!='')
																		  {
																			echo $row5->$b;
																		  }
																		  else
																		  {
																			echo 'A';  
																		  }
																			
																		} 
																	?>
																</td>
															<?php } ?>
														</tr>
													</tbody>
												</table>
												
											</td>	
										</tr>
										
										
										
										<?php 
											$sun=0;
											$Snn++;
										}
									?>    
									
								</tbody>
							</table>
							
							
							<script>
								function show(input)
								{
									$('#'+input).toggle();
									$("#icon1"+input).toggle();
									$("#icon2"+input).toggle();
								}
							</script>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<div class="modal fade" id="basic" tabindex="-1" role="dialog" aria-hidden="true">
	
</div>

<script>
	function view_full_attendance(id,year,month,day)
	{ 
		$("#basic").load("<?php echo base_url();?>index.php/employee/employee_attendance/"+id+"/"+year+"/"+month+"/"+day);
	}
</script>
<script>
	window.onload=function()
	{
		
		get_spcl();
		
	}
	
	
</script>	

<script>
	function get_spcl()
	{
		var dddesignation=$("#dddesignation").val();
		if(dddesignation!="-1")
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url(); ?>index.php/welcome/spcl",
				dataType: 'json',
				data: {'dddesignation': dddesignation},
				success: function(data) {
					$("#ddspcl").empty();
					$("#ddspcl").append("<option value=-1>Select Specialization</option>");
					$.each(data,function(i,item)
					{
						if($("#txtddspcl").val()==item.m_des_id)
						{
							$('#ddspcl').append("<option value="+item.m_des_id+" selected >"+item.m_des_name+"</option>");
						}
						else
						{
							$('#ddspcl').append("<option value="+item.m_des_id+">"+item.m_des_name+"</option>");
						}
					});
				}
			});
		}
		else
		{
			alert("Please Select State First");
		}
	}
</script>	

