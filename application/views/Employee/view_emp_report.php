<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="fa fa-user-plus"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/index/">Employee </a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li><i class="fa fa-user"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_employee_report/">
							Search All Employee Here
						</a>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">Employee Report</h3>
			<!-- END PAGE TITLE-->
			
			
			<div class="row">
				<div class="col-md-12">
					
					<div class="table-responsive" id="data1">
						<form method="post" action="#">
							<table class="table table-striped table-bordered table-hover" id="datatable_products">
								<thead>
									<tr role="row" class="heading">
										<th width="10%">
											Employee Code
										</th>
										<th width="10%">
											Employee Name
										</th>
										<th width="10%">
											State
										</th>
										<th width="10%">
											City
										</th>
										<th width="10%">
											Joining Date
										</th>
										<th width="10%">
											Department
										</th>
										<th width="10%">
											Specialization
										</th>
										<th width="10%">
											Action
										</th>
									</tr>
									<tr role="row" class="filter">
										<td>
											<input type="text" class="form-control input-sm" name="txtempcode" id="txtempcode" value="<?php echo $empcode;?>" placeholder="Employee Code">
										</td>
										
										<td>
											<select name="ddname" id="ddname" class="form-control input-sm opt">
												<option value="">Select...</option>
												<?php 
													foreach($user->result() as $row2)
													{
													?>
													<option value="<?php echo $row2->or_m_reg_id; ?>" <?php if($empregid==$row2->or_m_reg_id){ echo "selected"; } ?> ><?php echo $row2->or_m_name; ?></option>
													<?php
													}
												?>
											</select>
										</td>
										
										<td>
											<select name="ddstate" id="ddstate" class="form-control input-sm opt" onchange="get_city()">
												<option value="">Select State...</option>
												<?php 
													foreach($loc->result() as $row3)
													{
													?>
													<option value="<?php echo $row3->m_loc_id; ?>" <?php if($empstate==$row3->m_loc_id){ echo "selected"; } ?> ><?php echo $row3->m_loc_name; ?></option>
													<?php
													}
												?>
											</select>
										</td>
										
										<td>
											<select name="ddcity" id="ddcity" class="form-control input-sm opt">
												<option value="">Select City...</option>
											</select>
											<input type="hidden" name="txtcity" id="txtcity" value="<?php echo $empcity; ?>">
										</td>
										
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="txtfromdate" id="txtfromdate" value="<?php echo $fromdate;?>" placeholder="From">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="txttodate" id="txttodate" value="<?php echo $todate;?>" placeholder="To">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										
										<td>
											<select name="dddesignation" id="dddesignation" class="form-control input-sm opt" onchange="get_spcl()">
												<option value="">Select...</option>
												<?php 
													foreach($desig->result() as $row4)
													{
													?>
													<option value="<?php echo $row4->m_des_id; ?>" <?php if($empdesig==$row4->m_des_id){ echo "selected"; } ?> ><?php echo $row4->m_des_name; ?></option>
													<?php
													}
												?>
											</select>
										</td>
										
										<td>
											<select name="ddspcl" id="ddspcl" class="form-control input-sm select2me">
												<option value="">Select...</option>
											</select>
											<input type="hidden" name="txtddspcl" id="txtddspcl" value="<?php echo $empspecification; ?>">
										</td>
										
										<td>
											<button class="btn btn-sm green filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
											<button type="reset" class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
										</td>
									</tr>
								</thead>
							</table>
						</form>
						
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-settings font-dark"></i>
								<span class="caption-subject bold uppercase">View Employee Report</span>
							</div>
							
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								
								<thead>
									<tr>
										<th>S No.</th>
										<th>Employee Code</th>
										<th>Employee Name</th>
										<th>Date of Joining</th>
										<th>Email</th>
										<th>Mobile</th>
										<th>Designation</th>
										<th>Profile Pic</th>
									</tr>
								</thead>
								<tbody>
									
									<?php
										foreach($rec->result() as $row5)
										{
										?>
										<tr>
											<td><?php echo $row5->SN; ?></td>
											<td><?php echo $row5->or_m_user_id; ?></td>
											<td><?php echo $row5->or_m_name; ?></td>
											<td><?php echo $row5->doj; ?></td>
											<td><?php echo $row5->or_m_email; ?></td>
											<td><?php echo $row5->or_m_mobile_no; ?></td>
											<td><?php echo $row5->m_des_name; ?></td>
											<td><a href="<?php echo base_url(); ?>application/emp_pics/<?php echo $row5->or_m_userimage; ?>" target="_blank"><?php echo $row5->or_m_userimage; ?></a></td>
											<?php 
											}
										?>    
										
									</tbody>
								</table>
							</div>
						</div>
						
					</div>
					<!-- END PAGE CONTENT-->
				</div>
				
				</div>
			<!-- END CONTENT BODY -->
		</div>
	</div>
	
	
	<script>
	    window.onload=function()
		{
		get_city();
		get_spcl();
		}
		
		function get_city()
		{
			var ddstate=$("#ddstate").val();
			if(ddstate!="-1")
			{
				$.ajax(
				{
					type:"POST",
					url:"<?php echo base_url(); ?>index.php/welcome/get_city",
					dataType: 'json',
					data: {'ddstate': ddstate},
					success: function(data)
					{
						$("#ddcity").empty();
						$("#ddcity").append("<option value=-1>Select City</option>");
						$.each(data,function(i,item)
						{
						   if($("#txtcity").val()==item.m_loc_id)
						   {
							   $('#ddcity').append("<option value="+item.m_loc_id+" selected >"+item.m_loc_name+"</option>");
						   }
						   else
						   {
							$('#ddcity').append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
						   }
						});
					}
				});
			}
			else
			{
				alert("Please Select State First");
			}
		}
	</script>	
	
	<script>
		function get_spcl()
		{
			var dddesignation=$("#dddesignation").val();
			if(dddesignation!="-1")
			{
				$.ajax(
				{
					type:"POST",
					url:"<?php echo base_url(); ?>index.php/welcome/spcl",
					dataType: 'json',
					data: {'dddesignation': dddesignation},
					success: function(data) {
						$("#ddspcl").empty();
						$("#ddspcl").append("<option value=-1>Select Specialization</option>");
						$.each(data,function(i,item)
						{
						    if($("#txtddspcl").val()==item.m_des_id)
							{
								$('#ddspcl').append("<option value="+item.m_des_id+" selected >"+item.m_des_name+"</option>");
							}
							else
							{
							$('#ddspcl').append("<option value="+item.m_des_id+">"+item.m_des_name+"</option>");
							}
						});
					}
				});
			}
			else
			{
				alert("Please Select State First");
			}
		}
	</script>																																