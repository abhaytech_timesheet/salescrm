<!-- BEGIN CONTENT -->
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="fa fa-user-plus"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/index/">Employee </a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					<i class="fa fa-user"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_employee_report/">
							View All Employee Attendance Here
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Employee Multimedia Attachments </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div id="divload">
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-cursor font-black"></i>
									<span class="caption-subject font-black bold uppercase"> Upload Profile Picture </span>
								</div>
								
								<div class="actions">
									<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
										<i class="icon-cloud-upload"></i>
									</a>
									<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
										<i class="icon-wrench"></i>
									</a>
									<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
									<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
										<i class="icon-trash"></i>
									</a>
								</div>
							</div>
							<div class="portlet-body form" > 
								<!-- BEGIN FORM-->
								<form action="#" class="horizontal-form" enctype="multipart/form-data" method="post" id="form_sample_1">
									<div class="form-body"  id="action">
										<h4 class="caption-subject font-blue bold uppercase">Profile Picture</h4>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label">Profile Picture</label>
													<div style="clear:both;"></div>
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div class="input-group">
															<div class="form-control uneditable-input input-fixed " data-trigger="fileinput">
																<i class="fa fa-file fileinput-exists"></i>&nbsp;
																<span class="fileinput-filename">
																</span>
															</div>
															<span class="input-group-addon btn default btn-file">
																<span class="fileinput-new"> Select file </span>
																<span class="fileinput-exists"> Change </span>
																<input type="file" class="emptyfile" id="userfile" name="userfile" accept='image/*'>
															</span>
															<a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
														</div>
													</div>
													
												<span class="help-block"> File Size Must Be Smaller Than 2Mb </span> </div>
											</div>
										</div>                  
									</div>
									
									<div class="form-actions left">
										<button type="button" value="submit" onclick="submitFile()" class="btn blue"><i class="fa fa-check"></i> Upload</button>
										<button type="button" onclick="exit()" class="btn default">Cancel</button>
									</div>
								</form>
								<!--/row--> 
								<!-- END FORM--> 
							</div>
						</div>
						
					</div>
					<!-- END PAGE CONTENT-->
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	

<input type="hidden" name="txtpro_id" id="txtpro_id" value="<?php echo $id; ?>"/>
<input type="hidden" name="txtpicname" id="txtpicname" />
<input type="hidden" name="txtsignname" id="txtsignname" />
<input type="hidden" name="txtdocument" id="txtdocument" />
<script>
	function submitFile()
	{
		var img=$("#userfile").val();
		if(img!="")
		{
			var filesize=$("#userfile")[0].files[0].size;
			if(filesize < 1024 * 1024 * 2)
			{
				var formUrl = "<?php echo base_url(); ?>index.php/employee/uploadfile";
				var formData = new FormData($('.horizontal-form')[0]);
				//alert($('.horizontal-form')[0]);
				$.ajax({
					url: formUrl,
					type: 'POST',
					data: formData,
					mimeType: "multipart/form-data",
					contentType: false,
					cache: false,
					processData: false,
					success: function(data){
						//alert(data);
						if(data!='')
						{
							$('#txtpicname').val(data);
							save();
						}
						else
						{
							$('#userfile').val('');
							//alert(data);
							
						}
					},
					error: function(jqXHR, textStatus, errorThrown){
						//handle here error returned
					}
				});
			}
			else
			{
				alert("File Size must be less than 2 mb");
			}
		}
		else
		{
			alert("Please upload a image first");
		}
	}
</script> 
<script>
	function save()
	{
		//alert("ff");
		var data={file1:$('#txtpicname').val(),txtpro_id:$('#txtpro_id').val()};
		//alert($('#txtpicname').val());
	//	alert('Image uploaded');
		$.ajax(
		{
			type: "POST",
			url:"<?php echo base_url();?>index.php/employee/insert_profile_pic/",
			mimeType:"multipart/form-data",
			data:data,
			success: function(msg){
				//alert(msg);
				if(msg=='1')			
				{
					//alert("hello");
					$("#divload").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
					$("#divload").load("<?php echo base_url().'index.php/employee/view_sign_pic'?>");
				}
			}
		});	
	}
</script> 

<script>
	function submitFile1()
	{
		var img=$("#userfile").val();
		if(img!="")
		{
			var filesize=$("#userfile")[0].files[0].size;
			if(filesize < 1024 * 1024 * 2)
			{
				var formUrl = "<?php echo base_url(); ?>index.php/employee/uploadfile1";
				var formData = new FormData($('#form_sample_1')[0]);
				//alert($('.horizontal-form')[0]);
				$.ajax({
					url: formUrl,
					type: 'POST',
					data: formData,
					mimeType: "multipart/form-data",
					contentType: false,
					cache: false,
					processData: false,
					success: function(data){
						//alert(data);
						
						if(data!="")
						{
							$('#txtsignname').val(data);
							save1();
						}
						else
						{
							alert('There are some error on this page');
						}
					},
					error: function(jqXHR, textStatus, errorThrown){
						//handle here error returned
					}
				});
			}
			else
			{
				alert("File Size must be less than 2 mb");
			}
		}
		else
		{
			alert("Please upload a image first");
		}
	}
</script> 
<script>
	function save1()
	{
		//alert("ff");
		var data={file1:$('#txtsignname').val(),txtpro_id:$('#txtpro_id').val()};
		//alert($('#txtsignname').val());
		//alert('Image uploaded');
		$.ajax(
		{
			type: "POST",
			url:"<?php echo base_url();?>index.php/employee/insert_sign_pic/",
			mimeType:"multipart/form-data",
			data:data,
			success: function(msg){
				alert(msg);
				if(msg=='1')			
				{
					//alert("hello");
					$("#divload").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
					//$("#divload").load("<?php echo base_url().'index.php/employee/view_document'?>");
					window.location.href="<?php echo base_url();?>index.php/employee/view_employee/";
				}
			}
		});	
	}
</script> 


<script>
	function submitFile2()
	{
		var formUrl = "<?php echo base_url(); ?>index.php/employee/uploadfile2";
		var formData = new FormData($('#form_sample_2')[0]);
		//alert($('.horizontal-form')[0]);
		$.ajax({
			url: formUrl,
			type: 'POST',
			data: formData,
			mimeType: "multipart/form-data",
			contentType: false,
			cache: false,
			processData: false,
			success: function(data){
				//alert(data);
				var str = data;
				var res=str.split(",");
				var result = res[0];
				var values = res[1];
				//alert(result);
				//alert(values);
				//now get here response returned by PHP in JSON fomat you can parse it using JSON.parse(data)
				if(result==1)
				{
					$('#txtdocument').val(values);
					save2();
				}
				else
				{
					alert(values);
					
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				//handle here error returned
			}
		});
	}
</script> 
<script>
	function save2()
	{
		//alert("ff");
		var data={file1:$('#txtdocument').val(),txtpro_id:$('#txtpro_id').val()};
		//alert($('#txtsignname').val());
		$.ajax(
		{
			type: "POST",
			url:"<?php echo base_url();?>index.php/employee/insert_document/",
			mimeType:"multipart/form-data",
			data:data,
			success: function(msg){
				//alert(msg);
				if(msg=='1')			
				{
					//alert("hello");
					$(".page-content").html("<center><h2>Employee added successfully!</h2></center>")
					.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
					.hide()
					.fadeIn(1000,function()
					{
						$("#stylizeds").load("<?php echo base_url().'index.php/employee/after_create_employee'?>")
					)};
				}
			}
		});	
	}
</script> 

<script>
	function exit()
	{		
		if(confirm('Are you sure to Exit from Employee Registration?')==true)
		{
			$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
			$("#stylized").load("<?php echo base_url().'index.php/employee/after_view_employee'?>");
		}
	}
</script> 