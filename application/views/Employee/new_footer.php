<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<script src="<?php echo base_url() ?>application/libraries/tableToExcel.js"></script>
<script src="<?php echo base_url() ?>application/libraries/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/jspdf.debug.js"></script>


<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN: Page level plugins -->
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
<!-- BEGIN:File Upload Plugin JS files-->

<script type="text/javascript" src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>


<!-- The main application script -->
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo base_url() ?>application/libraries/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>application/libraries/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>application/libraries/assets/plugins/data-tables/DT_bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo base_url() ?>application/libraries/assets/plugins/ckeditor/ckeditor.js"></script>

<!-- END PAGE LEVEL PLUGINS -->


<!-- END: Page level plugins -->
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/core/app.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/index.js" type="text/javascript"></script>

<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/table-managed.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/components-pickers.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>

<script>
jQuery(document).ready(function() {       
   // initiate layout and plugins
      App.init();

   TableManaged.init();
   Index.initDashboardDaterange();
   ComponentsPickers.init();
   ComponentsEditors.init();
 
  
});
</script>
