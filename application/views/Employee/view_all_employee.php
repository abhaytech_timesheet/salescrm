<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="fa fa-user-plus"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/index/">Employee</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li><i class="fa fa-user"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_employee/">
							View All Registered Employee Here
						</a>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">View all Employees</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-settings font-dark"></i>
								<span class="caption-subject bold uppercase">View & Modify View All Employees</span>
							</div>
							
							<div class="actions">
								<div class="btn-group btn-group-devided" data-toggle="buttons">
									<a href="javascript:void(0)" onclick="create_employee()" class="btn green blue-stripe">
										<i class="fa fa-plus"></i>
											Create Employee 
									</a> 
									<a href="javascript:void(0)" onclick="create_employee_csv()" class="btn green blue-stripe" >
										<i class="fa fa-plus"></i> Upload CSV Data
									</a>
									<a href="#" onclick="modified_employee()" class="btn green blue-stripe" >
										View Modified Employee Details
									</a>
									
								</div>
							</div>
							<div class="tools"> </div><br>
							
							
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<h5> &nbsp;</h5>
										</div>
									</div>
									<div class="col-md-6">
										
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<th>S No.</th>
									<th class="ignore">Action</th>
									<th>Employee Code</th>
									<th>Full Name</th>
									<th>Designation</th>
									<th>Date of Birth</th>
									<th>Date Of Joining</th>
									<th>Contact No</th>
									<th>Email Address</th>
									<th>State/Province</th>
									<th>Profile Picture</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$sn=1;
									foreach($rec->result() as $row)
									{
									?>
									<tr>
										<td><?php echo $sn;?></td>
										<td class="ignore">
											<div class="btn-group">
												<button class="btn red btn-sm dropdown-toggle" data-toggle="dropdown">Action
													<i class="fa fa-angle-down"></i>
												</button>
												<ul class="dropdown-menu">
													<li>
														<a href="javascript:void(0)" onclick="edit_employee(<?php echo $row->or_m_reg_id;?>)" title="Edit"><i class="fa fa-pencil tooltips" data-placement="top" data-original-title="Edit Details" ></i> Edit</a>
													</li>
													<li>
														
														<a href="javascript:void(0)" onclick="upload_doccument(<?php echo $row->or_m_reg_id;?>)" title="Upload Document"><i class="fa fa-upload tooltips" data-placement="top" data-original-title="Upload Document" ></i>Upload Document</a>
													</li>
													<li>
														<a href="javascript:void(0)" onclick="upload_image(<?php echo $row->or_m_reg_id;?>)" title="Upload Image"><i class="fa fa-upload tooltips" data-placement="top" data-original-title="Upload Image" ></i>Upload Image</a>
														
													</li>
													
												</ul>
											</div>
											
										</td>
										<td><?php echo $row->or_m_user_id; ?></td>
										<td><?php echo $row->or_m_name; ?></td>
										<td><?php echo $row->m_des_name; ?></td>
										<td><?php echo $row->or_m_dob; ?></td>
										<td><?php echo $row->or_member_joining_date; ?></td>
										<td><?php echo $row->or_m_mobile_no; ?></td>
										<td><?php echo $row->or_m_email; ?></td>
										<td><?php echo $row->or_m_address; ?></td>
										<td><a href="<?php echo base_url(); ?>application/emp_pics/<?php echo $row->or_m_userimage; ?>" target="_blank"><?php echo $row->or_m_userimage; ?></a></td>
									</tr>
									<?php
										$sn++;
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
				
			</div>
		</div>
		
	</div>
	<!-- END CONTENT BODY -->
</div>
</div>
<!-- END CONTENT -->	

<script>
	function create_employee()
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url()?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url()?>index.php/employee/create_employee");
		
	}
</script> 
<script>
	function upload_doccument(id)
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url()?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url()?>index.php/employee/view_upload_doccument/"+id);
		
	}
</script> 
<script>
	function upload_image(id)
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url()?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url()?>index.php/employee/view_upload_profile_image/"+id);
		
	}
</script>
<script>
	function edit_employee(id)
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url()?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url()?>index.php/employee/view_edit_employee/"+id);
		
	}
</script> 
