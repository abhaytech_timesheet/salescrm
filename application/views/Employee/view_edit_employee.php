<!-- BEGIN CONTENT -->
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="fa fa-user-plus"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/index/">Employee</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-user"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_employee/">
							View All Registered Employee Here
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<span> Registration</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Edit Employee  </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Edit Employee  </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<?php
								foreach($resul->result() as $row)
								{
									break;	
								} 
							?>
							<form method="post" action="#" id="record">
								<div class="form-body">
									
									<h4 class="caption-subject font-blue bold uppercase"> Basic Information </h4>
									
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Employee Name</label>
												<input type="text" class="form-control input-sm empty" placeholder="Enter Employee Name" value="<?php echo $row->or_m_name; ?>" name="txtemp_name" id="txtemp_name">
												<span id="divtxtemp_name" style="color:red"></span>	                   
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Employee Code</label>
												<input type="text" class="form-control input-sm empty" placeholder="Enter Employee Code" value="<?php echo $row->or_m_user_id; ?>" readonly="readonly" name="txtemp_code" id="txtemp_code">
												<span id="divtxtemp_code" style="color:red"></span>	  
											</div>
										</div>
									</div>	
									
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Gender </label>
												<select class="form-control input-sm opt" name="ddgender" id="ddgender">
													<option value="-1">Select Gender</option>
													<option value="1">Male</option>
													<option value="2">Female</option>
												</select>
												<span id="divddgender" style="color:red"></span>	
												<script>
													document.getElementById('ddgender').selectedIndex="<?php echo $row->or_m_gender;?>";
                                                    document.getElementById('dddesignation').selectedIndex="<?php echo $row->or_m_designation;?>";
													document.getElementById('ddweekoff').selectedIndex="<?php echo $row->m_extra_weekoff;?>";
													document.getElementById('ddworkexp').selectedIndex="<?php echo $row->emp_exp;?>";
												</script>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Date Of Birth <span class="required"> * </span></label>
												<input type="text" name="txtdob" id="txtdob" data-date-format="yyyy-mm-dd" class="form-control input-sm date-picker" placeholder="yyyy-mm-dd" value="<?php $date=date_create($row->or_m_dob); echo date_format($date,'Y-m-d')?>">
												<span id="divtxtdob" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Date Of Joining</label>
												<input type="text" class="form-control input-sm date-picker" placeholder="yyyy-mm-dd" disabled="disabled" value="<?php echo $row->or_member_joining_date;?>" name="txtdoj" id="txtdoj" data-date-format="yyyy-mm-dd">
												<span id="divtxtdoj" style="color:red"></span>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label"> Email ID </label>
												<input type="text" placeholder="Enter Your Email ID" name="txtmail" id="txtmail" value="<?php echo $row->or_m_email; ?>" class="form-control input-sm">
												<span id="divtxtmail" style="color:red"></span>	
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Contact Number </label>
												<input type="text" placeholder="Enter Your Mobile No" class="form-control input-sm" maxlength="10" value="<?php echo $row->or_m_mobile_no; ?>" name="txtcontact" id="txtcontact">
												<span id="divtxtcontact" style="color:red"></span>
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">State</label>
												<input type="hidden" id="txtstate" name="txtstate" class="form-control input-inline input-medium"  value="<?php echo $row->or_m_state;?>"/>
												<input type="hidden" id="txtcity" name="txtcity" class="form-control input-inline input-medium"  value="<?php echo $row->or_m_city;?>"/>
												
												<div id="state">
													<select class="form-control input-sm opt" name="ddstate" id="ddstate" onchange="get_city()">
														<option value="">Select State</option>
														
													</select>
													
												</div>
												<span id="divddstate" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">City </label>
												<div id="city">
													<select class="form-control input-sm opt" name="ddcity" id="ddcity">
														<option value="">Select City</option>
													</select>
												</div>
												<span id="divddcity" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Pincode </label>
												<input type="text"placeholder="Enter Your Pincode" name="txtpincode" id="txtpincode" value="<?php echo $row->or_m_pincode; ?>" maxlength="6" class="form-control input-sm empty">
												<span id="divtxtpincode" style="color:red"></span>
											</div>
										</div>
										
										<!--row--> 
									</div>
									<div class="row">
										<div class="col-md-8">
											<div class="form-group">
												<label class="control-label">Address </label>
												<textarea placeholder="Enter Your Address" name="txtaddress" id="txtaddress" class="form-control input-sm empty"><?php echo $row->or_m_address; ?></textarea>
												<span id="divtxtaddress" style="color:red"></span>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Designation <span class="required">*</span></label>
												<select class="form-control input-sm opt" name="dddesignation" id="dddesignation" onchange="get_spcl()" >
													<option value="-2">Select Designation</option>
													<?php
														foreach($desig->result() as $desigrow)
														{
															if($row->or_m_designation==$desigrow->m_des_id)
															{
															?>
															<option value="<?php echo $desigrow->m_des_id ?>" selected="selected"><?php echo $desigrow->m_des_name; ?></option>
															<?php
															}
															else
															{
															?>
															<option value="<?php echo $desigrow->m_des_id ?>"><?php echo $desigrow->m_des_name ?></option>
															<?php 
															}
														}
													?>
												</select>
												<span id="divdddesignation" style="color:red"></span>
											</div>
										</div>
										
										<input type="hidden" id="txtspcl1" name="txtspcl1" value="<?php echo $row->emp_spcl;?>">
										<div class="col-md-4" id="1">
											<div class="form-group">
												<label class="control-label">Specialization</label>
												<div id="spcl">
													<select class="form-control input-sm opt" name="ddspcl" id="ddspcl">
														<option value="-1">Select Specialization</option>
													</select>
													
												</div>
												<span id="divddspcl" style="color:red"></span>
											</div>
										</div>
										
										<div class="col-md-4" id="2">
											<div class="form-group">
												<label class="control-label">Work Experience<span class="required"> * </span></label>
												<select id="ddworkexp" name="ddworkexp" class="form-control input-sm opt">
													<option value="0.1">Fresher</option>
													<option value="1">1 Year</option>
													<option value="2">2 Year</option>
													<option value="3">3 Year</option>
													<option value="4">4 Year</option>
													<option value="5">5 Year</option>
													<option value="6">More</option>
												</select>
												<span id="divddworkexp" style="color:red"></span>
											</div>
										</div>
										
									</div>
									<!--/span--> 
									<div class="row">
										<div class="col-md-4" id="3">
											<div class="form-group">
												<label class="control-label">Qualification <span class="required"> * </span></label>
												<select class="form-control input-sm opt" name="ddqualification" id="ddqualification">
													<option value="">Select Qualification</option>
													<?php
														foreach($quali->result() as $row10)
														{
															if($row10->quali_id==$row->emp_quali)
															{
															?>
															<option value="<?php echo $row10->quali_id?>" selected="selected"><?php echo $row10->quali_name; ?></option>
															<?php
															}
															else
															{
															?>
															<option value="<?php echo $row10->quali_id?>"><?php echo $row10->quali_name; ?></option>
															<?php
															}
														}
													?>
												</select>
												<span id="divddqualification" style="color:red"></span>
											</div>
										</div>
									</div>
									<!--row--> 
									<h4 class="caption-subject font-blue bold uppercase"> Shift Information </h4>
									
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Select Shift Type </label>
												<select class="form-control input-sm opt" name="ddshift_type" id="ddshift_type" onchange="get_shift()">
													<option value="">Select Shift</option>
													<?php
														foreach($shift->result() as $rowsh)
														{
															if($rowsh->m_shift_id==$row->m_extra_shiftid)
															{
															?>
															<option selected="selected" value="<?php echo $rowsh->m_shift_id ?>"><?php echo $rowsh->m_shift_name; ?></option>
															<?php
															}
															else
															{
															?>
															<option value="<?php echo $rowsh->m_shift_id ?>"><?php echo $rowsh->m_shift_name; ?></option>
															<?php }
														}
													?>
												</select>
												<span id="divddshift_type" style="color:red"></span>
											</div>
										</div>
										<!--row-->
										<div id="shift_div" style="display:none">
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">Shift Start Time </label>
													<input type="text"placeholder="Enter Your Pincode" name="txtshift_strart_time" id="txtshift_strart_time" class="form-control input-sm" readonly value="">
													<span id="divddshift_type" class="help-inline" style="color:red"></span>
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">Shift End Time </label>
													<input type="text"placeholder="Enter Your Pincode" name="txtshift_end_time" id="txtshift_end_time" class="form-control input-sm" readonly value="">
													<span id="divddshift_type" class="help-inline" style="color:red"></span>
												</div>
											</div>
											
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">Shift Duration </label>
													<input type="text"placeholder="Enter Your Pincode" name="txtshift_duration" id="txtshift_duration" class="form-control input-sm" readonly value="">
													<span id="divddshift_type" class="help-inline" style="color:red"></span>
												</div>
											</div>
										</div>
									</div>
									<h4 class="caption-subject font-blue bold uppercase"> Week Off </h4>
									
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Select Week Off Day </label>
												<select class="form-control input-sm opt" name="ddweekoff" id="ddweekoff">
													<option value="-1">Select Week Off</option>
													<option value="1">Monday</option>
													<option value="2">Tuesday</option>
													<option value="3">Wednesday</option>
													<option value="4">Thursday</option>
													<option value="5">Friday</option>
													<option value="6">Saturday</option>
													<option value="7">Sunday</option>                       
												</select>
												<script>
													
												</script>
												<span id="divddshift_type" style="color:red"></span>
											</div>
										</div>
										
										<!--row-->
									</div>
									
									<!--/span-->
									
									<h4 class="caption-subject font-blue bold uppercase"> Account Information </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Account Name <span class="required"> * </span> </label>
												<input type="text" placeholder="Enter Account Name" class="form-control input-sm empty" value="<?php echo $row->bank_user_name; ?>" name="txtacc_name" id="txtacc_name">
												<span id="divtxtacc_name" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label"> Account Number <span class="required"> * </span> </label>
												<input type="text" placeholder="Enter Account No" class="form-control input-sm empty" value="<?php echo $row->or_m_b_cbsacno; ?>" name="txtacc_num" id="txtacc_num">
												<span id="divtxtacc_num" style="color:red"></span>
											</div>
										</div>
										
										<!--row-->
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Bank Name </label>
												<select class="form-control input-sm opt" name="ddbank_name" id="ddbank_name">
													<?php
														foreach($bank->result() as $row8)
														{
															if($row8->m_bank_id==$row->or_m_b_name)
															{
															?>
															<option value="<?php echo $row8->m_bank_id; ?>" selected="selected"><?php echo $row8->m_bank_name; ?></option>
															<?php
															}
															else
															{
															?>
															<option value="<?php echo $row8->m_bank_id; ?>"><?php echo $row8->m_bank_name; ?></option>
															<?php
															}
														}
													?>
												</select>
												<span id="divddbank_name" style="color:red"></span>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Bank Branch </label>
												<input type="text" class="form-control input-sm empty" placeholder="Enter Your Bank Branch"  value="<?php echo $row->or_m_b_branch; ?>" name="txtbank_branch" id="txtbank_branch"/>
												<span id="divtxtbank_branch" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Bank IFSC <span class="required"> * </span> </label>
												<input type="text" class="form-control input-sm empty" placeholder="Enter Bank IFSC Code"  value="<?php echo $row->or_m_b_ifscode; ?>" name="txtbank" id="txtbank"/>
												<span id="divtxtbank" style="color:red"></span>
											</div>
										</div>
										
										<!--row-->
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">MMID Code </label>
												<input type="text" class="form-control input-sm empty" placeholder="Enter MMID Code"  value="<?php echo $row->or_m_b_imps; ?>" name="txtmmid" id="txtmmid"/>
												<span id="divtxtmmid" style="color:red"></span>
											</div>
										</div>
									</div>
									<input type="hidden" value="<?php echo $id ?>" name="txtid" id="txtid"/>
								</div>
								<div id="function" class="form-actions left">
									<button type="reset" class="btn default">Cancel</button>
									<input type="button" onclick="update()" class="btn blue" value="Submit" />
								</div>
							</form>
							<!-- END FORM--> 
							
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	

<script>
	function abc()
	{
		var state_id=$("#txtstate").val();
		var country_id=1;
		$.ajax(
		{
			type:"POST",
			url:"<?php echo base_url();?>employee/select_state/"+country_id+"/"+state_id,
			dataType: "JSON",
			data:{'state_id':state_id},
			success:function(data){
				$("#ddstate").empty();
				$("#ddstate").append("<option value=-1> ~~ Select ~~</option>");
				
				$.each(data,function(i,item)
				{
				   
				    if(state_id==item.m_loc_id)
					{
						$("#ddstate").append("<option value="+item.m_loc_id+" selected>"+item.m_loc_name+"</option>");
						
					}
					else
					{
						$("#ddstate").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
					}
				});
			}
		});
		
		var city_id=$("#txtcity").val();
		$("#ddcity").empty();
		
		$.ajax(
		{
			type:"POST",
			url:"<?php echo base_url();?>employee/select_city/"+state_id+"/"+city_id,
			dataType: "JSON",
			data:{'state_id':state_id},
			success:function(data){
				
				$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
				$.each(data,function(i,item)
				{
				    if(city_id==item.m_loc_id)
					{
						$("#ddcity").append("<option value="+item.m_loc_id+" selected>"+item.m_loc_name+"</option>");
						
					}
					else
					{
						$("#ddcity").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
					}
				});
			}
		});
		
		var spcl_id=$("#dddesignation").val();
		var spcl_id1=$("#txtspcl1").val();
		$.ajax(
		{
			type:"POST",
			url:"<?php echo base_url();?>employee/spcl/"+spcl_id+"/"+spcl_id1,
			dataType: "JSON",
			data:{'spcl_id':spcl_id},
			success:function(data){
				$("#ddspcl").empty();
				$("#ddspcl").append("<option value=-1> ~~ Select ~~</option>");
				$.each(data,function(i,item)
				{
				  
					if(spcl_id1==item.m_des_id)
					{
						$("#ddspcl").append("<option value="+item.m_des_id+" selected>"+item.m_des_name+"</option>");
					}
					else
					{
						$("#ddspcl").append("<option value="+item.m_des_id+">"+item.m_des_name+"</option>");
					}
				});
			}
		});
		
		var shift_id=$("#ddshift_type").val();
		
		$.ajax(
		{
			type:"POST",
			url:"<?php echo base_url();?>employee/select_shift/"+shift_id,
			dataType: "JSON",
			data:{'shift_id':shift_id},
			success:function(data){
				if(data!='')
				{
					$("#shift_div").css('display','block');
					$.each(data,function(i,item)
					{
						$("#txtshift_strart_time").val(item.m_shift_starttime);
						$("#txtshift_end_time").val(item.m_shift_endtime);
						$("#txtshift_duration").val(item.m_shift_duration);
					});
				}
				else
				{
					$("#shift_div").css('display','none');
					$.each(data,function(i,item)
					{
						$("#txtshift_strart_time").val('');
						$("#txtshift_end_time").val('');
						$("#txtshift_duration").val('');
					});
				}
			}
		});
		
	}
</script>
<script>
	get_spcl();
	function get_spcl()
	{
		var spcl_id=$("#dddesignation").val();
		var spcl_id1=$("#txtspcl1").val();
		if(spcl_id!=1)
		{
			$('#1').show();
			$('#2').show();
			$('#3').show();
			//$("#spcl").html('<div><img src ="<?php echo base_url();?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
			
			$.blockUI({ message: '<h3>Please Wait...</h3>' });
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>employee/spcl/"+spcl_id,
				dataType: "JSON",
				data:{'spcl_id':spcl_id},
				success:function(data){
				    $("#ddspcl").empty();
				    setTimeout($.unblockUI, 01);
					$("#ddspcl").append("<option value=-1> ~~ Select ~~</option>");
					$.each(data,function(i,item)
					{
					    if(spcl_id1==item.m_des_id)
						{
							$("#ddspcl").append("<option value="+item.m_des_id+" selected>"+item.m_des_name+"</option>");
						}
						else 
						{
							$("#ddspcl").append("<option value="+item.m_des_id+">"+item.m_des_name+"</option>");
						}
						
					});
				}
			});
			
			//$("#spcl").load("<?php echo base_url();?>employee/spcl/"+spcl_id);
		}
		else
		{
			$('#1').hide();
			$('#2').hide();
			$('#3').hide();
		}
	}
</script>

<script>
	function get_shift()
	{
		var shift_id=$("#ddshift_type").val();
		$.blockUI({ message: '<h3>Please Wait...</h3>' });
		$.ajax(
		{
			type:"POST",
			url:"<?php echo base_url();?>employee/select_shift/"+shift_id,
			dataType: "JSON",
			data:{'shift_id':shift_id},
			success:function(data){
				if(data!='')
				{
					$("#shift_div").css('display','block');
					setTimeout($.unblockUI, 01);
					$.each(data,function(i,item)
					{
						$("#txtshift_strart_time").val(item.m_shift_starttime);
						$("#txtshift_end_time").val(item.m_shift_endtime);
						$("#txtshift_duration").val(item.m_shift_duration);
					});
				}
				else
				{
					$("#shift_div").css('display','none');
					setTimeout($.unblockUI, 01);
					$.each(data,function(i,item)
					{
						$("#txtshift_strart_time").val('');
						$("#txtshift_end_time").val('');
						$("#txtshift_duration").val('');
					});
				}
			}
		});
	}
</script>

<script>
	function get_city()
	{
		var state_id=$("#ddstate").val();
		$("#ddcity").empty();
		if(state_id!='-1' && state_id!='')
		{
	        $.blockUI({ message: '<h3>Please Wait...</h3>' });
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>employee/select_city/"+state_id+"/0",
				dataType: "JSON",
				data:{'state_id':state_id},
				success:function(data){
				    setTimeout($.unblockUI, 01);
					$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
					$.each(data,function(i,item)
					{
						$("#ddcity").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
					});
				}
			});
		}
		else
		{
			$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
			alert("Please First Select The State");
		}
		
	}
</script>


<script>
	abc();
</script>
<script>
	function update()
	{
		
		/*if(check('record'))
		{*/
		bootbox.confirm('Are you sure to Submit from?', function(result){
			if(result==true)
			{
				$("#function").html("<div>Employee Updation has been processed.<img id='checkmark' src='<?php echo base_url(); ?>/application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></div>");
				formData=$("#record").serialize();
				$.ajax(
				{
					type: "POST",
					url:"<?php echo base_url();?>employee/update_employee/",
					data:formData,
					success: function(msg) {
						
						if(msg!="")
						{
							$(".page-content").html("<center><h2>"+msg+"</h2></center>")
							.append("<center><img id='checkmark' src='<?php echo base_url(); ?>/application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif'/></center>")
							.hide()
							.fadeIn(2000,function()
							{
								window.location.href="<?php echo base_url();?>employee/view_employee/";
							});
							
						}	  
					}
				});
			}
		}); 
		/*}
			else
			{
			return false;
		}*/
	}
</script>