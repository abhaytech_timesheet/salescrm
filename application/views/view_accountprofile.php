<?php 
foreach($acc->result() as $row)
{}
?>
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					View Account Profile
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" >
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<!-- <i class="fa fa-angle-down"></i> -->
							</div>
						</li>
						<li>
							<i class="fa fa-home"></i>
							<a href="#">
								CRM
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?php echo base_url();?>index.php/crm/view_lead">
								View Account Profile
							</a>
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
				<div class="row profile">
				<div class="col-md-12">
					<!--BEGIN TABS-->
					<div class="tabbable tabbable-custom tabbable-full-width">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_1_1" data-toggle="tab">
									 Overview
								</a>
							</li>
							<li>
								<a href="#tab_1_3" data-toggle="tab">
									 Account
								</a>
							</li>
							<li>
								<a href="#tab_1_4" data-toggle="tab">
									 Projects
								</a>
							</li>
							<li>
								<a href="#tab_1_6" data-toggle="tab">
									 Help
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1_1">
								<div class="row">
									<div class="col-md-3">
										<ul class="list-unstyled profile-nav">
											<li>
												<?php
													if($row->account_image!="")
													{
														$img=base_url()."application/uploadimage/".$row->account_image;
													}
													else
													{
														$img=base_url()."application/libraries/assets/download1.jpg";
													}
												?>
												<img src="<?php echo $img; ?>" class="img-responsive" alt=""/>
											</li>
											
										</ul>
									</div>
									<div class="col-md-9">
										<div class="row">
											<div class="col-md-8 profile-info">
												<h1><?php echo $row->account_name; ?></h1>
												<p>
													 <?php echo $row->account_desc; ?>
												</p>
												<p>
													<a href="#">
														 <?php echo $row->account_email; ?>
													</a>
												</p>
												<ul class="list-inline">
													<li>
														<i class="fa fa-phone"></i> <?php echo $row->account_phone; ?>
													</li>
													<li>
														<i class="fa fa-map-marker"></i>  
														<?php 
														foreach($loc->result() as $l)
														{
															if($row->account_city==$l->m_loc_id)
															{
																echo $l->m_loc_name;
															}
														}
														echo '&nbsp; , '.$row->account_address; ?>
													</li>
													<li>
														<i class="fa fa-calendar"></i> <?php echo $row->account_regdate; ?>
													</li>
													
													
													
													<!--
                                                    <li>
														<i class="fa fa-star"></i> Top Seller
													</li>
													<li>
														<i class="fa fa-heart"></i> BASE Jumping
													</li>
                                                    -->
												</ul>
											</div>
											<!--end col-md-8-->
											<div class="col-md-4">
												<div class="portlet sale-summary">
													
													<div class="portlet-body">
														<ul class="list-unstyled">
															
															
															
															
														</ul>
													</div>
												</div>
											</div>
											<!--end col-md-4-->
										</div>
										<!--end row-->
										<div class="tabbable tabbable-custom tabbable-custom-profile">
											<ul class="nav nav-tabs">
												<li class="active">
													<a href="#tab_1_11" data-toggle="tab">
														 Generated Ticket
													</a>
												</li>
												<li>
													<a href="#tab_1_22" data-toggle="tab">
														 Announcements
													</a>
												</li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane active" id="tab_1_11">
													<div class="portlet-body">
														<table class="table table-striped table-bordered table-advance table-hover">
														<thead>
														<tr>
															<th>
																<i class="fa fa-ticket"></i> Ticket No
															</th>
															<th>
																Subject
															</th>
															
															<th>
																Action
															</th>
														</tr>
														</thead>
														<tbody>
														 <?php
															 foreach($ticket->result() as $tic)
															 {
																if($tic->txtemail==$row->account_email)
																{
														?>
														<tr>
															<td>
																<a href="#">
																	<?php
																		echo '#'.$tic->ticket_no;
																	?>
																</a>
															</td>
															<td>
															<a href="#">
																	<?php
																		echo $tic->txtsubject;
																	?>
																</a>
															</td>
															<td>
																<a class="btn default btn-xs blue-stripe" href="<?php echo base_url();?>index.php/admin/view_ticket/<?php echo $tic->ticket_no ?>">
																	 View
																</a>
															</td>
														</tr>
														<?php
															}
														 }
														 ?>
														</tbody>
														</table>
													</div>
												</div>
												<!--tab-pane-->
												<div class="tab-pane" id="tab_1_22">
													<div class="tab-pane active" id="tab_1_1_1">
														<div class="scroller" data-height="200px" data-always-visible="1" data-rail-visible1="1">
															<ul class="feeds">
															<?php  
																foreach($anonce->result() as $row12)
																{	
															 ?>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-success">
																					<i class="fa fa-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					 <?php echo $row12->txttitle; ?>
																				</div>
																			</div>
																		</div>
																	</div>
																</li>
																<?php
																	}
																 ?>
															</ul>
														</div>
													</div>
												</div>
												<!--tab-pane-->
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--tab_1_2-->
							<div class="tab-pane" id="tab_1_3">
								<div class="row profile-account">
									<div class="col-md-3">
										<ul class="ver-inline-menu tabbable margin-bottom-10">
											
											<li class="active">
												<a data-toggle="tab" href="#tab_2-2">
													<i class="fa fa-picture-o"></i> Change Account Image
												</a>
											</li>
											
										</ul>
									</div>
									<div class="col-md-9">
										<div class="tab-content">
											
											<div id="tab_2-2" class="tab-pane active">
												<h4>
													 Add a Profile Picture
												</h4>
												<form role="form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/crm/change_account_image/<?php echo $this->uri->segment(3); ?>" method="post">
													<div class="form-group">
														<div class="fileinput fileinput-new" data-provides="fileinput">
															<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
																<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
															</div>
															<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
															</div>
															<div>
																<span class="btn default btn-file">
																	<span class="fileinput-new">
																		 Select image
																	</span>
																	<span class="fileinput-exists">
																		 Change
																	</span>
																	<input type="file" name="userfile" id="userfile">
																</span>
																<a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
																	 Remove
																</a>
															</div>
														</div>
														
													</div>
													<div class="margin-top-10">
														<input type="submit" name="submit" value="Submit" class="btn green" />
														<a href="#" class="btn default">
															 Cancel
														</a>
													</div>
												</form>
											</div>
											
											
										</div>
									</div>
									<!--end col-md-9-->
								</div>
							</div>
							<!--end tab-pane-->
							<div class="tab-pane" id="tab_1_4">
								<div class="row">
									<div class="col-md-12">
										<div class="add-portfolio">
											<span>
												 Take an Action
											</span>
											<a href="<?php echo base_url() ?>index.php/crm/view_account_project/<?php echo $this->uri->segment(3); ?>" class="btn icn-only green">
												 Add a new Project <i class="m-icon-swapright m-icon-white"></i>
											</a>
										</div>
									</div>
								</div>
								<!--end add-portfolio-->
                                <?php 
								foreach($pro->result() as $row4)
								{ ?>
								<div class="row portfolio-block">
									<div class="col-md-3 portfolio-text">
										<div class="portfolio-text-info">
											<h4><?php echo $row4->m_project_name; ?></h4>
											<p>
												 <?php echo substr($row4->m_project_name,0,50); ?>
											</p>
										</div>
									</div>
									<div class="col-md-7 portfolio-stat">
										<div class="portfolio-info">
											 Project Id
											<span>
												 <?php echo $row4->m_project_sno; ?>
											</span>
										</div>
										
										<div class="portfolio-info">
											 Earns
											<span>
												 $21.700
											</span>
										</div>
									</div>
									
								</div>
                                
                                <?php } ?>
								<!--end row-->
							</div>
							<!--end tab-pane-->
							<div class="tab-pane" id="tab_1_6">
								<div class="row">
									<div class="col-md-3">
										<ul class="ver-inline-menu tabbable margin-bottom-10">
											
											<li class="active">
												<a data-toggle="tab" href="#tab_2">
													<i class="fa fa-columns"></i> Return Policy
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_3">
													<i class="fa fa-columns"></i> Refund Policy
												</a>
											</li>
											
											
											
										</ul>
									</div>
									<div class="col-md-9">
										<div class="tab-content">
											
											<div id="tab_2" class="tab-pane active">
												<div id="accordion2" class="panel-group">
													<div class="panel panel-warning">
														<div class="panel-heading">
															<h4 class="panel-title">
															<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_1">
																 1. Return policy
															</a>
															</h4>
														</div>
														<div id="accordion2_1" class="panel-collapse collapse in">
															<div class="panel-body">
																<p>
																	 <b>At</b> FerryIPL we work very hard to deliver the best services/products and shopping experience to you. However, sometimes because of the scale of operation a few exceptional cases do happen. If for any reason you do not feel satisfied with the services/product offered to you then please get in touch with our Customer Service team for an appropriate resolution.

We will try our best to solve the issues to your satisfaction and if that is not possible because of some operational constraint then we will provide full refund as per the requirement. In all such circumstances, before using the service, please get in touch with our Customer Service Team who will guide you on the process for the same.

To make things fair for all of us and provide you with the benefits of the Return Policy, we adhere to below mention points.<br>
<b><h5>What to do if I want Replacement/Refund?</h5></b><br>
1.Give us a call on 09721744999 or drop an email at savy@ferryipl.com within 48 hours of delivery.<br>

2.Register your complaint and get a Complaint ID by the customer service team. This is mandatory information which is needed to know about the status of the complaint and for returning the Product/Service.<br>

3.Please mention the following details.<br>
<b>Complaint ID:</b> (As provided by the customer care) for the Return.<br>

<b>Order #:</b> Number generated at the time of order Placement.<br>

<b>Reason for Return:</b> Please choose a reason from the below mentioned list.<br>
� Defective Service<br>

� Improper functioning<br>

� Incorrect Service Delivered<br>

� Others- If others then please specify (Please mention the reason in Hindi /English only).<br>
<b>Your full Contact Details:</b> Your name, address, phone number and email address.<br>

<b>DOA (DEAD ON ARRIVAL) certificate:</b> Needed only if you are returning a Defective Manufacture warranty product.<br>

<b>Picture of Damaged Product:</b> Needed only if you are returning a Damaged Manufacture warranty product.<br><br>


You have to return it to the following Address:<br>

<b>Ferry Infotech Pvt Limited,</b>

128, 3rd Floor, Sekhpura Kasaila, Lekhraj, Lucknow, U.p.- 226016<br><br>


4. Send an email to savy@ferryipl.com please mention the Order # and the Complaint ID clearly in email. This email is required to reimburse you for the courier charges you paid for returning the product.<br>
<br><b>What is the return procedure if I have received a wrong product?</b><br>

In case if you have received a wrong Service/product/Application from our side then please register a complaint within 24 Hrs of delivery to start the refund/replacement of your product.<br></br>
<b>What are the time lines provided for the Replacement/Refund?</b><br>

Once we have received your complaint, it would be sent for a Quality check to our Replacement Verification team. The replacement process would be initiated within 7 business days of receipt of the product.<br><br>
<b>What is the Payment Mode for Refund?</b><br>

It would remain the same as paid by you while placing an order. If you paid online, then it will be returned online, if you paid by Cash of Delivery option then it would be refunded to you via Demand Draft which will be sent to your address as mentioned while returning the product.


																</p>
															</div>
														</div>
													</div>
													
													
													
													
													
												</div>
											</div>
											<div id="tab_3" class="tab-pane">
												<div id="accordion3" class="panel-group">
													<div class="panel panel-danger">
														<div class="panel-heading">
															<h4 class="panel-title">
															<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_1">
																 Refund Policy
															</a>
															</h4>
														</div>
														<div id="accordion3_1" class="panel-collapse collapse in">
															<div class="panel-body">
																<p>
																<b>We </b>thank you and appreciate your purchase with us. Please read the policy, conditions and process carefully as they will give you important information and guidelines about your rights and obligations as our customer, concerning any purchase you make through us unless specifically stated otherwise on our product/Services pages or readers offer advertisements. We make every effort to service the order/s placed with us as per the specifications and timelines mentioned against each product. If due to any reason, unavoidable circumstances or beyond the limitations of the merchants the order is not delivered by us then the order shall be cancelled and refunded. In the event the order/product/services is delivered and has been cancelled for refund due to product fault, delivery of wrong product, partial product, etc the refund is processed.<br></br>
																<b>The order may be cancelled before Commencement/shipment due to any of the following reasons:</b><br>

1.The specific product/model is out dated or can�t be customised by us.<br>

2.The order could not be delivered within the specified timeline for solely our fault.<br>

3.On customers request the order is processed but is not serviceable.<br>

4.Any other reason beyond the control of FerryIPL.<br><br>
<b>The orders may be cancelled post shipment and delivery due to any of the following reasons:</b><br>

Please connect with our customer care via email or call.<br></br>
1.	The customer care team will validate the request by checking the timelines, product type, warranty terms, etc and shall take the request for refund or exchange.<br><br>
<b>Refund Timelines:</b><br>

Pay mode<br>

Credit Card<br>

Net Banking<br>

Debit Card<br>

COD -Cash On Delivery<br>

Demand draft/ Cheque<br><br>

<b>Refund Mode</b><br>

Credit Card<br>

Bank Account Online Transfer<br>

Cheque<br><br>
<b>Please note:</b> You can check the status of your refund in Your Account on the order's summary page.<br>

�We will process the refund after receipt of the product by Ferry Infotech Private Limited or its business partner. Refund will be processed based on the mode of payment of the order Refund processed through Credit card / Debit card will reflect in the next Credit Card/ Bank statement.<br>
�Refund Cheque will be issued in the billing name unless otherwise advised by customers.<br><br>

�In case the refund Cheque is lost by customer, Cheque will be reissued in15 days on the receipt of affidavit.<br><br>

�In case of incorrect name on the refund Cheque, a new Cheque will be reissued in 15 days on the receipt of original Cheque from the customer.<br><br>

�If your refund doesn't appear in Your Account, and the processing time for your payment method has passed, contact customer care for further assistance.<br><br>
																</p>
															</div>
														</div>
													</div>
													
													
												
													
													
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--end tab-pane-->
						</div>
					</div>
					<!--END TABS-->
				</div>
			</div>					
			<!-- END PAGE CONTENT-->
		
	</div>
	<!-- END CONTENT -->
   </div>
</div>
<!-- END CONTAINER -->