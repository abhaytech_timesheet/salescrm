<?php 
	foreach($rec->result() as $project)
	{
		break;
	}
?>
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-archive"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_project">
							Project
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="icon-list"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_all_task">
							Project Task
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-edit"></i>
						<span>
							Edit Project Task
						</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">Edit Task On Project</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs font-black"></i>
								<span class="caption-subject font-black bold uppercase">Edit Task On Project</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<form action="#" class="horizontal-form" method="post" id="insert_data">
								<div class="form-body">
									
									<h3 class="form-section">Task Information</h3>
									
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Task Subject</label>
												<input class="form-control empty" name="txtsubject" id="txtsubject" value="<?php echo $project->PTASK_TITLE ?>" type="text"/>
												<input name="txttaskid" id="txttaskid" value="<?php echo $project->PTASK_ID ?>" type="hidden"/>
												<span id="divtxtsubject" style="color:red"></span>
											</div>
										</div>
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Project Name</label>
												<select class="form-control opt" name="txtprojectname" id="txtprojectname" disabled="disabled" >
                                                <option value="-1">Select</option>
													<?php
														foreach($proj->result() as $p)
														{
														?>
														<option value="<?php echo $p->ACCP_ID; ?>" <?php echo ($project->PTASK_PROJECT_ID==$p->ACCP_ID ? 'selected':''); ?> ><?php echo $p->Account_name."-".$p->Website_name." / ".$p->Proj_name; ?></option>
														<?php
														}
													?>
                                                 </option>
                                                </select>
												<input name="txtprojectid" id="txtprojectid" type="hidden" value="<?php echo $project->PTASK_PROJECT_ID; ?>" readonly />
												<span id="divtxtprojectname" style="color:red"></span>
											</div>
										</div>
										
										<div class="col-md-2">
											<div class="form-group">
												<label class="control-label">Assigned By</label>
												<input type="text" class="form-control empty" name="txtassign" id="txtassign" value="<?php echo $project->ASSIGN_BY_NAME; ?>" readonly="readonly" />
												<input type="hidden" class="form-control" name="txtassign_id" id="txtassign_id" value="<?php echo $project->PTASK_ASSIGN_BY_ID; ?>" />
												<span id="divtxtassign" style="color:red"></span>
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Priority</label>
												<select class="form-control opt" name="ddpriority" id="ddpriority">
													<option value="-1">Select</option>
													<option <?php echo ($project->PTASK_PROIORTY_ID==1 ? 'selected':''); ?> value="1">High</option>
													<option <?php echo ($project->PTASK_PROIORTY_ID==2 ? 'selected':''); ?> value="2">Medium</option>
													<option <?php echo ($project->PTASK_PROIORTY_ID==3 ? 'selected':''); ?> value="3">Low</option>
												</select>
												<span id="divddpriority" style="color:red"></span>
					
											</div>
										</div>
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Due Date</label>
												<div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
													<input type="text" class="form-control empty" name="txtfrom" id="txtfrom" value="<?php echo $project->PTASK_SDATE; ?>">
													<span class="input-group-addon">
														to
													</span>
													<input type="text" class="form-control empty" name="txtto" id="txtto" value="<?php echo $project->PTASK_EDATE; ?>">
												</div>
												<span id="divtxtfrom" style="color:red"></span>
												<span id="divtxtto" style="color:red"></span>
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Assign To</label>
												<select class="form-control opt" name="ddassignto" id="ddassignto" disabled="disabled">
													<option value="-1"><?php echo $project->ASSIGN_TO_NAME; ?></option>
												</select>
												<span id="divddassignto" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Follower</label>
												<select class="form-control opt" name="ddfollower" id="ddfollower" disabled="disabled">
													<option value="-1"><?php echo $project->FOLLOWER_NAME; ?></option>
												</select>
												<span id="divddfollower" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
									</div>
									
									<h3 class="form-section">Reccurence</h3>
									
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Create Repeat Series of Task</label>
												<input type="checkbox" name="checkrec" value='1' id="checkrec" onclick="$(this).is(':checked') && $('#checked-a').slideDown('slow') || $('#checked-a').slideUp('slow');">                                        
											</div>
										</div>
									</div>
									
									<div class="dis-sub" style="display:none;" id="checked-a">
										<script>
											document.getElementById('ddschedule').selectedIndex="<?php echo $project->PTASK_SCHDULE;?>";
										</script>	
										<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">Set Schedule</label>
													<select class="form-control" name="ddschedule" id="ddschedule">
														<option value="0">Never</option>
														<option value="1">Daily</option>
														<option value="2">Weekly</option>
														<option value="3">Monthly</option>
														<option value="4">Yearly</option>
													</select>
												</div>
											</div>
											
										</div>
									</div>	
									
									<h3 class="form-section">Description Information</h3>
									
									<div class="row">
										<div class="col-md-8">
											<div class="form-group">
												<label class="control-label">Task Description</label>
												<textarea class="form-control empty" placeholder="Enter Task Description Information" name="txtcomment" id="txtcomment" style="font-size:20px" col="3"><?php echo $project->PTASK_DESCRIPTION;?></textarea>
												<span id="divtxtcomment" style="color:red"></span>
											</div>
										</div>
									</div>
									
								</div>
								
								<div class="form-actions left" id="function">
									<button type="button" value="submit" onclick="inserts('insert_data')" class="btn green"><i class="fa fa-check"></i> Save</button>
									<button type="button" onclick="goto('<?php echo base_url() ?>index.php/project/after_view_all_task/')" class="btn default">Cancel</button>
								</div>
								
							</form>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->
<script>
	var switch_url="<?php echo base_url();?>index.php/project/update_project_task",
	load_url="<?php echo base_url();?>index.php/project/after_view_all_task";
</script>