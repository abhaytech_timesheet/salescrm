<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-archive"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_project">
							Project
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-road"></i>
						<span>
							Followup Report
						</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Current Followup Report </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-book-open font-dark" ></i>
								<span class="caption-subject bold uppercase"> Current Followup Report </span>
							</div>
							<div class="tools"> 
							</div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S.No</th>
										<th>Project Name</th>
										<th>Task Title</th>
										<th>Response By</th>
										<th>Response By Name</th>
										<th>Description</th>
										<th>Response Date</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$SN=0;
										foreach($rec->result() as $row)
										{
											$SN++;
										?>
										<tr>
											<td><?php echo $SN; ?></td>
											<td><?php echo $row->m_project_name; ?></td>
											<td><?php echo $row->m_ptask_title; ?></td>
											<td><?php echo $row->or_m_user_id; ?></td>
											<td><?php echo $row->or_m_name; ?></td>
											<td><?php echo $row->task_description; ?></td>
											<td><?php echo $row->task_response_date; ?></td>
										</tr>
										<?php 
										}
									?>    
								</tbody>
							</table>
						</div>
						
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->