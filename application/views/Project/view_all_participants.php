<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-archive"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_project">
							Project
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-user-plus"></i>
						<span>
							Project Participants
						</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View Participants</h3>
			<!-- END PAGE TITLE-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-ticket font-dark" ></i>
								<span class="caption-subject bold uppercase">View & Modify View Participants</span>
							</div>
							<div class="actions">
								<div class="btn-group btn-group-devided" data-toggle="buttons">
									<a href="javascript:void(0)" >
										<span class="btn green btn-sm blue-stripe"  onclick="goto('<?php echo base_url();?>project/view_project_participants')">
											<i class="fa fa-plus"></i>
											Assign Project to Employee
										</span>
									</a>
									
								</div>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<h5> &nbsp;</h5>
										</div>
									</div>
									
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S.No</th>
										<th>Action</th>
										<th>Account-Website/Project Name</th>
										<th>Participant Name</th>
										<th>Designation</th>
										<th>Admin Rights</th>
										<th>Add Participants</th>
										<th>View Rights</th>
										<th>Update Rights</th>
										<th>Project Notification</th>
										<th>Task Notification</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$sn=1;
										foreach($rec->result() as $row)
										{
										?>
										<tr>
											<td><?php echo $sn; ?></td>
											<td nowrap> 
												<a href="javascript:void(0)" title="Edit" class="btn red btn-xs" onclick="goto('<?php echo base_url();?>project/edit_add_participants/<?php echo $row->PARICIPANT_ID; ?>')"><i class="fa fa-pencil-square-o"></i>Edit</a>
												<?php
													if($row->PARICIPANT_STATUS==1)
													{																	
													?>
													<a href="javascript:void(0)" class="btn red btn-xs" onclick="goto('<?php echo base_url();?>project/participants_status/<?php echo $row->PARICIPANT_ID; ?>/0')"><i class="fa fa-trash-o"></i> Disable</a>
													<?php 
													}
													else
													{
													?>
													<a href="javascript:void(0)" class="btn green btn-xs" onclick="goto('<?php echo base_url();?>project/participants_status/<?php echo $row->PARICIPANT_ID; ?>/1')"><i class="fa fa-check"></i> Enable</a>															
													<?php 
													}
												?>
											</td>
											<td nowrap><?php echo $row->ACCOUNT_NAME.'-'.$row->PROJ_WEB.'/'. $row->PROJ_NAME; ?></td>
											<td><?php echo $row->USER_NAME; ?></td>
											<td><?php echo $row->PARICIPANT_DESIG_NAME; ?></td>
											<td><?php echo $row->admin_right; ?></td>
											<td><?php echo $row->parti_add_participant; ?></td>
											<td><?php echo $row->parti_view_right; ?></td>
											<td><?php echo $row->parti_update_right; ?></td>
											<td><?php echo $row->parti_project_noti; ?></td>
											<td><?php echo $row->parti_task_noti; ?></td>
											<?php 
												$sn++;
											}
										?>    
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->