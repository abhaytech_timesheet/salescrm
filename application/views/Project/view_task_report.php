<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="#"> PROJECT MODULE </a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span> Project </span>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View Task Report </h3>
			<!-- END PAGE TITLE-->
			
			
			<div class="row">
				<div class="col-md-12">
					
					<div class="table-responsive" id="data1">
						<form method="post" action="">
							<table class="table table-striped table-bordered table-hover table-full-width" id="datatable_products">
								<thead>
									<tr role="row" class="heading">
										<th width="10%">
											Assign By
										</th>
										<th width="10%">
											Assign To
										</th>
										<th width="10%">
											Task Start Date
										</th>
										<th width="10%">
											Task End Date
										</th>
										<th width="10%">
											Status
										</th>
										<th width="10%">
											Priority
										</th>
										<th width="10%">
											Actions
										</th>

									</tr>
									
									<tr role="row" class="filter">
										<td>
											<select name="ddassignby" class="form-control input-sm">
												<option value="">Select...</option>
												<option value="0">Superadmin</option>
												<?php 
													foreach($user->result() as $row2)
													{
													?>
													<option value="<?php echo $row2->or_m_reg_id; ?>"><?php echo $row2->or_m_name; ?></option>
													<?php
													}
												?>
											</select>
										</td>
										
										<td>
											<select name="ddassignto" class="form-control input-sm">
												<option value="">Select...</option>
												<option value="0">Superadmin</option>
												<?php 
													foreach($user->result() as $row3)
													{
													?>
													<option value="<?php echo $row3->or_m_reg_id; ?>"><?php echo $row3->or_m_name; ?></option>
													<?php
													}
												?>
											</select>
										</td>
										
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control input-sm" readonly name="txtfromdate" id="txtfromdate" placeholder="From">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										
										<td>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control input-sm" readonly name="txttodate" id="txttodate" placeholder="To">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										
										<td>
											<select name="ddstatus" class="form-control input-sm">
												<option value="">Select...</option>
												<option value="1">Active</option>
												<option value="0">Deactive</option>
												<option value="2">Complete</option>
											</select>
										</td>
										
										<td>
											<select name="ddpriority" class="form-control input-sm">
												<option value="">Select...</option>
												<option value="1">High</option>
												<option value="2">Medium</option>
												<option value="3">Low</option>
											</select>
										</td>
										
										<td>
											<button class="btn btn-sm green filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
											<button type="reset" class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
										</td>
										
									</tr>
								</thead>
							</table>
						</form>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-book-open font-dark" ></i>
								<span class="caption-subject bold uppercase"> View Project Report</span>
							</div>
							<div class="tools"> 
							</div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								
								<thead>
									<tr>
										<th>Task Title</th>
										<th>Project Name</th>
										<th>Assign By</th>
										<th>Assign To</th>
										<th>Priority</th>
										<th>Start Date</th>
										<th>End Date</th>
										<th>Create Date</th>
<th>Description</th>
<th>Attachment</th>
									</tr>
								</thead>
								<tbody>
									
									<?php
										foreach($rec->result() as $row4)
										{
										?>
										<tr>
											<td><?php echo $row4->PTASK_TITLE; ?></td>
											<td><?php echo $row4->PROJECT_NAME; ?></td>
											<td><?php echo $row4->PTASK_ASSIGN_BY; ?></td>
											<td><?php echo $row4->ASSIGN_TO_NAME; ?></td>
											<td><?php echo $row4->PTASK_PROIORTY; ?></td>
											<td><?php echo $row4->PTASK_START_DATE; ?></td>
											<td><?php echo $row4->PTASK_END_DATE; ?></td>
											<td><?php echo $row4->PTASK_CREATE_DATE; ?></td>
<td><?php echo $row4->PTASK_DESCRIPTION; ?></td>
<td><?php echo $row4->PTASK_FILE; ?></td>
											<?php 
											}
										?>    
										
								</tbody>
							</table>
						</div>
							
					</div>
						
				</div>
					<!-- END PAGE CONTENT-->
			</div>
				
		</div>
			<!-- END CONTENT BODY -->
	</div>
</div>
	<!-- END CONTENT -->