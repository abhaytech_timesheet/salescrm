<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->

			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-archive"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_project">
							Project
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-user-plus"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_paricipants">
							Project Participants
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-user-plus"></i>
						<span>
							Add Participants
						</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">Add Participants </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Add Participants </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal" method="post" id="insert_data">
								<div class="form-body">
									
									<div class="form-group">
										<label class="col-md-4 control-label">Project Name</label>
										<div class="col-md-8">
											<select class="form-control select2 input-sm opt" name="txtprojectid" id="txtprojectid">
                                            	<option value="-1">Select</option>
												<?php 
													foreach($project->result() as $row2)
													{
													?>
													<option value="<?php echo $row2->ACCP_ACC_ID.'-'.$row2->ACCP_ID; ?>"><?php echo $row2->Account_name."-".$row2->Website_name." / ".$row2->Proj_name; ?></option>
													<?php 
													}
												?>
											</select>
											<span id="divtxtprojectid" style="color:red"></span>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-4 control-label">Participants Name</label>
										<div class="col-md-8">
											<select class="form-control select2 input-sm opt" name="txtparticipant" id="txtparticipant">
                                            	<option value="-1">Select</option>
												<?php
													foreach($emp->result() as $row3)
													{
													?>
													<option value="<?php echo $row3->or_m_reg_id; ?>"><?php echo $row3->or_m_name; ?></option>
													<?php
													}
												?>
											</select>
											<span id="divtxtparticipant" style="color:red"></span>
										</div>
									</div>

                                    <div class="form-group">
										<label class="col-md-4 control-label">Designation</label>
										<div class="col-md-8">
											<select class="form-control select2 input-sm" name="txtdesignation" id="txtdesignation">
												<option value="-1">Select</option>
												<option value="1">Project Manager</option>
												<option value="2">Team Leader</option>
												<option value="3">Assistant</option>
												<option value="4">Executive</option>
											</select>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-4 control-label">Admin Rights</label>
										<div class="col-md-8">
											<input type="checkbox" class="make-switch" name="chadminright" id="chadminright" value="1" data-size="small" data-on="success" data-off="danger">
										</div>
									</div>
									
                                    <div class="form-group">
										<label class="col-md-4 control-label">Add Project Participant</label>
										<div class="col-md-8">
											<input type="checkbox" class="make-switch" name="chaddparticipant" id="chaddparticipant" value="1" data-size="small" data-on="success" data-off="danger">
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-4 control-label">Viewing Rights</label>
										<div class="col-md-8">
                                            <input type="checkbox" class="make-switch" name="chviewright" id="chviewright" value="1" data-size="small" data-on="success" data-off="danger">
										</div>
									</div>

                                    <div class="form-group">
										<label class="col-md-4 control-label">Update Rights</label>
										<div class="col-md-8">
                                            <input type="checkbox" class="make-switch" name="chupdateright" id="chupdateright" value="1" data-size="small" data-on="success" data-off="danger">
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-md-4 control-label">Project Notifications</label>
										<div class="col-md-8">
                                            <input type="checkbox" class="make-switch" name="chproject_noti" id="chproject_noti" value="1" data-size="small" data-on="success" data-off="danger">
										</div>
									</div>
									
                                    <div class="form-group">
										<label class="col-md-4 control-label">Task Notifications</label>
										<div class="col-md-8">
                                            <input type="checkbox" class="make-switch" id="chtask_noti" name="chtask_noti" value="1" data-size="small" data-on="success" data-off="danger">
										</div>
									</div>
								</div>
								
								<div class="form-actions fluid">
									<div class="col-md-offset-4 col-md-8">
										<button type="button" onclick="inserts('insert_data')" class="btn green">Submit</button>
										<button type="button" onclick="goto('<?php echo base_url();?>index.php/project/after_view_project_participants')" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->

<script>
	var switch_url="<?php echo base_url();?>index.php/project/add_participants",
	load_url="<?php echo base_url(); ?>index.php/project/after_view_project_participants";
</script>  