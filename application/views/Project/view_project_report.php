<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-archive"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_project">
							Project
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="icon-book-open"></i>
						<span>
							Client Project Report
						</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View Project Report </h3>
			<!-- END PAGE TITLE-->
			
			
			<div class="row">
				<div class="col-md-12">
					
					<div class="table-responsive" id="data1">
						<form method="post" action="">
							<table class="table table-striped table-bordered table-hover table-full-width" id="datatable_products">
								<thead>
									<tr role="row" class="heading">
										<th width="15%">
											Project Name
										</th>
										<th width="15%">
											Project Leader
										</th>
										<th width="15%">
											Project Participants
										</th>
										<th width="15%">
											Project Start Date
										</th>
										<th width="15%">
											Project End Date
										</th>
										<th width="10%">
											Actions
										</th>
									</tr>
									
									<tr role="row" class="filter">
										<td>
											<select name="ddproject" class="form-control select2 input-sm">
												<option value="">Select...</option>
												<?php 
													foreach($proj->result() as $row1)
													{
													?>
													<option value="<?php echo $row1->m_project_id; ?>"><?php echo $row1->m_project_name; ?></option>
													<?php
													}
												?>
											</select>
										</td>
										
										<td>
											<select name="ddleader" class="form-control select2 input-sm">
												<option value="">Select...</option>
												<?php 
													foreach($user->result() as $row2)
													{
													?>
													<option value="<?php echo $row2->or_m_reg_id; ?>"><?php echo $row2->or_m_name; ?></option>
													<?php
													}
												?>
											</select>
										</td>
										
										<td>
											<select name="ddparticipants" class="form-control select2 input-sm">
												<option value="">Select...</option>
												<?php 
													foreach($user->result() as $row3)
													{
													?>
													<option value="<?php echo $row3->or_m_reg_id; ?>"><?php echo $row3->or_m_name; ?></option>
													<?php
													}
												?>
											</select>
										</td>
										
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control input-sm" readonly name="txtfromdate" id="txtfromdate" placeholder="From">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										
										<td>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control input-sm" readonly name="txttodate" id="txttodate" placeholder="To">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										
										<td>
											<button class="btn btn-sm green filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
											<button type="reset" class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
										</td>
									</tr>
								</thead>
							</table>
						</form>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-ticket font-dark" ></i>
								<span class="caption-subject bold uppercase"> View Project Report</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								
								<thead>
									<tr>
									    <th>S.No</th>
									    <th>Action</th>
										<th>Project Name</th>
										<th>Project Manager</th>
										<th>Team Leader</th>
										<th>Assistant</th>
										<th>Executive</th>
										
										<th>Project Start Date</th>
										<th>Project End Date</th>
									</tr>
								</thead>
								<tbody>
									
									<?php
									    $sn=1;
										foreach($rec->result() as $row4)
										{
										?>
										<tr>
										    <td><?php echo $sn; ?></td>
										    <td>
                                            <a href="<?php echo base_url();?>project/view_project_flow_report/<?php echo $row4->Account_id.'/'.$row4->PROJECT_ID.'/'.$row4->PRO_MANAGE_ID.'/'; ?>"><span class="btn blue btn-xs"><i class="fa fa-hand-o-right"></i> Project Flow</span></a>
                                            <?php if($row4->AMC_STATUS==1 && $row4->pr_amc_status==0) {?>
                                            <p style="margin:0">&nbsp;&nbsp;</p>
                                            <a href="<?php echo base_url();?>account/add_to_amc/<?php echo $row4->PROJECT_ID; ?>"><span class="btn green btn-xs"><i class="fa fa-hand-o-right"></i> Add To AMC</span></a>
                                             <?php } 
                                              if($row4->project_status==1)
                                              {
                                             ?>   
                                              <p style="margin:0">&nbsp;&nbsp;</p>
                                             <a href="<?php echo base_url();?>project/view_followup/<?php echo $row4->PROJECT_ID; ?>"><span class="btn purple btn-xs"><i class="fa fa-hand-o-right"></i> Project Chat</span></a>
                                             <?php } else { ?>
                                              <p style="margin:0">&nbsp;&nbsp;</p>
                                               <a href="<?php echo base_url();?>project/view_followup/<?php echo $row4->PROJECT_ID; ?>"><span class="btn purple btn-xs"><i class="fa fa-hand-o-right"></i>View Project Chat</span></a>
                                             <?php } ?>
                                            </td>
											<td><?php echo $row4->Account_name.'/'.$row4->Website_name.'/'.$row4->PROJECT_NAME; ?></td>
											<td align="center"><?php if($row4->USER_NAME==$row4->PRO_MANAGER) { echo "<button class='btn blue btn-sm'>".strtoupper($row4->PRO_MANAGER)."</button>"; } else { echo strtoupper($row4->PRO_MANAGER); }  ?></td>
                                            <td align="center"><?php if($row4->USER_NAME==$row4->TEAM_LEADER) { echo "<button class='btn blue btn-sm'>".strtoupper($row4->TEAM_LEADER)."</button>"; } else { echo strtoupper($row4->TEAM_LEADER); }  ?></td>
										    <td align="center"><?php if($row4->USER_NAME==$row4->PRO_ASSISTENT) { echo "<button class='btn blue btn-sm'>".strtoupper($row4->PRO_ASSISTENT)."</button>"; } else { echo strtoupper($row4->PRO_ASSISTENT); } ?></td>
											<td align="center"><?php if($row4->USER_NAME==$row4->PRO_EXECUTIVE) { echo "<button class='btn blue btn-sm'>".strtoupper($row4->PRO_EXECUTIVE)."</button>"; } else { echo strtoupper($row4->PRO_EXECUTIVE); } ?></td>
											<td align="center"><?php echo $row4->PROJ_STARTDATE; ?></td>
											<td align="center"><?php echo $row4->PROJ_ENDDATE; ?></td>
											<?php 
												$sn++;
											}
										?>    
										
									</tbody>
								</table>
							</div>
						</div>
						
					</div>
					<!-- END PAGE CONTENT-->
				</div>
				
			</div>
			<!-- END CONTENT BODY -->
		</div>
	</div>
<!-- END CONTENT -->