<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-archive"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_project">
							Project
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					   <i class="fa fa-plus-square"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_project">
							Add Project
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					   <i class="fa fa-edit"></i>
						<span>
							Edit Modules
						</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Edit Modules</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs font-black"></i>
								<span class="caption-subject font-black bold uppercase">View Edit Modules</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div id="tree_2"></div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder font-black"></i>
								<span class="caption-subject font-black bold uppercase">Edit Modules</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div id="div_load">
							<div class="portlet-body form">
								<form action="#" class="form-horizontal" method="post" id="insert_data">
									<div class="form-body">
										
										<div class="form-group">
											<label class="col-md-3 control-label">Module Name</label>
											<div class="col-md-6">
												<input type="text" class="form-control input-sm empty" id="txtname" name="txtname" placeholder="Enter Module Name" />
												<input type="hidden" id="txtparentid" name="txtparentid" value="0" />
												<span id="divtxtname" style="color:red"></span>
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-md-3 control-label">Attactment</label>
											<div class="col-md-6">
												<div class="fileinput fileinput-new input-xs" data-provides="fileinput">
													<div class="input-group">
														<div class="form-control uneditable-input input-xs " data-trigger="fileinput">
															<i class="fa fa-file fileinput-exists"></i>&nbsp;
															<span class="fileinput-filename">
															</span>
														</div>
														<span class="input-group-addon default btn-file btn-xs ">
															<span class="fileinput-new input-xs"> Select file </span>
															<span class="fileinput-exists input-xs"> Change </span>
															<input type="file" class="input-xs" name="userfile" id="userfile" >
														</span>
														<a href="javascript:;" class="input-group-addon btn btn-xs red fileinput-exists" data-dismiss="fileinput"> Remove </a>
													</div>
												</div>
												<input type="hidden" name="txtfileatt" id="txtfileatt" >
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-md-3 control-label">Module Description</label>
											<div class="col-md-6">
											<textarea class="form-control input-sm empty" id="txtdescription" rows="4" name="txtdescription" placeholder="Enter Module Description." /></textarea>
											<span id="divtxtdescription" style="color:red"></span>
										</div>
									</div>
									
								</div>
								
								<div class="form-actions">
									<div class="col-md-9 col-md-offset-3">
										<input type="button" onclick="submitFile()" class="btn green" value="Submit">
										<button type="reset" onclick="goto('<?php echo base_url(); ?>project/after_view_project')" class="btn">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT BODY -->
</div>
</div>
<!-- END CONTENT -->
<script>
	var UITree = function () {
		var handleSample2 = function () {
			$('#tree_2').jstree({
				'plugins': ["wholerow", "types"],
				'core': {
					"themes" : {
						"responsive": false
					},    
					'data': [
					<?php 
						$menu_name="";
						foreach($rec->result() as $row)
						{
							if($row->m_module_parent_id==0)
							{
								$menu_name=$row->m_module_name;
								$description=$row->m_module_description;
								$parent=$row->m_module_id;
								
							?>
							
							{
								"text": "<?php echo "<a href='#' onclick='go1($parent)' title='$description'>$menu_name</a>" ?>",
								"children":
								[
								<?php 
									foreach($rec->result() as $row1)
									{
										if($row1->m_module_parent_id==$parent)
										{
											$menu_name1=$row1->m_module_name;
											$description1=$row1->m_module_description;
											$parent1=$row1->m_module_id;
										?>
										{
											"text": "<?php echo "<a href='#' onclick='go1($parent1)' title='$description1'>$menu_name1</a>" ?>",
											"children": 
											[
											<?php 
												foreach($rec->result() as $row2)
												{
													if($row2->m_module_parent_id==$parent1)
													{
														$menu_name2=$row2->m_module_name;
														$description2=$row2->m_module_description;
														$parent2=$row2->m_module_id;
													?>
													{
														"text": "<?php echo "<a href='#' onclick='go1($parent2)' title='$description2'>$menu_name2</a>" ?>",
														"children": 
														[
														<?php 
															foreach($rec->result() as $row3)
															{
																if($row3->m_module_parent_id==$parent2)
																{
																	$menu_name3=$row3->m_module_name;
																	$description3=$row3->m_module_description;
																	$parent3=$row3->m_module_id;
																?>
																{
																	"text": "<?php echo "<a href='#' onclick='go1($parent3)' title='$description3'>$menu_name3</a>" ?>",
																	"children": 
																	[
																	<?php 
																		foreach($rec->result() as $row4)
																		{
																			if($row4->m_module_parent_id==$parent3)
																			{
																				$menu_name4=$row4->m_module_name;
																				$description4=$row4->m_module_description;
																				$parent4=$row4->m_module_id;
																			?>
																			{
																				"text": "<?php echo "<a href='#' onclick='go1($parent4)' title='$description4'>$menu_name4</a>" ?>",
																				"children": 
																				[
																				<?php 
																					foreach($rec->result() as $row5)
																					{
																						if($row5->m_module_parent_id==$parent4)
																						{
																							$menu_name5=$row5->m_module_name;
																							$description5=$row5->m_module_description;
																							$parent5=$row5->m_module_id;
																						?>
																						{
																							"text": "<?php echo "<a href='#' onclick='go1($parent5)' title='$description5'>$menu_name5</a>" ?>",
																							"children": 
																							[
																							<?php 
																								foreach($rec->result() as $row6)
																								{
																									if($row6->m_menu_id==$parent5)
																									{
																										$menu_name6=$row6->m_module_name;
																										$description6=$row6->m_module_description;
																										$parent6=$row6->m_module_id;
																									?>
																									{
																										"text": "<?php echo "<a href='#' onclick='go1($parent6)' title='$description6'>$menu_name6</a>" ?>",
																									},
																									<?php
																									}
																								}
																							?>
																							]
																						},
																						<?php
																						}
																					}
																				?>
																				]
																			},
																			<?php
																			}
																		}
																	?>
																	]
																},
																<?php
																}
															}
														?>
														]
													},
													<?php
													}
												}
											?>
											]
										},
										<?php
										}
									}
								?>
								]
								
							},
							<?php
							}
						}
					?>
					
					]
					
				},
				
			});
		}
		return {
			//main function to initiate the module
			init: function () {
				handleSample2();
			}
		};
	}();
</script>

<script>
	function go1(get_id)
	{
		$("#div_load").html("<div><center><img src ='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' alt='Loading.....' title='Loading....'></center></div>");
		$("#div_load").load("<?php echo base_url(); ?>project/module_edit_details/"+get_id+"/<?php echo $projid;?>");
	}
	</script>
<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
				