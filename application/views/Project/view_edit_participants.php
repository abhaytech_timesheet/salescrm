<?php
	foreach($rec->result() as $row)
	{
		break;
	}
?>
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-archive"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_project">
							Project
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					   <i class="fa fa-user-plus"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_paricipants">
							Project Participants
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					   <i class="fa fa-edit"></i>
						<span>
							Edit Participants
						</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">Edit Participants </h3>
			<!-- END PAGE TITLE-->

			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Edit Participants </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal" method="post" id="myform">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-4 control-label">Project Name</label>
										<div class="col-md-8">
											<select class="form-control input-sm opt" name="txtprojectid" id="txtprojectid">
                                            	<option value="-1">Select</option>
                                                <?php
													foreach($project->result() as $row1)
													{
														if($row->parti_project_id==$row1->ACCP_ID)
														{
														?> 
														<option value="<?php echo $row1->ACCP_ID; ?>" selected="selected"><?php echo $row1->Account_name."-".$row1->Website_name." / ".$row1->Proj_name ?></option>
														<?php
														}
														else
														{
														?>
														<option value="<?php echo $row1->ACCP_ID; ?>" ><?php echo $row1->Account_name."-".$row1->Website_name." / ".$row1->Proj_name ?></option>
														
														<?php
														}
													}
												?>
											</select>
											<span id="divtxtprojectid" style="color:red"></span>
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-md-4 control-label">Participants Name</label>
										<div class="col-md-8">
											<select class="form-control input-sm opt" name="txtparticipant" id="txtparticipant">
                                            	<option value="-1">Select</option>
                                                <?php
													foreach($emp->result() as $row2)
													{
														if($row->parti_participants_id==$row2->or_m_reg_id)
														{
														?> 
														<option value="<?php echo $row2->or_m_reg_id; ?>" selected="selected"><?php echo $row2->or_m_name; ?></option>
														<?php
														}
														else
														{
														?>
														<option value="<?php echo $row2->or_m_reg_id; ?>"><?php echo $row2->or_m_name; ?></option>                
														<?php 
														}
													}
												?>
											</select>
											<span id="divtxtparticipant" style="color:red"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-4 control-label">Designation</label>
										<div class="col-md-8">
											<select class="form-control select2 input-sm" name="txtdesignation" id="txtdesignation">
												<option value="-1">Select</option>
												<option value="1">Project Manager</option>
												<option value="2">Team Leader</option>
												<option value="3">Assistant</option>
												<option value="4">Executive</option>
											</select>
											<script>
												$("#txtdesignation").val(<?php echo $row->parti_designation; ?>);
											</script>
										</div>
									</div>
                                    
									<div class="form-group">
										<label class="col-md-4 control-label">Admin Rights</label>
										<div class="col-md-8">
											<?php 
												if($row->parti_admin_right==1)
												{ ?>
												<input type="checkbox" class="make-switch" data-size="small" name="chadminright" id="chadminright" value="1" checked="checked" data-on="success" data-off="danger">
												<?php
												}
												else
												{
												?>
												<input type="checkbox" class="make-switch" data-size="small" name="chadminright" id="chadminright" data-on="success" data-off="danger">
												<?php
												}
											?>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-4 control-label">Add Project Participant</label>
										<div class="col-md-8">
											<?php 
												if($row->parti_add_participant==1)
												{ ?>
												<input type="checkbox" class="make-switch" data-size="small" name="chaddparticipant" id="chaddparticipant" value="1" checked="checked" data-on="success" data-off="danger">
												<?php
												}
												else
												{
												?>
												<input type="checkbox" class="make-switch" data-size="small" name="chaddparticipant" id="chaddparticipant" data-on="success" data-off="danger">
                                                <?php
												}
											?>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">Viewing Rights</label>
										<div class="col-md-8">
											<?php 
												if($row->parti_view_right==1)
												{ ?>
												<input type="checkbox" class="make-switch" data-size="small" name="chviewright" id="chviewright" value="1" checked="checked" data-on="success" data-off="danger">
												<?php
												}
												else
												{
												?>
												<input type="checkbox" class="make-switch" data-size="small" name="chviewright" id="chviewright" data-on="success" data-off="danger">
												<?php
												}
											?>
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-md-4 control-label">Update Rights</label>
										<div class="col-md-8">
											<?php 
												if($row->parti_update_right==1)
												{ ?>
												<input type="checkbox" class="make-switch" data-size="small" name="chupdateright" id="chupdateright" value="1" checked="checked" data-on="success" data-off="danger">
												<?php
												}
												else
												{
												?>
												<input type="checkbox" class="make-switch" data-size="small" name="chupdateright" id="chupdateright" data-on="success" data-off="danger">
                                                <?php
												}
											?>
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-md-4 control-label">Project Notifications</label>
										<div class="col-md-8">
											<?php 
												if($row->parti_project_noti==1)
												{ ?>
												<input type="checkbox" class="make-switch" data-size="small" name="chproject_noti" id="chproject_noti" value="1" checked="checked" data-on="success" data-off="danger">
												<?php
												}
												else
												{
												?>
												<input type="checkbox" class="make-switch" data-size="small" name="chproject_noti" id="chproject_noti" data-on="success" data-off="danger">
												<?php
												}
											?>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-4 control-label">Task Notifications</label>
										<div class="col-md-8">
											<?php 
												if($row->parti_project_noti==1)
												{ ?>
												<input type="checkbox" class="make-switch" id="chtask_noti" data-size="small" name="chtask_noti" value="1" checked="checked" data-on="success" data-off="danger">
												<?php
												}
												else
												{
												?>
												<input type="checkbox" class="make-switch" id="chtask_noti" data-size="small" name="chtask_noti"  data-on="success" data-off="danger">
												<?php
												}
											?>
										</div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-4 col-md-8">
										<button type="button" onclick="update()" class="btn green">Update</button>
										<button type="button" onclick="goto('<?php echo base_url();?>index.php/project/after_view_project_participants')" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							<!-- END FORM--> 
							
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->

<script>
	function update()
	{
		var project_id = $('#txtprojectid').val();
		var participants = $('#txtparticipant').val();
        var designation = $('#txtdesignation').val();
		if($('#chadminright').prop('checked') === true)
		{
			var admin_right = '1';
		}
		else
		{
			var admin_right = '0';
		}
		
		if($('#chaddparticipant').prop('checked') === true)
		{
			var add_participant = '1';
		}
		else
		{
			var add_participant = '0';
		}
		
		if($('#chviewright').prop('checked') === true)
		{
			var view_right = '1';
		}
		else
		{
			var view_right = '0';
		}
		
		if($('#chupdateright').prop('checked') === true)
		{
			var update_right = '1';
		}
		else
		{
			var update_right = '0';
		}
		
		if($('#chproject_noti').prop('checked') === true)
		{
			var project_noti = '1';
		}
		else
		{
			var project_noti = '0';
		}
		
		if($('#chtask_noti').prop('checked') === true)
		{
			var task_noti = '1';
		}
		else
		{
			var task_noti = '0';
		}
		//alert(admin_right);
		//alert(<?php echo $this->uri->segment(3); ?>);
		$.ajax(
		{
			type: "POST",
			url:"<?php echo base_url();?>index.php/project/update_add_participants/<?php echo $this->uri->segment(3); ?>",
			data:"txtprojectid="+project_id+"&txtparticipant="+participants+"&txtdesignation="+designation+"&chadminright="+admin_right+"&chaddparticipant="+add_participant+"&chviewright="+view_right+"&chupdateright="+update_right+"&chproject_noti="+project_noti+"&chtask_noti="+task_noti,
			success: function(msg) {
				if(msg.trim()=="true")
				{
					$(".page-content").html("<center><h2>Details Updated Successfully!</h2></center>")
					.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
					.hide()
					.fadeIn(1000,function()
					{
						$("#stylized").load("<?php echo base_url().'index.php/project/after_view_project_participants'?>");
					}
					);
					
				}	  
			}
		});
	}
</script>