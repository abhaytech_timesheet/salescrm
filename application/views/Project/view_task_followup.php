<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-archive"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_project">
							Project
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="icon-list"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_all_task">
							Project Task
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-road"></i>
						<span>
							Task Followup
						</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">Task Followup</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs font-black"></i>
								<span class="caption-subject font-black bold uppercase">Insert Task Followup</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body" id="chats">
							<div style="height: 435px; overflow-y: scroll;" data-always-visible="1" data-rail-visible1="0">
								<ul class="chats">
									<?php
										foreach($rec->result() as $row)
										{
											if($row->or_m_userimage!="")
											{
												$img=base_url()."application/emp_pics/".$row->or_m_userimage;
											}
											else
											{
												$img=base_url()."application/libraries/noimage.png";
											}
										?>
										<li class="<?php echo $row->msg; ?>">
											<img class="avatar img-responsive" alt="" src="<?php echo $img; ?>" width="45px" height="45px"/>
											<div class="message">
												<span class="arrow">
												</span>
												<a href="javascript:void(0);" class="name">
													<?php echo $row->or_m_name; ?>
												</a>
												<span class="datetime">
													at <?php echo $row->task_response_date1; ?>
												</span>
												<span class="body">
													<?php echo $row->task_description; ?>
												</span>
											</div>
										</li>
										<?php
										}
									?>
								</ul>
							</div>
							<?php
								if($taskstatus==1)
								{
								?>
								<div class="chat-form">
									<form id="insert_data" name="insert_data" method="post">
										<div class="input-cont">
											<input class="form-control empty" type="text" name="txtdescription" id="txtdescription" placeholder="Type a message here..."/>
											<input type="hidden" name="txttaskid" id="txttaskid" value="<?php echo $id; ?>" />
										</div>
										
										<div class="btn-cont">
											<span class="arrow"></span>
											<a href="javascript:void(0)" onclick="insert()" class="btn green icn-only">
												<i class="fa fa-check icon-white"></i>
											</a>
										</div>
										
									</form>
									
								</div>
								<?php
								}
							?>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->
<script>
	$("#txtdescription").keypress(TriggerSearch);
	
	function search_product() {
		insert();
	}
	
	function TriggerSearch(e) {
		var search_string = $("#txtdescription").val();
		e = e || window.event;
		var keycode;
		if (window.event) {
			keycode = e.which ? window.event.which : window.event.keyCode;
		}
		var key = e.which;
		switch (key) {
			case 38:
            break;
			case 40:
            break;
			case 13:
            e.preventDefault();
            search_product();
            break;
			default:
            break;
		}
	}
</script>

<script>
	function insert()
	{
		if(check('insert_data'))
		{
			formData=$("#insert_data").serialize();
			$.ajax(
			{
				type: "POST",
				url:"<?php echo base_url();?>project/insert_follow_up/",
				data:formData,
				success: function(msg){
					$(".page-content").html("<center><h2>Add Task Followup Successfully!</h2></center>")
					.append("<center><img src ='<?php echo base_url(); ?>application/libraries/assets/global/img/loading-spinner-blue.gif' alt='Loading.....' title='Loading....'></center>")
					.hide()
					.fadeIn(1000,function()
					{
						$("#stylized").load("<?php echo base_url(); ?>project/view_task_follow_up/"+msg);
					});
				}	
			});
		}
	}
</script>