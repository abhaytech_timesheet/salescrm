<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-archive"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_project">
							Project
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="icon-book-open"></i>
						<span>
							Client Project Report
						</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View Project Report </h3>
			<!-- END PAGE TITLE-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light portlet-fit bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-layers font-green"></i>
								<span class="caption-subject font-green bold uppercase">Project Flow Report <?php foreach($users->result() as $rowpro){ } echo 'Of '.$rowpro->Account_name.' / '.$rowpro->Website_name.' / '.$rowpro->PROJECT_NAME; ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="mt-element-step">
								<div class="row step-line">
									<?php 
										$step = '';
										$sn=0;
										foreach($rec->result() as $row)
										{
											if($row->tr_flow_process_id == 1)
											{
												$step = 'first';
											}
											elseif($row->tr_flow_process_id == 12)
											{
												$step = 'last';
											}
											else
											{
												$step = '';
											}	
											
											if($row->tr_flow_status == 1)
											{
											?>
											<div class="col-md-1 mt-step-col <?php echo $step; ?> done" data-toggle="modal" data-target="#myModal" onclick="show_status('<?php echo $row->tr_flow_id; ?>','<?php echo $row->Assign_name; ?>','<?php echo $row->tr_next_assigndate; ?>','<?php echo $row->tr_flow_description; ?>')">
												<div class="mt-step-number bg-white" style="padding:12px 14px; margin-top: -8px;">
													<?php echo $row->tr_flow_process_id; ?>
												</div>
												<div class="mt-step-title"></div>
												<div class="mt-step-content uppercase font-grey-cascade"><?php echo $row->m_pro_flow_name; ?></div>
                                                <div style="color:red"><?php echo $row->Assign_name; ?></div>
											</div>
											<?php
											}
											if($row->tr_flow_status == 0)
											{
												$sn++;
												if($sn == 1)
												{
												?>
												<div class="col-md-1 mt-step-col active <?php echo $step; ?>" data-toggle="modal" data-target="#myModal" onclick="update_status('<?php echo $row->tr_flow_id; ?>','<?php echo $row->Assign_name; ?>','<?php echo $row->tr_next_assigndate; ?>')">
													<div class="mt-step-number bg-white" style="padding:12px 14px; margin-top: -8px;">
														<?php echo $row->tr_flow_process_id; ?>
													</div>
													<div class="mt-step-title"></div>
													<div class="mt-step-content uppercase font-grey-cascade"><?php echo $row->m_pro_flow_name; ?></div>
                                                    
                                                    <div style="color:red"><?php echo $row->Assign_name; ?></div>
												</div>
												<?php
												}
												else
												{
												?>
												<div class="col-md-1 mt-step-col <?php echo $step; ?>">
													<div class="mt-step-number bg-white" style="padding:12px 18px; margin-top: -8px;">
														<?php echo $row->tr_flow_process_id; ?>
													</div>
													<div class="mt-step-title"></div>
													<div class="mt-step-content uppercase font-grey-cascade"><?php echo $row->m_pro_flow_name; ?></div>
                                                    
                                                    <div style="color:red"><?php echo $row->Assign_name; ?></div>
												</div>
												<?php
												}
											}
										}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END : STEPS -->
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Process Flow Update</h4>
			</div>
			<div class="modal-body">
				
				<div class="tabbable-line">
					<ul class="nav nav-tabs ">
						<li class="active">
							<a href="#tab_15_1" data-toggle="tab"> Description </a>
						</li>
						<li>
							<a href="#tab_15_2" data-toggle="tab"> Reassign </a>
						</li>
						
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_15_1">
							<form action="<?php echo base_url(); ?>project/update_project_flow" method="post" class="horizontal-form"  id="form_sample_1">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label">Description<span class="required"> * </span></label>
										<textarea class="form-control empty" name="txtdescription" id="txtdescription" rows="3" style="resize:none;"></textarea>
										<input type="hidden" name="process_id" id="process_id">
										<input type="hidden" name="txt3" id="txt3" value="<?php echo $this->uri->segment(3); ?>">
										<input type="hidden" name="txt4" id="txt4" value="<?php echo $this->uri->segment(4); ?>">
                                        <input type="hidden" name="txt5" id="txt5" value="<?php echo $this->uri->segment(5); ?>">
										<span id="divtxtdescription" style="color:red"></span>
									</div>
									<div class="form-action" id="btn">
										<button type="button" onclick="conwv('form_sample_1')" class="btn btn-info">Submit</button>
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</form>
							
						</div>
						<div class="tab-pane" id="tab_15_2">
                            <div id="assignname">
							<form action="<?php echo base_url(); ?>project/update_project_assign" method="post" class="horizontal-form"  id="form_samp">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label">Reassign Name<span class="required"> * </span></label>
										<select id="ddreassign" name="ddreassign" class="form-control input-sm opt">
											<option>--- Select ---</option>
											<?php foreach($users->result() AS $rows) { ?>
												<option value="<?php echo $rows->PARICIPANT_USER_ID; ?>"> <?php echo $rows->USER_NAME; ?> </option>
											<?php } ?>
										</select>
										<input type="hidden" name="process_id1" id="process_id1">
										<input type="hidden" name="txtpro" id="txtpro" value="<?php echo $this->uri->segment(4); ?>">
										<input type="hidden" name="txtacc" id="txtacc" value="<?php echo $this->uri->segment(3); ?>">
										<input type="hidden" name="txtmanage" id="txtmanage" value="<?php echo $this->uri->segment(5); ?>">
										<span id="divddreassign" style="color:red"></span>
									</div>
									<div class="form-action">
										<button type="button" onclick="conwv('form_samp')" class="btn btn-info">Assign</button>
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</form>
                            </div>
                            <div id="assignname1" style="display:none">
                            </div>
						</div>
						
					</div>
				</div>
			</div>
			<div class="modal-footer">
				
			</div>
		</div>
		
	</div>
</div>

<script>
	function update_status(id,assign_name,assign_date)
	{
        $("#btn").css('display','block');
        $("#txtdescription").val('');
		$("#process_id").val(id);
		$("#process_id1").val(id);
        if(assign_date!='')
        {
         $("#assignname").css('display','none');
         $("#assignname1").css('display','block');
         $("#assignname1").html("<center style='font-size:24px; color:red'>"+assign_name+"</ceneter>");
        }
        else
        {
         $("#assignname").css('display','block');
         $("#assignname1").css('display','none');
        }
	}

    
    function show_status(id,assign_name,assign_date,discription)
    { 
        $("#btn").css('display','none');
        $("#txtdescription").val(discription);
        if(assign_date!='')
        {
         $("#assignname").css('display','none');
         $("#assignname1").css('display','block');
         $("#assignname1").html("<center style='font-size:24px; color:red'>"+assign_name+"</ceneter>");
        }
        else
        {
         $("#assignname").css('display','block');
         $("#assignname1").css('display','none');
        }

    }
</script>