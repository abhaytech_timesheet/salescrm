<?php 
	foreach($rec->result() as $row)
	{
		break;
	}
?>
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-archive"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_project">
							Project
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					   <i class="fa fa-edit"></i>
						<span>
							Edit Project
						</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">Edit Project</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-plus-square font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Edit Project</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/upload/anouncement" method="post" id="insert_data">
								<div class="form-body">
									
									<div class="form-group">
										<label class="col-md-3 control-label">Project Name</label>
										<div class="col-md-6">
											<input class="form-control input-sm empty" placeholder="Enter Project Name" name="txtpname" id="txtpname" type="text" value="<?php echo $row->m_project_name; ?>"  />
											<span id="divtxtpname1" style="color:red;"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Project Type</label>
										<div class="col-md-6">
											<select class="form-control input-sm empty" name="txttype" id="txttype">
												<option value="-1">Select</option>
												<option value="1">Website</option>
												<option value="2">Software</option>
											</select>
											<span id="divtxttype" style="color:red;"></span>
											<script>
												$("#txttype").val(<?php echo $row->m_project_type; ?>);
											</script>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Date</label>
										<div class="col-md-6">
											<div class="input-group input-large date-picker input-daterange" data-date="2012/11/10/" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control input-sm empty" name="txtfrom" id="txtfrom" value="<?php echo $row->m_project_startdate; ?>">
												<span class="input-group-addon">
													to
												</span>
												<input type="text" class="form-control input-sm empty" name="txtto" id="txtto" value="<?php echo $row->m_project_enddate; ?>">
											</div>
											<span id="divfrom" style="color:red;"></span>
											<span id="divto" style="color:red;"></span>
										</div>
									</div>
									
                                    <div class="form-group">
										<label class="col-md-3 control-label">Attactment</label>
										<div class="col-md-6">
											<div class="fileinput fileinput-new input-sm" data-provides="fileinput">
												<div class="input-group">
													<div class="form-control uneditable-input input-sm " data-trigger="fileinput">
														<i class="fa fa-file fileinput-exists"></i>&nbsp;
														<span class="fileinput-filename"><?php echo $row->m_project_file; ?>
														</span>
													</div>
													<span class="input-group-addon btn-sm default btn-file">
														<span class="fileinput-new"> Select file </span>
														<span class="fileinput-exists"> Change </span>
														<input type="file" class="input-xs" name="userfile" id="userfile" value="<?php echo $row->m_project_file; ?>">
													</span>
													<a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
												</div>
											</div>
											<input type="hidden" name="txtfileatt" id="txtfileatt" value="<?php echo $row->m_project_file; ?>">
											<span id="divuserfile" style="color:red;"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Project Description</label>
										<div class="col-md-6">
											<textarea class="form-control input-sm empty" rows="4" PLACEHOLDER="Enter Project Description Information" name="txtProject_des" id="txtProject_des"><?php echo $row->m_project_description; ?></textarea>
											<span id="divtxtProject_des" style="color:red;"></span>
										</div>
									</div>
									
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="button" onclick="submitFile()" class="btn blue">Update</button>
										<button type="button" onclick="goto('<?php echo base_url();?>index.php/project/after_view_project')" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->
<script>
	var switch_url="<?php echo base_url();?>index.php/project/update_project/<?php echo $this->uri->segment(3);?>",
	load_url="<?php echo base_url()?>index.php/project/after_view_project";
</script>

<script>
	function submitFile()
	{
		var txtfileatt=$("#txtfileatt").val();
		if(check("insert_data") && txtfileatt!='')
		{
			if()
			bootbox.confirm('Are you sure to Submit from?', function(result){
				if(result==true)
				{
					var formUrl = "<?php echo base_url(); ?>index.php/project/uploadfile";
					var formData = new FormData($('.form-horizontal')[0]);
					$.ajax({
						url: formUrl,
						type: 'POST',
						data: formData,
						mimeType: "multipart/form-data",
						contentType: false,
						cache: false,
						processData: false,
						success: function(data){
							//now get here response returned by PHP in JSON fomat you can parse it using JSON.parse(data)
							$("#txtfileatt").val(data);
							inserts('insert_data');
						},
						error: function(jqXHR, textStatus, errorThrown){
							//handle here error returned
						}
					});
				}
			});
		}
	}
</script>