<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="#.">PROJECT MODULE</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Project</span>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">Add New Project</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-plus-square font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Add New Project</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default " href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default remove" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal" id="insert_data" enctype="multipart/form-data" action="<?php echo base_url();?>upload/anouncement" method="post" onsubmit="return conwv('stylized')">
								<div class="form-body">

									<div class="form-group">
										<label class="col-md-3 control-label">Project Name</label>
										<div class="col-md-6">
											<input class="form-control input-sm empty" placeholder="Enter Project Name" name="txtpname" id="txtpname" type="text" />
											<span id="divtxtpname1" style="color:red;"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Project Type</label>
										<div class="col-md-6">
											<select class="form-control input-sm empty" name="txttype" id="txttype">
												<option value="-1">Select</option>
												<option value="1">Website</option>
												<option value="2">Software</option>
											</select>
											<span id="divtxttype" style="color:red;"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Date</label>
										<div class="col-md-6">
											<div class="input-group input-large date-picker input-daterange" data-date="2012/11/10/" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control input-sm empty" name="txtfrom" id="txtfrom">
												<span class="input-group-addon">
													to
												</span>
												<input type="text" class="form-control input-sm empty" name="txtto" id="txtto">
											</div>
											<span id="divfrom" style="color:red;"></span>
											<span id="divto" style="color:red;"></span>
										</div>
									</div>
									
                                    <div class="form-group">
										<label class="col-md-3 control-label">Attactment</label>
										<div class="col-md-6">
											<div class="fileinput fileinput-new input-xs" data-provides="fileinput">
												<div class="input-group">
													<div class="form-control uneditable-input input-xs " data-trigger="fileinput">
														<i class="fa fa-file fileinput-exists"></i>&nbsp;
														<span class="fileinput-filename">
														</span>
													</div>
													<span class="input-group-addon default btn-file btn-xs ">
														<span class="fileinput-new input-xs"> Select file </span>
														<span class="fileinput-exists input-xs"> Change </span>
														<input type="file" class="input-xs emptyfile" name="userfile" id="userfile" >
													</span>
													<a href="javascript:;" class="input-group-addon btn btn-xs red fileinput-exists" data-dismiss="fileinput"> Remove </a>
												</div>
											</div>
											<input type="hidden" name="txtfileatt" id="txtfileatt" >
											<span id="divuserfile" style="color:red;"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Project Description</label>
										<div class="col-md-6">
											<textarea class="form-control input-sm empty" rows="4" PLACEHOLDER="Enter Project Description Information" name="txtProject_des" id="txtProject_des"></textarea>
											<span id="divtxtProject_des" style="color:red;"></span>
										</div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="button" onclick="submitFile()" class="btn green"><i class="fa fa-check"></i> Save</button>
										<button type="button" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-reorder font-black"></i>
								<span class="caption-subject bold uppercase"></span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>
											S No.
										</th>
										<th>
											Project Name
										</th>
										<th>
											Start Date
										</th>
										<th>
											End Date
										</th>
										<th>
											Action
										</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$sn=1;
										foreach($rec->result() as $row)
										{
										?>
										<tr class="odd gradeX">
											<td><?php echo $sn; ?></td>
											<td><?php echo $row->m_project_name; ?></td>
											<td><?php echo $row->m_project_startdate; ?></td>
											<td><?php echo $row->m_project_enddate; ?></td>
											<td nowrap>
												<a href="javascript:void(0)"><span onclick="goto('<?php echo base_url() ?>project/view_module/<?php echo $row->m_project_id ?>')" class="btn blue btn-xs"><i class="fa fa-edit"></i> Add Module</span></a>&nbsp;
												<a href="javascript:void(0)"><span onclick="goto('<?php echo base_url() ?>project/edit_module/<?php echo $row->m_project_id ?>')" class="btn yellow btn-xs"><i class="fa fa-edit"></i> Edit Module</span></a>&nbsp;
												<a href="javascript:void(0)"><span onclick="goto('<?php echo base_url() ?>project/edit_project/<?php echo $row->m_project_id ?>')" class="btn red btn-xs"><i class="fa fa-edit"></i> Edit</span></a>&nbsp;
												<?php
												if($row->m_project_stauts=='1')
												{
												?>
													<a href="javascript:void(0)"><span onclick="goto('<?php echo base_url(); ?>project/project_status/<?php echo $row->m_project_id ?>/0')" class="btn red btn-xs"><i class="fa fa-times"></i> Disable</span></a>
												<?php 
												}
												else
												{
												?>
													<a href="javascript:void(0)"><span onclick="goto('<?php echo base_url(); ?>project/project_status/<?php echo $row->m_project_id ?>/1')" class="btn green btn-xs"><i class="fa fa-plus"></i> Enable</span></a>
												<?php
												}
												?>
												
											</td>
										</tr>
										<?php
											$sn++;
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													
<script>
function submitFile()
{
	if(check("insert_data"))
	{
		var formUrl = "<?php echo base_url(); ?>project/uploadfile";
		var formData = new FormData($('.form-horizontal')[0]);
		$.ajax({
				url: formUrl,
				type: 'POST',
				data: formData,
				mimeType: "multipart/form-data",
				contentType: false,
				cache: false,
				processData: false,
				success: function(data){
						//now get here response returned by PHP in JSON fomat you can parse it using JSON.parse(data)
						$("#txtfileatt").val(data);
						inserts('insert_data');
				},
				error: function(jqXHR, textStatus, errorThrown){
						//handle here error returned
				}
		});
	}
}
</script>

<script>
	var switch_url="<?php echo base_url();?>project/insert_project/",
	load_url="<?php echo base_url().'project/after_view_project'?>";
</script>