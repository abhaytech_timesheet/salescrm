<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8"/>
<title>DEMO MASTER</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>application/libraries/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>application/libraries/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-datepicker/css/datepicker.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>

<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/typeahead/typeahead.css">
<link href="<?php echo base_url() ?>application/libraries/assets/css/style-metronic.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>application/libraries/assets/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>application/libraries/assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>application/libraries/assets/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>application/libraries/assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?php echo base_url() ?>application/libraries/assets/css/pages/inbox.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>application/libraries/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-datepicker/css/datepicker.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/select2/select2-metronic.css"/>
<link rel="stylesheet" href="<?php echo base_url() ?>application/libraries/assets/plugins/data-tables/DT_bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
<link href="<?php echo base_url() ?>application/libraries/assets/css/pages/pricing-tables.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>application/libraries/assets/css/pages/profile.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<link rel="icon" type="image/gif" href="<?php echo base_url() ?>application/libraries/mh_ico.jpg">
<link href="<?php echo base_url() ?>application/libraries/assets/css/font.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>


</head>
<!-- END HEAD -->