<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-archive"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_project">
							Project
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="icon-list"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_all_task">
							Project Task
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-plus"></i>
						<span>
							Create Task on Project
						</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Create New Task On Project </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-link font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Create New Task On Project </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form action="javascript:void(0);" class="horizontal-form" enctype="multipart/form-data" method="post" id="insert_data">
								<div class="form-body">
									<h4 class="caption-subject font-blue bold uppercase">Task Information</h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Task Subject</label>
												<input class="form-control input-sm empty"  name="txtsubject" id="txtsubject" type="text"/>
												<span id="divtxtsubject" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Project Name</label>
												<select class="form-control input-sm opt" name="txtproject" id="txtproject" onchange="get_assign()">
													<option value="-1">Select</option>
													<?php
														foreach($proj->result() as $p)
														{
														?>
														<option value="<?php echo $p->ACCP_ID; ?>"><?php echo $p->Account_name."-".$p->Website_name." / ".$p->Proj_name; ?></option>
														<?php
														}
													?>
												</select>
												<span id="divtxtproject" style="color:red"></span>
											</div>
										</div>
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Assigned By</label>
												<input type="text" class="form-control input-sm" name="txtassign"  id="txtassign" value="<?php echo $this->session->userdata('name')?>" disabled>
												<input type="hidden" class="form-control" name="txtassign_id"  id="txtassign_id" value="<?php echo $this->session->userdata('profile_id'); ?>"/>
											</div>
										</div>
										
									</div>
									<!--/row-->
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Priority</label>
												<select class="form-control input-sm opt" name="ddpriority" id="ddpriority">
													<option value="-1">Select</option>
													<option value="1">High</option>
													<option value="2">Medium</option>
													<option value="3">Low</option>
												</select>
												<span id="divddpriority" style="color:red"></span>
											</div>
										</div>
										
										<!--/span-->
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Due Date</label>
												<div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
													<input type="text" class="form-control input-sm empty" name="txtfrom" id="txtfrom">
													<span class="input-group-addon">
														to
													</span>
													<input type="text" class="form-control input-sm empty" name="txtto" id="txtto">
												</div>
												<span id="divtxtto" style="color:red"></span>
												<span id="divtxtfrom" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
									</div>
									
									
									<!--/row-->
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Assign To</label>
												<select class="form-control input-sm opt" name="ddassignto" id="ddassignto" onChange="get_follower()">
													<option value="-1">Select</option>
												</select>
												<span id="divddassignto" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Follower</label>
												<select class="form-control input-sm opt" name="ddfollower" id="ddfollower">
													<?php /*<option value="-1">Select</option>
													<?php
														foreach($follower->result() as $fol)
														{
														?>
														<option value="<?php echo $fol->or_m_reg_id; ?>"><?php echo $fol->or_m_name; ?></option>
														<?php
														}
													*/?>
												</select>
												<span id="divddfollower" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
									</div>
									
									<h4 class="caption-subject font-blue bold uppercase">Reccurence</h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Create Repeat Series of Task</label>
												<input type="checkbox" name="checkrec" value='1' id="checkrec" onclick="$(this).is(':checked') && $('#checked-a').slideDown('slow') || $('#checked-a').slideUp('slow');">                                        
											</div>
										</div>
									</div>
									
									<!----------hide & show reminder------------>		
									
									<div class="dis-sub" style="display:none;" id="checked-a">
										
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Set Schedule</label>
													<select class="form-control input-sm" name="ddschedule" id="ddschedule">
														<option value="0">Never</option>
														<option value="1">Daily</option>
														<option value="2">Weekly</option>
														<option value="3">Monthly</option>
														<option value="4">Yearly</option>
													</select>
												</div>
											</div>
										</div>
									</div>	
									
									<h4 class="caption-subject font-blue bold uppercase">Document Attachment</h4>
									<div class="row">
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Attachment</label><div style="clear:both;"></div>
												
												<div class="fileinput fileinput-new" data-provides="fileinput">
													<div class="input-group input-sm">
														<div class="form-control uneditable-input input-sm input-fixed " data-trigger="fileinput">
															<i class="fa fa-file fileinput-exists"></i>&nbsp;
															<span class="fileinput-filename">
															</span>
														</div>
														<span class="input-group-addon btn btn-sm default btn-file">
															<span class="fileinput-new">
																Select file
															</span>
															<span class="fileinput-exists">
																Change
															</span>
															<input type="file" id="userfile" name="userfile">
														</span>
														<a href="javascript:;" class="input-group-addon btn btn-sm red fileinput-exists" data-dismiss="fileinput"> Remove </a>
													</div>
												</div>
												
												<input type="hidden" name="filename" id="filename" />
												<span class="help-block">
													File Size Must Be Smaller Than 4Mb
												</span>
											</div>
										</div>
									</div>
									
									<!--/span-->
									
									<!--row-->
									<h4 class="caption-subject font-blue bold uppercase">Description Information</h4>
									<div class="row">
										<div class="col-md-8">
											<div class="form-group">
												<label class="control-label">Task Description</label>
												<textarea class="form-control input-sm empty" placeholder="Enter Task Description Information" name="txtcomment" id="txtcomment" style="font-size:20px" col="3"></textarea>
												<span id="divtxtcomment" style="color:red"></span>
											</div>
										</div>
									</div>
									
								</div>
								
								<div class="form-actions left" id="function">
									<button type="button" value="Submit" onclick="submitFile()" class="btn blue"><i class="fa fa-check"></i> Save</button>
									<button type="button" onclick="goto('<?php echo base_url() ?>index.php/project/after_view_all_task')" class="btn default">Cancel</button>
								</div>
								<!-- END FORM-->
							</form>
							<!-- END FORM--> 
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->
<script>
	function get_follower()
	{
		var txtprojectid=$("#txtproject").val();
		var ddassignto=$("#ddassignto").val();
		$.ajax(
		{	
			type:"POST",
			url:"<?php echo base_url();?>index.php/project/get_task_follower",
			dataType: 'json',
			data:{'txtprojectid': txtprojectid,'ddassignto':ddassignto},
			success: function(data){
				$("#ddfollower").empty();
				$("#ddfollower").append("<option value=-1>Select</option>");
				$.each(data,function(i,item)
				{
					$('#ddfollower').append("<option value="+item.USER_ID+">"+item.USER_NAME+"</option>");
				});
			}
		});
	}
</script>
<script>
	function get_assign()
	{
		var txtprojectid=$("#txtproject").val();
		$.ajax(
		{	
			type:"POST",
			url:"<?php echo base_url();?>index.php/project/get_task_assign",
			dataType: 'json',
			data:{'txtprojectid': txtprojectid},
			success: function(data){
				$("#ddassignto").empty();
				$("#ddassignto").append("<option value=-1>Select</option>");
				$.each(data,function(i,item)
				{
					$('#ddassignto').append("<option value="+item.USER_ID+">"+item.USER_NAME+"</option>");
				});
			}
		});
	}
</script>

<script>
	function submitFile()
	{
		var file=$("#userfile").val();
		if(file!="")
		{
			
			$("#function").html("<div>Associates Updatation has been processed.<img id='checkmark' src='<?php echo base_url(); ?>/application/libraries/assets/img/loading-spinner-blue.gif' /></div>");
			
			var formUrl = "<?php echo base_url(); ?>index.php/project/uploadfile1";
			var formData = new FormData($('.horizontal-form')[0]);
			$.ajax({
				url: formUrl,
				type: 'POST',
				data: formData,
				mimeType: "multipart/form-data",
				contentType: false,
				cache: false,
				processData: false,
				success: function(data){
					//now get here response returned by PHP in JSON fomat you can parse it using JSON.parse(data)
					$("#filename").val(data);
					inserts('insert_data');
				},
				error: function(jqXHR, textStatus, errorThrown){
					//handle here error returned
				}
			});
			
		}
		else
		{
			inserts('insert_data');
		}
	}
</script>

<script>
	var switch_url="<?php echo base_url();?>index.php/project/insert_project_task",
	load_url="<?php echo base_url(); ?>index.php/project/after_view_all_task";
</script>  

<script>
	$("#checkrec").change(function(){
		
		var ischecked=$(this).is(':checked'); 
		if(ischecked)
		{	
			$("#checked-a").fadein(200);
		}
		else
		{
			document.getElementById("ddschedule").selectedIndex = "0";
			$("#checked-a").fadein(200);
		}
		
	});
</script>	