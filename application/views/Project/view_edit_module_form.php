<?php 
	foreach($rec->result() as $row)
	{
		break;
	}
	?>
<div class="portlet-body form">
	<form action="#" class="form-horizontal" method="post" id="insert_data">
		<div class="form-body">
			
			<div class="form-group">
				<label class="col-md-3 control-label">Module Name</label>
				<div class="col-md-6">
					<input type="text" class="form-control input-sm empty" id="txtname" name="txtname" value="<?php echo $row->m_module_name; ?>" placeholder="Enter Module Name" />
					<input type="hidden" id="txtparentid" name="txtparentid" value="0" />
					<span id="divtxtname" style="color:red"></span>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label">Attactment</label>
				<div class="col-md-6">
					<div class="fileinput fileinput-new input-xs" data-provides="fileinput">
						<div class="input-group">
							<div class="form-control uneditable-input input-xs " data-trigger="fileinput">
								<i class="fa fa-file fileinput-exists"></i>&nbsp;
								<span class="fileinput-filename"><?php echo $row->m_module_doc; ?>
								</span>
							</div>
							<span class="input-group-addon default btn-file btn-xs ">
								<span class="fileinput-new input-xs"> Select file </span>
								<span class="fileinput-exists input-xs"> Change </span>
								<input type="file" class="input-xs" name="userfile" id="userfile" value="<?php echo $row->m_module_doc; ?>">
							</span>
							<a href="javascript:;" class="input-group-addon btn btn-xs red fileinput-exists" data-dismiss="fileinput"> Remove </a>
						</div>
					</div>
					<input type="hidden" name="txtfileatt" id="txtfileatt" >
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label">Module Description</label>
				<div class="col-md-6">
				<textarea class="form-control input-sm empty" id="txtdescription" rows="4" name="txtdescription" placeholder="Enter Module Description."><?php echo $row->m_module_description; ?></textarea>
				<span id="divtxtdescription" style="color:red"></span>
			</div>
		</div>
	</div>
	
	<div class="form-actions">
		<div class="col-md-9 col-md-offset-3">
			<input type="button" onclick="submitFile()" class="btn green" value="Submit">
			<button type="reset" onclick="goto('<?php echo base_url(); ?>project/after_view_project')" class="btn">Cancel</button>
		</div>
	</div>
</form>
</div>

<script>
	function submitFile()
	{
		var userf=$("#userfile").val();
		if(check("insert_data"))
		{
			if(userf!='<?php echo $row->m_module_doc; ?>')
			{
				var formUrl = "<?php echo base_url(); ?>project/moduledoc/";
				var formData = new FormData($('#insert_data')[0]);
				$.ajax({
					url: formUrl,
					type: 'POST',
					data: formData,
					mimeType: "multipart/form-data",
					contentType: false,
					cache: false,
					processData: false,
					success: function(data){
						//now get here response returned by PHP in JSON fomat you can parse it using JSON.parse(data)
						$("#txtfileatt").val(data);
						inserts('insert_data');
					},
					error: function(jqXHR, textStatus, errorThrown){
						//handle here error returned
					}
				});
			}
		}
	}
</script>


<script>
	var switch_url="<?php echo base_url(); ?>project/update_module/<?php echo $row->m_module_id; ?>",
	load_url="<?php echo base_url(); ?>project/edit_module/<?php echo $proj_id; ?>";
</script>