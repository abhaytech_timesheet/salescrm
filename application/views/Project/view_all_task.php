<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-archive"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_project">
							Project
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="icon-list"></i>
						<span>
							Project Task
						</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> 
				View Task 
				
			</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					
					<div class="table-responsive" id="data1">
						<form method="post" id="myform" action="<?php echo base_url(); ?>index.php/project/view_all_task" >
							<table class="table table-striped table-bordered table-hover table-full-width" id="datatable_products">
								<thead>
									<tr role="row" class="heading">
										<th width="15%">
											Project Name
										</th>
										<th width="15%">
											Assign To
										</th>
										<th width="15%">
											Task Start Date
										</th>
										<th width="15%">
											Task End Date
										</th>

                                        <th width="15%">
											Task Type
										</th>

										<th width="10%">
											Actions
										</th>
									</tr>
									
									<tr role="row" class="filter">
										<td>
											<input type="text" id="txtproject" name="txtproject" class="form-control input-sm" placeholder="Enter Project Name.">
										</td>
										
										<td>
											<input type="text" id="txtassign_to" name="txtassign_to" class="form-control input-sm" placeholder="Enter Assign To.">
										</td>
										
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control input-sm" readonly name="txtfrom" id="txtfrom" placeholder="From">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										
										<td>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control input-sm" readonly name="txtto" id="txtto" placeholder="To">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
                                        
                                        <td>
											<select id="ddtasktype" name="ddtasktype" class="form-control input-sm opt">
                                            <option value="-1">Select Task Type</option>
                                            <option value="1">Task Assigned By</option>
                                            <?php if($this->session->userdata('user_type')==1) { ?>
                                            <option value="2">Task Assign To </option>
                                            <option value="3">Task Followed By</option>
                                            <?php } ?>
                                            </select>
										</td>
										
										<td>
											<button type="button" onclick="return conwv('myform')" class="btn btn-sm green filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
											<button type="reset" class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
										</td>
									</tr>
								</thead>
							</table>
						</form>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-list font-dark"></i>
								<span class="caption-subject bold uppercase"> 
									View & Modify Task
								</span>
							</div>
							<div class="actions">
								<div class="btn-group btn-group-devided" data-toggle="buttons">
									<a href="javascript:void(0)">
										<button class="btn green btn-sm blue-stripe" onclick="goto('<?php echo base_url();?>index.php/project/view_create_task')">Create Task on Project</button>
									</a>
								</div>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<h5>&nbsp;</h5>
										</div>
									</div>
									<div class="col-md-6">
										
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S.No</th>
										<th>Action</th>
										<th>Task Title</th>
										<th>Project Name</th>
										<th>Assign By</th>
										<th>Assign To</th>
										<th>Followed By</th>
										<th>Priority</th>
										<th>Start Date</th>
										<th>End Date</th>
										<th>Create Date</th>
										<th>Description</th>
										<th>Attachment</th>
									</tr>
								</thead>
								<tbody>
									<?php
										foreach($rec->result() as $row)
										{
										?>
										<tr>
											<td><?php echo $row->SN; ?></td>
											<td nowrap>
												<a href="javascript:void(0)" onclick="goto('<?php echo base_url();?>index.php/project/view_task_follow_up/<?php echo $row->PTASK_ID; ?>')" class="btn blue btn-xs"> <i class="fa fa-road"></i>Followup</a>
												<?php
													if($row->PTASK_STATUS==1 && $this->session->userdata('profile_id')==$row->PTASK_FOLLOWER || $this->session->userdata('usertype')==2)
													{																	
													?>
													<a href="javascript:void(0)" onclick="goto('<?php echo base_url();?>index.php/project/view_task_complete/<?php echo $row->PTASK_ID; ?>/2')" class="btn green btn-xs"><i class="fa fa-thumbs-o-up"></i> Complete </a>
													
													<a href="javascript:void(0)" onclick="goto('<?php echo base_url();?>index.php/project/view_task_edit/<?php echo $row->PTASK_ID; ?>')" class="btn red btn-xs"><i class="fa fa-pencil-square-o"></i> Edit </a>
													
													<a href="javascript:void(0)" onclick="goto('<?php echo base_url();?>index.php/project/view_task_complete/<?php echo $row->PTASK_ID; ?>/0')" class="btn red btn-xs"><i class="fa fa-trash-o"></i>Disable</a>
													<?php
													}
												?>
												<?php
													if($row->PTASK_STATUS==0 && $this->session->userdata('profile_id')==$row->PTASK_FOLLOWER || $this->session->userdata('usertype')==2)
													{																	
													?>
													<a href="javascript:void(0)" onclick="goto('<?php echo base_url();?>index.php/project/view_task_complete/<?php echo $row->PTASK_ID; ?>/1')" class="btn green btn-xs"><i class="fa fa-refresh"></i>Task Enable</a>
													<?php
													}
												?>
											</td>
											<td><?php echo $row->PTASK_TITLE; ?></td>
											<td><?php echo $row->Account_name.'<span style="color:red">/</span>'.$row->Website_name.'<span style="color:red">/</span>'.$row->PROJECT_NAME; ?></td>
											<td><?php echo $row->PTASK_ASSIGN_BY; ?></td>
											<td><?php echo $row->ASSIGN_TO_NAME; ?></td>
											<td><?php echo $row->FOLLOWER_NAME; ?></td>
											<td><?php echo $row->PTASK_PROIORTY; ?></td>
											<td><?php echo $row->PTASK_START_DATE; ?></td>
											<td><?php echo $row->PTASK_END_DATE; ?></td>
											<td><?php echo $row->PTASK_CREATE_DATE; ?></td>
											<td><?php echo $row->PTASK_DESCRIPTION; ?></td>
											<td><?php echo $row->PTASK_FILE; ?></td>
											<?php 
											}
										?>    
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->