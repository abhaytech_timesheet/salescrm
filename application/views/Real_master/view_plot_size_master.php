<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Manage Plot Size Master</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Master</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Plot Size Master</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Plot Size Master</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>index.php/real_master/insert_plot_size" >
								<div class="form-body" id="rank">
									<div class="form-group">
										<label class="col-md-1 control-label">Dimension</label>
										<div class="col-md-4">
											<input type="text" id="txtdimension" name="txtdimension"  class="form-control input-sm" value='0.00' onblur='plot_area1()' placeholder="Enter Dimension.">
											<span class="help-block">
												Plot width(Ft.).
											</span>
										</div>
										
										
										<label class="col-md-1 control-label">Inches</label>
										<div class="col-md-4">
											<input type="text" id="txtinches" name="txtinches"  class="form-control input-sm" value='0.00' onblur='plot_area1()' placeholder="Enter Inches.">
										</div>
									</div>
									
									
									<div class="form-group">
										<label class="col-md-1 control-label"></label>
										<div class="col-md-4">
											<input type="text" id="txtplot_height" name="txtplot_height" class="form-control input-sm" value='0.00' onblur='plot_area1()' placeholder="Enter Plot Height(Ft.).">
											<span class="help-block">
												Plot Height(Ft.)
											</span>
										</div>
										
										<label class="col-md-1 control-label">Inches</label>
										<div class="col-md-4">
											<input type="text" id="txtinches1" name="txtinches1" class="form-control input-sm" value='0.00' onblur='plot_area1()' placeholder="Enter Inches.">
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-1 control-label">Plot Area</label>
										<div class="col-md-4">
											<input type="text" id="txtplot_area" name="txtplot_area" class="form-control input-sm" readonly value='' placeholder="Enter Plot Area.">
										</div>
										
										
										<label class="col-md-1 control-label">Per</label>
										<div class="col-md-4">
											<select class="form-control input-sm" name="txtper" id="txtper" onchange="plot_area1()">
												<option value="-1">Select</option>
												<?php
													foreach($unit->result() as $u)
													{
													?>
													<option value="<?php echo $u->m_unit_id; ?>"><?php echo $u->m_unit_name; ?></option>
												<?php } ?>
											</select>
										</div>											
										<input type="hidden" id="txtbooking_amount" name="txtbooking_amount" value='0' >                                 															
									</div>
									<div class="form-actions fluid">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green">Save</button>
											<button type="reset" class="btn">Reset</button>
										</div>
									</div>
								</form>
								
							</div>
						</div>
						
					</div>
					<!-- END PAGE CONTENT-->
					
					
					<div class="col-md-12">
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption font-dark">
									<i class="icon-cursor"></i>
									<span class="caption-subject bold uppercase">View & Modify Plot Size Master</span>
								</div>
								<div class="tools"> </div>
							</div>
							<div class="portlet-body" >
								
								<table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										<tr>
											<th>S.No</th>
											<th>Plot Dimension</th>
											<th>Area</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$sn=0;
											foreach($info->result() as $row)
											{
												$sn++;
											?>       
											<tr>
												<td><?php echo $sn; ?></td>
												<td>[<?php echo $row->m_plot_width_ft; ?>' <?php echo $row->m_plot_width_inch; ?>" X <?php echo $row->m_plot_height_ft; ?>' <?php echo $row->m_plot_height_inch; ?>"]</td>
												<td><?php echo $row->m_plot_Area; ?> <?php echo $row->m_unit_name; ?></td>
												<td><a href="<?php echo base_url(); ?>index.php/real_master/view_edit_plot_size/<?php echo $row->m_plot_size_id; ?>"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;&nbsp;&nbsp;
													<?php 
														if($row->m_plot_status==1)
														{
														?>
														<a href="<?php echo base_url(); ?>index.php/real_master/plot_size_status/<?php echo $row->m_plot_size_id; ?>/0"><span class='glyphicon glyphicon-trash'></span></a>
														<?php
														}
														else
														{
														?>
														<a href="<?php echo base_url(); ?>index.php/real_master/plot_size_status/<?php echo $row->m_plot_size_id; ?>/1"><span class='glyphicon glyphicon-repeat'></span></a>
														<?php
														}
													?></td>
											</tr>
											<?php
											}
										?>                     
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END CONTENT BODY -->
		</div>
	</div>
	<!-- END CONTENT -->	
	
	
	<script>
		function plot_area1()
		{
		  
			var dimension=$('#txtdimension').val();
			var inches=$('#txtinches').val();
			var plot_height=$('#txtplot_height').val();
			var inches1=$('#txtinches1').val();
			
			i_to_f1=parseFloat(inches)*0.0833333;
			new_dimension=parseFloat(dimension)+i_to_f1;
			
			i_to_f2=parseFloat(inches1)*0.0833333;
			new_plot_height=parseFloat(plot_height)+i_to_f2;
			
			plot_area=(new_dimension*new_plot_height).toFixed(2);
			if(dimension!='' && inches!='' && plot_height!='' && inches1!='')
			{
				$('#txtplot_area').val(plot_area);
			}
			inunitarea();
		}
		
		
		function inunitarea()
		{
			
			var area=$("#txtplot_area").val();
			var txtper=$("#txtper").val();
			if(txtper!='' && txtper!='-1' && txtper!='0')
			{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/real_master/view_value/",
				data:{'txtper':txtper},
				success:function(data){
					if(data.trim()!='' || data.trim()!=0)
					{
					area=area/data.trim();
					}
				    $("#txtplot_area").val(area);
				
				}
			});
			}
		}
	</script>						