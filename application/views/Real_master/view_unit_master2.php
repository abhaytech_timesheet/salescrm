<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content" id="rank">
			
			
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Unit Master
					</h3>
					<ul class="page-breadcrumb breadcrumb">
					
						<li>
							<i class="fa fa-home"></i>
							<a href="#">
								Master
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
							 Unit Master
							</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								<!-- <i class="fa fa-angle-down"></i> -->
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
					<div class="col-md-6">
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Unit Master
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
						
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>index.php/master/inser_unit_master" >
								<div class="form-body">
								
									<div class="form-group">
										<label class="col-md-3 control-label">Unit Name</label>
										<div class="col-md-3">
											<input type="text" id="txtunit" name="txtunit" class="form-control input-inline input-medium" placeholder="Enter Unit Name.">
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Unit Description</label>
										<div class="col-md-3">
											<textarea id="txtdescription" name="txtdescription" class="form-control input-inline input-large" placeholder="Enter Unit Description." rows="3"></textarea>
										</div>
									</div>
									
									
									
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-4 col-md-8">
										<button type="submit" class="btn green">Save</button>
										<button type="reset" class="btn default">Reset</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					</div>			
				<!-- END FORM-->
				
				
				
				
				
				<div class="col-md-6">
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>View Unit Master
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
						
						<div class="portlet-body">
							
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
												<tr>
                                                    <th>S.No</th>
                                                    <th>Unit Name</th>
													<th>Unit Description</th>
													<th>Action</th>
													
												</tr>
							</thead>
							<tbody>
                                             	<?php 
												$sn=0;
												foreach($info->result() as $unit)
												{
													$sn++;
												?>												
												<tr>
													<td><?php echo $sn; ?></td>
													<td><?php echo $unit->m_unit_name; ?></td>
													<td><?php echo $unit->m_unit_description; ?></td>
													<td><a href="<?php echo base_url(); ?>index.php/master/view_edit_unit_master/<?php echo $unit->m_unit_id; ?>"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;&nbsp;&nbsp;
														<?php 
														if($unit->m_unit_status==1)
														{
															?>
															<a href="<?php echo base_url(); ?>index.php/master/unit_master_status/<?php echo $unit->m_unit_id; ?>/0"><span class='glyphicon glyphicon-trash'></span></a>
															<?php
														}
														else
														{
															?>
															<a href="<?php echo base_url(); ?>index.php/master/unit_master_status/<?php echo $unit->m_unit_id; ?>/1"><span class='glyphicon glyphicon-repeat'></span></a>
															<?php
														}
															?></td>
													
                                                </tr>
                                                <?php } ?>            
							</tbody>
							</table>
							
							
						</div>
					</div>
					</div>
				
			</div>						
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->