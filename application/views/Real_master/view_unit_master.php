<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Unit Master</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Master</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Unit Master</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Unit Master</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>index.php/real_master/inser_unit_master" >
								<div class="form-body">
									
									<div class="form-group">
										<label class="col-md-3 control-label">Unit Name</label>
										<div class="col-md-8">
											<input type="text" id="txtunit" name="txtunit" class="form-control input-sm" placeholder="Enter Unit Name.">
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Unit Value <code>(in feet)</code></label>
										<div class="col-md-8">
											<input type="text" id="txtunitvalue" name="txtunitvalue" class="form-control input-sm" placeholder="Enter Value">
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Unit Description</label>
										<div class="col-md-8">
											<textarea id="txtdescription" name="txtdescription" class="form-control input-sm" placeholder="Enter Unit Description." rows="3"></textarea>
										</div>
									</div>
									
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-4 col-md-8">
										<button type="submit" class="btn green">Save</button>
										<button type="reset" class="btn default">Reset</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">View Unit Master</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body" >
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S.No</th>
										<th>Unit Name</th>
										<th>Unit Value<small>In Feet</small></th>
										<th>Unit Description</th>
										<th>Action</th>
										
									</tr>
								</thead>
								<tbody>
									<?php 
										$sn=0;
										foreach($info->result() as $unit)
										{
											$sn++;
										?>												
										<tr>
											<td><?php echo $sn; ?></td>
											<td><?php echo $unit->m_unit_name; ?></td>
											<td><?php echo $unit->m_unit_value; ?></td>
											<td><?php echo $unit->m_unit_description; ?></td>
											<td><a href="<?php echo base_url(); ?>index.php/real_master/view_edit_unit_master/<?php echo $unit->m_unit_id; ?>"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;&nbsp;&nbsp;
												<?php 
													if($unit->m_unit_status==1)
													{
													?>
													<a href="<?php echo base_url(); ?>index.php/real_master/unit_master_status/<?php echo $unit->m_unit_id; ?>/0"><span class='glyphicon glyphicon-trash'></span></a>
													<?php
													}
													else
													{
													?>
													<a href="<?php echo base_url(); ?>index.php/real_master/unit_master_status/<?php echo $unit->m_unit_id; ?>/1"><span class='glyphicon glyphicon-repeat'></span></a>
													<?php
													}
												?></td>
												
										</tr>
									<?php } ?>            
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													

