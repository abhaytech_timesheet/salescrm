<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Manage Plot Size Master</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Master</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Plot Size Master</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Manage Plot Size Master</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						
						<?php  
							foreach($info->result() as $row)
							{
								break;
							}
						?>  
						
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>index.php/real_master/update_plot_size/<?php echo $row->m_plot_size_id; ?>" >
								<div class="form-body" id="rank">
									<div class="form-group">
										<label class="col-md-1 control-label">Dimension</label>
										<div class="col-md-3">
											<input type="text" id="txtdimension" name="txtdimension"  class="form-control input-sm" onblur='plot_area1()' placeholder="Enter Dimension." value="<?php echo $row->m_plot_width_ft; ?>">
											<span class="help-block">
												Plot width(Ft.).
											</span>
										</div>
										
										
										<label class="col-md-1 control-label">Inches</label>
										<div class="col-md-3">
											<input type="text" id="txtinches" name="txtinches"  class="form-control input-sm" onblur='plot_area1()' placeholder="Enter Inches." value="<?php echo $row->m_plot_width_inch; ?>">
										</div>
									</div>
									
									
									<div class="form-group">
										<label class="col-md-1 control-label"></label>
										<div class="col-md-3">
											<input type="text" id="txtplot_height" name="txtplot_height" class="form-control input-sm" onblur='plot_area1()' placeholder="Enter Plot Height(Ft.)." value="<?php echo $row->m_plot_height_ft; ?>">
											<span class="help-block">
												Plot Height(Ft.)
											</span>
										</div>
										
										<label class="col-md-1 control-label">Inches</label>
										<div class="col-md-3">
											<input type="text" id="txtinches1" name="txtinches1" class="form-control input-sm" onblur='plot_area1()' placeholder="Enter Inches." value="<?php echo $row->m_plot_height_inch; ?>">
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-1 control-label">Plot Area</label>
										<div class="col-md-3">
											<input type="text" id="txtplot_area" name="txtplot_area" class="form-control input-sm" readonly placeholder="Enter Plot Area." value="<?php echo $row->m_plot_Area; ?>">
										</div>
										
										
										<label class="col-md-1 control-label">Per</label>
										<div class="col-md-3">
											<select class="form-control input-medium" name="txtper" id="txtper">
												<option value="-1">Select</option>
												<?php
													foreach($unit->result() as $u)
													{
													?>
													<option value="<?php echo $u->m_unit_id; ?>" <?php echo ($row->m_plot_per==$u->m_unit_id ? 'selected': ''); ?>><?php echo $u->m_unit_name; ?></option>
												<?php } ?>
											</select>
										</div>
										
										
										<label class="col-md-1 control-label">Booking Amount</label>
										<div class="col-md-3">
											<input type="text" id="txtbooking_amount" name="txtbooking_amount" class="form-control input-sm" placeholder="Enter Booking Amount." value="<?php echo $row->m_plot_booking_amt; ?>">
										</div>
									</div>
									
									
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Update</button>
										<button type="reset" class="btn">Reset</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	


<script>
	function plot_area1()
	{
		var dimension=$('#txtdimension').val();
		var inches=$('#txtinches').val();
		var plot_height=$('#txtplot_height').val();
		var inches1=$('#txtinches1').val();
		
		i_to_f1=parseFloat(inches)*0.0833333;
		new_dimension=parseFloat(dimension)+i_to_f1;
		
		i_to_f2=parseFloat(inches1)*0.0833333;
		new_plot_height=parseFloat(plot_height)+i_to_f2;
		
		plot_area=(new_dimension*new_plot_height).toFixed(2);
		if(dimension!='' && inches!='' && plot_height!='' && inches1!='')
		{
			$('#txtplot_area').val(plot_area);
		}
		
	}
	</script>			