<?php
foreach($site->result() as $charge)
{
	break;
}
?>
			<div class="col-md-4">
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Plot Charges
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
						
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" action="#" >
								<div class="form-body" id="rank">
									<div class="form-group">
										<label class="col-md-6 control-label">Rate</label>
										<div class="col-md-6">
											<?php echo $charge->m_site_rate; ?> / 
											<?php if($charge->m_site_rate_per==1){
											echo 'Sq. Ft.';}
											if($charge->m_site_rate_per==2){
											echo 'Sq. M.';}
											if($charge->m_site_rate_per==3){
											echo 'Bigha';}
											?>
										</div>
									</div>
									
									
									<div class="form-group">
										<label class="col-md-6 control-label">Corner Rate</label>
										<div class="col-md-4">
											<?php echo $charge->m_site_corner_rate; ?> %
										</div>
									</div>
									
									
									<div class="form-group">
									<label class="col-md-6 control-label">Park Facing Rate</label>
										<div class="col-md-4">
											<?php echo $charge->m_site_park_facing; ?> %
										</div>
									</div>
									
									
									<div class="form-group">
										<label class="col-md-6 control-label">Commercial Rate</label>
										<div class="col-md-4">
											<?php echo $charge->m_site_commercial; ?> %
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-6 control-label">Commercial Facing Rate</label>
										<div class="col-md-4">
											<?php echo $charge->m_site_commercial_facing; ?> %
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-6 control-label">Main Road Facing</label>
										<div class="col-md-4">
											<?php echo $charge->m_site_main_raod; ?> %
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-6 control-label">Other Charges</label>
										<div class="col-md-4">
											<?php echo $charge->m_site_other; ?> %
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-6 control-label">Development Charges Method</label>
										<div class="col-md-2">
											<?php 
											if($charge->m_site_dev_charge_method==1){
											echo 'None';}
											if($charge->m_site_dev_charge_method==2){
											echo 'Area Wise';}
											if($charge->m_site_dev_charge_method==3){
											echo 'Plot Wise';}
											?>
										</div>
									</div>
									
									
									<div class="form-group">
										<label class="col-md-6 control-label">Development Charge</label>
										<div class="col-md-2">
											<?php echo $charge->m_site_dev_charge; ?>
										</div>
									</div>
								
							</form>
						</div>
					</div>
				</div>
			</div>
			
			<script>
				$('#txtplot_rate').val(<?php echo $charge->m_site_rate; ?>);
				document.getElementById('txtper').selectedIndex="<?php echo $charge->m_site_rate_per; ?>";
			</script>