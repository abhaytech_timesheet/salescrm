<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Manage Site Master</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Master</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">Manage Site Master</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Manage Site Master</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>index.php/real_master/insert_site_master" >
							<div class="form-body" id="rank">
								<h4>Site Information</h4>
								<div class="form-group">
									<label class="col-md-3 control-label">Site Name</label>
									<div class="col-md-3">
										<input type="text" id="txtsitename" name="txtsitename"  class="form-control input-sm" placeholder="Enter Site Name.">
									</div>
									
									<label class="col-md-1 control-label">Location</label>
									<div class="col-md-3">
										<textarea id="txtlocation" name="txtlocation" rows="3" col="9" placeholder="Enter Location." class="form-control input-sm"></textarea>
									</div>
								</div>
								
								<h4>Plot Charges</h4>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Rate</label>
									<div class="col-md-3">
										<input type="text" id="txtrate" name="txtrate" class="form-control input-sm" placeholder="Enter Rate.">
									</div>
									
									<label class="col-md-1 control-label">Per</label>
									<div class="col-md-3">
										<select class="form-control input-medium" name="txtper" id="txtper">
											<option value="-1">Select</option>
											<?php
												foreach($unit->result() as $u)
												{
												?>
												<option value="<?php echo $u->m_unit_id; ?>"><?php echo $u->m_unit_name; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Corner Rate</label>
									<div class="col-md-3">
										<input type="text" id="txtcorner_rate" name="txtcorner_rate" class="form-control input-sm" maxlength="2" placeholder="Enter Corner Rate.">
									</div>
									
									<label class="col-md-2 control-label">(in percentage)</label>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Park Facing Rate</label>
									<div class="col-md-3">
										<input type="text" id="txtpark_rate" name="txtpark_rate" class="form-control input-sm" maxlength="2" placeholder="Enter Park Facing Rate.">
									</div>
									
									<label class="col-md-2 control-label">(in percentage)</label>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Corner + Park Facing Rate</label>
									<div class="col-md-3">
										<input type="text" id="txtcorner_park" name="txtcorner_park" class="form-control input-sm" maxlength="2" placeholder="Enter Combine Rate.">
									</div>
									
									<label class="col-md-2 control-label">(in percentage)</label>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Commercial Rate</label>
									<div class="col-md-3">
										<input type="text" id="txtcommercial_rate" name="txtcommercial_rate" class="form-control input-sm" maxlength="2" placeholder="Enter Commercial Rate.">
									</div>
									
									<label class="col-md-2 control-label">(in percentage)</label>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Main Road Facing Rate</label>
									<div class="col-md-3">
										<input type="text" id="txtmain_road_rate" name="txtmain_road_rate" class="form-control input-sm" maxlength="2" placeholder="Enter Main Road Facing Rate.">
									</div>
									
									<label class="col-md-2 control-label">(in percentage)</label>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Others</label>
									<div class="col-md-3">
										<input type="text" id="txtother" name="txtother" class="form-control input-sm" maxlength="4" placeholder="Enter Other Rate.">
									</div>
									
									<label class="col-md-2 control-label">(in percentage)</label>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Commercial Facing Rate</label>
									<div class="col-md-3">
										<input type="text" id="txtcommercial_frate" name="txtcommercial_frate" class="form-control input-sm" maxlength="2" placeholder="Enter Commercial Facing Rate.">
									</div>
									
									<label class="col-md-2 control-label">(in percentage)</label>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Development Charges Method</label>
									<div class="col-md-6">
										<div class="radio-list">
											<label class="radio-inline">
											<input type="radio" name="optionsRadios" id="chnone" value="1" onclick="redio_check(1)" checked> None </label>
											<label class="radio-inline">
											<input type="radio" name="optionsRadios" id="charea_wise" value="2"  onclick="redio_check(2)"> Area Wise </label>
											<label class="radio-inline">
											<input type="radio" name="optionsRadios" id="chplot_wise" value="3"  onclick="redio_check(3)"> Plot Wise </label>
										</div>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Development Charge</label>
									<div class="col-md-3">
										<input type="text" id="txtdevelopment_chrg" name="txtdevelopment_chrg" class="form-control input-sm" disabled placeholder="Enter Development Charge.">
									</div>
									
									<label class="col-md-1 control-label">Per</label>
									<div class="col-md-3">
										<select class="form-control input-medium" name="txtper1" id="txtper1" disabled>
											<option value="-1">Select</option>
											<?php
												foreach($unit->result() as $u)
												{
												?>
												<option value="<?php echo $u->m_unit_id; ?>"><?php echo $u->m_unit_name; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Point Value@</label>
									<div class="col-md-3">
										<input type="text" id="txtplot_value" name="txtplot_value" class="form-control input-sm" placeholder="Enter Plot Value.">
									</div>
								</div>
								
								
							</div>
							<div class="form-actions fluid">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn green">Save</button>
									<button type="reset" class="btn default">Reset</button>
								</div>
							</div>
						</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">View Unit Master</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body" >
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
								<tr>
									<th>S.No</th>
									<th>Site Name</th>
									<th>Location Name</th>
									<th>Rate</th>
									<th>Corner Rate</th>
									<th>Park Facing Rate</th>
									<th>Corner + Park Facing Rate</th>
									<th>Commercial Rate</th>
									<th>Main Road Facing Rate</th>
									<th>Other Rate</th>
									<th>Commercial Facing Rate</th>
									<th>Development Method</th>
									<th>Development Charge</th>
									<th>PV Rate</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$sn=0;
									foreach($info->result() as $row)
									{
										$sn++;
									?>            		
									<tr>
										<td><?php echo $sn; ?></td>
										<td><?php echo $row->m_site_name; ?></td>
										<td><?php echo $row->m_site_location; ?></td>
										<td><?php echo $row->m_site_rate; ?>&nbsp;<?php echo $row->UNIT_NAME1; ?></td>
										<td><?php echo $row->m_site_corner_rate; ?></td>
										<td><?php echo $row->m_site_park_facing; ?></td>
										<td><?php echo $row->m_site_corner_park; ?></td>
										<td><?php echo $row->m_site_commercial; ?></td>
										<td><?php echo $row->m_site_main_raod; ?></td>
										<td><?php echo $row->m_site_other; ?></td>
										<td><?php echo $row->m_site_commercial_facing; ?></td>
										<td><?php echo $row->m_site_dev_charge_method; ?></td>
										<td><?php echo $row->m_site_dev_charge; ?>&nbsp;<?php echo $row->UNIT_NAME; ?></td>
										<td><?php echo $row->m_site_point_value; ?></td>
										<td><a href="<?php echo base_url(); ?>index.php/real_master/view_edit_site_master/<?php echo $row->m_site_id; ?>"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;&nbsp;&nbsp;
											<?php 
												if($row->m_site_point_status==1)
												{
												?>
												<a href="<?php echo base_url(); ?>index.php/real_master/site_master_status/<?php echo $row->m_site_id; ?>/0"><span class='glyphicon glyphicon-trash'></span></a>
												<?php
												}
												else
												{
												?>
												<a href="<?php echo base_url(); ?>index.php/real_master/site_master_status/<?php echo $row->m_site_id; ?>/1"><span class='glyphicon glyphicon-repeat'></span></a>
												<?php
												}
											?></td>
									</tr>
									<?php
									}
								?>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													

<script>
	function redio_check(id)
	{
		if(id==2)
		{
			$('#txtdevelopment_chrg').removeAttr('disabled');
			$('#txtdevelopment_chrg').val('0.00');
			$('#txtper1').removeAttr('disabled');
		}
		if(id==1)
		{
			$('#txtdevelopment_chrg').attr('disabled',true);
			$('#txtdevelopment_chrg').val('');
			$('#txtper1').attr('disabled',true);
		}
		if(id==3)
		{
			$('#txtdevelopment_chrg').removeAttr('disabled');
			$('#txtdevelopment_chrg').val('0.00');
			$('#txtper1').attr('disabled',true);
		}
		
	}
</script>							