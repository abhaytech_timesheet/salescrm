	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								
							</div>
						</li>
						
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">
								Home
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
							
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<div class="row">
				
			<div class="col-md-12 ">
			<!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> 
                    </div>
                    <div class="tools">
                        <a href="" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="" class="reload">
                        </a>
                        <a href="" class="remove">
                        </a>
                    </div>
                </div>
               <?php 
			   foreach($content->result() as $rows)
			   { 
			   ?>
			   
                <div class="portlet-body form">
                    <div class="form-body">
                        <div class="form-group">
                                <label>Title</label>
                                <input type="text" class="form-control input-large" id="txttitle" name="txttitle" placeholder="Enter text" value="<?php echo $rows->m_news_title;?>" />
                         </div>
                                
                         <div class="form-group">
                        <label>Date of News</label>
                        <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                                <input type="text" class="form-control" id="news_date" value="<?php echo $rows->m_entrydate;?>" name="news_date" readonly>
                                <span class="input-group-btn">
                                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                </span>
                            </div>
                        </div>
                    
                            <div class="form-group">
                                <div>
                                    <textarea class="ckeditor form-control" name="editor1"  id="editor1" rows="6"><?php echo $rows->m_news_des;?></textarea>
                                </div>
                            </div>
                           
                                <label class="control-label col-md-3">Status</label>
                                <div class="col-md-4">
                                    <select class="bs-select form-control" name="status" id="status">
                                        <option value="-1">Select</option>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                        <div style="clear:both"></div>
                        <div class="form-actions">
                            <button  onclick="update()" class="btn green"><i class="fa fa-check"></i> Save</button>								
                        </div>
                    </div>
            
            <input type="hidden" id="hidden" name="hidden" value="<?php echo $rows->m_news_id;?>" />
            <!-- END SAMPLE FORM PORTLET-->
            </div>
			<?php } ?>
				</div>
				
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
</div>

<script>
function update()
{
	var title = $('#txttitle').val();
	var editor = CKEDITOR.instances.editor1;
	//alert( editor.getData() );
	var content=editor.getData();
	var date=$('#news_date').val();
	var status =$('#status').val();
	var id = $('#hidden').val();
 	$.ajax(
        {
 	type: "POST",
 	url:"<?php echo base_url(); ?>index.php/cms/update_news/",
 	data: "title="+title+"&content="+content+"&news_date="+date+"&status="+status+"&id="+id,
 	success: function(msg) {
 	      alert(msg);
         }
 
       });
	
}
</script>
