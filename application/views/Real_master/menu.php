<!-- BEGIN BODY -->

<body class="page-header-fixed page-sidebar-fixed">
<!-- BEGIN HEADER -->
<div class="header navbar navbar-fixed-top"> 
  <!-- BEGIN TOP NAVIGATION BAR -->
  <div class="header-inner"> 
    <!-- BEGIN LOGO -->
    <div class="navbar-brand" style="margin-top: -30px; margin-left: 33px; width:400px;">
      <h1 style="font-size: 30px;" class="fontsforweb_fontid_17281"> <a href="http://rssgroup.org" title="Rss Group" style="text-decoration:none;"> <span style="color:#FFFFFF;">Rss Group</span> </a> </h1>
    </div>
    <!-- END LOGO --> 
    <!-- BEGIN RESPONSIVE MENU TOGGLER --> 
    <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <img src="<?php echo base_url() ?>application/libraries/assets/img/menu-toggler.png" alt=""/> </a> 
    <!-- END RESPONSIVE MENU TOGGLER --> 
    <!-- BEGIN TOP NAVIGATION MENU -->
    <ul class="nav navbar-nav pull-right">
      <!-- BEGIN USER LOGIN DROPDOWN -->
      <li class="dropdown user"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> <img alt="" src="<?php echo base_url() ?>application/libraries/assets/img/avatar1_small.jpg"/> <span class="username"> <?php echo $this->session->userdata('name'); ?> </span> <i class="fa fa-angle-down"></i> </a>
        <ul class="dropdown-menu">
          <li> <a href="#"> <i class="fa fa-user"></i>ID-<?php echo $this->session->userdata('user_id'); ?> </a> </li>
          <li> <a href="#"> <i class="fa fa-user"></i>NAME-<?php echo $this->session->userdata('name'); ?> </a> </li>
          <li> <a href="<?php echo base_url() ?>index.php/auth/logout"> <i class="fa fa-key"></i> Log Out </a> </li>
        </ul>
      </li>
      <!-- END USER LOGIN DROPDOWN -->
    </ul>
    <!-- END TOP NAVIGATION MENU --> 
  </div>
  <!-- END TOP NAVIGATION BAR --> 
</div>
<!-- END HEADER -->
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
  <div id="menu" class="page-sidebar navbar-collapse collapse"> 
    <!-- BEGIN SIDEBAR MENU -->
    <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
      <li class="sidebar-toggler-wrapper"> 
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="sidebar-toggler hidden-phone"> </div>
        <!-- BEGIN SIDEBAR TOGGLER BUTTON --> 
      </li>
      <li class="sidebar-search-wrapper"> 
        <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
        <form class="sidebar-search" action="#" method="POST">
          <div class="form-container">
            <div class="input-box"> <a href="javascript:;" class="remove"></a>
              <input type="text" placeholder="Search..."/>
              <input type="button" class="submit" value=" "/>
            </div>
          </div>
        </form>
        <!-- END RESPONSIVE QUICK SEARCH FORM --> 
      </li>
		
	<li class="active"> <a href="javascript:;"><i class="fa fa-cogs"></i><span class="title"> Master</span><span class="arrow "></span></a>
        <ul class="sub-menu">
		  <li><a href="<?php echo base_url() ?>index.php/master/view_mainconfig"><i class="fa fa-cogs"></i> Manage Configuration</a></li>
          <li><a href="<?php echo base_url() ?>index.php/master/view_city"><i class="fa fa-th-large"></i> Manage City</a></li>
          <li><a href="<?php echo base_url() ?>index.php/master/view_bank"><i class="fa fa-money"></i> Manage Bank</a></li>
		  <li><a href="<?php echo base_url() ?>index.php/master/view_news"><i class="fa fa-bullhorn"></i><span class="title"> Manage News</span></a></li>
          <li><a href="<?php echo base_url() ?>index.php/master/plan_master"><i class="fa fa-th"></i> Plan Master</a></li>
		  <li><a href="<?php echo base_url() ?>index.php/real_master/unit_master"><i class="fa fa-fire"></i> Unit Master</a></li>
		  <li><a href="<?php echo base_url() ?>index.php/real_master/plot_size_master"><i class="fa fa-road"></i> Plot Size Master</a></li>
		  <li><a href="<?php echo base_url() ?>index.php/real_master/site_master"><i class="fa fa-plus"></i> Site Master</a></li>
		  <li><a href="<?php echo base_url() ?>index.php/real_master/site_plot_master"><i class="fa fa-th"></i> Site Plot Master</a></li>
		  <li><a href="<?php echo base_url() ?>index.php/real_master/plan_master"><i class="fa fa-th"></i> Plan Master</a></li>
          
        </ul>
      </li>
	
	  
	  
	  <li> <a href="javascript:;"><i class="fa fa-file-text-o"></i><span class="title"> E-pin Panel</span><span class="arrow "></span></a>
        <ul class="sub-menu">
			<li><a href="<?php echo base_url();?>index.php/pin/generate_pin"><i class="fa fa-upload"></i> Generate Pin</a></li>
			<li><a href="<?php echo base_url();?>index.php/pin/generate_pin1"><i class="fa fa-refresh"></i> Pin Transfer</a></li>
			<li><a href="<?php echo base_url();?>index.php/pin/generate_pin2"><i class="fa fa-check-square-o"></i> Pin Approval</a></li>
			<li><a href="<?php echo base_url();?>index.php/pin/view_cancel_pin" ><i class="fa fa-ban"></i> Cancel Pin</a></li>
			<li><a href="<?php echo base_url();?>index.php/pin/pin_reoprt" ><i class="fa fa-list-ol"></i> Pin Report</a></li>
			<li><a href="<?php echo base_url();?>index.php/pin/member_pin_details" ><i class="fa fa-group"></i> Pin Report Member</a></li>
			<li><a href="<?php echo base_url();?>index.php/pin/current_pin_position"><i class="fa fa-info"></i> Current Pin Position</a></li>
			<li><a href="<?php echo base_url();?>index.php/pin/pin_req_report"><i class="fa fa-plus-square-o"></i> Pin Request Report</a></li>
        </ul>
      </li> 
	  
	  
	  
      <li> <a href="javascript:;"> <i class="fa fa-users"></i> <span class="title"> Registration</span> <span class="arrow "></span> </a>
        <ul class="sub-menu">
          <li><a href="<?php echo base_url() ?>index.php/corporate/member_reg/"><i class="fa fa-users"></i> Member Registration</a></li>
		  <li><a href="<?php echo base_url() ?>index.php/corporate/plot_booking/"><i class="fa fa-sitemap"></i> Plot Booking</a></li>
        </ul>
      </li>
	  
	  
	  
	  <li> <a href="javascript:;"> <i class="fa fa-magic"></i> <span class="title">Transaction</span> <span class="arrow "></span> </a>
        <ul class="sub-menu">
		  <li><a href="<?php echo base_url() ?>index.php/corporate/edit_member_details/"><i class="fa fa-edit (alias)"></i> Edit Member Details</a></li>
          <li><a href="<?php echo base_url() ?>index.php/corporate/view_installment_pay/"><i class="fa fa-rupee"></i> Installment Payment</a></li>
          <li><a href="#"><i class="fa fa-check-square-o"></i> Cheque/DD Approval</a></li>
		  <li><a href="<?php echo base_url() ?>index.php/corporate/view_edit_plot_status"><i class="fa fa-pencil"></i> Update Plot Status</a></li>
		  <li><a href="#"><i class="fa fa-pencil-square"></i> Update Member Plot</a></li>
        </ul>
      </li>

	  <li> <a href="javascript:;"> <i class="fa fa-list-alt"></i> <span class="title"> Report</span> <span class="arrow "></span> </a>
        <ul class="sub-menu">
       	<li><a href="<?php echo base_url() ?>index.php/admin_report/plotbook_report"><i class="fa fa-search"></i> Plot Booking Report</a></li>
          <li><a href="<?php echo base_url() ?>index.php/admin_report/member_search"><i class="fa fa-search"></i> Search Member</a></li>
          <li><a href="<?php echo base_url() ?>index.php/admin_report/plot_member_report"><i class="fa fa-search-plus"></i> Search Plot Member</a></li>
		  <li><a href="#"><i class="fa fa-dedent (alias)"></i> Pendencies Report</a></li>
		  <li><a href="#"><i class="fa fa-inr"></i> Payments Report</a></li>
		  <li><a href="#"><i class="fa fa-calendar"></i> Consolidated Date Wise Payments Report</a></li>
		  <li><a href="#"><i class="fa fa-trophy"></i> Reward Achievers</a></li>
		  <li><a href="#"><i class="fa fa-money"></i> Plot Holder Payment Report</a></li>
        </ul>
      </li>
	  
	  
      <li> <a href="javascript:;"> <i class="fa fa-file-text-o"></i> <span class="title">Document</span> <span class="arrow "></span> </a>
        <ul class="sub-menu">
          <li><a href="#"><i class="fa fa-hospital-o"></i> Payment Receipts</a></li>
          <li><a href="#"><i class="fa fa-meh-o"></i> Members Welcome Letter</a></li>
		  <li><a href="#"><i class="fa fa-rupee"></i> Wing Bankers</a></li>
        </ul>
      </li>
      
      <li> <a href="javascript:;"><i class="fa fa-cog"></i><span class="title">Manage</span><span class="arrow "></span></a>
        <ul class="sub-menu">
          <li><a href="<?php echo base_url() ?>index.php/corporate/area_calculator"><i class="fa fa-compass"></i> Area Calculator</a></li>
          <li><a href="<?php echo base_url() ?>index.php/corporate/change_password"><i class="fa fa-edit"></i> Change Password</a></li>
        </ul>
      </li>
	  
	  
	  
	  <li> <a href="javascript:;"><i class="fa fa-sitemap"></i><span class="title"> Downline</span><span class="arrow "></span></a>
        <ul class="sub-menu">
          <li><a href="<?php echo base_url() ?>index.php/corporate/tree?i=1"><i class="fa fa-arrows"></i> Tree View</a></li>
        </ul>
      </li>
	  
	  
      
    </ul>
    
    <!-- END SIDEBAR MENU --> 
  </div>
</div>
<!-- END SIDEBAR -->