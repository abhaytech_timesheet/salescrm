<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Manage Site Plot Master</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Master</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Site Plot Master</h3>
			<!-- END PAGE TITLE-->
			
			<div class="note note-success">
				<div class="form-body">
					<div class="form-group">
						<label class="col-md-offset-2 col-md-2 control-label">Select Site :</label>
						<div class="col-md-5">
							<select class="form-control input-xlarge" name="txtsite" id="txtsite" onchange="site()">
								<option value="-1">Select</option>
								<?php 
									foreach($site->result() as $master)
									{
									?>
									<option value="<?php echo $master->m_site_id; ?>"><?php echo $master->m_site_name; ?></option>
									<?php
									}
								?>
							</select>
						</div>
					</div>
				</div><br><br>	
			</div>
			
			<div class="row">
				<div id="plot_charge">
					<div class="col-md-4">
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-cursor"></i>
									<span class="caption-subject bold uppercase">Plot Charges</span>
								</div>
								
								<div class="actions">
									<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
										<i class="icon-cloud-upload"></i>
									</a>
									<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
										<i class="icon-wrench"></i>
									</a>
									<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
									<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
										<i class="icon-trash"></i>
									</a>
								</div>
							</div>
							<div class="portlet-body form">
								<form class="form-horizontal" role="form" method="post" action="#" >
									<div class="form-body" id="rank">
										<div class="form-group">
											<label class="col-md-6 control-label">Rate</label>
											<div class="col-md-2">
												/
											</div>
										</div>
										
										
										<div class="form-group">
											<label class="col-md-6 control-label">Corner Rate</label>
											<div class="col-md-2">
												%
											</div>
										</div>
										
										
										<div class="form-group">
											<label class="col-md-6 control-label">Park Facing Rate</label>
											<div class="col-md-2">
												%
											</div>
										</div>
										
										
										<div class="form-group">
											<label class="col-md-6 control-label">Commercial Rate</label>
											<div class="col-md-2">
												%
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-md-6 control-label">Commercial Facing Rate</label>
											<div class="col-md-2">
												%
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-md-6 control-label">Main Road Facing</label>
											<div class="col-md-2">
												%
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-md-6 control-label">Other Charges</label>
											<div class="col-md-2">
												%
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-md-6 control-label">Development Charges Method</label>
											<div class="col-md-2">
												
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-md-6 control-label">Development Charges Method</label>
											<div class="col-md-2">
												
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-md-6 control-label">Development Charge</label>
											<div class="col-md-2">
												
											</div>
										</div>
										
									</form>
									
								</div>
							</div>
							
						</div>
					</div>
					<!-- END PAGE CONTENT-->
					
				</div>
				
				<div class="col-md-8">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Plot Information</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>index.php/real_master/insert_site_plot" >
								<div class="form-body" id="rank">
									<div class="form-group">
										<label class="col-md-3 control-label">Plot No</label>
										<div class="col-md-9">
											<input type="text" id="txtplot_no" name="txtplot_no"  class="form-control input-sm" placeholder="Enter Name / Number.">
										</div>
									</div>
									
									<input type="hidden" name="siteid" id="siteid" />
									
									<div class="form-group">
										<label class="col-md-3 control-label">Plot Rate</label>
										<div class="col-md-9">
											<input type="text" id="txtplot_rate" name="txtplot_rate"  class="form-control input-sm" placeholder="Enter Inches." onblur="detail()">
											
											<select class="form-control input-sm" name="txtper" id="txtper">
												<option value="-1">Select</option>
												<?php
													foreach($unit->result() as $u)
													{
													?>
													<option value="<?php echo $u->m_unit_id; ?>"><?php echo $u->m_unit_name; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									
									
									<div class="form-group">
										<label class="col-md-3 control-label">Plot Type</label>
										<div class="col-md-9">
											<div class="checkbox-list">
												
												<input type="checkbox" class='c' id="chnormal" value="1" onclick="chcheck()"> Normal 
												<br /><div id="check_hid">
													<input type="checkbox" class='c' id="chcorner" value="2" onclick="chcheck()"> Corner 
													
													<input type="checkbox" class='c' id="chpark_facing" value="3" onclick="chcheck()"> Park Facing 
													
													<input type="checkbox" class='c' id="chcommercial" value="4" onclick="chcheck()"> Commercial 
													
													<input type="checkbox" class='c' id="chcommercial_facing" value="5" onclick="chcheck()"> Commercial Facing 
													<br />
													<input type="checkbox" class='c' id="ch_road_facing" value="6" onclick="chcheck()"> Main Road Facing 
													
													<input type="checkbox" class='c' id="chother_charge" value="7" onclick="chcheck()"> Other Charges 
												</div>
											</div>
										</div>
									</div>
									
									
									<div class="form-group">
										<label class="col-md-3 control-label">Development Method</label>
										<div class="col-md-9">
											<div class="radio-list">
												<label class="radio-inline">
												<input type="radio" name="optionsRadios" id="chnone" value="1" onclick='rec_check(1)' checked> None </label>
												<label class="radio-inline">
												<input type="radio" name="optionsRadios" id="charea_wise" value="2" onclick='rec_check(2)'> Area Wise </label>
												<label class="radio-inline">
												<input type="radio" name="optionsRadios" id="chplot_wise" value="3" onclick='rec_check(3)'> Plot Wise </label>
											</div>
										</div>
									</div>
									
									<div class="form-group" id='dvc' style='display:none;'>
										<label class="col-md-3 control-label">Development Charge</label>
										<div class="col-md-9">
											<input type="text" id="txtdev_charge" name="txtdev_charge" class="form-control input-sm" value='' placeholder="Enter Plot Area.">
											/
											<select class="form-control input-sm" name="txtper1" id="txtper1">
												<option value="-1">Select</option>
												<?php
													foreach($unit->result() as $u)
													{
													?>
													<option value="<?php echo $u->m_unit_id; ?>"><?php echo $u->m_unit_name; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Plot Size</label>
										<div class="col-md-9">
											<select class="form-control input-medium" name="txtplot_size" id="txtplot_size" onchange="detail()">
												<option value="-1">Select</option>	
												<?php 
													$dim="";
													foreach($plot->result() as $plot_size)
													{
														foreach($unit->result() as $u)
														{
															if($plot_size->m_plot_per==$u->m_unit_id)
															{
																$dim=$u->m_unit_name;
															}
														}
														
													?>
													<option value="<?php echo $plot_size->m_plot_size_id; ?>"><?php echo $plot_size->m_plot_Area; echo " ".$dim; ?> [<?php echo $plot_size->m_plot_width_ft; ?>' <?php echo $plot_size->m_plot_width_inch; ?>" X <?php echo $plot_size->m_plot_height_ft; ?>' <?php echo $plot_size->m_plot_height_inch; ?>"]</option>
													<?php
													}
												?>
											</select>
										</div>
									</div>
									
									<div id="amt">
										<div class="form-group">
											<label class="col-md-3 control-label">Plot Amount</label>
											<div class="col-md-9">
												<input type="text" id="txtplot_amount" name="txtplot_amount" class="form-control input-sm" readonly value='' placeholder="Enter Plot Area.">
											</div>
										</div>
									</div>
								</div>
								
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Save</button>
										<button type="reset" class="btn">Reset</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					
				</div>
			</div>
			
			
			
			<div class="row">
				<div class="col-md-12">
					<div class="note note-danger">
						<form action="<?php echo base_url(); ?>index.php/real_master/search_plot" method="post" name="search_plot">
							<div class="form-body">
								<div class="form-group">
									<label class="col-md-1 control-label"> Site </label>
									<div class="col-md-5">
										<select class="form-control input-xlarge" name="txtsite_search" id="txtsite_search">
											<option value="-1">Select</option>
											<?php 
												foreach($site->result() as $master)
												{
												?>
												<option value="<?php echo $master->m_site_id; ?>"><?php echo $master->m_site_name; ?></option>
												<?php
												}
											?>
										</select>
									</div>
									
									<label class="col-md-1 control-label">  </label>
									<div class="col-md-3">
										<input type="text" id="txtplotno_search" name="txtplotno_search" class="form-control input-inline" placeholder="Enter Plot No.">
									</div>
									<div class="col-md-1">
										<button type="submit" class="btn red">Search Plot</button>
									</div>
								</div>
							</div>
						</form>
						<br><br>
					</div>
					
					<div id="searching_plot">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption font-dark">
									<i class="icon-cursor"></i>
									<span class="caption-subject bold uppercase">View & Modify Plot Site Master</span>
								</div>
								<div class="tools"> </div>
							</div>
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" id="sample_2">
									<thead>
										<tr>
											<th>S.No</th>
											<th>Plot No.</th>
											<th>Site</th>
											<th>Plot Size</th>
											<th>Plot Area</th>
											<th>Plot Rate</th>
											<th>Plot Type</th>
											<th>Dev. Method</th>
											<th>Dev. Charge</th>
											<th>Booking Amount</th>
											<th>Plot Amount</th>
											
										</tr>
									</thead>
									<tbody>
										
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											
										</tr>
										
										
									</tbody>
								</table>
								
							</div>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	

<script>
	function rec_check(id)
	{
		if(id==1)
		{
			$('#dvc').css('display','none');
		}
		if(id==2)
		{
			$('#dvc').removeAttr('style');
			$('#txtper1').removeAttr('style');
		}
		if(id==3)
		{
			$('#dvc').removeAttr('style');
			$('#txtper1').css('display','none');
		}
	}
</script>

<script>
	function chcheck()
	{
		if($('#chnormal').prop('checked') === true)
		{	
			$("#check_hid").css('display','none');
			
		}
		if($('#chnormal').prop('checked') === false)
		{
			$("#check_hid").css('display','block');
		}
	}
</script>

<script>
	function site()
	{
		var site_id=$('#txtsite').val();
		$('#siteid').val(site_id);
		if(site_id!='-1')
		{
			$.blockUI({ message: '<img src="<?php echo base_url(); ?>application/libraries/progressloading.gif" />' });
			setTimeout($.unblockUI, 2000);
			$("#plot_charge").load("<?php echo base_url();?>index.php/real_master/view_plot_charges/"+site_id);
		}
		else
		{
			alert('Please Select Site First');
		}
	}
</script>

<script>
	function detail()
	{
		var size_id=$('#txtplot_size').val();
		if(size_id!='-1')
		{
			$.blockUI({ message: '<img src="<?php echo base_url(); ?>application/libraries/progressloading.gif" />' });
			setTimeout($.unblockUI, 2000);
			$("#amt").load("<?php echo base_url();?>index.php/real_master/fill_amount/"+size_id);
		}
		else
		{
			alert('Please Select Plot Size First');
		}
	}
</script>

<script>
	var quid="";
	var status=0;
	var descr="";
	function chbcheckall()
	{
		quid="";
		var collection=$("#rank");
		var inputs=collection.find("input[type=checkbox]");
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			var name=inputs[x].name;
			if(document.getElementById(id).checked)
			{ 
				quid=id+"-"+quid;
			}
		}
		//alert(quid);
		$("#txtcategory").val(quid);
	}
	
</script>

