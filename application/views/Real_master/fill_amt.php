<?php 
foreach($plot->result() as $amt)
{
	break;
}
?>
<input type="hidden" name="area" id="area" value="<?php echo $amt->m_plot_Area;?>" />
<div class="form-group">
	<label class="col-md-3 control-label">Plot Amount</label>
	<div class="col-md-9">
		<input type="text" id="txtplot_amount" name="txtplot_amount" class="form-control input-inline input-medium" readonly placeholder="Enter Plot Area.">
	</div>
</div>

<script>
var p_rate=$("#txtplot_rate").val();
var p_area=$("#area").val();
p_amt=p_rate*p_area;
$('#txtplot_amount').val(p_amt);
</script>