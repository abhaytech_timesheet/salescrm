<?php
	foreach($info->result() as $row)
	{
		break;
	}
?>
<!-- BEGIN CONTENT -->
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Unit Master</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Master</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Unit Master</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Unit Master</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>index.php/real_master/update_unit_master/<?php echo $row->m_unit_id; ?>" >
								<div class="form-body">
									
									<div class="form-group">
										<label class="col-md-3 control-label">Unit Name</label>
										<div class="col-md-8">
											<input type="text" id="txtunit" name="txtunit" class="form-control input-sm" placeholder="Enter Unit Name." value="<?php echo $row->m_unit_name; ?>">
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Unit Value <code>(in feet)</code></label>
										<div class="col-md-8">
											<input type="text" id="txtunitvalue" name="txtunitvalue" class="form-control input-sm" value="<?php echo $row->m_unit_value;?>" placeholder="Enter Value">
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Unit Description</label>
										<div class="col-md-8">
											<textarea id="txtdescription" name="txtdescription" class="form-control input-sm" placeholder="Enter Unit Description." rows="3"><?php echo $row->m_unit_description; ?></textarea>
										</div>
									</div>
									
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-4 col-md-8">
										<button type="submit" class="btn green">Update</button>
										<button type="reset" class="btn default">Reset</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	
