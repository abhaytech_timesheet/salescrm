<script>
	function check()
	{
		var collection=$("#rank");
		var mark=0;
		var obtainmark=0;
		var inputs=collection.find("input[type=checkbox],input[type=radio]");
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			var name=inputs[x].name;
			if($("#"+id+"").is(':checked'))
			{
				if(id=="radioregistry1")
				{
					$("#txtregistrystatus").val('1');
				}
				if(id=="radioregistry2")
				{
					$("#txtregistrystatus").val('0');
				}
			}
		}
	}
</script>

<script>
	function check1()
	{
		var collection=$("#rank");
		var mark=0;
		var obtainmark=0;
		var inputs=collection.find("input[type=checkbox],input[type=radio]");
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			var name=inputs[x].name;
			if($("#"+id+"").is(':checked'))
			{
				if(id=="radiorefund1")
				{
					$("#txtrefundstatus").val('1');
				}
				if(id=="radiorefund2")
				{
					$("#txtrefundstatus").val('0');
				}
			}
		}
	}
</script>
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig"> Manage Plan Master </a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Master</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Plan Master</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Manage Plan Master</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>index.php/real_master/insert_plan_master" >
							<div class="form-body" id="rank">
								<h4>Site Information</h4>
								<div class="form-group">
									<label class="col-md-3 control-label">Site Name</label>
									<div class="col-md-4">
										<select name="ddsitename" class="form-control input-sm">
											<option value="-1">Select</option>
											<?php foreach($sites->result() as $rowsite)
												{ ?>
												<option value="<?php echo $rowsite->m_site_id; ?>"><?php echo $rowsite->m_site_name; ?></option>
											<?php } ?>
										</select>
									</div>                  
								</div>
								
								<h4>Plan Information</h4>
								<div class="form-group">
									<label class="col-md-3 control-label">Plan Name</label>
									<div class="col-md-4">
										<input type="text" id="txtplanname" name="txtplanname"  class="form-control input-sm" placeholder="Enter Plan Name.">
									</div>
									<label class="col-md-1 control-label">Description</label>
									<div class="col-md-3">
										<textarea id="txtdesc" name="txtdesc" rows="3" col="9" placeholder="Enter Description." class="form-control input-sm"></textarea>
									</div>
								</div>
								<h4>Plot Charges</h4>
								<div class="form-group">
									<label class="col-md-3 control-label">Duration</label>
									<div class="col-md-4">
										<input type="text" id="txtduration" name="txtduration" class="form-control input-sm" maxlength="3" placeholder="Enter Plan Duration."> months
									</div>
									<label class="col-md-2 control-label"> </label>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Booking Amount</label>
									<div class="col-md-4">
										<input type="text" id="txtbookamnt" name="txtbookamnt" class="form-control input-sm" placeholder="Enter Booking Amount."> <del>&#2352;</del> ( INR )
									</div>                 
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Allotment</label>
									<div class="col-md-4">
										<input type="text" id="txtallotment" name="txtallotment" class="form-control input-sm" placeholder="Enter Allotment Rate."> %
									</div>
									<label class="col-md-1 control-label">Days</label>
									<div class="col-md-3">
										<input type="text" id="txtallot_days" name="txtallot_days" class="form-control input-sm" maxlength="3" placeholder="Enter No. of Days.">
										<span class="help-block" style="color:grey;">No. of days from booking date</span>	
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Installment Payment Type</label>
									<div class="col-md-4">
										<select name="ddinstallment_type" id="ddinstallment_type" class="form-control input-sm" onchange="get_installments()">
											<option value="-1">Select</option>
											<option value="1">Daily</option>
											<option value="2">Weekly</option>
											<option value="3">Monthly</option>
											<option value="4">Quarterly</option>
											<option value="5">Half Yearly</option>
											<option value="6">Yearly</option>
											<option value="7">One Time</option>
										</select>
									</div>
								</div>
								
								
								<div class="form-group">
									<label class="col-md-3 control-label">No. of Installments </label>
									<div class="col-md-4">
										<input type="text" id="txtinstallment" name="txtinstallment" class="form-control input-sm" maxlength="3" placeholder="Enter No. of Installments.">
									</div>
									<label class="col-md-1 control-label">Days</label>
									<div class="col-md-3">
										<input type="text" id="txtins_days" name="txtins_days" class="form-control input-sm" maxlength="3" placeholder="Enter No. of Days.">
										<span class="help-block" style="color:grey;">No. of days from booking date</span>	
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Discount</label>
									<div class="col-md-4">
										<input type="text" id="txtdiscount" name="txtdiscount" class="form-control input-sm" placeholder="Enter Discount Rate."> %
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Interest</label>
									<div class="col-md-4">
										<input type="text" id="txtinterest" name="txtinterest" class="form-control input-sm" placeholder="Enter Interest Rate."> %
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Is Registry Amount Included ?</label>
									<div class="col-md-6">
										<div class="radio-list">
											<label class="radio-inline">
												<input type="radio" name="radioregistry" id="radioregistry1" onclick="get_registry(1)"> Yes 
											</label>
											<label class="radio-inline">
												<input type="radio" name="radioregistry" id="radioregistry2" checked="checked" onclick="get_registry(2)"> No 
											</label>
										</div>
									</div>
								</div>
								<input type="hidden" id="txtregistrystatus" name="txtregistrystatus" value="0">
								<div id="load_registry" style="display:none">
									<div class="form-group" >
										<label class="col-md-3 control-label">Registry</label>
										<div class="col-md-4">
											<input type="text" id="txtregistry" name="txtregistry" class="form-control input-sm" placeholder="Enter Registry Rate." > %
										</div>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Is Refundable ?</label>
									<div class="col-md-6">
										<div class="radio-list">
											<label class="radio-inline">
												<input type="radio" name="radiorefund" id="radiorefund1" onclick="get_refund(1)"> Yes 
											</label>
											<label class="radio-inline">
												<input type="radio" name="radiorefund" id="radiorefund2" checked="checked" onclick="get_refund(2)"> No 
											</label>
										</div>
									</div>                 
								</div>
								<input type="hidden" id="txtrefundstatus" name="txtrefundstatus" value="0">
								<div id="load_refund" style="display:none">
									<div class="form-group" >
										<label class="col-md-3 control-label">Refundable</label>
										<div class="col-md-4">
											<input type="text" id="txtrefund" name="txtrefund" class="form-control input-sm" maxlength="2" placeholder="Enter Refundable Rate."> %
										</div>
									</div>
								</div>
								
							</div>
							<div class="form-actions fluid">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn green">Save</button>
									<button type="reset" class="btn default">Reset</button>
								</div>
							</div>
						</form>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">View & Modify Details</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body" >
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
								<tr>
									<th>S.No</th>
									<th>Plan Name</th>
									<th>Plan Duration</th>
									<th>Booking Amount</th>
									<th>Site</th>
									<th>Allotment</th>                 
									<th>Days Within Allotment <br />To Be Paid</th>
									<th>Installment Type</th>
									<th>No. of Installments</th>
									<th>Days Within Installment <br />To Be Paid</th>
									<th>Discount</th>
									<th>Insterest</th>
									<th>Is Registry Amount <br />Included ?</th>
									<th>Registry Rate</th>
									<th>If Refundable ?</th>
									<th>Refundable Rate</th>
									<th>Description</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$sn =1;
									foreach($resul->result() as $rows)
									{
									?>
									<tr>
										<td><?php echo $sn; ?></td>
										<td><?php echo $rows->Plan_name; ?></td>
										<td><?php echo $rows->Plan_duration; ?> months</td>
										<td><del>&#2352;</del> <?php echo $rows->Plan_book_amount; ?></td>
										<td><?php echo $rows->Site_name; ?></td>
										<td><?php echo $rows->Plan_allotment; ?> %</td>
										<td><?php echo $rows->Plan_allot_days; ?></td>
										<td><?php echo $rows->Plan_Installment_type; ?></td>
										<td><?php echo $rows->Plan_installment; ?></td>
										<td><?php echo $rows->Plan_install_days; ?></td>
										<td><?php echo $rows->Plan_discount; ?> %</td>
										<td><?php echo $rows->Plan_interest; ?> %</td>
										<td><?php echo $rows->Plan_is_registry; ?></td>
										<td><?php echo $rows->Plan_registry; ?> %</td>
										<td><?php echo $rows->Plan_is_refundable; ?></td>
										<td><?php echo $rows->Plan_refund; ?> %</td>
										<td><?php echo $rows->Plan_description; ?></td>
										<td><?php echo $rows->Plan_status; ?></td>
										<td></td>
									</tr>
								<?php $sn++; } ?>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->

<script>
	function redio_check(id)
	{
		if(id==2)
		{
			$('#txtdevelopment_chrg').removeAttr('disabled');
			$('#txtdevelopment_chrg').val('0.00');
			$('#txtper1').removeAttr('disabled');
		}
		if(id==1)
		{
			$('#txtdevelopment_chrg').attr('disabled',true);
			$('#txtdevelopment_chrg').val('');
			$('#txtper1').attr('disabled',true);
		}
		if(id==3)
		{
			$('#txtdevelopment_chrg').removeAttr('disabled');
			$('#txtdevelopment_chrg').val('0.00');
			$('#txtper1').attr('disabled',true);
		}
		
	}
</script>

<script>
	function get_registry(id)
	{
		check();
		if(id==1)
		{
			$("#load_registry").removeAttr('style');
			$("#txtcurr_ints").val('');
		}
		if(id==2)
		{
			$("#load_registry").css('display','none');
		}
	}
</script>

<script>
	function get_refund(id)
	{
		check1();
		if(id==1)
		{
			$("#load_refund").removeAttr('style');
		}
		if(id==2)
		{
			$("#load_refund").css('display','none');
		}
	}
</script>
<script>
	function get_installments()
	{
		var duration = $('#txtduration').val();
		var ins_type = $('#ddinstallment_type').val();
		if(ins_type==-1)
		{
			$('#txtinstallment').val('');
		}
		if(ins_type==1)
		{
			var daily = duration / 12;
			var ins = daily * 365;
			$('#txtinstallment').val(ins);
		}
		if(ins_type==2)
		{
			var week = duration / 12;
			var ins = week * 52;
			$('#txtinstallment').val(ins);
		}
		if(ins_type==3)
		{
			var ins = duration / 1;
			$('#txtinstallment').val(ins);
		}
		if(ins_type==4)
		{
			var ins = duration / 4 ;
			$('#txtinstallment').val(ins);
		}
		if(ins_type==5)
		{
			var ins = duration / 2;
			$('#txtinstallment').val(ins);
		}
		if(ins_type==6)
		{
			var ins = duration / 12;
			$('#txtinstallment').val(ins);
		}
		if(ins_type==7)
		{
			$('#txtinstallment').val(0);
		}
		
	}
	</script>									