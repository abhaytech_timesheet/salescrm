<script>
jQuery(document).ready(function(){
	App.init();
	TableDatatablesButtons.init();
<?php 
	if($this->router->fetch_method()=='view_module' || $this->router->fetch_method()=='edit_module')
	{
	?>
		UITree.init();
	<?php
	}
	?>
	ComponentsSelect2.init();
	ComponentsDateTimePickers.init();
	ComponentsBootstrapSelect.init();
	ComponentsBootstrapSwitch.init();
	Dashboard.init();
	
	});
</script>