<section class="sub-banner about">
    <div class="container">
      <div class="position-center-center">
        <h2>TERM OF USE</h2>
        <span style="color:#fff;"><q style="font-size:20px;">TechAarjavam where simplicity is the ultimate sophistication....................</q></span>
	</div>
    </div>
</section>
  
  <!-- Content -->
  <div id="content"> 
    <section class="front-page padding-top-100 padding-bottom-100">
      <div class="container">
        <div class="row">
           <div class="col-md-12"> 
            <!-- Tittle -->
            <div class="heading-block text-left margin-bottom-20">
              <h2>Term of Use</h2>
          <h6>The User shall be deemed to have accepted and agreed to the following terms and conditions upon the Users usage of the website www.techaarjavam.com.</h6>
              <p align="justify">
The term "User" shall mean the User who is browsing the website www.techaarjavam.com (Site).</p>
<p align="justify">The domain address www.techaarjavam.com is owned by Tech Aarjavam. </p>
<p align="justify">All content present on this Site, including all the software, text, images, graphics, video and audio used on this Site, is exclusive and proprietary material owned by Tech Aarjavam.</p>
<p align="justify">By using the Site, the User agrees to follow and be bound by the following terms and conditions and these terms and conditions may be revised or modified at any time by Tech Aarjavam without any notice to the User:
No material from this site may be copied, modified, reproduced, republished, uploaded, transmitted, posted or distributed in any form without prior written permission from Tech Aarjavam. All rights not expressly granted here in are reserved.</p>
<p align="justify">Unauthorized use of the materials appearing on this Site may violate copyright, trademark and other applicable laws, and could result in criminal or civil penalties. Tech Aarjavam may monitor access to the Site.</p>
<p align="justify">Tech Aarjavam may terminate the User's access to the Site at any time for any reason. The provisions regarding disclaimer of warranty, accuracy of information, and indemnification shall survive such termination.</p>
<p align="justify">Tech Aarjavam makes no warranties, express or implied, including, without limitation, those of merchantability and fitness for a particular purpose, with respect to any information, data, statements or products made available on the Site.</p>
<p align="justify">The Site, and all content, materials, information, software, products and services provided on the Site, are provided on an "as is" and "as available" basis.</p>
<p align="justify">Tech Aarjavam shall have no responsibility for any damage to User's computer system or loss of data that results from the download of any content, materials, information from the Site.</p>
<p align="justify">Tech Aarjavam may unilaterally change or discontinue any aspect of the Site at any time, including, its content or features.</p>
<p align="justify">Tech Aarjavam will not be liable for damages of any kind, including without limitation, direct, indirect, incidental or consequential damages (including, but not limited to, damages for lost profits, business interruption and loss of programs or information) arising out of the use of or inability to use the Site, or any information provided on the Site, or for any other reason whatsoever.</p>
<p align="justify">The User agrees to indemnify, defend and hold Tech Aarjavam harmless from and against all losses, expenses, damages and costs, including reasonable attorneys' fees, arising out of or relating to any misuse by the User of the content and services provided on the Site.</p>
<p align="justify">The information contained in the Site has been obtained from sources believed to be reliable. Tech Aarjavam disclaims all warranties as to the accuracy, completeness or adequacy of such information.</p>
<p align="justify">The Site provides links to web sites and access to content, products and services from third parties, including users, advertisers, affiliates and sponsors of the Site. The User agrees that Tech Aarjavam is not responsible for the availability of, and content provided on, third party web sites. The User is requested to peruse the policies posted by other web sites regarding privacy and other topics before use. Tech Aarjavam is not responsible for third party content accessible through the Site, including opinions, advice, statements and advertisements, and the User shall bear all risks associated with the use of such content. Tech Aarjavam is not responsible for any loss or damage of any sort that the User may incur from dealing with any third party.</p>
<p align="justify">Tech Aarjavam makes no warranty that: (a) the Site will meet your requirements; (b) the Site will be available on an uninterrupted, timely, secure, or error-free basis; (c) the results that may be obtained from the use of the Site or any services offered through the Site will be accurate or reliable.</p>

</div>
</div>
</div>
</div>
</section>
</div>
  <!-- End Content --> 
   


