  <!-- End Header --> 
  
  <!--======= SUB BANNER =========-->
  <section class="sub-banner about">
    <div class="container">
      <div class="position-center-center">
        <h2>About Us</h2>
        <span style="color:#fff;"><q style="font-size:20px;">TechAarjavam where simplicity is the ultimate sophistication....................</q></span>
	</div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    
    <!-- Intro -->
    <section class="light-gray-bg solution padding-top-20 padding-bottom-20">
      <div class="container"> 
        
        <!-- Tittle -->
        <div class="heading-block text-center margin-bottom-20">
           <ul class="row text-center">
          
          <!-- Web Analytics -->
          <li class="col-md-3"> <img src="<?php echo base_url()?>application/libraries/front-lib/images/icon-1.png" alt="">
            <h6>Web Creation</h6>
            
          </li>
          
          <!-- Keyword Targeting -->
          <li class="col-md-3"> <img src="<?php echo base_url()?>application/libraries/front-lib/images/icon-2.png" alt="">
            <h6>Process Targeting</h6>
            
          </li>
          
          <!-- Technical Service -->
          <li class="col-md-3"> <img src="<?php echo base_url()?>application/libraries/front-lib/images/icon-3.png" alt="">
            <h6>Technical Service</h6>
           
          </li>
          
          <!-- Support Center -->
          <li class="col-md-3"> <img src="<?php echo base_url()?>application/libraries/front-lib/images/icon-4.png" alt="">
            <h6>Support Center</h6>
           
          </li>
        </ul>
      </div>
    </section>
    
    <!-- Google Front Page -->
    <section class="front-page padding-top-100 padding-bottom-100">
      <div class="container">
        <div class="row">
          <div class="col-md-6"> <img class="img-responsive margin-top-30" src="<?php echo base_url()?>application/libraries/front-lib/images/ipad.jpg" alt=" "> </div>
          <div class="col-md-6"> 
            <!-- Tittle -->
            <div class="heading-block text-left margin-bottom-20">
              <h2>Let's know about us!.</h2>
              <p align="justify">We are a brand who focuses to helps customers visually to enable them to rise.
We work relentlessly to provide to solve problems in ways no one thought of before.
This is why we come to work every day.Our excellence in customer service and technology gives us the edge to support every inch of business in a fabulous way.
In addition, TechAarjavam enjoys a strong presence in Mobile Application, ERP simplification,enterprises application and Digital services.
So Today we are working over adapting changing trends and challenges and it increases predictability of the results delivered, thereby reducing business risks associated with the transfer of responsibility.
</p>
            </div>
            
            <!-- List Style -->
            <ul class="list-style">
              <li>
                <p><img src="<?php echo base_url()?>application/libraries/front-lib/images/list-icon-1.png" alt=""> We deliver client satisfacton. </p>
              </li>
              <li>
                <p><img src="<?php echo base_url()?>application/libraries/front-lib/images/list-icon-2.png" alt=""> High customer retention rate. </p>
              </li>
              <li>
                <p><img src="<?php echo base_url()?>application/libraries/front-lib/images/list-icon-3.png" alt=""> We always return e-mails and calls within one business day. </p>
              </li>
              <li>
                <p><img src="<?php echo base_url()?>application/libraries/front-lib/images/list-icon-4.png" alt=""> Afordable pricing offers. </p>
              </li>
            </ul>
            
            <!-- Buttons --> 
            <a href="#." class="btn margin-top-20">Learn More</a> <a href="#." class="btn btn-orange margin-left-30 margin-top-20">Get a quote</a> </div>
        </div>
      </div>
    </section>
    
    <!-- Our Clients  -->
   <?php /*<section class="clients padding-bottom-100 padding-top-100">
      <div class="container"> 
        
        <!-- Tittle -->
        <div class="heading-block white-text text-center margin-bottom-80">
          <h2>Our Clients </h2>
          <span class="intro-style">Do you want to be part of this client list? </span> </div>
        
        <!-- Clients Images -->
        <ul class="col-5 text-center">
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-1.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-2.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-3.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-4.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-5.png" alt=""></li>
        </ul>
      </div>
    </section>
    */?>
    <!-- Team  -->
   
  </div>
  <!-- End Content --> 
  
  <!-- Footer -->
 
</div>
<!-- End Page Wrapper --> 

<!-- JavaScripts --> 
</body>

<!-- Mirrored from premiumlayers.com/demos/infinity/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 06 Mar 2016 23:42:21 GMT -->
</html>