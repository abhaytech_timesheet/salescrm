  <!-- End Header --> 
  
  <!--======= SUB BANNER =========-->
  <section class="sub-banner about">
    <div class="container">
      <div class="position-center-center">
        <h2>Digital transformation</h2>
        <span style="color:#fff;"><q style="font-size:20px;">TechAarjavam where simplicity is the ultimate sophistication....................</q></span>
	</div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    
    <!-- Intro -->
    
    
    <!-- Google Front Page -->
    <section class="front-page padding-top-100 padding-bottom-100">
      <div class="container">
        <div class="row">
          <div class="col-md-4"> <img class="img-responsive margin-top-30" src="<?php echo base_url()?>application/libraries/front-lib/images/digital-trans.png" alt="digital-trans"> </div>
          <div class="col-md-8"> 
            <!-- Tittle -->
            <div class="heading-block text-left margin-bottom-20">
              <h2>Digital transformation</h2>
              <h4>Meet your digital challenges head-on</h4>
              
              <p align="justify">Digital transformation is the profound and accelerating transformation of business activities, processes, competencies and models to fully leverage the changes and opportunities of digital technologies and their impact across society in a strategic and prioritized way.</p>
           <p align="justify">Software designed to integrate all aspects of a firm's operations and processes such as accounting, finance, human resources, inventory control, manufacturing, marketing, sales, and distribution, and resource planning. Advanced enterprise applications provide linkages with customers, business partners, and suppliers.

</p>
          
          
            </div>
            
            <!-- List Style -->
            
            
           
              </div>
        </div>
      </div>
    </section>
    
    <!-- Our Clients  -->
   <?php /*<section class="clients padding-bottom-100 padding-top-100">
      <div class="container"> 
        
        <!-- Tittle -->
        <div class="heading-block white-text text-center margin-bottom-80">
          <h2>Our Clients </h2>
          <span class="intro-style">Do you want to be part of this client list? </span> </div>
        
        <!-- Clients Images -->
        <ul class="col-5 text-center">
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-1.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-2.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-3.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-4.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-5.png" alt=""></li>
        </ul>
      </div>
    </section>
    */?>
    <!-- Team  -->
   
  </div>
  <!-- End Content --> 
  
  <!-- Footer -->
 
</div>
<!-- End Page Wrapper --> 

<!-- JavaScripts --> 
</body>


</html>
