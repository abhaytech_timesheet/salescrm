<!--======= SUB BANNER =========-->
<section class="sub-banner contact">
    <div class="container">
		<div class="position-center-center">
			<h2>CONTACT</h2>
			<span style="color:#fff;"><q style="font-size:20px;">TechAarjavam where simplicity is the ultimate sophistication....................</q></span>
		</div>
	</div>
</section>

<!-- Content -->
<div id="content"> 
    
    <!-- Contact  -->
    
    <!-- Contact Info -->
    <section class="contact-info padding-top-80 padding-bottom-80">
		<div class="container">
			
<div class="row">
				<div class="col-md-4">
					<h3 class="font-alegreya margin-top-30"> Corporate Office</h3>
				</div>
				<div class="col-md-8">
					<ul class="row">
						<li class="col-sm-4"> <i class="fa fa-map-marker"></i>
							<p>B-116/5 Indira Nagar Near Shekhar Hospital</p>
							<p>Lucknow,UttarPradesh, India.</p>
						</li>
						<li class="col-sm-4"> <i class="fa fa-envelope"></i>
							
							<p>contact@techaarjavam.com</p>
							<p>techaarjavam@gmail.com</p>
						</li>
						<li class="col-sm-4"> <i class="fa fa-phone"></i>
							
							
							<p>+91 7080552897</p>
							
						</li>
					</ul>
				</div>
				
			</div>
<hr>
			<hr>

			<div class="row">
				<div class="col-md-4">
					<h3 class="font-alegreya margin-top-30">Sales Office</h3>
				</div>
				<div class="col-md-8">
					<ul class="row">
						<li class="col-sm-4"> <i class="fa fa-map-marker"></i>
							
							
							<p>06-132, Bangkit Road </p>
							<p>Singapore-670276</p>
						</li>
						<li class="col-sm-4"> <i class="fa fa-envelope"></i>
							
							<p>singapore@techaarjavam.com</p>
							<p>techaarjavam@gmail.com</p>
						</li>
						<li class="col-sm-4"> <i class="fa fa-phone"></i>
							
							
							<p>+65 93535284</p>
							
						</li>
					</ul>
				</div>
				
			</div>
			<hr>
			<div class="row">
				<div class="col-md-4">
					<h3 class="font-alegreya margin-top-30">Sales Office</h3>
				</div>
				<div class="col-md-8">
					<ul class="row">
						<li class="col-sm-4"> <i class="fa fa-map-marker"></i>
							
							
							<p>2612 Genesee Street Utica &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; New York-13502</p>
						</li>
						<li class="col-sm-4"> <i class="fa fa-envelope"></i>
							
							<p>contact@techaarjavam.com</p>
							<p>techaarjavam@gmail.com</p>
						</li>
						<li class="col-sm-4"> <i class="fa fa-phone"></i>
							
							<p>+1 315 796 3343</p>
							
						</li>
					</ul>
				</div>
				
			</div>
			
		</div>
	</section>
    <section class="contact-us light-gray-bg">
		<div class="container-fluid">
			<div class="row">
				
				<!-- MAP -->
				<div class="col-md-4">
					<div id="map"></div>
				</div>
				
				<!-- Contact From -->
				<div class="col-md-8">
					<h3 class="font-alegreya margin-top-50">Get In Touch With Us</h3>
					<div class="contact-form"> 
						
						<!-- FORM -->
						<form role="form" id="contact_form" class="contact-form" method="post">
							<ul class="row">
								<li class="col-sm-6">
									<label>*NAME
										<input type="text" class="form-control input-sm empty" name="txtname" id="txtname" placeholder="">
										<span style="color: #e61c26;" id="divtxtname">&nbsp;</span>
									</label>
								</li>
								<li class="col-sm-6">
									<label>*EMAIL
										<input type="text" class="form-control input-sm empty" name="txtemail" id="txtemail" placeholder="">
										<span style="color: #e61c26;" id="divtxtemail">&nbsp;</span>
									</label>
								</li>
								<li class="col-sm-6">
									<label>PHONE
										<input type="text" class="form-control input-sm empty" name="txtmobile" id="txtmobile" placeholder="">
										<span style="color: #e61c26;" id="divtxtmobile">&nbsp;</span>
									</label>
								</li>
								<li class="col-sm-6">
									<label>SUBJECT
										<input type="text" class="form-control input-sm empty" name="txtsubject" id="txtsubject" placeholder="">
										<span style="color: #e61c26;" id="divtxtsubject">&nbsp;</span>
									</label>
								</li>
								<li class="col-sm-12">
									<label>*MESSAGE
										<textarea class="form-control input-sm empty" name="txtmessage" id="txtmessage" rows="5" placeholder=""></textarea>
										<span style="color: #e61c26;" id="divtxtmessage">&nbsp;</span>
									</label>
								</li>
								<li class="col-sm-12 no-margin">
									<button type="button" value="submit" class="btn" id="btn_submit" onClick="submitt('contact_form');">SEND NOW</button>
								</li>
							</ul>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<!-- End Content --> 

<!-- Footer -->

</div>
<!-- End Page Wrapper --> 

<!-- JavaScripts --> 

<!-- begin map script--> 
<script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false'></script> 
<script type="text/javascript">
	/*==========  Map  ==========*/
	var map;
	function initialize_map() {
		if ($('#map').length) {
			var myLatLng = new google.maps.LatLng(-37.814199, 144.961560);
			var mapOptions = {
				zoom: 17,
				center: myLatLng,
				scrollwheel: false,
				panControl: false,
				zoomControl: true,
				scaleControl: false,
				mapTypeControl: false,
				streetViewControl: false
			};
			map = new google.maps.Map(document.getElementById('map'), mapOptions);
			var marker = new google.maps.Marker({
				position: myLatLng,
				map: map,
				tittle: 'Envato',
				icon: '<?php echo base_url()?>application/libraries/images/map-locator.png'
				
			});
			} else {
			return false;
		}
	}
	google.maps.event.addDomListener(window, 'load', initialize_map);
</script>

<script>
	function submitt(id)
	{
		if(check(id))
		{
			var datas=$("#"+id).serialize();
			$.ajax({
				type:"POST",
				url:"<?php echo base_url(); ?>index.php/welcome/get_freelead",
				data:datas,
				success:function(msg) 
				{   
					alert(msg.trim());
					location.reload();
				}
			});
		}
	}
	</script>				