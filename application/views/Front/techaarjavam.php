  <!-- End Header --> 
  
  <!--======= SUB BANNER =========-->
  <section class="sub-banner about">
    <div class="container">
      <div class="position-center-center">
        <h2>TechAarjavam</h2>
        <span style="color:#fff;"><q style="font-size:20px;">TechAarjavam where simplicity is the ultimate sophistication....................</q></span>
	</div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    
    <!-- Intro -->
    
    
    <!-- Google Front Page -->
    <section class="front-page padding-top-100 padding-bottom-100" style="background:url(http://techaarjavam.com/application/libraries/front-lib/images/tech-bg.png); background-size:cover;">
      <div class="container">
        <div class="row">
		<div class="col-md-12">
			

			<div class="tabbable-panel">
				<div class="tabbable-line">
					<ul class="nav nav-tabs ">
						<li class="active">
							<a href="#tab_default_1" data-toggle="tab">
							Web Development Strategy</a>
						</li>
						<li>
							<a href="#tab_default_2" data-toggle="tab">
						Digital Marketing Strategy </a>
						</li>
						<li>
							<a href="#tab_default_3" data-toggle="tab">
							Enterprise App Strategy</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_default_1">
							  <h4></h4>
            <p align="justify"><strong style="    color: #e61c26;">Researching Business Needs: </strong>It all starts with setting a goal and forwarding it to the team members. Discussing the business ideas with the clients and coming up with an appropriate idea which is in sync with the client. </p>
            <p align="justify"><strong style="    color: #e61c26;">Researching User Needs:</strong> Building a website to promote the business and be at the center of search results. This requires understanding the users and their needs.This can be done by some brainstorming and that's why we do it as a group  to share ideas, generate possibilities, and sharpen focus. Ultimately, the goal is to clarify features that: users want, clients desire, and developers can make happen. </p>
            <p align="justify"><strong style="    color: #e61c26;">Analyzing The Competition:</strong> The third step involves checking out the competition. We set up accounts on competitor sites, critique their design on every level, evaluate their feature sets, learn what they're doing right, and steer clear of what they're doing wrong. </p>
            <p align="justify"><strong style="    color: #e61c26;">Putting It All Together:</strong> And of course, a website development strategy is only good as the team that is working on it and the outcome it leads to, and ours is as good as it gets: the strategy enables us to build brilliant, functional and professional websites for our valued clients that completely meets business goals, satisfies end-user tasks and desires, and blows away the competition. </p>
 		</div>
						<div class="tab-pane" id="tab_default_2">
                        	  <h4></h4>
							<p align="justify">Digital marketing is essential in today's business environment. To showcase company's proficiency. With both competitors and potential customers more accessible online, digital marketing is the only way to stay ahead.</p>
            <p align="justify">Here's how the fundamentals of digital marketing strategies are laid:</p>
            <p align="justify"><strong style="    color: #e61c26;">Make a plan:</strong> Having a solid plan in mind increases the chances of success. Digital marketing is a great way for a business to prosper and expand its operations. A lot of strategy and precision goes into digital marketing and having a goal helps you know what to focus on.</p>
            <p align="justify"><strong  style="    color: #e61c26;">Create a Marketing Funnel: </strong>A marketing funnel is when you map out a customer's journey from when a customer is a stranger to when they become a lead. Then make some strategies and convert these leads into projects. Things like lead magnets, calls to action, opt-ins and offers are all effective pieces of a funnel. You can think of a marketing funnel in four parts: Awareness, Interest, Desire, and Action.</p>
            <p align="justify"><strong  style="    color: #e61c26;">Call-to-action:</strong> A call-to-action (CTA) is an image or text that prompts visitors to go to your page CTAs should direct people to go to your page, where you can collect information in exchange for a marketing offer.</p>
            <p align="justify"><strong  style="    color: #e61c26;">Driving Traffic: </strong>To attract more people to your page, there has to be traffic on your website. There are a number of ways in which this can be done.</p>
            <p align="justify"><em><strong  style="    color: #e61c26;">Here are a few that we do:</strong></em></p>
            <p align="justify">Content writing: Use content such as blog posts, press releases and articles on authority websites. Insert links to various places on your website within this content to build your brand name through exposure and drive traffic to your website.</p>
            <p align="justify"><strong  style="    color: #e61c26;">Generating Keywords:</strong> Inserting related keywords into content will help your content and website show up in more search results, this leads to higher volumes of web traffic.</p>
            <p align="justify"><strong  style="    color: #e61c26;">Website Optimization: </strong>Ensuring that your website is optimized and functioning at it's best is essential. People don't want to visit a website that doesn't work properly.</p>
            <p align="justify"><strong  style="    color: #e61c26;">Social Media: </strong>Use engaging social media posts to attract more traffic to your site. Using pictures, video, and other relevant media will help your posts get more engagement.
              
              If you want your business to grow, digital marketing is the place to start. </p>
						</div>
						<div class="tab-pane" id="tab_default_3">
                        	  <h4></h4>
							  <p align="justify">We see mobile apps streamlining business critical operations and giving them more opportunities to reach their customers and increase employee productivity. Clearly understanding what the app needs to do, the processes they will support, the target audience, the technology/platforms for execution and the ongoing management, will make all the difference in successful implementation.</p>
            <p align="justify"><strong  style="    color: #e61c26;"><em>Steps involved in this process are:</em></strong></p>
            <p align="justify"><strong  style="    color: #e61c26;">Understanding your business needs:</strong> An effective app strategy involves knowing the reason behind building an app. You need to look into your goals and identify why you need an app or how will an app help your business. Once you have the answers to all the questions then itís time to start thinking what would the customers prefer.</p>
            <p align="justify"><strong  style="    color: #e61c26;">Defining your mobile app users:</strong> Users want to see an app that fulfill all their needs and demands. The more experience and exposure a user has, the more complex apps they want to see. The company that supports their demands, those are the companies they support.</p>
            <p align="justify"><strong  style="    color: #e61c26;">Determining user access and trends:</strong> Understanding why your enterprise needs mobile apps, and what business processes they will support, is key to a successful mobile app strategy. A big piece of that puzzle is mapping those processes to the needs of the end users and the overall audience you will be addressing early in the planning stages. This approach will help create the solid enterprise mobile app foundation.</p>
            <p align="justify"><strong  style="    color: #e61c26;">Technology selection:</strong> Just as there is no one single mobile device platform and no one mobile device type, companies may also want to consider adopting an enterprise mobile app strategy, and development tool, that offers native and HTML5 options. Native apps give the ultimately satisfying end user experience, being able to take full advantage of native device features and functionality, and should be part of any organizationís mobile app strategy. That may mean going wholly native, which is the ideal scenario, or possibly even hybrid if the organization feels strongly in that approach.</p>
            <p align="justify"><strong  style="    color: #e61c26;">Management and Analytics:</strong> App analytics relates to a number of users- new, active already register, how often they use the app to track the immediate term. The customers can also provide feedback on the app itself if you have added a discussion forum. This will help you understand how your app is performing and this will help you improve your app. </p>
		</div>
					</div>
				</div>
			</div>

			
			
		</div>
	</div>
      </div>
    </section>
    
    <!-- Our Clients  -->
   <?php /*<section class="clients padding-bottom-100 padding-top-100">
      <div class="container"> 
        
        <!-- Tittle -->
        <div class="heading-block white-text text-center margin-bottom-80">
          <h2>Our Clients </h2>
          <span class="intro-style">Do you want to be part of this client list? </span> </div>
        
        <!-- Clients Images -->
        <ul class="col-5 text-center">
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-1.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-2.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-3.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-4.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-5.png" alt=""></li>
        </ul>
      </div>
    </section>
    */?>
    <!-- Team  -->
   
  </div>
  <!-- End Content --> 
  
  <!-- Footer -->
 
</div>
<!-- End Page Wrapper --> 

<!-- JavaScripts --> 
</body>


</html>