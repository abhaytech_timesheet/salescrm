  <!-- End Header --> 
  
  <!--======= SUB BANNER =========-->
  <section class="sub-banner about">
    <div class="container">
      <div class="position-center-center">
        <h2>BUSINESS SERVICES GROUP</h2>
        <span style="color:#fff;"><q style="font-size:20px;">TechAarjavam where simplicity is the ultimate sophistication....................</q></span>
	</div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    
    <!-- Intro -->
    
    
    <!-- Google Front Page -->
    <section class="front-page padding-top-100 padding-bottom-100">
      <div class="container">
        <div class="row">
          <div class="col-md-4"> <img class="img-responsive margin-top-30" src="<?php echo base_url()?>application/libraries/front-lib/images/businessservices.png" alt="businessservices"> </div>
          <div class="col-md-8"> 
            <!-- Tittle -->
            <div class="heading-block text-left margin-bottom-20">
              <h2>BUSINESS SERVICES GROUP</h2>
              <h4>Creating Memorable Digital Experiences Everyday</h4>
              
              <p align="justify">
Techaarjavam Business Services Group delivers innovative and cost-effective solutions that help transform our customer’s business.We leverage our domain expertise and platform-driven process transformations to offer end to end solutions to our customers.</p>
           <p align="justify">Entering a new era of transformations, we refocus energies towards ‘Creating memorable digital experiences everyday’ for our customers.

</p>
          
          
            </div>
            
            <!-- List Style -->
            
            
           
              </div>
        </div>
      </div>
    </section>
    
    <!-- Our Clients  -->
   <?php /*<section class="clients padding-bottom-100 padding-top-100">
      <div class="container"> 
        
        <!-- Tittle -->
        <div class="heading-block white-text text-center margin-bottom-80">
          <h2>Our Clients </h2>
          <span class="intro-style">Do you want to be part of this client list? </span> </div>
        
        <!-- Clients Images -->
        <ul class="col-5 text-center">
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-1.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-2.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-3.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-4.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-5.png" alt=""></li>
        </ul>
      </div>
    </section>
    */?>
    <!-- Team  -->
   
  </div>
  <!-- End Content --> 
  
  <!-- Footer -->
 
</div>
<!-- End Page Wrapper --> 

<!-- JavaScripts --> 
</body>


</html>
