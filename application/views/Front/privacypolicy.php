<section class="sub-banner about">
    <div class="container">
      <div class="position-center-center">
        <h2>Privacy Statement</h2>
        <span style="color:#fff;"><q style="font-size:20px;">TechAarjavam where simplicity is the ultimate sophistication....................</q></span>
	</div>
    </div>
</section>
  
  <!-- Content -->
  <div id="content"> 
    <section class="front-page padding-top-100 padding-bottom-100">
      <div class="container">
        <div class="row">
           <div class="col-md-12"> 
		   <!-- Tittle -->
            <div class="heading-block text-left margin-bottom-20">
              <h2>Privacy Statement</h2>
          <h6>Privacy Statement for techaarjavam.com.</h6>
<p align="justify"> Tech Aarjavam. has created this privacy statement in order to demonstrate our company commitment to privacy. The following discloses our information gathering and dissemination practices for this website: techaarjavam.com.</p>

<h6>Collection of Personal Information from Service Visitors.</h6>

<p align="justify">Tech Aarjavam. may collect and/or track (1) the home server domain names, email addresses, type of client computer, files downloaded, search engine used, operating system, and type of web browser of visitors to Tech Aarjavam. web service, (2) the email addresses of visitors that communicate with Tech Aarjavam. via email, (3) information knowingly provided by the visitor in online forms, registration forms, surveys, email, contest entries, and other online avenues (including demographic and personal profile data), and (4) aggregate and user-specific information on which pages visitors access.</p>

<p align="justify">Tech Aarjavam. may place Internet “cookies” on visitors’ hard drives. Internet cookies save data about individual visitors, such as the organization’s name, password, user-name, screen preferences, and the pages of a service viewed by the visitor. When the visitor revisitsTech Aarjavam. web service, Tech Aarjavam. may recognize the visitor by the Internet cookie and customize the visitor’s experience accordingly. Visitors may decline Internet cookies, if any, by using the appropriate feature of their web client software, if available.</p>

<h6>Use of Personal Data Collected.</h6>

<p align="justify">Personal data collected by Tech Aarjavam. may be used by Tech Aarjavam. for editorial and feedback purposes, for marketing and promotional purposes, for a statistical analysis of users’ behavior, for product development, for content improvement, or to customize the content and layout of Tech Aarjavam. service. Aggregate data on visitors’ home servers may be used for internal purposes but will not be provided to third parties such as marketing firms. Individually identifying information, such as names, postal and email addresses, phone numbers, and other personal information which visitors voluntarily provide to Tech Aarjavam. may be added to Tech Aarjavam. databases and used for future calls and mailings regarding service updates, new products and services, and upcoming events.</p>

<p align="justify">Our company use client’s information from 30-day Trial Registration form. We keep customers contact information strictly confidential and use it only to send important information to our customers.</p>

<h6>Security Measures.</h6>

<p align="justify">Tech Aarjavam. has implemented numerous security features to prevent the unauthorized release of or access to personal information. For example, all Tech Aarjavam. employees are required to certify their understanding that personal information is considered confidential, that it is important to safeguard personal information, and that Tech Aarjavam. will take appropriate action against any employee who fails to acknowledge these facts or adhere to the requisite standards of conduct. Please be advised, that Tech Aarjavam. is not responsible for the security of information transmitted via the Internet (electronic mail services) . For more private communication contact Tech Aarjavam. by telephone at +91 9919111099.</p>

<h6>Tech Aarjavam. Right to Contact Users.</h6>

<p align="justify">Tech Aarjavam. reserves the right to contact service visitors regarding sales and other informational requests made through its web service.</p>

<h6>Tech Aarjavam. Right to Change Privacy Policy.</h6>

<p align="justify">Tech Aarjavam. reserves the right to change this Privacy Policy at any time without notice. Any change to this Privacy Policy shall be effective as to any visitor that has accepted the techaarjavam.com Service Terms and Conditions before the change was made.</p>

<h6>Contact Information</h6>

<p align="justify">Tech Aarjavam. welcomes your comments regarding this Statement of Privacy, please contact us by e-mail, or postal mail.</p>

<p align="justify">Tech Aarjavam.</p>
<p align="justify">D-1205/30</p>
<p align="justify">Indira Nagar,226016 Lucknow.</p>
<p align="justify">Uttar Pradesh INDIA.</p>
</div>
</div>
</div>
</div>
</section>
</div>
  <!-- End Content --> 
   



