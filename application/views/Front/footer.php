<footer>
    <div class="container" >
		<div class="row">
			<div class="col-md-6 padding-top-50"> 
				
				<!-- News Letter -->
				<div class="news-letter">
					<h6>News Letter</h6>
					<form>
						<input type="email" placeholder="Enter your email..." >
						<button type="submit" ><i class="fa fa-envelope-o"></i></button>
					</form>
				</div>
			</div>
			
			<!-- Folow Us -->
			<div class="col-md-6 padding-top-50">
				<div class="news-letter">
					<h6>Follow us</h6>
					<ul class="social_icons pull-left margin-left-50 margin-top-10">
						<li><a href="https://www.facebook.com/TechAarjavam"><i class="fa fa-facebook"></i></a></li>
						<li><a href="https://twitter.com/techaarjavam"><i class="fa fa-twitter"></i></a></li>
						<li><a href="https://plus.google.com/112178789991194759306"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="https://www.linkedin.com/company/tech-aarjavam"><i class="fa fa-linkedin"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
    
    <!-- Footer Info -->
    <div class="footer-info">
		<div class="container" >
			<div class="row"> 
				
				<!-- About -->
				<div class="col-md-4" itemscope itemtype="http://schema.org/Organization"> <img class="margin-bottom-30" src="<?php echo base_url()?>application/libraries/front-lib/images/logo.png" alt="Tech Aarjavam Official Logo." TITLE="Tech Aarjavam Mobile App Development,Web Development.">
					<p><span itemprop="name">Tech Aarjavam</span>  provides enterprise software to manage customer relations and business operations.</p>
					<ul class="personal-info" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
						<!--<li><i class="fa fa-map-marker"></i><b>Corp Office:</b> D1205/30 Indira Nagar,
						Lucknow (U.P)</li>-->
						
						<li itemprop="telephone"><a href="mailto:support@techaarjavam.com"><i class="fa fa-envelope"></i></a>contact@techaarjavam.com</li>
						<li><i class="fa fa-phone"></i>+91-9919111099</li>
                       <li><i class="fa fa-skype" aria-hidden="true"></i>vivek_jaiswal7</li>
					</ul>
				</div>
				
				<!-- Service provided -->
				<div class="col-md-4">
					<h6 style="color:#E61C26;">Service provided</h6>
					<ul class="links">
						<li><a href="<?php echo base_url()?>mobile-application" title="Android ,Iphone Mobile Apps Development in Mumbai,Lucknow,Singapore,NewYork.">Mobile Apps</a></li>
						<li><a href="<?php echo base_url()?>" title="REST,SOAP Web Services integration and development in Mumbai,Lucknow,Singapore,NewYork. ">Web Services</a></li>
						<li><a href="<?php echo base_url()?>"  title="Logo Design in Mumbai,Lucknow,Singapore,NewYork">Logo Design.</a></li>
						<li><a href="<?php echo base_url()?>" title="Graphics Design in Mumbai,Lucknow,Singapore,NewYork">Graphic Design.</a></li>
						<li><a href="<?php echo base_url()?>"title="Website Design in Mumbai,Lucknow,Singapore,NewYork" >Website Design.</a></li>
						<li><a href="<?php echo base_url()?>" title="Content Management System Development,Wordpress, Joomla Development in Mumbai,Lucknow,Singapore,NewYork">Content Management.</a></li>
						<li><a href="<?php echo base_url()?>" title="E-commerce Solutions like PayTM,Flipkart,Snapdeal,Chumbak in Mumbai,Lucknow,Singapore,NewYork">E-commerce Solutions.</a></li>
						<li><a href="<?php echo base_url()?>" title="Classified Solutions like OLX,Naukri,99Acre,MetroHeights in Mumbai,Lucknow,Singapore,NewYork">Classified Solutions.</a></li>
						<li><a href="<?php echo base_url()?>" title="SEO Services in Mumbai,Lucknow,Singapore,NewYork">SEO Services.</a></li>
						<li><a href="<?php echo base_url()?>" title="Social Media (SMO) on Facebook,Twitter,Pinterest,Linkedin in Mumbai,Lucknow,Singapore,NewYork">Social Media (SMO).</a></li>
						<li><a href="<?php echo base_url()?>" title="Pay per Click Sevice by Google,Infolynks in Mumbai,Lucknow,Singapore,NewYork">Pay per Click.</a></li>
						<li><a href="<?php echo base_url()?>analytics."title="Web Analytics Report by Google,MSN,Yahoo  in Mumbai,Lucknow,Singapore,NewYork">Web Analytics.</a></li>
						<li><a href="<?php echo base_url()?>" title="Digital Marketing in Mumbai,Lucknow,Singapore,NewYork">Digital Marketing.</a></li>
						<li><a href="<?php echo base_url()?>" title="Email Marketing in Mumbai,Lucknow,Singapore,NewYork">Email Marketing.</a></li>
					</ul>
				</div>
				
				<!-- Quote -->
				<div class="col-md-4">
					<h6 style="color:#E61C26;">Get Free Quote</h6>
					<div class="quote">
						<form action="javascript:get_freequote()" method="post" id="formquote">
							<input class="form-control input-sm empty" type="text" id="txtname" name="txtname" placeholder="Name">
							<span style="color: #e61c26; padding-bottom: 15px; margin-top: -17px; float: right;" id="divtxtname"></span>
							<input class="form-control input-sm empty" type="text" id="txtmobile" name="txtmobile" maxlength="10" placeholder="Phone No">
							<div style="color: #e61c26; padding-bottom: 15px; margin-top: -17px; float: right;"  id="divtxtmobile"></div>
							<textarea class="form-control" id="txtmsgquery" name="txtmsgquery" placeholder="Messages"></textarea>
							<button type="submit" class="btn btn-orange">SEND NOW</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="rights">
			<div class="container" >
				<p style="color:#FFFFFF;">Copyright © 2016 <a href="<?php echo base_url()?>" title="Tech Aarjavam Mobile App Development and Web Development in Mumbai,Lucknow,Singapore,NewYork."><b style="color:#FFFFFF;">Tech Aarjavam</b></a>. All Rights Reserved. &nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url()?>term-of-use" title="Tech Aarjavam Terms of use."><b style="color:#FFFFFF;">Terms of Use</b></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url()?>privacy-policy" title="Tech Aarjavam Privacy Policy."><b style="color:#FFFFFF;">Privacy Policy</b></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url()?>" title="Tech Aarjavam Site Map."><b style="color:#FFFFFF;">Site Map</b></a></p>
			</div>
		</div>
	</div>
    
    <!-- Rights -->
    
</footer>
<script src="<?php echo base_url()?>application/libraries/front-lib/js/vendors/jquery/jquery.min.js"></script> 
<script src="<?php echo base_url()?>application/libraries/front-lib/js/vendors/wow.min.js"></script> 
<script src="<?php echo base_url()?>application/libraries/front-lib/js/vendors/bootstrap.min.js"></script> 
<script src="<?php echo base_url()?>application/libraries/front-lib/js/vendors/own-menu.js"></script> 
<script src="<?php echo base_url()?>application/libraries/front-lib/js/vendors/flexslider/jquery.flexslider-min.js"></script> 
<script src="<?php echo base_url()?>application/libraries/front-lib/js/vendors/jquery.countTo.js"></script> 
<script src="<?php echo base_url()?>application/libraries/front-lib/js/vendors/jquery.isotope.min.js"></script> 
<script src="<?php echo base_url()?>application/libraries/front-lib/js/vendors/jquery.bxslider.min.js"></script> 
<script src="<?php echo base_url()?>application/libraries/front-lib/js/vendors/owl.carousel.min.js"></script> 
<script src="<?php echo base_url()?>application/libraries/front-lib/js/vendors/jquery.sticky.js"></script> 

<!-- SLIDER REVOLUTION 4.x SCRIPTS  --> 
<script type="text/javascript" src="<?php echo base_url()?>application/libraries/front-lib/rs-plugin/js/jquery.themepunch.tools.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url()?>application/libraries/front-lib/rs-plugin/js/jquery.themepunch.revolution.min.js"></script> 
<script src="<?php echo base_url()?>application/libraries/front-lib/js/main.js"></script> 
<script src="<?php echo base_url()?>application/libraries/front-lib/js/vendors/modernizr.js"></script>
</div>
<!-- End Page Wrapper --> 

</body>
<script src="<?php echo base_url(); ?>/application/libraries/js/check.js"></script>

<script>
	function get_freequote()
	{
		if(check('formquote'))
		{
			var data=$("#formquote").serialize();
			$.ajax({
				type:"POST",
				url:"<?php echo base_url(); ?>index.php/welcome/get_freequote",
				data:data,
				success:function(msg) 
				{   
                  alert(msg.trim());
                  location.reload();
				}
				
			});
		}
	}
</script>

</html>