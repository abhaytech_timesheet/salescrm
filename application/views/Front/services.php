  
  <!--======= SUB BANNER =========-->
  <section class="sub-banner services">
    <div class="container">
      <div class="position-center-center">
        <h2>Services</h2>
    <span style="color:#fff;"><q style="font-size:20px;">TechAarjavam has one of the largest practices focusing on Customer Experience......................</q></span>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    
    <!-- What We do -->
    <section class="intro padding-top-20 padding-bottom-100">
      <div class="container"> 
        <!-- Tittle -->
    
        <!-- What We do -->
        <div class="row">
          <div class="col-md-6"> 
            <!-- Tittle -->
            <div class="heading-block text-left margin-bottom-20">
              <h2>We Improve your working process performance</h2>
              <p>TechAarjavam has one of the largest practices focusing on Customer Experience brining in expertise in customer services across sales, service, marketing and commerce.</p>
            </div>
            
            <!-- List Style -->
            <ul class="row list-what-we">
              <li class="col-sm-6">
                <h6>Development</h6>
                <hr>
                <p>On adding Application Development , Mobile solution development for enterprises.</p>
              </li>
              <li class="col-sm-6">
                <h6>Deployment</h6>
                <hr>
                <p>Deployment support for individual set-up as well as, across all leading app stores on different platforms.</p>
              </li>
              <li class="col-sm-6">
                <h6>Consultating</h6>
                <hr>
                <p>Provides Full consultation for enterprises to set-up goals in mobility domain.</p>
              </li>
              <li class="col-sm-6">
                <h6>Security Management</h6>
                <hr>
                <p>Provides Content security management to make sure that organizational data is accessible to only authorized users.</p>
              </li>
			  <li class="col-sm-6">
                <h6>Promotion and Branding</h6>
                <hr>
                <p>We will improve the online visibility of your brand and drive more relevant traffic to your website .</p>
              </li>
			  <li class="col-sm-6">
                <h6>ERP Simplification</h6>
                <hr>
                <p>Regarding ERP simplification TechAarjavam  is uniquely positioned to enable our customers to simplify their ERPs to streamline business processes .</p>
              </li>
			   <li class="col-sm-6">
                <h6>Process Custmization.</h6> 
                <hr>
                <p>So we are  uniquely placed to assemble customized, end-to-end digital solutions enabling clients to undertake business transformation journeys.</p>
              </li>
            </ul>
          </div>
          
          <!-- Image -->
          <div class="col-md-6"> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/process.jpg" alt=" "> </div>
        </div>
      </div>
    </section>
    
    <!-- We Offer -->
    <section class="offer-services padding-top-100">
      <div class="container"> 
        
        <!-- Tittle -->
        <div class="heading-block text-center margin-bottom-80">
          <h2>What Services We Offer </h2>
          <span class="intro-style">Do you want to improve the online visibility of your brand and 
          drive more relevant traffic to your website?</span> </div>
        <div class="text-center"> <img src="<?php echo base_url()?>application/libraries/front-lib/images/services-img.jpg" alt=""> </div>
      </div>
    </section>
    
    <!-- INTRO -->
    <section class="bg-parallax text-center padding-top-60 padding-bottom-60" style="background:url(<?php echo base_url()?>application/libraries/front-lib/images/bg/bg-parallax.jpg) no-repeat;">
      <div class="container">
        <div class="text-center margin-bottom-50">
          <p class="text-white intro-style font-14px">We are a full service SEO agency. Our social media experts can help you establish your business objectives, identify your target audience, create engaging and share-worthy content and finally integrate your social media with all other aspects of your online presence.</p>
        </div>
        <a href="#." class="btn btn-orange">Learn More</a> </div>
    </section>
  
    <!-- Price table -->
    <section class="pricing-table light-gray-bg padding-top-100 padding-bottom-100">
      <div class="container"> 
        
        <!-- Tittle -->
        <div class="heading-block text-center margin-bottom-80">
          <h2>Affordable SEO Services Packages </h2>
          <span class="intro-style">Choose from affordable SEO services packages &amp; get the best results in return. </span> </div>
        <div class="row"> 
          
          <!-- Price -->
          <div class="col-md-4"> 
            <!-- Icon -->
            <div class="plan-icon"><img src="<?php echo base_url()?>application/libraries/front-lib/images/plan-icon-1.png" alt=" "></div>
            
            <!-- Plan  -->
            <div class="pricing-head">
              <h4>Basic Plan</h4>
              <span class="curency">$</span> <span class="amount">25<span>.99</span></span> <span class="month">/ month</span> </div>
            
            <!-- Plean Detail -->
            <div class="price-in">
              <ul class="text-center">
                <li>25 Analytics Campaigns</li>
                <li> 1,900 Keywords</li>
                <li> 1,250,000 Crawled Pages</li>
                <li> Includes Branded Reports</li>
                <li> 50 Social Accounts</li>
              </ul>
              <a href="#." class="btn btn-orange">PURCHACE</a> </div>
          </div>
          
          <!-- Price -->
          <div class="col-md-4"> 
            <!-- Icon -->
            <div class="plan-icon orange-bg"><img src="<?php echo base_url()?>application/libraries/front-lib/images/plan-icon-2.png" alt=" "></div>
            
            <!-- Plan  -->
            <div class="pricing-head orange-bg">
              <h4>Advanced Plan</h4>
              <span class="curency">$</span> <span class="amount">45<span>.99</span></span> <span class="month">/ month</span> </div>
            
            <!-- Plean Detail -->
            <div class="price-in">
              <ul class="text-center">
                <li>25 Analytics Campaigns</li>
                <li> 1,900 Keywords</li>
                <li> 1,250,000 Crawled Pages</li>
                <li> Includes Branded Reports</li>
                <li> 50 Social Accounts</li>
              </ul>
              <a href="#." class="btn btn-orange">PURCHACE</a> </div>
          </div>
          
          <!-- Price -->
          <div class="col-md-4"> 
            <!-- Icon -->
            <div class="plan-icon"><img src="<?php echo base_url()?>application/libraries/front-lib/images/plan-icon-3.png" alt=" "></div>
            
            <!-- Plan  -->
            <div class="pricing-head">
              <h4>Premium Plan</h4>
              <span class="curency">$</span> <span class="amount">65<span>.99</span></span> <span class="month">/ month</span> </div>
            
            <!-- Plean Detail -->
            <div class="price-in">
              <ul class="text-center">
                <li>25 Analytics Campaigns</li>
                <li> 1,900 Keywords</li>
                <li> 1,250,000 Crawled Pages</li>
                <li> Includes Branded Reports</li>
                <li> 50 Social Accounts</li>
              </ul>
              <a href="#." class="btn btn-orange">PURCHACE</a> </div>
          </div>
        </div>
      </div>
    </section>
    
    <!-- TESTIMONIALS -->
    <section class="testimonial red-bg padding-top-100 padding-bottom-100">
      <div class="container"> 
        
        <!-- Tittle -->
        <div class="heading-block white-text text-center margin-bottom-80">
          <h2>Why Customer <i class="fa fa-heart white-text"></i> us! </h2>
          <span class="intro-style">Do you want to improve the online visibility of your brand and 
          drive more relevant traffic to your website? </span> </div>
        
        <!-- Testi Slider -->
        <div class="testi-slides-flex">
          <ul class="slides">
            
            <!-- Slide 1 -->
            <li>
              <div class="avatar"> <img src="<?php echo base_url()?>application/libraries/front-lib/images/avatar-2.png" alt="" >
                <h6>tim rijkes  / <span>CEO - Founder </span></h6>
                <p>“Here's the story of a lovely lady who was bringing up three very lovely girls. The first mate and his Skipper too will do their very best to make the others comfortable in their tropic island nest. I have always wanted to have a neighbor just like you. I've always wanted to live in a neighborhood with you.”</p>
              </div>
            </li>
            
            <!-- Slide 2 -->
            <li>
              <div class="avatar"> <img src="<?php echo base_url()?>application/libraries/front-lib/images/avatar-2.png" alt="" >
                <h6>tim rijkes  / <span>CEO - Founder </span></h6>
                <p>“Here's the story of a lovely lady who was bringing up three very lovely girls. The first mate and his Skipper too will do their very best to make the others comfortable in their tropic island nest. I have always wanted to have a neighbor just like you. I've always wanted to live in a neighborhood with you.”</p>
              </div>
            </li>
            
            <!-- Slide 3 -->
            <li>
              <div class="avatar"> <img src="<?php echo base_url()?>application/libraries/front-lib/images/avatar-2.png" alt="" >
                <h6>tim rijkes  / <span>CEO - Founder </span></h6>
                <p>“Here's the story of a lovely lady who was bringing up three very lovely girls. The first mate and his Skipper too will do their very best to make the others comfortable in their tropic island nest. I have always wanted to have a neighbor just like you. I've always wanted to live in a neighborhood with you.”</p>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </section>
    
    <!-- Clients -->
    <section class="clients white-bg padding-bottom-100 padding-top-100">
      <div class="container"> 
        
        <!-- Tittle -->
        <div class="heading-block text-center margin-bottom-80">
          <h2>Our Valued Clients </h2>
          <span class="intro-style">Do you want to improve the online visibility of your brand and 
          drive more relevant traffic to your website? </span> </div>
        
        <!-- Clients Images -->
        <ul class="row text-center">
          <li class="col-md-2"> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/clients-logo-1.jpg" alt=""></li>
          <li class="col-md-2"> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/clients-logo-2.jpg" alt=""></li>
          <li class="col-md-2"> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/clients-logo-3.jpg" alt=""></li>
          <li class="col-md-2"> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/clients-logo-4.jpg" alt=""></li>
          <li class="col-md-2"> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/clients-logo-5.jpg" alt=""></li>
          <li class="col-md-2"> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/clients-logo-6.jpg" alt=""></li>
        </ul>
      </div>
    </section>
  </div>
  <!-- End Content --> 
  
  <!-- Footer -->
 
</div>
<!-- End Page Wrapper --> 

<!-- JavaScripts --> 
</body>

<!-- Mirrored from premiumlayers.com/demos/infinity/services.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 06 Mar 2016 23:42:35 GMT -->
</html>