<body>
<!-- LOADER ===========================================-->
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5KLLP7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5KLLP7');</script>
<!-- End Google Tag Manager -->
<!-- Page Wrapper -->
<div id="wrap"> 
  <!-- Top bar -->
  <div class="container">
    <div class="row">
      <div class="col-md-2 noo-res"></div>
      <div class="col-md-10">
        <div class="top-bar">
          <div class="col-md-6">
            <ul class="social_icons">
              <li><a href="https://www.facebook.com/TechAarjavam" title="TECH AARJAVAM FACEBOOK" target="_blank"> <i class="fa fa-facebook"></i></a></li>
              <li><a href="https://twitter.com/techaarjavam" title="TECH AARJAVAM TWITTER" target="_blank"><i class="fa fa-twitter" ></i></a></li>
              <li><a href="https://plus.google.com/112178789991194759306" title="TECH AARJAVAM GOOGLE PLUS" target="_blank"><i class="fa fa-google" ></i></a></li>
              <li><a href="https://www.linkedin.com/company/tech-aarjavam" title="TECH AARJAVAM LINKEDIN " target="_blank"><i class="fa fa-linkedin"></i></a></li>
 
            </ul>
          </div>
          
          <!-- Social Icon -->
          <div class="col-md-6" style="float:right;">
            <ul class="some-info font-open-sans">
              <li><a href="<?php echo base_url();?>news.html" title="News"><i class="fa fa-newspaper-o" ></i>&nbsp;NEWS</a>&nbsp;&nbsp;</li>
              <li><a href="#" title="Career"><i class="fa fa-bookmark" ></i>&nbsp;CAREER</a>&nbsp;&nbsp;</li>
              <li><a href="<?php echo base_url()?>contact" title="Location"><i class="fa fa-location-arrow" ></i>&nbsp;LOCATION</a>&nbsp;&nbsp;</li>
              <li><a href="<?php echo base_url()?>helpdesk.html" title="Help Desk"><i class="fa fa-ticket" ></i>&nbsp;HELP DESK</a></li>
           </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Header -->
  <header class="header coporate-header">
    <div class="sticky">
      <div class="container">
        <div class="logo"> <a href="<?php echo base_url()?>welcome/index" title="TECH AARJAVAM"><img src="<?php echo base_url()?>application/libraries/front-lib/images/logo.png" alt="TECH AARJAVAM"></a> </div>
        
        <!-- Nav -->
        <nav>
          <ul id="ownmenu" class="ownmenu">
            <li class="active"><a href="<?php echo base_url()?>welcome/index" title="Home">Home</a></li>
            <li><a href="<?php echo base_url()?>who-we-are" title="Who We Are">Who We Are</a><ul class="dropdown">
			 <li><a href="<?php echo base_url()?>Aarjavam" title="Aarjavam">Aarjavam</a></li>
			 <li><a href="<?php echo base_url()?>techaarjavam" title="Tech Aarjavam">Tech Aarjavam</a></li>
			 <li><a href="<?php echo base_url()?>leadership" title="Leadership">Leadership</a></li>
			 </ul></li>
            <li><a href="<?php echo base_url()?>what-we-do" title="What We Do">What We Do </a>
			 <ul class="dropdown">
			 <li><a href="<?php echo base_url()?>solution-services" title="Solution & Services">Solution & Services</a>
			 <ul class="dropdown">
		     <li><a href="<?php echo base_url()?>mobile-application" title="MOBILITY SOLUTION(MOBILE APP DEVELOPMENT)">MOBILE APP DEVELOPMENT</a></li>
			 <li><a href="<?php echo base_url()?>analytics" title="ANALYTICS">ANALYTICS</a></li>
			 <li><a href="<?php echo base_url()?>erp" title="ERP SIMPLIFICATION">ERP SIMPLIFICATION</a></li>
			 <li><a href="<?php echo base_url()?>enterprise" title="ENTERPRISES APPLICATION">ENTERPRISES APPLICATION</a></li>
			 <li><a href="<?php echo base_url()?>digitalisation" title="ENABLING DIGITAL TRANSFORMATION">ENABLING DIGITAL TRANSFORMATION</a></li>
			 </ul>
			 </li>
			 <li><a href="<?php echo base_url()?>outsourcing" title="Outsourcing">Outsourcing</a>
			  <ul class="dropdown" >
			 <li><a href="<?php echo base_url()?>service-group" title="BUSINESS SERVICES GROUP">BUSINESS SERVICES GROUP</a></li>
			 </ul></li>
			 <li><a href="<?php echo base_url()?>platforms" title="Platforms">Platforms</a>
			  <ul class="dropdown">
			 <li><a href="<?php echo base_url()?>salesbooster" title="SALES BOOSTER(FOR SALES MONITORING TOOL WITH  LOTS OF OTHER TOOL)" >SALES BOOSTER</a></li>
			 <li><a href="<?php echo base_url()?>brandmaker" title="BRAANDMAKER  (FOR TALKTIME RETAIL MARKET)">BRAANDMAKER</a></li>
			 <li><a href="<?php echo base_url()?>metroheights" title="METROHEIGHTS (FOR PROPERTY VENDOR)" >METROHEIGHTS</a></li>
			 <li><a href="<?php echo base_url()?>onebox" title="ONE BOX (LIKE IDIOT BOX FOR SOCIAL MEDIA )">ONE BOX</a></li>
			  </ul>
			 </li>
			</ul>
			 </li>
            <li><a href="<?php echo base_url()?>welcome/offerings">Who we Serve</a>
			 <ul class="dropdown">
			 <li><a href="<?php echo base_url()?>banking" title="BANKING AND FINANCIAL SERVICES,INSURANCE">FINANCIAL SERVICES</a></li>
			 <li><a href="<?php echo base_url()?>retail" title="RETAIL">RETAIL</a></li>
			 <li><a href="<?php echo base_url()?>manufacturing" title="MANUFACTURING">MANUFACTURING</a></li>
			 <li><a href="<?php echo base_url()?>entertainment" title="TECHNOLOGY,MEDIA,ENTERTAINMENT">ENTERTAINMENT</a></li>
			 <li><a href="<?php echo base_url()?>travel" title="TRAVEL,TRANSPORTATION,LOGISTICS">LOGISTICS</a></li>
			 <li><a href="<?php echo base_url()?>psu" title="PUBLIC SECTOR & GOVT SECTOR">PUBLIC & GOVT SECTOR</a>
			
			 </li>
			 </ul>
			</li>
           
            
            
            <!--======= SEARCH ICON =========-->
            <li class="search-nav right"><a href="#."><i class="fa fa-search"></i></a>
              <ul class="dropdown">
                <li>
                  <form>
                    <input type="search" class="form-control" placeholder="Enter Your Keywords..." required>
                    <button type="submit"> SEARCH </button>
                  </form>
                </li>
              </ul>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </header>
  <!-- End Header --> 