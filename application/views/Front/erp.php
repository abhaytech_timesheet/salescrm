  <!-- End Header --> 
  
  <!--======= SUB BANNER =========-->
  <section class="sub-banner about">
    <div class="container">
      <div class="position-center-center">
        <h2>ERP SIMPLIFICATION</h2>
        <span style="color:#fff;"><q style="font-size:20px;">TechAarjavam where simplicity is the ultimate sophistication....................</q></span>
	</div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    
    <!-- Intro -->
    
    
    <!-- Google Front Page -->
    <section class="front-page padding-top-100 padding-bottom-100">
      <div class="container">
        <div class="row">
          <div class="col-md-4"> <img class="img-responsive margin-top-30" src="<?php echo base_url()?>application/libraries/front-lib/images/erp.png" alt="mobile-app "> </div>
          <div class="col-md-8"> 
            <!-- Tittle -->
            <div class="heading-block text-left margin-bottom-20">
              <h2>ERP SIMPLIFICATION</h2>
              <h4>We are focused on building a smarter future, so we are literally </h4>
              
              <p align="justify">Over the years, ERP system has grown to be a massive software application that can automate the flow of information among enterprise functions across geographically strewn business units as well as with the external stakeholders. The current estimates put ERP market to over $50 billion in total revenue with a multitude of tightly integrated functional modules covering end to end and standardized business processes for any industry vertical.</p>
           <p align="justify">Success in IT projects isn’t simply about going live on time and on budget, nor does it rest with successful execution of technology-centered aspects. Real success is measured in the achievement of business outcomes. Leading analysts suggest that organizations often fail to achieve desired business outcomes because of underestimating the importance of organizational change management in ERP projects. In a dynamic business environment with increasing complexities, ERP is fast becoming less integrated with the desired business outcomes of administrative and operational functions within the organization. </p>
          
          
            </div>
            
            <!-- List Style -->
            
            
           
              </div>
        </div>
      </div>
    </section>
    
    <!-- Our Clients  -->
   <?php /*<section class="clients padding-bottom-100 padding-top-100">
      <div class="container"> 
        
        <!-- Tittle -->
        <div class="heading-block white-text text-center margin-bottom-80">
          <h2>Our Clients </h2>
          <span class="intro-style">Do you want to be part of this client list? </span> </div>
        
        <!-- Clients Images -->
        <ul class="col-5 text-center">
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-1.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-2.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-3.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-4.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-5.png" alt=""></li>
        </ul>
      </div>
    </section>
    */?>
    <!-- Team  -->
   
  </div>
  <!-- End Content --> 
  
  <!-- Footer -->
 
</div>
<!-- End Page Wrapper --> 

<!-- JavaScripts --> 
</body>


</html>
