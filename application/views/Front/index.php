<!--======= HOME MAIN SLIDER =========-->
<section class="home-slider">
    <div class="tp-banner-container">
		<div class="tp-banner-fix">
			<ul>
				
				<!-- Slider 1 -->
				<li data-transition="fade" data-slotamount="7"> <img src="<?php echo base_url();?>application/libraries/front-lib/images/slides/slider-bg2.jpg" alt="Tech Aarjavam! Banner Background" data-bgposition="center top"  /> 
					
					<!-- Layer -->
					<div class="tp-caption sft tp-resizeme font-extra-bold" 
					data-x="right" data-hoffset="0"
					data-y="center" data-voffset="0" 
					data-speed="700" 
					data-start="700" 
					data-easing="easeOutBack"
					data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
					data-splitin="none" 
					data-splitout="none" 
					data-elementdelay="0.1" 
					data-endelementdelay="0.1" 
					data-endspeed="300" 
					data-captionhidden="on"> <img src="<?php echo base_url();?>application/libraries/front-lib/images/slides/img--1-1.png" alt="Tech Aarjavam! Services on single platform." > </div>
					
					<!-- Layer -->
					<div class="tp-caption sfb tp-resizeme font-bold" 
					data-x="left" data-hoffset="40"
					data-y="center" data-voffset="-100"
					data-speed="500" 
					data-start="700" 
					data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 50%;" 
					data-easing="Back.easeOut" 
					data-splitin="none" 
					data-splitout="none" 
					data-elementdelay="0.1" 
					data-endelementdelay="0.1"
					data-endspeed="300" 
					data-captionhidden="on"
					><h1 style="color: #fff; font-size: 48px; font-weight: normal; letter-spacing:0px; line-height:55px;"> We Provide You With The<br>
				    Highest level of services.</h1></div>
					
					<!-- Layer -->
					<div class="tp-caption sfb tp-resizeme" 
					data-x="left" data-hoffset="40"
					data-y="center" data-voffset="30"
					data-speed="500" 
					data-start="1000" 
					data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
					data-easing="Back.easeOut" 
					data-splitin="none" 
					data-splitout="none" 
					data-elementdelay="0.1" 
					data-endelementdelay="0.1" 
					data-endspeed="300" 
					data-captionhidden="on"
					style="color: #fff; font-size: 30px; font-weight: normal; line-height:36px;"> We are the answer for <br> all the solutions you need <br> for your business.</div>
					
					<!-- Layer -->
					<div class="tp-caption sfb tp-resizeme font-crimson" 
					data-x="left" data-hoffset="40"
					data-y="center" data-voffset="150"
					data-speed="500" 
					data-start="1300" 
					data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
					data-easing="Back.easeOut" 
					data-splitin="none" 
					data-splitout="none" 
					data-elementdelay="0.1" 
					data-endelementdelay="0.1" 
					data-endspeed="300" 
					data-captionhidden="on"
					style=""> <a href="#." class="btn">Learn More</a> <a href="#." class="btn btn-white margin-left-20">Get a quote</a> </div>
				</li>
				
			</ul>
		</div>
	</div>
</section>

<!-- Content -->
<div id="content"> 
    <!-- Tech Aarjavam Solution -->
    <section class="light-gray-bg solution padding-top-20 padding-bottom-2">
		<div class="container"> 
			
			<!-- Tittle -->
			<div class="heading-block text-center margin-bottom-80">
				<h2>Welcome To TechAarjavam</h2>
				<span style="color:#000000"><q style="font-size:20px;">TechAarjavam where simplicity is the ultimate sophistication....................</q></br>
				We are a brand who focuses to helps customers visually to enable them to rise.    </span><a href="#." >Learn More</a> </div>
				<ul class="row text-center">
					<li class="col-md-4" itemscope itemtype="http://schema.org/NewsArticle"><img itemprop="image" src="<?php echo base_url();?>application/libraries/front-lib/images/mobile-app.jpg" alt="Mobile Apps Development" title="Mobile Apps Development" class="img-responsive">
						<h6 align="justify" style="color:#000000" itemprop="headline">Mobile Apps Development.</h6>
						<p align="justify" style="color:#000000"  itemprop="description">Mobile app development leads to build apps for any mobile operating environment and device with your preferred development approach and can manage your mobile app portfolio from a single central interface with detailed operational analytics with your own development tools.   <a href="#." >Learn More</a></p>
</li>
					<!-- Support Center -->
					<li class="col-md-4" itemscope itemtype="http://schema.org/NewsArticle"><img itemprop="image"  src="<?php echo base_url();?>application/libraries/front-lib/images/web-dev.jpg" alt="Web Apps Development,Maintenance & Support." title="Web Apps Development,Maintenance & Support." class="img-responsive">
						<h6 align="justify" style="color:#000000" itemprop="headline">Web Apps Development & Support. </h6>
						<p align="justify" style="color:#000000" itemprop="description">With the web application services we use structured framework programming, best programming practices, coding guidelines and standards. Based on comprehensive research of the most advanced frameworks. we uses the best, most advanced architecture for versatility and future growth; object-oriented best practices; simplicity and a comprehensive code base.     <a href="#." >Learn More</a></p>
					</li>
					<li class="col-md-4" itemscope itemtype="http://schema.org/NewsArticle"><img itemprop="image"  src="<?php echo base_url();?>application/libraries/front-lib/images/enterprise.jpg" alt="Enterprise Apps Development."  title="Enterprise Apps Development." class="img-responsive">
						<h6 align="justify" style="color:#000000" itemprop="headline">Enterprise Apps Development.</h6>
						<p align="justify" style="color:#000000" itemprop="description">Automate your business with enterprise apps by linking applications by following our trends and blueprints,Our wide range of services encompasses the entire financial process value chain from accounting to financial analytics. Our proven processes will streamline financial systems delivery, beating rather than meeting industry benchmarks.  <a href="#." >Learn More</a></p>
					</li>
				</ul>
		</div>
	</section>
	
    <!-- Tech Aarjavam Flow Work  -->
    <section class="flow-work padding-top-20 padding-bottom-80">
		<div class="container"> 
			<!-- Tittle -->
			<div class="heading-block text-center margin-bottom-80">
				<h2>Our Work Flow </h2>
			<span align="justify" class="intro-style">Do you want to improve the online visibility of your brand and drive more relevant traffic to your website? </span> </div>
			<div class="text-center"> <img src="<?php echo base_url();?>application/libraries/front-lib/images/work-folow-img (2).jpg" alt="TechAarjavam! Process Flow or Work Flow Graph. "> </div>
			<ul class="row padding-left-50 padding-right-50">
				<li class="col-sm-3">
					<div class="icon"> <img src="<?php echo base_url();?>application/libraries/front-lib/images/flow-icon-1.png" alt="TechAarjavam! Planning of Process Flow or Planing of Work Flow Graph" > </div>
					<h6>Planning</h6>
					<p align="justify">Planning To accomplish great things, we must not only act but also dream. Not only plan but also believe.</p>
				</li>
				<li class="col-sm-3">
					<div class="icon"> <img src="<?php echo base_url();?>application/libraries/front-lib/images/flow-icon-2.png" alt="TechAarjavam! Designing of Process Flow or Designing of Work Flow Graph"> </div>
					<h6>Designing</h6>
					<p align="justify">Crafting the user experience. The visual design blooms from the templates you choose.</p>
				</li>
				<li class="col-sm-3">
					<div class="icon"> <img src="<?php echo base_url();?>application/libraries/front-lib/images/flow-icon-3.png" alt="TechAarjavam! Developing of Process Flow or Developing of Work Flow Graph" > </div>
					<h6>Developing</h6>
					<p align="justify">Bring the design to life. content and programming transform your template into a finished, manageable website.</p>
				</li>
				<li class="col-sm-3">
					<div class="icon"> <img src="<?php echo base_url();?>application/libraries/front-lib/images/flow-icon-4.png" alt="TechAarjavam! Deployment of Process Flow or Deployment of Work Flow Graph" > </div>
					<h6>Deployment</h6>
					<p align="justify">Introducing your site to the world. After quality assurance your site is submitted to search engines and officially live.</p>
				</li>
			</ul>
		</div>
	</section>
    
    <!-- Our Clients  -->
</div>
<!-- End Content --> 

<!-- Footer -->