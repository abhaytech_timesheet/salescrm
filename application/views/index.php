<script>
window.onload=function(){
var no=<?php echo $depart;?>;
if(no=='1')
$("#txtdepartment").prop("selectedIndex",1);
if(no=='2')
$("#txtdepartment").prop("selectedIndex",2);
if(no=='3')
$("#txtdepartment").prop("selectedIndex",3);
if(no=='4')
$("#txtdepartment").prop("selectedIndex",4);
};
</script>
<div id="stylized">
<input type="hidden" id="txtdep" name="txtdep" value="<?php echo $depart;?>">
    <!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
		<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					HELP DESK 
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo base_url();?>index.php/<?php echo $this->router->fetch_class();?>/dashboard/">
								Help Desk
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?php echo base_url();?>index.php/<?php echo $this->router->fetch_class();?>/index/">
								All Ticket
							</a>
							
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="">
								<i class="fa fa-calendar"></i>
								<span></span>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                	<div class="note note-success">
						<p>
                         <form method="post" action="#" name="status" id="status">
                        <div class="form-body">
							 <div class="form-group">
										<label class="col-md-2 control-label">Department</label>
										<div class="col-md-3">
                                            <select name="txtdepartment" id="txtdepartment" class="form-control input-medium">
                                              <option value="-1">Select</option>
                                              <option value="1">Support</option>
                                              <option value="2">Sales</option>
                                              <option value="3">Development</option>
                                              <option value="4">Complaints</option>
                                            </select>
										</div>
                                        <input type="button" name="submit" id="submit" onClick="display_ticket()" class="btn green" value="SUBMIT"/>
									</div>
                            </div>
						</p>
                        </form>
					</div>
	
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Ticket
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								
								
									
								
							</div>
                            <div class="table-responsive" id="data1">
							<table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
							<thead>
							<tr>
								<th>
									 S No.
								</th>
								<th>
									 Department
								</th>
								<th>
									 Subject
								</th>
								<th>
									 Status
								</th>
								<th>
									 Urgency
								</th>
                                <th>
									 Date
								</th>
                                <th>
									 Assign to
								</th>
                                <th>
									 Action
								</th>
							</tr>
							</thead>
                            <tbody>
                            <?php  
							$sn=1;
									foreach($rec->result() as $row)
									{
										if(($row->C_Status)!="DEACTIVE")
										{
										
							?>
							<tr>
                            	<td>
									 <?php echo $sn; ?>
								</td>
								<td>
                                	<?php echo $row->Department ?>
								</td>
								<td>
									<a href="<?php echo base_url();?>index.php/<?php echo $this->router->fetch_class();?>/view_ticket/<?php echo $row->Ticket_reference_no ?>" style="color:#0066FF; text-decoration:none;">
										<?php echo '#'.($row->Ticket_reference_no).' - '.$row->Subject ?>
                                        <input type="hidden" name="hd<?php echo $row->Ticket_id; ?>" id="hd<?php echo $row->Ticket_id; ?>" value="<?php echo $row->Ticket_reference_no?>" />
                        			</a>
								</td>
								<td>
                                	 <?php echo $row->C_Status; ?>
								</td>
								<td>
									<?php 
										echo $row->Urgency;
								    ?>
								</td>
								<td>
									 <?php echo substr($row->Sub_date,0,10) ?>
								</td>
                                
                                <td>
								<?php 
								if(($row->C_Status)!="DEACTIVE")
										{
								?>
									 	<select name="ddemp_name<?php echo $row->Ticket_id; ?>" id="ddemp_name<?php echo $row->Ticket_id; ?>" class="form-control input-inline input-medium">
                                     	<option value="-1">Select Employee</option>
								
								<?php
								$ticket_type=$row->Dep_id;
                                    foreach($emp->result() as $row2)
                                        {
                                        if($row->Dep_id==1||$row->Dep_id==3)
											{
												if($row2->order_id==1)
										 {
												?>
                                     <option value="<?php echo $row2->user_id; ?>"><?php echo $row2->user_name; ?></option>
											<?php
											}
											}
										 if($row->Dep_id==$row2->order_id && $row2->order_id!=1 )
										 {
                                        ?>                 
					                   <option value="<?php echo $row2->user_id; ?>"><?php echo $row2->user_name; ?></option>
								       <?php
										 }
                                        }

                                        ?>
									</select>
<?php
}?>
								</td>
                                
                                <td>
                                	<input type="button" name="submit" class="btn blue" value="Submit" onclick="save('<?php echo $row->Ticket_id;?>')" />
                                </td>
							</tr>
							
							
                            <?php
								$sn++;
								
										}
								
									}
									
							  ?>
                              </tbody>
							</table>
						</div>
						</div>
					</div>
					
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
</div>
<?php echo $this->session->userdata('profile_id');?>
<script>
function save(id)
{ 

   if($('#ddemp_name'+id).val()!="-1")
    {
    var hidden = $('#hd'+id).val();
	var empname = $('#ddemp_name'+id).val();
 	$.ajax(
        {
 	type: "POST",
 	url:"<?php echo base_url();?>index.php/help_desk/task_assign/",
 	data:"&hd="+hidden+"&ddemp_name="+empname,
 	success: function(msg){
		
    if(msg==1){
 	 $(".page-content").html("<center><h2>Ticket Assign Successfully!</h2></center>")
			.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/img/loading-spinner-blue.gif' /></center>")
			.hide()
			.fadeIn(1000,function()
			{
			
			$("#stylized").load("<?php echo base_url().'index.php/help_desk/after_index/-1'?>");
			
			}
			);
			}
             else
			{
				alert("Some Error on this page");
			}
         }
       });
}
else
{
alert('Please Select Employee!!');
$('#ddemp_name'+id).focus();
}
	
}


</script>

<script>
function printDiv()
{
  $('td:nth-child(2)').hide();
  $('td:nth-child(2),th:nth-child(2)').hide();
  var divToPrint=document.getElementById('sample_2');
  newWin= window.open("");
  newWin.document.write(divToPrint.outerHTML);
  newWin.print();
  newWin.close();
  $('td:nth-child(2)').show();
  $('td:nth-child(2),th:nth-child(2)').show();
}
</script>

<script>
function demoFromHTML() {
    var pdf = new jsPDF('p', 'pt', 'letter');
    // source can be HTML-formatted string, or a reference
    // to an actual DOM element from which the text will be scraped.
    source = $('#data1')[0];

    // we support special element handlers. Register them with jQuery-style 
    // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
    // There is no support for any other type of selectors 
    // (class, of compound) at this time.
    specialElementHandlers = {
        // element with id of "bypass" - jQuery style selector
        '#bypassme': function (element, renderer) {
            // true = "handled elsewhere, bypass text extraction"
            return true
        }
    };
    margins = {
        top: 0,
        bottom: 0,
        left: 40,		
        width: 1280
    };
    // all coords and widths are in jsPDF instance's declared units
    // 'inches' in this case
    pdf.fromHTML(
    source, // HTML string or DOM elem ref.
    margins.left, // x coord
    margins.top, { // y coord
        'width': margins.width, // max width of content on PDF
        'elementHandlers': specialElementHandlers
    },

    function (dispose) {
        // dispose: object with X, Y of the last line add to the PDF 
        //          this allow the insertion of new lines after html
        pdf.save('Test.pdf');
    }, margins);
}
</script>
 <script>
function display_ticket()
{
var dept=$('#txtdepartment').val();
$('#stylized').html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/img/loading-spinner-blue.gif' /></center>");
$('#stylized').load('<?php echo base_url();?>index.php/help_desk/after_index/'+dept);

}
</script>