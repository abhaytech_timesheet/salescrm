<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Configuration</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-shopping-cart"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_news">Manage News</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-plus"></i>
						<span>Add News</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Add News</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-black"></i>
								<span class="caption-subject font-black bold uppercase">Add News</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" action="<?php echo base_url().'index.php/master/insert_news/'?>" id="myform" onsubmit="return check(conwv('myform'))">
								<div class="form-body" id="rank">
									
									<div class="form-group">
										<label class="col-md-2 control-label">Title</label>
										<div class="col-md-7">
											<input type="text" id="txttitle" name="txttitle"  class="form-control input-sm empty" placeholder="Enter News Title." >
											<span id="divtxttitle" style="color:red;"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-2 control-label">Description</label>
										<div class="col-md-9">
											<textarea class="form-control input-sm empty" name="txtdescription" id='txtdescription' placeholder="Enter User Name." cols='50' rows='6' data-provide="markdown" data-error-container="#editor_error" ></textarea>
											<span id="divtxtdescription" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-2 control-label">Status</label>
										<div class="col-md-7">
											<select class="form-control input-sm opt" name="ddstatus" id="ddstatus">
												<option value="-1">Select</option>
												<option value="1" selected>Active</option>
												<option value="0">Inactive</option>
											</select>
											<span id="divddstatus" style="color:red;"></span>
										</div>
									</div>
									
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-2 col-md-9">
										<button type="submit" class="btn green">Add News</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													
