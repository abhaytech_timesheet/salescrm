<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Configuration</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-square"></i>
						<span>Manage Apps</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Add Menus</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs font-black"></i>
								<span class="caption-subject font-black bold uppercase">Add Menus</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div id="tree_2"></div>
						</div>
					</div>
				</div>
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder font-black"></i>
								<span class="caption-subject font-black bold uppercase">Details</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div id="div_load">
							<div class="portlet-body form">
								<form action="<?php echo base_url(); ?>index.php/master/insert_apps" method="post" id="myform" onsubmit="return check(conwv('myform'))">
									<div class="form-body">
										<div class="form-group">
											<label>Main Menu Name</label>
											<input type="text" class="form-control input-sm empty" id="txtname" name="txtname" placeholder="Enter Main Menu Name" />
											<span id="divtxtname" style="color:red"></span>
										</div>
										
										<div class="form-group">
											<label>Main Menu Icon</label>
											<input type="text" class="form-control input-sm empty" id="txticon" name="txticon" placeholder="Enter Main Menu Icon." />
											<span id="divtxticon" style="color:red"></span>
										</div>
										
										<div class="form-group">
											<label>Main Menu Serial No</label>
											<input type="text" class="form-control input-sm empty" id="txtserial" name="txtserial" placeholder="Enter Main Menu Serial No." />
											<span id="divtxtserial" style="color:red"></span>
										</div>
										
										<div class="form-group">
											<label>Main Menu Url</label>
											<input type="text" class="form-control input-sm empty" id="txturl" name="txturl" placeholder="Enter Main Menu Url." />
											<input type="hidden" class="form-control empty" id="txtparent" name="txtparent" value="0" />
											<span id="divtxturl" style="color:red"></span>
										</div>
										
										<div class="form-group">
											<label>Main Menu Description</label>
											<input type="text" class="form-control input-sm empty" id="txtdes" name="txtdes" placeholder="Enter Main Menu Description." />
											<span id="divtxtdes" style="color:red"></span>
										</div>
										
										<div class="form-group">
											<label class="col-md-3 control-label">Is Link</label>
											<div class="col-md-7">
												<div class="radio-list">
													<label class="radio-inline">
														<div class="radio" id="uniform-optionsRadios25">
															<input type="radio"  name="optionsRadios1" id="optionsRadios1" onClick="check_radio(1)">
														</div>
														Enable
													</label>
													<label class="radio-inline">
														<div class="radio" id="uniform-optionsRadios26">
															<input type="radio" name="optionsRadios1" id="optionsRadios2" checked="checked" onClick="check_radio(0)">
														</div>
														Disable
													</label>
												</div>
												<input type="hidden" value="1" id="txtsubcate" name="txtsubcate" />
											</div>
										</div>
										
									</div>
									
									<div class="form-actions">
										<div class="col-md-9 col-md-offset-3">
											<button type="submit" class="btn green">Submit</button>
											<button type="reset" class="btn">Cancel</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													

<script>
	function check_radio(id)
	{
		$("#txtstatus").val(id);
	}
</script>
<script>
	jQuery(document).ready(function(){
		UITree.init();
	});
</script>
<script>
	var UITree = function () {
		var handleSample2 = function () {
			$('#tree_2').jstree({
				'plugins': ["wholerow", "types"],
				'core': {
					"themes" : {
						"responsive": false
					},    
					'data': [
					<?php 
						$mmenu="";
						foreach($menu->result() as $row)
						{
							if($row->m_menu_parentid==0)
							{
								$mmenu=$row->m_menu_name;
								$smenu=$row->m_menu_id;
								$serial=$row->m_menu_serial;
								$parent=$row->m_menu_id;
								
							?>
							
							{
								"text": "<?php echo "<a href='#' onclick='go1($smenu)'>$mmenu-$serial</a>" ?>",
								"children": 
								[
								<?php 
									foreach($menu->result() as $row1)
									{
										if($row1->m_menu_parentid==$smenu)
										{
											$sub=$row1->m_menu_name;
											$sub_id=$row1->m_menu_id;
											$serial1=$row1->m_menu_serial;
											$accno=$row1->m_menu_id;
										?>
										{
											"text": "<?php echo "<a href='#' onclick='go1($sub_id)'>$sub-$serial1</a>" ?>",
											"children": 
											[
											<?php 
												foreach($menu->result() as $row2)
												{
													if($row2->m_menu_parentid==$sub_id)
													{
														$sub2=$row2->m_menu_name;
														$sub_id2=$row2->m_menu_id;
														$accno=$row2->m_menu_id;
														$serial2=$row->m_menu_serial;
													?>
													{
														"text": "<?php echo "<a href='#' onclick='go1($sub_id2)'>$sub2-$serial2</a>" ?>",
														"children": 
														[
														<?php 
															foreach($menu->result() as $row3)
															{
																if($row3->m_menu_parentid==$sub_id2)
																{
																	$sub3=$row3->m_menu_name;
																	$sub_id3=$row3->m_menu_id;
																	$accno=$row3->m_menu_id;
																	$serial3=$row->m_menu_serial;
																?>
																{
																	"text": "<?php echo "<a href='#' onclick='go1($sub_id3)'>$sub3-$serial3</a>" ?>",
																	"children": 
																	[
																	<?php 
																		foreach($menu->result() as $row4)
																		{
																			if($row4->m_menu_parentid==$sub_id3)
																			{
																				$sub4=$row4->m_menu_name;
																				$sub_id4=$row4->m_menu_id;
																				$accno=$row4->m_menu_id;
																				$serial4=$row->m_menu_serial;
																			?>
																			{
																				"text": "<?php echo "<a href='#' onclick='go1($sub_id4)'>$sub4-$serial4</a>"  ?>",
																				"children": 
																				[
																				<?php 
																					foreach($menu->result() as $row5)
																					{
																						if($row5->m_menu_parentid==$sub_id4)
																						{
																							$sub5=$row5->m_menu_name;
																							$sub_id5=$row5->m_menu_id;
																							$accno=$row5->m_menu_id;
																							$serial5=$row->m_menu_serial;
																						?>
																						{
																							"text": "<?php echo "<a href='#' onclick='go1($sub_id5)'>$sub5-$serial5</a>" ?>",
																							"children": 
																							[
																							<?php 
																								foreach($menu->result() as $row6)
																								{
																									if($row6->m_menu_parentid==$sub_id5)
																									{
																										$sub6=$row6->m_menu_name;
																										$sub_id6=$row6->m_menu_id;
																										$accno=$row6->m_menu_id;
																										$serial6=$row->m_menu_serial;
																									?>
																									{
																										"text": "<?php echo "<a href='#' onclick='go1($sub_id6)'>$sub6-$serial6</a>" ?>",
																									},
																									<?php
																									}
																								}
																							?>
																							]
																						},
																						<?php
																						}
																					}
																				?>
																				]
																			},
																			<?php
																			}
																		}
																	?>
																	]
																},
																<?php
																}
															}
														?>
														]
													},
													<?php
													}
												}
											?>
											]
										},
										<?php
										}
									}
								?>
								]
								
							},
							<?php
							}
						}
					?>
					
					]
					
				},
				
			});
		}
		return {
			//main function to initiate the module
			init: function () {
				
				handleSample2();
				
			}
			
		};
		
	}();
</script>

<script>
	function go1(id)
	{
		$("#div_load").html("<center><img id='checkmark' src='<?php echo base_url(); ?>/application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif'  /></center>");
		$('#div_load').load("<?php echo base_url()?>index.php/master/view_add_subapps/"+id+"/");
	}
</script>