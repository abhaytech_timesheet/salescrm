<script>
	function check()
	{
		var collection=$("#mainmenu");
		var mark=0;
		var obtainmark=0;
		var inputs=collection.find("input[type=checkbox],input[type=radio]");
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			var name=inputs[x].name;
			if($("#"+id+"").is(':checked'))
			{
				if(id=="radio-1")
				{
					$("#txtstatus").val('1');
				}
				if(id=="radio-2")
				{
					$("#txtstatus").val('0');
				}
			}
		}
	}
</script>
<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content"> 
		
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12"> 
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title"> Admin Panel </h3>
				<ul class="page-breadcrumb breadcrumb">
					<li> <i class="fa fa-home"></i> <a href="#"> Dashboard </a> <i class="fa fa-angle-right"></i> </li>
					<li> <a href="#"> Main Apps </a> </li>
					<li class="pull-right">
						<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" > <i class="fa fa-calendar"></i> <span> </span> </div>
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB--> 
			</div>
		</div>
		<!-- END PAGE HEADER--> 
		<!-- BEGIN PAGE CONTENT-->
		<div class="row"> 
			<!--row-->
			<div class="col-md-6"> 
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="portlet box blue">
					<div class="portlet-title">
						<div class="caption"> <i class="fa fa-reorder"></i> Manage Main Apps </div>
						<div class="tools"> <a href="" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="" class="reload"> </a> <a href="" class="remove"> </a> </div>
					</div>
					<div class="portlet-body form">
						<form name="myForm" id="mainmenu" action="<?php echo base_url() ?>index.php/master/add_mainmenu" method="post" role="form" class="form-horizontal" onsubmit="return validateForm()">
							<div class="form-body">
								<div class="form-group">
									<label class="col-md-3 control-label">Main Apps</label>
									<div class="col-md-9">
										<input type="text" class="form-control input-large" id="txtmname" name="txtmname" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Apps Description</label>
									<div class="col-md-9">
										<textarea id="txtdesc" name="txtdesc" rows="5" class="form-control input-large"></textarea>
										
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Apps Status</label>
									<div class="col-md-9">
										<div class="radio-list">
											<label class="radio-inline">
												<div class="radio" id="uniform-optionsRadios25">
													<input type="radio"  name="optionsRadios1" id="optionsRadios1" checked="checked" onClick="check()">
												</div>
												Enable
											</label>
											<label class="radio-inline">
												<div class="radio" id="uniform-optionsRadios26">
													<input type="radio" name="optionsRadios1" id="optionsRadios2" onClick="check()">
												</div>
												Disable
											</label>
										</div>
										<input type="hidden" value="1" id="txtstatus" name="txtstatus" />
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn blue">Submit</button>
									<button type="button" class="btn">Cancel</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET--> 
			</div>
			<!-- END FORM CONTENT-->
			
			<div class="col-md-6"> 
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box blue">
					<div class="portlet-title">
						<h4><i class="icon-edit"></i>VIEW & MODIFY </h4>
						<div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#portlet-config" data-toggle="modal" class="config"></a> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
					</div>
					<div class="portlet-body">
						<div class="clearfix"> </div>
						<table class="table table-striped table-hover table-bordered" id="sample_2">
							<thead>
								<tr>
									<th width="266">Apps Name</th>
									<th width="58">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									
									foreach($main_menu->result() as $row)
									{
										$status=0;
										if($row->m_menu_status==1)
										{
											$status="Enable";
										}
										if($row->m_menu_status==0)
										{
											$status="Disable";
										}
									?>
									<tr>
										<td><?php echo $row->m_menu_name?></td>
										<td><a href="<?php echo base_url();?>index.php/master/edit_menu/<?php echo $row->m_menu_id;?>" rel="facebox"> <span class="btn yellow btn-xs">Edit</span></a>
											<?php 
												if($status=="Enable")
												{
												?>
												&nbsp; <a href="<?php echo base_url();?>index.php/master/stch_menu/<?php echo $row->m_menu_id;?>/0"> <span class="btn red btn-xs">Disable</span> </a>
												<?php
												}
												if($status=="Disable")
												{
												?>
												&nbsp; <a href="<?php echo base_url();?>index.php/master/stch_menu/<?php echo $row->m_menu_id;?>/1"> <span class="btn green btn-xs">Enable</span> </a>
												<?php
												}
											?></td>
									</tr>
									<?php
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT--> 
		</div>
	</div>
	<!-- END CONTENT --> 
</div>
<!-- END CONTAINER --> 
<script type="text/javascript">
	
	function validateForm()
	{
		var x=document.forms["myForm"]["txtmname"].value;
		var y=document.forms["myForm"]["txtdesc"].value;
		if(x!=null && x!="")
		{
			if(y!=null && y!="")
			{
				return true;
			}
			else
			{
				document.forms["myForm"]["txtdesc"].focus();
				$("#divtxtdesc").html('Please Enter Discription');
				return false;
			}
		}
		else
		{
			document.forms["myForm"]["txtmname"].focus();
			$("#divtxtmname").html('Please Enter Menu Name');
			return false;
		} 
		
	}
</script>