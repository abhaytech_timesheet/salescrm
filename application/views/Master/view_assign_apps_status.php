<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Configuration</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-file-o"></i>
						<span>Update Assign App and subapp</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> All Assign Apps And Subapps</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-file-o font-dark"></i>
								<span class="caption-subject bold uppercase">Manage All Assign Apps And Subapps</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body" style="overflow-x: auto;">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S.No</th>
										<th>Menu Name</th>
										<th>User ID</th>
										<th>View Rights</th>
										<th>Add Rights</th>
										<th>Update Rights</th>
										<th>Delete Rights</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$sn=0;
										foreach($menu->result() as $row)
										{
											$sn++;
										?>
										<tr>
											<td><?php echo $sn; ?></td>
											<td><?php echo $row->m_submenu_name; ?></td>
											<td><?php echo $row->or_m_name; ?> <b>( <?php echo $row->or_m_user_id; ?> )</b> </td>
											<td><?php if($row->tr_assign_view){echo "Yes";}else{echo "No";} ?></td>
											<td><?php if($row->tr_assign_add){echo "Yes";}else{echo "No";} ?></td>
											<td><?php if($row->tr_assign_update){echo "Yes";}else{echo "No";} ?></td>
											<td><?php if($row->tr_assign_delete){echo "Yes";}else{echo "No";} ?></td>
											<td>
												<?php
													if($row->tr_assign_status==1)
													{
													?>
													<a href="<?php echo base_url(); ?>index.php/master/update_assign_status_apps/<?php echo $row->tr_assign_id; ?>/0" class="btn red btn-sm" >Disable</a>
													<?php
													}
													else
													{
													?>
													<a href="<?php echo base_url(); ?>index.php/master/update_assign_status_apps/<?php echo $row->tr_assign_id; ?>/1" class="btn green btn-sm">Enable</a>
													<?php
													}
												?>
											</td>
											<?php 
											}
										?>    
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->
