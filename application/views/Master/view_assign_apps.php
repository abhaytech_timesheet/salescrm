<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Configuration</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-sitemap"></i>
						<span>Assign App</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Assign Apps To User</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Assign Apps To User </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form" id="userid">
							
							<?php 
								$mmenu="";
								foreach($menu->result() as $row)
								{
									if($row->m_menu_parentid==0)
									{
										$mmenu=$row->m_menu_name;
										$smenu=$row->m_menu_id;
										$assign_menu="";
										if($user->num_rows()>0)
										{
											foreach($user->result() as $rows)
											{
												if($rows->tr_assign_menuid==$smenu)
												{
													$assign_menu=$rows->tr_assign_menuid;
												}
											}
										}
									?>
									<ul class="list-unstyled">
										<li>
											<label><input type="checkbox" onClick="chbchecksin()" id="<?php echo $smenu; ?>" name="<?php echo $smenu; ?>" value="<?php echo $smenu; ?>" <?php echo ($assign_menu==$smenu ? 'checked':'');?>><?php echo $mmenu; ?></label>
											<ul class="list-unstyled">
												<?php 
													foreach($menu->result() as $row2)
													{
														if($row2->m_menu_parentid==$smenu)
														{
															$sub=$row2->m_menu_name;
															$sub_id=$row2->m_menu_id;
															$subassign_menu="";
															if($user->num_rows()>0)
															{
																foreach($user->result() as $rows)
																{
																	if($rows->tr_assign_menuid==$sub_id)
																	{
																		$subassign_menu=$rows->tr_assign_menuid;
																	}
																}
															}
														?>
														<li>
															<label><input type="checkbox" onClick="chbchecksin()" id="<?php echo $sub_id; ?>" name="<?php echo $sub_id; ?>" value="<?php echo $sub_id; ?>" <?php echo ($subassign_menu==$sub_id ? 'checked':'');?> /><?php echo $sub; ?></label>
															<ul class="list-unstyled">
																<?php 
																	foreach($menu->result() as $row3)
																	{
																		if($row3->m_menu_parentid==$sub_id)
																		{
																			$sub1=$row3->m_menu_name;
																			$sub_id1=$row3->m_menu_id;
																			$sub1assign_menu="";
																			if($user->num_rows()>0)
																			{
																				foreach($user->result() as $rows)
																				{
																					if($rows->tr_assign_menuid==$sub_id1)
																					{
																						$sub1assign_menu=$rows->tr_assign_menuid;
																					}
																				}
																			}
																		?>
																		<li>
																			<label><input type="checkbox" onClick="chbchecksin()" id="<?php echo $sub_id1; ?>" name="<?php echo $sub_id1; ?>" value="<?php echo $sub_id1; ?>" <?php echo ($sub1assign_menu==$sub_id1 ? 'checked':'');?> /><?php echo $sub1; ?></label>
																		</li>
																		<?php
																		}
																	}
																?>
															</ul>
														</li>
														<?php
														}
													}
												?>
											</ul>
										</li>
									</ul>
									<?php
									}
								}
							?>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Details </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div id="div_load">
							<div class="portlet-body form">
								<form class="form-horizontal" action="<?php echo base_url(); ?>index.php/master/assign_apps_to_user" method="post" id="myform" onsubmit="return check(conwv('myform'))">
									<div class="form-body">
										<div class="form-group">
											<label class="control-label col-md-4">Email Id</label>
											<div class="col-md-8">
												<input type="text" class="form-control input-sm" id="txtemail" name="txtemail" placeholder="Enter Email Id" value="<?php echo $user_mail; ?>" />
												<span id="divtxtemail" style="color:red"></span>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-md-4">View Rights</label>
											<div class="col-md-8">
												<input type="checkbox" id="chview" onclick="get_id('chview','txtview')">
												<input type="hidden" name="txtview" id="txtview">
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-md-4">Add Rights</label>
											<div class="col-md-8">
												<input type="checkbox" id="chadd" onclick="get_id('chadd','txtadd')">
												<input type="hidden" name="txtadd" id="txtadd">
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-md-4">Updation Rights</label>
											<div class="col-md-8">
												<input type="checkbox" id="chupdate" onclick="get_id('chupdate','txtupdate')">
												<input type="hidden" name="txtupdate" id="txtupdate">
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-md-4">Deletetion Rights</label>
											<div class="col-md-8">
												<input type="checkbox" id="chdel" onclick="get_id('chdel','txtdelete')">
												<input type="hidden" name="txtdelete" id="txtdelete">
												<input type="hidden" name="txtquid" id="txtquid">
											</div>
										</div>
									</div>
									
									<div class="form-actions">
										<div class="col-md-9 col-md-offset-3">
											<button type="submit" class="btn green">Submit</button>
											<button type="reset" class="btn">Cancel</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->

<script>
	function check_radio(id)
	{
		$("#txtstatus").val(id);
	}
</script>

<script>
	var quid="";
	var status=0;
	var descr="";
    chbchecksin();
	function chbchecksin()
	{
		quid="";
		$("#txtuserid").val('');
		var collection=$("#userid");
		var inputs=collection.find("input[type=checkbox]");
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			var name=inputs[x].name;
			if(document.getElementById(id).checked)
			{ 
				quid=id+","+quid;
				
			}
		}
		$("#txtquid").val(quid);
	}
</script>

<script>
	function get_id(id,id2)
	{
		if($('#'+id).prop('checked') === true)
		{
			$('#'+id2).val('1');
		}
		else
		{
			$('#'+id2).val('0');
		}
	}
</script>
