<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Configuration</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-file-o"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_change_status_apps">Status Change Apps</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-edit"></i>
						<span>Edit Menu</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Edit Menus</h3>
			<!-- END PAGE TITLE-->
			<?php 
				foreach($menu->result() AS $rows)
				{
					break;
				}
			?>
			<div class="row">
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder font-black"></i>
								<span class="caption-subject font-black bold uppercase">Details</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div id="div_load">
							<div class="portlet-body form">
								<form action="<?php echo base_url(); ?>index.php/master/update_apps/<?php echo $rows->m_menu_id; ?>" method="post" id="myform" onsubmit="return check(conwv('myform'))">
									<div class="form-body">
										<div class="form-group">
											<label>Menu Name</label>
											<input type="text" class="form-control input-sm empty" id="txtname" name="txtname" value="<?php echo $rows->m_menu_name; ?>" placeholder="Enter Menu Name" />
											<span id="divtxtname" style="color:red"></span>
										</div>
										
										<div class="form-group">
											<label>Menu Icon</label>
											<input type="text" class="form-control input-sm empty" id="txticon" name="txticon" value="<?php echo $rows->m_menu_icon; ?>" placeholder="Enter Menu Icon." />
											<span id="divtxticon" style="color:red"></span>
										</div>
										
										<div class="form-group">
											<label>Menu Serial No</label>
											<input type="text" class="form-control input-sm empty" id="txtserial" name="txtserial" value="<?php echo $rows->m_menu_serial; ?>" placeholder="Enter Menu Serial No." />
											<span id="divtxtserial" style="color:red"></span>
										</div>
										
										<div class="form-group">
											<label>Menu Url</label>
											<input type="text" class="form-control input-sm empty" id="txturl" name="txturl" value="<?php echo $rows->m_menu_url; ?>" placeholder="Enter Menu Url." />
											<input type="hidden" class="form-control empty" id="txtparent" name="txtparent" value="<?php echo $rows->m_menu_id; ?>" />
											<span id="divtxturl" style="color:red"></span>
										</div>
										
										<div class="form-group">
											<label>Menu Description</label>
											<input type="text" class="form-control input-sm empty" id="txtdes" name="txtdes" value="<?php echo $rows->m_menu_desc; ?>" placeholder="Enter Menu Description." />
											<span id="divtxtdes" style="color:red"></span>
										</div>
										
										<div class="form-group">
											<label class="col-md-3 control-label">Is Link</label>
											<div class="col-md-7">
												<div class="radio-list">
													<label class="radio-inline">
														<div class="radio" id="uniform-optionsRadios25">
															<input type="radio"  name="optionsRadios1" id="optionsRadios1" onClick="check_radio(1)">
														</div>
														Enable
													</label>
													<label class="radio-inline">
														<div class="radio" id="uniform-optionsRadios26">
															<input type="radio" name="optionsRadios1" id="optionsRadios2" checked="checked" onClick="check_radio(0)">
														</div>
														Disable
													</label>
												</div>
												<input type="hidden" value="1" id="txtsubcate" name="txtsubcate" />
											</div>
										</div>
										
									</div>
									
									<div class="form-actions">
										<div class="col-md-9 col-md-offset-3">
											<button type="submit" class="btn green">Submit</button>
											<button type="reset" class="btn">Cancel</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													

<script>
	function check_radio(id)
	{
		$("#txtstatus").val(id);
	}
</script>