<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Configuration</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-file-o"></i>
						<span>Status Change Apps</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> All Apps And Subapps</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				
				<!-- END PAGE CONTENT-->
				
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-file-o font-dark"></i>
								<span class="caption-subject bold uppercase">Manage All Apps And Subapps</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S.No</th>
										<th>Menu Name</th>
										<th>Serail No.</th>
										<th>Icon</th>
										<th>Description</th>
										<th>Url</th>
										<th>Entry Date</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$sn=0;
										foreach($menu->result() as $row)
										{
											$sn++;
										?>
										<tr>
											<td><?php echo $sn; ?></td>
											<td><?php echo $row->m_menu_name; ?></td>
											<td><?php echo $row->m_menu_serial; ?></td>
											<td><?php echo $row->m_menu_icon; ?></td>
											<td><?php echo $row->m_menu_desc; ?></td>
											<td><?php echo $row->m_menu_url; ?></td>
											<td><?php echo $row->m_entrydate; ?></td>
											<td>
												<div class="btn-group">
													<button class="btn red btn-sm dropdown-toggle" data-toggle="dropdown">Action
														<i class="fa fa-angle-down"></i>
													</button>
													<ul class="dropdown-menu">
														<?php
															if($row->m_menu_status==1)
															{
															?>
															<li>
																<a href="<?php echo base_url(); ?>index.php/master/update_status_apps/<?php echo $row->m_menu_id; ?>/0"><i class='fa fa-trash-o'></i>Disable</a>
															</li>
															<?php
															}
															else
															{
															?>
															<li>
																<a href="<?php echo base_url(); ?>index.php/master/update_status_apps/<?php echo $row->m_menu_id; ?>/1"><i class='fa fa-refresh'></i>Enable</a>
															</li>
															<?php
															}
														?>
														<li>
															<a class="edit" id="appid" href="javascript:;" onclick="assignid(<?php echo $row->m_menu_id; ?>)">
																<i class="fa fa-edit (alias)"></i>Edit
															</a>
														</li>
													</ul>
												</div>
											</td>
											<?php 
											}
										?>    
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	

<script>
	var TableEditable = function () {
		
		return {
			
			//main function to initiate the module
			init: function () {
				function restoreRow(oTable, nRow) {
					var aData = oTable.fnGetData(nRow);
					var jqTds = $('>td', nRow);
					
					for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
						oTable.fnUpdate(aData[i], nRow, i, false);
					}
					
					oTable.fnDraw();
				}
				
				function editRow(oTable, nRow) {
					var aData = oTable.fnGetData(nRow);
					var jqTds = $('>td', nRow);
					jqTds[1].innerHTML = '<input type="text" class="form-control input-small" id="txtname" value="' + aData[1] + '">';
					jqTds[2].innerHTML = '<input type="text" class="form-control input-small" id="txtserial" value="' + aData[2] + '">';
					jqTds[3].innerHTML = '<input type="text" class="form-control input-small" id="txticon" value="' + aData[3] + '">';
					jqTds[4].innerHTML = '<input type="text" class="form-control input-small" id="txtdesc" value="' + aData[4] + '">';
					jqTds[5].innerHTML = '<input type="text" class="form-control input-small" id="txturl" value="' + aData[5] + '">';
					jqTds[6].innerHTML = '<input type="text" class="form-control input-small" id="txtdate" value="' + aData[6] + '">';
					jqTds[7].innerHTML = '<a class="edit" href="" onclick="update_data()"><i class="fa fa-check fa-2x"></i></a>  <a class="cancel" href=""><i class="fa fa-times fa-2x"></i></a>';
				}
				
				
				var oTable = $('#sample_editable_1').dataTable({
					"aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
					],
					// set the initial value
					"iDisplayLength": 5,
					
					"sPaginationType": "bootstrap",
					"oLanguage": {
						"sLengthMenu": "_MENU_ records",
						"oPaginate": {
							"sPrevious": "Prev",
							"sNext": "Next"
						}
					},
					"aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
					}
					]
				});
				
				jQuery('#sample_editable_1_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); // modify table search input
				jQuery('#sample_editable_1_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
				jQuery('#sample_editable_1_wrapper .dataTables_length select').select2({
					showSearchInput : false //hide search box with special css class
				}); // initialize select2 dropdown
				
				var nEditing = null;
				
				
				$('#sample_editable_1 a.cancel').live('click', function (e) {
					e.preventDefault();
					if ($(this).attr("data-mode") == "new") {
						var nRow = $(this).parents('tr')[0];
						oTable.fnDeleteRow(nRow);
						} else {
						restoreRow(oTable, nEditing);
						nEditing = null;
					}
				});
				
				$('#sample_editable_1 a.edit').live('click', function (e) {
					e.preventDefault();
					
					/* Get the row as a parent of the link that was clicked on */
					var nRow = $(this).parents('tr')[0];
					
					if (nEditing !== null && nEditing != nRow) {
						/* Currently editing - but not this row - restore the old before continuing to edit mode */
						restoreRow(oTable, nEditing);
						editRow(oTable, nRow);
						nEditing = nRow;
						} else if (nEditing == nRow && this.innerHTML == "Save") {
						/* Editing this row and want to save it */
						saveRow(oTable, nEditing);
						nEditing = null;
						alert("Updated! Do not forget to do some ajax to sync with backend :)");
						} else {
						/* No edit in progress - let's start one */
						editRow(oTable, nRow);
						nEditing = nRow;
					}
				});
			}
			
		};
		
	}();	
</script>

<script>
	function assignid(id)
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>/application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif'  /></center>");
		$('#stylized').load("<?php echo base_url()?>index.php/master/view_edit_apps/"+id+"/");
	}
</script>	

<script>	 
	function update_data()
	{
		var data = {
			txtname:$('#txtname').val(),
			txtserial:$('#txtserial').val(),
			txticon:$('#txticon').val(),
			txtdesc:$('#txtdesc').val(),
			txturl:$('#txturl').val(),
			txtdate:$('#txtdate').val(),	
			hdid:$('#hdid').val(),	
		};
		$.ajax(
		{
			type: "POST",
			url:"<?php echo base_url();?>index.php/master/update_menu/",
			data:data,
			success: function(msg){
				if(msg==1)
				{
					alert('Update Successfull');
					window.location.reload();
				}
			}
		});
	}
</script>								