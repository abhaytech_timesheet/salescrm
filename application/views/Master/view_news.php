<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Configuration</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-shopping-cart"></i>
						<span>Manage News</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View All News </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-ticket font-dark" ></i>
								<span class="caption-subject bold uppercase">View All News</span>
							</div>
							<div class="actions">
								<div class="btn-group">
									<a href="<?php echo base_url(); ?>index.php/master/add_news" class="btn green btn-sm blue-stripe" style="margin-right:20px;">
										<i class="fa fa-plus"></i>
										<span class="hidden-480">
											Add News
										</span>
									</a>
								</div>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<h5> &nbsp;</h5>
										</div>
									</div>
									<div class="col-md-6">
										
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>
											S No.
										</th>
										<th>
											Action
										</th>
										<th>
											News Title
										</th>
										<th>
											News Date
										</th>
										
										<th>
											Description
										</th>
										
									</tr>
								</thead>
								<tbody>
									<?php 
										$sn=0;
										foreach($content->result() as $row)
										{
											$sn++;
										?>
										<tr class="odd gradeX">
											<td>
												<?php echo $sn;?>
											</td>
											<td>
											    <div class="btn-group">
													<button class="btn red btn-sm dropdown-toggle" data-toggle="dropdown">Action
														<i class="fa fa-angle-down"></i>
													</button>
													<ul class="dropdown-menu">
														
														<li>
															<a href="<?php echo base_url(); ?>index.php/master/edit_news/<?php echo $row->m_news_id;?>" title="Edit" >
															<i class="fa fa-pencil"></i>Edit</a>
														</li>
														<?php 
															if($row->m_news_status==1)
															{
															?>
															<li>
																<a href="<?php echo base_url(); ?>index.php/master/delete_news/<?php echo $row->m_news_id;?>/0" title="Delete News" >
																<i class="fa fa-trash-o"></i>Delete News</a>
															</li>
															<?php
															}
															else
															{
															?>
															<li>
																<a href="<?php echo base_url(); ?>index.php/master/delete_news/<?php echo $row->m_news_id;?>/1" title="Delete News" >
																<i class="glyphicon glyphicon-repeat"></i>Enable News</a>
															</li>
															<?php
															}
														?>
													</ul>
												</div>
												
											</td>
											<td>
												<?php echo $row->m_news_title; ?>
												<td>
													<?php echo $row->m_entrydate; ?>
												</td>
												
												<td>
													<?php echo substr($row->m_news_des,0,100); ?>
												</td>
												
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
						
					</div>
				</div>
				
			</div>
			<!-- END CONTENT BODY -->
		</div>
	</div>
	<!-- END CONTENT -->	
	
	
	<script>
		function create_news()
		{
			$("#stylized").html('<img src="<?php echo base_url(); ?>application/libraries/progressloading.gif" />');
			$("#stylized").load("<?php echo base_url().'index.php/master/add_news'?>");			
		}
	</script>
	
	
