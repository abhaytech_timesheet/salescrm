<script>
	function check()
	{
		var collection=$("#fsunm");
		var mark=0;
		var obtainmark=0;
		var inputs=collection.find("input[type=checkbox],input[type=radio]");
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			var name=inputs[x].name;
			if($("#"+id+"").is(':checked'))
			{
				if(id=="optionsRadios1")
				{
					$("#txtstatus").val('1');
				}
				if(id=="optionsRadios2")
				{
					$("#txtstatus").val('0');
				}
			}
		}
	}
</script>

<script>
	function linkstatus()
	{
		var collection=$("#fsunm");
		var mark=0;
		var obtainmark=0;
		var inputs=collection.find("input[type=checkbox],input[type=radio]");
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			var name=inputs[x].name;
			if($("#"+id+"").is(':checked'))
			{
				if(id=="link1")
				{
					$("#txtlink").val('1');
				}
				if(id=="link2")
				{
					$("#txtlink").val('0');
				}
			}
		}
	}
</script>
<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content"> 
		
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12"> 
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title"> Admin Panel </h3>
				<ul class="page-breadcrumb breadcrumb">
					<li> <i class="fa fa-home"></i> <a href="#"> Dashboard </a> <i class="fa fa-angle-right"></i> </li>
					<li> <a href="#"> Sub Apps </a> </li>
					<li class="pull-right">
						<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" > <i class="fa fa-calendar"></i> <span> </span> </div>
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB--> 
			</div>
		</div>
		<!-- END PAGE HEADER--> 
		<!-- BEGIN PAGE CONTENT-->
		<div class="row"> 
			<!--row-->
			<div class="col-md-6"> 
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="portlet box blue">
					<div class="portlet-title">
						<div class="caption"> <i class="fa fa-reorder"></i> Manage Sub Apps </div>
						<div class="tools"> <a href="" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="" class="reload"> </a> <a href="" class="remove"> </a> </div>
					</div>
					<div class="portlet-body form">
						<form name="myForm" id="fsunm" action="<?php echo base_url();?>index.php/master/add_assign_smenu" method="post"  class="form-horizontal" onsubmit="return validateForm()">
							<div class="form-body">
								
								<div class="form-group">
									<label class="col-md-3 control-label">Designation Name</label>
									<div class="col-md-9">
										<select id="desig" name="desig" class="form-control input-large" onchange="menu()">
											<option value="-1"> Select </option>
											<?php 
												foreach($designation->result() as $desig)
												{
												?>
												<option value="<?php echo $desig->m_des_id;?>"><?php echo $desig->m_des_name;?></option>
												<?php
												}
											?>
										</select>
										
									</div>
								</div>
								
								<div id="menu12">
									<div class="form-group">
										<label class="col-md-3 control-label">Apps Name</label>
										<div class="col-md-9">
											<select id="main_menu" name="main_menu" class="form-control input-large" onchange="submenu()">
												<option value="-1"> Select </option>
											</select>
										</div>
									</div>
								</div>
								
								
								<div id="submenu">
									<div class="form-group">
										<label class="col-md-3 control-label">Subapps Name</label>
										<div class="col-md-9">
											<select id="sub_menu" name="sub_menu" class="form-control input-large">
												<option value="-1"> Select </option>
											</select>
										</div>
									</div>
								</div>
								
								
								<div class="form-group">
									<label class="col-md-3 control-label">Sub Apps IsLink</label>
									<div class="col-md-9">
										<div class="radio-list">
											<label class="radio-inline">
											<div class="radio" id="uniform-optionsRadios25"><input type="radio"  name="link1" id="link1" checked="checked" onClick="linkstatus()"></div> Enable </label>
											<label class="radio-inline">
											<div class="radio" id="uniform-optionsRadios26"><input type="radio" name="link1" id="link2" onClick="linkstatus()"></div> Disable </label>
										</div>
										<input type="hidden" value="1" id="txtislink" name="txtislink" />
									</div>
								</div>
								
								
								
								<div class="form-group">
									<label class="col-md-3 control-label">Status</label>
									<div class="col-md-9">
										<div class="radio-list">
											<label class="radio-inline">
											<div class="radio" id="uniform-optionsRadios25"><input type="radio"  name="optionsRadios1" id="optionsRadios1" checked="checked" onClick="check()"></div> Enable </label>
											<label class="radio-inline">
											<div class="radio" id="uniform-optionsRadios26"><input type="radio" name="optionsRadios1" id="optionsRadios2" onClick="check()"></div> Disable </label>
										</div>
										<input type="hidden" value="1" id="txtstatus" name="txtstatus" />
									</div>
								</div>
								
							</div>
							
							<div class="form-actions fluid">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn blue">Submit</button>
									<button type="button" class="btn">Cancel</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- END SAMPLE FORM PORTLET--> 
			
			
			
			
			
			
			
			<div class="col-md-6"> 
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box blue">
					<div class="portlet-title">
						<h4><i class="icon-edit"></i>VIEW & MODIFY </h4>
						<div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#portlet-config" data-toggle="modal" class="config"></a> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
					</div>
					<div class="portlet-body">
						
						<table class="table table-striped table-hover table-bordered" id="sample_2">
							<thead>
								<tr>
									<th>S No.</th>
									<th>Designation Name</th>
									<th>Apps Name</th>
									<th>subapps Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									$sn=1;
									$desig="";$menu="";$submenu="";
									foreach($assigns->result() as $assrow)
									{
										$status=0;
										if($assrow->m_dsm_status==1)
										{
											$status="Enable";
										}
										if($assrow->m_dsm_status==0)
										{
											$status="Disable";
										}
										foreach($assign->result() as $asrow)
										{
											if($asrow->m_menu_id==$assrow->m_dm_id)
											{
												
												
												foreach($desig1->result() as $derow)
												{
													if($asrow->m_des_id==$derow->m_des_id)
													{
														$desig=$derow->m_des_name;
														//break;
													}
												}
												foreach($menu1->result() as $merow)
												{
													if($asrow->m_menu_id==$merow->m_menu_id)
													{
														$menu=$merow->m_menu_name;
													}
												}
												foreach($smenu->result() as $smrow)
												{
													if($assrow->m_submenu_id==$smrow->m_submenu_id)
													{
														$submenu=$smrow->m_submenu_name;
													}
												}
											}
										}
									?>
									<tr>
										<td><?php echo $sn;?></td>
										<td><?php echo $desig;?></td>
										<td><?php echo $menu;?></td>
										<td><?php echo $submenu;?></td>
										<td>
											<a href="" onClick="edit(<?php echo $assrow->m_dsm_id;?>)" class="medium grey modalopen">
												<button type="button" class="btn btn-xs green">Edit</button>
												<?php 
													if($status=="Enable")
													{
													?>
													<a href="<?php echo base_url();?>index.php/master/stch_assmenu/<?php echo $assrow->m_dsm_id;?>/0">
														<button type="button" class="btn btn-xs red">Disable</button>
													</a>
													<?php
													}
													if($status=="Disable")
													{
													?>
													<a href="<?php echo base_url();?>index.php/master/stch_assmenu/<?php echo $assrow->m_dsm_id;?>/1">
														<button type="button" class="btn btn-xs purple">Enable</button>
														</a><?php
													}
												?>
											</td>
										</tr>
									<?php } ?>
								</tbody>
								
							</table>
						</div>
					</div>
				</div>
				
				
				
				
				
				
				
				<!-- END PAGE CONTENT--> 
			</div>
		</div>
		<!-- END CONTENT --> 
	</div>
	<!-- END CONTAINER --> 
	<script>
		function menu()
		{
			var desig=$('#desig').val();
			$("#menu12").html('<div><img src ="<?php echo base_url();?>application/libraries/assets/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
			$("#menu12").load("<?php echo base_url();?>index.php/master/select_menu/"+desig+"/0");
		}
	</script>
	
	
	<script>
		function submenu()
		{
			var menu=$('#main_menu').val();
			$("#submenu").html('<div><img src ="<?php echo base_url();?>application/libraries/assets/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
			$("#submenu").load("<?php echo base_url();?>index.php/master/select_submenu2/"+menu+"/0");
		}
	</script>
	
