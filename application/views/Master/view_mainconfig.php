<?php
	foreach($config->result() as $row)
	{
		break;
	}
?>
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Configuration</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-cogs"></i>
						<span>Manage Configuration</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Software Configuration</h3>
			<!-- END PAGE TITLE-->
			
			
			<div class="row">
				<div class="col-md-7">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-purple"></i>
								<span class="caption-subject font-purple bold uppercase">Software Configuration</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							
							<div class="tabbable-custom nav-justified">
								<ul class="nav nav-tabs nav-justified">
									<li class="active">
										<a href="#tab_1_1_1" data-toggle="tab">
											Primary Details
										</a>
									</li>
									<li>
										<a href="#tab_1_1_2" data-toggle="tab">
											Contact Details
										</a>
									</li>
									<li>
										<a href="#tab_1_1_3" data-toggle="tab">
											Login Settings
										</a>
									</li>
									<li>
										<a href="#tab_1_1_4" data-toggle="tab">
											Mail Setting
										</a>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="tab_1_1_1">
										<p>
											<div class="add-portfolio">	Change Primary Details:</div>
										</p>
										<form class="form-horizontal" role="form" method="post" action="<?php echo base_url().'master/update_mainconfig/1'?>" enctype="multipart/form-data" >
											<p>
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label class="col-md-4 control-label">Set Logo</label>
															<div class="col-md-8">
																<div class="fileinput fileinput-new" data-provides="fileinput">
																	<div class="fileinput-new thumbnail" style="width: 150px; height: 100px;">
																		<img src="<?php $img=($row->m00_sitelogo)?$row->m00_sitelogo:'-text.png'; echo base_url().'/application/logo/'.$img; ?>" alt=""/> 
																	</div>
																	<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 150px; max-height: 100px;"> 
																	</div>
																	<div>
																		<span class="btn default btn-file btn-xs">
																			<span class="fileinput-new">
																				Select image
																			</span>
																			<span class="fileinput-exists">
																				Change
																			</span>
																			<input type="file" name="userfile" id="userfile">
																		</span>
																		<a href="#" class="btn  btn-xs default fileinput-exists" data-dismiss="fileinput">
																			Remove
																		</a>
																	</div>
																</div>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Project Title</label>
															<div class="col-md-6">
																<input type="text" id="txtprojname" name="txtprojname"  class="form-control input-sm empty" placeholder="Enter Project Title." value="<?php echo $row->m00_sitename;?>">
																
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Website</label>
															<div class="col-md-6">
																<input type="text" id="txtwebsite" name="txtwebsite"  class="form-control input-sm empty" placeholder="Enter Website." value="<?php echo $row->m00_website;?>">
																
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Description</label>
															<div class="col-md-6">
																<textarea id="txtdesc" name="txtdesc" rows="3" col="9" placeholder="Enter Project Description." class="form-control input-sm empty"><?php echo $row->m00_description;?></textarea>
																
															</div>
														</div>
													</div>
												</div>
											</p>
											<p>
												<div class="row">
													<div class="form-actions fluid">
														<div class="col-md-offset-4 col-md-8">
															<button type="submit" class="btn green">Update</button>
															<button type="reset" class="btn default">Cancel</button>
														</div>
													</div>
												</div> 
											</p>
										</form>
									</div>
									<div class="tab-pane" id="tab_1_1_2">
										<form class="form-horizontal" role="form" method="post" action="<?php echo base_url().'master/update_mainconfig/2'?>" id="myform1" onsubmit="return check(conwv('myform1'))">
											<p>
												<div class="add-portfolio"> Change Contact Details:</div>
											</p>
											<p>
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label class="col-md-4 control-label">Primary Email Address</label>
															<div class="col-md-8">
																<input type="text" id="txtemail" name="txtemail" style="width:72%" readonly="true" class="form-control input-inline input-sm empty" placeholder="Enter Email Address." value="<?php echo $row->m00_email;?>">
																<strong><a href="javascript:void(0);" id="add_more1" class="addCF "><i class="fa fa-plus"></i>&nbsp;Add More</a></strong>
															</div>
														</div>
														
														<div class="form-group">
															<label class="col-md-4 control-label">Primary Email Password</label>
															<div class="col-md-8">
																<input type="password" id="txtemailpassword" name="txtemailpassword" style="width:72%" class="form-control input-inline input-sm empty" placeholder="Enter Email Password." value="<?php echo $row->m00_email_pwd;?>">
																<span id="divtxtemailpassword" style="color:red" ></span>
															</div>
														</div>
														
														<div id="add_new1">
															<?php 
																$j=0;
																if($row->m00_secondary_emails !='')
																{
																	$email=$row->m00_secondary_emails;
																	$email=explode(",",$email);
																	for($j=0;$j<count($email);$j++)
																	{
																	?>
																	<div class="form-group">
																		<label class="col-md-4 control-label">Secondary Email</label>
																		<div class="col-md-8">
																			<input type="text" id="txtemail<?php echo $j+1; ?>" style="width:72%" name="txtemail[]" class="form-control input-inline input-sm empty" value="<?php echo $email[$j];?>" placeholder="Enter Secondary Email." >
																			<strong><a href="javascript:void(0);" class="remCF"><i class="fa fa-times"></i>&nbsp;Remove</a></strong>
																			<span id="divtxtemail'+maxAppend+'" style="color:red;"></span>
																		</div>
																	</div>
																	<?php
																	}
																	$j++;
																}
															?>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Address Details</label>
															<div class="col-md-6">
																<textarea id="txtadd" name="txtadd" rows="3" col="9" placeholder="Enter Address Details" class="form-control input-sm empty"><?php echo $row->m00_address;?></textarea>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Primary Contact Number</label>
															<div class="col-md-8">
																<input type="text" id="txtcontact" name="txtcontact"  style="width:72%" readonly="true"  class="form-control input-inline input-sm empty" placeholder="Enter Contact Number." value="<?php echo $row->m00_primary_contact;?>" maxlength="10">
																<strong><a href="javascript:void(0);" id="add_more" class="addCF "><i class="fa fa-plus"></i>&nbsp;Add More</a></strong>
																
															</div>
														</div>
														<div id="add_new">
															<?php 
																$i=0;
																if($row->m00_contact !='')
																{
																	$contact=$row->m00_contact;
																	$contact=explode(",",$contact);
																	for($i=0;$i<count($contact);$i++)
																	{
																	?>
																	<div class="form-group">
																		<label class="col-md-4 control-label">Contact Number</label>
																		<div class="col-md-8">
																			<input type="text" id="txtcontact<?php echo $i+1; ?>" style="width:72%" name="txtcontact[]" maxlength="10" class="form-control input-inline input-sm empty" value="<?php echo $contact[$i];?>" placeholder="Enter Contact Number." value="">
																			<strong><a href="javascript:void(0);" class="remCF"><i class="fa fa-times"></i>&nbsp;Remove</a></strong>
																			<span id="divtxtcontact'+maxAppend+'" style="color:red;"></span>
																		</div>
																	</div>
																	<?php
																	}
																	$i++;
																}
															?>
														</div>
														
														<div class="form-group">
															<label class="col-md-4 control-label">SMS UserID</label>
															<div class="col-md-8">
																<input type="text" id="txtsmsuserid" name="txtsmsuserid" style="width:72%" class="form-control input-inline input-sm empty" placeholder="Enter SMS UserID." value="<?php echo $row->m00_sms_uername;?>">
																<span id="divtxtsmsuserid" style="color:red" ></span>
															</div>
														</div>
														
														<div class="form-group">
															<label class="col-md-4 control-label">SMS Password</label>
															<div class="col-md-8">
																<input type="password" id="txtsmspassword" name="txtsmspassword" style="width:72%" class="form-control input-inline input-sm empty" placeholder="Enter SMS Password." value="<?php echo $row->m00_sms_pwd;?>">
																<span id="divtxtsmspassword" style="color:red" ></span>
															</div>
														</div>
														
													</div>
												</div>
											</p>
											<p>
												<div class="row">
													<div class="form-actions fluid">
														<div class="col-md-offset-4 col-md-8">
															<button type="submit" class="btn green">Update</button>
															<button type="reset" class="btn default">Cancel</button>
														</div>
													</div>
												</div>
											</p>
										</form>
									</div>
									<div class="tab-pane" id="tab_1_1_3">
										<form class="form-horizontal" role="form" method="post" action="<?php echo base_url().'master/update_mainconfig/3'?>" id="myform" onsubmit="return check(conwv('myform'))">
											<p><div class="add-portfolio"> Change User name and Password:</div></p>
											<p>
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label class="col-md-4 control-label">User Name</label>
															<div class="col-md-6">
																<input type="text" id="txtusername" name="txtusername" class="form-control input-sm empty" placeholder="Enter User Name." value="<?php echo $row->m00_username;?>">
																
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">New Login Password</label>
															<div class="col-md-6">
																<input type="password" id="txtuserpass" name="txtuserpass" class="form-control input-sm empty" placeholder="Enter New Login Password." value="">
																<span id="divtxtuserpass" style="color:red;"></span>
															</div>
															
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Confirm New Login Password</label>
															<div class="col-md-6">
																<input type="password" id="txtuserpinpass" name="txtuserpinpass" class="form-control input-sm empty" placeholder="Enter Confirm New Login Password." value="">
																<span id="divtxtuserpinpass" style="color:red;"></span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Old Login Password</label>
															<div class="col-md-6">
																<input type="password" id="txtolduserpass" name="txtolduserpass" class="form-control input-sm empty" placeholder="Enter Old Login Password." value="<?php echo $row->m00_password;?>">
																<span id="divtxtolduserpass" style="color:red;"></span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Send OTP</label>
															<div class="col-md-8">
																<input type="checkbox" checked class="make-switch switch-mini" data-on="warning" data-size="small" name="chkotp">
																
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Send Login Email Alert</label>
															<div class="col-md-8">
																<input type="checkbox" checked class="make-switch switch-mini" data-size="small" name="chkemail">
																
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Send Login SMS Alert</label>
															<div class="col-md-8">
																<input type="checkbox" checked class="make-switch switch-mini" data-on="danger" data-size="small" name="chksms">
																
															</div>
														</div>
													</div>
												</div>
											</p>
											<p>
												<div class="row">
													<div class="form-actions fluid">
														<div class="col-md-offset-4 col-md-8">
															<button type="submit" class="btn green">Update</button>
															<button type="reset" class="btn default">Cancel</button>
														</div>
													</div>
												</div> 
											</p>
											
										</form>
									</div>
									
									
									<div class="tab-pane" id="tab_1_1_4">
										<form class="form-horizontal" role="form" method="post" action="<?php echo base_url().'CRM/update_mail_config/'?>" id="mymailform" >
											<p><div class="add-portfolio"> Mail configaretion Setting:</div></p>
											<p>
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label class="col-md-4 control-label">Host Name</label>
															<div class="col-md-6">
																<input type="text" name="txthost_name" id="txthost_name" value="<?=POP3_HOST?>" class="form-control input-sm empty" />
																<span id="divtxthost_name" style="color:red;"></span> 
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Port Address</label>
															<div class="col-md-6">
																<input  type="text" name="txtportadd" id="txtportadd" value="<?=POP3_PORT?>" class="form-control numeric" />
																<span id="divtxtportadd" style="color:red;"></span>
															</div>
															
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Email ID</label>
															<div class="col-md-6">
																<input type="text" name="txtpopemail" id="txtpopemail" value="<?=PO3_EMAIL?>" class="form-control input-sm empty"/>
																<span id="divtxtpopemail" style="color:red;"></span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-4 control-label">Password</label>
															<div class="col-md-6">
																<input type="password" name="txtpwd1" id="txtpwd1" value="<?=POP3_PWD?>" class="form-control input-sm empty"/>
																<span id="divtxtpwd1" style="color:red;"></span> </div>
															</div>
														</div>
														
													</div>
												
											</p>
											<p>
												<div class="row">
													<div class="form-actions fluid">
														<div class="col-md-offset-4 col-md-8">
															<button type="button" onclick="return check(conwv('mymailform'))" class="btn green">Update</button>
															<button type="reset" class="btn default">Cancel</button>
														</div>
													</div>
												</div> 
											</p>
											
										</form>
									</div>
									
									
								</div>
							</div>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				<div class="col-md-6" style="display:none;">
					<div class="portlet box green ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Software Configuration
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
						
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" action="<?php echo base_url().'master/update_mainconfig/'?>" >
								<div class="form-body" id="rank">
									
									<!---------------------------------------------------->
									
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-4 col-md-8">
										<button type="submit" class="btn green">Update</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->


<script type="text/javascript">
	$(document).ready(function() {
		/*------------Add More Contact Numbers----------------*/
		var maxAppend = <?php echo $i;?>;
		$("#add_more").click(function() {
			if (maxAppend >= 5)
			{
				alert("Maximum 5 Contacts are allowed");
				} else {
				var add_new = $('<div class="form-group">\n\
				<label class="col-md-4 control-label">Contact Number</label>\n\
				<div class="col-md-8">\n\
				<input type="text" id="txtcontact'+maxAppend+'" style="width:72%" name="txtcontact[]" maxlength="10" class="form-control input-inline input-sm empty" placeholder="Enter Contact Number." value="">\n\
				<strong><a href="javascript:void(0);" class="remCF"><i class="fa fa-times"></i>&nbsp;Remove</a></strong>\n\
				<span id="divtxtcontact'+maxAppend+'" style="color:red;"></span>\n\
				</div>\n\
				</div>');
				maxAppend++;
				$("#add_new").append(add_new);
			}
			
		});
		
		$("#add_new").on('click', '.remCF', function() {
			maxAppend--;
			$(this).parent().parent().parent().remove();
		});
		
		/*-------------Add More Secondary Emails ----------------------*/
		var maxAppend2 = <?php echo $j;?>;
		$("#add_more1").click(function() {
			if (maxAppend2 >= 5)
			{
				alert("Maximum 5 Contacts are allowed");
				} else {
				var add_new = $('<div class="form-group">\n\
				<label class="col-md-4 control-label">Secondary Email</label>\n\
				<div class="col-md-8">\n\
				<input type="text" id="txtemail'+maxAppend2+'" style="width:72%" name="txtemail[]" class="form-control input-inline input-sm empty" placeholder="Enter Secondary Email." value="">\n\
				<strong><a href="javascript:void(0);" class="remCF"><i class="fa fa-times"></i>&nbsp;Remove</a></strong>\n\
				<span id="divtxtemail'+maxAppend2+'" style="color:red;"></span>\n\
				</div>\n\
				</div>');
				maxAppend2++;
				$("#add_new1").append(add_new);
			}
			
		});
		
		$("#add_new1").on('click', '.remCF', function() {
			maxAppend2--;
			$(this).parent().parent().parent().remove();
		});
	});
	
	
	
</script>
<script type="text/javascript">
	
	function confirmation()
	{
		bootbox.confirm("Are you sure?", function(result) {
			if(result==true)
			{  
				return true ;
			}
		});		
		
	}
</script>		
<script type="text/javascript">
	function validateForm()
	{
		var x=document.forms["myForm"]["txtstate"].value;
		var y=document.forms["myForm"]["txtcity"].value;
		if(x!=null && x!="" && x!=-1)
		{
			$("#divtxtstate").html('');
			if(y!=null && y!="")
			{
				return true;
			}
			else
			{
				document.forms["myForm"]["txtcity"].focus();
				$("#divtxtcity").html('Please Enter City');
				return false;
			}
		}
		else
		{
			document.forms["myForm"]["txtstate"].focus();
			$("#divtxtstate").html('Please Select State');
			return false;
		} 
		
	}
	
</script>

