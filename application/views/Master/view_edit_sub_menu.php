<script>
	function check()
	{
		var collection=$("#fsunm");
		var mark=0;
		var obtainmark=0;
		var inputs=collection.find("input[type=checkbox],input[type=radio]");
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			var name=inputs[x].name;
			if($("#"+id+"").is(':checked'))
			{
				if(id=="optionsRadios1")
				{
					$("#txtstatus").val('1');
				}
				if(id=="optionsRadios2")
				{
					$("#txtstatus").val('0');
				}
			}
		}
	}
	function fill_city()
	{
		
		var state_id=$("#txtstate").val();
		$("#city").html('<div><img src ="<?php echo base_url();?>application/lib_images/loading.gif" alt="Loading....." title="Loading...."></div>');
		$("#city").load("<?php echo base_url();?>index.php/master/select_city/"+state_id);
	}
	
</script>
<?php
	foreach($smenu->result() as $row)
	{
	}
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content"> 
		
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12"> 
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title"> Admin Panel </h3>
				<ul class="page-breadcrumb breadcrumb">
					<li> <i class="fa fa-home"></i> <a href="#"> Dashboard </a> <i class="fa fa-angle-right"></i> </li>
					<li> <a href="#"> Sub Apps </a> </li>
					<li class="pull-right">
						<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" > <i class="fa fa-calendar"></i> <span> </span> </div>
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB--> 
			</div>
		</div>
		<!-- END PAGE HEADER--> 
		<!-- BEGIN PAGE CONTENT-->
		<div class="row"> 
			<!--row-->
			<div class="col-md-6"> 
				<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="portlet box blue">
					<div class="portlet-title">
						<div class="caption"> <i class="fa fa-reorder"></i> Manage Sub Apps </div>
						<div class="tools"> <a href="" class="collapse"> </a> <a href="#portlet-config" data-toggle="modal" class="config"> </a> <a href="" class="reload"> </a> <a href="" class="remove"> </a> </div>
					</div>
					<div class="portlet-body form">
						<form name="myForm" id="fsunm" action="<?php echo base_url();?>index.php/master/edit_submenu/<?php echo $id; ?>" method="post"  class="form-horizontal" onsubmit="return validateForm()">
							<div class="form-body">
								<div class="form-group">
									<label class="col-md-3 control-label">Apps Name</label>
									<div class="col-md-9">
										<select id="main_menu" name="main_menu" class="form-control input-large">
											<option value="-1">Select Main Apps</option>
											<?php foreach($menu->result() as $row1)
												{
													if($row->m_menu_id==$row1->m_menu_id)
													{
													?>
													<option value="<?php echo $row1->m_menu_id;?>" selected="selected"><?php echo $row1->m_menu_name;?></option>
													<?php
													}
													else
													{
													?>
													<option value="<?php echo $row1->m_menu_id;?>"><?php echo $row1->m_menu_name;?></option>
													<?php
													}
												}?>
										</select>
										
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Sub Apps Name</label>
									<div class="col-md-9">
										<input type="text" id="txtsname" name="txtsname" value="<?php echo $row->m_submenu_name;?>" class="form-control input-large" />
										
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Sub Apps Link</label>
									<div class="col-md-9">
										<input type="text" id="txtslink" name="txtslink" value="<?php echo $row->m_sbcatlink;?>" class="form-control input-large" />
										
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Sub Apps Description</label>
									<div class="col-md-9">
										<textarea rows="5" id="txtsdesc" name="txtsdesc" class="form-control input-large"><?php echo $row->m_submenu_desc;?></textarea>
										
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label"> Sub Apps Status</label>
									<div class="col-md-9">
										<div class="radio-list">
											<label class="radio-inline">
											<div class="radio" id="uniform-optionsRadios25"><input type="radio"  name="optionsRadios1" id="optionsRadios1" checked="checked" onClick="check()"></div> Enable </label>
											<label class="radio-inline">
											<div class="radio" id="uniform-optionsRadios26"><input type="radio" name="optionsRadios1" id="optionsRadios2" onClick="check()"></div> Disable </label>
											
										</div>
										<input type="hidden" value="1" id="txtstatus" name="txtstatus" />
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn blue">Update</button>
									<button type="button" class="btn">Cancel</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET--> 
			</div>
			<!-- END FORM CONTENT-->
			
			<div class="col-md-6"> 
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box blue">
					<div class="portlet-title">
						<h4><i class="icon-edit"></i>VIEW & MODIFY </h4>
						<div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#portlet-config" data-toggle="modal" class="config"></a> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
					</div>
					<div class="portlet-body">
						<div class="clearfix">
							
						</div>
						<table class="table table-striped table-hover table-bordered" id="sample_2">
							<thead>
								<tr>
									<th>Apps</th>
									<th>Sub Apps</th>
									<th>Sub Apps Link</th>
									<th>Action</th>
									
								</tr>
							</thead>
							<tbody>
								<?php 
									$mmenu="";
									
									foreach($submenu->result() as $row1)
									{$status=0;
										if($row1->m_submenu_status==1)
										{
											$status="Enable";
										}
										if($row1->m_submenu_status==0)
										{
											$status="Disable";
										}
										foreach($menu->result() as $row)
										{
											if($row->m_menu_id==$row1->m_menu_id)
											{
												$mmenu=$row->m_menu_name;
											}
										}
									?>
									<tr>
										<td><?php echo $mmenu; ?></td>
										<td><?php echo $row1->m_submenu_name;?></td>
										<td><?php echo $row1->m_sbcatlink;?></td>
										<td><a href="<?php echo base_url();?>index.php/master/edit_smenu/<?php echo $row1->m_submenu_id;?>" rel="facebox">
										<span class="btn yellow btn-xs">Edit</span></a>
										<?php 
											if($status=="Enable")
											{
											?>
											<a href="<?php echo base_url();?>index.php/master/st_chsubmst/<?php echo $row1->m_submenu_id;?>/0">
												<span class="btn red btn-xs">Disable</span>
											</a>
											<?php
											}
											if($status=="Disable")
											{
											?>
											<a href="<?php echo base_url();?>index.php/master/st_chsubmst/<?php echo $row1->m_submenu_id;?>/1">
												<span class="btn green btn-xs">Enable</span>
												</a><?php
											}
										?>
										</td>
									</tr>
									<?php
									}
								?>
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT--> 
		</div>
	</div>
	<!-- END CONTENT --> 
</div>
<!-- END CONTAINER --> 
<script type="text/javascript">
	function validateForm()
	{
		var x=document.forms["myForm"]["main_menu"].value;
		var y=document.forms["myForm"]["txtsname"].value;
		var z=document.forms["myForm"]["txtslink"].value;
		if(x!=null && x!="" && x!=-1)
		{
			$("#divmain_menu").html('');
			if(y!=null && y!="")
			{
				$("#divtxtsname").html('');
				if(z!=null && z!="")
				{
					$("#divtxtslink").html('');
					$("#divtxtsname").html('');
					$("#divmain_menu").html('');
					return true;
				}
				else
				{
					document.forms["myForm"]["txtslink"].focus();
					$("#divtxtslink").html('Please Enter Sub Apps Link');
					return false;
				}
			}
			else
			{
				document.forms["myForm"]["txtsname"].focus();
				$("#divtxtsname").html('Please Enter Sub Apps Name');
				return false;
			}
		}
		else
		{
			document.forms["myForm"]["main_menu"].focus();
			$("#divmain_menu").html('Please Select Main Apps');
			return false;
		} 
		
	}
</script>										