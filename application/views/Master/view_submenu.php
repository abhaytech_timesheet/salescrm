<div class="form-group">
	<label class="col-md-3 control-label">Subapps Name</label>
	<div class="col-md-9">
		<select id="sub_menu" name="sub_menu" class="form-control input-large">
			<option value="-1"> Select </option>
			<?php 
				foreach($submenu->result() as $rec)
				{
				?>
				<option value="<?php echo $rec->m_submenu_id; ?>"> <?php echo $rec->m_submenu_name; ?> </option>
			<?php } ?>
		</select>
	</div>
</div>