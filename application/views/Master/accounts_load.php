<?php 
	foreach($details->result() as $rows)
	{
		break;
	}
?>
	
	<div class="portlet-body form">
        <div class="form-body">
            <div class="form-group">
				
                <label>General Ledger A/C No.</label>
                <input type="text" class="form-control input-large alpha_numeric" id="txtgeneralled" name="txtgeneralled" placeholder="Enter General Ledger A/C No" value="<?php echo $rows->m_acc_num; ?>" />
                <span  id="divtxtgeneralled" style="color:red;"></span>
			</div>
            
            <div class="form-group">
                <label>General Ledger A/C Name</label>
                <input type="text" class="form-control input-large" id="txtgeneralname" name="txtgeneralname" placeholder="Enter General Ledger A/C Name" value="<?php echo $rows->m_acc_name; ?>" />
                
			</div>
            
            <div class="form-group">
                <label>Sub Ledger A/C No.</label>
                <input type="text"  id="txtsubled" class="form-control input-large alpha_numeric"  name="txtsubled" placeholder="Enter Sub Ledger A/C No."/>
                <span  id="divtxtsubled" style="color:red;"></span>
			</div>
            
            <div class="form-group">
                <label>Sub Ledger A/C Name</label>
                <input type="text" class="form-control input-large"  id="txtsubledname" name="txtsubledname" placeholder="Enter Sub Ledger A/C Name."/>
                
			</div>
			<input type="hidden" class="form-control input-large" id="txtparent" name="txttitle" placeholder="Enter text" value="<?php echo $rows->m_acc_id; ?>" />
			<input type="hidden" class="form-control input-large" id="txtmain_acc" name="txttitle" placeholder="Enter text" value="<?php echo $rows->m_main_acc_id; ?>" />
			
		</div>
		<div class="form-actions">
        	<button type="button" class="btn default">Cancel</button>
			<button type="button" class="btn green" onclick="insert()"><i class="fa fa-check"></i> Save</button>
		</div>	
	</div>
</div>


<script>
	function insert()
	{
		if(check())
{
		$("#function").html("<div>Account has been added.<img id='checkmark' src='<?php echo base_url(); ?>/application/libraries/assets/img/loading-spinner-blue.gif' /></div>");
		var m_acc_name =$('#txtsubledname').val();
		var m_acc_num=$('#txtsubled').val();
		var m_acc_descrip=$('#txtsubledname').val();
		var m_acc_parent=$('#txtparent').val();
		var m_main_acc_id=$('#txtmain_acc').val();
		$.ajax(
        {
			type:"POST",
			url:"<?php echo base_url().'index.php/master/insert_account/'?>",
			dataType:'json',
			data:{'txtsubledname':m_acc_name,
				'txtsubled':m_acc_num,
				'txtsubledname':m_acc_descrip,
				'm_acc_id':m_acc_parent,
				'm_main_acc_id':m_main_acc_id
				},
			success: function(msg) {
				if(msg!="")
				{
					$(".page-content").html("<center><h2>Account has been added</h2><p></p></center>")
					.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/img/loading-spinner-blue.gif' /></center>")
					.append("<center><a href='<?php echo base_url();?>index.php/master/account/'>Back</a></center>");
				}
				else
				{
					alert("Some Error on this page");
				}
			}
		});
		}
	}
	</script>		
<script src="<?php echo base_url();?>application/libraries/validation.js"></script>