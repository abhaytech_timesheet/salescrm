<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Configuration</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Manage Service Providers</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Service Providers</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Service Providers</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" id="insert_data" action="<?php echo base_url().'index.php/master/add_srv_provider/'?>" method="post" onsubmit="return check(conwv('myform'))">
								<div class="form-body" id="city">
									<div class="form-group">
										<label class="col-md-3 control-label">Select Service Type</label>
										<div class="col-md-6">
											<select id="ddservice_prov" name="ddservice_prov" class="form-control input-sm opt">
												<option selected="selected" value="-1">Select Service</option>
												<?php 
													foreach($services->result() as $row)
													{
														
													?><option value="<?php echo $row->Service_type_id	;?>"><?php echo $row->Service_type;?></option>
													<?php
													}
												?>
											</select>
											<span id="divddstate" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Operator Name</label>
										<div class="col-md-6">
											<input type="text" id="txtoptname" name="txtoptname" class="form-control input-sm empty" placeholder="Enter Service Name" id="myform" >
											<span id="divtxtservice" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Operator Code</label>
										<div class="col-md-6">
											<input type="text" id="txtoptcode" name="txtoptcode" class="form-control input-sm empty" placeholder="Enter Service Name" id="myform" >
											<span id="divtxtservice" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Status</label>
										<div class="col-md-6">
											<div class="radio-list">
												<label class="radio-inline">
												<input type="radio" name="rbstatus" id="rbrbstatus1" value="1" checked> Enable</label>
												<label class="radio-inline">
												<input type="radio" name="rbstatus" id="rbrbstatus2" value="0"> Disable</label>
											</div>
										</div>
									</div>
									
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="button" onclick="conwv('insert_data')" class="btn green">Submit</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">View Service Provider</span>
							</div>
							<div class="tools"> 
								<button class="btn btn-sm blue" onclick="change_status('master','stch_sp')">Enable<i class="fa fa-check"></i></button>
							</div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover table-checkable" id="sample_1">
								<thead>
									<tr>
										<th>
											<input type="checkbox" id="checkAll" onclick="chbcheckall()" class="group-checkable">
										</th>
										<th>SNo.</th>
										<th>Service</th>
										<th>Operator</th>
										<th>Code</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="userid">
									<?php 
										$sn=1;
										foreach($sp->result() as $row)
										{?>
											<tr>
												<td>
												<?php 
													if($row->Sp_status==0)
													{
													?>
														<input type="checkbox" class="checkboxes" id="<?php echo $row->Sp_id;?>" onclick="chbchecksin()" >
													<?php
													}
													else if($row->Sp_status==1)
													{
													?>
														<input type="checkbox" class="checkboxes" id="<?php echo $row->Sp_id;?>" onclick="chbchecksin()" checked>
													<?php
													}
												?>
												</td>
												<td><?php echo $sn++;?></td>
												<td><?php echo $row->Service_type; ?></td>
												<td><?php echo $row->Sp_name; ?></td>
												<td><?php echo $row->Sp_code;?></td>
												<td>
													<a href="<?php echo base_url();?>index.php/master/edit_service_opt/<?php echo $row->Sp_id;?>"><i class="icon-note"></i> Edit </a>
													
												</td>
											</tr>
											<?php
										}
									?>
									
								</tbody>
							</table>
							<input type="hidden" id="txtquid" name="txtquid">
							<input type="hidden" id="txtunchk" name="txtunchk">
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													



<script src="<?php echo base_url() ?>application/libraries/js/checkboxscipt.js"></script>