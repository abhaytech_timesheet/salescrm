<div class="portlet-body form">
	<form action="<?php echo base_url(); ?>index.php/master/insert_apps" method="post" id="myform" onsubmit="return check(conwv('myform'))">
		<div class="form-body">
			<div class="form-group">
				<label>Sub Apps Name</label>
				<input type="text" class="form-control input-sm empty" id="txtname" name="txtname" placeholder="Enter Sub Apps Name" />
				<span id="divtxtname" style="color:red"></span>
			</div>
			
			<div class="form-group">
				<label>Sub Apps Icon</label>
				<input type="text" class="form-control input-sm empty" id="txticon" name="txticon" placeholder="Enter Sub Apps Icon." />
				<span id="divtxticon" style="color:red"></span>
			</div>
			
			<div class="form-group">
				<label>Sub Apps Serial No</label>
				<input type="text" class="form-control input-sm empty" id="txtserial" name="txtserial" placeholder="Enter Sub Apps Serial No." />
				<span id="divtxtserial" style="color:red"></span>
			</div>
			
			<div class="form-group">
				<label>Sub Apps Url</label>
				<input type="text" class="form-control input-sm empty" id="txturl" name="txturl" placeholder="Enter Sub Apps Url." />
				<input type="hidden" class="form-control input-sm empty" id="txtparent" name="txtparent" value="<?php echo $parent_id; ?>" />
				<span id="divtxturl" style="color:red"></span>
			</div>
			
			<div class="form-group">
				<label>Sub Apps Description</label>
				<input type="text" class="form-control input-sm empty" id="txtdes" name="txtdes" placeholder="Enter Sub Apps Description." />
				<span id="divtxtdes" style="color:red"></span>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label">Is Link</label>
				<div class="col-md-7">
					<div class="radio-list">
						<label class="radio-inline">
							<input type="radio"  name="optionsRadios1" id="optionsRadios1" onClick="check_radio(1)">
							Enable
						</label>
						<label class="radio-inline">
							<input type="radio" name="optionsRadios1" id="optionsRadios2" checked="checked" onClick="check_radio(0)">
							Disable
						</label>
					</div>
					<input type="hidden" value="1" id="txtsubcate" name="txtsubcate" />
				</div>
			</div>
			
		</div>
		
		<div class="form-actions">
			<div class="col-md-9 col-md-offset-3">
				<button type="submit" class="btn green">Submit</button>
				<button type="reset" class="btn">Cancel</button>
			</div>
		</div>
	</form>
</div>				