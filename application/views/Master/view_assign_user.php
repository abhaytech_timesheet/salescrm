<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Configuration</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-sitemap"></i>
						<span>Assign App</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Assign Apps To User</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Details </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div id="div_load">
							<div class="portlet-body form">
								<form class="form-horizontal" action="<?php echo base_url(); ?>index.php/master/assign_apps_to_user" method="post" id="myform" >
									<div class="form-body">
										<div class="form-group">
											<label class="control-label col-md-4">Email Id</label>
											<div class="col-md-8">
												<input type="text" class="form-control input-sm" id="txtemail" name="txtemail" placeholder="Enter Email Id" />
												<span id="divtxtemail" style="color:red"></span>
											</div>
										</div>
										
									</div>
									
									<div class="form-actions">
										<div class="col-md-9 col-md-offset-3">
											<button type="button" onclick="redirectassign()" class="btn green">Submit</button>
											<button type="reset" class="btn">Cancel</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->

<script>
	function redirectassign()
	{
		var emailid=$("#txtemail").val();
		$.ajax(
        {
			type: "POST",
			url:"<?php echo base_url();?>master/check_user/",
			data:"emailid="+emailid,
			success: function(msg) {
                
				if(msg.trim()!="" && msg.trim()!=0)
				{
			       $(".page-content").html("<center><i class='fa fa-spinner fa-spin fa-5x fa-fw'></i></center>")
				    var userid=msg.trim();
					$.ajax(
					{
					type: "POST",
			        url:"<?php echo base_url();?>master/view_assign_apps/"+userid,
					data:"emailid="+emailid,
					success: function(data) {
					$("#stylized").html(data);
					}
					});
				}
				else
				{
					alert("Email id is invalid. Please check it.");
				}
			}
		});
		
	}
</script>
