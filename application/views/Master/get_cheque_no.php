<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label">Cheque/DD No
				<span class="required">
					*
				</span></label>
				<input type="text" name="txtchequeno" id="txtchequeno" class="form-control">										
		</div>
	</div>
	<!--/span-->
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label">Bank Name
				<span class="required">
					*
				</span></label>
				<input type="text" name="txtbnk_name" id="txtbnk_name" class="form-control">
		</div>
	</div>
	<!--/span-->
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label">Bank Address
				<span class="required">
					*
				</span></label>
				<input type="text" name="txtbnk_address" id="txtbnk_address" class="form-control">
		</div>
	</div>
</div>