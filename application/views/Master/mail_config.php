<div id="stylized"> 
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper"> 
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content"> 
			<!-- BEGIN PAGE HEADER--> 
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li> <i class="icon-globe"></i> <a href="#">Configuration</a> <i class="fa fa-angle-right"></i> </li>
					<li> <i class="fa fa-location-arrow"></i> <a href="#">Manage Email</a> <i class="fa fa-angle-right"></i> </li>
					<li> <i class="fa fa-edit"></i> <span>Edit Email</span> <i class="fa fa-angle-right"></i> </li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range"> <i class="icon-calendar"></i>&nbsp; <span class="thin uppercase hidden-xs"></span>&nbsp; <i class="fa fa-angle-down"></i> </div>
				</div>
			</div>
			<!-- END PAGE BAR --> 
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Mail Configaretion</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption"> <i class="icon-globe font-black"></i> <span class="caption-subject font-black bold uppercase">Mail Setting</span> </div>
							<div class="actions"> <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;"> <i class="icon-cloud-upload"></i> </a> <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;"> <i class="icon-wrench"></i> </a> <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a> <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;"> <i class="icon-trash"></i> </a> </div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" action="<?php echo base_url().'index.php/CRM/update_mail_config/' ?>" method="post" id="myform" >
								<div class="form-body" id="city">
									<div class="form-group">
										<label class="col-md-3 control-label">Host Name*</label>
										<div class="col-md-6">
											<input type="text" name="host" id="host_name" value="<?=POP3_HOST?>" class="form-control input-sm empty" />
										<span id="divhost_name" style="color:red;"></span> </div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Port Address *</label>
										<div class="col-md-6">
											<input type="text" name="port" id="port" value="<?=POP3_PORT?>" class="form-control numeric"/>
										<span id="divport" style="color:red;"></span> </div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Email ID*</label>
										<div class="col-md-6">
											<input type="text" name="email" id="txtemail" value="<?=PO3_EMAIL?>" class="form-control input-sm txtemail"/>
										<span id="divtxtemail" style="color:red;"></span> </div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label"> Password*</label>
										<div class="col-md-6">
											<input type="text" name="pwd" id="pwd" value="<?=POP3_PWD?>" class="form-control input-sm empty"/>
										<span id="divpwd" style="color:red;"></span> </div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="button" onclick="conwv('myform')" class="btn green">Update  Setting</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption"> <i class="icon-globe font-black"></i> <span class="caption-subject font-black bold uppercase">Get Mail Records</span> </div>
							
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" action="<?php echo base_url().'index.php/CRM/get_mail_records/' ?>" method="post">
								
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit"  class="btn green">Update Email Records</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT--> 
			</div>
		</div>
		<!-- END CONTENT BODY --> 
	</div>
</div>
<!-- END CONTENT -->