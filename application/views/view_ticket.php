<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Admin Panel 
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="#">Dashboard</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">VIEW TICKET</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
									<?php
													foreach($rec->result() as $row)
													{
														if(($row->Mail)!=null)
														{
														$tic=($row->Ticket_reference_no);
														break; 
														} 
													}
									?>
								<strong><?php echo '#'.$tic; ?></strong>
							</a>
							
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                	<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>View Ticket Detail
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							
								<table class="table table-striped table-bordered table-hover">
								<thead>
								<tr>
									<th>
										 Department
									</th>
									<th>
										 Subject
									</th>
									<th>
										 Status
									</th>
									<th>
										 Urgency
									</th>
									<th>
										 Date
									</th>
									
								</tr>
								</thead>
								<tbody>
                                
                                  <?php  
							
										foreach($rec->result() as $row)
										{
                                        }
											if(($row->Mail)!=null)
											{
												
													
								  ?>
                                  
								<tr class="odd gradeX">
									<td>
										 <?php echo $row->Department  ?>
									</td>
									<td>
										 <?php echo $row->Subject ?>
									</td>
									<td>
										 <?php echo $row->C_Status; ?>
									</td>
									<td>
										 <?php 
										 echo $row->Urgency;
										?>
									</td>
									<td>
										 <?php echo $row->Sub_date; ?>
									</td>
									</tr>
                                 <?php
										  }
								  ?>
								</tbody>
								</table>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
					
				</div>
			</div>
            <!--/row-->
            
            <div class="col-md-12">
            	<?php
							if($row->C_Status !="DEACTIVE")
							{
							?>
                 <form method="post" action="<?php echo base_url();?>index.php/admin/resolve" name="resolved" id="resolved" style="margin:25px 0px 0px 360px;">
                 <input type="hidden" value="<?php echo $tic ?>" id="ticket_no" name="ticket_no" />
                 <input type="hidden" value="0" id="txtC_Status" name="txtC_Status" />
                 <input type="submit" name="submit" value="Click Here If Problem Is Resolved" class="btn green" style="width:300px;" />
   				 </form>
                 <?php
				 }
				 ?>
            </div>
            <!--row-->
            <div class="col-md-12" style="height:30px;">
            
            </div>
            
            <div class="col-md-12">
                <div class="col-md-7 col-sm-7">
					<!-- BEGIN PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title line">
							<div class="caption">
								<i class="fa fa-comments"></i>Message
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
                        
						<div class="portlet-body">
							<div class="scroller">
								<ul class="chats">
                                <?php
									foreach($rec1->result() as $reply_row)
												{
													if(($reply_row->Reply_by)!=null)
													{
														if(($reply_row->Reply_type)==1)
														{
														$rep=$reply_row->Reply_by;
														$type='out';
														}
														if(($reply_row->Reply_type)==2)
														{
														$rep=$reply_row->Reply_by;
														$type='out';
														}
														if(($reply_row->Reply_type)==3)
														{
														$rep=$reply_row->Reply_by;
														$type='in';
														}
														if(($reply_row->Reply_type)==4)
														{
														$rep=$reply_row->Reply_by;
														$type='in';
														}
							   ?>
									<li class="<?php echo $type; ?>">
										
										<div class="message">
											<span class="arrow">
											</span>
											<a href="#" class="name">
												 <?php echo $rep; ?>
											</a>
											<span class="datetime">
												 at <?php echo $reply_row->Reply_date ?>
											</span>
											<span class="body">
												 <?php echo $reply_row->Reply_desc ?>
                                                 <br />
												<a href="<?php echo base_url();?>application/uploadimage/<?php echo $reply_row->Reply_file ?>" style="color:#0066CC">
													<?php echo $reply_row->Reply_file ?>
                                                </a>
											</span>
										</div>
									</li>
									<?php 
												} 
											}
									?>
									</ul>
							</div>
							
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
            </div>
            
            <?php
						if(($row->C_Status)!="DEACTIVE")
							{
			?>
			<!--row-->
            <div class="col-md-7">
            	<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Reply Form
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/admin/admin_response"  method="post">
								<div class="form-body">
                                			
									<div class="form-group">
										<label class="col-md-3 control-label">User</label>
										<div class="col-md-9">
											<input type="text" name="txtemail" id="txtemail" style="border:none;" value="Super Admin" class="form-control input-large" readonly="readonly" />
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Comment
                                        	<span class="required">
                                                *
                                            </span>
                                        </label>
										<div class="col-md-9">
											<textarea rows="5" name="txtdiscription" data-provide="markdown" data-error-container="#editor_error"></textarea>
                                            <div id="editor_error">
											</div>
                                        </div>
									</div>
                                     <div class="form-group">
										<label class="col-md-3 control-label">Attachments</label>
										<div class="col-md-9">
											
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
												<div class="input-group input-large">
													<div class="form-control uneditable-input span3" data-trigger="fileinput">
														<i class="fa fa-file fileinput-exists"></i>&nbsp;
														<span class="fileinput-filename">
														</span>
													</div>
													<span class="input-group-addon btn default btn-file">
														<span class="fileinput-new">
															 Select file
														</span>
														<span class="fileinput-exists">
															 Change
														</span>
														<input type="file" name="userfile" id="userfile">
													</span>
													<a href="#" class="input-group-addon btn default fileinput-exists" data-dismiss="fileinput">
														 Remove
													</a>
												</div>
											</div>
										</div>
									</div>
                                </div>
									<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
                                    	<input type="hidden" value="<?php echo $row->Ticket_reference_no ?>" name="txtticket" id="txtticket" />
                                        <input type="hidden" value="<?php echo $row->Department ?>" name="txtdepartment" id="txtdepartment" />
                                        <input type="hidden" value="<?php echo $row->Subject ?>" name="txtsubject" id="txtsubject" />
                                        <input type="hidden" value="<?php echo $row->Urgency ?>" name="txturgency" id="txturgency" />
										<input type="hidden" value="<?php echo $row->Assign_to_id; ?>" name="txtassign" id="txtassign" />
										<button type="submit" class="btn green">Submit</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
            </div>
            <?php  
				 	
			   }
			 ?>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
