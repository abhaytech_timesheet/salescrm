<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title><?php echo sitename?></title>
<meta name="description" content="Abhay TechSolutions LLP .Need Help mail to cs@abhaytech.com." />
<meta name="keywords" content="mobile app developer mumbai,mobile app developer lucknow, android app developer, iphone apps developer, web design in mumbai, web design in lucknow,web design firm, website designing companies, web development mumbai, Web Design Mumbai, Web Application Mumbai, PHP, Dot NET, Drupal, Joomla, Wordpress,Magento,Virtuemart,Zen cart,Prestashop, Hire a programmer,PSD to HTML, Logo design, Banner design, iPhone Apps, Android Apps, CMS, SEO, SEM, SMO, JW Player" />
<meta name="robots" content="index, follow">
<meta name="copyright" content="Copyright &copy; 2021, Abhay TechSolutions LLP" />
<meta name="author" content="Abhay TechSolutions LLP" />
<meta name="email" content="cs@abhaytech.com" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url() ?>application/libraries/assets/pages/css/error.min.css" rel="stylesheet" type="text/css" />
		 <!-- END GLOBAL MANDATORY STYLES -->
		 <!-- BEGIN PAGE LEVEL PLUGINS -->
		 <?php
			if($this->router->fetch_class()!='direct_ticket')
			{
			?>
			<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
			<?php
				if($this->router->fetch_method()=='dashboard')
				{
				?>
				<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
               			<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
           	<link href="<?php echo base_url() ?>application/libraries/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        	<link href="<?php echo base_url() ?>application/libraries/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        
				<!-- END PAGE LEVEL PLUGINS -->
				<?php
				}
				if($this->router->fetch_method()=='view_opportunity')
				{
					?>
				<link href="<?php echo base_url() ?>application/libraries/assets/pages/css/pricing.min.css" rel="stylesheet" type="text/css" />
				<?php
				}
				if($this->router->fetch_method()=='view_send_email_template' || $this->router->fetch_method()=='get_template')
				{
				?>
				<link href="<?php echo base_url() ?>application/libraries/assets/apps/css/inbox.min.css" rel="stylesheet" type="text/css" />
				<?php
				}
			?>
			<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css" />
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
            <link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
			<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
			<!-- END PAGE LEVEL PLUGINS -->
			<?php
			}
		?>
		<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
		
		<!-- BEGIN THEME GLOBAL STYLES -->
		<link href="<?php echo base_url() ?>application/libraries/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
		<link href="<?php echo base_url() ?>application/libraries/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
		<!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url() ?>application/libraries/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/layouts/layout/css/themes/light.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url() ?>application/libraries/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
		<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
		<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/jquery.min.js"></script>

	</head>
<!-- END HEAD -->