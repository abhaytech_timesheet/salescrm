<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View Announcements </h3>
			<!-- END PAGE TITLE-->
			<div class="row">
				
				<div class="col-md-12">
					<div class="portlet light portlet-fit bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-microphone font-green"></i>
								<span class="caption-subject bold font-green uppercase"> Full Announcement</span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="row">
								<div class="col-md-9 col-sm-8 article-block">
									<?php
										foreach($rec->result() as $row)
										{
											if(($row->id)!=null)
											{
											?>
											<a href="javascript:void"><h3><?php echo $row->txttitle ?></h3></a>
											<p>
												<i class="fa fa-calendar"></i>
												<a href="#">
													<?php echo $row->adddate ?>
												</a>
											</p>
<div class="blog-single-desc"><span class="font-black-cascade"> <?php echo substr($row->txtdescription,0,152) ?></span>
</div><?php if($row->userfile!=""){ ?>
													
<div class="blog-single-img"><img src="<?php echo base_url();?>application/uploadimage/<?php echo $row->userfile ?>"/></div>
												<?php } ?>
											
										<div class="blog-single-desc">
												<span class="font-black-cascade"> <?php echo substr($row->txtdescription,152,2000) ?></span>

											</div>
                                        <div class="blog-single-foot">
										<ul class="blog-post-tags">
                                                <li class="uppercase">
                                                    <a href="javascript:;">Bootstrap</a>
                                                </li>
                                                <li class="uppercase">
                                                    <a href="javascript:;">Sass</a>
                                                </li>
                                                <li class="uppercase">
                                                    <a href="javascript:;">HTML</a>
                                                </li>
                                            </ul>
                                        </div>
											<?php
											}
										}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
</div>