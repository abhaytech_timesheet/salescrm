<?php  
	if($dep=='1')
	{
		$department='Support';
	}
	if($dep=='2')
	{
		$department='Sales';
	}
	if($dep=='3')
	{
		$department='Development';
	}
	if($dep=='4')
	{
		$department='Complaints';
	}									
?>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Submit Ticket  for <?php echo $department; ?></h3>
			<!-- END PAGE TITLE-->
			
			
			<div class="row">
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Submit Ticket</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form" id="stylized">
							<form role="form" class="form-horizontal" id="insert_data" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/direct_ticket/outer_ticket" method="post">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Name
                                        	<span class="required"> * </span>
										</label>
										<div class="col-md-6">
											<input type="text" name="txtname" id="txtname" class="form-control input-sm empty" />
											<span id="divtxtname" style="color:red"></span>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Email Address
                                        	<span class="required"> * </span>
										</label>
										<div class="col-md-6">
											<input type="text" name="txtemail" id="txtemail" class="form-control input-sm" />
											<span id="divtxtemail" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Department</label>
										<div class="col-md-6">
                                            <input type="text" name="txtdepartment1" id="txtdepartment1" value="<?php echo $department ?>" readonly="readonly" class="form-control input-sm empty" />
                                            <input type="hidden" name="txtdepartment" id="txtdepartment" value="<?php echo $dep ?>" readonly="readonly" />
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Subject
                                        	<span class="required"> * </span>
										</label>
										<div class="col-md-6">
                                            <input type="text" name="txtsubject" id="txtsubject" class="form-control input-sm empty" />
										    <span id="divtxtsubject" style="color:red"></span>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Urgency</label>
										<div class="col-md-6">
                                            <select name="txturgency" id="txturgency" class="form-control input-sm opt">
                                                <option value="1">High</option>
                                                <option value="2" selected="selected">Medium</option>
                                                <option value="3">Low</option>
											</select>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Comment
                                        	<span class="required"> * </span>
										</label>
										<div class="col-md-9">
                                            <textarea id="txtdiscription" name="txtdiscription" data-provide="markdown" class="form-control input-sm empty" rows="5"></textarea>
                                            <span id="divtxtdiscription" style="color:red"></span>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Attachments</label>
										<div class="col-md-9">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
												<div class="input-group input-large">
													<div class="form-control uneditable-input span3" data-trigger="fileinput">
														<i class="fa fa-file fileinput-exists"></i>&nbsp;
														<span class="fileinput-filename">
														</span>
													</div>
													<span class="input-group-addon btn default btn-file">
														<span class="fileinput-new">
															Select file
														</span>
														<span class="fileinput-exists">
															Change
														</span>
														<input type="file"  name="userfile" id="userfile" accept=".gif,.jpg,.jpeg,.png,.pdf,.doc,.xlsx,.xml,.zip,.txt">
													</span>
													<a href="#" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput">
														Remove
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<?php
                                            $ticket_no=rand(100000, 999999);
										?>
										<input type="hidden" value="<?php echo $ticket_no ?>" name="txtticket" id="txtticket"/>
										<button type="button" onclick="conwv('insert_data')" class="btn blue">Submit</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
</div>