<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-full-width page-md">
	<div class="page-header navbar navbar-fixed-top">
		<!-- BEGIN HEADER INNER -->
		<div class="page-header-inner ">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="<?php echo base_url();?>">
				<img src="<?php if(sitelogo=='') { echo base_url().'application/libraries/assets/layouts/layout/img/company_logo_1582267357-1.png'; } else { echo base_url().'application/logo/86x14/'.sitelogo; } ?>" alt="logo" class="logo-default" style="margin-top:2px;" /> </a>
			</div>
			<!-- END LOGO -->
			<div class="hor-menu hidden-sm hidden-xs">
				<ul class="nav navbar-nav">
					<!-- DOC: Remove data-hover="megamenu-dropdown" and data-close-others="true" attributes below to disable the horizontal opening on mouse hover -->
					<li class="classic-menu-dropdown active">
						<a href="<?php echo base_url();?>support.html"> Home
							<span class="selected"> </span>
						</a>
					</li>
					<li class="mega-menu-dropdown">
						<a href="<?php echo base_url();?>news.html"> Announcements
						</a>
					</li>
					<li class="mega-menu-dropdown mega-menu-full">
						<a href="<?php echo base_url();?>auth/index"> Login
						</a>
					</li>
				</ul>
			</div>
			<!-- END MEGA MENU -->
		
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
			<!-- END RESPONSIVE MENU TOGGLER -->
		</div>
		<!-- END HEADER INNER -->
	</div>
<!-- END HEADER -->	
 <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->

			 <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
               <div class="page-sidebar navbar-collapse collapse">
                    <!-- END SIDEBAR MENU -->
                    <div class="page-sidebar-wrapper">
                        <!-- BEGIN RESPONSIVE MENU FOR HORIZONTAL & SIDEBAR MENU -->
                        <ul class="page-sidebar-menu visible-sm visible-xs  page-header-fixed">
                            <li class="nav-item start open active">
                                <a href="<?php echo base_url();?>direct_ticket/index" class="nav-link nav-toggle"> Home
                                    <span class="selected"> </span>
                                </a>
                            </li>
                           
                            <li class="nav-item start open">
                                <a href="<?php echo base_url();?>direct_ticket/announcements" class="nav-link nav-toggle"> Announcements
                                    <span class="selected"> </span>
                                </a>
                            </li>
							
                            <li class="nav-item start open">
                                <a href="<?php echo base_url();?>auth/index" class="nav-link nav-toggle"> Login
                                    <span class="selected"> </span>
                                </a>
                            </li>
                        </ul>
                        <!-- END RESPONSIVE MENU FOR HORIZONTAL & SIDEBAR MENU -->
                    </div>
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->