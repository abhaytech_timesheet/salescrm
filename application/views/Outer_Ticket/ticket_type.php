<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content"  style="margin-top:-100px;">
			<h3 class="page-title"> Ticket Type </h3>
<?php 
				if($info==1)
				{
					$class="success";
					$msg="<b>Thank You ".trim($name).", Your Ticket ID has been successfully generated!<br/>
					Your HIGH Priority Ticket ID: <span style='color:red'><a href=".base_url()."direct_ticket/view_ticket/".$ticketno.">".$ticketno."</a></span><br/>
					Concerned Department: <span style='color:red'>".$depart."</span><br/>
					For tracking the status a Copy of the Ticket ID has been mailed to you, <br/>
					Thanks<br/>
					Team TechAarjavam .</b>";
				}
				else if($info==2)
				{
					$class="danger";
					$msg="<b>Please Submit Ticket According Ticket Category.</b>";
				}
				else
				{
					$class="info";
					$msg="<b>Submit Ticket According Ticket Category.</b>";
				}
			?>
  <div class="note note-<?php echo $class?>">
				<p> <?php echo $msg?> </p>
			</div>
			<!-- END PAGE TITLE-->
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat red">
						<div class="visual">
							<i class="fa fa-book"></i>
						</div>
						<div class="details">
							<div class="number">
								<span>Support Ticket</span>
							</div>
							<div class="desc"> 
								Technical Assistance Related
							</div>
						</div>
						<a class="more" href="<?php echo base_url();?>ticket/1"> View more
							<i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
				
				
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat green">
						<div class="visual">
							<i class="fa fa-book"></i>
						</div>
						<div class="details">
							<div class="number">
								<span>Sales Ticket</span>
							</div>
							<div class="desc"> 
								Pre Sales, Sales Assistance
							</div>
						</div>
						<a class="more" href="<?php echo base_url();?>ticket/2"> View more
							<i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
				
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat blue">
						<div class="visual">
							<i class="fa fa-book"></i>
						</div>
						<div class="details">
							<div class="number">
								<span>Development Ticket</span>
							</div>
							<div class="desc"> 
								Web Design, Web Development,<br> SEO/SEM Queries
							</div>
						</div>
						<a class="more" href="<?php echo base_url();?>ticket/3"> View more
							<i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
				
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat yellow">
						<div class="visual">
							<i class="fa fa-book"></i>
						</div>
						<div class="details">
							<div class="number">
								<span>Complaints Ticket</span>
							</div>
							<div class="desc"> 
								Abuse issues,complaints
							</div>
						</div>
						<a class="more" href="<?php echo base_url();?>ticket/4"> View more
							<i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
				
			</div>
			
			<br>
			
			<div class="block-carousel">
				<div id="promo_carousel" class="carousel slide">
					<div class="container">
						<div class="carousel-inner">
							<div class="active item">
								<div class="row">
									<div class="col-md-7 margin-bottom-20 margin-top-20 animated rotateInUpRight">
										<h1>Welcome to Tech Aarjavam ..</h1>
										<p></p>
										<a href="http://www.techaarjavam.com" class="btn red btn-lg m-icon-big">
											Take a tour <i class="m-icon-big-swapright m-icon-white"></i>
										</a>
									</div>
									<div class="col-md-5 animated rotateInDownLeft">
										<a href="#">
											<img src="<?php echo base_url();?>application/libraries/assets/pages/img/img1.png" alt="" class="img-responsive">
										</a>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="row">
									<div class="col-md-5 animated rotateInUpRight">
										<a href="http://www.metroheights.co.in">
											<img src="<?php echo base_url();?>application/libraries/assets/pages/img/img1_2.png" alt="" class="img-responsive">
										</a>
									</div>
									<div class="col-md-7 margin-bottom-20 animated rotateInDownLeft">
										<h1>Buy Sales Booster Today..</h1>
										<p>
										</p>
										<a href="index.html" class="btn green btn-lg m-icon-big">
											But it today <i class="m-icon-big-swapright m-icon-white"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<a class="carousel-control left" href="#promo_carousel" data-slide="prev">
							<i class="m-icon-big-swapleft"></i>
						</a>
						<a class="carousel-control right" href="#promo_carousel" data-slide="next">
							<i class="m-icon-big-swapright"></i>
						</a>
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#promo_carousel" data-slide-to="0" class="active">
							</li>
							<li data-target="#promo_carousel" data-slide-to="1">
							</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
</div>