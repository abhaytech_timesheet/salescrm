<!DOCTYPE html>
<html lang="en">

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="TECH AARJAVAM ERP 1.0 Login.Type your user and password to connect TECH AARJAVAM ERP 1.0.Call us at +91-9919111099 or email to info@techaarjavam.com" />
	<meta name="keywords" content="mobile app developer mumbai,mobile app developer lucknow, android app developer, iphone apps developer, web design in mumbai, web design in lucknow,web design firm, website designing companies, web development mumbai, Web Design Mumbai, Web Application Mumbai, PHP, Dot NET, Drupal, Joomla, Wordpress,Magento,Virtuemart,Zen cart,Prestashop, Hire a programmer,PSD to HTML, Logo design, Banner design, iPhone Apps, Android Apps, CMS, SEO, SEM, SMO, JW Player" />
	<meta name="robots" content="index, follow">
	<meta name="copyright" content="Copyright &copy; 2016, TechAarjavam Pvt. Ltd." />
	<meta name="author" content="Vivek Jaiswal" />
	<meta name="email" content="info@techaarjavam.com" />
	<title><?php echo sitename . '-' . $this->uri->segment(1) . '-' . $this->uri->segment(2); ?></title>
	<link href="https://fonts.googleapis.com/css?family=Karla:400,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.8.95/css/materialdesignicons.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/login.css">
</head>

<body>
	<main class="d-flex align-items-center min-vh-100 py-3 py-md-0">
		<div class="container">
			<div class="card login-card">
				<div class="row no-gutters">
					<div class="col-md-5">
						<img src="<?php echo base_url() ?>assets/images/revenue-income-money-profit-costs-budget-banking-concept.jpg" alt="login" class="login-card-img">
					</div>
					<div class="col-md-7">
						<div class="card-body">
							<div class="brand-wrapper">
								<a href="<?php echo base_url(); ?>"><img src="<?php if (sitelogo == '') {
																					echo base_url() . 'application/libraries/assets/layouts/layout/img/company_logo_1582267357-1.png';
																				} else {
																					echo base_url() . 'application/logo/86x14/' . sitelogo;
																				} ?>" alt="logo" class="logo" style="margin-top:2px;" /> </a>
							</div>
							<?php foreach ($aff_detail->result() as $row2) {
								break;
							} ?>
							<form class="login-form" id="login-form" method="post">
								<p class="login-card-description">Login</p>
								<p>Type your user and password to connect <?php echo $row2->affiliate_name; ?>.</p>
								<div class="form-group">
									<label for="txtlogin" class="sr-only">Select Role</label>
									<select class="form-control" id="ddtype" onchange="get_url()">
										<option value="-1">Select</option>
										<option value="2">Admin</option>
										<!--<option value="2">Affiliate</option>-->
										<option value="3">Employee</option>
									</select>
								</div>
								<div class="form-group">
									<label for="txtlogin" class="sr-only">Username</label>
									<input type="text" name="txtlogin" id="txtlogin" class="form-control" placeholder="Username" required>
								</div>
								<div class="form-group mb-4">
									<label for="txtpwd" class="sr-only">Password</label>
									<input type="password" id="txtpwd" class="form-control" placeholder="Password" name="txtpwd" required>
								</div>
								<?php
								if ($info == 1 || $info == 5) {
									$class = "alert alert-danger display";
									$msg = "Please Enter Correct Username and Password.";
								} else if ($info == 2) {
									$class = "alert alert-success display";
									$msg = "You've logged out.";
								} else if ($info == 4) {
									$class = "alert alert-danger display";
									$msg = "Your login has been disabled.Please Contact Admin.";
								} else if ($info == 6) {
									$class = "alert alert-danger display";
									$msg = "You had not logout properly.";
								} else if ($info == 7) {
									$class = "alert alert-danger display";
									$msg = "Your account has been disabled.Please Contact Admin.";
								} else {
									$class = "alert alert-danger display-hide";
									$msg = "Enter any Username and Password.";
								}
								?>
								<div class="<?php echo $class ?>">
									<button class="close" data-close="alert"></button>
									<span><?php echo $msg ?> </span>
								</div>
								<input name="login" id="login" class="btn btn-block login-btn mb-4" type="submit" value="Login">
							</form>
							<form class="forget-form" action="<?php echo base_url(); ?>auth/resetpassword/" method="post">
								<p class="login-card-description">Forgot Password ?</p>
								<p> Enter your e-mail address below to reset your password. </p>
								<div class="form-group">
									<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" />
								</div>
								<div class="form-actions">
									<button type="button" id="back-btn" class="btn btn-block login-btn mb-4" style="width:49%;display:inline-block;">Back</button>
									<button type="submit" class="btn btn-block login-btn mb-4" style="width:50%;display:inline-block;margin-top: 0;">Submit</button>
								</div>
							</form>
							<a href="javascript:;" id="forget-password" class="forget-password forgot-password-link">Forgot password?</a>
							<p class="login-card-footer-text"></p>
							<p class="login-card-footer-text" style="margin-bottom: 0px;">Copyright © 2021 <a href="https://abhaytech.com/" class="text-reset">ABHAY TECHNOLOGIES ERP</a>. All Rights Reserved.
								<br><a href="#!" class="text-reset">Terms & Conditions</a> |
								<a href="#!" class="text-reset">Privacy Policy</a>
							<p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script>
		function get_url() {
			var ddtype = $("#ddtype").val();
			if (ddtype == '1') {
				$('#login-form').attr('action', '<?php echo base_url(); ?>auth/login/1');
			}
			if (ddtype == '2') {
				$('#login-form').attr('action', '<?php echo base_url(); ?>auth/affiliate_login/2');
			}
			if (ddtype == '3') {
				$('#login-form').attr('action', '<?php echo base_url(); ?>auth/employee_login/3');
			}
		}
	</script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>application/libraries/assets/pages/scripts/login-5.min.js"></script>
</body>

</html>