<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title><?php echo sitename.'-'.$this->uri->segment(1).'-'.$this->uri->segment(2);?></title>
<meta name="description" content="TECH AARJAVAM ERP 1.0 Login.Type your user and password to connect TECH AARJAVAM ERP 1.0.Call us at +91-9919111099 or email to info@techaarjavam.com" />
<meta name="keywords" content="mobile app developer mumbai,mobile app developer lucknow, android app developer, iphone apps developer, web design in mumbai, web design in lucknow,web design firm, website designing companies, web development mumbai, Web Design Mumbai, Web Application Mumbai, PHP, Dot NET, Drupal, Joomla, Wordpress,Magento,Virtuemart,Zen cart,Prestashop, Hire a programmer,PSD to HTML, Logo design, Banner design, iPhone Apps, Android Apps, CMS, SEO, SEM, SMO, JW Player" />
<meta name="robots" content="index, follow">
<meta name="copyright" content="Copyright &copy; 2016, TechAarjavam Pvt. Ltd." />
<meta name="author" content="Vivek Jaiswal" />
<meta name="email" content="info@techaarjavam.com" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

		<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/socicon/socicon.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>application/libraries/assets/pages/css/login-5.min.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->