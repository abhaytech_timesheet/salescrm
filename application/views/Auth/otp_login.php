<script language="JavaScript">
	var count=0;
	function load_form()
	{
		var pinpassword=$("#txtpinpwd").val();
		var type=<?php echo $type; ?>;
		if(pinpassword!="")
		{  
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/auth/authentcate_otppassword",
				data:"pinpassword="+pinpassword+"&msg=<?php echo $msg; ?>&txtlogin=<?php echo $txtlogin; ?>&type="+type,
				success: function(msg) {
					//alert(msg);
					if(msg.trim()!="false" && msg.trim()!="")
					{
						if(type=='1')
						{
							window.location.href = "<?php echo base_url();?>report/dashboard/"; 
						}
						if(type=='2')
						{
							window.location.href = "<?php echo base_url();?>report/dashboard"; 
						}
						if(type=='3')
						{
							window.location.href = "<?php echo base_url();?>report/acc_dashboard"; 
						}
					}
					else
					{
						$("#txtpinpwd").focus();
						alert('Please Fill Correct OTP Password');
						count=count+1;
						//alert(count);
						if(count==3)
						{
							window.location.href = "<?php echo base_url();?>";
						}
					}
				}
			});
			
		}
		else
		{
			alert('Please Fill Pin Password');
		}
		
	}
</script>

<!-- BEGIN : LOGIN PAGE 5-1 -->
<div class="user-login-5">
	<div class="row bs-reset">
		<div class="col-md-6 bs-reset">
			<div class="login-bg" style="background-image:url(<?php echo base_url() ?>application/libraries/assets/pages/img/login/bg1.jpg)">
			<img src="<?php if(sitelogo=='') { echo base_url().'application/libraries/assets/layouts/layout/img/company_logo_1582267357-1.png'; } else { echo base_url().'application/logo/86x14/'.sitelogo; } ?>" alt="logo" class="logo-default" style="margin-top:2px;" /> </a>
		 </div>
		</div>
		
		<div class="col-md-6 login-container bs-reset">
			<div class="login-content">
				
				<form class="login-form" action="#" method="post" id="stylized" onsubmit="return check()">
					<h3>One Time Password</h3>
					<p>Enter your OTP password below which send on your registered phone Or Email.</p>
					<div class="form-group">
						<label class="control-label visible-ie8 visible-ie9">Otp</label>
						<div class="input-icon">
							
							<input class="form-control empty" type="text" placeholder="Otp Password" name="txtpinpwd" id="txtpinpwd"/>
							<span id="divtxtpinpwd" style="color:red;"></span>
						</div>
					</div>
					
					<div class="form-actions">
						<button type="button" id="back-btn" class="btn" onclick="gofunction()">
							<i class="m-icon-swapleft"></i> Back
						</button>
						<button type="button" class="btn green pull-right" onclick="load_form()">
							Submit <i class="m-icon-swapright m-icon-white"></i>
						</button>            
					</div>
					
				</form>
				
			</div>
			<div class="login-footer">
				<div class="row bs-reset">
					<div class="col-xs-4 bs-reset">
						<ul class="login-social">
							<li>
								<a href="javascript:;">
									<i class="icon-social-facebook"></i>
								</a>
							</li>
							<li>
								<a href="javascript:;">
									<i class="icon-social-twitter"></i>
								</a>
							</li>
							<li>
								<a href="javascript:;">
									<i class="icon-social-dribbble"></i>
								</a>
							</li>
						</ul>
					</div>
					<div class="col-xs-8 bs-reset">
						<div class="login-copyright text-right">
							<p>Copyright &copy; <?php echo date('Y') ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	function gofunction()
	{
		window.location.href = "<?php echo base_url();?>";
	}
</script>			