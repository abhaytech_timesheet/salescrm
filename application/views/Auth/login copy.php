<!-- BEGIN : LOGIN PAGE 5-1 -->
<div class="user-login-5">
	<div class="row bs-reset">
		<div class="col-md-6 bs-reset">
			<div class="login-bg" style="background-image:url(http://traininginlucknow.com/ERP_CRM/application/libraries/assets/pages/img/login/bg1.jpg)">
				<a href="<?php echo base_url(); ?>"><img src="<?php if (sitelogo == '') {
																	echo base_url() . 'application/libraries/assets/layouts/layout/img/company_logo_1582267357-1.png';
																} else {
																	echo base_url() . 'application/logo/86x14/' . sitelogo;
																} ?>" alt="logo" class="logo-default" style="margin-top:2px;" /> </a>
			</div>
		</div>

		<div class="col-md-6 login-container bs-reset">
			<div class="login-content">
				<img src="<?php if (sitelogo == '') {
								echo base_url() . 'application/libraries/assets/layouts/layout/img/company_logo_1582267357-1.png';
							} else {
								echo base_url() . 'application/logo/' . sitelogo;
							} ?>" alt="logo" class="logo-default" style="margin-top:-200px;" />
				<?php foreach ($aff_detail->result() as $row2) {
					break;
				} ?>
				<h1><?php echo $row2->affiliate_name; ?> LOGIN</h1>
				<p> Type your user and password to connect <?php echo $row2->affiliate_name; ?>. </p>
				<form class="login-form" id="login-form" method="post">

					<div class="row">
						<div class="col-xs-12 form-group form-md-line-input">
							<select class="form-control" id="ddtype" onchange="get_url()">
								<option value="-1">Select</option>
								<option value="2">Admin</option>
								<!--<option value="2">Affiliate</option>-->
								<option value="3">Employee</option>
							</select>
						</div>
						<div class="col-xs-6">
							<input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="txtlogin" required />
						</div>
						<div class="col-xs-6">
							<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="txtpwd" required />
						</div>
					</div>
					<?php
					if ($info == 1 || $info == 5) {
						$class = "alert alert-danger display";
						$msg = "Please Enter Correct Username and Password.";
					} else if ($info == 2) {
						$class = "alert alert-success display";
						$msg = "You've logged out.";
					} else if ($info == 4) {
						$class = "alert alert-danger display";
						$msg = "Your login has been disabled.Please Contact Admin.";
					} else if ($info == 6) {
						$class = "alert alert-danger display";
						$msg = "You had not logout properly.";
					} else if ($info == 7) {
						$class = "alert alert-danger display";
						$msg = "Your account has been disabled.Please Contact Admin.";
					} else {
						$class = "alert alert-danger display-hide";
						$msg = "Enter any Username and Password.";
					}
					?>
					<div class="<?php echo $class ?>">
						<button class="close" data-close="alert"></button>
						<span><?php echo $msg ?> </span>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<div class="rem-password">
								<p>Remember Me
									<input type="checkbox" class="rem-checkbox" />
								</p>
							</div>
						</div>
						<div class="col-sm-8 text-right">
							<div class="forgot-password">
								<a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
							</div>
							<button class="btn blue" type="submit">Sign In</button>
						</div>
					</div>
					<hr>

				</form>
				<!-- BEGIN FORGOT PASSWORD FORM -->
				<form class="forget-form" action="<?php echo base_url(); ?>auth/resetpassword/" method="post">
					<h3 class="font-green">Forgot Password ?</h3>
					<p> Enter your e-mail address below to reset your password. </p>
					<div class="form-group">
						<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" />
					</div>
					<div class="form-actions">
						<button type="button" id="back-btn" class="btn grey btn-default">Back</button>
						<button type="submit" class="btn blue btn-success uppercase pull-right">Submit</button>
					</div>
				</form>
				<!-- END FORGOT PASSWORD FORM -->
			</div>
			<div class="login-footer">
				<!--div class="row bs-reset">
					<div class="col-xs-12 bs-reset">
						<div class="fb-like" data-share="true" data-width="450" data-show-faces="true"></div>
						<ul class="login-social">
							<li>
								<a href="javascript:;">
									<i class="icon-social-facebook"></i>
								</a>
							</li>
							<li>
								<a href="javascript:;">
									<i class="icon-social-twitter"></i>
								</a>
							</li>
							<li>
								<a href="javascript:;">
									<i class="icon-social-dribbble"></i>
								</a>
							</li>
						</ul>
						
					</div>
					</div-->
				<div class="row bs-reset">
					<div class="col-xs-12 bs-reset">
						<ul class="login-social">
							<li>
								<a href="javascript:;">
									<i class="icon-social-facebook"></i>
								</a>
							</li>
							<li>
								<a href="javascript:;">
									<i class="icon-social-twitter"></i>
								</a>
							</li>
							<li>
								<a href="javascript:;">
									<i class="icon-social-dribbble"></i>
								</a>
							</li>
						</ul>
						<div class="login-copyright text-right">
							Copyright &copy; <?php echo date('Y'); ?> <b>
								<a href=""><?php echo $row2->affiliate_name; ?></a></b>. All Rights Reserved. &nbsp;&nbsp;&nbsp;&nbsp;Terms of use&nbsp;&nbsp;&nbsp;&nbsp;Privacy Policy&nbsp;&nbsp;&nbsp;&nbsp;Site Map
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	function get_url() {
		var ddtype = $("#ddtype").val();
		if (ddtype == '1') {
			$('#login-form').attr('action', '<?php echo base_url(); ?>auth/login/1');
		}
		if (ddtype == '2') {
			$('#login-form').attr('action', '<?php echo base_url(); ?>auth/affiliate_login/2');
		}
		if (ddtype == '3') {
			$('#login-form').attr('action', '<?php echo base_url(); ?>auth/employee_login/3');
		}
	}
</script>

<script>
	// This is called with the results from from FB.getLoginStatus().
	function statusChangeCallback(response) {
		console.log('statusChangeCallback');
		console.log(response);
		// The response object is returned with a status field that lets the
		// app know the current login status of the person.
		// Full docs on the response object can be found in the documentation
		// for FB.getLoginStatus().
		if (response.status === 'connected') {
			// Logged into your app and Facebook.
			testAPI();
		} else if (response.status === 'not_authorized') {
			// The person is logged into Facebook, but not your app.
			document.getElementById('status').innerHTML = 'Please log ' +
				'into this app.';
		} else {
			// The person is not logged into Facebook, so we're not sure if
			// they are logged into this app or not.
			document.getElementById('status').innerHTML = 'Please log ' +
				'into Facebook.';
		}
	}

	// This function is called when someone finishes with the Login
	// Button.  See the onlogin handler attached to it in the sample
	// code below.
	function checkLoginState() {
		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});
	}

	window.fbAsyncInit = function() {
		FB.init({
			appId: '{295244744159375}',
			cookie: true, // enable cookies to allow the server to access 
			// the session
			xfbml: true, // parse social plugins on this page
			version: 'v2.5' // use graph api version 2.5
		});

		// Now that we've initialized the JavaScript SDK, we call 
		// FB.getLoginStatus().  This function gets the state of the
		// person visiting this page and can return one of three states to
		// the callback you provide.  They can be:
		//
		// 1. Logged into your app ('connected')
		// 2. Logged into Facebook, but not your app ('not_authorized')
		// 3. Not logged into Facebook and can't tell if they are logged into
		//    your app or not.
		//
		// These three cases are handled in the callback function.

		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});

	};

	// Load the SDK asynchronously
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	// Here we run a very simple test of the Graph API after login is
	// successful.  See statusChangeCallback() for when this call is made.
	function testAPI() {
		console.log('Welcome!  Fetching your information.... ');
		FB.api('/me', function(response) {
			console.log('Successful login for: ' + response.name);
			document.getElementById('status').innerHTML =
				'Thanks for logging in, ' + response.name + '!';
		});
	}
</script>


<script>
	window.fbAsyncInit = function() {
		FB.init({
			appId: '295244744159375',
			xfbml: true,
			version: 'v2.7'
		});
	};

	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {
			return;
		}
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>