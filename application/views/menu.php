<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
	<!-- BEGIN HEADER -->
	<div class="page-header navbar navbar-fixed-top">
		<!-- BEGIN HEADER INNER -->
		<div class="page-header-inner ">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="">
				<img src="<?php if(sitelogo=='') { echo base_url().'application/libraries/assets/layouts/layout/img/company_logo_1582267357-1.png'; } else { echo base_url().'application/logo/86x14/'.sitelogo; } ?>" alt="logo" class="logo-default" style="" /> </a>
				<div class="menu-toggler sidebar-toggler"> </div>
			</div>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
					<li class="dropdown dropdown-user">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<img alt="" class="img-circle" src="<?php echo base_url() ?>application/libraries/assets/layouts/layout/img/avatar3_small.jpg" />
							<span class="username username-hide-on-mobile"> <?php echo $this->session->userdata('name'); ?> </span>
							<i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="<?php echo base_url();?>dashboard">
								<i class="icon-user"></i> My Profile </a>
							</li>
							<li>
								<a href="<?php echo base_url() ?>email_marketing">
									<i class="icon-envelope-open"></i> My Inbox
								</a>
							</li>
							<li>
								<a href="<?php echo base_url() ?>to_do/0">
									<i class="icon-rocket"></i> My Tasks
								</a>
							</li>
							<li class="divider"> </li>
							<li>
								<a href="page_user_lock_1.html">
								<i class="icon-lock"></i> Lock Screen </a>
							</li>
							<li>
								<a href="<?php echo base_url() ?>logout/2">
								<i class="icon-key"></i> Log Out </a>
							</li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
					<!-- BEGIN QUICK SIDEBAR TOGGLER -->
					<li class="dropdown dropdown-quick-sidebar-toggler">
						<a href="javascript:;" class="dropdown-toggle">
							<i class="icon-logout"></i>
						</a>
					</li>
					<!-- END QUICK SIDEBAR TOGGLER -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
		<!-- END HEADER INNER -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN HEADER & CONTENT DIVIDER -->
	<div class="clearfix"> </div>
	<!-- END HEADER & CONTENT DIVIDER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar-wrapper">
			<!-- BEGIN SIDEBAR -->
			<div class="page-sidebar navbar-collapse collapse">
				<!-- BEGIN SIDEBAR MENU -->
				<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
					<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
					<li class="sidebar-toggler-wrapper hide">
						<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
						<div class="sidebar-toggler"> </div>
						<!-- END SIDEBAR TOGGLER BUTTON -->
					</li>					
					<li class="nav-item start">
						<a href="<?php echo base_url();?>dashboard">
							<i class="icon-home"></i>
							<span class="title">Dashboard</span>
							<span class="selected"></span>
						</a>
					</li>
					
					<li class="heading">
						<h3 class="uppercase"> Ticket System</h3>
					</li>
					
					<li class="nav-item">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="icon-screen-desktop"></i>
							<span class="title">Help Desk</span>
							<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item ">
								<a href="<?php echo base_url();?>tickets" class="nav-link ">
									<i class="fa fa-ticket"></i>
									<span class="title">View All Ticket</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url();?>allocate" class="nav-link ">
									<i class="icon-user-following"></i>
									<span class="title">Assign Ticket</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url();?>explore" class="nav-link ">
									<i class="icon-magnifier"></i>
									<span class="title">Search Ticket</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url();?>employee_delineate" class="nav-link ">
									<i class="icon-speedometer"></i>
									<span class="title">Ticket Duration Report</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url();?>declaration" class="nav-link ">
									<i class="icon-bell"></i>
									<span class="title">Add Announcements</span>
								</a>
							</li>
						</ul>
					</li>
					
					<li class="nav-item">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="icon-user-follow"></i>
							<span class="title">Registration</span>
							<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>employee_outlook" class="nav-link ">
									<i class="icon-user-following"></i>
									<span class="title">View/Modify Employee</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>employee_referal" class="nav-link ">
									<i class="icon-book-open"></i>
									<span class="title">Employee Report</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>employee_presence" class="nav-link ">
									<i class="icon-book-open"></i>
									<span class="title">Employee Attendance</span>
								</a>
							</li>
						</ul>
					</li>
					
					<li class="heading">
						<h3 class="uppercase">Sales Booster</h3>
					</li>
					
					<li class="nav-item">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="icon-badge"></i>
							<span class="title">CRM</span>
							<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>leads/0" class="nav-link ">
									<i class="icon-bag"></i>
									<span class="title">Lead</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>converted_leads/0" class="nav-link ">
									<i class="icon-briefcase"></i>
									<span class="title">Opportunity</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>profiles/0" class="nav-link ">
									<i class="icon-bubbles"></i>
									<span class="title">Account</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>associates/0" class="nav-link ">
									<i class="icon-call-in"></i>
									<span class="title">Contact</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>to_do/0" class="nav-link ">
									<i class="icon-speedometer"></i>
									<span class="title">Task</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>episodes/0" class="nav-link ">
									<i class="icon-calendar"></i>
									<span class="title">Event</span>
								</a>
							</li>
						</ul>
					</li>
					
					<li class="nav-item">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="icon-folder"></i>
							<span class="title">Campaign</span>
							<span class="arrow "></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item">
								<a href="javascript:;" class="nav-link nav-toggle">
									<i class="icon-note"></i> Create Campaign
									<span class="arrow"></span>
								</a>
								<ul class="sub-menu">
									<li class="nav-item">
										<a href="<?php echo base_url() ?>text_campaign" class="nav-link">
										<i class="icon-screen-tablet"></i> SMS Campaign</a>
									</li>
									<li class="nav-item">
										<a href="<?php echo base_url() ?>creative_templates" class="nav-link">
										<i class="icon-envelope-open"></i> Email Campaign</a>
									</li>
								</ul>
							</li>
							<li class="nav-item">
								<a href="javascript:;" class="nav-link nav-toggle">
									<i class="icon-rocket"></i> Send Campaign
									<span class="arrow nav-toggle"></span>
								</a>
								<ul class="sub-menu">
									<li class="nav-item">
										<a href="<?php echo base_url() ?>sms_marketing" class="nav-link">
										<i class="icon-action-redo"></i> Send SMS Campaign</a>
									</li>
									<li class="nav-item">
										<a href="<?php echo base_url() ?>email_marketing" class="nav-link">
										<i class="icon-envelope-open"></i> Send Email Campaign</a>
									</li>
								</ul>
							</li>
						</ul>
					</li>
					
					<li class="nav-item">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="fa fa-bar-chart-o"></i>
							<span class="title">AMC</span>
							<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>account/view_amc_report" class="nav-link ">
									<i class="fa fa-folder-o"></i>
									<span class="title">AMC Detail</span>
								</a>
							</li>
						</ul>
					</li>
					
					<li class="nav-item">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="icon-wallet"></i>
							<span class="title">Accounts</span>
							<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>clients" class="nav-link ">
									<i class="fa fa-credit-card"></i>
									<span class="title">Billing</span>
								</a>
							</li>
						</ul>
					</li>
					
					<li class="heading">
						<h3 class="uppercase">Project Module</h3>
					</li>
					
					<li class="nav-item">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="fa fa-archive"></i>
							<span class="title">Project</span>
							<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>projects" class="nav-link ">
									<i class="fa fa-plus-square"></i>
									<span class="title">Add New Project</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>project_detail" class="nav-link ">
									<i class="icon-book-open"></i>
									<span class="title">View Client Project Report</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>team" class="nav-link ">
									<i class="icon-plus"></i>
									<span class="title">Add Project Participants</span>
								</a>
							</li>
							
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>team_todo" class="nav-link ">
									<i class="icon-list"></i>
									<span class="title">View All Task</span>
								</a>
							</li>
							<!--<li class="nav-item  ">
								<a href="<?php echo base_url() ?>project_detail" class="nav-link ">
									<i class="icon-book-open"></i>
									<span class="title">View Project Report</span>
								</a>
							</li>-->
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>todo_list" class="nav-link ">
									<i class="icon-book-open"></i>
									<span class="title">View Task Report</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>follow-ups" class="nav-link ">
									<i class="icon-book-open"></i>
									<span class="title">Current Followup Report</span>
								</a>
							</li>
						</ul>
					</li>
					
					<li class="heading">
						<h3 class="uppercase">ACS Module</h3>
					</li>
					
					<li class="nav-item">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="fa fa-wrench"></i>
							<span class="title">ACS Configuration</span>
							<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>sessions" class="nav-link ">
									<i class="icon-calendar"></i>
									<span class="title">Add Session</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>departments" class="nav-link ">
									<i class="icon-grid"></i>
									<span class="title">Manage Department</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>roles" class="nav-link ">
									<i class="icon-target"></i>
									<span class="title">Manage Role</span>
								</a>
							</li>
							<!-- <li class="nav-item  ">
								<a href="<?php echo base_url() ?>salary_evaluation" class="nav-link ">
									<i class="fa fa-money"></i>
									<span class="title">Define Salary Scale</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>allowances" class="nav-link ">
									<i class="fa fa-google-wallet"></i>
									<span class="title">Add Allowances</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>holidays" class="nav-link ">
									<i class="fa fa-ban"></i>
									<span class="title">Holiday List</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>salary" class="nav-link ">
									<i class="fa fa-money"></i>
									<span class="title">Manage Salary</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>work_schedule" class="nav-link ">
									<i class="fa fa-object-ungroup"></i>
									<span class="title">Work Parameter</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>leave_management" class="nav-link ">
									<i class="fa fa-navicon (alias)"></i>
									<span class="title">Leave Master</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>shifts" class="nav-link ">
									<i class="fa fa-mortar-board (alias)"></i>
									<span class="title">Shift Master</span>
								</a>
							</li> -->
						</ul>
					</li>
					
					
					<li class="heading">
						<h3 class="uppercase">Settings</h3>
					</li>
					
					<li class="nav-item">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="icon-globe"></i>
							<span class="title">Configuration</span>
							<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>configuration" class="nav-link ">
									<i class="fa fa-cogs"></i>
									<span class="title">Manage Configuration</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>city" class="nav-link ">
									<i class="fa fa-location-arrow"></i>
									<span class="title">Manage City</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>news" class="nav-link ">
									<i class="fa fa-shopping-cart"></i>
									<span class="title">Manage News</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>applications" class="nav-link ">
									<i class="fa fa-square"></i>
									<span class="title">Manage Apps</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>application_status" class="nav-link ">
									<i class="fa fa-file-o"></i>
									<span class="title">Status Change Apps</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>app_allocation" class="nav-link ">
									<i class="fa fa-sitemap"></i>
									<span class="title">Assign Apps</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>review-app-status" class="nav-link ">
									<i class="fa fa-file-o"></i>
									<span class="title">Update Assign Apps</span>
								</a>
							</li>
							<li class="nav-item  ">
								<a href="<?php echo base_url() ?>accounts" class="nav-link ">
									<i class="fa fa-file-o"></i>
									<span class="title">Manage Account Head</span>
								</a>
							</li>
						</ul>
					</li>
					
					
					
					
					
					
				</ul>
				<!-- END SIDEBAR MENU -->
			</div>
			<!-- END SIDEBAR -->
		</div>
	<!-- END SIDEBAR -->																																												