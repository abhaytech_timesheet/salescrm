<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="#">Client API</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Manage API URI</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Manage API URI</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Manage API URI</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" id="insert_data" action="<?php echo base_url().'index.php/api/add_api_url'?>" method="post" onsubmit="return check(conwv('myform'))">
								<div class="form-body" id="city">
									<div class="form-group">
										<label class="col-md-4 control-label">Select API Provider Name</label>
										<div class="col-md-6">
											<select id="ddapi" name="ddapi" class="form-control input-sm opt">
												<option selected="selected" value="-1">Select API Provider</option>
												<?php foreach($rec->result() as $row)
													{
													?><option value="<?php echo $row->m_api_id;?>"><?php echo $row->m_api_name;?></option>
													<?php
													}
												?>
											</select>
											<span id="divddapi" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">API Name</label>
										<div class="col-md-6">
											<input type="text" id="txtapi_name" name="txtapi_name" class="form-control input-sm empty" placeholder="API Name" id="myform" >
											<span id="divtxtapi_provname" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">API URL</label>
										<div class="col-md-6">
											<input type="text" id="txtapi_url" name="txtapi_url" class="form-control input-sm empty" placeholder="Enter API URL" id="myform" >
											<span id="divtxtapi_url" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">API Parameter</label>
										<div class="col-md-6">
											<input type="text" id="txtapiprm" name="txtapiprm" class="form-control input-sm empty" placeholder="Enter API Parameter" id="myform" >
											<span id="divtxtapi_url" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">API For</label>
										<div class="col-md-6">
											<select id="rb_apifor" name="rb_apifor" class="form-control input-sm opt">
												<option selected="selected" value="-1">Select API For</option>
												<option value="1">Recharge</option>
												<option value="2">Balance</option>
												<option value="3">Disputes</option>
												<option value="4">Status</option>
												<option value="5">SMS</option>
											</select>
											<span id="divrb_apifor" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">API Status</label>
										<div class="col-md-6">
											<div class="radio-list">
												<label class="radio-inline">
												<input type="radio" name="rbapi" id="rbapi1" value="1" checked> Enable</label>
												<label class="radio-inline">
												<input type="radio" name="rbapi" id="rbapi2" value="0"> Disable</label>
											</div>
										</div>
									</div>
									
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-4 col-md-9">
										<button type="button" onclick="conwv('insert_data')" class="btn green">Submit</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">View API URI</span>
							</div>
							<div class="tools">
								<button class="btn btn-sm blue" onclick="change_status('api','stch_api_url')">Enable<i class="fa fa-check"></i></button>
							</div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
								<thead>
									<tr>
										<th>
											<input type="checkbox" id="checkAll" onclick="chbcheckall()" class="group-checkable">
										</th>
										<th>S No.</th>
										<th>API Provider</th>
										<th>API URI Name</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="userid">
									<?php
										$sn=1;
										foreach($api->result() as $row)
										{?>
										<tr>
											<td>
												<?php 
													if($row->Api_url_status==0)
													{
													?>
													<input type="checkbox" class="checkboxes" id="<?php echo $row->Api_url_id;?>" onclick="chbchecksin()" >
													<?php
													}
													else if($row->Api_url_status==1)
													{
													?>
													<input type="checkbox" class="checkboxes" id="<?php echo $row->Api_url_id;?>" onclick="chbchecksin()" checked>
													<?php
													}
												?>
											</td>
											<td><?php echo $sn++; ?></td>
											<td><?php echo $row->Api_name; ?></td>
											<td><?php echo $row->Api_url_name; ?></td>
											<td>
												<a href="<?php echo base_url();?>index.php/api/edit_api_uri/<?php echo $row->Api_url_id;?>" title="Edit Api Name" class="label label-success">
													EDIT
												</a>
											</td>
										</tr>
										<?php
										}
									?>
								</tbody>
							</table>
							<input type="hidden" id="txtquid" name="txtquid">
							<input type="hidden" id="txtunchk" name="txtunchk">
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													
<script src="<?php echo base_url() ?>application/libraries/js/checkboxscipt.js"></script>