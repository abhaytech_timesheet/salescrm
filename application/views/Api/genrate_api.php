<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="#">API User</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Generate API</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Generate API</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				
				<!-- END PAGE CONTENT-->
				
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Generate API for User</span>
							</div>
							<div class="tools">
							</div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover dt-responsive" id="sample_1">
							<thead>
									<tr>
										<th>SNo.</th>
										<th>UserID</th>
										<th>User Name</th>
										<th>Mobile No.</th>
										<th>Introducer Name</th>
										<th>Introducer ID</th>
										<th>City / State</th>
										<th>Joining Date</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$sn=1;
									foreach($record->result() as $row)
									{?>
									<tr>
										<td><?php echo $sn++;?></td>
										<td><?php echo $row->or_m_user_id;?></td>
										<td><?php echo $row->or_m_name;?></td>
										<td><?php echo $row->or_m_mobile_no;?></td>
										<td><?php echo $row->or_m_intr_name;?></td>
										<td><?php echo $row->or_m_intr_id;?></td>
										<td><?php echo $row->city." / ".$row->state;?></td>
										<td><?php echo $row->or_member_joining_date;?></td>
										<td>
											<a href="<?php echo base_url();?>api/generate_apiuser/<?php echo $row->or_m_reg_id;?>"><i class="fa fa-edit"></i> Edit</a>
										</td>
									</tr>
									<?php
									}?>
								</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END CONTENT BODY -->
		</div>
	</div>
	<!-- END CONTENT -->	
	
	<script>
		function assign_group_user()
		{
			var collection=$(".op");
			var inputs=collection.find("input[type=radio]");
			var status=0;
			var gp_userid="";
			
			for(var x=0;x<inputs.length;x++)
			{
				var id=inputs[x].id;
				if($("#"+id+"").is(':checked'))
				{
					var op=$("#"+id+"").val();
					var op1=op.split(',');
					grpid=op1[1];
					gp_userid=op1[0]+","+grpid+"-"+gp_userid;
					status=1;
				}
			}
			if(status==0)
			{
				alert('Please Select any User Id');
			}
			else
			{
				$.ajax(
				{
					type:"POST",
					url:"<?php echo base_url();?>index.php/group/assign_groupuser",
					data:"gp_userid="+gp_userid,
					success: function(msg)
					{
						alert(msg.trim());
						location.reload();
					}
				});
			}
			
		}
	</script>			