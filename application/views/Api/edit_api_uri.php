<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="#">Client API</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Manage API URI</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Edit API URI</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Edit API URI</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" id="insert_data" action="<?php echo base_url().'index.php/api/update_api_url/'.$api->m_api_url_id?>" method="post" onsubmit="return check(conwv('myform'))">
								<div class="form-body" id="city">
									<div class="form-group">
										<label class="col-md-4 control-label">Select API Provider Name</label>
										<div class="col-md-6">
											<select id="ddapi" name="ddapi" class="form-control input-sm opt">
												<option selected="selected" value="-1">Select API Provider</option>
												<?php foreach($rec->result() as $row)
													{
													if($api->m_api_id == $row->m_api_id)
													{?>
														<option value="<?php echo $row->m_api_id;?>" selected><?php echo $row->m_api_name;?></option>
												<?php}
													else
													{?>
														<option value="<?php echo $row->m_api_id;?>"><?php echo $row->m_api_name;?></option>
													<?php
													}
													}
												?>
											</select>
											<span id="divddapi" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">API Name</label>
										<div class="col-md-6">
											<input type="text" id="txtapi_name" name="txtapi_name" class="form-control input-sm empty" placeholder="API Name" id="myform" value="<?php echo $api->m_api_url_name;?>">
											<span id="divtxtapi_provname" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">API URL</label>
										<div class="col-md-6">
											<input type="text" id="txtapi_url" name="txtapi_url" class="form-control input-sm empty" placeholder="Enter API URL" id="myform" value="<?php echo $api->m_api_url_address;?>">
											<span id="divtxtapi_url" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">API Parameter</label>
										<div class="col-md-6">
											<input type="text" id="txtapiprm" name="txtapiprm" class="form-control input-sm empty" placeholder="Enter API Parameter" id="myform"  value="<?php echo $api->m_api_url_prm;?>">
											<span id="divtxtapi_url" style="color:red;"></span>
										</div>
									</div>
									
									
									
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-4 col-md-9">
										<button type="button" onclick="conwv('insert_data')" class="btn green">Submit</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													
<script>
	function checkk1(id,func)
	{
		
		if(func=="1")
		{
			$("#txtstatus").val(id);
		}
		if(func=="2")
		{
			$("#txtstatusfor").val(id);
		}
		
	}
</script>