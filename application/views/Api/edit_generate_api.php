<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="#">API User</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Manage API User</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Manage API User</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Manage API User</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" id="insert_data" action="<?php echo base_url().'index.php/api/edit_apiuser'?>" method="post" onsubmit="return check(conwv('myform'))">
								<div class="form-body" id="city">
								<div class="row">
									<div class="form-group">
										<label class="col-md-2 control-label">Member ID<span class="required">*</span></label>
										<div class="col-md-4">
											<input type="text" id="txtmemid" name="txtmemid" class="form-control input-sm empty" placeholder="Enter Member ID" id="myform" value="<?php echo $record->or_m_user_id;?>" readonly>
											<span id="divtxtmemid" style="color:red;"></span>
										</div>
									
										<label class="col-md-2 control-label">Member Name<span class="required">*</span></label>
										<div class="col-md-3">
											<input type="text" id="txtmem_name" name="txtmem_name" class="form-control input-sm empty" placeholder="Enter Member Name" id="myform" value="<?php echo $record->or_m_name;?>" readonly>
											<span id="divtxtmem_name" style="color:red;"></span>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-md-2 control-label">I.P.<span class="required">*</span></label>
										<div class="col-md-4">
											<input type="text" id="txtip" name="txtip" class="form-control input-sm empty" placeholder="Enter I.P." id="myform" value="" >
											<span id="divtxtip" style="color:red;"></span>
										</div>
									
										<label class="col-md-2 control-label">Call Back Url<span class="required">*</span></label>
										<div class="col-md-3">
											<input type="text" id="txt_cburl" name="txt_cburl" class="form-control input-sm empty" placeholder="Enter Call Back Url" id="myform" value="">
											<span id="divtxt_cburl" style="color:red;"></span>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<label class="col-md-2 control-label">Logo<span class="required">*</span></label>
										<div class="col-md-4">
											<div class="fileinput fileinput-new" data-provides="fileinput">
													<div class="input-group input-large">
														<div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
															<i class="fa fa-file fileinput-exists"></i>&nbsp;
															<span class="fileinput-filename"> </span>
														</div>
														<span class="input-group-addon btn default btn-file">
															<span class="fileinput-new"> Select file </span>
															<span class="fileinput-exists"> Change </span>
															<input type="file" name="userfile" multiple id="userfile" accept="image/*"> 
														</span>
														<a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
													</div>
												</div>
												<span id="divuserfile" style="color:red"></span>
										</div>
									
										
									</div>
								</div>
								
									
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="button" onclick="conwv('insert_data')" class="btn green">Submit</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->		

<script src="<?php echo base_url() ?>application/libraries/js/checkboxscipt.js"></script>