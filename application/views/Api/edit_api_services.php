<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="#">Client API</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Manage API Services</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Edit API Services</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Edit API Services</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" id="insert_data" action="update_api_service" method="post" onsubmit="return check(conwv('myform'))">
								<div class="form-body" id="city">
									<div class="form-group">
										<label class="col-md-4 control-label">Select API Name</label>
										<div class="col-md-6">
											<select id="ddapi" name="ddapi" class="form-control input-sm opt">
												<option selected="selected" value="-1">Select API Name</option>
												<?php foreach($rec->result() as $row)
													{
														if($apiservice->Api_id == $row->m_api_id)
														{?>
														<option value="<?php echo $row->m_api_id;?>" selected><?php echo $row->m_api_name;?></option>
												<?php	}
														else
														{?>
														<option value="<?php echo $row->m_api_id;?>"><?php echo $row->m_api_name;?></option>
												<?php
														}
													}?>
											</select>
											<span id="divddapi" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">Select Service Type</label>
										<div class="col-md-6">
											<select id="ddapi_service" name="ddapi_service" class="form-control input-sm opt">
												<option selected="selected" value="-1">Select Service Type</option>
												<?php foreach($services->result() as $row)
													{
														if($apiservice->Servicetype_id == $row->Service_type_id)
														{?>
															<option value="<?php echo $row->Service_type_id;?>" selected><?php echo $row->Service_type;?></option>
												<?php	
														}
														else
														{?>
															<option value="<?php echo $row->Service_type_id;?>"><?php echo $row->Service_type;?></option>
												<?php
														}
													}
												?>
											</select>
											<span id="divddapi_service" style="color:red;"></span>
										</div>
									</div>
									
									
									
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-4 col-md-9">
										<button type="button" onclick="SUBMITT('insert_data')" class="btn green">Submit</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													
<script>
	function SUBMITT(form_id)
	{
		if(check(form_id))
		{
			bootbox.confirm('Are you sure to Submit from?', function(result){
				if(result==true)
				{
					var dataa=$("#"+form_id).serialize();
					$.ajax(
					{
						type:"POST",
						url:"<?php echo base_url();?>index.php/api/update_api_service/<?php echo $apiservice->Service_id;?>",
						data:dataa,
						success: function(msg) {
							$('#stylized').html("<center><h2>"+msg+"</h2></center>")
							.fadeIn(1500, function() {
								$('#stylized').append("<center><img id='checkmark' src='<?php echo base_url(); ?>/application/lib_images/check.png' /></center>");
								location.reload();
							});
						}
					}
					);
				}
			}); 
		}
		else
		{
			return false;
		}
		
	}
</script>