<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="#">Client API</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Manage API Services Operator</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Manage API Services Operator</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Manage API Services Operator</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" id="insert_data" action="<?php echo base_url().'index.php/api/add_api_operator'?>" method="post" onsubmit="return check(conwv('myform'))">
								<div class="form-body" id="city">
									<div class="form-group">
										<label class="col-md-4 control-label">Select API</label>
										<div class="col-md-6">
											<select id="ddapi" name="ddapi" class="form-control input-sm opt" onChange="fill_st()">
												<option selected="selected" value="-1">Select API</option>
												<?php foreach($rec->result() as $row)
													{
													?><option value="<?php echo $row->m_api_id;?>"><?php echo $row->m_api_name;?></option>
													<?php
													}
												?>
											</select>
											<span id="divddapi" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">Select Service Type</label>
										<div class="col-md-6" id="st">
											<select id="ddapi_service" name="ddapi_service" class="form-control input-sm opt" onChange="fill_sp()">
												<option selected="selected" value="-1">Select Service Type</option>
											</select>
											<span id="divddapi_service" style="color:red;"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-4 control-label">Select Service Provider</label>
										<div class="col-md-6" id="SP">
											<select id="ddsp" name="ddsp" class="form-control input-sm opt">
												<option selected="selected" value="-1">Select Service Type</option>
											</select>
											<span id="divddsp" style="color:red;"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-4 control-label">Operator Code</label>
										<div class="col-md-6">
											<input type="text" id="txtoptcode" name="txtoptcode" placeholder="Operator Code Max 25 characters..." id="txtoptcode" class="form-control input-sm empty" id="myform" >
											<span id="divtxtoptcode" style="color:red;"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-4 control-label">API Status</label>
										<div class="col-md-6">
											<div class="radio-list">
												<label class="radio-inline">
												<input type="radio" name="rbapi" id="rbapi1" value="1" checked> Enable</label>
												<label class="radio-inline">
												<input type="radio" name="rbapi" id="rbapi2" value="0"> Disable</label>
											</div>
										</div>
									</div>
									
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-4 col-md-9">
										<button type="button" onclick="conwv('insert_data')" class="btn green">Submit</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-edit font-purple"></i>
								<span class="caption-subject bold uppercase">View API Service Operator</span>
							</div>
							<div class="tools">
								<button class="btn btn-sm blue" onclick="change_status('api','st_chserop')">Enable<i class="fa fa-check"></i></button>
							</div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
								<thead>
									<tr>
										<th>
											<input type="checkbox" id="checkAll" onclick="chbcheckall()" class="group-checkable">
										</th>
										<th>S No.</th>
										<th>API</th>
										<th>Service</th>
										<th>Operator</th>
										<th>Code</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="userid">
									<?php
										$sn=1;
										foreach($api_operator->result() as $oprow)
										{
											
										?>
										<tr>
											<td>
												<?php 
													if($oprow->So_status==0)
													{
													?>
													<input type="checkbox" class="checkboxes" id="<?php echo $oprow->So_id;?>" onclick="chbchecksin()" >
													<?php
													}
													else if($oprow->So_status==1)
													{
													?>
													<input type="checkbox" class="checkboxes" id="<?php echo $oprow->So_id;?>" onclick="chbchecksin()" checked>
													<?php
													}
												?>
											</td>
											<td><?php echo $sn++; ?></td>
											<td><?php echo $oprow->Api_name; ?></td>
											<td><?php echo $oprow->Service_type; ?></td>
											<td><?php echo $oprow->Sp_name; ?></td>
											<td><?php echo $oprow->So_code;?></td>
											<td>
												<a onclick="loadmodal('<?php echo base_url();?>index.php/api/edit_api_operator/<?php echo $oprow->So_id;?>')" class="label label-success" data-toggle="modal" href="#basic">
												EDIT</a>
												
											</td>
										</tr>
										<?php
										}
										?>
								</tbody>
							</table>
						<input type="hidden" id="txtquid" name="txtquid">
							<input type="hidden" id="txtunchk" name="txtunchk">
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	
<!-- /.modal -->
<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Edit Services API Operator</h4>
			</div>
			<div id="useraddress" >
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script>

	function fill_st()
	{
		$("#ddapi_service").empty();
		var api_id=$("#ddapi").val();
		//alert(api_id);
		if(api_id != '-1')
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url(); ?>index.php/api/select_st/"+api_id,
				dataType: "JSON",
				data:{'api_id':api_id},
				success:function(data){
					$("#ddapi_service").append("<option value=-1> ~~ Select ~~</option>");
					$.each(data,function(i,item)
					{
						$("#ddapi_service").append("<option value="+item.Service_id+'_'+item.Servicetype_id+">"+item.Service_type+"</option>");
					});
				}
			});
		}
		else
		{
			$("#ddapi_service").append("<option value=-1> ~~ Select ~~</option>");
			bootbox.alert("Please First Select The API Name");
		}
	}
</script>
<script>
	function fill_sp()
	{
		$("#ddsp").empty();
		var st_id=$("#ddapi_service").val();
		if(st_id!='-1')
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url(); ?>index.php/api/select_srv_provider/"+st_id,
				dataType: "JSON",
				data:{'st_id':st_id},
				success:function(data){
					$("#ddsp").append("<option value=-1> ~~ Select ~~</option>");
					$.each(data,function(i,item)
					{
						$("#ddsp").append("<option value="+item.Sp_id+">"+item.Sp_name+"</option>");
					});
				}
			});
		}
		else
		{
			$("#ddsp").append("<option value=-1> ~~ Select ~~</option>");
			bootbox.alert("Please First Select Service Type");
		}
		
	}
</script>

<script>
	function loadmodal(url)
	{
		$("#useraddress").load(url);
	}
</script>
<script src="<?php echo base_url() ?>application/libraries/js/checkboxscipt.js"></script>