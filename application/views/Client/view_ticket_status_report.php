<script>
window.onload=function(){
var no=$('#txtdep').val();
if(no=='Support')
$("#txtdepartment").prop("selectedIndex",1);
if(no=='Sales')
$("#txtdepartment").prop("selectedIndex",2);
if(no=='Development')
$("#txtdepartment").prop("selectedIndex",3);
if(no=='Complaints')
$("#txtdepartment").prop("selectedIndex",4);
};
</script>
<div id="stylized">
<input type="hidden" id="txtdep" name="txtdep" value="<?php echo $depart;?>">
    <!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
		<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Admin Panel 
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo base_url();?>index.php/admin/dashboard/">
								Admin
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?php echo base_url();?>index.php/admin/index/">
								All Ticket
							</a>
							
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="">
								<i class="fa fa-calendar"></i>
								<span></span>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                	<div class="note note-success">
						<p>
                         <form method="post" action="<?php echo base_url();?>index.php/admin/search_status_report" name="status" id="status">
                        <div class="form-body">
							 <div class="form-group">
							 <label class="col-md-1 control-label">From Date</label>
										<div class="col-md-3">
                                            <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="" name="txtfromdate" id="txtfromdate" data-date-format="yyyy-mm-dd"/>
										</div>
										<label class="col-md-1 control-label">To Date</label>
										<div class="col-md-3">
                                            <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="" name="txttodate" id="txttodate" data-date-format="yyyy-mm-dd"/>
										</div>
										<label class="col-md-1 control-label">Status</label>
										<div class="col-md-3">
                                            <select name="ddstatus" id="ddstatus" class="form-control input-medium">
                                              <option value="">Select</option>
                                              <option value="0">RESOLVE</option>
                                              <option value="1">PENDING</option>
                                             
                                            </select>
										</div>
										<br><br><br>
				
									<div class="form-group">
									
										<label class="col-md-6 control-label"></label>
										
                                        <input type="submit" name="submit" value="Submit" class="btn green"  />
									</div>
                            </div>
						</p>
                        </form>
					</div>
					
					
			</div>
			<div id="display">
					
					</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
</div>
<script>
function save(id)
{
    var hidden = $('#hd'+id).val();
	var empname = $('#ddemp_name'+id).val();
 	$.ajax(
        {
 	type: "POST",
 	url:"<?php echo base_url();?>index.php/admin/task_assign/",
 	data:"&hd="+hidden+"&ddemp_name="+empname,
 	success: function(msg) {
 	 if(msg="true")
		    {
			$(".page-content").html("<center><h2>Task Assign Successfully!</h2></center>")
			.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/img/loading-spinner-blue.gif' /></center>")
			.hide()
			.fadeIn(1000,function()
			{
			
			$("#stylized").load("<?php echo base_url().'index.php/admin/after_index'?>");
			
			}
			);

	}	  
         }
       });
	
}


</script>

<script>
function printDiv()
{
  $('td:nth-child(2)').hide();
  $('td:nth-child(2),th:nth-child(2)').hide();
  var divToPrint=document.getElementById('sample_2');
  newWin= window.open("");
  newWin.document.write(divToPrint.outerHTML);
  newWin.print();
  newWin.close();
  $('td:nth-child(2)').show();
  $('td:nth-child(2),th:nth-child(2)').show();
}
</script>

<script>
function demoFromHTML() {
    var pdf = new jsPDF('p', 'pt', 'letter');
    // source can be HTML-formatted string, or a reference
    // to an actual DOM element from which the text will be scraped.
    source = $('#data1')[0];

    // we support special element handlers. Register them with jQuery-style 
    // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
    // There is no support for any other type of selectors 
    // (class, of compound) at this time.
    specialElementHandlers = {
        // element with id of "bypass" - jQuery style selector
        '#bypassme': function (element, renderer) {
            // true = "handled elsewhere, bypass text extraction"
            return true
        }
    };
    margins = {
        top: 0,
        bottom: 0,
        left: 40,		
        width: 1280
    };
    // all coords and widths are in jsPDF instance's declared units
    // 'inches' in this case
    pdf.fromHTML(
    source, // HTML string or DOM elem ref.
    margins.left, // x coord
    margins.top, { // y coord
        'width': margins.width, // max width of content on PDF
        'elementHandlers': specialElementHandlers
    },

    function (dispose) {
        // dispose: object with X, Y of the last line add to the PDF 
        //          this allow the insertion of new lines after html
        pdf.save('Test.pdf');
    }, margins);
}
</script>
<script>
function display_ticket()
{
	var status=$('#ddstatus').val();
	var txttodate=$('#txttodate').val();
	var txtfromdate=$('#txtfromdate').val();
	$('#display').load('<?php echo base_url();?>index.php/admin/search_status_report/'+status+'/'+txttodate+'/'+txtfromdate);
}
</script>
 