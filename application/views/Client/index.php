
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Client Panel 
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">
								Dashboard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								All Ticket
							</a>
							
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                <div class="note note-success">
						<p>
							 <form method="post" action="<?php echo base_url();?>index.php/client/index" name="status" id="status" class="horizontal-form">
                                
                                <div class="row">
												<div class="form-body">
                                                    <div class="form-group">
                                                        <label class="col-md-1 control-label">Category</label>
                                                        <div class="col-md-3">
                                                            <select name="txtcategory" id="txtcategory" class="form-control input-medium">
                                                              <option value="1">Active</option>
                                                              <option value="0">Deactive</option>
                                                            </select>
                                                        </div>
                                                        <input type="submit" name="submit" class="btn blue" />
                                                        <a href="<?php echo base_url();?>index.php/client/ticket">
                                                        	<input type="button" name="submit" value="Submit Ticket" class="btn red" style="float:right; margin-right:30px;" />   
                                                         </a>
                                                    </div>
                                                 </div>
													<!--/span-->
                                 </div>
                                             
                            </form>
                           
						</p>
                        
					</div>
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>View Ticket
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							
								<table class="table table-striped table-bordered table-hover" id="sample_2">
								<thead>
								<tr>
									<th>
										 #
									</th>
									
									<th>
										 Subject
									</th>
									<th>
										 Status
									</th>
									<th>
										 Urgency
									</th>
									<th>
										 Date
									</th>
									
								</tr>
								</thead>
								<tbody>
                                 <?php  
							$sn=1;
										foreach($rec->result() as $row)
										{
											
												if(($row->txtstatus)=='1')
													{
														$status='Active';
														
														
								  ?>
								<tr class="odd gradeX">
                                	<td>
                                    	 <?php echo $sn; ?>
                                    </td>
									<td>
										 <a href="<?php echo base_url();?>index.php/client/view_ticket/<?php echo $row->ticket_no ?>" style="color:#FF0000; text-decoration:none;">
												<?php echo '#'.($row->ticket_no).' - '.$row->txtsubject ?>
                                        </a>
									</td>
									<td>
										<?php echo $status; ?>
									</td>
									<td>
										 <?php echo $row->txturgency ?>
									</td>
									<td>
										 <?php echo $row->adddate ?>
									</td>
									
								</tr>
                                <?php
								$sn++;
				  					}
									else if(($row->txtstatus)=='0') 
									{
										$status='Deactive';
										?>
                                 <tr>
											<td><?php echo $sn; ?></td>
											<td><a href="<?php echo base_url();?>index.php/client/view_ticket/<?php echo $row->ticket_no ?>" style="color:#FF0000; text-decoration:none;">
														<?php echo '#'.($row->ticket_no).' - '.$row->txtsubject ?>
												</a></td>
											<td><?php echo $status; ?></td>
											<td><?php echo $row->txturgency ?></td>
                                            <td><?php echo $row->adddate ?></td>
								</tr>
                                
                                
                                
                                 <?php
								 $sn++;
										  }
										}
								  ?>
								</tbody>
								</table>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
					
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->