<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		

			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Client Panel 
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="btn-group">
							<button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							<span>
								Actions
							</span>
							<i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="#">
										Action
									</a>
								</li>
								<li>
									<a href="#">
										Another action
									</a>
								</li>
								<li>
									<a href="#">
										Something else here
									</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">
										Separated link
									</a>
								</li>
							</ul>
						</li>
						<li>
							<i class="fa fa-home"></i>
							<a href="#">
								Dashboard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Ticket Category
							</a>
							
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12 blog-page">
					
							<h1>Ticket Type</h1>
                            <div class="row">
						<div class="col-md-3">
                        
							<div class="top-news">
								<a href="<?php echo base_url();?>index.php/client/submit_ticket/1" class="btn red">
									<span>
										 Support Ticket
									</span>
									<em>Technical Assistance Related<br />
										 to your Domain Registration,<br />
                                          Web Hosting and Servers</em>
									<i class="fa fa-book top-news-icon"></i>
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="top-news">
								<a href="<?php echo base_url();?>index.php/client/submit_ticket/2" class="btn green">
									<span>
										 Sales Ticket
									</span>
									<em>Pre Sales, Sales Assistance</em>
									<em>
									<br />
                                    <br />
                                     </em>
									<i class="fa fa-book top-news-icon"></i>
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="top-news">
								<a href="<?php echo base_url();?>index.php/client/submit_ticket/3" class="btn blue">
									<span>
										 Development Ticket
									</span>
									<em>Web Design,<br />
										Web Development,<br /> SEO/SEM Queries</em>
									<em>
									</em>
									<i class="fa fa-book top-news-icon"></i>
								</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="top-news">
								<a href="<?php echo base_url();?>index.php/client/submit_ticket/4" class="btn yellow">
									<span>
										 Complaints Ticket
									</span>
									<em>Abuse issues, complaints</em>
									<em>
									<br />
                                    <br />
                                    </em>
									<i class="fa fa-book top-news-icon"></i>
								</a>
							</div>
						</div>
					</div>
						
				</div>
			</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->