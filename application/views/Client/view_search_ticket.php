<div id="stylized">
<input type="hidden" id="txtdep" name="txtdep" value="<?php echo $depart;?>">
    <!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Admin Panel 
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo base_url();?>index.php/admin/dashboard/">
								Admin
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?php echo base_url();?>index.php/admin/index/">
								All Ticket
							</a>
							
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="">
								<i class="fa fa-calendar"></i>
								<span></span>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- BEGIN PAGE HEADER-->
		<div class="row">
		<div class="col-md-12 ">
					<div class="portlet box blue ">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Search All Ticket
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form action="<?php echo base_url();?>index.php/admin/search_ticket_report" method="post" class="horizontal-form"  id="form_sample_1" >
											<div class="form-body">
												
                                                <div class="row">
													
													<div class="col-md-3">
														<div class="form-group">
															<label class="control-label">From Date</label>
															<input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="" name="txtfromdate" id="txtfromdate" data-date-format="yyyy-mm-dd"/>								
														</div>
													</div>
                                                    
                                                    <div class="col-md-3">
														<div class="form-group">
															<label class="control-label">To Date</label>
															<input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="" name="txttodate" id="txttodate" data-date-format="yyyy-mm-dd"/>										
														</div>
													</div>
                                                    
                                                    <div class="col-md-3">
														<div class="form-group">
														
															<label class="control-label">Department </label>
															<select name="txtdepartment" id="txtdepartment" class="form-control input-medium">
															  <option value="">Select</option>
															  <option value="1">Support</option>
															  <option value="2">Sales</option>
															  <option value="3">Development</option>
															  <option value="4">Complaints</option>
															</select>		
																
														</div>
													</div>
                                                    
                                                    <div class="col-md-3">
													
													
													
													
														<div class="form-group">
															<label class="control-label">Status</label>
															 <select name="ddstatus" id="ddstatus" class="form-control input-medium">
															  <option value="-1">Select</option>
															  <option value="1">ACTIVE</option>
															  <option value="0">DEACTIVE</option>
															 
															</select>										
														</div>
													</div>
                                                  </div>
													<!--/span-->
													    <div class="row">
													
													<div class="col-md-3">
														<div class="form-group">
															<label class="control-label">Assign Person</label>
															<select name="ddassign" id="ddassign" class="form-control input-medium">
																  <option value="">Select</option>
																  <?php foreach($emp->result() as $row)
																	{?>
																  <option value="<?php echo $row->or_m_reg_id;?>"><?php echo $row->or_m_name;?></option>
																	<?php	}?>
															</select>										
														</div>
													</div>
                                                    
                                                    <div class="col-md-3">
														<div class="form-group">
															<label class="control-label">Account Type</label>
															<select name="ddactype" id="ddactype" class="form-control input-medium">
															  <option value="">Select</option>
															  <option value="0">External User</option>
															  <option value="1">Admin</option>
															  <option value="2">Client</option>
															 
															</select>												
														</div>
													</div>
                                            </div>    
										
											</div>
											<div id="function" class="form-actions right">
												<button type="button" class="btn default">Cancel</button>
												<button type="button" class="btn blue" onclick="display_ticket()"><i class="fa fa-check"></i> Search</button>
											</div>
										</form>
					
							</div>
						</div>
						
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Ticket
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							
                            <div class="table-responsive" id="data1">
							<table class="table table-striped table-bordered table-hover table-full-width">
							<thead>
							<tr>
								<th>
									 S No.
								</th>
								<th>
									 Department
								</th>
								<th>
									 Subject
								</th>
								<th>
									 Status
								</th>
								<th>
									 Urgency
								</th>
                                <th>
									 Assign to
								</th>
                                <th>
									Date
								</th>
                               
							</tr>
								</thead>
								<tbody id="fetch">
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</tbody>
							</table>
						</div>
						</div>
					</div>
					<div id="display">
					
					</div>
                    </div>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
</div>

<script>
function display_ticket()
{
	var txtdepartment=$('#txtdepartment').val(),
		txttodate=$('#txttodate').val(),
		txtfromdate=$('#txtfromdate').val(),
		ddstatus=$('#ddstatus').val(),
		ddassign=$('#ddassign').val(),
		ddactype=$('#ddactype').val();
			
		$.ajax(
			{
			type:"POST",
			url:"<?php echo base_url(); ?>index.php/client/search_ticket_report/",
			dataType: 'json',
			data: {'txtdepartment': txtdepartment,'txttodate':txttodate,'txtfromdate':txtfromdate,'ddstatus':ddstatus,'ddassign':ddassign,'ddactype':ddactype},
			success: function(data) {
					$("#fetch").empty();
					$.each(data,function(i,item)
					{
						$("#fetch").append("<tr><td>"+item.SN+"</td><td>"+item.department+"</td><td>"+item.subjects+"</td><td>"+item.status+"</td><td>"+item.urgency+"</td><td>"+item.user_name+"</td><td>"+item.sub_date+"</td></tr>");
					});  
				}
			});
}
</script>
 