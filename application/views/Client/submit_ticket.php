<?php  
							
	foreach($rec->result() as $row)
	{
		if(($row->contact_id)!=null)
		{
			$name=$row->contact_name;
			$email=$row->contact_email;
		}
	}
	
	if($dep=='1')
	{
		$department='Support';
	}
	if($dep=='2')
	{
		$department='Sales';
	}
	if($dep=='3')
	{
		$department='Development';
	}
	if($dep=='4')
	{
		$department='Complaints';
	}									
?>
<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Client Panel 
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">
								Dashboard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Submit Ticket
							</a>
							
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
			<!--row-->
           
            <div class="col-md-7">
            	<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Submit Ticket
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/upload/ticket_submit" method="post">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Name</label>
										<div class="col-md-9">
											<input type="text" name="txtname" id="txtname" value="<?php echo $name ?>" readonly="readonly" class="form-control input-large" />
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Email Address</label>
										<div class="col-md-9">
											<input type="text" name="txtemail" id="txtemail" value="<?php echo $email ?>" readonly="readonly" class="form-control input-large" />
										</div>
									</div>
                                     <div class="form-group">
										<label class="col-md-3 control-label">Department</label>
										<div class="col-md-9">
                                            <input type="text" name="txtdepartment" id="txtdepartment" value="<?php echo $department ?>" readonly="readonly" class="form-control input-large" />
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Subject</label>
										<div class="col-md-9">
                                            <input type="text" name="txtsubject" id="txtsubject" class="form-control input-large" />
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Urgency</label>
										<div class="col-md-9">
                                            <select name="txturgency" id="txturgency" class="form-control input-large">
                                                <option value="1">High</option>
                                                <option value="2" selected="selected">Medium</option>
                                                <option value="3">Low</option>
                                              </select>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Comment</label>
										<div class="col-md-9">
                                            <textarea rows="7" name="txtdiscription" id="txtdiscription" cols="50" class="form-control input-large"></textarea>
										</div>
									</div>
                                    <div class="form-group">
										<label class="col-md-3 control-label">Attachments</label>
										<div class="col-md-9">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
												<div class="input-group input-large">
													<div class="form-control uneditable-input span3" data-trigger="fileinput">
														<i class="fa fa-file fileinput-exists"></i>&nbsp;
														<span class="fileinput-filename">
														</span>
													</div>
													<span class="input-group-addon btn default btn-file">
														<span class="fileinput-new">
															 Select file
														</span>
														<span class="fileinput-exists">
															 Change
														</span>
														<input type="file"  name="userfile" id="userfile">
													</span>
													<a href="#" class="input-group-addon btn default fileinput-exists" data-dismiss="fileinput">
														 Remove
													</a>
												</div>
											</div>
										</div>
									</div>
                                </div>
									<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<?php
                                            $ticket_no=rand(100000, 999999);
                                        ?>
										<input type="hidden" value="<?php echo $ticket_no ?>" name="txtticket" id="txtticket"/>
										<button type="submit" class="btn blue">Submit</button>
										<button type="button" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
            </div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
