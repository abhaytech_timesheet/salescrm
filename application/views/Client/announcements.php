
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Modal title</h4>
					</div>
					<div class="modal-body">
						Widget settings form goes here
					</div>
					<div class="modal-footer">
						<button type="button" class="btn blue">Save changes</button>
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Client Panel 
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					<li class="btn-group">
						<button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							<span>
								Actions
							</span>
							<i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu pull-right" role="menu">
							<li>
								<a href="#">
									Action
								</a>
							</li>
							<li>
								<a href="#">
									Another action
								</a>
							</li>
							<li>
								<a href="#">
									Something else here
								</a>
							</li>
							<li class="divider">
							</li>
							<li>
								<a href="#">
									Separated link
								</a>
							</li>
						</ul>
					</li>
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">
							Dashboard
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">
							Announcement
						</a>
						
					</li>
					
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<ul class="timeline">
                    <?php  
						
						foreach($rec->result() as $row)
						{
							if(($row->id)!=null)
							{
								
							?>
							<li class="timeline-blue">
								<div class="timeline-time">
									<span class="date">
										<?php echo substr($row->adddate,0,10) ?>
									</span>
									<span class="time">
										<?php echo substr($row->adddate,11,20) ?>
									</span>
								</div>
								<div class="timeline-icon">
									<i class="fa fa-bar-chart-o"></i>
								</div>
								<div class="timeline-body">
									<h2><?php echo $row->txttitle ?></h2>
									<div class="timeline-content">
										<?php echo substr($row->txtdescription,0,300) ?>
									</div>
									<div class="timeline-footer">
										<a href="<?php echo base_url();?>index.php/client/full_Announcements/<?php echo $row->id ?>" class="nav-link">
											Read more <i class="m-icon-swapright m-icon-white"></i>
										</a>
									</div>
								</div>
							</li>
							<?php
							}
						}
					?>
				</ul>
			</div>
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<script type="text/javascript">
	function MM_jumpMenu(targ,selObj,restore){ //v3.0
		eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
		if (restore) selObj.selectedIndex=0;
	}
</script>