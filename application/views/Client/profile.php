<?php  
							
											foreach($rec2->result() as $row)
											{
												break; 
											}
					 ?>

	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Registration Form 
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">
								Dashbord
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Form
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom boxless tabbable-reversed">
						
						
							
							<div class="tab-pane" id="tab_1">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Registration Form
										</div>
										<div class="tools">
											<a href="javascript:;" class="collapse">
											</a>
											<a href="#portlet-config" data-toggle="modal" class="config">
											</a>
											<a href="javascript:;" class="reload">
											</a>
											<a href="javascript:;" class="remove">
											</a>
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<form action="<?php echo base_url();?>index.php/client/mydetail_update" class="horizontal-form" method="post">
											<div class="form-body">
												<h3 class="form-section">Personal Info</h3>
												<div class="row">
													<div class="col-md-4">
															<label class="control-label">First Name</label>
                                                            <div class="form-group">
															<input type="text" name="txtfname" id="txtfname" value="" class="form-control">
															
														</div>
													</div>
													<!--/span-->
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Last Name</label>
															<input type="text" name="txtlname" id="txtlname" value="" class="form-control" >
															
														</div>
													</div>
													<!--/span-->
                                                    
												</div>
												<!--/row-->
												<div class="row">
                                                    <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Company name</label>
                                                                <input type="text" name="txtcompany" id="txtcompany" value="" class="form-control" />
                                                                
                                                            </div>
                                                        </div>
                                                        
													<div class="col-md-4">
															<label class="control-label">Email Address</label>
                                                            <div class="form-group">
															<input type="email" name="txtemail" id="txtemail" value="" class="form-control" readonly="readonly" />
															
														</div>
													</div>
                                                </div>
													<!--row-->
                                                <div class="row">
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Counrty</label>
															<select name="txtcountry" id="txtcountry" class="form-control" onChange="get_state()">
                                                             		
                                                            </select>
															
														</div>
													</div>
													<!--/span-->
                                                    <div class="col-md-4">
														<div class="form-group">
															<label class="control-label">State</label>
															<div id="state">
                                                                <select name="txtstate" id="txtstate" class="form-control" onChange="get_city()">
                                                                    </select>
                                                             </div>
															
														</div>
													</div>
                                                    <div class="col-md-4">
														<div class="form-group">
															<label class="control-label">City</label>
															<div id="city">
                                                                <select name="txtcity" id="txtcity" class="form-control">
                                                                    </select>
                                                              </div>
															
														</div>
													</div>
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-4">
															<label class="control-label">Locality</label>
                                                            <div class="form-group">
															<input type="text" name="txtlocality" id="txtlocality" value="" class="form-control" />
															
														</div>
													</div>
													<!--/span-->
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Zip Code</label>
															<input type="text" name="txtzip" id="txtzip" maxlength="6" class="form-control" />
															
														</div>
													</div>
													<!--/span-->
                                                    <div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Phone No.</label>
															<input type="text" name="txtphone" id="txtphone"  maxlength="10" class="form-control" />
															
														</div>
													</div>
												</div>
                                                <div class="row">
													<div class="col-md-4">
                                                    	<div class="form-group">
															<label class="control-label">Chose Currency</label>
                                                            <select name="txtcurrency" id="txtcurrency" class="form-control">
                                                                <option value="INR">INR</option>
                                                                <option value="USD">USD</option>
                                                              </select>
														</div>
                                                    </div>
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-4 ">
														<div class="form-group">
															<label class="control-label">Payment Method</label>
															<select name="txtpaymentmethod" id="txtpaymentmethod" class="form-control">
                                                                <option value="none">Use Default (Set Per Order)</option>
                                                                <option value="paypal">PayPal</option>
                                                                <option value="ebs">Net Banking/Credit Card2</option>
                                                                <option value="bob">Credit Card/Debit Card</option>
                                                                <option value="banktransfer">Bank Transfer Detail</option>
                                                              </select>
														</div>
													</div>
												
													<div class="col-md-4">
														<div class="form-group">
															<label>Default Billing Contact</label>
															<select name="txtbillingcid" id="txtbillingcid" class="form-control">
                                                                <option value="Use Default Contact (Details Above)">Use Default Contact (Details Above)</option>
                                                              </select>
														</div>
													</div>
                                                </div>
													
												<!--/row-->
												
											</div>
											<div class="form-actions right">
												<button type="button" class="btn default">Cancel</button>
												<button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
											</div>
										</form>
										<!-- END FORM-->
									</div>
								</div>
							</div>
							
						
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
