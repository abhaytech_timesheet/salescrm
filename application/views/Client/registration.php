	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Registration Form 
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="btn-group">
							<button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							<span>
								Actions
							</span>
							<i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="#">
										Action
									</a>
								</li>
								<li>
									<a href="#">
										Another action
									</a>
								</li>
								<li>
									<a href="#">
										Something else here
									</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">
										Separated link
									</a>
								</li>
							</ul>
						</li>
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">
								Dashbord
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Form
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom boxless tabbable-reversed">
						
						
							
							<div class="tab-pane" id="tab_1">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-reorder"></i>Registration Form
										</div>
										<div class="tools">
											<a href="javascript:;" class="collapse">
											</a>
											<a href="#portlet-config" data-toggle="modal" class="config">
											</a>
											<a href="javascript:;" class="reload">
											</a>
											<a href="javascript:;" class="remove">
											</a>
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<form action="<?php echo base_url();?>index.php/welcome/user_insert" class="horizontal-form" method="post">
											<div class="form-body">
												<h3 class="form-section">Personal Info</h3>
												<div class="row">
													<div class="col-md-4">
															<label class="control-label">First Name</label>
                                                            <div class="form-group">
															<input type="text" name="txtfname" id="txtfname" class="form-control">
															
														</div>
													</div>
													<!--/span-->
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Last Name</label>
															<input type="text" name="txtlname" id="txtlname" class="form-control" >
															
														</div>
													</div>
													<!--/span-->
                                                    
												</div>
												<!--/row-->
												<div class="row">
                                                    <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="control-label">Company name</label>
                                                                <input type="text" name="txtcompany" id="txtcompany" class="form-control" />
                                                                
                                                            </div>
                                                        </div>
                                                        
													<div class="col-md-4">
															<label class="control-label">Email Address</label>
                                                            <div class="form-group">
															<input type="email" name="txtemail" id="txtemail" class="form-control" />
															
														</div>
													</div>
                                                </div>
													<!--row-->
                                                <div class="row">
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Counrty</label>
															<select name="txtcountry" id="txtcountry" class="form-control" onChange="get_state()">
                                                              <option value="-1">Select</option>
                                                              <?php
                                                              foreach($rec->result() as $row)
                                                              {
                                                              ?>
                                                              <option value="<?php echo $row->m_loc_id ?>"><?php echo $row->m_loc_name ?></option>
                                                              <?php
                                                              }
                                                              ?>
                                                            </select>
															
														</div>
													</div>
													<!--/span-->
                                                    <div class="col-md-4">
														<div class="form-group">
															<label class="control-label">State</label>
															<div id="state">
                                                                <select name="txtstate" id="txtstate" class="form-control" onChange="get_city()">
                                                                  <option value="-1">Select</option>
                                                                </select>
                                                             </div>
															
														</div>
													</div>
                                                    <div class="col-md-4">
														<div class="form-group">
															<label class="control-label">City</label>
															<div id="city">
                                                                <select name="txtcity" id="txtcity" class="form-control">
                                                                  <option value="-1">Select</option>
                                                                </select>
                                                              </div>
															
														</div>
													</div>
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-4">
															<label class="control-label">Locality</label>
                                                            <div class="form-group">
															<input type="text" name="txtlocality" id="txtlocality" class="form-control" />
															
														</div>
													</div>
													<!--/span-->
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Zip Code</label>
															<input type="text" name="txtzip" id="txtzip" maxlength="6" class="form-control" />
															
														</div>
													</div>
													<!--/span-->
                                                    <div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Phone No.</label>
															<input type="text" name="txtphone" id="txtphone" class="form-control" />
															
														</div>
													</div>
												</div>
                                                <div class="row">
													<div class="col-md-4">
                                                    	<div class="form-group">
															<label class="control-label">Chose Currency</label>
                                                            <select name="txtcurrency" id="txtcurrency" class="form-control">
                                                                <option value="-1">Select</option>
                                                                <option value="INR">INR</option>
                                                                <option value="USD">USD</option>
                                                              </select>
														</div>
                                                    </div>
												</div>
												<!--/row-->
												<h3 class="form-section">Add Login details</h3>
												<div class="row">
													<div class="col-md-4 ">
														<div class="form-group">
															<label class="control-label">Password</label>
															<input type="password" id="password" name="password"  class="form-control input-large" />
														</div>
													</div>
												
													<div class="col-md-4">
														<div class="form-group">
															<label>Confirm Password</label>
															<input type="password" name="txtconfirm" id="txtconfirm"  class="form-control input-large" />
														</div>
													</div>
                                                </div>
													<!--/span-->
                                                   <div class="row">
													<div class="col-md-6">
														<div class="form-group">
															
															<input name="txtcondition" id="txtcondition" type="checkbox" value="1"> &nbsp; I have read and agree to the Terms of Service .
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												
											</div>
											<div class="form-actions right">
												<button type="button" class="btn default">Cancel</button>
												<button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
											</div>
										</form>
										<!-- END FORM-->
									</div>
								</div>
							</div>
							
						
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
