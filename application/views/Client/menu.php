<!-- BEGIN BODY -->
<body class="page-header-fixed">
<!-- BEGIN HEADER -->
<div class="header navbar navbar-fixed-top">
	<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="header-inner">
		<!-- BEGIN LOGO -->
	<div class="navbar-brand" style="margin-top: -30px; margin-left: 33px; width:400px;">
           <h1 style="font-size: 30px;" class="fontsforweb_fontid_17281"> 
               <a href="http://www.metroheights.co.in" title="Metro Heights" style="text-decoration:none;"> 
               <span style="color:#FFFFFF;">Metroheights</span></a></h1>
           <h6 style="margin-top:-20px; font-size:9px; color:#FFF;">India's Leading Portal.</h6>
   </div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<img src="<?php echo base_url() ?>application/libraries/assets/img/menu-toggler.png" alt=""/>
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<ul class="nav navbar-nav pull-right">
		<!-- BEGIN USER LOGIN DROPDOWN -->
			<li class="dropdown user">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<img alt="" src="<?php echo base_url() ?>application/libraries/assets/img/avatar1_small.jpg"/>
					<span class="username">
						 <?php echo $this->session->userdata('e_email'); ?>
					</span>
					<i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu">
					<li>
						<a href="<?php echo base_url();?>index.php/affiliates/view_account_profile">
							<i class="fa fa-user"></i> My Profile
						</a>
					</li>
					<li>
						<a href="<?php echo base_url();?>index.php/affiliates/change_password">
							<i class="fa fa-arrows"></i> Change Password
						</a>
					</li>
					<li class="divider"></li>
					<li>
						<a href="javascript:;" id="trigger_fullscreen">
							<i class="fa fa-arrows"></i> Full Screen
						</a>
					</li>
					
					<li>
						<a href="<?php echo base_url();?>index.php/auth/logout">
							<i class="fa fa-key"></i> Log Out
						</a>
					</li>
				</ul>
			</li>
			<!-- END USER LOGIN DROPDOWN -->
		</ul>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<div id="menu" class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler hidden-phone">
					</div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
				<li class="sidebar-search-wrapper">
					<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
					<form class="sidebar-search" action="extra_search.html" method="POST">
						<div class="form-container">
							<div class="input-box">
								<a href="javascript:;" class="remove">
								</a>
								<input type="text" placeholder="Search..."/>
								<input type="button" class="submit" value=" "/>
							</div>
						</div>
					</form>
					<!-- END RESPONSIVE QUICK SEARCH FORM -->
				</li>
				<li class="start active" id="1">
					<a href="<?php echo base_url();?>index.php/client/index">
						<i class="fa fa-home"></i>
						<span class="title">
							Dashboard
						</span>
					</a>
				</li>
			<?php
		foreach($menu->result() as $m)
		{
		?>
	    <li><a href="javascript:;"><i class="<?php echo $m->MENU_ICON; ?>"></i> <span class="title"><?php echo $m->MENU_NAME; ?></span> <span class="arrow "></span> </a>
        <ul class="sub-menu">
		<?php
 
				foreach($sub->result() as $s)
				{
					if($s->MENU_NAME==$m->MENU_NAME)
					{
		?>
        <li><a href="<?php echo base_url() ?>index.php/<?php echo $s->LINK;?>/" ><i class="fa fa-bullhorn"></i><?php echo $s->SUBMENU ;?></a></li>
		<?php
					}
				}
		?>
		</ul>
		</li>
		<?php
		}
		?>
				
			</ul>
			<!-- END SIDEBAR MENU -->
		
		</div>
	</div>
	<!-- END SIDEBAR -->
	
