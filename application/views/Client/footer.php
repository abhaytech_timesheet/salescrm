<!-- BEGIN FOOTER -->
<div class="footer">
	<div class="footer-inner">
 <?php echo date('Y') ?> &copy; Ferry Info Pvt. Ltd.
	</div>
	<div class="footer-tools">
		<span class="go-top">
			<i class="fa fa-angle-up"></i>
		</span>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/respond.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-migrate-1.2.1.min.js"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery.blockui.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/uniform/jquery.uniform.min.js"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/select2/select2.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/data-tables/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/core/app.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/form-samples.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/table-managed.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/components-form-tools.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/fuelux/js/spinner.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/typeahead/handlebars.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/typeahead/typeahead.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/components-editors.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/components-pickers.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jqvmap/jqvmap/jquery.vmap.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-markdown/lib/markdown.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/tasks.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
   App.init();
   TableManaged.init();
   Index.init();
   Index.initDashboardDaterange();
   Index.initCalendar();
   Index.initIntro();
   Index.initJQVMAP();
   Tasks.initDashboardWidget();
   FormSamples.init();
   ComponentsFormTools.init(); 
   ComponentsEditors.init();
   ComponentsPickers.init();
   
});
</script>
<!-- END JAVASCRIPTS -->

<!--own script-->
<script>
function get_state()
{	
	var countryid=document.getElementById("txtcountry").value;
	$("#state").load('<?php echo base_url();?>index.php/master_loaction/get_state/'+countryid);
}
</script>
<script>
function get_city()
{
	var stateid=document.getElementById("txtstate").value;
	$("#city").load('<?php echo base_url();?>index.php/master_loaction/get_city/'+stateid);
}
</script>
<!--/own script-->
</body>
<!-- END BODY -->
</html>