<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">	
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Client Panel 
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">
							Dashboard
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">
							Change Password
						</a>
						
					</li>
					
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<!--row-->
            <?php 
				foreach($rec->result() as $row)
				{
					$p=$row->or_login_pwd;
					break;
				}
			?>
            <div class="col-md-7">
            	<!-- BEGIN SAMPLE FORM PORTLET-->
				<div class="portlet box blue">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i> Change Password
						</div>
						<div class="tools">
							<a href="" class="collapse">
							</a>
							<a href="#portlet-config" data-toggle="modal" class="config">
							</a>
							<a href="" class="reload">
							</a>
							<a href="" class="remove">
							</a>
						</div>
					</div>
					<div class="portlet-body form">
						<form role="form" class="form-horizontal" action="<?php echo base_url();?>index.php/client/password_update/" method="post">
                            <input type="hidden" value="<?php echo $p;?>" name="op" id="op" />
							<div class="form-body">
								<div class="form-group">
									<label class="col-md-3 control-label">Existing Password</label>
									<div class="col-md-9">
										<input type="password" name="txtepassword" id="txtepassword" class="form-control input-large" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">New Password</label>
									<div class="col-md-9">
										<input type="password" name="oldpassword" id="oldpassword" class="form-control input-large" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Confirm New Password</label>
									<div class="col-md-9">
										<input type="password" name="txtconfirm" id="txtconfirm" class="form-control input-large" />
									</div>
								</div>
							</div>
							<div class="form-actions fluid">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn blue">Submit</button>
									<button type="button" class="btn default">Cancel</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- END SAMPLE FORM PORTLET-->
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
