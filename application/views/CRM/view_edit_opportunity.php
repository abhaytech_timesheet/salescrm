<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_opportunity/0">
							CRM
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="icon-briefcase"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_opportunity/0">
							Opportunity
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-edit"></i>
						<span>
							Edit Opportunity 
						</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Edit Opportunity </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Edit Opportunity </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form  class="horizontal-form" action="#" id="form_sample_1">
								
								<?php
									foreach($info->result() as $row)
									{
										break;
									}
								?>
								<div class="form-body">
									
									<h4 class="caption-subject font-blue bold uppercase">Opportunity Information</h4>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Subject</label>
												<textarea placeholder="Enter Opportunity subject" class="form-control input-sm empty" name="txtsubj" style="font-size:20px" col="3" id="txtsubj"><?php echo $row->opportunity_subject; ?></textarea>
												<span id="divtxtsubj" style="color:red"></span>	
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Opportunity Owner</label>
												<input type="text" placeholder="Enter Owner Name" class="form-control input-sm empty" name="txtop_ownrname" id="txtop_ownrname" disabled>
												<input type="hidden"  class="form-control" name="txtop_ownr" id="txtop_ownr" value="<?php echo $row->opportunity_owner;?>"  >
												<span id="divtxtop_ownrname" style="color:red"></span>	
											</div>
										</div> 
										
									</div>
									
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Type</label>
												<select class="form-control input-sm opt" name="ddop_type" id="ddop_type" onchange="load_client()">
													<option value="-1">Select Type</option>
													<option <?php echo ($row->opportunity_type==1 ? 'selected' : ''); ?> value="1">Existing Buisness</option>
													<option <?php echo ($row->opportunity_type==2 ? 'selected' : ''); ?> value="2">New Buisness</option>
												</select>	
												<span id="divddop_type" style="color:red"></span>
											</div>
										</div> 
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Account Name</label>
												<span id="account_name"><input type="text" class="form-control input-sm" name="txtacc_name" id="txtacc_name" value="<?php echo $row->opportunity_account_name?>"/></span>	
												<span id="account_id" style="display:none" >
													<select class="form-control input-sm" name="ddacc_name" id="ddacc_name" >
														<option value="-1">Select Account</option>
													</select>
													
												</span><span id="divtxtacc_name" style="color:red"></span>	
											</div>
										</div> 
										
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Stage</label>
												<select class="form-control input-sm opt" name="ddop_stg" id="ddop_stg">
													<option value="-1">Select Stage</option>
													<option <?php echo ($row->opportunity_stage==1 ? 'selected' : ''); ?> value="1">Prospecting</option>
													<option <?php echo ($row->opportunity_stage==2 ? 'selected' : ''); ?> value="2">Qualification</option>
													<option <?php echo ($row->opportunity_stage==3 ? 'selected' : ''); ?> value="3">Needs Annalysis</option>
													<option <?php echo ($row->opportunity_stage==4 ? 'selected' : ''); ?> value="4">Value Proposition</option>
													<option <?php echo ($row->opportunity_stage==6 ? 'selected' : ''); ?> value="6">Loss</option>
												</select>		
												<span id="divddop_stg" style="color:red"></span>
											</div>
										</div>
									</div>
									<!--/row-->
									
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Lead For Product</label></br>
												<?php  $val=explode(',',$row->opportunity_for);?>
												<select class="bs-select form-control input-sm opt" name="ddenqtype[]" id="ddenqtype" multiple="multiple" onchange="get_project()">
													<option value="1" <?php echo (in_array('1',$val))?'selected':'';?>>Logistic</option>
													<option value="2" <?php echo (in_array('2',$val))?'selected':'';?>>Software</option>
													<option value="3" <?php echo (in_array('3',$val))?'selected':'';?>>Manpower</option>
												</select>
												
												<span id="divddenqtype" style="color:red"></span>																		
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Select Type</label>
												<?php  $webval=($row->opportunity_website_id!='' ? $row->opportunity_website_id:'');?>
												<select class="form-control input-sm" name="ddwebsite" id="ddwebsite" >
													<option value="">Select Type</option>
												</select>
												
												<span id="divddwebsite" style="color:red"></span>
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Select Product</label>
												<?php  $projval=$row->opportunity_project_id;?>
												<select class="form-control input-sm" name="ddproject" id="ddproject" >
													<option value="">Select Product Type</option>
													
												</select>
												
												<span id="divddproject" style="color:red"></span>
											</div>
										</div> 
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Select Services</label>
												<?php  $val1=explode(',',$row->opportunity_service_id);?>
												<select class="bs-select form-control input-sm" name="ddservices[]" id="ddservices" multiple="multiple" >
													<option value="-1">Select Services Type</option>
													
												</select>
												<!--<script>
													$("#ddservices").prop('selectedIndex', <?php echo $row->opportunity_service_id;?>);
												</script>-->
												<span id="divddservices" style="color:red"></span>
											</div>
											
										</div>
										
									</div>
									<!--/row-->
									
									
									<div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<label class="control-label">Expected Revenue</label>
												<input type="text" placeholder="0.00" class="form-control input-sm numeric" name="txtrevenue" id="txtrevenue" value="<?php echo $row->expected_revenue;?>">
												<span id="divtxtrevenue" style="color:red"></span>	
											</div>
										</div>
										<div class="col-md-1">
											<div class="form-group">
												
												<label class="control-label" style="margin-top:30px">at</label>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												
												<label class="control-label">Probability(%)
												</label>
												<input type="text"placeholder="0%" name="txtprobability" id="txtprobability" class="form-control input-sm percent" value="<?php echo $row->probability; ?>">
												<span id="divtxtprobability" style="color:red"></span>	
											</div></div>
											<div class="col-md-3">
												<div class="form-group">
													
													<label class="control-label">Priority</label>
													<select id="ddpriority" name="ddpriority" class="form-control input-sm" >
														<option value="-1">Select Priority Type</option>
														<option <?php echo ($row->opportunity_priority==1 ? 'selected' : ''); ?> value="1">High</option>
														<option <?php echo ($row->opportunity_priority==2 ? 'selected' : ''); ?> value="2">Medium</option>
														<option <?php echo ($row->opportunity_priority==3 ? 'selected' : ''); ?> value="3">Low</option>
													</select>
													<span id="divddpriority" style="color:red"></span>	
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">Close Date</label>
													<input type="text" name="txtclose_dt" id="txtclose_dt" class="form-control input-sm date-picker" value="<?php echo $row->opportunity_close_date; ?>" data-date-format="yyyy-mm-dd">   
													<span id="divtxtclose_dt" style="color:red"></span>	
												</div>
											</div>  
									</div>
									<div class="row">
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Upload Proposal</label>
												<div class="fileinput fileinput-new input-sm empty" data-provides="fileinput">
													<div class="input-group">
														<div class="form-control uneditable-input input-sm " data-trigger="fileinput">
															<i class="fa fa-file fileinput-exists"></i>&nbsp;
															<span class="fileinput-filename"><?php echo $row->opportunity_proposal_copy?>
															</span>
														</div>
														<span class="input-group-addon btn-sm default btn-file">
															<span class="fileinput-new"> Select file </span>
															<span class="fileinput-exists"> Change </span>
															<input type="file" class="emptyfile input-sm" name="userfile" id="userfile" accept="application/pdf">
														</span>
														<a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
													</div>
												</div>
												<input type="hidden" name="filename" id="filename" />
												<span id="divuserfile" style="color:red"></span>
											</div>
											
										</div>
									</div>
									<?php 
										$rem="";
										$sub="";
										foreach($task_info->result() as $task_row)
										{
											$rem=$task_row->task_reminder;
											$sub=$task_row->task_subject;
											break;
										}
									?>
									
									<h4 class="caption-subject font-blue bold uppercase">Opportunity Information</h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Next Step</label> 
												<div class="form-group input-group date form_datetime input-medium">
													<input type="text" size="16" name="next_step" id="next_step" readonly class="form-control input-sm" value="<?php echo $rem?>">
													<span class="input-group-btn">
														<button class="btn btn-sm date-set" type="button"><i class="fa fa-calendar"></i></button>
													</span>
													
												</div>  
												<span id="divnext_step" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Next Step Description</label> 
												<textarea placeholder="Next Step Description" class="form-control input-sm " name="txtd" id="txtd" style="font-size:20px" col="3" ><?php echo $sub;?></textarea>	
												<span id="divtxtd" style="color:red"></span>
											</div>
										</div> 
									</div>
									<h4 class="caption-subject font-blue bold uppercase">Description Information</h4>
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Description</label>
												<textarea  placeholder="Enter Description Information" name="txtop_des" id="txtop_des"  class="form-control input-sm " style="font-size:20px" col="3"><?php echo $row->opportunity_description;?></textarea>
												<span id="divtxtop_des" style="color:red"></span>
											</div>
										</div>
									</div>
								</div>
								<?php 
									if($row->opportunity_stage!=5)
									{
									?>
									<div class="form-actions middle">
										<button type="button" onclick="submitFile(0)" class="btn green"><i class="fa fa-check"></i> Save</button>
										<button type="button" onclick="submitFile(1)" class="btn green" >Save & Exit</button>
										<button type="button" onclick="exit()" class="btn default">Cancel</button>
									</div>
									<?php 
									} 
								?>
							</form>
							<!-- END FORM--> 
							
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	

<link rel="stylesheet" href="<?php echo base_url() ?>application/libraries/bootstrap-multiselect.css" type="text/css"/>
<script>
	load_client();
	function load_client()
	{
		var ddop_type=$("#ddop_type").val();
		$("#ddacc_name").empty();
		if(ddop_type=='1')
		{
			$('#account_name').hide();
			$('#account_id').show();
			
			$.post("<?php echo base_url();?>crm/get_account_detail/", 
			function(data) {
				$("#ddacc_name").append("<option value=-1>Select Account Name</option>");
				$.each(data,function(i,item)
				{
					$('#ddacc_name').append("<option value="+item.account_id+">"+item.account_name+"</option>");
				});
				
			}, "json");
		}
		if(ddop_type!='1')
		{
			$('#account_id').hide();
			$('#account_name').show();
		}
	}
</script>

<script>
	function submitFile(id)
	{
		var file=$("#userfile").val();
		
		if(file!="")
		{
			var formUrl = "<?php echo base_url(); ?>crm/insert_opportunity_file";
			var formData = new FormData($('.horizontal-form')[0]);
			$.ajax({
				url: formUrl,
				type: 'POST',
				data: formData,
				mimeType: "multipart/form-data",
				contentType: false,
				cache: false,
				processData: false,
				success: function(data){
					alert(data);
					//now get here response returned by PHP in JSON fomat you can parse it using JSON.parse(data)
					$("#filename").val(data.trim());
					update_opporunity(id);
				},
				error: function(jqXHR, textStatus, errorThrown){
					//handle here error returned
				}
			});
		}
		else
		{
			update_opporunity(id);
		}
	}
</script>
<script>
	fill_userid();
	function fill_userid()
	{
		var user_id=document.getElementById("txtop_ownr").value;
		if(user_id!="0")
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/master/validateUser/<?php echo $lcid;?>",
				data:"txtintuserid="+user_id,
				success: function(msg) {
					
					if(msg!="false")
					{
						document.getElementById("txtop_ownrname").value=msg.trim(); 
					}
					else
					{
						document.getElementById("txtop_ownrname").value=msg;                   
						alert('No User in this Id');
					}
				}
			}
			)
			
		}
		else
		{
			document.getElementById("txtop_ownrname").value="SuperAdmin";                    
		}	
	}
</script>
<script>
	function exit()
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url().'crm/show_opportunity'?>");
	}
</script>
<script>
	function update_opporunity(id)
	{
		if(check("form_sample_1"))
		{
			bootbox.confirm('Are you sure to Submit form?', function(result){
				if(result==true)
				{
					var ac_nm=""; 
					if($('#ddacc_name').val())
					{
						ac_nm=$('#ddacc_name').val();
					}
					if( $('#txtacc_name').val())
					{
						ac_nm=$('#txtacc_name').val();
					}
					var datas={
						txtsubj:$('#txtsubj').val(),
						txtop_ownrname:$('#txtop_ownrname').val(),
						txtrevenue:$('#txtrevenue').val(),
						txtprobability:$('#txtprobability').val(),
						txtclose_dt:$('#txtclose_dt').val(),
						txtacc_name:ac_nm,
						ddenqtype:$('#ddenqtype').val(),
						ddwebsite:$('#ddwebsite').val(),
						ddproject:$('#ddproject').val(),
						ddservice:$('#ddservices').val(),
						ddpriority:$('#ddpriority').val(),
						ddop_type:$('#ddop_type').val(),
						ddop_stg:$('#ddop_stg').val(),
						userfile:$('#filename').val(),
						next_step:$('#next_step').val(),
						txtd:$('#txtd').val(),
						txtop_des:$('#txtop_des').val()
					};
					$.ajax(
					{
						type:"POST",
						url:"<?php echo base_url();?>crm/update_opportunity/<?php echo $row->opportunity_id?>/<?php echo $lcid;?>",
						data:datas,
						success: function(msg) {
							//$(".page-content").html(msg);
							if(msg!=0)
							{	
								
								$(".page-content").html("<center><h2>"+msg+"</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(1000,function()
								{
									if(id==1)
									{
										location.reload();
										//$("#stylized").load("<?php echo base_url().'crm/show_opportunity'?>");
									}
									else
									{
										location.reload();
										//$("#stylized").load("<?php echo base_url().'crm/edit_opportunity/'.$row->opportunity_id?>");
									}					
								}
								);
								
							}
						}
						
					});
				}
			}); 
		}
		else
		{
			return false;
		}
	}
</script>
<script>
	get_project();
	function get_project()
	{
		var ddenqtype=$("#ddenqtype").val();
		var web_val="<?php echo $webval;?>";
		var project_val="<?php echo $projval;?>";
		
		$("#ddwebsite").empty();
		$("#ddproject").empty();
		$("#ddservices").empty();
		if(ddenqtype=='1' || ddenqtype=='1,2' || ddenqtype=='1,3' || ddenqtype=='1,2,3')
		{
			$.post("<?php echo base_url();?>crm/project_category/1", function(data) {
				//$("#ddwebsite").empty();
				$("#ddwebsite").append("<option value=-1>Select Website Type</option>");
				$.each(data,function(i,item)
				{  
					if(web_val==item.m_project_id)
					{
						$('#ddwebsite').append("<option value="+item.m_project_id+" selected >"+item.m_project_name+"</option>");
						
					}
					else
					{
						$('#ddwebsite').append("<option value="+item.m_project_id+" >"+item.m_project_name+"</option>");
						
					}
				});
				
			}, "json");
		}
		
		if(ddenqtype=='2' || ddenqtype=='1,2' || ddenqtype=='2,3' || ddenqtype=='1,2,3')
		{
			$.post("<?php echo base_url();?>crm/project_category/2", function( data1 ) {
				//$("#ddproject").empty();
				$("#ddproject").append("<option value=-1>Select Project Type</option>");
				$.each(data1,function(j,item1)
				{
					if(project_val==item1.m_project_id)
					{
						$('#ddproject').append("<option value="+item1.m_project_id+" selected>"+item1.m_project_name+"</option>");
					}
					else
					{
						$('#ddproject').append("<option value="+item1.m_project_id+">"+item1.m_project_name+"</option>");
					}
				});
			}, "json");
		}
		
		if(ddenqtype=='3' || ddenqtype=='1,3' || ddenqtype=='2,3' || ddenqtype=='1,2,3')
		{
			$.post("<?php echo base_url();?>crm/services_category/", function( data2 ) {
				//$("#ddservices").empty();
				$("#ddservices").append("<option value=-1>Select Service Type</option>");
				$.each(data2,function(k,item2)
				{
					$('#ddservices').append("<option value="+item2.m_service_type_id+" <?php echo (in_array('"+item2.m_service_type_id+"',$val))?'selected':'';?>>"+item2.m_service_type+"</option>");
				});
			}, "json");
		}
	}
</script>					