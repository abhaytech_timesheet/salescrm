<!-- BEGIN CONTENT -->
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_account/0">
							CRM
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-bubbles"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_account/0">Account</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-edit"></i>
						<span>Edit Account</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Edit Account </h3>
			<!-- END PAGE TITLE-->
			<?php
				foreach($info->result() as $row)
				{
					break;
				}
			?>
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Edit Account </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form action="#" class="horizontal-form"  id="form_sample_1">
								
								
								<div class="form-body">
									<h4 class="caption-subject font-blue bold uppercase"> Basic Information </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Account Name</label>
												<input type="text" placeholder="Enter Account Name" class="form-control input-sm empty"  name="txtacc_name" id="txtacc_name" value="<?php echo $row->account_name;?>">
												<span id="divtxtacc_name" style="color:red"></span>	
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Account Owner</label>
												<input type="text" placeholder="Enter Account Ownername" class="form-control input-sm empty" name="txtacc_ownername" id="txtacc_ownername" disabled>
												<span id="divtxtacc_ownername" style="color:red"></span>	
												<input type="hidden" class="form-control input-sm" name="txtacc_owner" id="txtacc_owner" value="<?php echo $this->session->userdata('profile_id')?>">
											</div>
										</div>
										
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Account Type</label>
												<select class="form-control input-sm opt" name="ddacc_type" id="ddacc_type">
													<option value="">Select Account Type</option>
													<?php
														foreach($type->result() as $typerow)
														{
															if($row->account_type==$typerow->m_des_id)
															{
															?>
															<option value="<?php echo $typerow->m_des_id;?>" selected ><?php echo $typerow->m_des_name; ?></option>
															<?php
															}
															else
															{
															?>
															
															<option value="<?php echo $typerow->m_des_id;?>"><?php echo $typerow->m_des_name; ?></option>
															<?php
															}
														}
													?>
												</select>	
												<span id="divddacc_type" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Industry</label>
												<select id="ddindustry" name="ddindustry" class="form-control input-sm empty" onChange="fill_city()">
													<option value="-1"> Select Industry</option>
													<?php
														foreach($ind->result() as $indrow)
														{
															if($row->account_industry!=$indrow->industry_id)
															{
															?>
															<option value="<?php echo $indrow->industry_id; ?>" ><?php echo $indrow->industry_name; ?></option>
															<?php
															}
															else
															{?>																				
															<option value="<?php echo $indrow->industry_id; ?> " selected><?php echo $indrow->industry_name; ?></option>
															<?php
															}
														}
													?>
												</select>
												<span id="divddindustry" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Employees
												</label>
												<input type="text" placeholder="Enter Employees" name="txtemp" id="txtemp" class="form-control" value="<?php echo $row->account_emp;?>" class="form-control input-sm empty">
												<span id="divtxtemp" style="color:red"></span>
											</div>
										</div>
										
									</div>
									
									<!--row-->
									
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Website
												</label>
												<input type="text" placeholder="Enter Website"name="txtacc_website" id="txtacc_website" class="form-control input-sm empty"  value="<?php echo $row->account_website?>">	
												<span id="divtxtacc_website" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Contact Number
												</label>
												<input type="text" placeholder="Enter Mobile Number" name="txtcontact" id="txtcontact" value="<?php echo $row->account_phone?>" class="form-control input-sm" maxlength=10>
												<span id="divtxtcontact" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">
													Email Address
													
												</label>
												<input type="text"placeholder="Enter Email Address" name="txtemail" id="txtemail" value="<?php echo $row->account_email?>" class="form-control input-sm empty">
												<span id="divtxtemail" style="color:red"></span>
											</div>
											
										</div>
										<!--/span-->
										
									</div>
									
									
									<!--row-->
									
									
									<div class="row">
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Fax</label>
												<input type="text"placeholder="Enter Fax Number" class="form-control input-sm empty" value="<?php echo $row->account_fax?>" name="txtfax" id="txtfax">
												<span id="divtxtfax" style="color:red"></span>
											</div>
										</div>
										
										<!--/span-->
									</div>
									<h4 class="caption-subject font-blue bold uppercase"> Billing Address Information </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Country
													<span class="required">
														*
													</span>
												</label>
												<input type="hidden" id="txtstate" name="txtstate" class="form-control input-inline input-medium"  value="<?php echo $row->account_state; ?>"/>
												<input type="hidden" id="txtcity" name="txtcity" class="form-control input-inline input-medium"  value="<?php echo $row->account_city; ?>"/>
												
												<select class="form-control input-sm opt" name="ddcountry" id="ddcountry" onchange="get_state()">
													<option value="-2">Select Country</option>
													<?php
														foreach($loc->result() as $locrow)
														{
															if($row->account_country==$locrow->m_loc_id)
															{
															?>
															<option value="<?php echo $locrow->m_loc_id?>" selected><?php echo $locrow->m_loc_name; ?></option>
															<?php
															}
															else
															{
															?>
															<option value="<?php echo $locrow->m_loc_id?>"><?php echo $locrow->m_loc_name; ?></option>
															<?php
															}			
														}
													?>
												</select>       
												<span id="divddcountry" style="color:red"></span>
											</div>
											</div> <div class="col-md-4">
											<div class="form-group">
												<label class="control-label">State
												</label>
												<div id="state">
													<select class="form-control input-sm opt" name="ddstate" id="ddstate" onchange="get_city()">
														<option value="">Select State</option>
														
													</select>
													<span id="divddstate" style="color:red"></span>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">City    </label>
												<div id="city">
													<select class="form-control input-sm opt" name="ddcity" id="ddcity">
														<option value="">Select City</option>
														
													</select>
													<span id="divddcity" style="color:red"></span>
												</div>
											</div>
										</div>
									</div>
									<!--/span-->
									
									<div class="row">
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Street
													<span class="required">
														*
													</span>
												</label>
												<input type="text" class="form-control input-sm empty" PLACEHOLDER="Enter Street/Locality" name="txtstreet" id="txtstreet" value="<?php echo $row->account_address?>">				
												<span id="divtxtstreet" style="color:red"></span>
											</div>
										</div>
										
										<!--row-->
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Zip Code
													<span class="required">
														*
													</span>
												</label>
												<input type="text" class="form-control input-sm empty" PLACEHOLDER="Enter Zipcode" name="txtzipcode" id="txtzipcode" value="<?php echo $row->account_zipcode?>">	
												<span id="divtxtzipcode" style="color:red"></span>
											</div>
										</div></div>
										<!--/span-->
										
										<!--row-->
										<h4 class="caption-subject font-blue bold uppercase">Description Information </h4>
										<div class="row">
											<div class="col-md-8">
												<div class="form-group">
													<label class="control-label">Description
													</label>
													<textarea class="form-control input-sm empty" PLACEHOLDER="Enter Account Description Information" name="txtacc_des" id="txtacc_des" style="font-size:20px" col="3"><?php echo $row->account_desc;?></textarea>		
													<span id="divtxtacc_des" style="color:red"></span>
												</div>
											</div>
										</div>
										
								</div>
								<div id="function" class="form-actions middle">
									<button type="button" onclick="update_account(0)" class="btn green"><i class="fa fa-check"></i> Save</button>
									<button type="button" onclick="update_account(1)" class="btn green" >Save & Exit</button>
									<button type="reset" onclick="exit()" class="btn default">Cancel</button>
									
								</div>
							</form>
							<!-- END FORM--> 
							
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->		

<script>
	load();
	function load()
	{
		var state_id=$("#txtstate").val();
		var country_id=$("#ddcountry").val();
		$("#ddstate").empty();
		if(country_id!='-1' && country_id!='')
		{
			
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/crm/select_state/"+country_id+"/0",
				dataType: "JSON",
				data:{'country_id':country_id},
				success:function(data){
					
					$("#ddstate").append("<option value=-1> ~~ Select ~~</option>");
					$.each(data,function(i,item)
					{
						if(item.m_loc_id==state_id)
						{
							$("#ddstate").append("<option value="+item.m_loc_id+" selected >"+item.m_loc_name+"</option>");
						}
						else
						{
							$("#ddstate").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
						}
					});
				}
			});
		}
		else
		{
			$("#ddstate").append("<option value=-1> ~~ Select ~~</option>");
			alert("Please First Select The Country");
		}
		//$("#state").html('<div><img src ="<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
		//$("#state").load("<?php echo base_url();?>index.php/crm/select_state/"+country_id+"/"+state_id);
		var city_id=$("#txtcity").val();
		$("#ddcity").empty();
		if(state_id!='-1' && state_id!='')
		{
			$.blockUI({ message: '<h3>Please Wait...</h3>' });
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/crm/select_city/"+state_id+"/0",
				dataType: "JSON",
				data:{'state_id':state_id},
				success:function(data){
					setTimeout($.unblockUI, 01);
					$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
					$.each(data,function(i,item)
					{
						if(city_id==item.m_loc_id)
						{
							$("#ddcity").append("<option value="+item.m_loc_id+" selected >"+item.m_loc_name+"</option>");
						}
						else
						{
							$("#ddcity").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
						}
					});
				}
			});
		}
		else
		{
			$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
			alert("Please First Select The State");
		}
		//$("#city").html('<div><img src ="<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
		//$("#city").load("<?php echo base_url();?>index.php/crm/select_city/"+state_id+"/"+city_id);
	}
	function get_state()
	{
		var country_id=$("#ddcountry").val();
		$("#ddstate").empty();
		if(country_id!='-1' && country_id!='')
		{
			$.blockUI({ message: '<h3>Please Wait...</h3>' });
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/crm/select_state/"+country_id+"/0",
				dataType: "JSON",
				data:{'country_id':country_id},
				success:function(data){
					setTimeout($.unblockUI, 01);
					$("#ddstate").append("<option value=-1> ~~ Select ~~</option>");
					$.each(data,function(i,item)
					{
						$("#ddstate").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
					});
				}
			});
		}
		else
		{
			$("#ddstate").append("<option value=-1> ~~ Select ~~</option>");
			alert("Please First Select The Country");
		}
		//$("#state").html('<div><img src ="<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
		//$("#state").load("<?php echo base_url();?>index.php/crm/select_state/"+country_id);
	}
</script>
<script>
	function get_city()
	{
		var state_id=$("#ddstate").val();
		$("#ddcity").empty();
		if(state_id!='-1' && state_id!='')
		{
			$.blockUI({ message: '<h3>Please Wait...</h3>' });
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/crm/select_city/"+state_id+"/0",
				dataType: "JSON",
				data:{'state_id':state_id},
				success:function(data){
					setTimeout($.unblockUI, 01);
					$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
					$.each(data,function(i,item)
					{
						$("#ddcity").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
					});
				}
			});
		}
		else
		{
			$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
			alert("Please First Select The State");
		}
		//$("#city").html('<div><img src ="<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
		//$("#city").load("<?php echo base_url();?>index.php/crm/select_city/"+state_id);
	}
</script>

<script>
	fill_userid();
	function fill_userid()
	{
		var user_id=<?php echo $this->session->userdata('user_id')?>;
		if(user_id!="9999999999")
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/master/validateUser/0",
				data:"txtintuserid="+user_id,
				success: function(msg) {
					if(msg!="false")
					{
					}
					else
					{
						document.getElementById("txtacc_ownername").value=msg;                   
						alert('No User in this Id');
					}
				}
			}
			)
			
		}
		else
		{
			document.getElementById("txtacc_ownername").value="SuperAdmin";                    
		}	
	}
</script>
<script>
	function exit()
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url().'index.php/crm/show_account'?>");
	}
</script>
<script>
	function update_account(id)
	{
		if(check("form_sample_1"))
		{
			bootbox.confirm('Are you sure to Submit form?', function(result){
				if(result==true)
				{
					var accountname = $('#txtacc_name').val();
					var accounttype= $('#ddacc_type').val();
					var ownership = $('#ddacc_own').val();
					var accountindustry = $('#ddindustry').val();
					var employees = $('#txtemp').val();
					var accountwebsite = $('#txtacc_website').val();
					var contactnumber = $('#txtcontact').val();
					var emailaddress = $('#txtemail').val();
					var fax = $('#txtfax').val();
					var country = $('#ddcountry').val();
					var state = $('#ddstate').val();
					var city = $('#ddcity').val();
					var street = $('#txtstreet').val();
					var zipcode = $('#txtzipcode').val();
					var accdesc= $('#txtacc_des').val();
					$.ajax(
					{
						type: "POST",
						url:"<?php echo base_url();?>index.php/crm/update_account/<?php echo $account_id;?>",
						data:"txtacc_name="+accountname+"&ddacc_type="+accounttype+"&ddacc_own="+ownership+"&ddindustry="+accountindustry+"&txtemp="+employees+"&txtacc_website="+accountwebsite+"&txtcontact="+contactnumber+"&txtemail="+emailaddress+"&txtfax="+fax+"&ddcountry="+country+"&ddstate="+state+"&ddcity="+city+"&txtstreet="+street+"&txtzipcode="+zipcode+"&txtacc_des="+accdesc,
						success: function(msg) {
							if(msg.trim()=="true")
							{
								$(".page-content").html("<center><h2>Edit Account Successfully!</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(1000,function()
								{
									if(id==1)
									{
										location.reload();     //$("#stylized").load("<?php echo base_url().'index.php/crm/show_account'?>");   //changed to location.reload();
									}
									else
									{
										$("#stylized").load("<?php echo base_url().'index.php/crm/edit_account/'.$account_id?>");
									}					
								}
								);
								
							}
							else
							{
								alert("Some Error on this page");
								
							}
							
						}
						
					});
					
				}
			}); 
		}
		else
		{
			return false;
		}
	}
</script>
