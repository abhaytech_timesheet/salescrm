<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-folder"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/create_sms_campaign">
							Campaign
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-rocket"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_send_email_template">Send Campaign</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-action-redo"></i>
						<span> Send SMS Campaign</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">Send Campaign</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-action-redo font-black"></i>
								<span class="caption-subject font-black bold uppercase">Send SMS Campaign</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal" id="comp">
								<div class="form-body">
                                	
									<div class="form-group">
										<label class="col-md-3 control-label">Source</label>
										<div class="col-md-6">
											<select id="ddsource" name="ddsource" class="form-control input-sm opt" onchange="enable()">
												<option value="-1" selected="selected">Select</option>
												<option value="1">Lead</option>
												<option value="2">Contact</option>
											</select>
											<span id="divddsource" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Category </label>
										<div class="col-md-6">
											<select id="ddcategory" name="ddcategory" class="form-control input-sm opt" disabled="disabled">
												<option value="-1" selected="selected">Select</option>
												<option value="1">Account</option>
											</select>
											<span id="divddcategory" style="color:red;"></span>
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-md-3 control-label">Contacts </label>
										<div class="col-md-6">
                                        	<div id="contact">
												<textarea class="form-control input-sm empty" name="txtcontact" id="txtcontact" rows="5"></textarea>
											</div>
											<span id="divtxtcontact" style="color:red;"></span>
										</div>
									</div>
                                    
                                    
                                    <div class="form-group">
										<label class="col-md-3 control-label">SMS templates </label>
										<div class="col-md-6">
                                        	<select id="ddtemplates" name="ddtemplates" class="form-control input-sm opt" onchange="get_msg()">
												<option value="-1" selected="selected">Select</option>
												<?php
													foreach($content->result() as $row)
													{
													?>
													<option value="<?php echo $row->m_email_temp_id; ?>"><?php echo $row->m_email_temp_title ?></option>
													<?php
													}
												?>
											</select>
											<span id="divddtemplates" style="color:red;"></span>
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-md-3 control-label">Template </label>
										<div class="col-md-6">
											<div id="get_template">
												<textarea class="form-control input-sm empty" id="txttemplate" name="txttemplate" rows="5"></textarea>
												<span id="divtxttemplate" style="color:red;"></span>
											</div>
										</div>
									</div>
                                    
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
                                    	<button type="button" class="btn green" onclick="get_details()">Get Details</button>
										<button type="button" class="btn green" onclick="send()">Send</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-action-redo font-dark"></i>
								<span class="caption-subject bold uppercase"></span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th width="33">
											#
										</th>
										<th width="126">
											SMS Title
										</th>
										<th width="126">
											SMS Description
										</th>
										<th width="49">
											Action
										</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$serial = 1;
										foreach($content->result() as $row)
										{
										?>
										<input type="text" name="txtdep" id="txtdep<?php echo $row->m_email_temp_id; ?>" value="<?php echo $row->m_email_temp_description ?>" style="display:none;" />
										<tr class="odd gradeX">
											
											<td><?php echo $serial; ?></td>
											<td><?php echo $row->m_email_temp_title ?></td>
											<td><?php echo substr($row->m_email_temp_description,0,20) ?></td>
											<td>
												<a href="javascript:void(0)" title="Edit Your Event Details" onclick="send_action('<?php echo $row->m_email_temp_id; ?>')" class="btn btn-xs purple"><i class="fa fa-check"></i>Get Message</a>
											</td>
										</tr>
										<?php
											$serial++;
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->														

<script>
	function enable()
	{
		
		var sour=$('#ddsource').val();
		var cate=$('#ddcategory').val();
		if(sour==2)
		{
			$('#ddcategory').removeAttr("disabled");	
		}
		else
		{
			$('#ddcategory').attr( "disabled", "disabled" );
		}
		
	}
</script>

<script>
	function get_details()
	{
		var sour=$('#ddsource').val();
		var cate=$('#ddcategory').val();
		$("#contact").html('<div><img src ="<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
		$("#contact").load("<?php echo base_url() ?>index.php/crm/get_mobile_no/"+sour+"/"+cate);
		
	}
</script>

<script>
	function send()
	{
		var contact = $('#txtcontact').val();
		var tmplate = $('#txttemplate').val();
		$.ajax(
		{
			type: "POST",
			url:"<?php echo base_url();?>index.php/crm/make_compaign/",
			data:"txtcontact="+contact+"&txttemplate="+tmplate,
			success: function(msg) {
				if(msg=="true")
				{
					$(".page-content").html("<center><h2>Message send Successfully!</h2></center>")
					.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
					.hide()
					.fadeIn(1000,function()
					{
						$("#stylized").load("<?php echo base_url().'index.php/crm/create_Campaign'?>");
					}
					);
					
				}	  
			}
		});
	}
</script>
<script>
	function send_action(id)
	{
		var dep=$('#txtdep'+id).val();
		$('#txttemplate').attr('value',dep);
	}
	
	function get_msg()
	{
		var ddsmstemplate=$('#ddtemplates').val();
		alert(ddsmstemplate);
		$("#get_template").html('<div><img src ="<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
		$("#get_template").load("<?php echo base_url() ?>index.php/crm/get_msg/"+ddsmstemplate);
	}
</script>
