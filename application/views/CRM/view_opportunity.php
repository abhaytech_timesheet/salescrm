<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_opportunity/0">
							CRM
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="icon-briefcase"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_opportunity/0">Opportunity</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-plus"></i>
						<span>
							Create Opportunity
						</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Create Opportunity </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Create Opportunity </span>
							</div>
							
							<!-- <div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div> -->
						</div>
						<div class="portlet-body form">
							<form action="javascript:void(0);" class="horizontal-form" method="post" enctype="multipart/form-data" id="form_sample_1">
								<div class="form-body">
									<h4 class="caption-subject font-blue bold uppercase">Opportunity Information</h4>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Subject</label>
												<textarea placeholder="Enter Opportunity subject" class="form-control input-sm empty"  style="font-size:20px" col="3" id="txtsubj" name="txtsubj" ></textarea>
												<span id="divtxtsubj" style="color:red"></span>	
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Opportunity Owner</label>
												<input type="text" placeholder="Enter Owner Name" class="form-control input-sm empty" name="txtop_ownrname" id="txtop_ownrname" value="<?php echo $this->session->userdata('name')?>" disabled>
												<input type="hidden"  class="form-control" name="txtop_ownr" id="txtop_ownr" value="<?php echo $this->session->userdata('profile_id'); ?>"  >
												<span id="divtxtop_ownrname" style="color:red"></span>	
											</div>
										</div> 
										
									</div>
									
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Type</label>
												<select class="form-control input-sm opt" name="ddop_type" id="ddop_type" onchange="load_client()">
													<option value="-1">Select Type</option>
													<option value="1">Existing Buisness</option>
													<option value="2">New Buisness</option>
												</select>		
												<span id="divddop_type" style="color:red"></span>
											</div>
										</div> 
										
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Account Name</label>
												<span id="account_name"><input type="text" class="form-control input-sm" name="txtacc_name" id="txtacc_name" /></span>
												<span id="account_id" style="display:none" ><select class="form-control input-sm" name="ddacc_name" id="ddacc_name" >
													<option value="-1">Select Account</option>
												</select></span>
												<span id="divtxtacc_name" style="color:red"></span>	
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Stage</label>
												<select class="form-control input-sm opt" name="ddop_stg" id="ddop_stg">
													<option value="-1">Select Stage</option>
													<option value="1">Prospecting</option>
												</select>		
												<span id="divddop_stg" style="color:red"></span>
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Lead For Product</label>
												<select class="bs-select form-control input-sm opt" name="ddenqtype[]" id="ddenqtype" multiple="multiple" onchange="get_project()">
													<option value="1">Logistics</option>
													<option value="2">Software</option>
													<option value="3">Manpower</option>
												</select>
												
												
												<span id="divddenqtype" style="color:red"></span>																		
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Select Type</label>
												<select class="form-control input-sm" name="ddwebsite" id="ddwebsite" >
													<option value="-1">Select Type</option>
												</select>
												<span id="divddwebsite" style="color:red"></span>	
											</div>
										</div>
										
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Select Product</label>
												
												<select class="form-control input-sm " name="ddproject" id="ddproject" >
													<option value="-1">Select Product Type</option>
													
												</select>
												<span id="divddproject" style="color:red"></span>
											</div>
										</div> 
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Select Services</label>
												
												<select class=" bs-select form-control input-sm " name="ddservices[]" id="ddservices" multiple="multiple" >
													<option value="-1">Select Services Type</option>
													
												</select>
												<span id="divddservices" style="color:red"></span>
											</div>
										</div> 
										
									</div>
									<!--/row-->
									
									
									
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Expected Revenue</label>
												<input type="text" placeholder="0.00" class="form-control input-sm empty" name="txtrevenue" id="txtrevenue">
												<span id="divtxtrevenue" style="color:red"></span>	
											</div>
										</div>
										<div class="col-md-1">
											<div class="form-group">
												
												<label class="control-label" style="margin-top:30px">at</label>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												
												<label class="control-label">Probability(%)
												</label>
												<input type="text" placeholder="0%" name="txtprobability" id="txtprobability" class="form-control input-sm empty">
												<span id="divtxtprobability" style="color:red"></span>	
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label class="control-label">Priority</label>
												<select class="form-control input-sm" name="ddpriority" id="ddpriority">
													<option value="-1">Select Priority Type</option>
													<option value="0">High</option>
													<option value="1">Medium</option>
													<option value="2">Low</option>
												</select>
												<span id="divddpriority" style="color:red"></span>	
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Close Date</label>
												<input type="text" name="txtclose_dt" id="txtclose_dt" class="form-control input-sm date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d"> 
												<span id="divtxtclose_dt" style="color:red"></span>	
											</div>
										</div>  
									</div> 
									
									<div class="row">
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Upload Proposal</label>
												
												<div class="fileinput fileinput-new input-sm" data-provides="fileinput">
													<div class="input-group">
														<div class="form-control uneditable-input input-sm " data-trigger="fileinput">
															<i class="fa fa-file fileinput-exists"></i>&nbsp;
															<span class="fileinput-filename">
															</span>
														</div>
														<span class="input-group-addon btn-sm default btn-file">
															<span class="fileinput-new"> Select file </span>
															<span class="fileinput-exists"> Change </span>
															<input type="file" class="emptyfile input-sm" name="userfile" id="userfile" accept="application/pdf">
														</span>
														<a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
													</div>
												</div>
												
												<input type="hidden" name="filename" id="filename" />
												<span id="divuserfile" style="color:red"></span>
											</div>
										</div>
									</div>
									
									<h4 class="caption-subject font-blue bold uppercase">Opportunity Information</h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Next Step</label> 
												<div class="form-group input-group date form_datetime input-medium">
													<input type="text" size="16" name="next_step" id="next_step" readonly class="form-control input-sm ">
													<span class="input-group-btn">
														<button class="btn btn-sm date-set" type="button"><i class="fa fa-calendar"></i></button>
													</span>
												</div>  
												<span id="divnext_step" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Next Step Description</label> 
												<textarea placeholder="Next Step Description" class="form-control input-sm " name="txtd" id="txtd" style="font-size:20px" col="3" ></textarea>
												<span id="divtxtd" style="color:red"></span>
											</div>
										</div> 
									</div>
									<h4 class="caption-subject font-blue bold uppercase">Description Information</h4>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Description</label>
												<textarea  placeholder="Enter Description Information" name="txtop_des" id="txtop_des"  class="form-control input-sm " style="font-size:20px" col="3"></textarea>	
												<span id="divtxtop_des" style="color:red"></span>
											</div>
										</div></div></div>
										<div id="function" class="form-actions middle">
											<button type="button" onclick="submitFile(0)" class="btn green"><i class="fa fa-check"></i> Save</button>
											<button type="button" onclick="submitFile(1)" class="btn green" >Save & Next</button>
											<button type="button" onclick="exit()" class="btn default">Cancel</button>
											
										</div>
							</form>
							<!-- END FORM--> 
							
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	

<script>
	function submitFile(id)
	{
		if(check("form_sample_1"))
		{
			bootbox.confirm('Are you sure to Submit form?', function(result){
				var ac_nm=""; 
				if(result==true)
				{
					$("#function").html("<div>Your Request has been processed.<img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></div>");
					var file=$("#userfile").val();
					if(file!="")
					{
						var formUrl = "<?php echo base_url(); ?>crm/insert_opportunity_file";
						var formData = new FormData($('.horizontal-form')[0]);
						$.ajax({
							url: formUrl,
							type: 'POST',
							data: formData,
							mimeType: "multipart/form-data",
							contentType: false,
							cache: false,
							processData: false,
							success: function(data){
								//alert(data);
								//now get here response returned by PHP in JSON fomat you can parse it using JSON.parse(data)
								$("#filename").val(data.trim());
								create_opporunity(id);
							},
							error: function(jqXHR, textStatus, errorThrown){
								//handle here error returned
							}
						});
					}
					else
					{
						create_opporunity(id);
					}
					
				}
			}); 
		}
		else
		{
			return false;
		}
	}
</script>

<script>
	/*fill_userid();*/
	function fill_userid()
	{
		var user_id=document.getElementById("txtop_ownr").value;
		if(user_id!="0")
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>master/validateUser/1/",
				data:"txtintuserid="+user_id,
				success: function(msg) {
					
					if(msg.trim()!="false")
					{
						document.getElementById("txtop_ownrname").value=msg.trim(); 
						//$("#txtop_ownrname").val(msg.trim());
					}
					else
					{
						document.getElementById("txtop_ownrname").value=msg.trim();                   
						alert('No User in this Id');
					}
				}
			}
			)
			
		}
		else
		{
			document.getElementById("txtop_ownrname").value="SuperAdmin";                    
		}
		
	}
</script>
<script>
	function create_opporunity(id)
	{
		var ac_nm=""; 
		$("#function").html("<div>Your Request has been processed.<img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></div>");
		
		if($('#ddacc_name').val())
		{
			ac_nm=$('#ddacc_name').val();
		}
		if( $('#txtacc_name').val())
		{
			ac_nm=$('#txtacc_name').val();
		}
		var ddop_type=$('#ddop_type').val();
		var datas={
			txtsubj:$('#txtsubj').val(),
			txtop_ownrname:$('#txtop_ownrname').val(),
			txtrevenue:$('#txtrevenue').val(),
			txtprobability:$('#txtprobability').val(),
			txtclose_dt:$('#txtclose_dt').val(),
			txtacc_name:ac_nm,
			ddenqtype:$('#ddenqtype').val(),
			ddwebsite:$('#ddwebsite').val(),
			ddproject:$('#ddproject').val(),
			ddservice:$('#ddservices').val(),
			ddpriority:$('#ddpriority').val(),
			ddop_type:$('#ddop_type').val(),
			ddop_stg:$('#ddop_stg').val(),
			userfile:$('#filename').val(),
			next_step:$('#next_step').val(),
			txtd:$('#txtd').val(),
			txtop_des:$('#txtop_des').val()
		};
		$.ajax(
		{
			type:"POST",
			url:"<?php echo base_url();?>crm/insert_opportunity/",
			data:datas,
			success:function(msg){
				if(msg.trim()!="") ///have change (msg=="true")
				{
					$(".page-content").html("<center><h2>Create Opportunity Successfully!</h2></center>"+msg)
					.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
					.hide()
					.fadeIn(1000,function(){
						if(ddop_type==2)
						{
							//location.reload();
							//$("#stylized").load("<?php echo base_url()?>crm/edit_contact_afteropp/"+msg.trim());
						}
						else
						{
							 //location.reload();
							//$("#stylized").load("<?php echo base_url()?>crm/edit_contact_afteropp/"+msg.trim());
						}					
					});	
				}
			}
		});
	}
	
	
</script>
<script>
	function exit()
	{
		if(confirm('Are you sure to Exit from Create Opportunity?')==true)
		{
			$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
			$("#stylized").load("<?php echo base_url().'crm/show_opportunity'?>");
		}
	}
</script>

<script>
	function load_client()
	{
		var ddop_type=$("#ddop_type").val();
		$("#ddacc_name").empty();
		if(ddop_type=='1')
		{
			$('#account_name').hide();
			$('#account_id').show();
			
			$.post("<?php echo base_url();?>crm/get_account_detail/", 
			function(data) {
				$("#ddacc_name").append("<option value=-1>Select Account Name</option>");
				$.each(data,function(i,item)
				{
					$('#ddacc_name').append("<option value="+item.account_id+"-"+item.account_name+">"+item.account_name+"</option>");
				});
				
			}, "json");
		}
		if(ddop_type!='1')
		{
			$('#account_id').hide();
			$('#account_name').show();
		}
	}
</script>
<script>
	function get_project()
	{
		var ddenqtype=$("#ddenqtype").val();
		$("#ddwebsite").empty();
		$("#ddproject").empty();
		$("#ddservices").empty();
		if(ddenqtype=='1' || ddenqtype=='1,2' || ddenqtype=='1,3' || ddenqtype=='1,2,3')
		{
			$.post("<?php echo base_url();?>crm/project_category/1", function(data) {
				//$("#ddwebsite").empty();
				$("#ddwebsite").append("<option value=-1>Select Website Type</option>");
				$.each(data,function(i,item)
				{
					$('#ddwebsite').append("<option value="+item.m_project_id+">"+item.m_project_name+"</option>");
				});
				
			}, "json");
		}
		
		if(ddenqtype=='2' || ddenqtype=='1,2' || ddenqtype=='2,3' || ddenqtype=='1,2,3')
		{
			$.post("<?php echo base_url();?>crm/project_category/2", function( data1 ) {
				//$("#ddproject").empty();
				$("#ddproject").append("<option value=-1>Select Project Type</option>");
				$.each(data1,function(j,item1)
				{
					$('#ddproject').append("<option value="+item1.m_project_id+">"+item1.m_project_name+"</option>");
				});
			}, "json");
		}
		
		if(ddenqtype=='3' || ddenqtype=='1,3' || ddenqtype=='2,3' || ddenqtype=='1,2,3')
		{
			$.post("<?php echo base_url();?>crm/services_category/", function( data2 ) {
				//$("#ddservices").empty();
		    	$("#ddservices").append("<option value=-1>Select Service Type</option>");
				$.each(data2,function(k,item2)
				{
					$('#ddservices').append("<option value="+item2.m_service_type_id+">"+item2.m_service_type+"</option>");
				});
			}, "json");
		}
	}
</script>															