<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/<?php echo $this->router->fetch_method();?>/<?php echo $this->uri->segment(3);?>">
							CRM
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="icon-briefcase"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_opportunity/0">Opportunity</a>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View Opportunity</h3>
			<!-- END PAGE TITLE-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN INLINE NOTIFICATIONS PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-trophy"></i>Opportunity Stages
							</div>
							<?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_add==1) { ?>
								<div style="float:right;margin-top:-11px;">
									<a href="javascript:void(0)" onclick="create_opportunity()" class="btn default red-stripe" style="margin-right:20px;">
										<i class="fa fa-plus"></i>
										<span class="hidden-480">
											Create Opportunity
										</span>
									</a>
								</div>
							<?php } ?>
						</div>
						<div class="portlet-body">
							<div class="pricing-content-1">
								<div class="row">
									<div class="col-md-2">
                                        <div class="price-column-container border-active">
                                            <div class="price-table-head bg-blue">
                                                <h2 class="no-margin">Prospecting</h2>
												<?php
													$expected_revenue1=0.00;
													foreach($info->result() as $row)
													{
														if($row->opportunity_stage==1)
														{
															$expected_revenue1=($expected_revenue1+$row->expected_revenue);
														}
													}
												?>
											</div>
                                            <div class="arrow-down border-top-blue"></div>
                                            <div class="price-table-pricing">
                                                <h4>
												<span class="price-sign"><i class="fa fa-inr "></i></span><?php echo $expected_revenue1;?></h4>
                                                <p>Monthly</p>
											</div>
                                            <div class="price-table-content">
												<?php
													foreach($info->result() as $rowdt)
													{
														if($rowdt->opportunity_stage==1)
														{
														?>
														<div class="row mobile-padding">
															<div class="col-xs-3 text-right mobile-padding">
																<i class="icon-user"></i>
															</div>
															<div class="col-xs-9 text-left mobile-padding">
																<a href="javascript:void(0)" <?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_update==1) { ?> onclick="update_opportunity('<?php echo $rowdt->opportunity_id;?>','<?php echo $rowdt->opportunity_owned_by;?>')" <?php } ?>>
																	<?php echo $rowdt->opportunity_subject;?>
																</a>
																<br/><?php echo $rowdt->expected_revenue?>
																<br/>
																<a href="javascript:void(0)" <?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_update==1) { ?> onclick="convert_opportunityto_acc('<?php echo $rowdt->opportunity_id;?>')" <?php } ?>><i class="fa fa-angle-right"></i><?php  if($row->opportunity_type==2){echo 'Convert to Account';} else {echo 'Convert to Project';} ?></a>
																
																
															</div>
														</div>
														<?php
														}
													}
												?>
											</div>
										</div>
									</div>
									
									
									<div class="col-md-2">
                                        <div class="price-column-container border-active">
                                            <div class="price-table-head bg-blue">
                                                <h2 class="no-margin">Qualification</h2>
												<?php
													$expected_revenue2=0.00;
													foreach($info->result() as $qualirow)
													{
														if($qualirow->opportunity_stage==2)
														{
															$expected_revenue2=($expected_revenue2+$qualirow->expected_revenue);
														}
													}
												?>
											</div>
                                            <div class="arrow-down border-top-blue"></div>
                                            <div class="price-table-pricing">
                                                <h4>
												<span class="price-sign"><i class="fa fa-inr "></i></span><?php echo $expected_revenue2;?></h4>
                                                <p>Monthly</p>
											</div>
                                            <div class="price-table-content">
												<?php
													foreach($info->result() as $qualirow)
													{
														if($qualirow->opportunity_stage==2)
														{
														?>
														<div class="row mobile-padding">
															<div class="col-xs-3 text-right mobile-padding">
																<i class="icon-user"></i>
															</div>
															<div class="col-xs-9 text-left mobile-padding">
																<a href="javascript:void(0)" <?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_update==1) { ?> onclick="update_opportunity('<?php echo $qualirow->opportunity_id;?>','<?php echo $qualirow->opportunity_owned_by;?>')" <?php } ?>>
																	<?php echo $qualirow->opportunity_subject;?>
																</a>
																<br/><?php echo $qualirow->expected_revenue;?>
																<br/>
																<a href="javascript:void(0)" <?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_update==1) { ?> onclick="convert_opportunityto_acc('<?php echo $rowdt->opportunity_id;?>')" <?php } ?>><i class="fa fa-angle-right"></i><?php  if($row->opportunity_type==2){echo 'Convert to Account';} else {echo 'Convert to Project';} ?></a>
																
															</div>
														</div>
														<?php
														}
													}
												?>
											</div>
										</div>
									</div>
									
									
									<div class="col-md-2">
                                        <div class="price-column-container border-active">
                                            <div class="price-table-head bg-blue">
                                                <h2 class="no-margin">Need Analysis</h2>
												<?php
													$expected_revenue3=0.00;
													foreach($info->result() as $anarow)
													{
														if($anarow->opportunity_stage==3)
														{
															$expected_revenue3=($expected_revenue3+$anarow->expected_revenue);
														}
													}
												?>
											</div>
                                            <div class="arrow-down border-top-blue"></div>
                                            <div class="price-table-pricing">
                                                <h4>
												<span class="price-sign"><i class="fa fa-inr "></i></span><?php echo $expected_revenue3;?></h4>
                                                <p>Monthly</p>
											</div>
                                            <div class="price-table-content">
												<?php
													foreach($info->result() as $anarowdt)
													{
														if($anarowdt->opportunity_stage==3)
														{
														?>
														<div class="row mobile-padding">
															<div class="col-xs-3 text-right mobile-padding">
																<i class="icon-user"></i>
															</div>
															<div class="col-xs-9 text-left mobile-padding">
																<a href="javascript:void(0)" <?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_update==1) { ?> onclick="update_opportunity('<?php echo $anarowdt->opportunity_id;?>','<?php echo $anarowdt->opportunity_owned_by;?>')" <?php } ?>><i class="fa fa-angle-right"></i><?php echo $anarowdt->opportunity_subject;?></a>
																<br/><?php echo $anarowdt->expected_revenue;?>
																<br/>
																<a href="javascript:void(0)" <?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_update==1) { ?> onclick="convert_opportunityto_acc('<?php echo $rowdt->opportunity_id;?>')" <?php } ?>><i class="fa fa-angle-right"></i><?php  if($row->opportunity_type==2){echo 'Convert to Account';} else {echo 'Convert to Project';} ?></a>
																
															</div>
														</div>
														<?php
														}
													}
												?>
											</div>
										</div>
									</div>
									
									<div class="col-md-2">
										<div class="price-column-container border-active">
											<div class="price-table-head bg-blue">
                                                <h2 class="no-margin">Value Proposition</h2>
												<?php
													$expected_revenue4=0.00;
													foreach($info->result() as $vprow)
													{
														if($vprow->opportunity_stage==4)
														{
															$expected_revenue4=($expected_revenue4+$vprow->expected_revenue);
														}
													}
												?>
											</div>
											<div class="arrow-down border-top-blue"></div>
											<div class="price-table-pricing">
                                                <h4>
												<span class="price-sign"><i class="fa fa-inr "></i></span><?php echo $expected_revenue4;?></h4>
                                                <p>Monthly</p>
											</div>
											<div class="price-table-content">
												<?php
													
													foreach($info->result() as $vprowdt)
													{
														if($vprowdt->opportunity_stage==4)
														{
														?>
														<div class="row mobile-padding">
															<div class="col-xs-3 text-right mobile-padding">
																<i class="icon-user"></i>
															</div>
															<div class="col-xs-9 text-left mobile-padding">
																<a href="javascript:void(0)" <?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_update==1) { ?> onclick="update_opportunity('<?php echo $vprowdt->opportunity_id;?>','<?php echo $vprowdt->opportunity_owned_by;?>')" <?php } ?>><i class="fa fa-angle-right"></i><?php echo $vprowdt->opportunity_subject;?></a>
																<br/><?php echo $vprowdt->expected_revenue;?>
																<br/>
																<a href="javascript:void(0)" <?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_update==1) { ?> onclick="convert_opportunityto_acc('<?php echo $rowdt->opportunity_id;?>')" <?php } ?>><i class="fa fa-angle-right"></i><?php  if($row->opportunity_type==2){echo 'Convert to Account';} else {echo 'Convert to Project';} ?></a>
																
															</div>
														</div>
														<?php
														}
													}
												?>
											</div>
										</div>
									</div>
									
									
									<div class="col-md-2">
										<div class="price-column-container border-active">
											<div class="price-table-head bg-green">
                                                <h2 class="no-margin">Won</h2>
												<?php
													$expected_revenue5=0.00;
													foreach($info->result() as $wonrow)
													{
														if($wonrow->opportunity_stage==5)
														{
															$expected_revenue5=($expected_revenue5+$wonrow->expected_revenue);
														}
													}
												?>
											</div>
											<div class="arrow-down border-top-green"></div>
											<div class="price-table-pricing">
                                                <h4>
												<span class="price-sign"><i class="fa fa-inr "></i></span><?php echo $expected_revenue5;?></h4>
                                                <p>Monthly</p>
											</div>
											<div class="price-table-content">
												<?php
													foreach($info->result() as $wonrowdt)
													{
														if($wonrowdt->opportunity_stage==5)
														{
														?>
														<div class="row mobile-padding">
															<div class="col-xs-3 text-right mobile-padding">
																<i class="icon-user"></i>
															</div>
															<div class="col-xs-9 text-left mobile-padding">
																
																<a href="javascript:void(0)" <?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_update==1) { ?> onclick="update_opportunity(<?php echo $wonrowdt->opportunity_id;?>,<?php echo $wonrowdt->opportunity_owned_by;?>)" <?php } ?>>
																	<?php echo $wonrowdt->opportunity_subject;?>
																</a>
																<br/><?php echo $wonrowdt->expected_revenue;?>
															</div>
														</div>
														<?php
														}
													}
												?>
											</div>
										</div>
									</div>
									
									<div class="col-md-2">
										<div class="price-column-container border-active">
											<div class="price-table-head bg-red">
                                                <h2 class="no-margin">Lost</h2>
												<?php
													$expected_revenue6=0.00;
													foreach($info->result() as $lostrow)
													{
														if($lostrow->opportunity_stage==6)
														{
															$expected_revenue6=($expected_revenue6+$lostrow->expected_revenue);
														}
													}
												?>
											</div>
											<div class="arrow-down border-top-red"></div>
											<div class="price-table-pricing">
                                                <h4>
												<span class="price-sign"><i class="fa fa-inr "></i></span><?php echo $expected_revenue6;?></h4>
                                                <p>Monthly</p>
											</div>
											<div class="price-table-content">
												<?php
													foreach($info->result() as $lostrowdt)
													{
														if($lostrowdt->opportunity_stage==6)
														{
														?>
														<div class="row mobile-padding">
															<div class="col-xs-3 text-right mobile-padding">
																<i class="icon-user"></i>
															</div>
															<div class="col-xs-9 text-left mobile-padding">
																<a href="javascript:void(0)" <?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_update==1) { ?> onclick="update_opportunity('<?php echo $lostrowdt->opportunity_id;?>','<?php echo $lostrowdt->opportunity_owned_by;?>')" <?php } ?>><i class="fa fa-angle-right"></i><?php echo $lostrowdt->opportunity_subject;?></a>
																<br/><?php echo $lostrowdt->expected_revenue;?>
															</div>
														</div>
														<?php
														}
													}
												?>
											</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
					<!-- END INLINE NOTIFICATIONS PORTLET-->
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->
<script>
	function create_opportunity()
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url()?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url()?>crm/add_opportunity");
	}
</script>
<script>
	function update_opportunity(id,ownid)
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url()?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url()?>crm/edit_opportunity/"+id+"/"+ownid);
		
	}
</script>									

<script>
	function convert_opportunityto_acc(id)
	{
		if(confirm('Are you sure to Convert Status?')==true)
		{
			$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url()?>application/libraries/assets/img/loading-spinner-blue.gif' /></center>");
			window.location.href="<?php echo base_url();?>crm/conert_opp_to_acc/"+id;
		}
	}
</script>												