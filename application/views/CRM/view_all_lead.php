<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/<?php echo $this->router->fetch_method();?>/<?php echo $this->uri->segment(3);?>">
							CRM
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="icon-bag"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_lead/<?php echo $this->uri->segment(3);?>">
							View All Leads
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View All Lead</h3>
			<!-- END PAGE TITLE-->
			
			
			<div class="row">
				<div class="col-md-12">
					
					<div class="table-responsive" id="data1">
						<table class="table table-striped table-bordered table-hover table-full-width" id="datatable_products">
							<thead>
								<tr role="row" class="heading">
									<th width="10%">
										Contact
									</th>
									<th width="15%">
										Name
									</th>
									<!-- <th width="15%">
										Email
									</th> -->
									<th width="10%">
										Company
									</th>
									<th width="10%">
										Owner&nbsp;Alias
									</th>
									<th width="15%">
										Date&nbsp;From
									</th>
									<th width="15%">
										Date&nbsp;To
									</th>
									<th width="10%">
										Status
									</th>
									<th width="10%">
										Actions
									</th>
								</tr>
								<form method="post" action="<?php echo base_url()?><?php echo $this->router->fetch_class();?>/lead_search_report/<?php echo $id;?>" onsubmit="" >
									
									<tr role="row" class="filter">
										<td>
											<input type="text" class="form-control form-filter input-sm" name="lead_contact">
										</td>
										<td>
											<input type="text" class="form-control form-filter input-sm" name="lead_name">
										</td>
										<!-- <td>
											<input type="text" class="form-control form-filter input-sm" name="lead_email">
										</td> -->
										<td>
											<input type="text" class="form-control form-filter input-sm" name="lead_company">
										</td>
										<td>
											<select name="lead_owneralias" id="lead_owneralias" class="form-control form-filter ">
												<option value="-1">Select...</option>
												<?php if($this->session->userdata('profile_id')==0)
													{?>
													<option value="0">Super Admin</option>
													<?php 
														foreach($user->result() as $row2)
														{
														?>
														<option value="<?php echo $row2->or_m_reg_id; ?>"><?php echo $row2->or_m_name; ?></option>
														<?php
														}
													}
													else
													{?>
													<option value="<?php echo $this->session->userdata('profile_id'); ?>"><?php echo $this->session->userdata('name'); ?></option>												
													<?php
													}
												?>
											</select>
											<!--<input type="text" class="form-control form-filter input-sm" name="lead_owneralias">-->
										</td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="lead_created_from" id="lead_created_from" placeholder="From">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>											
										</td>
										<td>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="leadcreated_to" id="leadcreated_to" placeholder="To">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										<td>
											<select name="lead_status" id="lead_status" class="form-control form-filter input-sm">
												<option value="">Select...</option>
												<option value="0">Deactive</option>
												<option value="1">Active</option>
												<option value="2">Pending</option>
											</select>
										</td>
										<td>
											<div class="margin-bottom-5">
												<button class="btn btn-sm g filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
											</div>
											
										</td>
									</tr>
								</form>
							</thead>
						</table>
						
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-ticket font-dark" ></i>
								<span class="caption-subject bold uppercase">View & Modify View all Leads</span>
							</div>
                            <?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_add==1) { ?>
								<div class="actions">
									<div class="btn-group btn-group-devided" data-toggle="buttons">
										<a href="javascript:void(0)" onclick="create_lead()" class="btn green blue-stripe" >
											<i class="fa fa-plus"></i>
											<span class="hidden-480">
												Create Lead
											</span>
										</a>
										<a href="javascript:void(0)" onclick="create_csv()" class="btn green blue-stripe" >
											<i class="fa fa-plus"></i><span class="hidden-480">Upload CSV Data</span>
										</a>
										<a href="#" onclick="modified_leads()" class="btn green blue-stripe" >
											View Modified Leads
										</a>
										
									</div>
								</div>
							<?php } ?>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<h5> &nbsp;</h5>
										</div>
									</div>
									<div class="col-md-6">
										
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								
								<thead>
									<tr>
									<th>S No.</th>
                                    <?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_update==1) { ?>
										<th class="ignore">Action</th>
									<?php } ?>
									<th>Created On</th>
									<th>For</th>
									<th>Company</th>
									<th>Name</th>
									<th>Contact</th>
									<th>Email</th>
									<th>Owner Alias</th>
									<th>Address</th>										
									<th>Status</th>							
									
								</tr>
							</thead>
							<tbody>
								<?php
									$sn=1;
									
									foreach($rec->result() as $row)
									{
									?>
									<tr>
										<td><?php echo $sn;?></td>
										<?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_update==1) { ?>
											<td class="ignore" id="act<?php echo $row->Lead_id;?>">
												<!--<div id="act<?php echo $row->Lead_id;?>">-->
													<?php
														if($row->Lead_Status1!='7' || $this->session->userdata('profile_id')==0)
														{
															if($row->Lead_Status1!='7')
															{
															?>
															<div class="btn-group">
																<button class="btn red btn-sm dropdown-toggle" data-toggle="dropdown">Action
																	<i class="fa fa-angle-down"></i>
																</button>
																<ul class="dropdown-menu" >
																	<?php if($row->Lead_Status1=='4'){ ?>
																		<li>
																		<a href="<?=base_url()?>index.php/crm/convert_lead_to_event/<?=$row->Lead_id;?>" title="Edit" onClick="update_lead('<?php echo $row->Lead_id;?>','<?php echo $row->Lead_Created_By;?>')">
																			<i class="fa fa-calendar"></i>Add Event</a>
																		</li>
																	<?php } ?>
																	<li>
																		<a href="javascript:void(0)" title="Edit" onClick="update_lead('<?php echo $row->Lead_id;?>','<?php echo $row->Lead_Created_By;?>')">
																		<i class="fa fa-pencil"></i>Edit</a>
																	</li>
																	<?php 
																		if($row->Lead_Status1!='6')
																		{?>
																		<li>
																			
																			<a href="javascript:void(0)" title="Convert Opportunity" onClick="convert_opporunity('<?php echo $row->Lead_id;?>','<?php echo $row->Lead_Created_By;?>')" >
																			<i class="fa fa-exchange"></i>Convert Opportunity</a>
																		</li>
																		<li>
																			<a href="javascript:void(0)" title="Create Task on Lead" onClick="create_task(<?php echo $row->Lead_id;?>)" >
																			<i class="fa fa-tasks"></i>Create Task </a>
																			
																		</li>
																		
																		<?php 
																		}
																	?>
																	<li>
																		<a href="javascript:void(0)" title="Delete Lead" onClick="delete_lead(<?php echo $row->Lead_id;?>)" >
																		<i class="fa fa-trash-o"></i>Delete Lead</a>
																		
																	</li>
																	<li>
																		<a href="javascript:void(0)" title="Reassign Lead" onClick="assign_lead('<?php echo $row->Lead_id;?>','<?php echo $row->lead_assign_id;?>')" >
																			<i class="fa fa-mail-reply-all"></i>Reassign Lead
																		</a>
																		
																	</li>
																	<li>
																		<a href="<?php echo base_url();?>crm/view_lead_history/<?php echo $row->Lead_id;?>" title="View Follow Up" >
																		<i class="fa fa-reply"></i>Add Follow Up &nbsp;</a> 
																	</li>
																</ul>
															<!--</div>-->
															<?php
															}
														}
													?>
												</div>
											</td>  
										<?php } ?>                                                    
										<td><?php echo $row->Lead_Created_On; ?></td>  
										<td><?php echo $row->Lead_for; ?></td> 
										<td><?php echo $row->Lead_Company; ?></td>
										<td><?php echo $row->Lead_Name; ?></td>                                                                
										<td><?php echo $row->Lead_Contact; ?></td>
										<td><?php echo $row->Lead_Email; ?></td>
										<td><?php echo $row->Lead_Owner ?></td>
										<td><?php echo $row->Lead_Address; ?></td>
										<td><?php echo $row->Lead_Status; ?></td>
									</tr>
									
									<?php
										$sn++;
									}
									
								?>
							</tbody>
						</table>
					</div>
				</div>
				
			</div>
			<!-- END PAGE CONTENT-->
		</div>
		
	</div>
	<!-- END CONTENT BODY -->
</div>
</div>
<!-- END CONTENT -->



<!-- END CONTAINER -->
<script>
	function create_task(id)
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url().'crm/assign_task/'?>"+id+"/1");			
	}
	
	function delete_lead(id)
	{
		if(confirm('Are you sure to disable the Lead?')==true)
		{
			$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
			$("#stylized").load("<?php echo base_url().'crm/delete_lead/'?>"+id);			
		}
	}
	function create_lead()
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url().'crm/add_lead/'.$id?>");			
	}
	function update_lead(id,ownid)
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url().'crm/edit_lead/'?>"+id+"/"+ownid);			
	}
	function convert_opporunity(id,ownid)
	{
		if(confirm('Are you sure to convert into Opportunity?')==true)
		{
			$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
			$("#stylized").load("<?php echo base_url().'crm/convert_opportunity/'?>"+id+"/"+ownid);			
		}
	}
	
	function create_csv()
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url().'crm/view_csv_upload/'?>");
	}
    
    
    function assign_lead(id,assign_id)
	{
		$("#act"+id).html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#act"+id).load("<?php echo base_url().$this->router->fetch_class().'/assign_lead/'?>"+id+"/"+assign_id);			
	}
	
</script>				