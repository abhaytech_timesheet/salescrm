<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_event/0">
							CRM
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-calendar"></i>
						<span>Event</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View Events</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					
					<div class="table-responsive" id="data1">
						<table class="table table-striped table-bordered table-hover table-full-width" id="datatable_products">
							<thead>
								<tr role="row" class="heading">
									<th width="10%">
										Account&nbsp;ID
									</th>
									<th width="15%">
										Account&nbsp;Name
									</th>
									<th width="15%">
										Type
									</th>
									<th width="10%">
										Owner&nbsp;Alias
									</th>
									<th width="15%">
										Date&nbsp;From
									</th>
									<th width="15%">
										Date&nbsp;To
									</th>
									<th width="10%">
										Status
									</th>
									<th width="10%">
										Actions
									</th>
								</tr>
								
								<tr role="row" class="filter">
									
									<td>
										<input type="text" class="form-control form-filter input-sm" name="product_id">
									</td>
									<td>
										<input type="text" class="form-control form-filter input-sm" name="product_name">
									</td>
									<td>
										<select name="product_category" class="form-control form-filter input-sm">
											<option value="-1">Select...</option>
											<option value="1">Customer</option>
										</select>
									</td>
									<td>
										<input type="text" class="form-control form-filter input-sm" name="product_name">
									</td>
									<td>
										<div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
											<input type="text" class="form-control form-filter input-sm" readonly name="product_created_from" placeholder="From">
											<span class="input-group-btn">
												<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</td>
									<td>										
										<div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
											<input type="text" class="form-control form-filter input-sm" readonly name="product_created_to " placeholder="To">
											<span class="input-group-btn">
												<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</td>
									<td>
										<select name="product_status" class="form-control form-filter input-sm">
											<option value="">Select...</option>
											<option value="0">Deactive</option>
											<option value="1">Active</option>
											<option value="2">Pending</option>
										</select>
									</td>
									<td>
										<div class="margin-bottom-5">
											<button class="btn btn-sm green filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
										</div>
									</td>
								</tr>
							</thead>
						</table>
						
					</div>
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-tasks font-dark" ></i>
								<span class="caption-subject bold uppercase">View & Modify View all Event</span>
							</div>
                            <?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_add==1) { ?>
							<div class="actions">
								<div class="btn-group btn-group-devided" data-toggle="buttons">
									<a href="javascript:void(0)" onclick="create_event()" class="btn green blue-stripe" style="margin-right:20px;">
										<i class="fa fa-plus"></i>
										<span class="hidden-480">
											Create Event
										</span>
									</a>
									
								</div>
							</div>
                            <?php } ?>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<h5> &nbsp;</h5>
										</div>
									</div>
									<div class="col-md-6">
										
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S.No</th>
                                        <?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_update==1) { ?>
										<th class="ignore">Action</th>
                                        <?php } ?>
										<th>Create On</th>
										<th>Start Date</th>
										<th>End Date</th>
										<th>Event Type</th>
										<th>Subject</th>
										<th>Rel To</th>
										<th>Rel Name</th>
										<th>Location</th>
										<th>Assign</th>
										<th>Status</th>
										
										
									</tr>
								</thead>
								<tbody>
									<?php
										$sn=1;
										foreach($rec->result() as $row)
										{
										?>
										<tr>
											<td><?php echo $sn;?></td>
                                            <?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_update==1) { ?>
											<td class="ignore">
											    <div class="btn-group">
													<button class="btn red btn-sm dropdown-toggle" data-toggle="dropdown">Action
														<i class="fa fa-angle-down"></i>
													</button>
													<ul class="dropdown-menu">
														<li>
															<a href="javascript:void(0)" onClick="update_event(<?php echo $row->event_id;?>)">
																<i class="fa fa-pencil"></i> Update Event
															</a>
														</li>
														<?php
															if($row->event_type=='1' && $row->event_status!='0')
															{
															?>
															<li>
																<a href="javascript:void(0)" title="Edit" onClick="goto_type(1)">
																<i class="glyphicon glyphicon-phone-alt"></i> Edit Phone</a>
															</li>
															<?php 
															}
															if($row->event_type=='2' && $row->event_status!='0')
															{
															?>
															<li>
																<a href="javascript:void(0)" title="Edit" onClick="goto_type(2)">
																<i class="glyphicon glyphicon-envelope"></i>Edit Message</a>
															</li>
															
														<?php } ?>
														
														
													</ul>
												</div>
												
												
												&nbsp;&nbsp;&nbsp;
												
												
											</td>
                                            <?php } ?>
											<td><?php echo substr($row->event_create_date,0,10); ?></td>
											<td><?php echo substr($row->event_start_date,0,10); ?></td>
											<td><?php echo substr($row->event_end_date,0,10); ?></td>
											<td><?php echo $row->event_type; ?></td>
											<td><?php echo $row->subject; ?></td>
											<td><?php echo $row->event_related_to; ?></td>
											<td><?php echo $row->name; ?></td>
											<td>
												<?php echo $row->event_location; ?>
											</td>
											<td><?php echo $row->event_assignto_id;	?></td>
											<td>
												<?php
													$date=date_create($row->event_end_date);
													if($row->event_status==1 && date_format($date,'d-m-Y') <= $curr)
													{
														echo "Open";
													}
													if($row->event_status==0)
													{
														echo "Closed";
													}
													if($row->event_status==1 && date_format($date,'d-m-Y') >= $curr)
													{
														echo "Event Time Expired";
													}
												?>
											</td>
										</tr>
										
										<?php
											$sn++;
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->


<script>
	function create_event()
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url().'index.php/crm/create_event/'?>");			
	}
	function update_event(id)
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url().'index.php/crm/view_edit_event/'?>"+id);			
	}
</script>

<script>
	function goto_type(id)
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		if(id=='1')
		{
			$("#stylized").load("<?php echo base_url().'index.php/crm/view_makecall'?>");
		}
		if(id=='2')
		{
			$("#stylized").load("<?php echo base_url().'index.php/crm/make_email'?>");
		}
	}
</script>
