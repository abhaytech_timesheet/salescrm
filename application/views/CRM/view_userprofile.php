
<?php 
	foreach($user->result() as $row)
	{
		break;
	}
$logincount=0; $logout=0; $logoutcount=0; $login_hour=0; $login_min=0; $logout_hour=0; $logout_min=0;?>
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="fa fa-user"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/<?php echo $this->router->fetch_method();?>/<?php echo $this->uri->segment(3);?>">View Profile</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-upload"></i>
						<span>CRM</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View Profile</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row profile">
				<div class="col-md-12">
					<!--BEGIN TABS-->
					<div class="tabbable tabbable-custom tabbable-full-width">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_1_1" data-toggle="tab">
									Overview
								</a>
							</li>
							<li>
								<a href="#tab_1_3" data-toggle="tab">
									Account
								</a>
							</li>
							<li>
								<a href="#tab_1_4" data-toggle="tab">
									Projects
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1_1">
								<div class="row">
									<div class="col-md-3">
										<ul class="list-unstyled profile-nav">
											<li>
												<img src="<?php echo base_url() ?>application/emp_pics/<?php echo $row->or_m_userimage; ?>" class="img-responsive" alt=""/>
											</li>
										</ul>
									</div>
									<div class="col-md-9">
										<div class="row">
											<div class="col-md-8 profile-info">
												<h1><?php echo $row->or_m_name; ?></h1>
												
												<p>
													<a href="#">
														<?php echo $row->or_m_email; ?>
													</a>
												</p>
												<ul class="list-inline">
													<li>
														<i class="fa fa-phone"></i> <?php echo $row->or_m_mobile_no; ?>
													</li>
													<li>
														<i class="fa fa-map-marker"></i> <?php echo $row->or_m_address; ?>
													</li>
													<li>
														<i class="fa fa-briefcase"></i> 
														<?php echo $row->m_des_name; ?>
													</li>
													<li>
														<i class="fa fa-birthday-cake"></i> <?php echo $row->or_m_dob; ?>
													</li>
													<li>
														<i class="fa fa-registered"></i> <?php echo $row->or_m_regdate; ?>
													</li>
													
													
												</ul>
											</div>
											<!--end col-md-8-->
											<div class="col-md-4">
												<div class="portlet sale-summary">
													<div class="portlet-title">
														<div class="caption">
															<?php echo $row->or_m_name; ?>
														</div>
														<div class="tools">
															<a class="reload" href="javascript:;">
															</a>
														</div>
													</div>
													<div class="portlet-body">
														<ul class="list-unstyled">
															<li>
																<span class="sale-info">
																	&nbsp; Days of Present :
																</span>
																<span class="sale-num">
																	<?php foreach($atten->result() as $row5)
																	{ echo $row5->Present; } ?>
																</span>
															</li>
															<li>
																<span class="sale-info">
																	&nbsp; No. of Absent :
																</span>
																<span class="sale-num">
																	<?php $sun=0;
																		for($j=1;$j<=date('d');$j++)
																		{ 
																			$c="a".$j;
																			
																			if($row5->$c!='P') 
																			{ 
																				if($row5->$c!=''){ $sun=$sun+1;  }
																			} 
																		} 
																		echo date('d')-$sun-$row5->Present;
																	?>
																</span>
															</li>
															
															<li>
																<span class="sale-info">
																	&nbsp; No. of Leave :
																</span>
																<span class="sale-num">
																	<?php echo $leave->leaves; 	?>
																</span>
															</li>
															
															<li>
																<span class="sale-info">
																	&nbsp; Login :
																</span>
																<span class="sale-num">
																	<?php
																		foreach($full_att->result() as $row1)
																		{
																			if($row1->emp_atten_status=='1') 
																			{
																				$login_hour=$row1->hour_of_attendance;
																				$login_min=$row1->min_of_attendance; 
																				break;
																			} 
																		}
																		echo $login_hour.":".$login_min; 
																	?>
																</span>
																<span class="sale-info">
																	&nbsp;Logout :	
																</span>
																<span class="sale-num">
																	<?php 
																		foreach($full_att->result() as $row1)
																		{
																			if($row1->emp_atten_status=='0') 
																			{
																				$logout=$row1->hour_of_attendance.":".$row1->min_of_attendance; 
																			} 
																		}
																		if($logout==0){} else {echo $row1->hour_of_attendance.":".$row1->min_of_attendance;} 
																	?>
																</span>
															</li>
															<li>
																<span class="sale-info">
																	&nbsp;Assign Task :	
																</span>
																<span class="sale-num">
																	<?php echo $task->taskk; 	?>
																</span>
															</li>
														</ul>
													</div>
												</div>
											</div>
											<!--end col-md-4-->
										</div>
										<!--end row-->
										<div class="tabbable tabbable-custom tabbable-custom-profile">
											<ul class="nav nav-tabs">
												<li class="active">
													<a href="#tab_1_11" data-toggle="tab">
														Generated Ticket
													</a>
												</li>
												<li >
													<a href="#tab_1_22" data-toggle="tab">
														Attendance Report
													</a>
												</li>
												<li>
													<a href="#tab_1_33" data-toggle="tab">
														Announcements
													</a>
												</li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane active" id="tab_1_11">
													<div class="portlet-body">
														<table class="table table-striped table-bordered table-advance table-hover">
															<thead>
																<tr>
																	<th>
																		<i class="fa fa-ticket"></i> Ticket No.
																	</th>
																	<th class="hidden-xs">
																		<i class="fa fa-briefcase"></i> Subject
																	</th>
																	<th class="hidden-xs">
																		<i class="fa fa-status"></i> Status
																	</th>
																	<th>
																		<i class="fa fa-bookmark"></i> Action
																	</th>
																	
																</tr>
															</thead>
															<tbody>
																<?php  
																	foreach($rec->result() as $row123)
																	{
																	?>
																	
																	<tr>
																		<td>
																			<a href="#">
																				<?php echo '#'.($row123->Ticket_reference_no); ?>
																			</a>
																		</td>
																		<td class="hidden-xs">
																			<?php echo $row123->Subject ?>
																		</td>
																		<td class="hidden-xs">
																			<?php echo $row123->C_Status ?>
																		</td>
																		<td>
																			<a href="<?php echo base_url();?>support/view_ticket/<?php echo $row123->Ticket_reference_no ?>" style="color:#0066FF; text-decoration:none;">
																				<span class="label label-success label-sm">
																					View
																				</span>
																			</a>
																		</td>
																	</tr>
																<?php } ?>
															</tbody>
														</table>
													</div>
												</div>
												
												<!--tab-pane-->
												<div class="tab-pane" id="tab_1_22">
													<div class="portlet-body" style="overflow-x: auto;">
														<table class="table table-striped table-bordered table-hover">
															<thead>
																<tr>
																	<?php for($i=1;$i<=date('d');$i++)
																		{ ?>
																		<th><?php echo $i."/".date('m'); ?></th>
																	<?php } ?>
																</tr>
															</thead>
															<tbody>
																<tr >
																	
																	<?php for($i=1;$i<=date('d');$i++)
																		{ 
																			$b="a".$i;
																		?>
																		<td> 
																			<?php 
																				if($row5->$b=='P') 
																				{ 
																				?>
																				
																				<?php echo "<font style='color:blue'>".$row5->$b."</font>"; ?> 
																				
																				<?php 
																				} 
																				else 
																				{  
																					if($row5->$b!="")
																					{
																						if($row5->$b!="<font style=color:red>S</font>")
																						{
																							$leave11=explode("_",$row5->$b);
																							if($leave11[0]=='leave')
																							{
																							?>
																							
																							<?php echo "<font style='color:green'>L</font>"; ?> 
																							
																							<?php 
																							}	
																							else
																							{ ?>
																							
																							<?php echo "<font style='color:blue'>H</font>"; ?> 
																							
																							<?php
																							}
																						}
																						else
																						{
																							echo $row5->$b;
																						}
																					}
																					else
																					{
																						echo 'A';  
																					}
																					
																				} 
																			?>
																		</td>
																	<?php } ?>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
												<!--tab-pane--><!--tab-pane-->
												<div class="tab-pane" id="tab_1_33">
													<div class="tab-pane active" id="tab_1_1_1">
														<div class="scroller" data-height="290px" data-always-visible="1" data-rail-visible1="1">
															<ul class="feeds">
																<?php  
																	
																	foreach($rec12->result() as $row11)
																	{
																		
																	?>
																	<li>
																		<div class="col1">
																			<div class="cont">
																				<div class="cont-col1">
																					<div class="label label-success">
																						<i class="fa fa-bell-o"></i>
																					</div>
																				</div>
																				<div class="cont-col2">
																					<div class="desc">
																						<?php echo substr($row11->txtdescription,0,100) ?>
																						<span class="label label-danger label-sm">
																							<a href="<?php echo base_url();?>support/full_Announcements/<?php echo $row11->id ?>" style="text-decoration:none; color:#fff" class="nav-link">
																								Read More <i class="fa fa-share"></i>
																							</a>
																						</span>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="col2">
																			<div class="date">
																				<?php echo substr($row11->adddate,0,10) ?>
																			</div>
																		</div>
																	</li>
																<?php } ?>
															</ul>
														</div>
													</div>
												</div>
												<!--tab-pane-->
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--tab_1_2-->
							<div class="tab-pane" id="tab_1_3">
								<div class="row profile-account">
									<div class="col-md-3">
										<ul class="ver-inline-menu tabbable margin-bottom-10">
											<li class="active">
												<a data-toggle="tab" href="#tab_1-1">
													<i class="fa fa-cog"></i> Personal info
												</a>
												<span class="after">
												</span>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_2-2">
													<i class="fa fa-picture-o"></i> Change Avatar
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_3-3">
													<i class="fa fa-lock"></i> Change Password
												</a>
											</li>
											
										</ul>
									</div>
									<div class="col-md-6">
										<div class="tab-content">
											<div id="tab_1-1" class="tab-pane active">
												<form role="form" action="#" id="myform1" >
													<div class="form-group">
														<label class="control-label">Name</label>
														<input type="text" id="txtname" name="txtname" value="<?php echo $row->or_m_name; ?>" placeholder="Enter Name" class="form-control input-sm empty" />
														<span id="divtxtname" style="color:red;"></span>
													</div>
													<div class="form-group">
														<label class="control-label">Email</label>
														<input type="text" name="txtemail" id="txtemail" value="<?php echo $row->or_m_email; ?>" placeholder="John@gmail.com" disabled class="form-control input-sm empty"/>
														<span id="divtxtemail" style="color:red;"></span>
													</div>
													<div class="form-group">
														<label class="control-label">Mobile Number</label>
														<input type="text" name="txtmobile" id="txtmobile" value="<?php echo $row->or_m_mobile_no; ?>" maxlength="10" placeholder="Enter Mobile No." class="form-control input-sm empty"/>
														<span id="divtxtmobile" style="color:red;"></span>
													</div>
													<div class="form-group">
														<label class="control-label">Date of Birth</label>
														<input type="text" name="txtdob" id="txtdob" value="<?php echo $row->or_m_dob; ?>" placeholder="yyyy/mm/dd" class="form-control input-sm date-picker" data-date-format="yyyy-mm-dd"/>
														<span id="divtxtdob" style="color:red;"></span>
													</div>
													<div class="form-group">
														<label class="control-label">Date of Joining</label>
														<input type="text" name="txtdoj" id="txtdoj" placeholder="yyyy/mm/dd" class="form-control input-sm date-picker" value="<?php echo $row->or_member_joining_date; ?>" readonly="readonly" data-date-format="yyyy-mm-dd"/>
														<span id="divtxtdoj" style="color:red;"></span>
													</div>
													<div class="form-group">
														<label class="control-label">Address</label>
														<textarea class="form-control input-sm empty" name="txtaddress" id="txtaddress" rows="3" placeholder="Enter Address Here"><?php echo $row->or_m_address; ?></textarea>
														<span id="divtxtaddress" style="color:red;"></span>
													</div>
													
													<div class="margiv-top-10">
														<a href="#" class="btn green" onclick="update_user()">
															Save Changes
														</a>
														<a href="#" class="btn default">
															Cancel
														</a>
													</div>
												</form>
											</div>
											<div id="tab_2-2" class="tab-pane">
												<h4>
													Add a Profile Picture
												</h4>
												<form role="form" enctype="multipart/form-data" action="<?php echo base_url();?>crm/change_profile_image" id="myform2" method="post">
													<div class="form-group">
														<div class="fileinput fileinput-new" data-provides="fileinput">
															<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
																<img src="<?php echo ($row->or_m_userimage=="" || $row->or_m_userimage=="0" ? "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" : base_url()."application/emp_pics/".$row->or_m_userimage);?>" alt="Profile Image" />
															</div>
															<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
															</div>
															<div>
																<span class="btn default btn-file">
																	<span class="fileinput-new">
																		Select image
																	</span>
																	<span class="fileinput-exists">
																		Change
																	</span>
																	<input type="file" name="userfile" id="userfile">
																</span>
																<a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
																	Remove
																</a>
															</div>
														</div>
														<input type="hidden" id="hdprofilepic" name="hdprofilepic" value="<?php echo $row->or_m_userimage; ?>" />
													</div>
													<div class="margin-top-10">
														<input type="submit" name="submit" value="Submit" class="btn green" />
														<a href="#" class="btn default">
															Cancel
														</a>
													</div>
												</form>
											</div>
											<div id="tab_3-3" class="tab-pane">
												<form action="#" method="post" id="myform3">
													<div class="form-group">
														<label class="control-label">Current Password</label>
														<input type="password" name="txtoldpassword" id="txtoldpassword" class="form-control input-sm empty"/>
														<span id="divtxtoldpassword" style="color:red;"></span>
													</div>
													<div class="form-group">
														<label class="control-label">New Password</label>
														<input type="password" name="txtpassword" id="txtpassword" class="form-control input-sm empty"/>
														<span id="divtxtpassword" style="color:red;"></span>
													</div>
													<div class="form-group">
														<label class="control-label">Re-type New Password</label>
														<input type="password" name="txtrepassword" id="txtrepassword" class="form-control input-sm empty"/>
														<span id="divtxtrepassword" style="color:red;"></span>
													</div>
													<div class="margin-top-10">
														<a href="#" class="btn green" onclick="change_password()">
															Change Password
														</a>
														<a href="" class="btn default">
															Cancel
														</a>
													</div>
												</form>
											</div>
											
										</div>
									</div>
									<!--end col-md-9-->
								</div>
							</div>
							<!--end tab-pane-->
							<div class="tab-pane" id="tab_1_4">
								<div class="row">
									<div class="col-md-12">
										<div class="add-portfolio">
											<span>
												Total Number of Project in Which You are Participates.&nbsp; 
											</span>
											<span class="label label-default" id="pulsate-regular">
												<?php echo $total_project; ?>
											</span>
										</div>
									</div>
								</div>
								<!--end add-portfolio-->
								<div class="scroller" data-height="450px" data-always-visible="1" data-rail-visible1="1">
									<?php 
										$sn=0;
										foreach($project->result() as $row4)
										{
											$sn++;
										?>
										<div class="row portfolio-block">
											<div class="col-md-1 portfolio-text">
												<div class="portfolio-text-info" style="margin-left:5%;">
													<h3><?php echo $sn; ?>-</h3>
												</div>
											</div>
											
											<div class="col-md-2 portfolio-text">
												<div class="portfolio-text-info" style="margin-left:5%;">
													<h4><b><?php echo $row4->Account_name.'<span style="color:red">/</span>'.$row4->Website_name.'<span style="color:red">/</span>'.$row4->PROJECT_NAME; ?></b></h4>
													<p>
														<?php echo substr($row4->PROJ_DESC,0,50); ?>
													</p>
												</div>
											</div>
											
											<div class="col-md-3 portfolio-text">
												<div class="portfolio-text-info" style="margin-left:5%;">
													<h4><b>Project Manager</b></h4>
													<p>
														<?php echo $row4->PRO_MANAGER; ?>
													</p>
												</div>
											</div>
											
											<div class="col-md-2 portfolio-text">
												<div class="portfolio-text-info" style="margin-left:5%;">
													<h4><b>Team Leader</b></h4>
													<p>
														<?php echo $row4->TEAM_LEADER; ?>
													</p>
												</div>
											</div>
											
											<div class="col-md-2 portfolio-text">
												<div class="portfolio-text-info" style="margin-left:5%;">
													<h4><b>Assistant</b></h4>
													<p>
														<?php echo $row4->PRO_ASSISTENT; ?>
													</p>
												</div>
											</div>
											
											<div class="col-md-2 portfolio-text">
												<div class="portfolio-text-info" style="margin-left:5%;">
													<h4><b>Executive</b></h4>
													<p>
														<?php echo $row4->PRO_EXECUTIVE; ?>
													</p>
												</div>
											</div>
											
											
										</div>
									<?php } ?>
								</div>
								<!--end add-portfolio-->
								
								<!--end tab-pane-->
							</div>
						</div>
						<!--END TABS-->
					</div>
				</div>					
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END CONTENT -->
			
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													

<script>
	function update_user()
	{
		var datas=$("#myform1").serialize();
        alert("hello");
		$.ajax(
		{
			type: "POST",
			url:"<?php echo base_url();?>crm/update_profile_details",
			data:datas,
			success: function(msg) {
				if(msg.trim()=="true")
				{
					$(".page-content").html("<center><h2>Detail Update Successfully!</h2></center>")
					.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
					.hide()
					.fadeIn(1000,function()
					{
						location.reload();
					}
					);
					
				}	  
			}
		});
		
	}
</script>

<script>
	function change_password()
	{
		if(confirm('Are you sure to change password?'))
		{
			var oldpassword = $('#txtoldpassword').val();
			var password = $('#txtpassword').val();
			var repassword = $('#txtrepassword').val();
			if(oldpassword!='')
			{
				if(password==repassword)
				{
					$.ajax(
					{
						type: "POST",
						url:"<?php echo base_url();?>crm/update_profile_password",
						data:"txtoldpassword="+oldpassword+"&txtpassword="+password+"&txtrepassword="+repassword,
						success: function(msg) {
							if(msg.trim()=="true")
							{
								$(".page-content").html("<center><h2>Password Update Successfully !</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(1000,function()
								{
									location.reload();
								}
								);
								
							}
                            else
                            {
                             alert(msg.trim());
                            }	  
						}
					});
				}
				else
				{
					alert('Confirm Password Not Match');	
				}
			}
			else
			{
				alert('Please enter old password');
			}
		}
	}
</script>
