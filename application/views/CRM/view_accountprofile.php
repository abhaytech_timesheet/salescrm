<?php 
	foreach($acc->result() as $row)
	{
break;
    }
?>
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
<?php
						if($this->session->userdata('owned_by')==0)
			{
			?>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_account/0">
							CRM
						</a>
<?php }
			?>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-bubbles"></i>
<?php 
						if($this->session->userdata('owned_by')==0)
			{
			?>
						<a href="<?php echo base_url();?>crm/view_account/0">Account</a>
<?php }
				
			?>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-plus"></i>
						<span>Account Profile</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View Account Profile</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row profile">
				<div class="col-md-12">
					<!--BEGIN TABS-->
					<div class="tabbable tabbable-custom tabbable-full-width">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_1_1" data-toggle="tab">
									Overview
								</a>
							</li>
							<li>
								<a href="#tab_1_3" data-toggle="tab">
									Account
								</a>
							</li>
							<li>
								<a href="#tab_1_4" data-toggle="tab">
									Projects
								</a>
							</li>
							<li>
								<a href="#tab_1_6" data-toggle="tab">
									Help
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<!--tab_1_1--->
							<div class="tab-pane active" id="tab_1_1">
								<div class="row">
									<div class="col-md-3">
										<ul class="list-unstyled profile-nav">
											<li>
                                                <img src="<?php echo base_url()."application/uploadimage/".$row->account_image; ?>" class="img-responsive" alt="Profile Picture" />
											</li>
											
										</ul>
									</div>
									<div class="col-md-9">
										<div class="row">
											<div class="col-md-8 profile-info">
												<h1><?php echo $row->account_name; ?></h1>
												<p>
													<?php echo $row->account_desc; ?>
												</p>
												<p>
													<a href="#">
														<?php echo $row->account_email; ?>
													</a>
												</p>
												<ul class="list-inline">
													<li>
														<i class="fa fa-phone"></i> <?php echo $row->account_phone; ?>
													</li>
													<li>
														<i class="fa fa-map-marker"></i>  
														<?php 
															foreach($loc->result() as $l)
															{
																if($row->account_city==$l->m_loc_id)
																{
																	echo $l->m_loc_name;
																}
															}
														echo '&nbsp; , '.$row->account_address; ?>
													</li>
													<li>
														<i class="fa fa-calendar"></i> <?php echo $row->account_regdate; ?>
													</li>
													
												</ul>
											</div>
											<!--end col-md-8-->
											
											<!--end col-md-4-->
										</div>
										<!--end row-->
										<div class="tabbable tabbable-custom tabbable-custom-profile">
											<ul class="nav nav-tabs">
												<li class="active">
													<a href="#tab_1_11" data-toggle="tab">
														Generated Ticket
													</a>
												</li>
												<li>
													<a href="#tab_1_22" data-toggle="tab">
														Announcements
													</a>
												</li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane active" id="tab_1_11">
													<div class="portlet-body">
														<table class="table table-striped table-bordered table-advance table-hover">
															<thead>
																<tr>
																	<th>
																		<i class="fa fa-ticket"></i> Ticket No
																	</th>
																	<th>
																		Subject
																	</th>
																	
																	<th>
																		Action
																	</th>
																</tr>
															</thead>
															<tbody>
																<?php
																	foreach($ticket->result() as $tic)
																	{
																		if($tic->tkt_email==$row->account_email)
																		{
																		?>
																		<tr>
																			<td>
																				<a href="#">
																					<?php
																						echo '#'.$tic->tkt_ref_no;
																					?>
																				</a>
																			</td>
																			<td>
																				<a href="#">
																					<?php
																						echo $tic->tkt_subject;
																					?>
																				</a>
																			</td>
																			<td>
																				<a class="btn default btn-xs blue-stripe" href="<?php echo base_url();?>index.php/admin/view_ticket/<?php echo $tic->tkt_ref_no; ?>">
																					View
																				</a>
																			</td>
																		</tr>
																		<?php
																		}
																	}
																?>
															</tbody>
														</table>
													</div>
												</div>
												<!--tab-pane-->
												<div class="tab-pane" id="tab_1_22">
													<div class="tab-pane active" id="tab_1_1_1">
														<div class="scroller" data-height="200px" data-always-visible="1" data-rail-visible1="1">
															<ul class="feeds">
																<?php  
																	foreach($anonce->result() as $row12)
																	{	
																	?>
																	<li>
																		<div class="col1">
																			<div class="cont">
																				<div class="cont-col1">
																					<div class="label label-success">
																						<i class="fa fa-bullhorn"></i>
																					</div>
																				</div>
																				<div class="cont-col2">
																					<div class="desc">
																						<?php echo $row12->txttitle; ?>
																					</div>
																				</div>
																			</div>
																		</div>
																	</li>
																	<?php
																	}
																?>
															</ul>
														</div>
													</div>
												</div>
												<!--tab-pane-->
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--tab_1_2-->
							
							<div class="tab-pane" id="tab_1_3">
								<div class="row profile-account">
									<div class="col-md-3">
										<ul class="ver-inline-menu tabbable margin-bottom-10">
											
											<li class="active">
												<a data-toggle="tab" href="#tab_2_1">
													<i class="icon-bubbles"></i> Change Account Detail
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_2_2">
													<i class="fa fa-phone"></i> Change Contact Detail
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_2_3">
													<i class="fa fa-picture-o"></i> Change Account Image
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_2_4">
													<i class="fa fa-picture-o"></i> Change Contact Image
												</a>
											</li>
											
											
										</ul>
									</div>
									<div class="col-md-9">
										<div class="tab-content">
											
											
											<div id="tab_2_1" class="tab-pane active">
												<h4>
													Edit a Account Detail
												</h4>
												<form role="form" enctype="multipart/form-data" action="<?php echo base_url();?>crm/update_account_detail/<?php echo $row->account_id."/".$this->uri->segment(3); ?>"  method="post" id="insert_data" >
													<div class="form-group">
														<label class="control-label">Account Name</label>
														<input type="text" id="txtname" name="txtname" value="<?php echo $row->account_name; ?>" placeholder="Enter Account Name" class="form-control input-sm empty" />
														<span id="divtxtname" style="color:red;"></span>
													</div>
													
													<div class="form-group">
														<label class="control-label"> Mobile Number</label>
														<input type="text" name="txtmobile" id="txtmobile" value="<?php echo $row->account_phone; ?>" maxlength="10" placeholder="Enter Mobile No." class="form-control input-sm empty"/>
														<span id="divtxtmobile" style="color:red;"></span>
													</div>
													
                                                    <div class="form-group">
														<label class="control-label"> State</label>
														<select id="ddstate" name="ddstate" class="form-control input-sm opt" onchange="get_city()">
                                                        <option value="-1">-- Select State --</option>
                                                        <?php foreach($state->result() AS $rowst) { ?>
                                                        <option value="<?php echo $rowst->m_loc_id;?>" <?php echo ($rowst->m_loc_id==$row->account_state ? 'selected':''); ?>><?php echo $rowst->m_loc_name;?></option>
                                                        <?php } ?>
                                                        </select>
														<span id="divddstate" style="color:red;"></span>
													</div>
                                                     <input type="hidden" id="hdcity" name="hdcity" value="<?php echo $row->account_city?>" />
                                                    <div class="form-group">
														<label class="control-label"> City</label>
														<select id="ddcity" name="ddcity" class="form-control input-sm opt">
                                                        <option value="-1">-- Select City --</option>
                                                        </select>
														<span id="divddcity" style="color:red;"></span>
													</div>
													
													<div class="form-group">
														<label class="control-label"> Address</label>
														<textarea class="form-control input-sm empty" name="txtaddress" id="txtaddress" rows="3" placeholder="Enter Address"><?php echo $row->account_address; ?></textarea>
														<span id="divtxtaddress" style="color:red;"></span>
													</div>

                                                    <div class="form-group">
														<label class="control-label"> Pin Code</label>
														<input type="text" name="txtpincode" id="txtpincode" value="<?php echo $row->account_zipcode; ?>" maxlength="6" placeholder="Enter Pin Code" class="form-control input-sm empty"/>
														<span id="divtxtpincode" style="color:red;"></span>
													</div>
													
													<div class="margiv-top-10">
														<button type="button" onclick="conwv('insert_data')" class="btn blue">Submit</button>
														<button type="reset" class="btn default">Cancel</button>
													</div>
												</form>
											</div>
											
											<div id="tab_2_2" class="tab-pane">
												<h4>
													Edit a Contact Detail
												</h4>
												<form role="form" enctype="multipart/form-data" action="<?php echo base_url();?>crm/update_contact_detail/<?php echo  $this->session->userdata('profile_id')."/".$this->uri->segment(3); ?>"  method="post" id="insert_data1" >
														<div class="form-group">
														<label class="control-label">Contact Name</label>
														<input type="text" id="txtcontname" name="txtcontname" value="<?php echo $row->contact_name; ?>" placeholder="Enter Contact Name" class="form-control input-sm empty" />
														<span id="divtxtcontname" style="color:red;"></span>
													</div>
													
													<div class="form-group">
														<label class="control-label"> Mobile Number</label>
														<input type="text" name="txtcontmobile" id="txtcontmobile" value="<?php echo $row->contact_mobile; ?>" maxlength="10" placeholder="Enter Mobile No." class="form-control input-sm empty"/>
														<span id="divtxtcontmobile" style="color:red;"></span>
													</div>
													
                                                    <div class="form-group">
														<label class="control-label"> State</label>
														<select id="ddcontstate" name="ddcontstate" class="form-control input-sm opt" onchange="get_city1()">
                                                        <option value="-1">-- Select State --</option>
                                                        <?php foreach($state->result() AS $rowst1) { ?>
                                                        <option value="<?php echo $rowst1->m_loc_id;?>" <?php echo ($rowst1->m_loc_id==$row->contact_state ? 'selected':''); ?> ><?php echo $rowst1->m_loc_name;?></option>
                                                        <?php } ?>
                                                        </select>
														<span id="divddcontstate" style="color:red;"></span>
													</div>
                                                    <input type="hidden" id="hdcontcity" name="hdcontcity" value="<?php echo $row->contact_city; ?>" />
                                                    <div class="form-group">
														<label class="control-label"> City</label>
														<select id="ddcontcity" name="ddcontcity" class="form-control input-sm opt">
                                                        <option value="-1">-- Select City --</option>
                                                        </select>
														<span id="divddcontcity" style="color:red;"></span>
													</div>
													
													<div class="form-group">
														<label class="control-label"> Address</label>
														<textarea class="form-control input-sm empty" name="txtcontaddress" id="txtcontaddress" rows="3" placeholder="Enter Address"><?php echo $row->contact_address; ?></textarea>
														<span id="divtxtcontaddress" style="color:red;"></span>
													</div>

                                                    <div class="form-group">
														<label class="control-label"> Pin Code</label>
														<input type="text" name="txtcontpincode" id="txtcontpincode" value="<?php echo $row->contact_zipcode; ?>" maxlength="6" placeholder="Enter Pin Code" class="form-control input-sm empty"/>
														<span id="divtxtcontpincode" style="color:red;"></span>
													</div>
													
													<div class="margiv-top-10">
														<button type="button" onclick="conwv('insert_data1')" class="btn blue">Submit</button>
														<button type="reset" class="btn default">Cancel</button>
													</div>
												</form>
											</div>
											
											
											<div id="tab_2_3" class="tab-pane">
												<h4>
													Change a Account Image
												</h4>
												<form role="form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/crm/change_account_image/<?php echo $this->uri->segment(3); ?>" method="post">
													<div class="form-group">
														<div class="fileinput fileinput-new" data-provides="fileinput">
															<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                 
																<img src="<?php echo ($row->account_image!="" || $row->account_image!="0" ?  base_url()."application/uploadimage/".$row->account_image : "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"); ?>" alt="" height="200" width="200"/>
															</div>
															<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
															</div>
															<div>
																<span class="btn default btn-file">
																	<span class="fileinput-new">
																		Select image
																	</span>
																	<span class="fileinput-exists">
																		Change
																	</span>
																	<input type="file" name="userfile" id="userfile">
																</span>
																<a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
																	Remove
																</a>
															</div>
														</div>
														<input type="hidden" id="hdaccimg" name="hdaccimg" value="<?php echo $row->account_image; ?>" />
													</div>
													<div class="margin-top-10">
														<input type="submit" name="submit" value="Submit" class="btn green" />
														<a href="#" class="btn default">
															Cancel
														</a>
													</div>
												</form>
											</div>
											
											<div id="tab_2_4" class="tab-pane">
												<h4>
													Change a Contact Image
												</h4>
												<form role="form" enctype="multipart/form-data" action="<?php echo base_url();?>index.php/crm/change_contact_image/<?php echo  $this->session->userdata('profile_id')."/".$this->uri->segment(3); ?>" method="post">
													<div class="form-group">
														<div class="fileinput fileinput-new" data-provides="fileinput">
															<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
																<img src="<?php echo ($row->contact_image!="" || $row->contact_image!="0" ?  base_url()."application/uploadimage/".$row->contact_image : "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"); ?>" alt="" height="200" width="200" />
															</div>
															<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
															</div>
															<div>
																<span class="btn default btn-file">
																	<span class="fileinput-new">
																		Select image
																	</span>
																	<span class="fileinput-exists">
																		Change
																	</span>
																	<input type="file" name="userfile" id="userfile">
																</span>
																<a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
																	Remove
																</a>
															</div>
														</div>
														<input type="hidden" id="hdcontimg" name="hdcontimg" value="<?php echo $row->contact_image; ?>" />
													</div>
													<div class="margin-top-10">
														<input type="submit" name="submit" value="Submit" class="btn green" />
														<a href="#" class="btn default">
															Cancel
														</a>
													</div>
												</form>
											</div>
											
											
										</div>
									</div>
									<!--end col-md-9-->
								</div>
							</div>
							<!--end tab-pane-->
							<div class="tab-pane" id="tab_1_4">
								<div class="row">
									<div class="col-md-12">
										<div class="add-portfolio">
											<span>
												Take an Action
											</span>
											<!--a href="<?php echo base_url() ?>index.php/crm/view_account_project/<?php echo $this->uri->segment(3); ?>" class="btn icn-only green">
												View All Project <i class="m-icon-swapright m-icon-white"></i>
											</a-->
										</div>
									</div>
								</div>
								<!--end add-portfolio-->
								<?php 
									foreach($pro->result() as $row4)
									{ ?>
									<div class="row portfolio-block">
										<div class="col-md-3 portfolio-text">
											<div class="portfolio-text-info">
												<h4><?php echo $row4->PROJECT_NAME; ?></h4>
												<p>
													<?php echo substr($row4->PROJECT_NAME,0,50); ?>
												</p>
											</div>
										</div>
										<div class="col-md-7 portfolio-stat">
											<div class="portfolio-info">
												Project SNo.
												<span>
													<?php echo $row4->PROJECT_SNO; ?>
												</span>
											</div>
											
											<div class="portfolio-info">
												Cost
												<span>
													&nbsp;&nbsp;<i class="fa fa-rupee (alias)"></i> &nbsp;<?php echo $row4->UNIT_PRICE; ?> 
												</span>
											</div>
										</div>
										<div class="col-md-2 portfolio-btn">
											<a href="#" class="btn bigicn-only">
												<span>
													RAISE TICKET
												</span>
											</a>
										</div>
									</div>
									
								<?php } ?>
								<!--end row-->
							</div>
							
							
							
							<div class="tab-pane" id="tab_1_6">
								<div class="row">
									<div class="col-md-3">
										<ul class="ver-inline-menu tabbable margin-bottom-10">
											<li class="active">
												<a data-toggle="tab" href="#tab_1">
													<i class="fa fa-briefcase"></i> General Questions
												</a>
												<span class="after">
												</span>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_2">
													<i class="fa fa-group"></i> Membership
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_3">
													<i class="fa fa-leaf"></i> Terms Of Service
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_1">
													<i class="fa fa-info-circle"></i> License Terms
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_2">
													<i class="fa fa-tint"></i> Payment Rules
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_3">
													<i class="fa fa-plus"></i> Other Questions
												</a>
											</li>
										</ul>
									</div>
									<div class="col-md-9">
										<div class="tab-content">
											<div id="tab_1" class="tab-pane active">
												<div id="accordion1" class="panel-group">
													<div class="panel panel-default">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_1">
																	1. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ?
																</a>
															</h4>
														</div>
														<div id="accordion1_1" class="panel-collapse collapse in">
															<div class="panel-body">
																Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
															</div>
														</div>
													</div>
													<div class="panel panel-default">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_2">
																	2. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ?
																</a>
															</h4>
														</div>
														<div id="accordion1_2" class="panel-collapse collapse">
															<div class="panel-body">
																Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
															</div>
														</div>
													</div>
													<div class="panel panel-success">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_3">
																	3. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor ?
																</a>
															</h4>
														</div>
														<div id="accordion1_3" class="panel-collapse collapse">
															<div class="panel-body">
																Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
															</div>
														</div>
													</div>
													<div class="panel panel-warning">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_4">
																	4. Wolf moon officia aute, non cupidatat skateboard dolor brunch ?
																</a>
															</h4>
														</div>
														<div id="accordion1_4" class="panel-collapse collapse">
															<div class="panel-body">
																3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
															</div>
														</div>
													</div>
													<div class="panel panel-danger">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_5">
																	5. Leggings occaecat craft beer farm-to-table, raw denim aesthetic ?
																</a>
															</h4>
														</div>
														<div id="accordion1_5" class="panel-collapse collapse">
															<div class="panel-body">
																3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et
															</div>
														</div>
													</div>
													<div class="panel panel-default">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_6">
																	6. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth ?
																</a>
															</h4>
														</div>
														<div id="accordion1_6" class="panel-collapse collapse">
															<div class="panel-body">
																3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et
															</div>
														</div>
													</div>
													<div class="panel panel-default">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_7">
																	7. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft ?
																</a>
															</h4>
														</div>
														<div id="accordion1_7" class="panel-collapse collapse">
															<div class="panel-body">
																3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et
															</div>
														</div>
													</div>
												</div>
											</div>
											<div id="tab_2" class="tab-pane">
												<div id="accordion2" class="panel-group">
													<div class="panel panel-warning">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_1">
																	1. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ?
																</a>
															</h4>
														</div>
														<div id="accordion2_1" class="panel-collapse collapse in">
															<div class="panel-body">
																<p>
																	Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
																</p>
																<p>
																	Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
																</p>
															</div>
														</div>
													</div>
													<div class="panel panel-danger">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_2">
																	2. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ?
																</a>
															</h4>
														</div>
														<div id="accordion2_2" class="panel-collapse collapse">
															<div class="panel-body">
																Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
															</div>
														</div>
													</div>
													<div class="panel panel-success">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_3">
																	3. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor ?
																</a>
															</h4>
														</div>
														<div id="accordion2_3" class="panel-collapse collapse">
															<div class="panel-body">
																Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
															</div>
														</div>
													</div>
													<div class="panel panel-default">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_4">
																	4. Wolf moon officia aute, non cupidatat skateboard dolor brunch ?
																</a>
															</h4>
														</div>
														<div id="accordion2_4" class="panel-collapse collapse">
															<div class="panel-body">
																3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
															</div>
														</div>
													</div>
													<div class="panel panel-default">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_5">
																	5. Leggings occaecat craft beer farm-to-table, raw denim aesthetic ?
																</a>
															</h4>
														</div>
														<div id="accordion2_5" class="panel-collapse collapse">
															<div class="panel-body">
																3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et
															</div>
														</div>
													</div>
													<div class="panel panel-default">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_6">
																	6. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth ?
																</a>
															</h4>
														</div>
														<div id="accordion2_6" class="panel-collapse collapse">
															<div class="panel-body">
																3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et
															</div>
														</div>
													</div>
													<div class="panel panel-default">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_7">
																	7. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft ?
																</a>
															</h4>
														</div>
														<div id="accordion2_7" class="panel-collapse collapse">
															<div class="panel-body">
																3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et
															</div>
														</div>
													</div>
												</div>
											</div>
											<div id="tab_3" class="tab-pane">
												<div id="accordion3" class="panel-group">
													<div class="panel panel-danger">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_1">
																	1. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ?
																</a>
															</h4>
														</div>
														<div id="accordion3_1" class="panel-collapse collapse in">
															<div class="panel-body">
																<p>
																	Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
																</p>
																<p>
																	Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
																</p>
																<p>
																	Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
																</p>
															</div>
														</div>
													</div>
													<div class="panel panel-success">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_2">
																	2. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ?
																</a>
															</h4>
														</div>
														<div id="accordion3_2" class="panel-collapse collapse">
															<div class="panel-body">
																Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
															</div>
														</div>
													</div>
													<div class="panel panel-default">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_3">
																	3. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor ?
																</a>
															</h4>
														</div>
														<div id="accordion3_3" class="panel-collapse collapse">
															<div class="panel-body">
																Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
															</div>
														</div>
													</div>
													<div class="panel panel-default">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_4">
																	4. Wolf moon officia aute, non cupidatat skateboard dolor brunch ?
																</a>
															</h4>
														</div>
														<div id="accordion3_4" class="panel-collapse collapse">
															<div class="panel-body">
																3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
															</div>
														</div>
													</div>
													<div class="panel panel-default">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_5">
																	5. Leggings occaecat craft beer farm-to-table, raw denim aesthetic ?
																</a>
															</h4>
														</div>
														<div id="accordion3_5" class="panel-collapse collapse">
															<div class="panel-body">
																3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et
															</div>
														</div>
													</div>
													<div class="panel panel-default">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_6">
																	6. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth ?
																</a>
															</h4>
														</div>
														<div id="accordion3_6" class="panel-collapse collapse">
															<div class="panel-body">
																3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et
															</div>
														</div>
													</div>
													<div class="panel panel-default">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_7">
																	7. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft ?
																</a>
															</h4>
														</div>
														<div id="accordion3_7" class="panel-collapse collapse">
															<div class="panel-body">
																3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--END TABS-->
					</div>
				</div>					
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END CONTENT -->
			
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->		

<script>
	    get_city();
		function get_city()
		{
			var state_id=$("#ddstate").val();
			var city=$("#hdcity").val();
			$("#ddcity").empty();
			if(state_id!='-1' && state_id!='')
			{
				
				$.ajax(
				{
					type:"POST",
					url:"<?php echo base_url();?>index.php/crm/select_city/"+state_id+"/0",
					dataType: "JSON",
					data:{'state_id':state_id},
					success:function(data){
						
						
						$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
						$.each(data,function(i,item)
						{
						    if(city==item.m_loc_id)
							{
							$("#ddcity").append("<option value="+item.m_loc_id+" selected>"+item.m_loc_name+"</option>");
							}
							else
							{
								$("#ddcity").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
							}
						});
					}
				});
			}
			else
			{
				$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
				alert("Please First Select The State");
			}
		}
</script>
<script>
	   get_city1();
		function get_city1()
		{
			var state_id=$("#ddcontstate").val();
			var city=$("#hdcontcity").val();
			$("#ddcontcity").empty();
			if(state_id!='-1' && state_id!='')
			{
			
				$.ajax(
				{
					type:"POST",
					url:"<?php echo base_url();?>index.php/crm/select_city/"+state_id+"/0",
					dataType: "JSON",
					data:{'state_id':state_id},
					success:function(data){
						
					
						$("#ddcontcity").append("<option value=-1> ~~ Select ~~</option>");
						$.each(data,function(i,item)
						{
							if(city==item.m_loc_id)
							{
							$("#ddcontcity").append("<option value="+item.m_loc_id+" selected>"+item.m_loc_name+"</option>");
							}
							else
							{
								$("#ddcontcity").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
							}
						});
					}
				});
			}
			else
			{
				$("#ddcontcity").append("<option value=-1> ~~ Select ~~</option>");
				alert("Please First Select The State");
			}
		}
</script>											