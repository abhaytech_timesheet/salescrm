<link href="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					 <li>
							<i class="icon-badge"></i>
							<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_lead/<?php echo $this->uri->segment(3);?>">CRM</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
						    <i class="icon-bag"></i>
							<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_lead/<?php echo $this->uri->segment(3);?>">
								View All Leads
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
						   <i class="fa fa-plus"></i>
						    <span>
								Create Lead 
							</span>
							<i class="fa fa-angle-right"></i>
						</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Create Lead </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Create New lead </span>
							</div>
							
							<!-- <div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div> -->
						</div>
						<div class="portlet-body form">
							<form  class="horizontal-form" id="form_sample_1">
								<div class="form-body">
									<h4 class="caption-subject font-blue bold uppercase"> Lead Information</h4>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Subject</label>
												<textarea id="txtltitle" name="txtltitle" class="form-control input-sm empty" placeholder="Describe the lead" style="font-size:20px" col="3"></textarea>	
												<span id="divtxtltitle" style="color:red"></span>															
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Lead Ownername</label>
												<input class="form-control input-sm" placeholder="Enter Lead Ownername" name="txtldownername" id="txtldownername" size="16" type="text"  value="<?php echo $this->session->userdata('name')?>" disabled />
												<input class="form-control input-sm empty" placeholder="Enter Lead Ownername" name="txtldownerid" id="txtldownerid" size="16" type="hidden" value="<?php echo $this->session->userdata('profile_id')?>" />
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Name</label>
											</div>
											<div class="form-group">
												<div class="col-md-4" style="margin:0; padding:0">
													<select id="ddprefix" name="ddprefix" class="form-control input-sm opt">
														<option value="-1">Select</option>
														<option value="1">Mr</option>
														<option value="2">Ms</option>
														<option value="3">Mrs</option>
														<option value="4">Dr</option>
														<option value="5">Prof</option>
													</select>
												</div>
												<div class="col-md-8" style="margin:0; padding:0">
													<input type="text" class="form-control input-sm empty" name="txtleadname" placeholder="Enter Name" id="txtleadname">
												</div>
												<span id="divddprefix" style="color:red"></span>	
												<span id="divtxtleadname" style="color:red"></span>	
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Mobile Number</label>
											</div>
											<div class="form-group">
												<input type="text" class="form-control input-sm" placeholder="Enter Lead Mobile Number" name="txtmobile" id="txtmobile" maxlength="10">
												<span id="divtxtmobile" style="color:red"></span>	
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Email ID</label>
											</div>
											<div class="form-group">
												<input type="text" class="form-control input-sm" name="txtmail" placeholder="Enter Email address" id="txtmail">
												<span id="divtxtmail" style="color:red"></span>	
											</div>
										</div>
									</div>
									
									
									<div class="row">
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Company</label>
												<input type="text" class="form-control input-sm" name="txtcname" placeholder="Enter Company Name" id="txtcname">
												<span id="divtxtcname" style="color:red"></span>	
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Industry</label>
												<select class="form-control input-sm opt" name="ddindustry" id="ddindustry">
													<option value="--Select--">Select Industry</option>
													<?php
														foreach($ind->result() as $row)
														{
														?>
														<option value="<?php echo $row->industry_id; ?>"><?php echo $row->industry_name; ?></option>
														<?php
														}
													?>
												</select>
												<span id="divddindustry" style="color:red"></span>	
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">No of Employees</label>
												<input type="text" Placeholder="Enter Employees" name="txtnoemp" id="txtnoemp" class="form-control input-sm empty">
												<span id="divtxtnoemp" style="color:red"></span>	
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Lead For Product</label></br>
												
												<select class="bs-select form-control input-sm opt" name="ddenqtype" id="ddenqtype" multiple>
												    <option value="1">Logistics</option>
													<option value="2">Software</option>
                                                    <option value="3">Manpower</option>
												</select>
												<script id="example">
													/*$('#ddenqtype').multiselect({
														enableClickableOptGroups: true
													});*/
												</script>	
												<span id="divddenqtype" style="color:red"></span>																		
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Lead Source</label>
												<select id="ddsource" name="ddsource" class="form-control input-sm opt">
													<option value="-1">Select Lead Source</option>
													<?php
														foreach($lsource->result() as $row)
														{
														?>
														<option value="<?php echo $row->source_id;?>"><?php echo $row->source_name;?></option>
														<?php
														}
													?>
												</select>	
												<span id="divddsource" style="color:red"></span>																		
											</div>
										</div>
										
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Lead Status </label>
												<select id="ddldstatus" name="ddldstatus" class="form-control input-sm opt">
													<option selected="selected" value="">Select status</option>
													<?php
														foreach($lstatus->result() as $row)
														{
														?>
														<option value="<?php echo $row->status_id; ?>"><?php echo $row->status_name; ?></option>
														<?php
														}
													?>
												</select>
												<span id="divddldstatus" style="color:red"></span>	
											</div>
										</div>
									</div>
									<h4 class="caption-subject font-blue bold uppercase">Address Information</h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Country</label>
												<select class="form-control input-sm opt" name="ddcountry" id="ddcountry" onchange="get_state()">
													<option value="">Select Country</option>
													<?php
														foreach($loc->result() as $row)
														{
														?>
														<option value="<?php echo $row->m_loc_id?>"><?php echo $row->m_loc_name; ?></option>
														<?php
														}
													?>		
												</select>
												<span id="divddcountry" style="color:red"></span>	
											</div>
										</div>
										<!--/span-->
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">State</label>
												<div id="state">
													<select class="form-control input-sm opt" name="ddstate" id="ddstate" onchange="get_city()">
														<option value="">Select State</option>
														
													</select>
												</div>
												<span id="divddstate" style="color:red"></span>
											</div>
										</div>
										
										<!--row-->
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">City</label>
												<div id="city">
													<select class="form-control input-sm opt" name="ddcity" id="ddcity">
														<option value="">Select City</option>
													</select>										
												</div>
												<span id="divddcity" style="color:red"></span>
											</div>
										</div>
									</div>
									<!--/span-->
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Street
												</label>
												<input type="text" class="form-control input-sm " PLACEHOLDER="Enter Street/Locality" name="txtstreet" id="txtstreet">										
												<span id="divtxtstreet" style="color:red"></span>	
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Zip Code
												</label>
												<input type="text" class="form-control input-sm " placeholder="Enter Zipcode" name="txtzipcode" id="txtzipcode" maxlength="6">
												<span id="divtxtzipcode" style="color:red"></span>	
											</div>
										</div>
										<!--/span-->
									</div>
									
									<!--row-->
									<h4 class="caption-subject font-blue bold uppercase">Description Information</h4>
									<div class="row">
										<div class="col-md-10">
											<div class="form-group">
												<label class="control-label">Lead Description</label>
												<textarea class="form-control input-sm " PLACEHOLDER="Enter Lead Description Information" name="txtlead_des" id="txtlead_des" style="font-size:20px" col="3"></textarea>										
												<span id="divtxtlead_des" style="color:red"></span>	
											</div>
										</div>
									</div>
									
								</div>
								<div id="function" class="form-actions middle">
									<button type="button" onclick="create_lead(0)" class="btn blue"><i class="fa fa-check"></i> Save</button>
									<button type="button" onclick="create_lead(1)" class="btn blue" >Save & Next</button>
									<button type="reset" class="btn default">Cancel</button>
									
								</div>
							</form>
							<!-- END FORM--> 
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													

<script>
	/*fill_userid();*/
	function fill_userid()
	{
		var user_id=<?php echo $this->session->userdata('profile_id')?>;
		if(user_id!="0")
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>master/validateUser/1/",
				data:"txtintuserid="+user_id,
				success: function(msg) {
					if(msg!="false")
					{
						document.getElementById("txtldownername").value=msg.trim(); 
					}
					else
					{
						document.getElementById("txtldownername").value=msg.trim();                   
						alert('No User in this Id');
					}
				}
			}
			)
			
		}
		else
		{
			document.getElementById("txtldownername").value="SuperAdmin";                    
		}	
	}
</script>
<script>
	function exit()
	{		
		if(confirm('Are you sure to Exit from Lead?')==true)
		{
			$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
			$("#stylized").load("<?php echo base_url().'crm/show_lead/'.$id?>");
		}
	}
</script>
<script>
	function create_lead(id)
	{
		if(check("form_sample_1"))
		{
			bootbox.confirm('Are you sure to Submit form?', function(result){
				if(result==true)
				{
					$("#function").html("<div>Your Request has been processed.<img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></div>");
					var data1={
						lead_for:$('#ddenqtype').val(),
						txtownername:$('#txtldownerid').val(),
						txtcname:$('#txtcname').val(),
						txtleadprefix:$('#ddprefix').val(),
						txtleadname:$('#txtleadname').val(),
						txtltitle:$('#txtltitle').val(),
						txtmobile:$('#txtmobile').val(),
						txtmail:$('#txtmail').val(),
						ddsource:$('#ddsource').val(),
						ddldstatus:$('#ddldstatus').val(),
						ddindustry:$('#ddindustry').val(),
						txtnoemp:$('#txtnoemp').val(),
						ddcountry:$('#ddcountry').val(),
						ddstate:$('#ddstate').val(),
						ddcity:$('#ddcity').val(),
						txtstreet:$('#txtstreet').val(),
						txtzipcode:$('#txtzipcode').val(),
						txtlead_des:$('#txtlead_des').val()
					};
					$.ajax(
					{
						type: "POST",
						url:"<?php echo base_url();?>crm/insert_lead/<?php echo $id?>",
						data:data1,
						success: function(msg) {
							if(msg!="0")
							{
								$(".page-content").html("<center><h2>"+msg+"</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(5000,function()
								{
									if(id==0)
									{
										location.reload();  //$("#stylized").load("<?php echo base_url().'crm/show_lead'?>");              //changed to location.reload();
									}
									else
									{
										$("#stylized").load("<?php echo base_url().'crm/add_lead/'.$id?>");
									}					
								}
								);
								
							}
							else
							{
								alert(msg);	
							}
							
						}
					});
				}
			}); 
		}
		else
		{
			return false;
		}
	}
</script>

<script>
	function get_state()
	{
		var country_id=$("#ddcountry").val();
		$("#ddstate").empty();
		if(country_id!='-1' && country_id!='')
		{
	        $.blockUI({ message: '<h3>Please Wait...</h3>' });
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>crm/select_state/"+country_id+"/0",
				dataType: "JSON",
				data:{'country_id':country_id},
				success:function(data){
				    setTimeout($.unblockUI, 01);
					$("#ddstate").append("<option value=-1> ~~ Select ~~</option>");
					$.each(data,function(i,item)
					{
						$("#ddstate").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
					});
				}
			});
		}
		else
		{
			$("#ddstate").append("<option value=-1> ~~ Select ~~</option>");
			alert("Please First Select The Country");
		}
		
	}
</script>
<script>
	function get_city()
	{
		var state_id=$("#ddstate").val();
		$("#ddcity").empty();
		if(state_id!='-1' && state_id!='')
		{
	        $.blockUI({ message: '<h3>Please Wait...</h3>' });
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>crm/select_city/"+state_id+"/0",
				dataType: "JSON",
				data:{'state_id':state_id},
				success:function(data){
				    setTimeout($.unblockUI, 01);
					$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
					$.each(data,function(i,item)
					{
						$("#ddcity").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
					});
				}
			});
		}
		else
		{
			$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
			alert("Please First Select The State");
		}
		
	}
	
</script>