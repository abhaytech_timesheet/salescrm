<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
						CRM
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						
						<li>
							<i class="fa fa-home"></i>
							<a href="#">
								Dashboard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Campaign
							</a>
							
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<!--row-->
				<div class="col-md-6">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Search Contact
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form role="form" class="form-horizontal" id="comp">
								<div class="form-body">
                                	
									<div class="form-group">
										<label class="col-md-3 control-label">Source</label>
										<div class="col-md-9">
											<select id="ddsource" name="ddsource" class="form-control input-large" onchange="enable()">
												<option value="-1" selected="selected">Select</option>
												<option value="1">Lead</option>
												<option value="2">Contact</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Category </label>
										<div class="col-md-9">
											<select id="ddcategory" name="ddcategory" class="form-control input-large" disabled="disabled">
												<option value="-1" selected="selected">Select</option>
												<option value="1">Account</option>
												<option value="2">Opportunity</option>
											</select>
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-md-3 control-label">Contacts </label>
										<div class="col-md-9">
                                        	<div id="contact">
												<textarea class="form-control input-large" name="txtcontact" id="txtcontact" rows="5"></textarea>
											</div>
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-md-3 control-label">Type of SMS</label>
										<div class="col-md-9">
											<div class="radio-list">
												<label class="radio-inline">
												<input type="radio" name="chksms" id="chksms1" value="1" checked="checked"> Promotional </label>
												<label class="radio-inline">
												<input type="radio" name="chksms" id="chksms2" value="2"> Transactional </label>
											</div>
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-md-3 control-label">SMS templates </label>
										<div class="col-md-9">
                                        	<select id="ddtemplates" name="ddtemplates" class="form-control input-large" onchange="get_msg()">
												<option value="-1" selected="selected">Select</option>
												<?php
													foreach($content->result() as $row)
													{
													?>
													<option value="<?php echo $row->m_email_temp_id; ?>"><?php echo $row->m_email_temp_title ?></option>
													<?php
													}
												?>                                          
											</select>
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-md-3 control-label">Template </label>
										<div class="col-md-9">
											<div id="smstmp">
												<textarea class="form-control input-large" id="txttemplate" name="txttemplate" rows="5"></textarea>
											</div>
										</div>
									</div>
                                    
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
                                    	<button type="button" class="btn yellow" onclick="get_details()">Get Details</button>
										<button type="button" class="btn green" onclick="send()">Send</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
				</div>
				<!-- END PAGE CONTENT-->
				<div class="col-md-6">
                	<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>View SMS Template
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
								<thead>
									<tr>
										<th width="33">
											#
										</th>
										<th width="126">
											SMS Title
										</th>
										<th width="126">
											SMS Description
										</th>
										<th width="49">
											Action
										</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$serial = 1;
										foreach($content->result() as $row)
										{
										?>
										<input type="text" name="txtdep" id="txtdep<?php echo $row->m_email_temp_id; ?>" value="<?php echo $row->m_email_temp_description ?>" style="display:none;" />
										<tr class="odd gradeX">
											
											<td><?php echo $serial; ?></td>
											<td><?php echo $row->m_email_temp_title ?></td>
											<td><?php echo substr($row->m_email_temp_description,0,20) ?></td>
											<td>
												<a href="javascript:void(0)" title="Edit Your Event Details" onclick="send_action('<?php echo $row->m_email_temp_id; ?>')" class="btn btn-xs purple"><i class="fa fa-check"></i>Get Message</a>
											</td>
										</tr>
										<?php
											$serial++;
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->
</div>
<script>
	function enable()
	{
		
		var sour=$('#ddsource').val();
		var cate=$('#ddcategory').val();
		if(sour==2)
		{
			$('#ddcategory').removeAttr("disabled");	
		}
		else
		{
			$('#ddcategory').attr( "disabled", "disabled" );
		}
		
	}
</script>

<script>
	function get_details()
	{
		var sour=$('#ddsource').val();
		var cate=$('#ddcategory').val();
		$("#contact").html('<div><img src ="<?php echo base_url();?>application/libraries/assets/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
		$("#contact").load("<?php echo base_url() ?>index.php/crm/get_conatct_no/"+sour+"/"+cate);
		
	}
</script>

<script>
	function send()
	{
		var contact = $('#txtcontact').val();
		var tmplate = $('#txttemplate').val();
		var checked_site_radio = $('input:radio[name=chksms]:checked').val();
		$.ajax(
		{
			type: "POST",
			url:"<?php echo base_url();?>index.php/crm/make_compaign/",
			data:"txtcontact="+contact+"&txttemplate="+tmplate,
			success: function(msg) {
				if(msg=="true")
				{
					$(".page-content").html("<center><h2>Message send Successfully!</h2></center>")
					.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/img/loading-spinner-blue.gif' /></center>")
					.hide()
					.fadeIn(1000,function()
					{
						$("#stylized").load("<?php echo base_url().'index.php/crm/create_Campaign'?>");
					}
					);
					
				}	  
			}
		});
	}
</script>

<script>
	function get_msg()
	{
		var ddsmstemplate=$('#ddtemplates').val();
		
		$("#smstmp").html('<div><img src ="<?php echo base_url();?>application/libraries/assets/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
		$("#smstmp").load("<?php echo base_url() ?>index.php/crm/get_msg/"+ddsmstemplate+"/2");
	}
</script>						