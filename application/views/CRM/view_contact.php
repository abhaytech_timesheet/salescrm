<script>
	function get_state()
	{
		var country_id=$("#ddcountry").val();
		$("#ddstate").empty();
		if(country_id!='-1' && country_id!='')
		{
	        $.blockUI({ message: '<h3>Please Wait...</h3>' });
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/crm/select_state/"+country_id+"/0",
				dataType: "JSON",
				data:{'country_id':country_id},
				success:function(data){
				    setTimeout($.unblockUI, 01);
					$("#ddstate").append("<option value=-1> ~~ Select ~~</option>");
					$.each(data,function(i,item)
					{
						$("#ddstate").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
					});
				}
			});
		}
		else
		{
			$("#ddstate").append("<option value=-1> ~~ Select ~~</option>");
			alert("Please First Select The Country");
		}
	}
</script>
<script>
	function get_city()
	{
		var state_id=$("#ddstate").val();
		$("#ddcity").empty();
		if(state_id!='-1' && state_id!='')
		{
	        $.blockUI({ message: '<h3>Please Wait...</h3>' });
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/crm/select_city/"+state_id+"/0",
				dataType: "JSON",
				data:{'state_id':state_id},
				success:function(data){
				    setTimeout($.unblockUI, 01);
					$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
					$.each(data,function(i,item)
					{
						$("#ddcity").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
					});
				}
			});
		}
		else
		{
			$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
			alert("Please First Select The State");
		}
	}
</script>

<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_contact/0">
							CRM
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-call-in"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_contact/0">Contact</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-plus"></i>
						<span>Create Contact</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Create Contact </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Create New Contact </span>
							</div>
						</div>
						<div class="portlet-body form">
							<form  class="horizontal-form"  id="form_sample_1">
								<div class="form-body">
									<h4 class="caption-subject font-blue bold uppercase">Conatct Information </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Account Name</label>
												<select class="form-control input-sm opt" name="ddacc_name" id="ddacc_name">
													<option value="">Select Account Name</option>
													<?php
														foreach($acname->result() as $row)
														{
														?>
														<option value="<?php echo $row->account_id ?>"><?php echo $row->account_name; ?></option>
														<?php
														}
													?>
												</select>
												<span id="divddacc_name" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Job Position</label>
												<input class="form-control input-sm empty" placeholder="Enter Job Position" name="txtjobpostion" id="txtjobpostion" size="16" type="text"  />
												<span id="divtxtjobpostion" style="color:red"></span>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group" >
												<label class="control-label">Name</label>
											</div>
											<div class="form-group">
												<div class="col-md-3" style="margin:0px; padding:0px">
													<select id="ddprefix" name="ddprefix" class="form-control input-sm opt">
														<option value="-1">Select</option>
														<option value="1">Mr</option>
														<option value="2">Ms</option>
														<option value="3">Mrs</option>
														<option value="4">Dr</option>
														<option value="5">Prof</option>
													</select>
												</div>
												<div class="col-md-9" style="margin:0px; padding:0px">
													<input type="text" class="form-control input-sm empty" name="txtcontactname" placeholder="Enter Name" id="txtcontactname">
												</div>
												<span id="divddprefix" style="color:red"></span>
												<span id="divtxtcontactname" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Mobile Number</label>
											</div>
											<div class="form-group">
												<input type="text" class="form-control input-sm empty" placeholder="Enter Lead Mobile Number" name="txtmobile" id="txtmobile" maxlength="10">
												<span id="divtxtmobile" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Email ID</label>
											</div>
											<div class="form-group">
												<input type="text" class="form-control input-sm empty" name="txtmail" placeholder="Enter Email address" id="txtmail">
												<span id="divtxtmail" style="color:red"></span>
											</div>
										</div>
									</div>
									<h4 class="caption-subject font-blue bold uppercase">Address Information </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Country</label>
												<select class="form-control input-sm opt" name="ddcountry" id="ddcountry" onchange="get_state()">
													<option value="">Select Country</option>
													<?php
														foreach($loc->result() as $row)
														{
														?>
														<option value="<?php echo $row->m_loc_id?>"><?php echo $row->m_loc_name; ?></option>
														<?php
														}
													?>		
												</select>
												<span id="divddcountry" style="color:red"></span>
												
											</div>
										</div>
										<!--/span-->
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">State</label>
												<div id="state">
													<select class="form-control input-sm opt" name="ddstate" id="ddstate" onchange="get_city()">
														<option value="">Select State</option>
														
													</select>
													<span id="divddstate" style="color:red"></span>
												</div>
											</div>
										</div>
										
										<!--row-->
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">City</label>
												<div id="city">
													<select class="form-control input-sm opt" name="ddcity" id="ddcity">
														<option value="">Select City</option>
													</select>	
													<span id="divddcity" style="color:red"></span>
												</div>
											</div>
										</div>
									</div>
									<!--/span-->
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Street
												</label>
												<input type="text" class="form-control input-sm " PLACEHOLDER="Enter Street/Locality" name="txtstreet" id="txtstreet">	
												<span id="divtxtstreet" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Zip Code
												</label>
												<input type="text" class="form-control input-sm " placeholder="Enter Zipcode" name="txtzipcode" id="txtzipcode" maxlength="6">	
												<span id="divtxtzipcode" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
									</div>
								</div>
								<div class="form-actions middle">
									<button type="button" onclick="create_contact(0)" class="btn green"><i class="fa fa-check"></i> Save</button>
									<button type="button" onclick="create_contact(1)" class="btn green" >Save & Next</button>
									<button type="reset" class="btn default">Cancel</button>
									
								</div>
							</form>
							<!-- END FORM--> 
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>

<script>
	function create_contact(id)
	{
		if(check("form_sample_1"))
		{
			bootbox.confirm('Are you sure to Submit form?', function(result){
				if(result==true)
				{
					var accountname=$('#ddacc_name').val();
					var designation=$('#txtjobpostion').val();
					var contactprefix=$('#ddprefix').val();
					var txtcontactname=$('#txtcontactname').val();
					var mobilenumber=$('#txtmobile').val();
					var emailid=$('#txtmail').val();
					var country=$('#ddcountry').val();
					var state=$('#ddstate').val();
					var city = $('#ddcity').val();
					var street = $('#txtstreet').val();
					var txtzipcode = $('#txtzipcode').val();
					//alert("accountname="+accountname+"&designation="+designation+"&txtcontactprefix="+contactprefix+"&txtcontactname="+txtcontactname+"&txtmobile="+mobilenumber+"&txtmail="+emailid+"&ddcountry="+country+"&ddstate="+state+"&ddcity="+city+"&txtstreet="+street+"&txtzipcode="+txtzipcode);
					$.ajax(
					{
						type: "POST",
						url:"<?php echo base_url();?>index.php/crm/insert_contact",
						data:"accountname="+accountname+"&designation="+designation+"&txtcontactprefix="+contactprefix+"&txtcontactname="+txtcontactname+"&txtmobile="+mobilenumber+"&txtmail="+emailid+"&ddcountry="+country+"&ddstate="+state+"&ddcity="+city+"&txtstreet="+street+"&txtzipcode="+txtzipcode,
						success: function(msg) {
							if(msg!="")
							{
								$(".page-content").html("<center><h2>Contact update Successfully!</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(1000,function()
								{
									//alert(id);
									//alert("<?php echo base_url().'index.php/crm/show_contact'?>");
									if(id=="0")
									location.reload();  //$("#stylized").load("<?php echo base_url().'index.php/crm/show_contact'?>");                //changed to location.reload();
									if(id=="1")
									$("#stylized").load("<?php echo base_url().'index.php/crm/add_contact/'?>");
									
								}
								);
							}
							else
							{
								alert("Some Error on this page");
								
							}
							
						}
					});
					
				}
			}); 
		}
		else
		{
			return false;
		}
	}
</script>
<script>
	function exit()
	{
		if(confirm('Are you sure to Exit from Create Contact?')==true)
		{
			$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
			$("#stylized").load("<?php echo base_url().'index.php/crm/show_contact'?>");
		}
	}
</script>																					