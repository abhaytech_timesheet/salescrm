<style type="text/css">
    #autoSuggestionsList > li {
	background: none repeat scroll 0 0 #F3F3F3;
	border-bottom: 0px solid #E3E3E3;
	list-style: none outside none;
	padding: 3px 15px 3px 15px;
	text-align: left;
    }
    #autoSuggestionsList > li a { color: #800000; }
    .auto_list {
	border: 0px solid #E3E3E3;
	border-radius: 5px 5px 5px 5px;
	position: absolute;
    }
</style>
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_event/0">
							CRM
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-calendar"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_event/0">Task</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-plus"></i>
						<span>Create Event</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Create Event </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-calendar font-black"></i>
								<span class="caption-subject font-black bold uppercase">Create New Event </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form  class="horizontal-form"  id="form_sample_1">
								<div class="form-body">
									<h4 class="caption-subject font-blue bold uppercase">Event Information </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Subject</label>
												<input class="form-control input-sm empty" value="<?=$lead_name.' - '.$lead_description?>"  name="txtsub" id="txtsub" size="16" type="text" value=""/>
												<span id="divtxtsub" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Type</label>
												<select class="form-control input-sm opt" name="ddeventtype" id="ddeventtype">
													<option value="-1">None</option>
													<option value="1">Call</option>
													<option value="2">Email</option>
													<option value="3">Meeting</option>
													<option value="4" selected>Other</option>
												</select>
												<span id="divddeventtype" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Assigned To</label>
												<input type="text" class="form-control input-sm empty" name="txtassign"  id="txtassign" value="<?php echo $this->session->userdata('name')?>" disabled>
												<input type="hidden" class="form-control" name="txtassign_id"  id="txtassign_id" value="<?php echo $this->session->userdata('profile_id')?>"/>
												<span id="divtxtassign" style="color:red"></span>
											</div>
										</div>
										
									</div>
									<!--/row-->
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Start Date</label>
												<div class="form-group input-group date form_datetime input-sm">
													<input type="text" size="16" name="txtstart_date" id="txtstart_date" readonly="" class="form-control input-sm empty">
													<span class="input-group-btn">
														<button class="btn btn-sm date-set" type="button"><i class="fa fa-calendar"></i></button>
													</span>
												</div>
												<span id="divtxtstart_date" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">End Date</label>
												<div class="form-group input-group date form_datetime input-sm">
													<input type="text" size="16" name="txtend_date" id="txtend_date" readonly="" class="form-control input-sm empty">
													<span class="input-group-btn">
														<button class="btn btn-sm date-set" type="button"><i class="fa fa-calendar"></i></button>
													</span>
												</div>
												<span id="divtxtend_date" style="color:red"></span>
											</div>
										</div>
									</div>
									<!--/span-->
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Location</label>
												<input type="text" class="form-control input-sm empty" value="<?=$lead_address?>" name="txtlocation" id="txtlocation" >
												<span id="divtxtlocation" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Status</label>
												<select class="form-control input-sm opt" name="ddstatus" id="ddstatus">
													<option value="1">Open</option>
													<option value="0">Completed</option>
												</select>
												<span id="divddstatus" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--row-->
									<h4 class="caption-subject font-blue bold uppercase">Related To</h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Name</label>
											</div>
											<div class="form-group">
												<div class="col-md-4" style="margin:0; padding:0">
													<select class="form-control input-sm empty" name="ddname" id="ddname" onchange="load_detail()">
														<option value="2">Contact</option>
														<option value="1" selected>Lead</option>
													</select>	
												</div>
												
												<div class="col-md-8" style="margin:0; padding:0" >
													<select class="form-control input-sm" name="txtrelname" id="txtrelname" ><!--onchange="related()"-->
														<option value="<?=$lead_id?>" selected><?=$lead_name?></option>
													</select>
												</div>
												
												<span id="divddname" style="color:red"></span>
												<span id="divtxtrelname" style="color:red"></span>
											</div>
											<div id="suggestions" class="form-group">
												<div id="autoSuggestionsList" style="margin-left:130px" >
													
												</div>
											</div> 
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Related To</label>
											</div>
											<div class="form-group">
												<div class="col-md-4" style="margin:0; padding:0">
													<select class="form-control input-sm opt" name="ddrelate" id="ddrelate" onchange="get_project()">
														<option value="1">Solution</option>
														<option value="2" selected>Product</option>
													</select>
												</div>
												
												<div class="col-md-8" style="margin:0; padding:0">
													<select class="form-control input-sm opt" name="txtname" id="txtname" >
														<option value="">Select Type</option>
													</select>
												</div>
												<span id="divddrelate" style="color:red"></span>
												<span id="divtxtname" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
										
									</div>
									<h4 class="caption-subject font-blue bold uppercase">Reccurence </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Create Recurring Series of Event
												</label>
												<input type="checkbox" name="checkrec"  id="checkrec">                                        
											</div>
										</div>
									</div>
									<!--/span-->
									<h4 class="caption-subject font-blue bold uppercase">Reminder </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Reminder</label> 
												<select class="form-control input-sm opt" name="ddreminder" id="ddreminder">
													<option value="0">0 minutes</option>
													<option value="5">5 minutes</option>
													<option value="10">10 minutes</option>
													<option value="15" selected="selected">15 minutes</option>
													<option value="30">30 minutes</option>
													<option value="60">1 hour</option>
													<option value="120">2 hours</option>
													<option value="180">3 hours</option>
													<option value="240">4 hours</option>
													<option value="300">5 hours</option>
													<option value="360">6 hours</option>
													<option value="420">7 hours</option>
													<option value="480">8 hours</option>
													<option value="540">9 hours</option>
													<option value="600">10 hours</option>
													<option value="660">11 hours</option>
													<option value="720">0.5 days</option>
													<option value="1080">18 hours</option>
													<option value="1440">1 day</option>
													<option value="2880">2 days</option>
													<option value="4320">3 days</option>
													<option value="5760">4 days</option>
													<option value="10080">1 week</option>
													<option value="20160">2 weeks</option>
												</select>  
												<span id="divddreminder" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Attachment</label>
												<div class="fileinput fileinput-new input-sm" data-provides="fileinput">
													<div class="input-group">
														<div class="form-control uneditable-input input-sm " data-trigger="fileinput">
															<i class="fa fa-file fileinput-exists"></i>&nbsp;
															<span class="fileinput-filename">
															</span>
														</div>
														<span class="input-group-addon btn-sm default btn-file">
															<span class="fileinput-new"> Select file </span>
															<span class="fileinput-exists"> Change </span>
															<input type="file" class="emptyfile input-sm" name="fileatt" id="fileatt" >
														</span>
														<a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
													</div>
												</div>		
												<span id="divfileatt" style="color:red"></span>
											</div>
										</div>
									</div>
									<!--/span-->
									
									<!--row-->
									<h4 class="caption-subject font-blue bold uppercase">Description Information </h4>
									<div class="row">
										<div class="col-md-8">
											<div class="form-group">
												<label class="control-label">Event Description</label>
												<textarea class="form-control input-sm empty" placeholder="Enter Event Description Information" name="txtcomment" id="txtcomment" style="font-size:20px" col="3"><?php echo $lead_company. ' '. $lead_name. ' '.$lead_description ?>
												</textarea>
												<span id="divtxtcomment" style="color:red"></span>
											</div>
										</div>
									</div>
								</div>
								
							</form>
							<div class="form-actions">
								<button type="button" onclick="create_event(0)" class="btn green"><i class="fa fa-check"></i> Save</button>
								<button type="button" onclick="create_event(1)" class="btn green"> Save & New</button>
								<button type="reset" onclick="exit()" class="btn default">Cancel</button>
								
							</div>
							<!-- END FORM-->
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>

<!-- <script>
	load_detail();
	
	function load_detail()
	{
		$("#txtrelname").empty();
		var txtrel=$("#ddname").val();
		$.post("<?php echo base_url();?>crm/get_detail_for_task/",{'for':txtrel} ,function(data) {
			$.each(data,function(i,item)
			{
				$('#txtrelname').append("<option value="+item.account_id+">"+item.account_name+"</option>");
			});
			
		}, "json");
	}
</script> -->

<script>
	get_project();
	function get_project()
	{
		var ddrelate=$("#ddrelate").val();
		$("#txtname").empty();
		$.post("<?php echo base_url();?>crm/project_category1/",{'ddrelate':ddrelate} ,function(data) {
			$.each(data,function(i,item)
			{
				$('#txtname').append("<option value="+item.m_project_id+">"+item.m_project_name+"</option>");
			});
			
		}, "json");
	}
</script>

<script>
	function exit()
	{
		if(confirm('Are you sure to Exit from Create Event?')==true)
		{
			$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
			$("#stylized").load("<?php echo base_url().'index.php/crm/after_view_event'?>");
		}
	}
</script>
<script>
	/*fill_userid();*/
	function fill_userid()
	{
		var user_id=<?php echo $this->session->userdata('user_id')?>;
		if(user_id!="9999999999")
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/master/validateUser/",
				data:"txtintuserid="+user_id,
				success: function(msg) {
					if(msg!="false")
					{
						document.getElementById("txtassign").value=msg;  
					}
					else
					{
						document.getElementById("txtassign").value=msg;                   
						alert('No User in this Id');
					}
				}
			}
			)
			
		}
		else
		{
			document.getElementById("txtassign").value="SuperAdmin";                    
		}
		
	}
</script>
<script>
	function fill_userid11()
	{
		var ddname=$('#ddname').val();
		var user_id=$('#txtrel1').val();
		$('#txtrel1').val($('#txtrel1').val());
		if(user_id!="0")
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/crm/get_detail/"+ddname+"/",
				data:"id="+user_id,
				success: function(msg) {
					if(msg!="false")
					{
						document.getElementById("txtrelname").value=msg; 
						document.getElementById("txtrel").value=user_id; 
					}
					else
					{
						document.getElementById("txtrelname").value=msg; 
						document.getElementById("txtrel").value="";	
						alert('No User in this Id');
					}
				}
			}
			)
			
		}
		else
		{
			document.getElementById("txtrelname").value="No Detail";                    
		}	
	}
</script>

<script>
	function change_state()
	{
		var val1=$('#ddname').val();
		if(val1=='2')
		{
			$('#ddrelate').attr('disabled','disabled');
			$('#txtname').attr('disabled','disabled');
		}
		else
		{
			$('#ddrelate').removeAttr('disabled');
			$('#txtname').removeAttr('disabled');
		}
	}
</script>
<script>
	function create_event(id)
	{
		if(check("form_sample_1"))
		{
			bootbox.confirm('Are you sure to Submit form?', function(result){
				if(result==true)
				{
					var subject=$('#txtsub').val();
					var assign=$('#txtassign_id').val();
					var start_date=$('#txtstart_date').val();
					var end_date=$('#txtend_date').val();
					var status=$('#ddstatus').val();
					var ddeventtype=$('#ddeventtype').val();
					var location=$('#txtlocation').val();
					var type=$('#ddtype').val();
					var rttype=$('#ddname').val();
					var rtname=$('#txtrel').val();
					var reltype=$('#ddreltype').val();
					var relname=$('#txtrelname').val();
					var recurance=$('#checkrec').val();
					var reminder=$('#ddreminder').val();
					var file1=$('#fileatt').val();
					var fileatt = file1.replace(/^.*\\/, "");
					var comment=$('#txtcomment').val();
					$.ajax(
					{
						type: "POST",
						url:"<?php echo base_url();?>index.php/crm/insert_event/",
						data:"txtsub="+subject+"&txtassign="+assign+"&ddeventtype="+ddeventtype+"&txtstart_date="+start_date+"&txtend_date="+end_date+"&ddstatus="+status+"&txtlocation="+location+"&ddtype="+type+"&ddrttype="+rttype+"&txtrtname="+rtname+"&ddreltype="+reltype+"&txtrelname="+relname+"&checkrec="+recurance+"&ddreminder="+reminder+"&fileatt="+fileatt+"&txtcomment="+comment,
						success: function(msg) {
							if(msg!="")
							{
								$(".page-content").html("<center><h2>Event created Successfully!</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(1000,function()
								{
									if(<?php echo $this->session->userdata('user_type')?> ==2){
										window.location.href = "<?= base_url()?>report/dashboard/";
									}
									else{
										window.location.href = "<?= base_url()?>report/acc_dashboard/";
									}					
								}
								);
								
							}
							else
							{
								alert("Some Error on this page");
								
							}
							
						}
					});
				}
			}); 
		}
		else
		{
			return false;
		}
	}
</script>				