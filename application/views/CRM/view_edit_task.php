<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_task/0">
							CRM
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="icon-speedometer"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_task/0">
							Task
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-edit"></i>
						<span>
							Edit Task
						</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Edit Task </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-black"></i>
								<span class="caption-subject font-black bold uppercase">Edit Task </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<?php                                
								foreach($info->result() as $row)
								{
									break;
								}
							?>
							<!-- BEGIN FORM-->
							<form action="#" class="horizontal-form"  id="form_sample_1">
								<div class="form-body">
									<h4 class="caption-subject font-blue bold uppercase"> Task Information </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Subject</label>
												<input class="form-control input-sm empty"  name="txtsub" id="txtsub" size="16" type="text" value="<?php echo $row->subject;?>"/>
												
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Task Type</label>
												
												<select class="form-control input-sm opt" name="txttasktype" id="txttasktype">
													<option value="0">Select Task</option>
													<option <?php echo ($row->task_type==1 ? 'selected' : '' ); ?> value="1">Call</option>
													<option <?php echo ($row->task_type==2 ? 'selected' : '' ); ?> value="2">Send a Mail</option>
												</select>
											</div>
											
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Assigned To</label>
												<input type="text" class="form-control input-sm" name="txtassign"  id="txtassign" value="<?php echo $row->task_assignto_name;?>" disabled>
												<input type="hidden" class="form-control" name="txtassign_id"  id="txtassign_id" value="<?php echo $row->task_assignto_id;?>"/>
											</div>
										</div>
										<!--/span-->
										
										<!--/span-->
									</div>
									<!--/row-->
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Status</label>
												<select class="form-control input-sm opt" name="ddstatus" id="ddstatus">
													<option <?php echo ($row->task_status==1 ? 'selected' : '' ); ?> value="1">Open </option>
													<option <?php echo ($row->task_status==2 ? 'selected' : '' ); ?> value="2">Completed</option>
												</select>										
											</div>
										</div>
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Priority</label>
												<select class="form-control input-sm opt" name="ddpriority" id="ddpriority">
													<option value="0">Select Priority</option>
													<option <?php echo ($row->Priority==1 ? 'selected' : '' ); ?>  value="1">High</option>
													<option <?php echo ($row->Priority==2 ? 'selected' : '' ); ?> value="2">Medium</option>
													<option <?php echo ($row->Priority==3 ? 'selected' : '' ); ?> value="3">Low</option>
													
												</select>
											</div>
										</div>
										
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<?php 
													$date=date_create($row->task_reminder);
													$d=date_format($date,'Y-m-d'); 
												?>
												<label class="control-label">Due Date</label>
												<input type="text" class="form-control input-sm date-picker" name="txtduedate" id="txtduedate" data-date-format="yyyy-mm-dd" value="<?php echo $d?>">
											</div>
										</div>
										<!--/span-->
										
										
									</div>
									
									<!--row-->
									<h4 class="caption-subject font-blue bold uppercase"> Related To </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Name </label>
											</div>
											<div class="form-group">
												<div class="col-md-4" style="margin:0; padding:0">
													<select class="form-control input-sm opt" name="txtrel" id="txtrel" onchange="load_detail()">
														<option <?php echo ($row->Task_related==2 ? 'selected' : '' ); ?> value="2" >Contact</option>
														<option <?php echo ($row->Task_related==1 ? 'selected' : '' ); ?> value="1">Lead</option>
														
													</select>	
												</div>
												<div class="col-md-8" style="margin:0; padding:0" >
													<select class="form-control input-sm opt" name="ddname" id="ddname" ><!--onchange="related()"-->
														<option value="-1">Select Name</option>
													</select>
												</div>
												
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Related To</label>
											</div>
											<div class="form-group">
												<div class="col-md-4" style="margin:0; padding:0">
													<select class="form-control input-sm opt" name="ddrelate" id="ddrelate" onchange="get_project()">
														
														<option <?php echo ($row->task_relto_service==1 ? 'selected' : '' ); ?> value="1">Website</option>
														<option <?php echo ($row->task_relto_service==2 ? 'selected' : '' ); ?> value="2">Product</option>
														
													</select>
												</div>
												<div class="col-md-8" style="margin:0; padding:0">
													<select class="form-control input-sm opt" name="ddwebsite" id="ddwebsite" >
														<option value="">Select Type</option>
													</select>
												</div>
												<span id="divddrelate" style="color:red"></span>
												<span id="divddwebsite" style="color:red"></span>
												
											</div>
										</div>
										<!--/span-->
										
									</div>
									<!--row-->
									<!--<h4 class="caption-subject font-blue bold uppercase">Reccurence</h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Create Recurring Series of Task
												</label>
												<input type="checkbox" name="checkrec" value='1' id="checkrec" onclick="$(this).is(':checked') && $('#checked-a').slideDown('slow') || $('#checked-a').slideUp('slow');">                                    
											</div>
										</div>
									</div>
									
									<div class="dis-sub" style="display:none;" id="checked-a">
										<script>
											function get_radio(id)
											{
												$('#radio_rec').val(id);
											}
										</script>				
										
										<div class="row">
											<div class="col-md-4">
												<div class="radio-list">
													<label >
														<input type="radio" name="radio" id="rec1" onclick="get_radio(1)"/>
													Daily </label>
													<label >
														<input type="radio" name="radio" id="rec2" onclick="get_radio(2)"/>
													Weekly </label>
													<label >
														<input type="radio" name="radio" id="rec3" onclick="get_radio(3)"/>
													After 30 Days </label>
													<label >
														<input type="radio" name="radio" id="rec4" onclick="get_radio(4)"/>
													Yearly </label>
												</div>
												<input type="hidden" name="radio_rec" id="radio_rec" value="" />
											</div>
											
											<!--/span
											
											<div class="col-md-4">
												<div class="input-group input-xlarge date-picker input-daterange" data-date-format="dd-mm-yyyy" data-date-start-date="+1d">
													<input type="text" class="form-control input-sm" name="txtfromdate" id="txtfromdate">
													<span class="input-group-addon">
														to
													</span>
													<input type="text" class="form-control input-sm" name="txttodate" id="txttodate">
												</div>
											</div>
										</div>
									</div>	-->
									
									<!--/span-->
									<h4 class="caption-subject font-blue bold uppercase">Reminder</h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Reminder</label> 
												<div class="form-group input-group date form_datetime input-medium">
													<input type="text" size="16" name="txtreminder" id="txtreminder" value="<?php echo ($row->task_reminder); ?>" readonly class="form-control input-sm empty">
													<span class="input-group-btn">
														<button class="btn btn-sm date-set" type="button"><i class="fa fa-calendar"></i></button>
													</span>
												</div>  
											</div>
										</div>
										<div id="recurance" style="display:none;">
											<select class="form-control input-sm" name="ddrecurance_before" id="ddrecurance_before">
												<option value="-1">On Recursion Date</option>
												<option value="1">Before 1 day</option>
												<option value="2">Before 2 day</option>
												<option value="3">Before 3 day</option>
											</select>
											<span id="divddrecurance_before" style="color:red"></span>
										</div>
										<!--row-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Attachment</label>
												<div class="fileinput fileinput-new input-sm" data-provides="fileinput">
													<div class="input-group">
														<div class="form-control uneditable-input input-sm " data-trigger="fileinput">
															<i class="fa fa-file fileinput-exists"></i>&nbsp;
															<span class="fileinput-filename">
															</span>
														</div>
														<span class="input-group-addon btn-sm default btn-file">
															<span class="fileinput-new"> Select file </span>
															<span class="fileinput-exists"> Change </span>
															<input type="file" class="emptyfile input-sm" name="userfile" id="userfile" >
														</span>
														<a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
													</div>
												</div>
												
											</div>
										</div>
									</div>
									<!--/span-->
									<input type="hidden" id="fileatt" name="fileatt" />
									<!--row-->
									<h4 class="caption-subject font-blue bold uppercase">Description Information</h4>
									<div class="row">
										<div class="col-md-8">
											<div class="form-group">
												<label class="control-label">Task Description</label>
												<textarea class="form-control input-sm empty" placeholder="Enter Task Description Information" name="txtcomment" id="txtcomment" style="font-size:20px" col="3"><?php echo $row->task_comment;?></textarea>										
											</div>
										</div>
									</div>
								</div>
							</form>
							
							<div class="form-actions left" id="function">
								<?php 
									$date1=date_create($curr);
									$today=date_format($date1,'Y-m-d');
									$date2=date_create($d);
									$diff=date_diff($date1,$date2);
									$t=$diff->format("%R%a");
									
								?>  <input type="hidden" name="hddate" id="hddate" value="<?php echo $today; ?>" />
								<!--<input type="text" name="difdate" id="difdate" value="<?php echo $t; ?>" />-->
								<?php if($row->task_status!=2)
									{?>
									<button type="button" onclick="submitFile(0,<?php echo $t; ?>)" class="btn green"><i class="fa fa-check"></i> Save</button>
									<button type="button" onclick="submitFile(1,<?php echo $t; ?>)" class="btn green" >Save & Exit</button>
									<button type="reset" class="btn default">Cancel</button>
									<?php
									}?>
							</div>
							<!-- END FORM-->
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>

<script>
	load_detail();
	function load_detail()
	{
		$("#ddname").empty();
		var txtrel=$("#txtrel").val();
		$.post("<?php echo base_url();?>crm/get_detail_for_task/",{'for':txtrel} ,function(data) {
			$.each(data,function(i,item)
			{
				$('#ddname').append("<option value="+item.account_id+">"+item.account_name+"</option>");
			});
			
		}, "json");
	}
	
</script>
<script>
	get_project();
	function get_project()
	{
		var ddrelate=$("#ddrelate").val();
		$("#ddwebsite").empty();
		$.post("<?php echo base_url();?>crm/project_category1/",{'ddrelate':ddrelate} ,function(data) {
			$.each(data,function(i,item)
			{
if(item.m_project_id==<?php echo ($row->related_to_servicename); ?>)
				$('#ddwebsite').append("<option value="+item.m_project_id+" selected >"+item.m_project_name+"</option>");
else
                $('#ddwebsite').append("<option value="+item.m_project_id+" >"+item.m_project_name+"</option>");
			});
			
		}, "json");
	}
	
</script>

<script>
	function submitFile(id,id2)
	{
		var file=$("#userfile").val();
		if(file!="")
		{
			var formUrl = "<?php echo base_url(); ?>crm/insert_opportunity_file";
			var formData = new FormData($('.horizontal-form')[0]);
			$.ajax({
				url: formUrl,
				type: 'POST',
				data: formData,
				mimeType: "multipart/form-data",
				contentType: false,
				cache: false,
				processData: false,
				success: function(data){
					//now get here response returned by PHP in JSON fomat you can parse it using JSON.parse(data)
					$("#fileatt").val(data.trim());
					update_task(id,id2);
				},
				error: function(jqXHR, textStatus, errorThrown){
					//handle here error returned
				}
			});
		}
		else
		{
			update_task(id,id2);
		}
		
	}
</script>

<script>
	function update_task(id,id2)
	{
		
		$("#function").html("<div>Your Request has been processed.<img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></div>");
		var stat = $('#ddstatus').val();
		var datas=$("#form_sample_1").serialize();
		if(id2<=0  )
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url().'crm/update_task/'.$task_id.'/' ?>",
				data:datas,
				success: function(msg) {
					if(msg.trim()!="")
					{
						//$(".page-content").html(msg)
						$(".page-content").html("<center><h2>Task Updated Successfully!</h2></center>")
						.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
						.hide()
						.fadeIn(1000,function()
						{
							if(id==1)
							{
								location.reload();  //$("#stylized").load("<?php echo base_url().'index.php/crm/show_task'?>");        //changed to location.reload();
							}
							else
							{
								location.reload();  //$("#stylized").load("<?php echo base_url().'index.php/crm/edit_task/'.$task_id?>");            //changed to location.reload();
							}					
						}
						);
						
					}
				}
			});
		}
		else
		{
			//location.reload();
			alert('Status of future task cannot be change');
location.reload();
		}
		
	}
</script>
<script>
	function exit()
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url().'index.php/crm/show_task'?>");
	}
</script>