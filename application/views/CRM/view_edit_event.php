<?php
	foreach($rec->result() as $row)
	{
	}
?>
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_event/0">
							CRM
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-calendar"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_event/0">Event</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-edit"></i>
						<span>Edit Event</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Edit Event </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-calendar font-black"></i>
								<span class="caption-subject font-black bold uppercase">Edit Event </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="#" class="horizontal-form"  id="form_sample_1">
								<div class="form-body">
									<h4 class="caption-subject font-blue bold uppercase">Event Information </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Subject</label>
												<input class="form-control input-sm empty"  name="txtsub" id="txtsub" type="text" value="<?php echo $row->subject;?>" />
												<span id="divtxtsub" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label ">Type</label>
												<select class="form-control input-sm opt" name="ddtype" id="ddtype">
													<option <?php echo ($row->event_type==1 ? 'selected' : '');?> value="1">Call</option>
													<option <?php echo ($row->event_type==2 ? 'selected' : '');?> value="2">Email</option>
													<option <?php echo ($row->event_type==3 ? 'selected' : '');?> value="3">Meeting</option>
													<option <?php echo ($row->event_type==4 ? 'selected' : '');?> value="4">Other</option>
												</select>
												<span id="divddtype" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Assigned To</label>
												<input type="text" class="form-control input-sm empty" name="txtassign"  id="txtassign" value="<?php echo $this->session->userdata('name')?>"  disabled>
												<input type="hidden" class="form-control" name="txtassign_id"  id="txtassign_id" value="<?php echo $row->event_assignto_id?>"/>
												<span id="divtxtassign" style="color:red"></span>
											</div>
										</div>
										
									</div>
									<!--/row-->
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Start Date</label>
												<div class="form-group input-group date form_datetime input-sm">
													<input type="text" size="16" name="txtstart_date" id="txtstart_date" readonly="" class="form-control input-sm empty" value="<?php echo $row->event_start_date;?>" />
													<span class="input-group-btn">
														<button class="btn btn-sm date-set" type="button"><i class="fa fa-calendar"></i></button>
													</span>
												</div>
												<span id="divtxtstart_date" style="color:red"></span>
											</div>
										</div>
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">End Date</label>
												<div class="form-group input-group date form_datetime input-sm">
													<input type="text" size="16" name="txtend_date" id="txtend_date" readonly="" class="form-control input-sm empty" value="<?php echo $row->event_end_date;?>"/>
													<span class="input-group-btn">
														<button class="btn btn-sm date-set" type="button"><i class="fa fa-calendar"></i></button>
													</span>
												</div>
												<span id="divtxtend_date" style="color:red"></span>
											</div>
										</div>
									</div>
									<!--/span-->
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Location</label>
												<input type="text" class="form-control input-sm empty" name="txtlocation" id="txtlocation" value="<?php echo $row->event_location;?>"/>
												<span id="divtxtlocation" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Status </label>
												<select class="form-control input-sm opt" name="ddstatus" id="ddstatus">
													<option <?php echo ($row->event_status==1 ? 'selected' : '');?> value="1">Open</option>
													<option <?php echo ($row->event_status==2 ? 'selected' : '');?> value="2">Completed</option>
												</select>
												<span id="divddstatus" style="color:red"></span>
											</div>
										</div>
									</div>
									<!--row-->
									<h3 class="form-section">Related To</h3>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Name</label>
											</div>
											<div class="form-group">
												<div class="col-md-4" style="margin:0; padding:0">
													<select class="form-control input-sm opt" name="ddrttype" id="ddrttype" onchange="load_detail()">
														<option <?php echo ($row->event_related_to==2 ? 'selected' : '');?> value="2" selected>Contact</option>
														<option <?php echo ($row->event_related_to==1 ? 'selected' : '');?> value="1">Lead</option>
													</select>
												</div>
												<div class="col-md-8" style="margin:0; padding:0" >
													<select class="form-control input-sm opt" name="txtrelname" id="txtrelname" ><!--onchange="related()"-->
														<option value="-1">Select Name</option>
													</select>
												</div>
												<span id="divddrttype" style="color:red"></span>
												<span id="divtxtrelname" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Related To</label>
											</div>
											<div class="form-group">
												<div class="col-md-4" style="margin:0; padding:0">
													<select class="form-control input-sm opt" name="ddreltype" id="ddreltype" onchange="get_project()">
														<option <?php echo ($row->event_rel_service==1 ? 'selected' : '');?> value="1">Solution</option>
														<option <?php echo ($row->event_rel_service==2 ? 'selected' : '');?> value="2">Product</option>
													</select>
												</div>
												
												<div class="col-md-8" style="margin:0; padding:0">
													<select class="form-control input-sm opt" name="txtname" id="txtname" >
														<option value="">Select Type</option>
													</select>
												</div>
												<span id="divddreltype" style="color:red"></span>
												<span id="divtxtrelname" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
										
									</div>
									<!--row-->
									<h4 class="caption-subject font-blue bold uppercase">Reccurence </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Create Recurring Series of Event</label>
												<input type="checkbox" name="checkrec"  id="checkrec" checked="checked">
											</div>
										</div>
									</div>
									<!--/span-->
									<script>
										if(<?php echo $row->event_recurrence; ?>=='1')
										{
											$('#checkrec').attr('value','1');
											$('#checkrec').attr('checked','checked');
										}
										else
										{
											$('#checkrec').attr('value','0');
											$('#checkrec').removeAttr('checked','checked');
										}
									</script>
									<h4 class="caption-subject font-blue bold uppercase">Reminder </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Reminder</label> 
												<select class="form-control input-sm opt" name="ddreminder" id="ddreminder">
													<option value="0">0 minutes</option>
													<option value="5">5 minutes</option>
													<option value="10">10 minutes</option>
													<option value="15" selected="selected">15 minutes</option>
													<option value="30">30 minutes</option>
													<option value="60">1 hour</option>
													<option value="120">2 hours</option>
													<option value="180">3 hours</option>
													<option value="240">4 hours</option>
													<option value="300">5 hours</option>
													<option value="360">6 hours</option>
													<option value="420">7 hours</option>
													<option value="480">8 hours</option>
													<option value="540">9 hours</option>
													<option value="600">10 hours</option>
													<option value="660">11 hours</option>
													<option value="720">0.5 days</option>
													<option value="1080">18 hours</option>
													<option value="1440">1 day</option>
													<option value="2880">2 days</option>
													<option value="4320">3 days</option>
													<option value="5760">4 days</option>
													<option value="10080">1 week</option>
													<option value="20160">2 weeks</option>
												</select>  
												<span id="divddreminder" style="color:red"></span>
											</div>
										</div>
										<script>
											document.getElementById("ddreminder").value = "<?php echo ($row->event_reminder); ?>";
										</script>
										<!--row-->
										
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Attachment</label>
												<div class="fileinput fileinput-new" data-provides="fileinput">
													<div class="input-group input-large">
														<div class="form-control uneditable-input span3" data-trigger="fileinput">
															<i class="fa fa-file fileinput-exists"></i>&nbsp;
															<span class="fileinput-filename">
															</span>
														</div>
														<span class="input-group-addon btn default btn-file">
															<span class="fileinput-new">
																Select file
															</span>
															<span class="fileinput-exists">
																Change
															</span>
															<input type="file" name="fileatt" id="fileatt">
														</span>
														<a href="#" class="input-group-addon btn default fileinput-exists" data-dismiss="fileinput">
															Remove
														</a>
													</div>
												</div>		
												<span id="divfileatt" style="color:red"></span>
											</div>
										</div>
									</div>
									<!--/span-->
									
									<h4 class="caption-subject font-blue bold uppercase">Description Information </h4>
									<!--row-->
									
									<div class="row">
										<div class="col-md-8">
											<div class="form-group">
												<label class="control-label">Event Description</label>
												<textarea class="form-control input-sm empty" placeholder="Enter Event Description Information" name="txtcomment" id="txtcomment" rows="5"><?php echo $row->event_comment;?></textarea>	
												<span id="divtxtcomment" style="color:red"></span>
											</div>
										</div>
									</div>
								</div>
							</form>
							
							<div class="form-actions">
								<button type="button" onclick="update(0)" class="btn blue"><i class="fa fa-check"></i> Save</button>
								<button type="button" onclick="update(1)" class="btn blue"> Save & New</button>
								<button type="reset" onclick="" class="btn default">Cancel</button>
								
							</div>
							<!-- END FORM-->
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>

<script>
	load_detail();
	
	function load_detail()
	{
		var rel_name="<?php echo $row->event_rel_servicename; ?>";
		$("#txtrelname").empty();
		var txtrel=$("#ddrttype").val();
		$.post("<?php echo base_url();?>crm/get_detail_for_task/",{'for':txtrel} ,function(data) {
			$.each(data,function(i,item)
			{
			    if(rel_name!=item.account_id)
				{
					$('#txtrelname').append("<option value="+item.account_id+" >"+item.account_name+"</option>");
				}
				else 
				{
					$('#txtrelname').append("<option value="+item.account_id+" selected >"+item.account_name+"</option>");
				}
			});
			
		}, "json");
		
	}
</script>

<script>
	get_project();
	function get_project()
	{
		var ddrelate=$("#ddreltype").val();
		$("#txtname").empty();
		$.post("<?php echo base_url();?>crm/project_category1/",{'ddrelate':ddrelate} ,function(data) {
			$.each(data,function(i,item)
			{
				$('#txtname').append("<option value="+item.m_project_id+">"+item.m_project_name+"</option>");
			});
			
		}, "json");
	}
</script>

<script>
	function change_state()
	{
		var val1=$('#ddrttype').val();
		if(val1=='2')
		{
			$('#ddreltype').attr('disabled','disabled');
			$('#txtrelname').attr('disabled','disabled');
		}
		else
		{
			$('#ddreltype').removeAttr('disabled');
			$('#txtrelname').removeAttr('disabled');
		}
	}
</script>
<!-- <script>
	fill_userid1();
	
	function fill_userid1()
	{
		var user_id= document.getElementById("txtassign_id").value;
		
		if(user_id!="0")
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/master/validateUser/",
				data:"txtintuserid="+user_id,
				success: function(msg) {
					if(msg.trim()!="false")
					{
						document.getElementById("txtassign").value=msg.trim();  
					}
					else
					{
						document.getElementById("txtassign").value=msg.trim();                   
						alert('No User in this Id');
					}
				}
			}
			)
			
		}
		else
		{
			document.getElementById("txtassign").value="SuperAdmin";                    
		}
		
	}
</script> -->


<script>
	function update(id)
	{
		if(check("form_sample_1"))
		{
			bootbox.confirm('Are you sure to Submit form?', function(result){
				if(result==true)
				{
					var subject=$('#txtsub').val();
					var assign=$('#txtassign_id').val();
					var start_date=$('#txtstart_date').val();
					var end_date=$('#txtend_date').val();
					var status=$('#ddstatus').val();
					var location=$('#txtlocation').val();
					var type=$('#ddtype').val();
					var rttype=$('#ddrttype').val();
					var rtname=$('#txtrtname').val();
					var reltype=$('#ddreltype').val();
					var relname=$('#txtrelname').val();
					var recurance=$('#checkrec').val();
					var reminder=$('#ddreminder').val();
					var comment=$('#txtcomment').val();
					
					$.ajax(
					{
						type: "POST",
						url:"<?php echo base_url();?>index.php/crm/update_event/<?php echo $id; ?>",
						data:"txtsub="+subject+"&txtassign="+assign+"&txtstart_date="+start_date+"&txtend_date="+end_date+"&ddstatus="+status+"&txtlocation="+location+"&ddtype="+type+"&ddrttype="+rttype+"&txtrtname="+rtname+"&ddreltype="+reltype+"&txtrelname="+relname+"&checkrec="+recurance+"&ddreminder="+reminder+"&txtcomment="+comment,
						success: function(msg) {
							if(msg.trim()=="true")
							{
								$(".page-content").html("<center><h2>Event Updated Successfully!</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(1000,function()
								{
									if(id==0)
									{
										location.reload(); 
									}
									else
									{
										location.reload();  
									}					
								}
								);
								
							}	  
						}
					});
				}
			}); 
		}
		else
		{
			return false;
		}
	}
</script>


<script>
	function exit()
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url().'index.php/crm/show_task'?>");
	}
</script>
