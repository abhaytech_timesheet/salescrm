<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="#.">SALES BOOSTER</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Campaign</span>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Multiple Template for Email</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-envelope-open font-black"></i>
								<span class="caption-subject font-black bold uppercase">Edit Email Template</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!--BEGIN TABS-->
							<form id="myform" method="post" >
								<div class="form-body">
									
									<div id="content_email">
										<div id="content_email_temp">
											
											<?php 
												foreach($content->result() as $row)
												{ 
												?>										
												<div class="form-group">                                     
													<label>Subject of Email</label>
													<input type="text" class="form-control input-sm empty" id="txttitle" name="txttitle" value="<?php echo $row->m_email_temp_title; ?>" placeholder="Enter text"/>
													<span id="divtxttitle" style="color:red"></span>
												</div>
												<div class="form-group">
													<label>Category Of Email</label>
													<select class="form-control input-sm opt" name="category" id="category">
														<option value="-1">Select</option>
														<?php
															foreach($cate->result() as $row1)
															{
																if($row1->m_email_id==$row->category)
																{
																?>
																<option value="<?php echo $row1->m_email_id; ?>" selected="selected"><?php echo $row1->m_email_name; ?></option>
																<?php
																}
																else
																{
																?>
																<option value="<?php echo $row1->m_email_id; ?>"><?php echo $row1->m_email_name; ?></option>
																<?php
																}
															}
														?>
													</select>
													<input type="hidden" id="hidden" value="<?php echo $id; ?>"  />
													<span id="divcategory" style="color:red"></span>
												</div>
												<div style="clear:both"></div>                                  
												<div class="form-group">
													<div>
														<textarea class="form-control input-sm empty" name="editor1"  id="editor1" data-provide="markdown" data-error-container="#editor_error" rows="6"><?php echo $row->m_email_temp_description; ?></textarea>
														<span id="diveditor1" style="color:red"></span>
													</div>
												</div>
											</div>
										</div>
									</div>
									
								<?php } ?>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="button" onclick="update()" class="btn green"><i class="fa fa-check"></i> Update</button>								
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	

<script>
	function update()
	{
		if(check("myform"))
		{
			bootbox.confirm('Are you sure to Submit form?', function(result){
				if(result==true)
				{
					var title = $('#txttitle').val();
					var category = $('#category').val();
					var id = $('#hidden').val();
					//alert(id);
					//var content=CKEDITOR.replace('editor1');
					//var editor = CKEDITOR.instances.editor1;
					var content=$('#editor1').val();                  //editor.getData();
					//alert(content);
					$.ajax(
					
					{
						type: "POST",
						url:"<?php echo base_url(); ?>index.php/crm/update_email_template/",
						data: "title="+title+"&category="+category+"&content="+content+"&id="+id,
						success: function(msg) {
							//alert(msg);
							if(msg!="")
							{
								$("#stylized").html("<center><h2>Email Template Updated Successfully!</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(1000,function()
								{
									$("#stylized").load("<?php echo base_url().'index.php/crm/after_view_create_email_template'?>");
								}
								);
								
							}
						}
					});
				}
			}); 
		}
		else
		{
			return false;
		}
	}
</script>
