<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-folder"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/create_sms_campaign">
							Campaign
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-calendar"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_create_email_template">Create Campaign</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-envelope-open"></i>
						<span> Email Campaign</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Multiple Template for Email</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-envelope-open font-black"></i>
								<span class="caption-subject font-black bold uppercase">Add Email Template</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form method="post" id="myform">
								<div class="form-body">
									
									<div id="content_email">
										<div id="content_email_temp">
											
											<div class="form-group">                                     
												<label>Subject of Email</label>
												<input type="text" class="form-control input-sm empty" id="txttitle" name="txttitle" placeholder="Enter text"/>
												<span id="divtxttitle" style="color:red"></span>
											</div>
											<div class="form-group">
												<label>Category Of Email</label>
												<select class="form-control input-sm opt" name="category" id="category">
													<option value="-1">Select</option>
													<?php
														foreach($cate->result() as $row)
														{
														?>
														<option value="<?php echo $row->m_email_id; ?>"><?php echo $row->m_email_name; ?></option>
														<?php
														}
													?>
												</select>
												<span id="divcategory" style="color:red"></span>
											</div>
											<div style="clear:both"></div>                                  
											<div class="form-group">
												<div>
													<textarea class="form-control input-sm empty" name="editor1"  id="editor1" rows="6" data-provide="markdown" data-error-container="#editor_error"></textarea>
													<span id="diveditor1" style="color:red"></span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="button" onclick="insert()" class="btn green"><i class="fa fa-check"></i> Save</button>								
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="glyphicon glyphicon-envelope font-dark"></i>
								<span class="caption-subject bold uppercase"></span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th width="33">
											S.No.
										</th>
										<th width="126">
											Email Title
										</th>
										<!--<th width="126">
											Email Description
										</th>-->
										<th>
											Action
										</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$serial = 1;
										foreach($content->result() as $row)
										{
										?>
										<tr>
											
											<td><?php echo $serial; ?></td>
											<td><?php echo $row->m_email_temp_title ?></td>
											<!-- <td><?php echo substr($row->m_email_temp_description,0,20); ?></td>-->
											<td>
												<div class="btn-group">
													<button class="btn red btn-sm dropdown-toggle" data-toggle="dropdown">Action
														<i class="fa fa-angle-down"></i>
													</button>
													<ul class="dropdown-menu">
														<li>
															<a href="javascript:void(0)" title="Edit Your Event Details" onclick="email_edit('<?php echo $row->m_email_temp_id; ?>')"><i class="fa fa-edit"></i> Edit</a>
														</li>
														<li>
															<a href="javascript:void(0)" title="Delete Event" onclick="del('<?php echo $row->m_email_temp_id; ?>')" ><i class="fa fa-times"></i> Delete</a>
														</li>
													</ul>
												</div>
												
											</td>
										</tr>
									<?php $serial++; } ?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													

<script>
	function insert()
	{
		if(check("myform"))
		{
			bootbox.confirm('Are you sure to Submit form?', function(result){
				if(result==true)
				{
					var title = $('#txttitle').val();
					//var editor = CKEDITOR.instances.editor1;
					var content=$('#editor1').val();   //editor.getData();
					var category =$('#category').val();
					alert(content);
					$.ajax(
					{
						type: "POST",
						url:"<?php echo base_url(); ?>index.php/crm/insert_email_temp/",
						data: "title="+title+"&content="+content+"&category="+category,
						success: function(msg) {
							//alert(msg);
							if(msg!="")
							{
								$(".stylized").html("<center><h2>Email Template added successfully!</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(1000,function()
								{
									$("#stylized").load("<?php echo base_url().'index.php/crm/after_view_create_email_template/'?>");
								}
								);
								
							}
						}
						
					});
				}
			}); 
		}
		else
		{
			return false;
		}
	}
</script>

<script>
	function del(id)
	{
		var email_id = id;
		$.ajax(
		{
			type:"POST",
			url: "<?php echo base_url(); ?>index.php/crm/delete_email/",
			data: "id="+email_id,
			success: function(msg) {
				location.reload();
				alert(msg);
			}
			
		});
		
	}
</script>

<script type="text/javascript">
	function loadUrl(newLocation)
	{
		window.location = newLocation;
		return false;
	}
</script>

<script>
	function email_edit(id)
	{
		var eve_id = id;
		loadUrl("<?php echo base_url();?>index.php/crm/view_edit_email/"+id);
	}
</script>								