<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/<?php echo $this->router->fetch_method();?>/<?php echo $this->uri->segment(3);?>">
							CRM
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-bubbles"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_account/0">Account</a>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">View All Account</h3>
			<!-- END PAGE TITLE-->
			
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet-title">
						<div class="caption font-dark">
							<i class="fa fa-search font-dark" ></i>
							<span class="caption-subject bold uppercase">Search Account</span>
						</div>
						<div class="tools"> </div>
					</div>
					<div class="table-responsive" id="data1">
						<table class="table table-striped table-bordered table-hover table-full-width" id="datatable_products">
							<thead>
								<tr role="row" class="heading">
									<th width="10%">
										Account&nbsp;ID
									</th>
									<th width="15%">
										Account&nbsp;Name
									</th>
									<th width="15%">
										Type
									</th>
									<th width="10%">
										Owner&nbsp;Alias
									</th>
									<th width="15%">
										Date&nbsp;From
									</th>
									<th width="15%">
										Date&nbsp;At
									</th>
									<th width="10%">
										Status
									</th>
									<th width="10%">
										Actions
									</th>
								</tr>
								
								<tr role="row" class="filter">
									
									<td>
										<input type="text" class="form-control input-sm" name="product_id">
									</td>
									<td>
										<input type="text" class="form-control input-sm" name="product_name">
									</td>
									<td>
										<select name="product_category" class="form-control input-sm">
											<option value="-1">Select...</option>
											<option value="1">Customer</option>
										</select>
									</td>
									<td>
										<input type="text" class="form-control input-sm" name="product_name">
									</td>
									<td>
										<div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
											<input type="text" class="form-control input-sm" readonly name="product_created_from" placeholder="From">
											<span class="input-group-btn">
												<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>										
									</td>
									<td>
										<div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
											<input type="text" class="form-control input-sm" readonly name="product_created_to " placeholder="To">
											<span class="input-group-btn">
												<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</td>
									<td>
										<select name="product_status" class="form-control input-sm">
											<option value="">Select...</option>
											<option value="0">Deactive</option>
											<option value="1">Active</option>
											<option value="2">Pending</option>
										</select>
									</td>
									<td>
										<div class="margin-bottom-5">
											<button class="btn btn-sm green filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
										</div>
										<!-- <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button> -->
									</td>
								</tr>
							</thead>
						</table>
						
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-ticket font-dark" ></i>
								<span class="caption-subject bold uppercase">View & Modify View all Accounts</span>
							</div>
                            <?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_add==1) { ?>
								<div class="actions">
									<div class="btn-group btn-group-devided" data-toggle="buttons">
										<a href="javascript:void(0)" onclick="create_account()" class="btn green blue-stripe">
											<i class="fa fa-plus"></i>
											<span class="hidden-480">
												Create Account
											</span>
										</a>
									</div>
								</div>
							<?php } ?>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<h5> &nbsp;</h5>
										</div>
									</div>
									<div class="col-md-6">
										
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								
								<thead>
									<th>S No.</th>
                                    <?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_update==1) { ?>
										<th class="ignore">Action</th>
									<?php } ?>
									<th>Created On</th>
									<th>Type</th>
									<th>ID</th>
									<th>Name</th>
									<th>Contact</th>
									<th>Email</th>
									<th>Owner Alias</th>
									<th>Address</th>										
									<th>Status</th>
									
									
								</tr>
							</thead>
							<tbody>
								<?php
									$sn=1;
									foreach($account->result() as $row)
									{
									?>
									<tr>
									    <td><?php echo $sn;?></td>
										<td class="ignore">
											<div class="btn-group">
												<button class="btn red btn-sm dropdown-toggle" data-toggle="dropdown">Action
													<i class="fa fa-angle-down"></i>
												</button>
												<ul class="dropdown-menu" >
													<?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_update==1) 
														{?>
														<li>
															<a href="javascript:void(0)" title="Edit" onClick="update_account(<?php echo $row->ACC_ID;?>)">
															<i class="fa fa-pencil"></i>Edit Account</a>
														</li>
														<li>
															
															<a href="<?php echo base_url();?>admin/ticket/<?php echo $row->ACC_ID; ?>" title="Submit Ticket" >
															<i class="fa fa-ticket"></i> Ticket</a>
															
														</li>
														<li>
															<a href="<?php echo base_url();?>crm/view_account_project/<?php echo $row->ACC_ID; ?>" title="Add Project"><i class="fa fa-plus"></i>Account Project</a>
															
														</li>
														<li>
															<a href="<?php echo base_url();?>crm/view_account_profile/<?php echo $row->ACC_ID."/".$row->ACC_OWNER; ?>" title="View Profile"><i class="fa fa-info"></i>Account Profile</a>
															
														</li>
													<?php }?>
												</ul>
											</div>
											
										</td>
										<td><?php $date=date_create($row->REG_DATE); echo date_format($date,'d-m-Y') ?></td>                                                                
										<td><?php echo $row->ACC_TYPE;; ?></td>
										<td><?php echo $row->USER_ID; ?></td>                                                                
										<td><?php echo $row->ACC_NAME; ?></td>                                                                
										<td><?php echo $row->ACC_PHONE; ?></td>
										<td><?php echo $row->ACC_EMAIL; ?></td>
										<td><?php echo $row->Owner ?></td>
										<td><?php echo $row->ACC_STATE.'/'.$row->ACC_CITY; ?></td>
										<td><?php echo$row->ACC_STATUS; ?></td>
									</tr>
									
									<?php
										$sn++;
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
				
			</div>
			<!-- END PAGE CONTENT-->
		</div>
		
	</div>
	<!-- END CONTENT BODY -->
</div>
</div>

<script>
	function create_account()
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url().'crm/add_account/'.$id?>");			
	}
	function update_account(id)
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url().'crm/edit_account/'?>"+id);			
	}
</script>