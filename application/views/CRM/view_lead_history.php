<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_lead/0">
							CRM
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="icon-speedometer"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_lead/0">
							Lead
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="icon-speedometer"></i>
						<span>
							Add Followup
						</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Lead  Follow Up History </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6 col-sm-6">
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-cursor font-purple"></i>
									<span class="caption-subject font-purple bold uppercase">Lead Follow Up History </span>
								</div>
								
								<div class="actions">
									<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
										<i class="icon-cloud-upload"></i>
									</a>
									<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
										<i class="icon-wrench"></i>
									</a>
									<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
									<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
										<i class="icon-trash"></i>
									</a>
								</div>
							</div>
							<div class="portlet-body form" id="chats">
								
								<div class="scroller" style="height: 435px;" data-always-visible="1" data-rail-visible1="1">
									<ul class="chats">
										<?php
											foreach($rec->result() as $reply_row)
											{
												if(($reply_row->Follow_up_by)!=null)
												{
													if(($reply_row->Response_type)==1)
													{
														$rep=$reply_row->or_m_name;
														$type='out';
													}
													if(($reply_row->Response_type)==2)
													{
														$rep=$reply_row->or_m_name;
														$type='in';
													}
													if(($reply_row->Response_type)==3)
													{
														$rep=$reply_row->or_m_name;
														$type='out';
													}
													if(($reply_row->Response_type)==4)
													{
														$rep=$reply_row->or_m_name;
														$type='in';
													}
                                                    
													if($reply_row->or_m_userimage!="")
													{
														$img=base_url()."application/".$reply_row->FILE.$reply_row->or_m_userimage;
													}
													else
													{
														$img=base_url()."application/libraries/noimage.jpg";
													}
                                                    
												?>
												<li class="<?php echo $type; ?>">
												    <img class="avatar img-responsive" alt="" src="<?php echo $img; ?>" width="45px" height="45px"/>
													<div class="message">
														<span class="arrow">
														</span>
														<a href="#" class="name">
															<?php echo  $rep; ?>
														</a>
														<span class="datetime">
															at <?php echo $reply_row->Follow_up_date ?>
														</span>
														<span class="body">
															<?php echo $reply_row->Follow_Description ?>
														</span>
													</div>
												</li>
												<?php 
												} 
											}
										?>
									</ul>
								</div>
								<div class="chat-form">
									<div class="input-cont">
										<input class="form-control" type="text" id="task_description" name="task_description" placeholder="Type a message here..."/>
									</div>
									<div class="btn-cont">
										<span class="arrow">
										</span>
										<a href="javascript:void(0)" onClick="enter_followup(1)" class="btn blue icn-only">
											<i class="fa fa-check icon-white"></i>
										</a>
									</div>
								</div>
							</div>
							
						</div>
						
					</div>
					<!-- END PAGE CONTENT-->
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<script>
	$("#task_description").keypress(TriggerSearch);
	
	function search_product() {
		enter_followup(1);
	}
	
	function TriggerSearch(e) {
		var search_string = $("#task_description").val();
		e = e || window.event;
		var keycode;
		if (window.event) {
			keycode = e.which ? window.event.which : window.event.keyCode;
		}
		var key = e.which;
		switch (key) {
			case 38:
            break;
			case 40:
            break;
			case 13:
            e.preventDefault();
            search_product();
            break;
			default:
            break;
		}
	}
</script>

<script>
	function enter_followup(id)
	{ 
		var task_description = $('#task_description').val();
		if(task_description!="" &&task_description!='')
		{
			if(confirm('Are you sure to add followup on a Task?')==true)
			{
				//var task_description = $('#task_description').val();
				$.ajax(
				{	
					type:"POST",
					url:"<?php echo base_url();?>crm/insert_lead_follow_up/<?php echo $task_id;?>",
					data:"task_description="+task_description,
					success: function(msg) {
						if(msg.trim()=="true")
						{
							$(".page-content").html("<center><h2>Followup Insert Successfully!</h2></center>")
							.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
							.hide()
							.fadeIn(1000,function()
							{
								location.reload();					
							}
							);
							
						}
					}
				});
			}
			else
			{
				$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
				window.location.href="<?php echo base_url().'crm/view_lead_history/'.$task_id?>";
			}
		}
		else
		{
			
			alert('Please Fill follow up detail');
			$('#task_description').focus();
		}
	}
</script>
</div>