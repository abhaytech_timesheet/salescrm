<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_task/0">
							CRM
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="icon-speedometer"></i>
						<span>
							Task
						</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View all Task</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-tasks font-dark" ></i>
								<span class="caption-subject bold uppercase">View & Modify View all Task</span>
							</div>
							<?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_add==1) { ?>
								<div class="actions">
									<div class="btn-group btn-group-devided" data-toggle="buttons">
										<a href="javascript:void(0)" onclick="create_task()" class="btn green blue-stripe" style="margin-right:20px;">
											<i class="fa fa-plus"></i>
											<span class="hidden-480">
												Create Task
											</span>
										</a>
										
									</div>
								</div>
							<?php } ?>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<h5> &nbsp;</h5>
										</div>
									</div>
									<div class="col-md-6">
										
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S.No</th>
                                        <?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_update==1) { ?>
											<th class="ignore">Action</th>
										<?php } ?>
                                        <th>Status</th>
										<th>Create On</th>
										<th>Schedule At</th>
										<th>Type</th>
										<th>Subject</th>
										<th>Rel To/Rel Name</th>
										<th>Assign</th>
										<th>Priority</th>
										
										<th>Completed At</th>
										<th>Attachment</th>
										
									</tr>
								</thead>
								<tbody>
									<?php
										$sn=1;
										foreach($rec->result() as $row)
										{
										?>
										<tr>
											<td><?php echo $sn;?></td>
                                            <?php foreach($menu->result() AS $roes) { } if($roes->tr_assign_update==1) { ?>
												<td class="ignore">
													<div class="btn-group">
														<button class="btn red btn-sm dropdown-toggle" data-toggle="dropdown">Action
															<i class="fa fa-angle-down"></i>
														</button>
														<ul class="dropdown-menu">
															<li>
																<a href="javascript:void(0)" title="Edit" onClick="update_task(<?php echo $row->task_id;?>)">
																<i class="fa fa-pencil"></i>Edit</a>
															</li>
															
															<?php 
																if($row->task_status==2)
																{ ?>
																<li>
																	<a href="<?php echo base_url();?>crm/view_task_history/<?php echo $row->task_id;?>" title="Task History" >
																	<i class="fa fa-file"></i>Task History</a>
																</li>
																<?php 
																}
																else
																{ ?>
																<li>
																	<a href="<?php echo base_url();?>crm/view_task_history/<?php echo $row->task_id;?>" title="Add Follow Up" onClick="update_task_followup(<?php echo $row->task_id;?>)">
																	<i class="fa fa-reply"></i>Add Follow Up</a>
																</li>
															<?php } ?>
															<?php
																if($row->task_type=='1' && $row->task_status!='2')
																{
																?>
																<li>
																	<a href="javascript:void(0)" title="Edit" onClick="goto_type(1)">
																	<i class="glyphicon glyphicon-phone-alt"></i>Make A Call</a>
																</li>
																<?php 
																}
																if($row->task_type=='2' && $row->task_status!='2')
																{
																?>
																<li>
																	<a href="javascript:void(0)" title="Edit" onClick="goto_type(2)">
																	<i class="glyphicon glyphicon-envelope"></i>Send Email </a>
																</li>
																
															<?php } ?>
														</ul>
													</div>
													
												</td>
											<?php } ?>
											<td><?php 
												if($row->task_status!=2)
												{
													$date=date_create($row->task_reminder);
													$d=date_format($date,'Y-m-d');
													$date1=date_create($curr);
													$today=date_format($date1,'Y-m-d');
													if($d < $today)
													{echo "<button type='button' class='btn btn-xs blue'>Task time expired</button>";}
													elseif($d > $today)
													{echo "<button type='button' class='btn btn-xs green'>Task Coming Soon</button>";}
													elseif($d == $today)
													{echo "<button type='button' class='btn btn-xs purple'>Do your Task Today</button>";} 
												}
												else
												{
													echo "<button type='button' class='btn btn-xs green'>Task Completed</button>";
												}?></td>
												<td><?php $date=date_create($row->task_create_date);echo date_format($date,'d-m-Y') ?></td>
												<td><?php echo $row->task_reminder; ?></td>
												<td><?php echo $row->type; ?></td>
												<td><?php echo $row->subject; ?></td>
												<td><?php echo$row->task_related_to; ?> / <?php echo $row->name; ?></td>
												<td><?php echo $row->task_assignto_name; ?></td>
												
												<td><?php echo $row->task_priority; ?></td>
												
												<td> <?php if($row->task_status==2)
													{ echo $row->task_complete_at;
													}elseif($row->task_status==1)
													{ echo '';
													}?> </td> 
													
													<td><?php echo $row->task_attachment; ?></td>
													
										</tr>
										
										<?php
											$sn++;
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->

<script>
	function create_task()
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url().'index.php/crm/create_task/'?>");			
	}
	function update_task(id)
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url().'index.php/crm/edit_task/'?>"+id);			
	}
</script>

<script>
	function goto_type(id)
	{
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		if(id=='1')
		{
			$("#stylized").load("<?php echo base_url().'index.php/crm/view_makecall'?>");
		}
		if(id=='2')
		{
			$("#stylized").load("<?php echo base_url().'index.php/crm/make_email'?>");
		}
	}
</script>