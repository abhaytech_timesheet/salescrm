<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mockdata_reports/0">
							Mock Data
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="icon-speedometer"></i>
						<span>
							View Mock Data Report
						</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View Mock Data Report</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-tasks font-dark" ></i>
								<span class="caption-subject bold uppercase">View Mock Data Report</span>
							</div>
							
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<h5> &nbsp;</h5>
										</div>
									</div>
									<div class="col-md-6">
										
									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S.No</th>
										<th>Date</th>
										<th>Trader</th>
										<th>Exch</th>
										<th>Algorithm</th>
										<th>Instrument</th>
										<th>FCM</th>
										<th>Size</th>
										<th>Filled</th>
										<th>Volume</th>
										<th>Passive</th>
										<th>Cleanup</th>
										<th>AP</th>
										<th>STF</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$sn=1;
										foreach($mock->result() as $row)
										{
										?>
										<tr>
											<td><?php echo $sn;?></td>
											
											<td><?php  echo $row->Date; ?></td>
											<td><?php echo $row->Trader; ?></td>
											<td><?php echo $row->Exch; ?></td>
											<td><?php echo $row->Algo; ?></td>
											<td><?php echo$row->Instrument; ?> </td>
											<td><?php echo $row->FCM; ?></td>
											
											<td><?php echo $row->Size; ?></td>
											<td><?php echo $row->Filled; ?></td>
												<td><?php echo $row->Volume; ?></td>
													
													<td><?php echo $row->Passive; ?></td>
													<td><?php echo $row->Cleanup; ?></td>
													<td><?php echo $row->AP; ?></td>
													<td><?php echo $row->STF; ?></td>
													
										</tr>
										
										<?php
											$sn++;
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->


