<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_contact/0">
							CRM
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-call-in"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_contact/0">Contact</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-edit"></i>
						<span>Edit Contact</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">Edit Contact </h3>
			<!-- END PAGE TITLE-->
			<?php
				foreach($cont->result() as $row)
				{
					break;
				}
			?>
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Edit Contact </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form  class="horizontal-form"  id="form_sample_1">
								<div class="form-body">
									<h4 class="caption-subject font-blue bold uppercase"> Conatct Information </h4>
									
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Account Name</label>
												<?php
													if($row->is_account!=0)
													{
													?>
													<select class="form-control input-sm opt" name="ddacc_name" id="ddacc_name">
														<?php
														}
														else
														{
														?>
														<select class="form-control input-sm opt" name="ddacc_name" id="ddacc_name" disabled>
															<option value="">Select Account Name</option>
															<?php
															}
															if($row->is_account!=0)
															{
																foreach($acname->result() as $accrow)
																{
																	if($row->account_id==$accrow->account_id)
																	{
																	?>
																	<option value="<?php echo $accrow->account_id ?>" selected><?php echo $accrow->account_name; ?></option>
																	<?php
																	}
																}
															}
															else
															{
																foreach($info->result() as $accrow)
																{
																	if($row->account_id==$accrow->opportunity_id)
																	{
																	?>
																	<option value="<?php echo $accrow->opportunity_id?>" selected><?php echo $accrow->opportunity_account_name; ?></option>
																	<?php
																	}	
																}
															}
														?>
													</select>
													<span id="divddacc_name" style="color:red"></span>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Job Position</label>
													<input class="form-control input-sm empty" placeholder="Enter Job Position" name="txtjobpostion" id="txtjobpostion" size="16" type="text" value="<?php echo $row->contact_designation ?>"  />
													<span id="divtxtjobpostion" style="color:red"></span>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<script>
														$("#ddprefix").prop('selectedIndex', <?php echo $row->contact_title;?>);
													</script>
													<label class="control-label">Name</label>
												</div>
												<div class="form-group">
													<div class="row">
														<div class="col-md-3" style="margin:0; padding:0;">
															<select id="ddprefix" name="ddprefix" class="form-control input-sm opt">
																<option value="-1">Select</option>
																<option value="1">Mr</option>
																<option value="2">Ms</option>
																<option value="3">Mrs</option>
																<option value="4">Dr</option>
																<option value="5">Prof</option>
															</select>
														</div>
														<div class="col-md-9" style="margin:0; padding:0;">
															<input type="text" class="form-control input-sm empty" name="txtcontactname" placeholder="Enter Name" id="txtcontactname" value="<?php echo $row->contact_name;?>">
														</div>
													</div>
													<span id="divddprefix" style="color:red"></span>
													<span id="divtxtcontactname" style="color:red"></span>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Mobile Number</label>
												</div>
												<div class="form-group">
													<input type="text" class="form-control input-sm" placeholder="Enter Lead Mobile Number" name="txtmobile" id="txtmobile" value="<?php echo$row->contact_mobile; ?>" maxlength="10">
													<span id="divtxtmobile" style="color:red"></span>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Email ID</label>
												</div>
												<div class="form-group">
													<input type="text" class="form-control input-sm" name="txtemail" placeholder="Enter Email address" id="txtemail" value="<?php echo $row->contact_email; ?>">
													<span id="divtxtmail" style="color:red"></span>
												</div>
											</div>
										</div>
										<h4 class="caption-subject font-blue bold uppercase">Address Information</h4>
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Country</label>
													<input type="hidden" id="txtstate" name="txtstate" class="form-control input-sm"  value="<?php echo $row->contact_state;?>"/>
													<input type="hidden" id="txtcity" name="txtcity" class="form-control input-sm"  value="<?php echo $row->contact_city;?>"/>
													
													
													<select class="form-control input-sm opt" name="ddcountry" id="ddcountry" onchange="get_state()">
														<option value="-2">Select Country</option>
														<?php
															foreach($loc->result() as $locrow)
															{
																if($row->contact_country==$locrow->m_loc_id)
																{
																?>
																<option value="<?php echo $locrow->m_loc_id?>" selected><?php echo $locrow->m_loc_name; ?></option>
																<?php
																}
																else
																{
																?>
																<option value="<?php echo $locrow->m_loc_id?>"><?php echo $locrow->m_loc_name; ?></option>
																<?php
																}		
															}
														?>
													</select>
													<span id="divddcountry" style="color:red"></span>
												</div>
											</div>
											<!--/span-->
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">State</label>
													<div id="state">
														<select class="form-control input-sm opt" name="ddstate" id="ddstate" onchange="get_city()">
															<option value="">Select State</option>
															
														</select>
													</div>
													<span id="divddstate" style="color:red"></span>
												</div>
											</div>
											
											<!--row-->
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">City</label>
													<div id="city">
														<select class="form-control input-sm opt" name="ddcity" id="ddcity">
															<option value="">Select City</option>
														</select>										
													</div>
													<span id="divddcity" style="color:red"></span>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Street
													</label>
													<input type="text" class="form-control input-sm" PLACEHOLDER="Enter Street/Locality" name="txtstreet" id="txtstreet" value="<?php echo $row->contact_address ?>">										
													<span id="divtxtstreet" style="color:red"></span>
												</div>
											</div>
											<!--/span-->
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Zip Code
													</label>
													<input type="text" class="form-control input-sm" placeholder="Enter Zipcode" name="txtzipcode" id="txtzipcode" value="<?php echo $row->contact_zipcode ?>" maxlength="6">														
													<span id="divtxtzipcode" style="color:red"></span>
												</div>
											</div>
											<!--/span-->
										</div>
										
									</div>
									<div class="form-actions middle">
										<button type="button" onclick="update_contact(0)" class="btn green"><i class="fa fa-check"></i> Save</button>
										<button type="button" onclick="update_contact(1)" class="btn green" >Save & Exit</button>
										<button type="reset" class="btn default">Cancel</button>
										
									</div>
								</form>
								
								
							</div>
						</div>
						
					</div>
					<!-- END PAGE CONTENT-->
					
				</div>
				
			</div>
			<!-- END CONTENT BODY -->
		</div>
	</div>
	<!-- END CONTENT -->	
	
	<script>
		load();
		function load()
		{
			var state_id=$("#txtstate").val();
			var country_id=$("#ddcountry").val();
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/crm/select_state/"+country_id+"/"+state_id,
				dataType: "JSON",
				data:{'state_id':state_id},
				success:function(data){
					$("#ddstate").empty();
					$("#ddstate").append("<option value=-1> ~~ Select ~~</option>");
					
					$.each(data,function(i,item)
					{
						if(state_id==item.m_loc_id)
						{
							$("#ddstate").append("<option value="+item.m_loc_id+" selected>"+item.m_loc_name+"</option>");
							
						}
						else
						{
							$("#ddstate").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
						}
					});
				}
			});
			
			var city_id=$("#txtcity").val();
			$("#ddcity").empty();
			if(state_id!='-1' && state_id!='')
			{
				$.ajax(
				{
					type:"POST",
					url:"<?php echo base_url();?>index.php/crm/select_city/"+state_id+"/"+city_id,
					dataType: "JSON",
					data:{'state_id':state_id},
					success:function(data){
						
						$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
						$.each(data,function(i,item)
						{
							if(city_id==item.m_loc_id)
							{
								$("#ddcity").append("<option value="+item.m_loc_id+" selected>"+item.m_loc_name+"</option>");
								
							}
							else
							{
								$("#ddcity").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
							}
						});
					}
				});
			}
			else
			{
				$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
				alert("Please First Select The State");
			}
			
		}
		function get_state()
		{
			var country_id=$("#ddcountry").val();
			$("#ddstate").empty();
			if(country_id!='-1' && country_id!='')
			{
				$.ajax(
				{
					type:"POST",
					url:"<?php echo base_url();?>index.php/crm/select_state/"+country_id+"/0",
					dataType: "JSON",
					data:{'country_id':country_id},
					success:function(data){
						$("#ddstate").empty();
						$("#ddstate").append("<option value=-1> ~~ Select ~~</option>");
						
						$.each(data,function(i,item)
						{
							
							$("#ddstate").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
							
						});
					}
				});
			}
			else
			{
				$("#ddstate").append("<option value=-1> ~~ Select ~~</option>");
				alert("Please First Select The State");
			}
			
		}
	</script>
	<script>
		function get_city()
		{
			var state_id=$("#ddstate").val();
			$("#ddcity").empty();
			if(state_id!='-1' && state_id!='')
			{
				$.blockUI({ message: '<h3>Please Wait...</h3>' });
				$.ajax(
				{
					type:"POST",
					url:"<?php echo base_url();?>index.php/crm/select_city/"+state_id+"/0",
					dataType: "JSON",
					data:{'state_id':state_id},
					success:function(data){
						
						setTimeout($.unblockUI, 01);
						$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
						$.each(data,function(i,item)
						{
							$("#ddcity").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
						});
					}
				});
			}
			else
			{
				$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
				alert("Please First Select The State");
			}
		}
	</script>
	
	<script>
		function fill_city()
		{
			var state_id=$("#ddstate").val();
			$("#city").html('<div><img src ="<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
			$("#city").load("<?php echo base_url();?>index.php/master/select_cit/"+state_id);
		}
	</script>
	<script>
		function update_contact(id)
		{
			if(check("form_sample_1"))
			{
				bootbox.confirm('Are you sure to Submit form?', function(result){
					if(result==true)
					{
						var accountname=$('#ddacc_name').val();
						var designation=$('#txtjobpostion').val();
						var contactprefix=$('#ddprefix').val();
						var txtcontactname=$('#txtcontactname').val();
						var mobilenumber=$('#txtmobile').val();
						var emailid=$('#txtemail').val();
						var country=$('#ddcountry').val();
						var state=$('#ddstate').val();
						var city = $('#ddcity').val();
						var street = $('#txtstreet').val();
						var txtzipcode = $('#txtzipcode').val();
						$.ajax(
						{
							type: "POST",
							url:"<?php echo base_url();?>index.php/crm/update_contact/<?php echo $contact_id?>",
							data:"accountname="+accountname+"&designation="+designation+"&txtcontactprefix="+contactprefix+"&txtcontactname="+txtcontactname+"&txtmobile="+mobilenumber+"&txtemail="+emailid+"&ddcountry="+country+"&ddstate="+state+"&ddcity="+city+"&txtstreet="+street+"&txtzipcode="+txtzipcode,
							success: function(msg) {
								if(msg.trim()=="true")
								{
									$(".page-content").html("<center><h2>Update Contact Successfully!</h2></center>")
									.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
									.hide()
									.fadeIn(1000,function()
									{
										if(id==1)
										{
											location.reload();  //$("#stylized").load("<?php echo base_url().'index.php/crm/show_contact'?>");  //changed to location.reload();
										}
										else
										{
											$("#stylized").load("<?php echo base_url().'index.php/crm/edit_contact/'.$contact_id?>");
										}					
									}
									);
									
								}
								else
								{
									alert("Some Error on this page");
									
								}
								
							}
						});
					}
				}); 
			}
			else
			{
				return false;
			}
			
		}
	</script>
	<script>
		function exit()
		{
			$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
			$("#stylized").load("<?php echo base_url().'index.php/crm/show_contact'?>");
		}
	</script>	
	