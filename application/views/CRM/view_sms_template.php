<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
						Admin Panel 
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						
						<li>
							<i class="fa fa-home"></i>
							<a href="#">
								Dashboard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Campaign
							</a>
							
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<!--row-->
				<div class="col-md-6">
					<!-- BEGIN PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Add SMS Template
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<!--BEGIN TABS-->
							<div class="tabbable tabbable-custom">
								
								<div id="content_email">
									<div id="content_email_temp">
										<div data-always-visible="1" data-rail-visible="0">	
											
											<div class="form-group">                                     
												<label>Subject of SMS</label>
												<input type="text" class="form-control input-large" id="txttitle" name="txttitle" placeholder="Enter text"/>
											</div>
											<div class="form-group">
												<label>Category Of SMS</label>
												<select class="form-control input-large" name="category" id="category">
													<option value="-1">Select</option>
													<?php
														foreach($cate->result() as $row)
														{
														?>
														<option value="<?php echo $row->m_email_id; ?>"><?php echo $row->m_email_name; ?></option>
														<?php
														}
													?>
												</select>
											</div>
											<div style="clear:both"></div>                                  
											<div class="form-group">
												<div>
													<label>Content Of SMS</label>
													<textarea class="form-control input-xlarge" name="editor1"  id="editor1" rows="6"></textarea>
												</div>
											</div>
											<div class="form-actions">
												<button  onclick="insert()" class="btn green"><i class="fa fa-check"></i> Save</button>								
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-6">
                	<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>View SMS Template
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
								<thead>
									<tr>
										<th width="33">
											#
										</th>
										<th width="126">
											SMS Title
										</th>
										<th width="126">
											SMS Description
										</th>
										<th width="49">
											Action
										</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$serial = 1;
										foreach($content->result() as $row)
										{
										?>
										<input type="text" name="txtdep" id="txtdep<?php echo $row->m_email_temp_id; ?>" value="<?php echo $row->m_email_temp_description ?>" style="display:none;" />
										<tr class="odd gradeX">
											
											<td><?php echo $serial; ?></td>
											<td><?php echo $row->m_email_temp_title ?></td>
											<td><?php echo substr($row->m_email_temp_description,0,20) ?></td>
											<td>
												<a href="javascript:void(0)" title="Edit Your Event Details" onclick="send_action('<?php echo $row->m_email_temp_id; ?>')" class="btn btn-xs purple"><i class="fa fa-check"></i>Get Message</a>
											</td>
										</tr>
										<?php
											$serial++;
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->
</div>
<script>
	function enable()
	{
		
		var sour=$('#ddsource').val();
		var cate=$('#ddcategory').val();
		if(sour==2)
		{
			$('#ddcategory').removeAttr("disabled");	
		}
		else
		{
			$('#ddcategory').attr( "disabled", "disabled" );
		}
		
	}
</script>

<script>
	function get_details()
	{
		var sour=$('#ddsource').val();
		var cate=$('#ddcategory').val();
		$("#contact").html('<div><img src ="<?php echo base_url();?>application/libraries/assets/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
		$("#contact").load("<?php echo base_url() ?>index.php/crm/get_details_no/"+sour+"/"+cate);
		
	}
</script>

<script>
	function send()
	{
		var contact = $('#txtcontact').val();
		var tmplate = $('#txttemplate').val();
		var checked_site_radio = $('input:radio[name=chksms]:checked').val();
		$.ajax(
        {
			type: "POST",
			url:"<?php echo base_url();?>index.php/crm/make_compaign/",
			data:"txtcontact="+contact+"&txttemplate="+tmplate,
			success: function(msg) {
				if(msg=="true")
				{
					$(".page-content").html("<center><h2>Message send Successfully!</h2></center>")
					.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/img/loading-spinner-blue.gif' /></center>")
					.hide()
					.fadeIn(1000,function()
					{
						$("#stylized").load("<?php echo base_url().'index.php/crm/create_Campaign'?>");
					}
					);
					
				}	  
			}
		});
	}
</script>
<script>
	function send_action(id)
	{
		var dep=$('#txtdep'+id).val();
		$('#editor1').attr('value',dep);
	}
</script>


<script>
	function insert()
	{
		var title = $('#txttitle').val();
		var content=$('#editor1').val();
		var category =$('#category').val();
		var type = $('input:radio[name=opttype]:checked').val();
		$.ajax(
        {
			type: "POST",
			url:"<?php echo base_url(); ?>index.php/crm/insert_sms_temp/",
			data: "title="+title+"&content="+content+"&category="+category,
			success: function(msg) {
				alert(msg);
				location.reload();
			}
			
		});
	}
</script>