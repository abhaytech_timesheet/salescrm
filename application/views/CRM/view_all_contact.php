<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->

			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
						<a href="<?php echo base_url(); ?><?php echo $this->router->fetch_class(); ?>/view_contact/0">
							CRM
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="icon-call-in"></i>
						<span>Contact</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View All Contact</h3>
			<!-- END PAGE TITLE-->
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive" id="data1">
						<table class="table table-striped table-bordered table-hover table-full-width" id="datatable_products">
							<thead>
								<tr role="row" class="heading">
									<th width="10%">
										Account&nbsp;ID
									</th>
									<th width="15%">
										Account&nbsp;Name
									</th>
									<th width="15%">
										Person&nbsp;Name
									</th>
									<th width="10%">
										Person&nbsp;Contact
									</th>
									<th width="15%">
										Date&nbsp;From
									</th>
									<th width="15%">
										Date&nbsp;To
									</th>
									<th width="10%">
										Status
									</th>
									<th width="10%">
										Actions
									</th>
								</tr>

								<tr role="row" class="filter">

									<td>
										<input type="text" class="form-control form-filter input-sm" name="product_id">
									</td>
									<td>
										<input type="text" class="form-control form-filter input-sm" name="product_name">
									</td>
									<td>
										<input type="text" class="form-control form-filter input-sm" name="product_name">
									</td>
									<td>
										<input type="text" class="form-control form-filter input-sm" name="product_name">
									</td>
									<td>
										<div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
											<input type="text" class="form-control form-filter input-sm" readonly name="product_created_from" placeholder="From">
											<span class="input-group-btn">
												<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</td>
									<td>
										<div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
											<input type="text" class="form-control form-filter input-sm" readonly name="product_created_to " placeholder="To">
											<span class="input-group-btn">
												<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
										</div>
									</td>
									<td>
										<select name="product_status" class="form-control form-filter input-sm">
											<option value="">Select...</option>
											<option value="0">Deactive</option>
											<option value="1">Active</option>
											<option value="2">Pending</option>
										</select>
									</td>
									<td>
										<div class="margin-bottom-5">
											<button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
										</div>
									</td>
								</tr>
							</thead>
						</table>
						<!-- <form method="post" action="#" name="status" id="status">
									<div class="form-body">
									<div class="form-group">
									<label class="col-md-2 control-label">Department</label>
									<div class="col-md-3">
									<select name="txtdepartment" id="txtdepartment" class="form-control input-medium">
									<option value="-1">Select</option>
									<option value="1">Support</option>
									<option value="2">Sales</option>
									<option value="3">Development</option>
									<option value="4">Complaints</option>
									</select>
									</div>
									<input type="button" name="submit" id="submit" onClick="display_ticket()" class="btn green" value="SUBMIT"/>
									</div>
									</div>
									
									</p>
								</form>-->
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-mobile font-dark"></i>
								<span class="caption-subject bold uppercase">View & Modify View All Contacts</span>
							</div>
							<?php foreach ($menu->result() as $roes) {
							}
							if ($roes->tr_assign_add == 1) { ?>
								<div class="actions">
									<div class="btn-group btn-group-devided" data-toggle="buttons">
										<a href="javascript:void(0)" onclick="create_account()" class="btn green blue-stripe" style="margin-right:20px;">
											<i class="fa fa-plus"></i>
											<span class="hidden-480">
												Create Contact
											</span>
										</a>

									</div>
								</div>
							<?php } ?>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<h5> &nbsp;</h5>
										</div>
									</div>
									<div class="col-md-6">

									</div>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover" id="sample_1">

								<thead>
									<tr>
										<th>S No.</th>
										<?php foreach ($menu->result() as $roes) {
										}
										if ($roes->tr_assign_update == 1) { ?>
											<th class="ignore">Action</th>
										<?php } ?>
										<th>Created On</th>
										<th>Account Rel To</th>
										<th>Name</th>
										<th>Job Position</th>
										<th>Contact</th>
										<th>Email</th>
										<th>Address</th>
										<th>Status</th>


									</tr>
								</thead>
								<tbody>
									<?php
									$sn = 1;
									foreach ($rec->result() as $row) {
									?>
										<tr>
											<td><?php echo $sn; ?></td>
											<?php foreach ($menu->result() as $roes) {
											}
											if ($roes->tr_assign_update == 1) { ?>
												<td class="ignore">
													<div class="btn-group">
														<button class="btn red btn-sm dropdown-toggle" data-toggle="dropdown">Action
															<i class="fa fa-angle-down"></i>
														</button>
														<ul class="dropdown-menu">
															<li>
																<a href="javascript:void(0)" title="Edit" onClick="update_account(<?php echo $row->contact_id; ?>)">
																	<i class="fa fa-pencil"></i>Edit</a>
															</li>

															<li>
																<a href="javascript:void(0)" title="Create Task on Contact" onClick="create_task(<?php echo $row->contact_id; ?>)">
																	<i class="fa fa-tasks"></i>Create Task</a>
															</li>

														</ul>
													</div>
												</td>
											<?php } ?>
											<td><?php $date = date_create($row->contact_reg_date);
												echo date_format($date, 'd-m-Y') ?></td>
											<td><?php echo $row->rel;
												echo $row->account_rel_to; ?></td>
											<td><?php echo $row->contact_name ?></td>
											<td><?php echo $row->contact_designation ?></td>
											<td><?php echo $row->contact_mobile ?></td>
											<td><?php echo $row->contact_email ?></td>
											<td><?php echo $row->contact_address ?></td>
											<td><?php echo $row->contact_status; ?></td>
										</tr>

									<?php
										$sn++;
									}
									?>
								</tbody>
							</table>
						</div>
					</div>

				</div>
				<!-- END PAGE CONTENT-->
			</div>

		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->

<script>
	function create_account() {
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url() . 'index.php/crm/add_contact' ?>");
	}

	function update_account(id) {
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url() . 'index.php/crm/edit_contact/' ?>" + id);
	}

	function create_task(id) {
		$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$("#stylized").load("<?php echo base_url() . 'index.php/crm/assign_task/' ?>" + id + "/2");
	}
</script>