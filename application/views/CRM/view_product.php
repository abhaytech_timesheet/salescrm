<script>
	function fill_city()
	{
		
		var state_id=$("#txtstate").val();
		$("#city").html('<div><img src ="<?php echo base_url();?>application/libraries/assets/img/loading.gif" alt="Loading....." title="Loading...."></div>');
		$("#city").load("<?php echo base_url();?>index.php/master/select_city/"+state_id);
	}
	function fill_city1()
	{
		
		var state_id1=$("#txtstate1").val();
		$("#city1").html('<div><img src ="<?php echo base_url();?>application/libraries/assets/img/loading.gif" alt="Loading....." title="Loading...."></div>');
		$("#city1").load("<?php echo base_url();?>index.php/master/select_city1/"+state_id1);
	}
</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Modal title</h4>
					</div>
					<div class="modal-body">
						Widget settings form goes here
					</div>
					<div class="modal-footer">
						<button type="button" class="btn blue">Save changes</button>
						<button type="button" class="btn default" data-dismiss="modal">Close</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Create Account 
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					<li class="btn-group">
						<button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							<span>
								Actions
							</span>
							<i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu pull-right" role="menu">
							<li>
								<a href="#">
									Action
								</a>
							</li>
							<li>
								<a href="#">
									Another action
								</a>
							</li>
							<li>
								<a href="#">
									Something else here
								</a>
							</li>
							<li class="divider">
							</li>
							<li>
								<a href="#">
									Separated link
								</a>
							</li>
						</ul>
					</li>
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">
							Home
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">
							Create Account
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">
							
						</a>
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<div class="tabbable tabbable-custom boxless tabbable-reversed">
					
					
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption"> <i class="fa fa-reorder"></i>Create New Account</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						
						
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="#" class="horizontal-form"  id="form_sample_1">
								<div class="alert alert-danger display-hide">
									<button class="close" data-close="alert"></button>
								You have some form errors. Please check below. </div>
								<div class="alert alert-success display-hide">
									<button class="close" data-close="alert"></button>
								Your form validation is successful! </div>
								
								<div class="form-body">
									<h3 class="form-section">Basic Information</h3>	
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Account Name
													<span class="required">
														*
													</span></label>
													<select id="ddacc_name" name="ddacc_name" class="form-control">
														<option selected="selected" value="">Select Account Name</option>
														<?php
															foreach($desig->result() as $row)
															{
															?>
															<option value="<?php echo $row->account_id?>"><?php echo $row->account_name; ?></option>
															<?php
															}
														?>
													</select>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Accont Contact Number
													<span class="required">
														*
													</span></label>
													<input type="text"PLACEHOLDER="Enter Account Contact Number" class="form-control" name="txtacc_contct" id="txtacc_contct">
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Product Category
													<span class="required">
														*
													</span></label>
													<select id="ddprod_categry" name="ddprod_categry" class="form-control">
														<option selected="selected" value="">Select Product Ctegory</option>
														<?php
															foreach($pcat->result() as $row)
															{
															?>
															<option value="<?php echo $row->cat_id?>"><?php echo $row->cat_name; ?></option>
															<?php
															}
														?>
													</select>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->
									
									
									
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Product Code
													<span class="required">
														*
													</span></label>
													<input type="text" PLACEHOLDER="Enter Product Code"class="form-control" name="txtprod_code" id="txtprod_code">									
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Product Name
													<span class="required">
														*
													</span></label>
													<input type="text"PLACEHOLDER="Enter Product Name" class="form-control" name="txtprod_name" id="txtprod_name">
													<?php
														foreach($desig->result() as $row)
														{
														?>
														<option value="<?php echo $row->account_id?>"><?php echo $row->account_industry; ?></option>
														<?php
														}
													?>
											</select>
										</div>
									</div>
									<!--/span-->
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">Vendor Name</label>
											<input type="text"Placeholder="Enter Vendor Name" class="form-control" name="txtvend_name" id="txtvend_name">
										</div>
									</div>
									
								</div>
								
								<!--row-->
								
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">Sales Start Date
												<span class="required">
													*
												</span></label>
												<input type="text"PLACEHOLDER="mm/dd/yyyy"class="form-control date-picker" name="txtstrt_date" id="txtstrt_date">                            
										</div>
									</div>
									<!--/span-->
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">Sales End Date
												<span class="required">
													*
												</span>
											</label>
											<input type="text"PLACEHOLDER="mm/dd/yyyy" class="form-control date-picker" name="txtend_date" id="txtend_date">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">
												Support Start Date
												<span class="required">
													*
												</span>
											</label>
											<input type="text"PLACEHOLDER="mm/dd/yyyy" class="form-control date-picker" name="txtsuppstrt_date" id="txtsuppstrt_date">
										</div>
										
									</div>
									<!--/span-->
									
								</div>
								
								
								<!--row-->
								
								
								<div class="row">
									
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">
												Support End Date
												<span class="required">
													*
												</span>
											</label>
											<input type="text"PLACEHOLDER="mm/dd/yyyy" class="form-control date-picker" name="txtsupportend_date" id="txtsupportend_date">
										</div>
									</div>
									
									<!--/span-->
								</div>
								<h3 class="form-section">Price Information</h3>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">Unit Price
												<span class="required">
													*
												</span>
											</label>
											<input type="text"PLACEHOLDER="Enter Product Unit Price" name="txtunit_price" id="txtunit_price" class="form-control">                                        
										</div>
										</div> <div class="col-md-4">
										<div class="form-group">
											<label class="control-label">Tax
												<span class="required">
													*
												</span></label>
												<select id="ddtax" name="ddtax" class="form-control">
													<option selected="selected" value="">Select Tax</option>
													<?php
														foreach($tax->result() as $row)
														{
														?>
														<option value="<?php echo $row->m_tax_id?>"><?php echo $row->m_tax_name; ?></option>
														<?php
														}
													?>
												</select>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">Actual Price
											</label>
											<input type="text" PLACEHOLDER="Enter Price In INR"name="txtact_prc" id="txtact_prc" class="form-control  input-medium">									
										</div>
									</div></div>
									<!--/span-->
									
									<div class="row">
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Commision Rate
													<span class="required">
														*
													</span>
												</label>
												<input type="text"PLACEHOLDER="Enter Commission Rate" name="txtcommsn_rate" id="txtcommsn_rate" class="form-control">										
											</div>
										</div>
										
										<!--row-->
										
										
									</div>
									<!--/span-->
									
									<!--row-->
									<h3 class="form-section">Description Information</h3>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Product Description
													<span class="required">
														*
													</span></label>
													<textarea class="form-control" PLACEHOLDER="Enter Product Description Information" name="txtlead_des" id="txtlead_des"></textarea>										
											</div>
										</div></div>
										<!--/span-->
										
										<!--/span-->
										
										<!--row-->
										
										
										<!--/span-->
										
										<!--/span-->
										
										<!--row-->
										
										
										<!--/span-->
										
										
										<!--row-->
										<!--/span-->
										
										<!--/span-->
										
										
										<!--row-->
										
										
										
										<!--/span-->
										
										<!--/span-->
										
										<!--row-->
										
										
										<!--row-->
										
										
										<!--row-->
										
										<!--/span-->
										<!--/span-->
										<!--row-->
										
										<!--/span-->
										
										<!--/span-->
										
										<!--row-->
										
										
										<!--/span-->
										
										
										
										<div class="form-actions right">
											<button type="button" class="btn default">Cancel</button>
											<button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
										</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
				
				
				
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<script>
	function get_spcl()
	{
		var desig=document.getElementById("txtdesignation").value;
		$("#desi").load('<?php echo base_url() ;?>index.php/crm/get_spcl'+desig);
	}
	</script>												