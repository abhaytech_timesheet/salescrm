<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_lead/0">
							CRM
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="icon-bag"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_lead/0">
							Leads
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-plus"></i>
						<span>
							Create Task
						</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Create Task </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-black"></i>
								<span class="caption-subject font-black bold uppercase">Create New Task </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form action="#" class="horizontal-form"  id="form_sample_1">
								<div class="form-body">
									<h4 class="caption-subject font-blue bold uppercase"> Task Information </h4>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Subject</label>
												<input class="form-control input-sm empty"  name="txtsubject" id="txtsubject" type="text"/>
												<span id="divtxtsubject" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Task Type</label>
												<select class="form-control input-sm opt" name="txttasktype" id="txttasktype">
													<option value="-1">Select Task</option>
													<option value="1">Call</option>
													<option value="2">Send a Mail</option>
												</select>
												<span id="divtxttasktype" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="control-label">Assigned To</label>
												<input type="text" class="form-control input-sm empty" name="txtassign"  id="txtassign" value="<?php echo $this->session->userdata('name')?>" disabled>
												<input type="hidden" class="form-control" name="txtassign_id"  id="txtassign_id" value="<?php echo $this->session->userdata('profile_id')?>"/>
												<span id="divtxtassign" style="color:red"></span>
											</div>
										</div>
										
									</div>
									<!--/row-->
									<div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<label class="control-label">Status</label>
												<select class="form-control input-sm opt" name="ddstatus" id="ddstatus">
													<option value="1">Open</option>                                    
													<option value="2">Completed</option>
												</select>	
												<span id="divddstatus" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<label class="control-label">Priority</label>
												<select class="form-control input-sm opt" name="ddpriority" id="ddpriority">
													<option value="1">High</option>
													<option value="2">Medium</option>
													<option value="3">Low</option>
												</select>
												<span id="divddpriority" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-2">
											<div class="form-group">
												<label class="control-label">Due Date</label>
												<input type="text" class="form-control input-sm date-picker" name="txtduedate" id="txtduedate" data-date-format="yyyy-mm-dd" data-date-start-date="+1d">
												<span id="divtxtduedate" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
										
										
									</div>
									
									<!--row-->
									<h4 class="caption-subject font-blue bold uppercase"> Related To </h4>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Name</label>
												<div class="form-group">
													<div class="col-md-3" style="margin:0; padding:0">
														<select class="form-control input-sm opt" name="txtrel" id="txtrel" readonly="readonly" onchange="load_detail()" ><!--onchange="related()"-->
															<option value="2" <?php  if($is_lead==2) { echo 'selected';} ?>>Contact</option>
															<option value="1" <?php  if($is_lead==1) { echo 'selected';} ?>>Lead</option>
														</select>
													</div>
													<div class="col-md-6" style="margin:0; padding:0" >
														<select class="form-control input-sm opt" name="ddassignto" readonly="readonly" id="ddassignto" ><!--onchange="related()"-->
															<option value="-1">Select Name</option>
														</select>
													</div>
												</div>
												
											</div>
											
										</div>
										<!--/span-->
										
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Related To</label>
												<div class="form-group">
													<div class="col-md-3" style="margin:0; padding:0">
														<select class="form-control input-sm opt" name="ddrelate" id="ddrelate" onchange="get_project()">
															<option value="1">Website</option>
															<option value="2">Product</option>
														</select>
													</div>
													<div class="col-md-6" style="margin:0; padding:0">
														<select class="form-control input-sm opt" name="ddwebsite" id="ddwebsite" >
															<option value="">Select Type</option>
														</select>	</div>
														<span id="divddrelate" style="color:red"></span>
														<span id="divtxtname" style="color:red"></span>
												</div>
											</div>
										</div>
									</div>
									<!--row-->
									<h4 class="caption-subject font-blue bold uppercase"> Reccurence </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Create Recurring Series of Task</label>
												<input type="checkbox" name="checkrec" value='1' id="checkrec" onclick="$(this).is(':checked') && $('#checked-a').slideDown('slow') || $('#checked-a').slideUp('slow');">                                        
											</div>
										</div>
									</div>
									<!----------hide & show reminder------------>		
									<div class="dis-sub" style="display:none;" id="checked-a">
										<script>
											function get_radio(id)
											{
												$('#radio_rec').val(id);
											}
										</script>				
										
										<div class="row">
											<div class="col-md-4">
												<div class="radio-list">
													<label >
														<input type="radio" name="radio" id="rec1" onclick="get_radio(1)"/>
													Daily </label>
													<label >
														<input type="radio" name="radio" id="rec2" onclick="get_radio(2)"/>
													Weekly </label>
													<label >
														<input type="radio" name="radio" id="rec3" onclick="get_radio(3)"/>
													After 30 Days </label>
													<label >
														<input type="radio" name="radio" id="rec4" onclick="get_radio(4)"/>
													Yearly </label>
												</div>
												<input type="hidden" name="radio_rec" id="radio_rec" value="" />
											</div>
											
											<!--/span-->
											
											<div class="col-md-4">
												<div class="input-group input-xlarge date-picker input-daterange" data-date-format="dd-mm-yyyy" data-date-start-date="+1d">
													<input type="text" class="form-control input-sm" name="txtfromdate" id="txtfromdate">
													<span class="input-group-addon">
														to
													</span>
													<input type="text" class="form-control input-sm" name="txttodate" id="txttodate">
												</div>
											</div>
										</div>
									</div>	
									<h4 class="caption-subject font-blue bold uppercase"> Reminder </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Reminder</label>
												<!----Reminder date--->
												<div id="reminder">
													<div class="form-group">
														
														<div class="form-group input-group date form_datetime input-medium">
															<input type="text" size="16" name="txtreminder" id="txtreminder" readonly class="form-control input-sm ">
															<span class="input-group-btn">
																<button class="btn btn-sm date-set" type="button"><i class="fa fa-calendar"></i></button>
															</span>
														</div>  
														
													</div>
													
												</div>
												<span id="divtxtreminder" style="color:red"></span>
												<!----Recurance Day--->
												<div id="recurance" style="display:none;">
													<select class="form-control input-sm" name="ddrecurance_before" id="ddrecurance_before">
														<option value="-1">On Recursion Date</option>
														<option value="1">Before 1 day</option>
														<option value="2">Before 2 day</option>
														<option value="3">Before 3 day</option>
													</select>
													<span id="divddrecurance_before" style="color:red"></span>
												</div>
												
											</div>
										</div>
										<!--row-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Attachment</label><div style="clear:both;"></div>
												<div class="fileinput fileinput-new input-sm" data-provides="fileinput">
													<div class="input-group">
														<div class="form-control uneditable-input input-sm " data-trigger="fileinput">
															<i class="fa fa-file fileinput-exists"></i>&nbsp;
															<span class="fileinput-filename">
															</span>
														</div>
														<span class="input-group-addon btn-sm default btn-file">
															<span class="fileinput-new"> Select file </span>
															<span class="fileinput-exists"> Change </span>
															<input type="file" class="emptyfile input-sm" name="userfile" id="userfile" >
														</span>
														<a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
													</div>
												</div>
												<input type="hidden" name="filename" id="filename" />
												<span id="divuserfile" style="color:red"></span>
											</div>
										</div>
									</div>
									<!--/span-->
									
									<!--row-->
									<h4 class="caption-subject font-blue bold uppercase"> Description Information </h4>
									<div class="row">
										<div class="col-md-8">
											<div class="form-group">
												<label class="control-label">Task Description</label>
												<textarea class="form-control input-sm empty" placeholder="Enter Task Description Information" name="txtcomment" id="txtcomment" style="font-size:20px" col="3"></textarea>				
												<span id="divtxtcomment" style="color:red"></span>
											</div>
										</div>
									</div>
								</div>
							</form>
							
							<div class="form-actions left">
								<button type="button" onclick="submitFile(0)" class="btn green"><i class="fa fa-check"></i> Save</button>
								<button type="button" onclick="submitFile(1)" class="btn green"><i class="fa fa-check"></i> Save & New Task</button>
								<button type="button" onclick="exit()" class="btn default">Cancel</button>
							</div>
							<!-- END FORM-->
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<script>
	function ajaxSearch() {
		var input_data = $('#txtrelname').val();
		var ddname=$('#ddname').val();
		if (input_data.length === 0) {
			$('#suggestions').hide();
			} else {
			
			var post_data = {
				'search_data': input_data
			};
			
			$.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>crm/autocomplete/"+ddname,
				data: post_data,
				success: function(data) {
					// return success
					if (data.length > 0) {
						$('#suggestions').hide();//$('#suggestions').show();
						$('#autoSuggestionsList').addClass('auto_list');
						$('#autoSuggestionsList').html(data);
					}
					else
					{
						$('#suggestions').hide();
					}
				}
			});
			
		}
		
	}
</script>

<script>
	function exit()
	{
		if(confirm('Are you sure to Exit from Create Task?')==true)
		{
			$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
			$("#stylized").load("<?php echo base_url().'crm/show_task'?>");
		}
	}
</script>
<script>
	fill_userid();
	function fill_userid()
	{
		var user_id=<?php echo $this->session->userdata('profile_id')?>;
		if(user_id!="0")
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>master/validateUser/1",
				data:"txtintuserid="+user_id,
				success: function(msg) {
					if(msg!="false")
					{
						document.getElementById("txtassign").value=msg.trim();  
					}
					else
					{
						document.getElementById("txtassign").value=msg.trim();                   
						alert('No User in this Id');
					}
				}
			}
			)
			
		}
		else
		{
			document.getElementById("txtassign").value="SuperAdmin";                    
		}
		
	}
</script>
<script>
	function submitFile(id)
	{
		var file=$("#userfile").val();
		if(file!="")
		{
			var formUrl = "<?php echo base_url(); ?>crm/insert_opportunity_file";
			var formData = new FormData($('.horizontal-form')[0]);
			$.ajax({
				url: formUrl,
				type: 'POST',
				data: formData,
				mimeType: "multipart/form-data",
				contentType: false,
				cache: false,
				processData: false,
				success: function(data){
					//now get here response returned by PHP in JSON fomat you can parse it using JSON.parse(data)
					$("#filename").val(data.trim());
					insert(id);
				},
				error: function(jqXHR, textStatus, errorThrown){
					//handle here error returned
				}
			});
		}
		else
		{
			insert(id);
		}
		
	}
</script>
<script>
	function insert(id)
	{ 
		if(check("form_sample_1"))
		{
			bootbox.confirm('Are you sure to Submit form?', function(result){
				if(result==true)
				{
					if($('#checkrec').prop('checked') === true)
					{
						var checkrec = $('#checkrec').val();
					}
					else
					{
						var checkrec = '0';
					}
					
					var txtsubject = $('#txtsubject').val();
					var txttasktype = $('#txttasktype').val();
					var txtassign_id = $('#txtassign_id').val();
					var ddstatus = $('#ddstatus').val();
					var ddpriority = $('#ddpriority').val();
					var txtduedate = $('#txtduedate').val();
					var ddname = $('#ddassignto').val();
					var txtrel = $('#txtrel').val();
					var ddrelate=$('#ddrelate').val();
					var txtname=$('#ddwebsite').val();
					var radio_rec = $('#radio_rec').val();
					var txtfromdate = $('#txtfromdate').val();
					var txttodate = $('#txttodate').val();
					var txtreminder = $('#txtreminder').val();
					var ddrecurance_before = $('#ddrecurance_before').val();
					var fileatt=$('#filename').val();
					
					var txtcomment = $('#txtcomment').val();
					$.ajax(
					{	
						type:"POST",
						url:"<?php echo base_url();?>crm/insert_task/",
						data:"txtsub="+txtsubject+"&txttasktype="+txttasktype+"&txtassign_id="+txtassign_id+"&ddstatus="+ddstatus+"&ddpriority="+ddpriority+"&txtduedate="+txtduedate+"&txtrel="+txtrel+"&ddname="+ddname+"&ddrelate="+ddrelate+"&txtname="+txtname+"&checkrec="+checkrec+"&radio_rec="+radio_rec+"&txtfromdate="+txtfromdate+"&txttodate="+txttodate+"&txtreminder="+txtreminder+"&ddrecurance_before="+ddrecurance_before+"&fileatt="+fileatt+"&txtcomment="+txtcomment,
						success: function(msg) {
							if(msg.trim()=="true")
							{
								$(".page-content").html("<center><h2>Task Created Successfully!</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(1000,function()
								{
									if(id==0)
									location.reload(); //$("#stylized").load("<?php echo base_url().'crm/show_task'?>"); //changed to location.reload();	
									if(id==1)
									$("#stylized").load("<?php echo base_url().'crm/create_task'?>");					
								}
								);
								
							}
						}
					});
				}
			}); 
		}
		else
		{
			return false;
		}
	}
</script>

<script>
	$("#checkrec").change(function(){
		
		var ischecked=$(this).is(':checked'); 
		if(ischecked)
		{	
			$('#reminder').css('display','none');
			$('#recurance').removeAttr('style');
			$("#checked-a").fadein(200);
			$("#checked-a").removeAttr('style');
		}
		else
		{
			$('#recurance').css('display','none');
			$('#reminder').removeAttr('style');
			$("#checked-a").fadein(200);
		}
		
	});
</script>

<script>
	get_project();
	function get_project()
	{
		var ddrelate=$("#ddrelate").val();
		$("#ddwebsite").empty();
		$.post("<?php echo base_url();?>crm/project_category1/",{'ddrelate':ddrelate} ,function(data) {
			$.each(data,function(i,item)
			{
				$('#ddwebsite').append("<option value="+item.m_project_id+">"+item.m_project_name+"</option>");
			});
			
		}, "json");
	}
</script>
<script>
	load_detail();
	
	function load_detail()
	{
		$("#ddassignto").empty();
		var txtrel=$("#txtrel").val();
        var lead="<?php echo $id;?>";
		$.post("<?php echo base_url();?>crm/get_detail_for_task/",{'for':txtrel} ,function(data) {
			$.each(data,function(i,item)
			{
                if(lead==item.account_id)
                {
				$('#ddassignto').append("<option value="+item.account_id+" selected='selected'>"+item.account_name+"</option>");
                } 
                else
                {
                $('#ddassignto').append("<option value="+item.account_id+">"+item.account_name+"</option>");
                }
			});
			
		}, "json");
	}
	
</script>																		