<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_account/0">
							CRM
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-bubbles"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_account/0">Account</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-bubbles"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_account_project/<?php echo $this->uri->segment(3);?>">View Account Project</a>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Create Project</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-settings font-dark"></i>
								<span class="caption-subject bold uppercase"></span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th width="33">
											#
										</th>
										<th width="126">
											Created On
										</th>
										<th width="126">
											Project / Website Name
										</th>
										<th width="126">
											Project Description
										</th>
										<th width="126">
											Project Cost
										</th>
										
									</tr>
								</thead>
								<tbody>
									<?php
										$serial = 1;
										foreach($proj->result() as $row5)
										{
											
										?>
										<tr class="odd gradeX">
											<td><?php echo $row5->ACCP_PROJ_SN; ?></td>
											<td><?php echo substr($row5->m_project_create,0,10); ?></td>
											<td><?php echo $row5->Proj_name.'<span style="color:red">/</span>'.$row5->Website_name; ?></td>
											<td><?php echo substr($row5->Proj_Description,0,40); ?></td>
											<td><?php echo $row5->ACCP_UNIT_PRICE; ?></td>
											
										</tr>
										<?php
											$serial++;
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	


<script>
	fill_userid();
	function fill_userid()
	{
		var user_id=<?php echo $this->session->userdata('user_id')?>;
		if(user_id!="9999999999")
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/master/validateUser/0",
				data:"txtintuserid="+user_id,
				success: function(msg) {
					if(msg!="false")
					{
					}
					else
					{
						document.getElementById("txtowner").value=msg;                   
						alert('No User in this Id');
					}
				}
			}
			)
			
		}
		else
		{
			document.getElementById("txtowner").value="SuperAdmin";                    
		}	
	}
</script>

<script>
	function insert()
	{
		if(check("myform"))
		{
			bootbox.confirm('Are you sure to Submit form?', function(result){
				if(result==true)
				{
					var txtpro_owner = $('#txtacc_owner').val();
					var txtprojectcost = $('#txtprojectcost').val();
					var txtprojectname = $('#txtprojectname').val();
					var txtdescription = $('#txtdescription').val();
					
					$.ajax(
					{
						type: "POST",
						url:"<?php echo base_url();?>index.php/crm/insert_acc_proj/<?php echo $this->uri->segment(3); ?>",
						data:"txtacc_owner="+txtpro_owner+"&txtprojectcost="+txtprojectcost+"&txtprojectname="+txtprojectname+"&txtdescription="+txtdescription,
						success: function(msg) {
							if(msg!="")
							{
								$(".page-content").html("<center><h2>Participants Added Successfully!</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(1000,function()
								{
									$("#stylized").load("<?php echo base_url().'index.php/crm/show_account'?>");
								}
								);
								
							}	  
						}
					});
				}
			}); 
		}
		else
		{
			return false;
		}
	}
</script>																