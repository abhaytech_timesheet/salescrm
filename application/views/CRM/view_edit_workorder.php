<script>
	function check()
	{
		var collection=$("#stylized");
		var mark=0;
		var obtainmark=0;
		var vali=false;
		var inputs=collection.find("input[type=text],select,textarea");
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			var name=inputs[x].name;
			var value=$("#"+id+"").val();
			var type=inputs[x].type;
			if($("#"+id+"").attr('class')=="form-control aplha_only" || $("#"+id+"").attr('class')=="form-control input-medium input-inline aplha_only")
			{
				if((type=="text" || type=="textarea") && value!="" && value!=0 )
				{
					var pattern=/^[a-zA-Z ]*$/;
					if($("#"+id+"").val().match(pattern))  
					{  
						$("#div"+id+"").html('');
					}  
					else
					{
						$("#"+id+"").focus();
						$("#div"+id+"").html("*Only alpha characters and space allowed. You have entered an invalid input in this field!");
						return false;  
					}
				}
				else
				{
					$("#"+id+"").focus();
					$("#div"+id+"").html("This Feild is required.You can't leave this empty");
					return false;
				}
			}
			if($("#"+id+"").attr('name')=="txtmail")
			{
				if(type=="text" && value!="" && value!=0 )
				{
					var pattern=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
					if($("#"+id+"").val().match(pattern))  
					{  
						$("#div"+id+"").html('');
					}  
					else
					{
						$("#"+id+"").focus();
						$("#div"+id+"").html("*You have entered an invalid Email!,Please Fill Email in this format xyz@domain.com");
						return false;  
					} 
				}
				else
				{
					$("#"+id+"").focus();
					$("#div"+id+"").html("This Feild is required.You can't leave this empty");
					return false;
				}
				
			}
			if($("#"+id+"").attr('name')=="txtmobile")
			{
				if(type=="text" && value!="" && value!=0 )
				{
					var numbers = /^[0-9]+$/;  
					if($("#"+id+"").val().match(numbers))  
					{   
						var val=$("#"+id+"").val();
						if(val.charAt(0)!="0")
						{
							$("#"+id+"").html('');
							if(val.length == 10)
							{
								$("#div"+id+"").html(''); 
							}
							else
							{
								$("#"+id+"").focus();
								$("#div"+id+"").html("Mobile Phone must be 10 Digit numeric no!. ");
								return false;
							}
						}
						else
						{
							$("#"+id+"").focus();
							$("#div"+id+"").html("Mobile Phone should not start with zero!. ");
							
							return false;
						}
					}   
					else  
					{ 
						$("#"+id+"").focus();
						$("#div"+id+"").html("Mobile Phone must be numeric!.This Field contain only numbers. ");   
						return false;  
					}
				}
				else
				{
					$("#"+id+"").focus();
					$("#div"+id+"").html("This Feild is required.You can't leave this empty");
					return false;
				}   
			}
			
			if($("#"+id+"").attr('class')=="form-control input-small input-inline opt"||$("#"+id+"").attr('class')=="form-control opt")
			{
				if($("#"+id+"").val() == -1 || $("#"+id+"").val()=="" || $("#"+id+"").val()==0)
				{
					$("#"+id+"").focus();
					$("#div"+id+"").html("This Feild is required.Please Select");
					return false;
				}
			}
			$("#div"+id+"").html('');
			vali=true;
			
		}
		return vali;
	}
	
</script>
<!-- BEGIN CONTENT -->
<div id="stylized">
	<div class="page-content-wrapper">
		<div class="page-content" >		
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
						Edit Proposal 
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="">CRM</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href=""> Edit Proposal </a>
							<i class="fa fa-angle-right"></i>
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom boxless tabbable-reversed">
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"> <i class="fa fa-reorder"></i>Edit Proposal </div>
								<div class="tools">
									<a href="javascript:;" class="collapse">
									</a>
									<a href="#portlet-config" data-toggle="modal" class="config">
									</a>
									<a href="javascript:;" class="reload">
									</a>
									<a href="javascript:;" class="remove">
									</a>
								</div>
							</div>
							
							<?php
								foreach($info1->result() as $info)
								{
									
								?>
								<div class="portlet-body form">
									<!-- BEGIN FORM-->
									<form  class="horizontal-form" id="form_sample_1">
										<div class="form-body">
											<div class="row">
												<div class="col-md-6" >
													<h3 class="form-section">Enqiury Information</h3>
													<div class="row">
														<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
															Enqiury No
														</div>
														<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
															:  <?php echo $info->Enqiury_No;?>	
														</div>
													</div>
													<div class="row">
														<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
															Enqiury Ownername
														</div>
														<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
															:  <?php echo $info->Enquiry_owner;?>	
														</div>
													</div>
													<div class="row">
														<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
															Enqiury Type
														</div>
														<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
															:  <?php echo $info->Enquiry_for;?>	
														</div>
													</div>
													<div class="row">
														<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
															Reference
														</div>
														<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
															:  <?php echo $info->Enquiry_refrence;?>	
														</div>
													</div>
													<div class="row">
														<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
															Enqiury Source
														</div>
														<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
															:  <?php echo $info->Enquiry_source;?>	
														</div>
													</div>
													<div class="row">
														<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
															Client Type
														</div>
														<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
															:  <?php echo $info->Enquiry_industry;?>
														</div>
													</div>
													<div class="row">
														<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
															Prospect Name
														</div>
														<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
															:  <?php echo $info->Enquiry_company;?>	
														</div>
													</div>
													<div class="row">
														<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
															Contact Person
														</div>
														<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
															:  <?php echo $info->Enquiry_prefix." ".$info->Enquiry_name;?>	
														</div>
													</div>
													<div class="row">
														<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
															Contact No
														</div>
														<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
															:  <?php echo $info->Enquiry_mobile;?>	
														</div>
													</div>
													<div class="row">
														<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
															Landline No
														</div>
														<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
															:  <?php echo $info->Enquiry_landline;?>	
														</div>
													</div>
													<div class="row">
														<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
															Email
														</div>
														<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
															:  <?php echo $info->Enquiry_email;?>	
														</div>
													</div>
													<div class="row">
														<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
															Remark
														</div>
														<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
															:  <?php echo $info->Enquiry_title;?>	
														</div>
													</div>
												</div>
												<div class="col-md-6" >
													<h3 class="form-section">Address Information</h3>
													<div class="row">
														<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
															Country
														</div>
														<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
															:  <?php echo 'India';?>	
														</div>
													</div>
													<div class="row">
														<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
															State
														</div>
														<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
															:  <?php echo $info->Enquiry_state;?>	
														</div>
													</div>
													<div class="row">
														<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
															City
														</div>
														<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
															:  <?php echo $info->Enquiry_city;?>	
														</div>
													</div>
													<div class="row">
														<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
															Address
														</div>
														<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
															:  <?php echo $info->Enquiry_address;?>	
														</div>
													</div>
													<div class="row">
														<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
															Zip Code
														</div>
														<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
															:  <?php echo $info->Enquiry_zipcode;?>	
														</div>
													</div>
													<h3 class="form-section">Billing Information</h3>
													<div class="row">
														<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
															Lead Source
														</div>
														<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
															:  <?php echo $info->Enquiry_source;?>	
														</div>
													</div>
													<div class="row">
														<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
															Billing City
														</div>
														<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
															:  <?php echo $info->Enquiry_city;?>	
														</div>
													</div>
												</div>
												<div class="col-md-12">
													<h3 class="form-section">Description Information</h3>
													<div class="row">
														<div class="col-md-3 name" style="font-size:16px; font-weight:normal">
															Enqiury Brief Requirement
														</div>
														<div class="col-md-9 value" style="font-size:16px; font-weight:bold">
															:  <?php echo $info->Enquiry_prepra_study_desc;?>	
														</div>
													</div>
												</div>
												<div class="col-md-12" >
													<hr/>
													<div class="col-md-6" >
														<h3 class="form-section">Proposal Information</h3>
														<div class="row">
															<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
																Proposal No
															</div>
															<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
																:  <?php echo $info->OPPORTUNITY_SNO;?>	
															</div>
														</div>
														<div class="row">
															<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
																Create Date
															</div>
															<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
																:  <?php echo $info->PROPOSAL_CREATE_DATE;?>	
															</div>
														</div>
													</div>
													<div class="col-md-6" >
														<h3 class="form-section">WorkOrder Information</h3>
														<div class="row">
															<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
																Proposal Submited Date
															</div>
															<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
																:  <?php echo $info->PROPOSAL_SUBMITED_DATE;?>	
															</div>
														</div>
														<div class="row">
															<div class="col-md-5 name" style="font-size:16px; font-weight:normal">
																WorkOrder Status
															</div>
															<div class="col-md-7 value" style="font-size:16px; font-weight:bold">
																:  <?php  if($info->PROPOSAL_STAGE=="Ordered") { echo $info->PROPOSAL_STAGE;}
																	else{ echo "Pending"; }
																?>	
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-12" >
													<hr/>
													<div class="row">
														
														<div class="col-md-3">
															<div class="form-group">
																<label class="control-label">Work Order Copy</label>
																<input type="text" value="" class="form-control input-medium" name="txtrevenue" id="txtrevenue" readonly>
															</div>
														</div>
														<div class="col-md-2">
															<div class="form-group">
																<label class="control-label">Work Order No.</label>
																<input type="text" placeholder="0.00" value="" class="form-control input-small" name="txtrevenue" id="txtrevenue">
															</div>
														</div>
														
														<div class="col-md-2">
															<div class="form-group">
																<label class="control-label">Work Order Date</label>
																<input type="text" placeholder="0%" value="" name="txtprobability" id="txtprobability" class="form-control input-small">
															</div>
														</div>
                                                		
                                                        <div class="col-md-2">
															<div class="form-group">
																<label class="control-label">Total Amount (Rs.)</label>
																<input type="text" class="form-control input-small input-inline numeric" name="txtuorder_amt"  placeholder="Amount (Rs.)" id="txtuorder_amt">
																<input type="text" class="form-control input-small input-inline numeric" name="txtuorder_tax"  placeholder="Tax (Rs.)" id="txtuorder_tax">
															</div>
														</div> 
														
													</div>
												</div>
											</div>
										</div>
									</div>
									<div id="function" class="form-actions middle">
										<button type="button" onclick="create_proposal(0)" class="btn blue"><i class="fa fa-check"></i> Save</button>
										<button type="button" onclick="create_proposal(1)" class="btn blue" >Save & Next</button>
										<button type="button" onclick="" class="btn default">Cancel</button>
										
									</div>
								</form>
								<!-- END FORM-->
								<?php
									break;
								}
							?>
						</div>
					</div>
				</div>		
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>
<!-- END CONTENT -->
</div>

<!-- END CONTAINER -->

<script>
	function create_proposal(id)
	{
		if(check())
		{
			if(confirm('Are you sure to Update Proposal?')==true)
			{
				$("#function").html("<div>Your Request has been processed.<img id='checkmark' src='<?php echo base_url(); ?>/application/libraries/assets/img/loading-spinner-blue.gif' /></div>");
				
				var leadownername =$('#txtldownerid').val();
				var companyname =$('#txtcname').val();
				var leadprefix =$('#ddprefix').val();
				var leadname =$('#txtleadname').val();
				var leadtitle =$('#txtltitle').val();
				var mobilenumber=$('#txtmobile').val();
				var emailid=$('#txtmail').val();
				var leadsource=$('#ddsource').val();
				var leadstatus=$('#ddldstatus').val();
				var industry=$('#ddindustry').val();
				var noofemployees=$('#txtnoemp').val();
				var country=$('#ddcountry').val();
				var state=$('#ddstate').val();
				var city = $('#ddcity').val();
				var street = $('#txtstreet').val();
				var txtzipcode = $('#txtzipcode').val();
				var leaddescription = $('#txtlead_des').val();
				$.ajax(
				{
					type: "POST",
					url:"<?php echo base_url();?>index.php/crm/insert_lead/<?php echo $id?>",
					data:"txtownername="+leadownername+"&txtcname="+companyname+"&txtleadprefix="+leadprefix+"&txtleadname="+leadname+"&txtltitle="+leadtitle+"&txtmobile="+mobilenumber+"&txtmail="+emailid+"&ddsource="+ leadsource+"&ddldstatus="+leadstatus+"&ddindustry="+industry+"&txtnoemp="+noofemployees+"&ddcountry="+country+"&ddstate="+state+"&ddcity="+city+"&txtstreet="+street+"&txtzipcode="+txtzipcode+"&txtlead_des="+leaddescription,
					success: function(msg) {
						if(msg=="true")
						{
							$(".page-content").html("<center><h2>Proposal upload Successfully!</h2></center>")
							.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/img/loading-spinner-blue.gif' /></center>")
							.hide()
							.fadeIn(1000,function()
							{
								if(id==0)
								{
									$("#stylized").load("<?php echo base_url().'index.php/crm/show_lead'?>");
								}
								else
								{
									$("#stylized").load("<?php echo base_url().'index.php/crm/add_lead/'.$id?>");
								}					
							}
							);
							
						}
						else
						{
							alert("Some Error on this page");
							
						}
						
					}
				});
			}
			else
			{
				return false;
			}
		}
		
	}
</script>
<script src="<?php echo base_url() ?>application/libraries/jquery.confirm/jquery.confirm.js"></script>
<script>
	function exit()
	{		
		if(confirm('Are you sure to Exit from Proposal?')==true)
		{
			$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/img/loading-spinner-blue.gif' /></center>");
			$("#stylized").load("<?php echo base_url().'index.php/crm/show_lead'?>");
		}
	}
</script>



<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			<label class="control-label">UPDESCO Manager</label>
			<select class="form-control" name="ddupdescomanager" id="ddupdescomanager">
				<option value="--Select--">Select</option>
				<option value="1">RK Gupta</option>
				<option value="2">HC Gupta</option>
				<option value="2">Nitin Mathur</option>
			</select>
			<span id="divddupdescomanager" style="color:red"></span>	
		</div>
	</div>
	
	<div class="col-md-3">
		<div class="form-group">
			<label class="control-label">Billing Cycle</label>
			<select class="form-control" name="ddubillingcycle" id="ddubillingcycle">
				<option value="--Select--">Select Billing Cycle</option>
				<option value="1">Annually</option>
				<option value="2">Half-Yearly</option>
				<option value="2">Quarterly</option>
				<option value="2">Maonthly</option>
			</select>
			<span id="divddubillingcycle" style="color:red"></span>	
		</div>
	</div>
	
</div>
