<!-- BEGIN CONTENT -->
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-badge"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_account/0">
							CRM
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-bubbles"></i>
						<a href="<?php echo base_url();?>crm/view_account/0">Account</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-plus"></i>
						<span>Create Account</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Create Account </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Create New Account </span>
							</div>
							
							<!-- <div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div> -->
						</div>
						<div class="portlet-body form">
							<form action="#" class="horizontal-form"  id="myform">
								
								
								<div class="form-body">
									<h4 class="caption-subject font-blue bold uppercase"> Basic Information </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Account Name</label>
												<input type="text" placeholder="Enter Account Name" class="form-control input-sm empty" name="txtacc_name" id="txtacc_name">
												<span id="divtxtacc_name" style="color:red"></span>	
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Account Owner</label>
												<input type="text" placeholder="Enter Account Ownername" class="form-control input-sm empty" name="txtacc_ownername" id="txtacc_ownername" value="<?php echo $this->session->userdata('name')?>" disabled>
												<span id="divtxtacc_ownername" style="color:red"></span>	
												<input type="hidden" class="form-control input-sm" name="txtacc_owner" id="txtacc_owner" value="<?php echo $this->session->userdata('profile_id')?>">
											</div>
										</div>
										
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Account Type</label>
												<select class="form-control input-sm opt" name="ddacc_type" id="ddacc_type">
													<option value="">Select Account Type</option>
													<?php
														foreach($type->result() as $row)
														{
														?>
														<option value="<?php echo $row->m_des_id;?>"><?php echo $row->m_des_name; ?></option>
														<?php
														}
													?>
												</select>	
												<span id="divddacc_type" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Industry</label>
												<select id="ddindustry" name="ddindustry" class="form-control input-sm empty" onChange="fill_city()">
													<option value="0"> Select Industry</option>
													<?php
														foreach($ind->result() as $row)
														{
														?>
														<option value="<?php echo $row->industry_id; ?>"><?php echo $row->industry_name; ?></option>
														<?php
														}
													?>
												</select>
												<span id="divddindustry" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Employees
												</label>
												<input type="text" placeholder="Enter Employees"name="txtemp" id="txtemp" class="form-control input-sm empty">
												<span id="divtxtemp" style="color:red"></span>
											</div>
										</div>
										
									</div>
									
									<!--row-->
									
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Website
												</label>
												<input type="text" placeholder="Enter Website"name="txtacc_website" id="txtacc_website" class="form-control input-sm empty">	
												<span id="divtxtacc_website" style="color:red"></span>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Contact Number
												</label>
												<input type="text" placeholder="Enter Mobile Number" name="txtcontact" id="txtcontact" class="form-control input-sm empty" maxlength=10>
												<span id="divtxtcontact" style="color:red"></span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">
													Email Address
													
												</label>
												<input type="text"placeholder="Enter Email Address" name="txtemail" id="txtemail" class="form-control input-sm empty">
												<span id="divtxtemail" style="color:red"></span>
											</div>
											
										</div>
										<!--/span-->
										
									</div>
									
									
									<!--row-->
									
									
									<div class="row">
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Fax</label>
												<input type="text"placeholder="Enter Fax Number" class="form-control input-sm empty" name="txtfax" id="txtfax">
												<span id="divtxtfax" style="color:red"></span>
											</div>
										</div>
										
										<!--/span-->
									</div>
									<h4 class="caption-subject font-blue bold uppercase"> Billing Address Information </h4>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Country
													<span class="required">
														*
													</span>
												</label>
												<select class="form-control input-sm opt" name="ddcountry" id="ddcountry" onchange="get_state()">
													<option value="-2">Select Country</option>
													<?php
														foreach($loc->result() as $row)
														{
														?>
														<option value="<?php echo $row->m_loc_id?>"><?php echo $row->m_loc_name; ?></option>
														<?php
														}
													?>
												</select>       
												<span id="divddcountry" style="color:red"></span>
											</div>
											</div> <div class="col-md-4">
											<div class="form-group">
												<label class="control-label">State
												</label>
												<div id="state">
													<select class="form-control input-sm opt" name="ddstate" id="ddstate" onchange="get_city()">
														<option value="">Select State</option>
														
													</select>
													<span id="divddstate" style="color:red"></span>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">City </label>
												<div id="city">
													<select class="form-control input-sm opt" name="ddcity" id="ddcity">
														<option value="">Select City</option>
														
													</select>
													<span id="divddcity" style="color:red"></span>
												</div>
											</div>
										</div></div>
										<!--/span-->
										
										<div class="row">
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Street
														<span class="required">
															*
														</span>
													</label>
													<input type="text" class="form-control input-sm " PLACEHOLDER="Enter Street/Locality" name="txtstreet" id="txtstreet">				
													<span id="divtxtstreet" style="color:red"></span>
												</div>
											</div>
											
											<!--row-->
											
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Zip Code
														<span class="required">
															*
														</span>
													</label>
													<input type="text" class="form-control input-sm " PLACEHOLDER="Enter Zipcode" name="txtzipcode" id="txtzipcode">	
													<span id="divtxtzipcode" style="color:red"></span>
												</div>
											</div></div>
											<!--/span-->
											
											<!--row-->
											<h4 class="caption-subject font-blue bold uppercase">Description Information </h4>
											<div class="row">
												<div class="col-md-8">
													<div class="form-group">
														<label class="control-label">Description
														</label>
														<textarea class="form-control input-sm " PLACEHOLDER="Enter Account Description Information" name="txtacc_des" id="txtacc_des" style="font-size:20px" col="3"></textarea>		
														<span id="divtxtacc_des" style="color:red"></span>
													</div>
												</div></div>
												
								</div>
								<div id="function" class="form-actions middle">
									<button type="button" onclick="create_account(0)" class="btn green"><i class="fa fa-check"></i> Save</button>
									<button type="button" onclick="create_account(1)" class="btn green" >Save & Next</button>
									<button type="reset" onclick="exit()" class="btn default">Cancel</button>
									
								</div>
							</form>
							<!-- END FORM--> 
							
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->		

<script>
	function get_state()
	{
		var country_id=$("#ddcountry").val();
		$("#ddstate").empty();
		if(country_id!='-1' && country_id!='')
		{
			$.blockUI({ message: '<h3>Please Wait...</h3>' });
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/crm/select_state/"+country_id+"/0",
				dataType: "JSON",
				data:{'country_id':country_id},
				success:function(data){
					setTimeout($.unblockUI, 01);
					$("#ddstate").append("<option value=-1> ~~ Select ~~</option>");
					$.each(data,function(i,item)
					{
						$("#ddstate").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
					});
				}
			});
		}
		else
		{
			$("#ddstate").append("<option value=-1> ~~ Select ~~</option>");
			alert("Please First Select The Country");
		}
		
	}
</script>
<script>
	function get_city()
	{
		var state_id=$("#ddstate").val();
		$("#ddcity").empty();
		if(state_id!='-1' && state_id!='')
		{
			$.blockUI({ message: '<h3>Please Wait...</h3>' });
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/crm/select_city/"+state_id+"/0",
				dataType: "JSON",
				data:{'state_id':state_id},
				success:function(data){
					setTimeout($.unblockUI, 01);
					$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
					$.each(data,function(i,item)
					{
						$("#ddcity").append("<option value="+item.m_loc_id+">"+item.m_loc_name+"</option>");
					});
				}
			});
		}
		else
		{
			$("#ddcity").append("<option value=-1> ~~ Select ~~</option>");
			alert("Please First Select The State");
		}
	}
</script>
<script>
	/*fill_userid();*/
	function fill_userid()
	{
		var user_id=<?php echo $this->session->userdata('profile_id')?>;
		if(user_id!="0")
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url();?>index.php/master/validateUser/1/",
				data:"txtintuserid="+user_id,
				success: function(msg) {
					if(msg!="false")
					{
						document.getElementById("txtacc_ownername").value=msg.trim();  
					}
					else
					{
						document.getElementById("txtacc_ownername").value=msg.trim();                   
						alert('No User in this Id');
					}
				}
			}
			)
			
		}
		else
		{
			document.getElementById("txtacc_ownername").value="SuperAdmin";                    
		}	
	}
</script>
<script>
	function exit()
	{
		if(confirm('Are you sure to Exit from Create Account?')==true)
		{
			$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
			$("#stylized").load("<?php echo base_url().'index.php/crm/show_account/'.$id?>");
		}
	}
</script>
<script>
	function create_account(sid)
	{
		
		if(check("myform"))
		{
			bootbox.confirm('Are you sure to Submit form?', function(result){
				if(result==true)
				{
					$("#function").html("<div>Your Request has been processed.<img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></div>");
					var accownername = $('#txtacc_owner').val();
					var accountname = $('#txtacc_name').val();
					var accounttype= $('#ddacc_type').val();
					var ownership =<?php echo $this->session->userdata('profile_id'); ?>;
					var accountindustry = $('#ddindustry').val();
					var employees = $('#txtemp').val();
					var accountwebsite = $('#txtacc_website').val();
					var contactnumber = $('#txtcontact').val();
					var emailaddress = $('#txtemail').val();
					var fax = $('#txtfax').val();
					var country = $('#ddcountry').val();
					var state = $('#ddstate').val();
					var city = $('#ddcity').val();
					var street = $('#txtstreet').val();
					var zipcode = $('#txtzipcode').val();
					var accdesc= $('#txtacc_des').val();
					$.ajax(
					{
						type: "POST",
						url: "<?php echo base_url().'index.php/crm/insert_account/'.$id?>",
						data:"txtacc_owner="+accownername+"&txtacc_name="+accountname+"&ddacc_type="+accounttype+"&ddacc_own="+ownership+"&ddindustry="+accountindustry+"&txtemp="+employees+"&txtacc_website="+accountwebsite+"&txtcontact="+contactnumber+"&txtemail="+emailaddress+"&txtfax="+fax+"&ddcountry="+country+"&ddstate="+state+"&ddcity="+city+"&txtstreet="+street+"&txtzipcode="+zipcode+"&txtacc_des="+accdesc,
						success: function(msg) 
						{
							//$(".page-content").html(msg);
							if(msg.trim()!="")
							{
								$(".page-content").html("<center><h2>Add Account Successfully!</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(1000,function()
								{
									if(sid==0)
									{
										$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
										location.reload();    //$("#stylized").load("<?php echo base_url().'index.php/crm/show_account/'.$id.'/'?>");  //changed to location.reload();
									}
									if(sid==1)
									{
										$("#stylized").html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
										$("#stylized").load("<?php echo base_url().'index.php/crm/add_account/'.$id.'/'?>");
									}			
								}
								);
							}
							else
							{
								alert("Some Error on this page");
							}
						}
					});
				}
			}); 
		}
		else
		{
			return false;
		}
	}
</script>
</div>					
<script src="<?php echo base_url() ?>application/libraries/js/check.js"></script>													