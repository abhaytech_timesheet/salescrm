
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-folder"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/create_sms_campaign">
							Campaign
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-calendar"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_create_email_template">Create Campaign</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-envelope-open"></i>
						<span> Email Campaign</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Multiple Template for Email</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-envelope-open font-black"></i>
								<span class="caption-subject font-black bold uppercase">Add Email Template</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form method="post" id="myform" action="<?php echo base_url('Campaign/insert_email_temp');?>">
								<div class="form-body">
									
									<div id="content_email">
										<div id="content_email_temp">
											
											<div class="form-group">                                     
												<label>Subject of Email</label>
												<input type="text" class="form-control input-sm empty" id="txttitle" name="txttitle" placeholder="Enter text"/>
												<span id="divtxttitle" style="color:red"></span>
											</div>
											<div class="form-group">
												<label>Category Of Email</label>
												<select class="form-control input-sm opt" name="category" id="category">
													<option value="-1">Select</option>
													<?php
														foreach($cate->result() as $row)
														{
														?>
														<option value="<?php echo $row->m_email_id; ?>"><?php echo $row->m_email_name; ?></option>
														<?php
														}
													?>
												</select>
												<span id="divcategory" style="color:red"></span>
											</div>
											<div style="clear:both"></div>                                  
											<div class="form-group">
                                                <label>Message</label>
												<div>
													<textarea class="ckeditor form-control" name="editor1"  id="editor1" rows="6"></textarea>
                                                    
													
													<span id="diveditor1" style="color:red"></span>
												</div>
                                                <div class="inbox-body">
													<div class="inbox-form-group">
													</div></div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" onclick="return check('myform')" class="btn green"><i class="fa fa-check"></i> Submit</button>								
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				
				
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="glyphicon glyphicon-envelope font-dark"></i>
								<span class="caption-subject bold uppercase"></span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th > Sr.No</th>
										<th > Category  </th>
										<th > Email Title </th>
										<th > Action </th>
										<th > Status </th>
									</tr>
								</thead>
								<tbody>
									<?php
										$serial = 1;
                                        $c='default';
										foreach($content->result() as $row)
										{
											if($row->m_status == 'Active') $c= 'primary';
											if($row->m_status == 'Deactive') $c= 'default';
											if($row->m_status == 'Pending') $c= 'warning';
											if($row->m_status == 'Reject') $c= 'danger';	  
										?>
										<tr>
											<td><?php echo $serial; ?></td>
											<td>
												<?php
													foreach($cate->result() as $row12)
													{
														if($row->category==$row12->m_email_id)
														{
															echo $row12->m_email_name;
														}
													}
												?>
											</td>
											<td><?php echo $row->m_email_temp_title ?></td>
											<td>
												<a href="<?php echo base_url(); ?>index.php/campaign/view_edit_email/<?php echo $row->m_email_temp_id ?>" class="label label-sm label-success"><i class="fa  fa-edit"></i> Edit Message</a>
												 / &nbsp; <?php 
													if($row->m_status == 'Active'){?>
													<a href="<?php echo base_url()?>index.php/campaign/update_email_template_status/<?php echo $row->m_email_temp_id?>/Deactive" class="label label-sm label-default" >Deactive</a>
													<?php }else{?>
													<a href="<?php echo base_url()?>index.php/campaign/update_email_template_status/<?php echo $row->m_email_temp_id?>/Active" class="label label-sm label-primary" >Active</a>
													<?php if($row->m_status == 'Pending'){?>
														<a href="<?php echo base_url()?>index.php/campaign/update_email_template_status/<?php echo $row->m_email_temp_id?>/Reject" class="label label-sm label-danger" >Reject</a>
													<?php } ?>
													
												<?php }?>
											</td>
											
											<td> <a href="javascript:" class="label label-sm label-<?php echo $c?>" ><?php echo $row->m_status?></a></td>
											
											
										</tr>
										<?php
											$serial++;
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->			



