<div id="stylized"> 
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper"> 
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content"> 
			<!-- BEGIN PAGE HEADER--> 
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li> <i class="icon-folder"></i> <a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/create_campaign"> Campaign </a> <i class="fa fa-angle-right"></i> </li>
					<li> <i class="icon-action-redo"></i> <span>Campaign Users</span> <i class="fa fa-angle-right"></i> </li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range"> <i class="icon-calendar"></i>&nbsp; <span class="thin uppercase hidden-xs"></span>&nbsp; <i class="fa fa-angle-down"></i> </div>
				</div>
			</div>
			<!-- END PAGE BAR --> 
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">New Campaign users</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption"> <i class="icon-action-redo font-black"></i> <span class="caption-subject font-black bold uppercase">
								<?php if($this->uri->segment(3)) echo "Update";else echo "New";?>
							users</span> </div>
							<div class="actions"> <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;"> <i class="icon-cloud-upload"></i> </a> <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;"> <i class="icon-wrench"></i> </a> <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a> <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;"> <i class="icon-trash"></i> </a> </div>
						</div>
						<div class="portlet-body form">
							<?php if($this->uri->segment(3)){?>
								
								<form role="form" class="form-horizontal" method="post" id="campaign" action="<?php echo base_url();?>campaign/update_campaign_user/<?php echo $this->uri->segment(3)?>">
									<div class="form-body">
										<div class="form-group">
											
												<div class="form-group">
													<label class="col-md-3 control-label">API Name</label>
													<div class="col-md-6">
														<div id="contact">
															<select name="api_name" id="api_name"  class="form-control input-sm opt">
																<option value="0">Select Api URL</option>
																<?php foreach($api_links->result() as $value){?>
																	<option value="<?php echo $value->m_api_id?>"<?php if($recs->api_id == $value->m_api_id) echo 'selected';?>><?php echo $value->m_api_name?></option>
																<?php }?>
															</select>
														</div>
													<span id="divuser_name" style="color:red;"></span> </div>
												</div>
												
												<div class="form-group">
													<label class="col-md-3 control-label">Select owner</label>
													<div class="col-md-6">
														<div id="contact">
															<select name="owner" id="owner" class="form-control input-sm opt">
																<option value="0">Select Owner</option>
																<option value="1"> 1Owner</option>
																<option value="2"> 2Owner</option>
																<option value="3"> 3Owner</option>
															</select>
														</div>
														<span id="divapi_name" style="color:red;"></span> 
													</div>
												</div>
											
											<label class="col-md-3 control-label">Sender Id</label>
											<div class="col-md-6">
												<div id="contact">
													<input type="text" class="form-control input-sm empty" name="sender_id" id="sender_id" value="<?php echo $recs->sender_id?>">
													
												</div>
												<span style="font-size:10px;">Note:Sender Id is max 6 charectors</span> <br>
											<span id="divsender_id" style="color:red;"> </span> </div>
										</div>
										
										
										
										<div class="form-group">
											<label class="col-md-3 control-label">User Name </label>
											<div class="col-md-6">
												<div id="contact">
													<input type="text" class="form-control input-sm empty" name="user_name" id="user_name" value="<?php echo $recs->user_name?>">
												</div>
											<span id="divuser_name" style="color:red;"></span> </div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">User Password</label>
											<div class="col-md-6">
												<div id="contact">
													<input type="password" class="form-control input-sm empty" name="user_pwd" id="user_pwd" value="<?php echo $recs->user_pwd?>">
												</div>
											<span id="divuser_pwd" style="color:red;"></span> </div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Status</label>
											<div class="col-md-6">
												<div id="contact">
													<select name="status" id="status" class="form-control input-sm opt">
														<option value="0">Select Status</option>
														<option value="Active" <?php if($recs->status == 'Active')echo 'selected'?>>Active</option>
														<option value="Deactive" <?php if($recs->status == 'Deactive')echo 'selected'?>>Deactive</option>
													</select>
												</div>
											<span id="divstatus" style="color:red;"></span> </div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Type </label>
											<div class="col-md-6">
												<div id="contact">
													<select name="type" id="type" class="form-control input-sm opt" onChange="valiate_users()">
														<option value="0" >Select Type</option>
														<option value="Email" <?php if($recs->type == 'Email')echo 'selected'?>>Email</option>
														<option value="SMS" <?php if($recs->type == 'SMS')echo 'selected'?>>SMS</option>
													</select>
												</div>
											<span id="divtype" style="color:red;"></span> </div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Description </label>
											<div class="col-md-6">
												<div id="contact">
													<textarea class="form-control empty" name="description"><?=$recs->description?></textarea>
												</div>
											<span id="divtxtcontact" style="color:red;"></span> </div>
										</div>
									</div>
									
									<div class="form-actions fluid" >
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green" onclick="return check('campaign')">Submit</button>
											<button type="reset" class="btn default">Cancel</button>
										</div>
									</div>
								</form>
								<?php }else{ ?>
									
									<form role="form" class="form-horizontal" method="post" id="campaign" action="<?php echo base_url();?>campaign/creat_new_user">
										<div class="form-body">
											<div class="form-group">
												<label class="col-md-3 control-label">API Name</label>
												<div class="col-md-6">
													<div id="contact">
														<select name="api_name" id="api_name" class="form-control input-sm opt">
															<option value="0">select Api</option>
															<?php foreach($api_links->result() as $value){?>
																<option value="<?php echo $value->m_api_id?>"><?php echo $value->m_api_name?></option>
															<?php }?>
														</select>
													</div>
													<span id="divapi_name" style="color:red;"></span> 
												</div>
											</div>
											
											<div class="form-group">
												<label class="col-md-3 control-label">Select owner</label>
												<div class="col-md-6">
													<div id="contact">
														<select name="owner" id="owner" class="form-control input-sm opt">
															<option value="0">Select Owner</option>
															<option value="1"> 1Owner</option>
															<option value="2"> 2Owner</option>
															<option value="3"> 3Owner</option>
														</select>
													</div>
													<span id="divapi_name" style="color:red;"></span> 
												</div>
											</div>
											
											<div class="form-group">
												<label class="col-md-3 control-label">Sender Id</label>
												<div class="col-md-6">
													<div id="contact">
														<input type="text" class="form-control input-sm empty" name="sender_id" id="sender_id">
														
													</div>
													<span style="font-size:10px;">Note:Sender Id is max 6 charectors</span> <br>
												<span id="divsender_id" style="color:red;"> </span> </div>
											</div>
											
											<div class="form-group">
												<label class="col-md-3 control-label">User Name </label>
												<div class="col-md-6">
													<div id="contact">
														<input type="text" class="form-control input-sm empty" name="user_name" id="user_name">
													</div>
												<span id="divuser_name" style="color:red;"></span> </div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">User Password</label>
												<div class="col-md-6">
													<div id="contact">
														<input type="password" class="form-control input-sm empty" name="user_pwd" id="user_pwd">
													</div>
												<span id="divuser_pwd" style="color:red;"></span> </div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Status</label>
												<div class="col-md-6">
													<div id="contact">
														<select name="status" id="status" class="form-control input-sm opt">
															<option value="0">Select Status</option>
															<option value="Active">Active</option>
															<option value="Deactive">Deactive</option>
														</select>
													</div>
												<span id="divstatus" style="color:red;"></span> </div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Type </label>
												<div class="col-md-6">
													<div id="contact">
														<select name="type" id="type" class="form-control input-sm opt" onChange="valiate_users()">
															<option value="0">Select Type</option>
															<option value="Email">Email</option>
															<option value="SMS">SMS</option>
														</select>
													</div>
												<span id="divtype" style="color:red;"></span> </div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label">Description </label>
												<div class="col-md-6">
													<div id="contact">
														<textarea class="form-control empty" name="description"></textarea>
													</div>
												<span id="divtxtcontact" style="color:red;"></span> </div>
											</div>
										</div>
										
										<div class="form-actions fluid" >
											<div class="col-md-offset-3 col-md-9">
												<button type="submit" class="btn green" onclick="return check('campaign')">Submit</button>
												<button type="reset" class="btn default">Cancel</button>
											</div>
										</div>
									</form>
								<?php  }?>
								
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark"> <i class="icon-action-redo font-dark"></i> <span class="caption-subject bold uppercase"></span> </div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>Sr.No</th>
										<th>Action</th>                   
										<th>Type</th>
										<th>User Name</th>
										<th>Sender Id</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$serial = 1;
										foreach($rec->result() as $row)
										{
											$cc ='';	
											if($row->status == 'Active'){ $c= 'primary'; $cc = 'default';$st = 'Deactive';}
											if($row->status == 'Deactive') {$c= 'default';$cc = 'primary'; $st = 'Active';}
											
										?>
										<tr class="odd gradeX">
											<td><?php echo $serial; ?></td>
											<td><a href="<?php echo base_url();?>Campaign/campaign_users/<?php echo $row->id?>"><span class="label label-sm label-success"> EDITE </span></a></td>
											
											<td><?php echo $row->type ?></td>
											<td><?php echo $row->user_name ?></td>
											<td><?php echo $row->sender_id ?></td>
											<td><a href="#"><span class="label label-sm label-<?php echo $c;?>"> <?php echo $row->status ?> </span></a></td>
											<td>
											<a href="<?php echo base_url();?>Campaign/update_campaign_users_status/<?php echo $row->id?>/<?php echo $row->status?>"><span class="label label-sm label-<?php echo $cc;?>"> <?php echo $st ?> </span></a></td>
										</tr>
										<?php
											$serial++;
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY --> 
	</div>
</div>
<!-- END CONTENT --> 

<script>
	
	
	function valiate_users()
	{ 
		if($('#sender_id').val().length >= 7)
		{
			$('#sender_id').val("");
			$('#divsender_id').html("Sender id less then 6 charectors")  
		}
		else
		{
			var user_name = $('#user_name').val();
			var type = $('#type').val();
			var sender_id = $('#sender_id').val();
			if (user_name != '' && type != '' && sender_id !='' )
			{ 
				$.ajax(
				{
					type:"POST",
					url:'<?php echo base_url()?>crm/validate_existing_user',
					data: {'user_name': user_name,'type':type,'sender_id':sender_id},
					success: function(data)
					{
						if (data.trim() == 1)
						{   
							$('#user_name').val("");
							$('#divuser_name').html("change Your user name  or sender id");
							$('#sender_id').val("");
							
							$('#divsender_id').html("change Your sender id or user name ");
							
						}
						console.log(data);
					}
				});
			}
		}
		
	}
	
</script> 
 