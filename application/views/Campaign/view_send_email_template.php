<?php
	$temp="";
	if($info!='')
	{
		foreach($info->result() as $row123)
		{
			$temp=$row123->m_email_temp_description;
		}
	}
?>
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="icon-folder"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/create_sms_campaign">
							Campaign
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-rocket"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_send_email_template"> Send Email Campaign</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-action-redo"></i>
						<span> Send SMS Campaign</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">  Send Email Campaign</h3>
			<!-- END PAGE TITLE-->
			<div class="inbox">
				<div class="row">
					<div class="col-md-2">
						<div class="inbox-sidebar">
							<a href="<?php echo base_url() ?>index.php/crm/view_send_email_template" data-title="Compose" class="btn red compose-btn btn-block">
							<i class="fa fa-edit"></i> Compose </a>
							<h3>Categories</h3>
							<ul class="inbox-nav">
								<?php 
									foreach($cate->result() as $row)
									{
									?>
									<li class="<?php echo $row->m_email_name; ?>" id="inbox<?php echo $row->m_email_id; ?>">
										<a href="javascript:;" onclick="follow('<?php echo $row->m_email_id; ?>');" data-title="<?php echo $row->m_email_name; ?>" data-type="<?php echo $row->m_email_name; ?>"> <?php echo $row->m_email_name; ?></a>
									</li>
									<?php 
									}
								?>
							</ul>
						</div>
					</div>
					
					
					<div class="col-md-10">
						<div class="inbox-body">
							<div class="inbox-header">
								<h1 class="pull-left">Compose</h1>
							</div>
							<div class="inbox-content" id="content_email">
								<form class="inbox-compose form-horizontal" id="fileupload" action="<?php echo base_url(); ?>index.php/campaign/send_template_mail/<?php echo $this->uri->segment(3)?>" method="POST" enctype="multipart/form-data">
									
									<div class="inbox-compose-btn">
										<input class="btn green " type="submit" name="Send" value="Send" onclick="return  check('fileupload')">
										<span class="btn yellow" onclick="get_details()">Get Email Id</span>
										<a href="<?php echo base_url() ?>index.php/campaign/view_send_email_template">
											<span class="btn red">Discard</span>
										</a>
									</div>
									
									<div class="inbox-form-group mail-to">
										<label class="control-label">Source:</label>
										<div class="controls">
											<select id="ddsource" name="ddsource" class="form-control input-sm opt" onchange="enable()">
												<option value="-1" selected="selected">Select</option>
												<option value="1">Lead</option>
												<option value="2">Contact</option>
											</select>
											<span id="divddsource" class="help-inline" style="color:red"></span>
										</div>
									</div>
									
									<div class="inbox-form-group input-cc">
										<label class="control-label">Category:</label>
										<div class="controls">
											<select id="ddcategory" name="ddcategory" class="form-control input-sm opt" disabled="disabled">
												<option value="1">Account</option>
											</select> 
                                            <span id="divddcategory" class="help-inline" style="color:red"></span> 
										</div>
									</div>
									
									<div class="inbox-form-group input-bcc">
										<label class="control-label">To:</label>
										<div class="controls">
											<div id="contact">
												<input type="text" class="form-control input-sm empty" name="txtto" id="txtto"><span id="divtxtto" class="help-inline" style="color:red"></span>
											</div>
										</div>
									</div>
									
									<div class="inbox-form-group">
										<label class="control-label">Subject:</label>
										<div class="controls">
											<div>
												<input type="text" class="form-control input-sm empty" name="txtsubject" id="txtsubject"><span id="divtxtsubject" class="help-inline" style="color:red"></span>
											</div>
										</div>
									</div>
									
									<div class="inbox-form-group">
										<textarea class="ckeditor form-control" name="txtdesciption" id="txtdesciption" rows="12"><?php if($temp!=""){echo $temp;} ?></textarea>
									</div>
									<span id="divtxtdesciption" class="help-inline" style="color:red"></span>
									<div class="inbox-compose-btn">
										<input class="btn green" type="submit" name="Send" value="Send" onclick=" return check('fileupload')">
										<span class="btn yellow" onclick="get_details()">Get Email Id</span>
										<a href="<?php echo base_url() ?>index.php/campaign/view_send_email_template">
											<span class="btn red">Discard</span>
										</a>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>	
		
	</div>
	<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<script>
	function follow(id)
	{	
		for(i=0; i<8; i++)
		{
			$("#inbox"+i).removeAttr("class","active");
		}
		$("#mail").removeAttr("class","active");
		$("#inbox"+id).attr("class","active");
		var flg=id;
		$("#content_email").html('<div><img src ="<?php echo base_url();?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
		$('#content_email').load("<?php echo base_url();?>index.php/campaign/email_template_display/"+flg);
	}
</script>
<script>
	function enable()
	{
		var sour=$('#ddsource').val();
		var cate=$('#ddcategory').val();
		if(sour==2)
		{
			$('#ddcategory').removeAttr("disabled");	
		}
		else
		{
			$('#ddcategory').attr( "disabled", "disabled" );
		}
	}
</script>
<script>
	function get_details()
	{
		
			var sour=$('#ddsource').val();
			var cate=$('#ddcategory').val();
			$("#contact").html('<div><img src ="<?php echo base_url();?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
			$("#contact").load("<?php echo base_url() ?>index.php/campaign/get_contact_email/"+sour+"/"+cate);
		
	}
</script>
<script>
	function send_mail()
	{
		var from=$('#txtfrom').val();
		var to=$('#txtto').val();
		var subject=$('#txtsubject').val();
		var description=$('#txtdescription').val();
		$.ajax(
		{
			type: "POST",
			url:"<?php echo base_url();?>index.php/campaign/send_template_mail/",
			data:"txtfrom="+from+"&txtto="+to+"&txtsubject="+subject+"&txtdescription="+description,
			success: function(msg) {
				if(msg="true")
				{
					$(".page-content").html("<center><h2>Template Send Successfully!</h2></center>")
					.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
					.hide()
					.fadeIn(1000,function()
					{
						$("#stylized").load("<?php echo base_url().'index.php/campaign/after_view_email_template'?>");
					}
					);
					
				}	  
			}
		});
		
	}
</script>				