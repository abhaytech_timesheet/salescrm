<link href="<?php echo base_url() ?>application/libraries/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css"/>
<div id="content_email_temp">
                    <div class="scroller" style="height: 500px;" data-always-visible="1" data-rail-visible="0">											
                       			<div class="form-group">
										<label>Title <?php echo $id; ?></label>
										<input type="text" class="form-control input-large" id="txttitle" name="txttitle" placeholder="Enter text" value="" />
								</div>
                                <div class="form-group"><div >
								<textarea class="ckeditor form-control" name="editor1"  id="editor1" rows="6"></textarea>
								</div>
								</div>
								<div class="form-actions">
								<button type="button" onclick="back_tab()" name="back" class="btn default"><i class="fa fa-angle-left"></i> Back</button>
								<button  onclick="update()" class="btn green"><i class="fa fa-check"></i> Save</button>
								<button  onclick="next_update()" class="btn green"><i class="fa fa-check-circle"></i> Save & Continue Edit</button>									
								</div>
					</div>
                    </div>
	<script>
jQuery(document).ready(function() {       
   // initiate layout and plugins
   App.init();
   
});
</script>