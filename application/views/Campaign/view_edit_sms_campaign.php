<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="#.">SALES BOOSTER</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Campaign</span>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> SMS Template</h3>
			<!-- END PAGE TITLE-->
			<?php
				foreach($info->result() as $row)
				{
				}
			?>
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-black"></i>
								<span class="caption-subject font-black bold uppercase">Edit SMS Template</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<?php echo form_open('campaign/update_sms_template/'.$this->uri->segment(3), 'method="post"  id="form_sample_1" ');?>
							<div id="content_email">
								<div id="content_email_temp">
									<div class="form-group">                                     
										<label>Subject of SMS</label>
										<input type="text" class="form-control input-sm empty" id="txttitle" name="txttitle" placeholder="Enter text" value="<?php echo $row->m_email_temp_title; ?>"/>
										<span id="divtxttitle" style="color:red;"></span>
									</div>
									<div class="form-group">
										<label>Category Of SMS</label>
										<select class="form-control input-sm opt" name="ddcategory" id="ddcategory">
											<option value="-1">Select</option>
											<?php
												foreach($cate->result() as $row1)
												{
													if($row->category==$row1->m_email_id)
													{
													?>
													<option value="<?php echo $row1->m_email_id; ?>" selected="selected"><?php echo $row1->m_email_name; ?></option>
													<?php
													}
													else
													{
													?>
													<option value="<?php echo $row1->m_email_id; ?>"><?php echo $row1->m_email_name; ?></option>
													<?php
													}
													
												}
											?>
										</select>
										<span id="divddcategory" style="color:red;"></span>
									</div>
									<div style="clear:both"></div>                                  
									<div class="form-group">
										<div>
											<label>Content Of SMS</label>
											<textarea class="form-control input-sm empty" name="txtdescription"  id="txtdescription" rows="6" data-provide="markdown" data-error-container="#editor_error"><?php echo $row->m_email_temp_description; ?></textarea>
											<span id="divtxtdescription" style="color:red;"></span>
										</div>
									</div>
								</div>
							</div>
							
							<div class="form-actions fluid">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" onclick="return check('form_sample_1')" class="btn green"><i class="fa fa-check"></i> Save</button>									
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			
			
			
		</div>
		
	</div>
	<!-- END CONTENT BODY -->
</div>
</div>
<!-- END CONTENT -->													

