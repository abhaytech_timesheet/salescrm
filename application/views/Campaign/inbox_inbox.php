<table class="table table-striped table-advance table-hover">
	<tbody>
		<?php 
			foreach($content->result() as $row)
			{ 
			?>
			<tr class="unread" data-messageid="1">
				
				<td class="inbox-small-cells">
					<i class="fa fa-star"></i>
				</td>
				<td class="view-message hidden-xs">
					<?php echo $row->m_email_temp_title; ?>
				</td>
				
				<td class="view-message hidden-xs">
					<?php echo substr($row->m_email_temp_description,0,30); ?>
				</td>
				
				<td class="view-message ">
					<?php
						foreach($cate->result() as $row1)
						{
							if($row1->m_email_id==$row->category)
							{
								echo $row1->m_email_name; 
							}
						}
					?>
				</td>
				<td align="right">
					<a href="<?php echo base_url(); ?>index.php/campaign/get_template/<?php echo $row->m_email_temp_id ?>">
						<div class="btn-group">
							<button class="btn blue reply-btn">
								<i class="fa fa-reply"></i> Forword
							</button>
						</div>
					</a>
				</td>
				
			</tr>
		<?php } ?>
	</tbody>
</table>