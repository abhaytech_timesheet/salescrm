<div id="stylized"> 
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper"> 
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content"> 
			<!-- BEGIN PAGE HEADER--> 
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li> <i class="icon-folder"></i> <a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/create_sms_campaign"> Campaign </a> <i class="fa fa-angle-right"></i> </li>
					<li> <i class="icon-calendar"></i> <a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/create_sms_campaign">Create Campaign</a> <i class="fa fa-angle-right"></i> </li>
					<li> <i class="icon-screen-tablet"></i> <span> SMS Campaign</span> <i class="fa fa-angle-right"></i> </li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range"> <i class="icon-calendar"></i>&nbsp; <span class="thin uppercase hidden-xs"></span>&nbsp; <i class="fa fa-angle-down"></i> </div>
				</div>
			</div>
			<!-- END PAGE BAR --> 
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> SMS Template</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption"> <i class="icon-cursor font-black"></i> <span class="caption-subject font-black bold uppercase">Add SMS Template</span> </div>
							<div class="actions"> <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;"> <i class="icon-cloud-upload"></i> </a> <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;"> <i class="icon-wrench"></i> </a> <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a> <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;"> <i class="icon-trash"></i> </a> </div>
						</div>
						
						<div class="portlet-body form">
							<?php echo form_open('Campaign/insert_sms_temp', 'method="post"  id="form_sample_1" ');?>
							<div id="content_email">
								<div id="content_email_temp">
									<div class="form-group">
										<label>Subject of SMS</label>
										<input type="text" class="form-control input-sm empty" id="title" name="title" placeholder="Enter text"/>
									<span id="divtitle" style="color:red;"></span> </div>
									<div class="form-group">
										<label>Category Of SMS</label>
										<select class="form-control input-sm opt" name="category" id="category">
											<option value="-1">Select</option>
											<?php
												foreach($cate->result() as $row)
												{
												?>
												<option value="<?php echo $row->m_email_id; ?>"><?php echo $row->m_email_name; ?></option>
												<?php
												}
											?>
										</select>
									<span id="divcategory" style="color:red;"></span> </div>
									<div style="clear:both"></div>
									<div class="form-group">
										<div>
											<label>Content Of SMS</label>
											<textarea class="form-control input-sm empty" name="content"  id="content" rows="6" data-provide="markdown" data-error-container="#editor_error"></textarea>
										<span id="divcontent" style="color:red;"></span> </div>
									</div>
								</div>
							</div>
							<div class="form-actions fluid">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" onclick="return check('form_sample_1')" class="btn green"><i class="fa fa-check"></i> Save</button>
								</div>
							</div>
						</form>
					</div>
					
				</div>
			</div>
			<div class="col-md-6">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption font-dark"> <i class="icon-screen-tablet font-dark"></i> <span class="caption-subject bold uppercase"></span> </div>
						<div class="tools"> </div>
					</div>
					<div class="portlet-body">
						<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
								<tr>
									<th width="33"> S.No. </th>
									<th width="126"> SMS Category </th>
									<th width="126"> SMS Title </th>
									<th width="126"> SMS Description </th>
									<th width="49"> Action </th>
									<th width="49"> Status </th>
								</tr>
							</thead>
							<tbody>
								<?php
									$serial = 1;
									foreach($content->result() as $row)
									{
										if($row->m_status == 'Active') $c= 'primary';
										if($row->m_status == 'Deactive') $c= 'default';
										if($row->m_status == 'Pending') $c= 'warning';
										if($row->m_status == 'Reject') $c= 'danger';	  
									?>
									<tr class="odd gradeX">
										<td><?php echo $serial; ?></td>
										<td>
											<?php
												foreach($cate->result() as $row12)
												{
													if($row->category==$row12->m_email_id)
													{
														echo $row12->m_email_name;
													}
												}
											?>
										</td>
										<td><?php echo $row->m_email_temp_title ?></td>
										<td><?php echo substr($row->m_email_temp_description,0,20) ?></td>
										<td><a href="<?php echo base_url(); ?>index.php/campaign/edit_sms_campaning/<?php echo $row->m_email_temp_id ?>" class="label label-sm label-success"><i class="fa  fa-edit"></i> Edit Message</a>
											
											
											<?php 
												if($row->m_status == 'Active'){?>
												<a href="<?php echo base_url()?>index.php/campaign/update_sms_template_status/<?php echo $row->m_email_temp_id?>/Deactive" class="label label-sm label-default" >Deactive</a>
												<?php }else{?>
												<a href="<?php echo base_url()?>index.php/campaign/update_sms_template_status/<?php echo $row->m_email_temp_id?>/Active" class="label label-sm label-primary" >Active</a>
												<?php if($row->m_status == 'Pending'){?>
													<a href="<?php echo base_url()?>index.php/campaign/update_sms_template_status/<?php echo $row->m_email_temp_id?>/Reject" class="label label-sm label-danger" >Reject</a>
												<?php }} ?>
												
										</td>
										<td>
											
											
											<a href="#" class="label label-sm label-<?php echo $c?>" ><?php echo $row->m_status?></a>
										</td>
									</tr>
									<?php
										$serial++;
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
    <!-- END CONTENT BODY --> 
</div>
</div>
<!-- END CONTENT --> 

<script>
	function insert()
	{
		
		if()
		{
			bootbox.confirm('Are you sure to Submit form?', function(result){
				if(result==true)
				{
					var title = $('#txttitle').val();
					var content=$('#editor1').val();
					var category =$('#category').val();
					$.ajax(
					{
						type: "POST",
						url:"",
						data: "title="+title+"&content="+content+"&category="+category,
						success: function(msg) {
							if(msg!="")
							{
								$(".page-content").html("<center><h2>Massege added successfully!</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(1000,function()
								{
									location.reload();
								}
								);
								
							}
						}
						
					});
				}
			}); 
		}
		else
		{
			return false;
		}
	}
	</script> 	