<div id="stylized">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->

            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li> <i class="icon-folder"></i> <a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/create_campaign"> Campaign </a> <i class="fa fa-angle-right"></i> </li>
                    <li> <i class="icon-action-redo"></i> <span>Campaign Sms Reports</span> <i class="fa fa-angle-right"></i> </li>
                </ul>
                <div class="page-toolbar">
                    <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range"> <i class="icon-calendar"></i>&nbsp; <span class="thin uppercase hidden-xs"></span>&nbsp; <i class="fa fa-angle-down"></i> </div>
                </div>
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h3 class="page-title">SMS Reports</h3>
            <!-- END PAGE TITLE-->

            <div class="row">

                <!-- END PAGE CONTENT-->

                <div class="col-md-12">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark"> <i class="icon-action-redo font-dark"></i> <span class="caption-subject bold uppercase"></span> </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Category</th>                                        
                                        <th>Title</th>
                                        
                                        <th>Sender Id</th>
                                        
                                        <th>User Id</th>
                                        <td>Date</td>
                                        <th>Status</th>
                                        <th>Mobile</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $serial=1 ; 
                                          foreach($rec->result() as $row)
                                          { 
										     $cc ='';	
									         if($row->status == 'Read'){ $c= 'primary'; ;$st = 'Read';}
									         if($row->status == 'Not Read'){$c= 'danger'; $st = 'Not Read';}
                                        ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo  $serial; ?></td>
                                        <td><?php echo  $row->category_name?></td>
                                        <td><?php echo  $row->tplte_title?></td>
                                        
                                       
                                        <td> <?php echo $row->sender_id ?></td>
                                        <td><?php echo  $row->owner ?></td>
                                        <td> <?php echo  $row->date ?></td>
                                        <td><span class="label label-sm label-<?php echo $c;?>"> <?php echo $st ?> </span></td> 								<td><?php echo  $row->contacts ?></td>
                                    </tr>
                                    <?php $serial++; 
									      } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
</div>
<!-- END CONTENT -->