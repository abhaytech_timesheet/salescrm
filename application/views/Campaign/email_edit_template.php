<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="#.">SALES BOOSTER</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Campaign</span>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Multiple Template for Email</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-10">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-envelope-open font-black"></i>
								<span class="caption-subject font-black bold uppercase">Edit Email Template</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!--BEGIN TABS-->
							<form id="myform" method="post" action="<?php echo base_url('Campaign/update_email_template/'.$this->uri->segment(3));?>" >
								<div class="form-body">
									
									<div id="content_email">
										<div id="content_email_temp">
											
											<?php 
												foreach($content->result() as $row)
												{ 
												?>										
												<div class="form-group">                                     
													<label>Subject of Email</label>
													<input type="text" class="form-control input-sm empty" id="txttitle" name="txttitle" value="<?php echo $row->m_email_temp_title; ?>" placeholder="Enter text"/>
													<span id="divtxttitle" style="color:red"></span>
												</div>
												<div class="form-group">
													<label>Category Of Email</label>
													<select class="form-control input-sm opt" name="category" id="category">
														<option value="-1">Select</option>
														<?php
															foreach($cate->result() as $row1)
															{
																if($row1->m_email_id==$row->category)
																{
																?>
																<option value="<?php echo $row1->m_email_id; ?>" selected="selected"><?php echo $row1->m_email_name; ?></option>
																<?php
																}
																else
																{
																?>
																<option value="<?php echo $row1->m_email_id; ?>"><?php echo $row1->m_email_name; ?></option>
																<?php
																}
															}
														?>
													</select>
													<input type="hidden" id="hidden" value="<?php echo $id; ?>"  />
													<span id="divcategory" style="color:red"></span>
												</div>
												<div style="clear:both"></div>                                  
												<div class="form-group">
													<div>
														<textarea class="ckeditor form-control" name="editor1"  id="editor1" ><?php echo $row->m_email_temp_description; ?></textarea>
														<span id="diveditor1" style="color:red"></span>
													</div>
												</div>
											</div>
										</div>
									</div>
									
								<?php } ?>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" onclick="return check('myform')" class="btn green"><i class="fa fa-check"></i> Update</button>								
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				
				
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	

