<div id="stylized"> 
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper"> 
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content"> 
			<!-- BEGIN PAGE HEADER--> 
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li> <i class="icon-folder"></i> <a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/create_campaign"> Campaign </a> <i class="fa fa-angle-right"></i> </li>
					<li> <i class="icon-action-redo"></i> <span>Campaign Users</span> <i class="fa fa-angle-right"></i> </li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range"> <i class="icon-calendar"></i>&nbsp; <span class="thin uppercase hidden-xs"></span>&nbsp; <i class="fa fa-angle-down"></i> </div>
				</div>
			</div>
			<!-- END PAGE BAR --> 
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">New Campaign API</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption"> <i class="icon-action-redo font-black"></i> <span class="caption-subject font-black bold uppercase">
								<?php if($this->uri->segment(3)) echo "Update";else echo "New";?>
							API</span> </div>
							<div class="actions"> <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;"> <i class="icon-cloud-upload"></i> </a> <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;"> <i class="icon-wrench"></i> </a> <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a> <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;"> <i class="icon-trash"></i> </a> </div>
						</div>
						<div class="portlet-body form">
							<?php if(is_numeric($this->uri->segment(3))){?>
								<form role="form" class="form-horizontal" method="post" id="campaign" action="<?php echo base_url();?>campaign/update__api_url/<?php echo $this->uri->segment(3)?>">
									<div class="form-body">
										<div class="form-group">
											<label class="col-md-3 control-label">Api Name<span style="color:red;">*</span></label>
											<div class="col-md-6">
												<div id="contact">
													<input type="text" class="form-control input-sm empty" name="api_name" id="api_name" value="<?php  echo $recss->m_api_name?>">
												</div>
											<span id="divapi_name" style="color:red;"> </span> </div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Api Url<span style="color:red;">*</span></label>
											<div class="col-md-6">
												<div id="contact">
													<input type="text" class="form-control input-sm empty" name="m_api_url" id="m_api_url" 
													value="<?php echo  $recss->m_api_url?>">
												</div>
											<span id="divuser_pwd" style="color:red;"></span> </div>
										</div>
										
										<div class="form-group">
											<label class="col-md-3 control-label">Description </label>
											<div class="col-md-6">
												<div id="contact">
													<textarea class="form-control empty" name="m_api_description"><?php echo  $recss->m_api_description?></textarea>
												</div>
											<span id="divtxtcontact" style="color:red;"></span> </div>
										</div>
										
										<div class="form-group">
											<label class="col-md-3 control-label">Status<span style="color:red;">*</span></label>
											<div class="col-md-6">
												<div id="contact">
													<select name="status" id="status" class="form-control input-sm opt">
														<option value="0">Select Status</option>
														<option value="Active" <?php if($recss->m_api_status == 'Active')echo 'selected'?>>Active</option>
														<option value="Deactive" <?php if($recss->m_api_status == 'Deactive')echo 'selected'?>>Deactive</option>
													</select>
												</div>
											<span id="divstatus" style="color:red;"></span> </div>
										</div>
									</div>
									<div class="form-actions fluid" >
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green" onclick="return check('campaign')">Submit</button>
										</div>
									</div>
								</form>
								<?php }else{?>
								
								<?php echo form_open("campaign/add_new_api_url",'method="post" id="campaign" class="form-horizontal" ');?>
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Api Name<span style="color:red;">*</span></label>
										<div class="col-md-6">
											<div id="contact">
												<input type="text" class="form-control input-sm empty" name="api_name" id="api_name" value="">
											</div>
										<span id="divapi_name" style="color:red;"> </span> </div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Api Url<span style="color:red;">*</span></label>
										<div class="col-md-6">
											<div id="contact">
												<input type="text" class="form-control input-sm empty" name="m_api_url" id="m_api_url" value="">
											</div>
										<span id="divuser_pwd" style="color:red;"></span> </div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Description </label>
										<div class="col-md-6">
											<div id="contact">
												<textarea class="form-control empty" name="m_api_description"></textarea>
											</div>
										<span id="divtxtcontact" style="color:red;"></span> </div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Status<span style="color:red;">*</span></label>
										<div class="col-md-6">
											<div id="contact">
												<select name="status" id="status" class="form-control input-sm opt">
													<option value="0">Select Status</option>
													<option value="Active">Active</option>
													<option value="Deactive">Deactive</option>
												</select>
											</div>
										<span id="divstatus" style="color:red;"></span> </div>
									</div>
								</div>
								<div class="form-actions fluid" >
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green" onclick="return check('campaign')">Submit</button>
									</div>
								</div>
							</form>
						<?php }?>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			<div class="col-md-6">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption font-dark"> <i class="icon-action-redo font-dark"></i> <span class="caption-subject bold uppercase"></span> </div>
						<div class="tools"> </div>
					</div>
					<div class="portlet-body">
						<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
								<tr>
									<th>Sr.No</th>
									<th>Action</th>
									<th>API Name</th>
									<th>API Url</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$serial = 1;
                                    $c ='default';
                                    $st='default';
                                    $ss='';
									foreach($api_links->result() as $row)
									{
										$cc ='';	
										if($row->m_api_status == '1'){ $c= 'primary'; $cc = 'default'; $st = 'Deactive'; $ss='Active';}
										if($row->m_api_status == '0') {$c= 'default';$cc = 'primary'; $st = 'Active'; $ss='Deactive';}
									?>              
									<tr class="odd gradeX">
										<td><?php echo $serial; ?></td>
										<td><a href="<?php echo base_url();?>Campaign/api_links/<?php echo $row->m_api_id?>" style="text-decoration:none"><span class="label label-sm label-success"> EDITE </span></a></td>
										<td><?php echo $row->m_api_name ?></td>
										<td><?php echo $row->m_api_url ?></td>
										<td><a href="#" style="text-decoration:none"><span class="label label-sm label-<?php echo $c;?>"> <?php echo $ss; ?> </span></a></td>
										<td><a href="<?php echo base_url();?>Campaign/upate_api_links_status/<?php echo $row->m_api_id?>/<?php echo $row->m_api_status?>" style="text-decoration:none"><span class="label label-sm label-<?php echo $cc;?>"> <?php echo $st; ?></span></a></td>
									</tr>
								<?php $serial++;}?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
    <!-- END CONTENT BODY --> 
</div>
</div>
<!-- END CONTENT --> 

