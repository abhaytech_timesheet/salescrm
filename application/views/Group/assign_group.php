<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="#">Groupping</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Assign Group</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Assign Group</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				
				<!-- END PAGE CONTENT-->
				
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Assign Group to Respective User</span>
							</div>
							<div class="tools">
								<button class="btn btn-sm green" onclick="assign_group_user()">Assign <i class="fa fa-user"></i></button>
							</div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>SNo.</th>
										<th>UserID</th>
										<th>User Name</th>
										<th>Group</th>
									</tr>
								</thead>
								<tbody>
									<?Php
									$sn=1;
									$gp=1;
									foreach($user->result() as $urow)
									{
										$usrgp="";
									?>
									<tr>
										<td><?php echo $sn;?></td>
										<td><?php echo $urow->or_m_user_id;?></td>
										<td><?php echo $urow->or_m_name;?></td>
										<td>
										<div class="op">
										<?php
											foreach($group->result() as $grow)
										{?>
											<label class="radio-inline">
											<?php
											if($record->num_rows() >0)
											{
												foreach($record->result() as $row)
												{
													if($row->m_u_id==$urow->or_m_reg_id && $row->m_group_id==$grow->m_gr_id)
													{
														$usrgp=$row->m_group_id;
														break;
													}
												}
											}
											if($grow->m_gr_id == $usrgp)
											{?>
											<input type="radio" name="radiobutton<?php echo $sn;?>" id="radio<?php echo $gp;?>" value="<?php echo $urow->or_m_reg_id.','.$grow->m_gr_id;?>" checked="checked"/> <?php echo $grow->m_gr_name;?>
											<?php
											}
											else
											{?>
											<input type="radio" name="radiobutton<?php echo $sn;?>" id="radio<?php echo $gp;?>" value="<?php echo $urow->or_m_reg_id.','.$grow->m_gr_id;?>"/> <?php echo $grow->m_gr_name;?>
										<?php
											}
										$gp++;
										}?>
										</div>
										</td>
										
									</tr>
									<?php
									$sn++;
									}?>
									
								</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END CONTENT BODY -->
		</div>
	</div>
	<!-- END CONTENT -->	
	
	<script>
		function assign_group_user()
		{
			var collection=$(".op");
			var inputs=collection.find("input[type=radio]");
			var status=0;
			var gp_userid="";
			
			for(var x=0;x<inputs.length;x++)
			{
				var id=inputs[x].id;
				if($("#"+id+"").is(':checked'))
				{
					var op=$("#"+id+"").val();
					var op1=op.split(',');
					grpid=op1[1];
					gp_userid=op1[0]+","+grpid+"-"+gp_userid;
					status=1;
				}
			}
			if(status==0)
			{
				alert('Please Select any User Id');
			}
			else
			{
				$.ajax(
				{
					type:"POST",
					url:"<?php echo base_url();?>index.php/group/assign_groupuser",
					data:"gp_userid="+gp_userid,
					success: function(msg)
					{
						alert(msg.trim());
						location.reload();
					}
				});
			}
			
		}
	</script>			