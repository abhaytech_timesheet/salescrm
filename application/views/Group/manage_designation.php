<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="#">Groupping</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Manage Designation</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Manage Designation</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Add Designation</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" id="insert_data" action="<?php echo base_url().'index.php/group/add_designation'?>" method="post">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-4 control-label">Designation Name</label>
										<div class="col-md-6">
											<input type="text" id="txtdsg" name="txtdsg" class="form-control input-sm empty" placeholder="Max. 25 characters..." id="myform" >
											<span id="divtxtdsg" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 control-label">Designation Status</label>
										<div class="col-md-6">
											<div class="radio-list">
												<label class="radio-inline">
												<input type="radio" name="rbdsg" id="rbdsg1" value="1" checked> Enable</label>
												<label class="radio-inline">
												<input type="radio" name="rbdsg" id="rbdsg2" value="0"> Disable</label>
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="button" onclick="conwv('insert_data')" class="btn green">Submit</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				
				
				<!-- END PAGE CONTENT-->
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">View Designation</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body" style="overflow-x: auto;">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S No.</th>
										<th>Designation Name</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
				
				<!-- end view report
				</div>
			</div>
			<!-- END CONTENT BODY -->
		</div>
	</div>
	<!-- END CONTENT -->	
	
	<script>
		//add_togroup(1,1,1);
		function add_togroup()
		{
			var sname="";
			var ssid="";
			var apiid="";
			var api_opid="";
			var collection=$(".op");
			var status=0;
			var count="";
			var group=$("#ddgrp").val();
			var inputs=collection.find("input[type=hidden],input[type=radio]");
			
			for(var x=0;x<inputs.length;x++)
			{
				var id=inputs[x].id;
				var name=inputs[x].name;
				var type=inputs[x].type;
				if(type =='hidden')
				{
					if(name=="txtcount")
					{
						count=$("#"+id+"").val();
					}
					if(id=="txtser"+count+"")
					{
						ssid=$("#txtser"+count+"").val();
					}
					if(id=="txtspname"+count+"")
					{
						sname=$("#txtspname"+count+"").val();
					}
					
				}
				if($("#"+id+"").is(':checked'))
				{
					var op=$("#"+id+"").val();
					var op1=op.split(',');
					apiid=op1[1];
					api_opid=op1[0]+","+ssid+","+sname+","+apiid+"-"+api_opid;
				}
				status=1
			}
			alert(api_opid);
			if(status==0)
			{
				alert('Please Select the Api Name for the operator');
			}
			else
			{
				$.ajax(
				{
					type:"POST",
					url:"<?php echo base_url();?>index.php/group/add_op_to_group/",
					data:"api_opid="+api_opid+"&group="+group,
					success: function(msg)
					{
						alert(msg.trim());
					}
				}
				)
				//alert(api_opid);
			}
			
		}
	</script>			