<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="#">Groupping</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Assign Margin</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Assign Margin</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Select Group</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" id="insert_data" action="<?php echo base_url().'index.php/group/assign_margin'?>" method="post">
								<div class="form-body" id="city">
									<div class="form-group">
										<label class="col-md-3 control-label">Group Name</label>
										<div class="col-md-6">
										<select id="ddgrp" name="ddgrp" class="form-control input-sm opt">
											<option selected="selected" value="-1">Select Group Name</option>
											<?php foreach($ddgroup->result() as $row)
												{
													if($grp_dd != "" && $grp_dd == $row->m_gr_id)
													{
													?>
													<option value="<?php echo $row->m_gr_id;?>" selected><?php echo $row->m_gr_name;?></option>
													<?php
													}
													else
													{?>
													<option value="<?php echo $row->m_gr_id;?>"><?php echo $row->m_gr_name;?></option>
													<?php
													}
												}
											?>
										</select>
										<span id="divddgrp" style="color:red;"></span>
									</div>
									</div>
									
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Select</button>
										<button type="button" class="btn blue" onclick="add_tomargin()">Update</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				
				
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Margin of Service Provider and Operator Code</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body" style="overflow-x: auto;">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S No.</th>
										<th>Type</th>
										<th>Service Provider</th>
										<th>Set Margin(%)</th>
									</tr>
								</thead>
								<tbody>
										<?php
										$sn=1;
										foreach($all_oper->result() as $row)
										{?>
											<td><?php echo $sn;?></td>
											<td><?php echo $row->Service_Name;?></td>
											<td><?php echo $row->Service_provider_name; ?></td>
											<td>
												<div class="right">
													<div class="op">
													<?php
														foreach($margin->result() as $mrow)
														{
															if($mrow->m_service_id == $row->Service_type && $mrow->m_so_name == $row->Service_provider)
															{?>
														
														<input id="txtcount<?php echo $sn;?>" name="txtcount" type="hidden" value="<?php echo $sn ;?>"/>
														<input id="txtser<?php echo $sn;?>" name="txtser<?php echo $sn;?>" type="hidden" value="<?php echo $row->Service_type ;?>"/>
														<input id="txtspname<?php echo $sn;?>"  name="txtspname<?php echo $sn;?>" type="hidden" value="<?php echo $row->Service_provider;?>"/>
														<input type="text" name="text<?php echo $sn;?>" id="text<?php echo $sn;?>" value="<?php echo $mrow->m_op_margin;?>" />
														<?php
															}
														}?>
													</div>
												</div>
											</td>
										</tr>
										<?php
										$sn++;
										}?>
								</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END CONTENT BODY -->
		</div>
	</div>
	<!-- END CONTENT -->	
	
	<script>
		function add_tomargin()
	{
			var count=0;
			var ssid=0;
			var sname=0;
			var marg_id="";
			var collection=$(".op");
			var group=$("#ddgrp").val();
			var inputs=collection.find("input[type=hidden],input[type=text]");
			
			for(var x=0; x < inputs.length;x++)
			{
				var id=inputs[x].id;
				var name=inputs[x].name;
				var type=inputs[x].type;
				if(type =='hidden')
				{
					if(name=="txtcount")
					{
					count=$("#"+id+"").val();
					}
					if(id=="txtser"+count+"")
					{
					ssid=$("#txtser"+count+"").val();
					}
					if(id=="txtspname"+count+"")
					{
					sname=$("#txtspname"+count+"").val();
					}
				}
				
				if(type =='text' && $("#"+id).val()!="")
				{
					marg_id=$("#"+id).val()+","+ssid+","+sname+"-"+marg_id;
					
				}
			}
			alert(marg_id);
			if(group=="")
			{
			alert('Please select a Group');
			}
			else
			{
				$.ajax(
				{
					type:"POST",
					url:"<?php echo base_url();?>index.php/group/add_mar_to_group/",
					data:"marg_id="+marg_id+"&group="+group,
					success: function(msg)
					{
						alert(msg.trim());
					}
				});
			}
		}
	</script>			