<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="#">Groupping</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Manage Group</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Manage Group</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Add Group</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" id="insert_data" action="<?php echo base_url().'index.php/group/add_group'?>" method="post">
								<div class="form-body" id="city">
									<div class="form-group">
										<label class="col-md-3 control-label">Group Name</label>
										<div class="col-md-6">
											<input type="text" id="txtgrp" name="txtgrp" class="form-control input-sm empty" placeholder="Max. 25 characters..." id="myform" >
											<span id="divtxtgrp" style="color:red;"></span>
										</div>
									</div>
									
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="button" onclick="conwv('insert_data')" class="btn green">Submit</button>
										<button type="reset" class="btn default">Cancel</button>
										</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Manage Group</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" id="insert_data" action="<?php echo base_url().'index.php/group/manage_group'?>" method="post">
								<div class="form-group">
									<label class="col-md-4 control-label">Select Group Name</label>
									<div class="col-md-6">
										<select id="ddgrp" name="ddgrp" class="form-control input-sm opt">
											<option selected="selected" value="-1">Select Group Name</option>
											<?php foreach($ddgroup->result() as $row)
												{
													if($grp_dd != "" && $grp_dd == $row->m_gr_id)
													{
													?>
													<option value="<?php echo $row->m_gr_id;?>" selected><?php echo $row->m_gr_name;?></option>
													<?php
													}
													else
													{?>
													<option value="<?php echo $row->m_gr_id;?>"><?php echo $row->m_gr_name;?></option>
													<?php
													}
												}
											?>
										</select>
										<span id="divddgrp" style="color:red;"></span>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Select</button>
										<button type="reset" class="btn default" onClick="add_togroup()">Update</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Service Provider and Operator Code</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body" style="overflow-x: auto;">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S No.</th>
										<th>Type</th>
										<th>Service Provider</th>
										<th>API</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										$t=1;
										foreach($all_oper->result() as $sprow)
										{
											$apiid=0;
											$sid=0;
											$status=0;
											$service="";
											$mar_value="";
											
										?>
										<tr>
											<td><?php echo $t;?></td>
											<td><?php echo $sprow->Service_Name;?></td>
											<td><?php echo $sprow->Service_provider_name; ?></td>
											<td>
												<div class="right">
													<div class="op">
														<input id="txtcount<?php echo $t;?>" name="txtcount" type="hidden" value="<?php echo $t ;?>"/>
														<input id="txtser<?php echo $t;?>" name="txtser<?php echo $t;?>" type="hidden" value="<?php echo $sprow->Service_type ;?>"/>
														<input id="txtspname<?php echo $t;?>"  name="txtspname<?php echo $t;?>" type="hidden" value="<?php echo $sprow->Service_provider;?>"/>
														<?php
															$t1=1;
															foreach($group->result() as $gprow)
															{ 
																$sid =$gprow->Service_Operator; //---changed
																if($sprow->Service_type==$gprow->Service_type && $sprow->Service_provider==$gprow->Service_provider )
																{
																	foreach($groupmargin->result() as $gmrow)
																	{
																		if($gmrow->Service_id == $sprow->Service_type && $gmrow->Operator_name == $sprow->Service_provider)
																		{
																			$mar_value =$gmrow->Api_id;
																			
																			break;
																		}
																	}?>
																	
																	<label class="radio-inline">
																		<?php
																		if($gprow->Api_id !=0)
																		{	
																			if($mar_value==$gprow->Api_id)
																			{
																			?>
																			<input type="radio" name="radiobutton<?php echo $t;?>" id="radio<?php echo $t.$t1;?>" value="<?php echo $sid.','.$gprow->Api_id;?>" checked="Checked" />
																			<?php
																			}
																			else
																			{
																			?>
																			<input type="radio" name="radiobutton<?php echo $t;?>" id="radio<?php echo $t.$t1;?>" value="<?php echo $sid.','.$gprow->Api_id;?>" />
																			<?php
																			}
																		
																			?>
																			<label for="radio<?php echo $t;?>"><?php echo $gprow->Api_name;?></label> 
																			<?php
																		}
																	$t1++;
																}
															}
														?>
													</div>
												</div>
											</td>
											
											</tr>
											<?php
												$t++;
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END CONTENT BODY -->
		</div>
	</div>
	<!-- END CONTENT -->	
	
	<script>
		//add_togroup(1,1,1);
		function add_togroup()
		{
			var sname="";
			var ssid="";
			var apiid="";
			var api_opid="";
			var collection=$(".op");
			var status=0;
			var count="";
			var group=$("#ddgrp").val();
			var inputs=collection.find("input[type=hidden],input[type=radio]");
			
			for(var x=0;x<inputs.length;x++)
			{
				var id=inputs[x].id;
				var name=inputs[x].name;
				var type=inputs[x].type;
				if(type =='hidden')
				{
					if(name=="txtcount")
					{
						count=$("#"+id+"").val();
					}
					if(id=="txtser"+count+"")
					{
						ssid=$("#txtser"+count+"").val();
					}
					if(id=="txtspname"+count+"")
					{
						sname=$("#txtspname"+count+"").val();
					}
					
				}
				if($("#"+id+"").is(':checked'))
				{
					var op=$("#"+id+"").val();
					var op1=op.split(',');
					apiid=op1[1];
					api_opid=op1[0]+","+ssid+","+sname+","+apiid+"-"+api_opid;
				}
				status=1
			}
			alert(api_opid);
			if(status==0)
			{
				alert('Please Select the Api Name for the operator');
			}
			else
			{
				$.ajax(
				{
					type:"POST",
					url:"<?php echo base_url();?>index.php/group/add_op_to_group/",
					data:"api_opid="+api_opid+"&group="+group,
					success: function(msg)
					{
						alert(msg.trim());
					}
				}
				)
				//alert(api_opid);
			}
			
		}
	</script>			