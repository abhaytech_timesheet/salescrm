<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="fa fa-wrench"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_session">ACS</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-calendar"></i>
						<span>Add Session</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">Add Session</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-calendar font-black"></i>
								<span class="caption-subject font-black bold uppercase">Add Session</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="" id="myform" action="#">
								<div class="form-body" id="rank">
									
									<div class="form-group">
										<label class="col-md-3 control-label">Session Dates</label>
										<div class="col-md-6">
											<div class="input-group input-medium date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control input-sm empty" name="txtfromdate" id="txtfromdate">
												<span class="input-group-addon">
													to
												</span>
												<input type="text" class="form-control input-sm empty" name="txttodate" id="txttodate">
											</div>
											<!-- /input-group -->
											<span class="help-block">
												Select date range for session.<br />( Example : 01-04-2015 to 31-03-2015)
											</span>
                                            <span id="divtxtfromdate" style="color:red;"></span>
                                            <span id="divtxttodate" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Status</label>
										<div class="col-md-9">
											<div class="radio-list">
												<label class="radio-inline">
												<input type="radio" name="rbrank" id="rbrank1" value="1" checked onClick="check22()"> Enable</label>
												<label class="radio-inline">
												<input type="radio" name="rbrank" id="rbrank2" value="0"  onClick="check22()"> Disable</label>
												<input type="hidden" value="1" id="txtstatus" name="txtstatus" />
											</div>
										</div>
									</div>
									
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="button" class="btn green" onclick="add_session()" >Submit</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-reorder font-black"></i>
								<span class="caption-subject bold uppercase"></span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S No.</th>
										<th>Session</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										$sn =1 ;
										foreach($sessionss->result() as $rows)
										{
										?>
										<tr>
											<td><?php echo $sn; ?></td>
											<td><?php echo $rows->m_session ?></td>
											<td>
												<?php 
													if($rows->m_session_status==1)
													{
													?>&nbsp;
													<a href="javascript:void(0)" onclick="edit_session(<?php echo $rows->m_session_id;?>,0)" class="label label-sm label-danger">
														Disable
													</a>
													<?php
													}
													if($rows->m_session_status==0)
													{
													?>&nbsp;
													<a href="javascript:void(0)" onclick="edit_session(<?php echo $rows->m_session_id;?>,1)" class="label label-sm label-success">
													Enable</a>
													<?php
													}
												?>
											</td>
										</tr>
									<?php $sn++;} ?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->													

<script>
	function check22()
	{
		var collection=$("#stylized");
		var mark=0;
		var obtainmark=0;
		var inputs=collection.find("input[type=checkbox],input[type=radio]");
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			var name=inputs[x].name;
			if($("#"+id+"").is(':checked'))
			{
				if(id=="rbrank1")
				{
					$("#txtstatus").val('1');
				}
				if(id=="rbrank2")
				{
					$("#txtstatus").val('0');
				}
			}
		}
	}
</script>
<script>
	
	function add_session()
	{
		if(check("myform"))
		{
			bootbox.confirm('Are you sure to Submit form?', function(result){
				if(result==true)
				{
					var txtfromdate = $('#txtfromdate').val();
					var txttodate = $('#txttodate').val();
					var rbrank= $('#txtstatus').val();
					//alert('hello');
					$.ajax({
						type:"POST",
						url:"<?php echo base_url();?>index.php/acs/add_session",
						dataType:'json',
						data: {'txtfromdate':txtfromdate,
							'txttodate':txttodate,
						'rbrank':rbrank},
						success: function(msg){
							//alert(msg);
							if(msg==1)
							{
								$(".page-content").html("<center><h2>Session Added Successfully</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(1000,function(){
									$('#stylized').load('<?php echo base_url();?>index.php/acs/view_session1/');
								});
							}
							else
							{
								//alert('ff');
								alert("Please select the year of last session .");
								$('#stylized').html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
								$('#stylized').load('<?php echo base_url();?>index.php/acs/view_session1/');
								//location.reload("<?php echo base_url();?>index.php/acs/view_session");
							}
						}
					});
					
				}
			}); 
		}
		else
		{
			return false;
		}
	}
</script>

<script>
	function edit_session(id,status)
	{
		$('#stylized').html("<center><h2>Session Status Altered Successfully</h2></center>")
		.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
		.hide()
		.fadeIn(1000,function(){
			$('#stylized').load('<?php echo base_url();?>index.php/acs/edit_session/'+id+'/'+status);
		});
	}
</script>

