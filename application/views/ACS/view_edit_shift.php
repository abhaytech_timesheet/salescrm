<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper" id="rank">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="fa fa-wrench"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_session">ACS</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-mortar-board"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_shift_master">Manage Shift</a>
						<i class="fa fa-angle-right"></i>
					</li>
					
					<li>
					    <i class="fa fa-edit"></i>
						<span>Edit Shift</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Edit Shift </h3>
			<!-- END PAGE TITLE-->
			<?php 
				foreach($shiftss->result() as $rows)
				{
				}
			?>
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-mortar-board (alias) font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Edit Shift  </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="" action="#" id="myform">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Shift Name</label>
										<div class="col-md-6">
											<input type="text" id="txtshiftname" name="txtshiftname" class="form-control input-sm empty" placeholder="Enter Shift Name" value="<?php echo $rows->shift_name ?>">
										<span id="divtxtshiftname" style="color:red;"></span> </div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Start Time</label>
										<div class="col-md-6">
											<div class="input-group">
												<input type="text" class="form-control input-sm empty timepicker timepicker-24" id="txtstarttime" name="txtstarttime" onblur="diff()" value="<?php echo $rows->shift_starttime ?>">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-clock-o"></i></button>
												</span> </div>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">End Time</label>
										<div class="col-md-6">
											<div class="input-group">
												<input type="text" class="form-control input-sm empty timepicker timepicker-24" id="txtendtime" name="txtendtime" onblur="diff()" value="<?php echo $rows->shift_endtime ?>">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-clock-o"></i></button>
												</span> </div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Shift Duration</label>
										<div class="col-md-6">
											<input name="txtshift" id="txtshift" type="text" class="form-control input-sm empty" readonly="readonly"  placeholder="Shift Duration" value="<?php echo $rows->shift_duration ?>" />
										<span id="divtxtshift" style="color:red;"></span> </div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Start Date</label>
										<div class="col-md-6">
											<input name="txtstart" id="txtstart" type="text" class="form-control input-sm date-picker" data-date-format="yyyy-mm-dd" placeholder="Enter Start Date" value="<?php echo $rows->shift_startdate ?>" onclick="diff()"/>
										<span id="divtxtstart" style="color:red;"></span> </div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">End Date</label>
										<div class="col-md-6">
											<input name="txtend" id="txtend" type="text" class="form-control input-sm date-picker" data-date-format="yyyy-mm-dd" placeholder="Enter End Date" value="<?php echo $rows->shift_enddate ?>"/>
										<span id="divtxtend" style="color:red;"></span> </div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Min Hrs. for Half Day</label>
										<div class="col-md-6">
											<input name="txthalfday" id="txthalfday" type="text" class="form-control input-sm empty" placeholder="Min Hrs. for Half Day" value="<?php echo $rows->shift_halfday ?>" />
											<span class="help-block">(Example - 00:15:00 or 01:00:00)</span>
										<span id="divtxthalfday" style="color:red;"></span> </div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Min Hrs. for Full Day</label>
										<div class="col-md-6">
											<input name="txtfullday" id="txtfullday" type="text" class="form-control input-sm empty" placeholder="Min Hrs. for Full Day" value="<?php echo $rows->shift_fullday ?>" />
											<span class="help-block">(Example - 00:15:00 or 01:00:00)</span>
										<span id="divtxtfullday" style="color:red;"></span> </div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Grace Time Before</label>
										<div class="col-md-6">
											<input name="txtgrace" id="txtgrace" type="text" class="form-control input-sm empty" placeholder="Grace Time" value="<?php echo $rows->shift_grace ?>"/>
											<span class="help-block">(Example - 00:15:00 or 01:00:00)</span>
										<span id="divtxtgrace" style="color:red;"></span></div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Status</label>
										<div class="col-md-6">
											<div class="radio-list">
												<?php 
													if($rows->shift_status==1)
													{
													?>
													<label class="radio-inline">
														<input type="radio" name="rbrank" id="rbrank1" value="1" checked onClick="check22()">
													Enable</label>
													<label class="radio-inline">
														<input type="radio" name="rbrank" id="rbrank2" value="0"  onClick="check22()">
													Disable</label>
													<input type="hidden" value="1" id="txtstatus" name="txtstatus" />
													<?php
													}
													if($rows->shift_status==0)
													{
													?>
													<label class="radio-inline">
														<input type="radio" name="rbrank" id="rbrank1" value="1"  onClick="check22()">
													Enable</label>
													<label class="radio-inline">
														<input type="radio" name="rbrank" id="rbrank2" value="0" checked  onClick="check22()">
													Disable</label>
													<input type="hidden" value="0" id="txtstatus" name="txtstatus" />
													<?php
													}
												?>
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="button" class="btn green" onclick="update_shift()" >Update</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-reorder font-dark"></i>
								<span class="caption-subject bold uppercase"></span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th width="10%">S No.</th>
										<th width="30%">Shift Name</th>
										<th width="30%">Start Time</th>
										<th width="30%">End Time</th>
										<th width="30%">Shift Duration</th>
										<th width="30%">Start Date</th>
										<th width="30%">End Date</th>
										<th width="30%">Half Day Time</th>
										<th width="30%">Full Day Time</th>
										<th width="30%">Grace Time</th>
										<th width="30%">Action</th>
										
									</tr>
								</thead>
								<tbody>
									<?php 
										$sn=1;
										foreach($shift->result() as $row)
										{
										?>
										<tr>
											<td><?php echo $sn;?></td>
											<td><?php echo $row->shift_name ?></td>
											<td><?php echo $row->shift_starttime ?></td>
											<td><?php echo $row->shift_endtime ?></td>
											<td><?php echo $row->shift_duration ?></td>
											<td><?php echo $row->shift_startdate ?></td> 
											<td><?php echo $row->shift_enddate ?></td>
											<td><?php echo $row->shift_halfday?></td>
											<td><?php echo $row->shift_fullday ?></td> 
											<td><?php echo $row->shift_grace ?></td>
											<td>
												<div class="btn-group">
													<button class="btn red btn-sm dropdown-toggle" data-toggle="dropdown">Action
														<i class="fa fa-angle-down"></i>
													</button>
													<ul class="dropdown-menu">
														
														<li>
															<a href="javascript:void(0)" onclick="edit_shift(<?php echo $row->shift_id;?>)" rel="facebox"> 
																Edit
															</a>
														</li>
														<?php 
															if($row->shift_status==1)
															{
															?>
															<li>
																<a href="javascript:void(0)" onclick="stch_shift(<?php echo $row->shift_id;?>,0)"> 
																	Disable
																</a>
															</li>
															<?php
															}
															if($row->shift_status==0)
															{
															?>
															<li>
																<a href="javascript:void(0)" onclick="stch_shift(<?php echo $row->shift_id;?>,1)">
																	Enable
																</a>
															</li>
															<?php
															}
														?>
													</ul>
												</div>
											</td>
										</tr>
										<?php
											$sn++;
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->

<script>
	function check22()
	{
		var collection=$("#rank");
		var mark=0;
		var obtainmark=0;
		var inputs=collection.find("input[type=checkbox],input[type=radio]");
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			var name=inputs[x].name;
			if($("#"+id+"").is(':checked'))
			{
				if(id=="rbrank1")
				{
					$("#txtstatus").val('1');
				}
				if(id=="rbrank2")
				{
					$("#txtstatus").val('0');
				}
			}
		}
	}
</script>
<script>
	function shifts()
	{
		alert('gd');
		document.getElementById("txtshift").value = diff(start, end);
	}
</script>
<script>
	function diff() {
		
		//alert('gd');
		var start = $("#txtstarttime").val();
		var end = $("#txtendtime").val();
		start = start.split(":");
		start[1]=start[1].split(" ");
		end = end.split(":")
		end[1] = end[1].split(" ");
		var startDate = new Date(0, 0, 0, start[0], start[1][0], 0);
		var endDate = new Date(0, 0, 0, end[0], end[1][0], 0);
		var diff = endDate.getTime() - startDate.getTime();
		var hours = Math.floor(diff / 1000 / 60 / 60);
		diff -= hours * 1000 * 60 * 60;
		var minutes = Math.floor(diff / 1000 / 60);
		var shifts=((hours <= 9 ? "0" : "") + hours + ":" + (minutes <= 9 ? "0" : "") + minutes+":00");
		//alert(shifts);
		
		if(start[1][1]!=end[1][1])
		{
			var shift1 = shifts.split(":");
			var shift2 = shift1[0].split("-");
			//alert(shift2[0]);
			
			if(shift2[0]==0)
			{
				var shift3 = (12-shift2[1]);
				$("#txtshift").val(shift3+":"+shift1[1]+":00");
			}
			else
			{	
				var shift4 = parseInt(shift2[0]);
				var shift5 = (shift4+12);
				$("#txtshift").val(shift5+":"+shift1[1]+":00");
			}
		}
		else
		{
			//alert("fdd");
			var shift1 = shifts.split(":");
			var shift2 = shift1[0].split("-");
			//alert(shift2[0]);
			if(shift2[0]==0)
			{
				alert("Please enter greater time than Start time");
			}
			else
			{
				$("#txtshift").val(shifts);
			}
		}
		
	}
</script>

<input type="hidden" id="txtid" name="txtid" value="<?php echo $id ?>" />
<script>
	function update_shift()
	{
		if(check("myform"))
		{
			bootbox.confirm('Are you sure to Submit form?', function(result){
				if(result==true)
				{
					var data={
						txtshiftname:$('#txtshiftname').val(),
						txtstarttime:$('#txtstarttime').val(),
						txtendtime:$('#txtendtime').val(),
						txtshift:$('#txtshift').val(),
						txtstart:$('#txtstart').val(),
						txtend:$('#txtend').val(),
						txthalfday:$('#txthalfday').val(),
						txtfullday:$('#txtfullday').val(),
						txtgrace:$('#txtgrace').val(),
						txtstatus:$('#txtstatus').val(),
						id:$('#txtid').val()
					};
					$.ajax(
					{
						type:"POST",
						url:"<?php echo base_url();?>index.php/acs/update_shift",
						dataType:'json',
						data:  data,
						success: function(msg) {
							
							//$(".page-content").html(msg);
							if(msg==1)
							{
								$(".page-content").html("<center><h2>Shift Altered Successfully</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(1000,function(){
									$('#rank').load('<?php echo base_url();?>index.php/acs/view_shift_master1/');
								});
							}
							else
							{
								//alert('ff');
								alert("Some error occured.");
								$('#rank').html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
								$('#rank').load('<?php echo base_url();?>index.php/acs/view_shift_master1/');
							}
							
						}
					});
				}
			}); 
		}
		else
		{
			return false;
		}
	}
</script> 
<script>
	function edit_shift(id)
	{
		$('#rank').html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$('#rank').load('<?php echo base_url();?>index.php/acs/edit_shift/'+id);
	}
</script> 
<script>
	function stch_shift(id,status)
	{
		$('#rank').html("<center><h2>Shift Status Altered Successfully</h2></center>")
		.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
		.hide()
		.fadeIn(1000,function(){
			$('#rank').load('<?php echo base_url();?>index.php/acs/stch_shift/'+id+'/'+status);
		});
	}
</script>																																						