<script>
	function check()
	{
		var collection=$("#rank");
		var mark=0;
		var obtainmark=0;
		var inputs=collection.find("input[type=checkbox],input[type=radio]");
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			var name=inputs[x].name;
			if($("#"+id+"").is(':checked'))
			{
				if(id=="rbrank1")
				{
					$("#txtstatus").val('1');
				}
				if(id=="rbrank2")
				{
					$("#txtstatus").val('0');
				}
			}
		}
	}
</script>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content" id="rank">
		
		
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Manage Department
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo base_url();?>index.php/master/index/">
							Master
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo base_url();?>index.php/master/view_manage_department/">
							Department
						</a>
					</li>
					<li class="pull-right">
						<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
							<i class="fa fa-calendar"></i>
							<span>
							</span>
							<!-- <i class="fa fa-angle-down"></i> -->
						</div>
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-6 ">
				<div class="portlet box green ">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i>Manage Department
						</div>
						<div class="tools">
							<a href="" class="collapse">
							</a>
							<a href="#portlet-config" data-toggle="modal" class="config">
							</a>
							<a href="" class="reload">
							</a>
							<a href="" class="remove">
							</a>
						</div>
					</div>
					<div class="portlet-body form">
						<form class="form-horizontal" role="form" method="" action="#">
							<div class="form-body" id="rank">
								
								<div class="form-group">
									<label class="col-md-3 control-label">Session Dates</label>
									<div class="col-md-6">
										<div class="input-group input-medium date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
											<input type="text" class="form-control" name="txtfromdate" id="txtfromdate">
											<span class="input-group-addon">
												to
											</span>
											<input type="text" class="form-control" name="txttodate" id="txttodate">
										</div>
										<!-- /input-group -->
										<span class="help-block">
											Select date range for session.<br />( Example : 01-04-2015 to 31-03-2015)
										</span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Status</label>
									<div class="col-md-9">
										<div class="radio-list">
											<label class="radio-inline">
											<input type="radio" name="rbrank" id="rbrank1" value="1" checked onClick="check()"> Enable</label>
											<label class="radio-inline">
											<input type="radio" name="rbrank" id="rbrank2" value="0"  onClick="check()"> Disable</label>
											<input type="hidden" value="1" id="txtstatus" name="txtstatus" />
										</div>
									</div>
								</div>
								
							</div>
							<div class="form-actions fluid">
								<div class="col-md-offset-3 col-md-9">
									<button type="button" class="btn green" onclick="add_session()" >Submit</button>
									<button type="reset" class="btn default">Cancel</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			
			<div class="col-md-6">
				
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-globe"></i>View & Modify Rank/Designation
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse">
							</a>
							<a href="#portlet-config" data-toggle="modal" class="config">
							</a>
							<a href="javascript:;" class="reload">
							</a>
							<a href="javascript:;" class="remove">
							</a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-toolbar">
							
							<div class="btn-group pull-right">
								<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
								</button>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="#">
											Print
										</a>
									</li>
									<li>
										<a href="#">
											Save as PDF
										</a>
									</li>
									<li>
										<a href="#">
											Export to Excel
										</a>
									</li>
								</ul>
							</div>
						</div>
						<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
								<tr>
									<th>S No.</th>
									<th>Session</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									$sn =1 ;
									foreach($session->result() as $rows)
									{
										$status=0;
										if($rows->m_session_status==1)
										{
											$status="Enable";
										}
										if($rows->m_session_status==0)
										{
											$status="Disable";
										}
									?>
									<tr>
										<td><?php echo $sn; ?></td>
										<td><?php echo $rows->m_session ?></td>
										<td>
											<?php 
												if($status=="Enable")
												{
												?>&nbsp;
												<a href="<?php echo base_url();?>index.php/acs/edit_session/<?php echo $rows->m_session_id;?>/0">
													<img src="<?php echo base_url();?>application/libraries/assets/img/cancel.png" alt="Disable" title="Disable">
												</a>
												<?php
												}
												if($status=="Disable")
												{
												?>&nbsp;
												<a href="<?php echo base_url();?>index.php/acs/edit_session/<?php echo $rows->m_session_id;?>/1">
													<img src="<?php echo base_url();?>application/libraries/assets/img/check.png" alt="Enable" title="Enable">
													</a><?php
												}
											?>
										</td>
									</tr>
								<?php $sn++;} ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			
			<!-- END FORM-->
		</div>
		
		<!-- END PAGE CONTENT-->
	</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

<script type="text/javascript">
	function confirmation()
	{
		/*App.blockUI({
			target: '#rank'
			});
			
			window.setTimeout(function () {
			App.unblockUI('#rank');
		}, 10000);*/
		
		bootbox.confirm("Are you sure?", function(result) {
			if(result==true)
			{  
				alert("Confirm result: "+result);
			}
		});		
	}
</script>		
<script type="text/javascript">
	function validateForm()
	{
		var x=document.forms["myForm"]["txtstate"].value;
		var y=document.forms["myForm"]["txtcity"].value;
		if(x!=null && x!="" && x!=-1)
		{
			$("#divtxtstate").html('');
			if(y!=null && y!="")
			{
				return true;
			}
			else
			{
				document.forms["myForm"]["txtcity"].focus();
				$("#divtxtcity").html('Please Enter City');
				return false;
			}
		}
		else
		{
			document.forms["myForm"]["txtstate"].focus();
			$("#divtxtstate").html('Please Select State');
			return false;
		} 
		
	}
</script>
<script>
	function add_session()
	{
		
		var txtfromdate = $('#txtfromdate').val();
		var txttodate = $('#txttodate').val();
		var rbrank= $('#txtstatus').val();
		//alert('hello');
		$.ajax({
			type:"POST",
			url:"<?php echo base_url();?>index.php/acs/add_session",
			dataType:'json',
			data: {'txtfromdate':txtfromdate,
				'txttodate':txttodate,
			'rbrank':rbrank},
			success: function(msg){
				//alert(msg);
				if(msg==1)
				{
					$(".page-content").html("<center><h2>Session Added Successfully</h2></center>")
					.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/img/loading-spinner-blue.gif' /></center>")
					.append("<center><a href='<?php echo base_url();?>index.php/acs/view_session/'>Click Here</a></center>");
				}
				else
				{
					//alert('ff');
					alert("Please select the year of last session .");
					//location.reload("<?php echo base_url();?>index.php/acs/view_session");
				}
			}
		});
		
	}
</script>
