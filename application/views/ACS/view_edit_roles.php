<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper" id="rank">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="fa fa-wrench"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_session">ACS</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="icon-target"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_manage_role">Manage Roles</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-edit"></i>
						<span>Edit Roles</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Edit Roles </h3>
			<!-- END PAGE TITLE-->
			<?php 
				foreach($rank->result() as $rank_row)
				{
					break;
				}
				
			?>
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-target font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Edit Roles </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" id="myform">
								<div class="form-body" id="rank">
									<div class="form-group">
										<label class="col-md-3 control-label">Department</label>
										<div class="col-md-6">
											<select class="form-control input-sm opt" name="dddepartment" id="dddepartment">
												<option value="-1">~~ Select ~~ </option>
												<?php foreach($rank->result() as $rowst)
													{
													}
													foreach($department->result() as $rows)
													{
														if($rowst->m_des_pat_id==$rows->m_des_id)
														{
														?>
														<option selected="selected" value="<?php echo $rows->m_des_id; ?>"><?php echo $rows->m_des_name; ?></option>
														<?php }
														else
														{
														?>
														<option value="<?php echo $rows->m_des_id; ?>"><?php echo $rows->m_des_name; ?></option>
													<?php } } ?>
											</select>
											<span id="divdddepartment" style="color:red;"></span> 
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Role Name</label>
										<div class="col-md-6">
											<input type="text" id="txtrole" name="txtrole" class="form-control input-sm empty" placeholder="Enter Designation" value="<?php echo $rank_row->m_des_name;?>">
											<span id="divtxtrole" style="color:red;"> </span> 
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Status</label>
										<div class="col-md-6">
											<div class="radio-list">
												<?php 
													if($rank_row->m_des_status==1)
													{
													?>
													<label class="radio-inline">
														<input type="radio" name="rbrank" id="rbrank1" value="1" checked onClick="check22()">
													Enable</label>
													<label class="radio-inline">
														<input type="radio" name="rbrank" id="rbrank2" value="0"  onClick="check22()">
													Disable</label>
													<input type="hidden" value="1" id="txtstatus" name="txtstatus" />
													<?php
													}
													if($rank_row->m_des_status==0)
													{
													?>
													<label class="radio-inline">
														<input type="radio" name="rbrank" id="rbrank1" value="1"  onClick="check22()">
													Enable</label>
													<label class="radio-inline">
														<input type="radio" name="rbrank" id="rbrank2" value="0" checked  onClick="check22()">
													Disable</label>
													<input type="hidden" value="0" id="txtstatus" name="txtstatus" />
													<?php
													}
												?>
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="button" onclick="update_roles()"  class="btn green" >Update</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-reorder font-dark"></i>
								<span class="caption-subject bold uppercase"></span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S No.</th>
										<th>Role Name</th>
										<th>Department</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										$sn=1;
										foreach($roles->result() as $row)
										{
										?>
										<tr>
											<td><?php echo $sn;?></td>
											<td><?php echo $row->Role_name?></td>
											<td><?php echo $row->Parent_name?></td>
											<td>
												<div class="btn-group">
                                                    <button class="btn red btn-xs dropdown-toggle" data-toggle="dropdown">Action
                                                        <i class="fa fa-angle-down"></i>
													</button>
                                                    <ul class="dropdown-menu" style="position: relative;">
														
                                                        <li>
															<a href="javascript:void(0)" onclick="edit_roles(<?php echo $row->Role_id;?>)" rel="facebox"> 
																Edit
															</a>
														</li>
														<?php 
															if($row->Role_status==1)
															{
															?>
															<li>
																
																<a href="javascript:void(0)" onclick="stch_designation(<?php echo $row->Role_id;?>,0)"> 
																	Disable
																</a>
																
															</li>
															<?php
															}
															if($row->Role_status==0)
															{
															?>
															<li>
																<a href="javascript:void(0)" onclick="stch_designation(<?php echo $row->Role_id;?>,1)">
																	Enable
																</a>
															</li>
															<?php
															}
														?>
													</ul>
												</div>
												
											</td>
										</tr>
										<?php
											$sn++;
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	
<input type="hidden" id="txtid" name="txtid" value="<?php echo $this->uri->segment(3); ?>" />

<script>
	function check22()
	{
		var collection=$("#rank");
		var mark=0;
		var obtainmark=0;
		var inputs=collection.find("input[type=checkbox],input[type=radio]");
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			var name=inputs[x].name;
			if($("#"+id+"").is(':checked'))
			{
				if(id=="rbrank1")
				{
					$("#txtstatus").val('1');
				}
				if(id=="rbrank2")
				{
					$("#txtstatus").val('0');
				}
			}
		}
	}
</script>
<!-- BEGIN CONTENT -->

<script>
	function update_roles()
	{
		if(check("myform"))
		{
			bootbox.confirm('Are you sure to Submit form?', function(result){
				if(result==true)
				{
					var dddepartment = $('#dddepartment').val();
					var txtrole = $('#txtrole').val();
					var txtstatus = $('#txtstatus').val();
					var id = $('#txtid').val();
					//alert(id);
					//alert('hello');
					$.ajax({
						type:"POST",
						url:"<?php echo base_url();?>index.php/acs/update_roles/",
						dataType:'json',
						data: {'dddepartment':dddepartment,
							'txtrole':txtrole,
							'txtstatus':txtstatus,
						'id':id},
						success: function(msg){
							//alert(msg);
							if(msg==1)
							{
								$(".page-content").html("<center><h2>Roles Altered Successfully</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(1000,function(){
									$('#rank').load('<?php echo base_url();?>index.php/acs/view_manage_role1/');
								});
							}
							else
							{
								//alert('ff');
								alert("Some error occured.");
								$('#rank').html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
								$('#rank').load('<?php echo base_url();?>index.php/acs/view_manage_role1/');
							}
						}
					});
				}
			}); 
		}
		else
		{
			return false;
		}
	}
</script> 
<script>
	function edit_roles(id)
	{
		$('#rank').html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$('#rank').load('<?php echo base_url();?>index.php/acs/edit_roles/'+id);
	}
</script> 
<script>
	function stch_roles(id,status)
	{
		//alert('f');
		$('#rank').html("<center><h2>Role Status Altered Successfully</h2></center>")
		.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
		.hide()
		.fadeIn(1000,function(){
			$('#rank').load('<?php echo base_url();?>index.php/acs/stch_roles/'+id+'/'+status);
		});
	}
</script> 
