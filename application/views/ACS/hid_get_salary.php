<div class="form-group">
  <label class="col-md-3 control-label">Salary Scale </label>
  <div class="col-md-9">
    <select  class="form-control input-inline input-medium opt" name="ddsalary" id="ddsalary">
      <option value="-1">~~ Select ~~</option>
      <?php foreach($salary->result() as $rows)
		{
		?>
	  <option value="<?php echo $rows->m_salsc_id;?>"><?php echo $rows->m_salsc_scale;?></option>
	  <?php } ?>
    </select>
    <span class="help-block" id="divddsalary" style="color:red;"></span>
  </div>
</div>
