<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper" id="rank">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="fa fa-wrench"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_session">ACS</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-money"></i>
						<span>Salary Scale</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Define Salary Scale </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-money font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Define Salary Scale  </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" id="myform">
								<div class="form-body" id="rank">
									<div class="form-group">
										<label class="col-md-3 control-label">Session </label>
										<div class="col-md-6">
											<select  class="form-control input-sm opt" name="ddsession" id="ddsession">
												<option value="-1">~~ Select ~~</option>
												<?php foreach($session->result() as $rowss)
													{
														if($rows->holiday_session_id==$rowss->m_session_id)
														{
														?>
														<option value="<?php echo $rowss->m_session_id; ?>" selected="selected"><?php echo $rowss->m_session; ?></option>
														<?php }
														else
														{
														?>
														<option value="<?php echo $rowss->m_session_id; ?>"><?php echo $rowss->m_session; ?></option>
													<?php }} ?>
											</select>
											<span id="divddsession" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group" id="rank1">
										<label class="col-md-3 control-label">Salary By</label>
										<div class="col-md-6">
											<div class="radio-list">
												<label class="radio-inline">
													<input type="radio" name="rbrank" id="rbrank1" value="1" onClick="check22()">
												Hour </label>
												<label class="radio-inline">
													<input type="radio" name="rbrank" id="rbrank2" value="2" onClick="check22()">
												Day </label>
												<label class="radio-inline">
													<input type="radio" name="rbrank" id="rbrank3" value="3" onClick="check22()">
												Week </label>
												<label class="radio-inline">
													<input type="radio" name="rbrank" id="rbrank4" value="4" checked="checked" onClick="check22()">
												Month </label>
												<input type="hidden" value="4" id="txtsalby" name="txtsalby" />
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Salary Scale</label>
										<div class="col-md-6">
											<input type="text" id="txtsalary" name="txtsalary" class="form-control input-sm empty" placeholder="Enter Salary Scale">
										<span class="help-inline" id="divtxtsalary" style="color:red;"></span></div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Scale Description</label>
										<div class="col-md-6">
											<textarea id="txtscaledesc" name="txtscaledesc" class="form-control input-sm empty" placeholder="Enter Description"></textarea>
										<span class="help-inline"> </span> </div>
									</div>
									<div class="form-group" id="rank2">
										<label class="col-md-3 control-label">Status</label>
										<div class="col-md-6">
											<div class="radio-list">
												<label class="radio-inline">
													<input type="radio" name="rbranks" id="rbranks1" value="1" checked="checked" onClick="check1()">
												Enable</label>
												<label class="radio-inline">
													<input type="radio" name="rbranks" id="rbranks2" value="0"  onClick="check1()">
												Disable</label>
												<input type="hidden" value="1" id="txtstatus" name="txtstatus" />
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="button" class="btn green" onclick="add_salary_scale()" >Submit</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-money font-dark"></i>
								<span class="caption-subject bold uppercase"></span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S No.</th>
										<th>Session</th>
										<th>Salary Scale</th>
										<th>Salary By</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										$sn=1;
										foreach($salary_scale->result() as $row)
										{
											$salby='';
											if($row->salsc_salaryby==1)
											{
												$salby='Hour';
											}
											if($row->salsc_salaryby==2)
											{
												$salby='Day';
											}
											if($row->salsc_salaryby==3)
											{
												$salby='Week';
											}
											if($row->salsc_salaryby==4)
											{
												$salby='Month';
											}
										?>
										<tr>
											<td><?php echo $sn;?></td>
											<td><?php echo $row->sessions;?></td>
											<td><?php echo $row->salsc_scale;?></td>
											<td><?php echo $salby;?></td>
											<td>
												<div class="btn-group">
                                                    <button class="btn red btn-sm dropdown-toggle" data-toggle="dropdown">Action
                                                        <i class="fa fa-angle-down"></i>
													</button>
                                                    <ul class="dropdown-menu">
														
                                                        <li>
															<a href="javascript:void(0)" onclick="edit_sal_scale(<?php echo $row->salsc_id;?>)" rel="facebox"> 
																Edit
															</a>
														</li>
														<?php 
															if($row->salsc_status==1)
															{
															?>
															<li>
																
																<a href="javascript:void(0)" onclick="stch_salscale(<?php echo $row->salsc_id;?>,0)"> 
																	Disable
																</a>
															</li>
															<?php
															}
															if($row->salsc_status==0)
															{
															?>
															<li>
																<a href="javascript:void(0)" onclick="stch_salscale(<?php echo $row->salsc_id;?>,1)">
																	Enable
																</a>
															</li>
															<?php
															}
														?>
													</ul>
												</div>
											</td>
										</tr>
										<?php $sn++;
										} ?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	

<script>
	function check22()
	{
		var collection=$("#rank1");
		var inputs=collection.find("input[type=checkbox],input[type=radio]");
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			var name=inputs[x].name;
			if($("#"+id+"").is(':checked'))
			{
				if(id=="rbrank1")
				{
					$("#txtsalby").val('1');
				}
				if(id=="rbrank2")
				{
					$("#txtsalby").val('2');
				}
				if(id=="rbrank3")
				{
					$("#txtsalby").val('3');
				}
				if(id=="rbrank4")
				{
					$("#txtsalby").val('4');
				}
				
			}
		}
	}
</script>
<script>
	function check1()
	{
		var collection=$("#rank2");
		var inputs=collection.find("input[type=checkbox],input[type=radio]");
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			var name=inputs[x].name;
			if($("#"+id+"").is(':checked'))
			{
				if(id=="rbranks1")
				{
					$("#txtstatus").val('1');
				}
				if(id=="rbranks2")
				{
					$("#txtstatus").val('0');
				}
			}
		}
	}
</script>
<!-- BEGIN CONTENT -->

<script>
	function add_salary_scale()
	{
		if(check("myform"))
		{
			bootbox.confirm('Are you sure to Submit form?', function(result){
				if(result==true)
				{
					//alert('salary');
					var data={
						ddsession:$('#ddsession').val(),
						txtsalby:$('#txtsalby').val(),
						txtsalary:$('#txtsalary').val(),
						txtscaledesc:$('#txtscaledesc').val(),
						txtstatus:$('#txtstatus').val()
					};
					$.ajax(
					{
						type:"POST",
						url:"<?php echo base_url();?>index.php/acs/add_salary_scale",
						dataType:'json',
						data: data,
						success: function(msg) {
							//alert(msg);
							if(msg==1)
							{
								$(".page-content").html("<center><h2>Salary Scale Added Successfully</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(1000,function(){
									$('#rank').load('<?php echo base_url();?>index.php/acs/view_define_salary_scale1/');
								});
							}
							else
							{
								//alert('ff');
								alert("Some error occured.");
								$('#rank').html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
								$('#rank').load('<?php echo base_url();?>index.php/acs/view_add_holiday1/');
							}
							
						}
					});
				}
			}); 
		}
		else
		{
			return false;
		}
	}
</script>
<script>
	function edit_sal_scale(id)
	{
		$('#rank').html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$('#rank').load('<?php echo base_url();?>index.php/acs/view_edit_salary_scale/'+id);
	}
</script> 
<script>
	function stch_salscale(id,status)
	{
		$('#rank').html("<center><h2>Salary Scale Status Altered Successfully</h2></center>")
		.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
		.hide()
		.fadeIn(1000,function(){
			$('#rank').load('<?php echo base_url();?>index.php/acs/stch_salscale/'+id+'/'+status);
		});
	}
</script>																																					