<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>application/libraries/js/check.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<script src="<?php echo base_url() ?>application/libraries/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>application/libraries/assets/global/scripts/app.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->				
