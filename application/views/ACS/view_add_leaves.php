<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper" id="rank">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="fa fa-wrench"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_session">ACS</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-navicon"></i>
						<span>Manage Leave</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Add Leave(s) </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-navicon (alias) font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Add Leave(s)  </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" action="#" id="myform">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Leave Name</label>
										<div class="col-md-6">
											<input type="text" id="txtleavename" name="txtleavename" class="form-control input-sm empty" placeholder="Enter Leave Name" value="">
										<span id="divtxtleavename" style="color:red;"></span> </div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Type </label>
										<div class="col-md-6">
											<select  class="form-control input-sm opt" name="ddtype" id="ddtype">
												<option value="-1">~~ Select ~~</option>
												<option value="1">Vacational ( V )</option>
												<option value="2">Non Vacational ( NV )</option>
											</select>
											<span id="divddtype" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Maximum Leave(s)</label>
										<div class="col-md-6">
											<input name="txtmax" id="txtmax" type="text" class="form-control input-sm empty"  placeholder="Enter Maximum No. Leave" />
										<span id="divtxtmax" style="color:red;"></span> </div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Status</label>
										<div class="col-md-6">
											<div class="radio-list">
												<label class="radio-inline">
													<input type="radio" name="rbrank" id="rbrank1" value="1" checked onClick="check22()">
												Enable</label>
												<label class="radio-inline">
													<input type="radio" name="rbrank" id="rbrank2" value="0"  onClick="check22()">
												Disable</label>
												<input type="hidden" value="1" id="txtstatus" name="txtstatus" />
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="button" class="btn green" onclick="add_leaves()" >Submit</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-reorder font-black"></i>
								<span class="caption-subject bold uppercase"></span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th width="10%">S No.</th>
										<th width="30%">Leave Name</th>
										<th width="30%">Type</th>
										<th width="30%">Maximum Leave(s)</th>
										<th width="30%">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										$sn=1;
										foreach($leave->result() as $row)
										{
										?>
										<tr>
											<td><?php echo $sn;?></td>
											<td><?php echo $row->leave_name?></td>
											<?php if($row->leave_type==1)
												{
													$type = "Vacational";
												}
												if($row->leave_type==2)
												{
													$type = "Non Vacational";
												}
											?>
											<td><?php echo $type ?></td>
											<td><?php echo $row->leave_max?></td>
											<td>
											    <div class="btn-group">
                                                    <button class="btn red btn-sm dropdown-toggle" data-toggle="dropdown">Action
                                                        <i class="fa fa-angle-down"></i>
													</button>
                                                    <ul class="dropdown-menu">
														
                                                        <li>
															<a href="javascript:void(0)" onclick="edit_leave(<?php echo $row->leavess_id;?>)" rel="facebox">Edit</a>
														</li>
														<?php 
															if($row->leave_status==1)
															{
															?>
															<li>
																<a href="javascript:void(0)" onclick="stch_leave(<?php echo $row->leavess_id;?>,0)"> Disable</a>
															</li>
															<?php
															}
															if($row->leave_status==0)
															{
															?>
															<li>
																<a href="javascript:void(0)" onclick="stch_leave(<?php echo $row->leavess_id;?>,1)"> Enable </a>
															</li>
															<?php
															}
														?>
													</ul>
												</div>
												
											</td>
										</tr>
										<?php
											$sn++;
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	

<script>
	function check22()
	{
		var collection=$("#rank");
		var mark=0;
		var obtainmark=0;
		var inputs=collection.find("input[type=checkbox],input[type=radio]");
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			var name=inputs[x].name;
			if($("#"+id+"").is(':checked'))
			{
				if(id=="rbrank1")
				{
					$("#txtstatus").val('1');
				}
				if(id=="rbrank2")
				{
					$("#txtstatus").val('0');
				}
			}
		}
	}
</script>
<script>
	function get_date()
	{
		$('#txtamount').val('');
		var start=$("#txtstart").val();
		var end=$("#txtend").val();
		$('#amount').html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
		.hide()
		.fadeIn(1000,function(){
			$('#amount').load('<?php echo base_url();?>index.php/acs/hid_amount_of_holiday/'+start+'/'+end);
		});
	}
</script>

<script>
	function add_leaves()
	{
		if(check("myform"))
		{
			bootbox.confirm('Are you sure to Submit form?', function(result){
				if(result==true)
				{
					//alert('hello');
					var data={
						txtleavename:$('#txtleavename').val(),
						ddtype:$('#ddtype').val(),
						txtmax:$('#txtmax').val(),
						txtstatus:$('#txtstatus').val()
					};
					$.ajax(
					{
						type:"POST",
						url:"<?php echo base_url();?>index.php/acs/add_leave",
						dataType:'json',
						data:  data,
						success: function(msg) {
							//alert(msg);
							if(msg==1)
							{
								$(".page-content").html("<center><h2>Leave(s) Added Successfully</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(1000,function(){
									$('#rank').load('<?php echo base_url();?>index.php/acs/view_leave_master1/');
								});
							}
							else
							{
								//alert('ff');
								alert("Some error occured.");
								$('#rank').html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
								$('#rank').load('<?php echo base_url();?>index.php/acs/view_leave_master1/');
							}
							
						}
					});
				}
			}); 
		}
		else
		{
			return false;
		}
	}
</script> 
<script>
	function edit_leave(id)
	{
		$('#rank').html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$('#rank').load('<?php echo base_url();?>index.php/acs/edit_leave/'+id);
	}
</script> 
<script>
	function stch_leave(id,status)
	{
		$('#rank').html("<center><h2>Leave Status Altered Successfully</h2></center>")
		.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
		.hide()
		.fadeIn(1000,function(){
			$('#rank').load('<?php echo base_url();?>index.php/acs/stch_leave/'+id+'/'+status);
		});
	}
</script>																		