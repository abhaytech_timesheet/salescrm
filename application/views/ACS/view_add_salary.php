<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper" id="rank">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="fa fa-wrench"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_session">ACS</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-money"></i>
						<span>Salary Scale</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Define Salary Scale </h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-money font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Define Salary  </span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" id="myform">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Department </label>
										<div class="col-md-6">
											<select  class="form-control input-sm opt" name="dddepartment" id="dddepartment" onchange="get_roles()">
												<option value="-1">~~ Select ~~</option>
												<?php foreach($department->result() as $rowd)
													{
													?>
													<option value="<?php echo $rowd->m_des_id;?>"><?php echo $rowd->m_des_name;?></option>
												<?php } ?>
											</select>
											<span id="divdddepartment" style="color:red;"></span>
										</div>
									</div>
									
									<div id="roles">
										<div class="form-group" >
											<label class="col-md-3 control-label">Roles </label>
											<div class="col-md-6">
												<select  class="form-control input-sm opt" name="ddroles" id="ddroles">
													<option value="-1" selected="selected">~~ Select ~~</option>
												</select>
												<span id="divddroles" style="color:red;"></span>
											</div>
										</div>
									</div>
									<div class="form-group" id="rank1">
										<label class="col-md-3 control-label">Salary By</label>
										<div class="col-md-9">
											<div class="radio-list">
												<label class="radio-inline">
													<input type="radio" name="rbrank" id="rbrank1" value="1" onClick="checksal(1)">
												Hour </label>
												<label class="radio-inline">
													<input type="radio" name="rbrank" id="rbrank2" value="2" onClick="checksal(2)">
												Day </label>
												<label class="radio-inline">
													<input type="radio" name="rbrank" id="rbrank3" value="3" onClick="checksal(3)">
												Week </label>
												<label class="radio-inline">
													<input type="radio" name="rbrank" id="rbrank4" value="4" onClick="checksal(4)">
												Month </label>
												<input type="hidden" value="4" id="txtsalby" name="txtsalby" />
											</div>
										</div>
									</div>
									<div id="salary">
										<div class="form-group">
											<label class="col-md-3 control-label">Salary Scale</label>
											<div class="col-md-6">
												<select  class="form-control input-sm opt" name="ddsalary" id="ddsalary">
													<option value="-1" selected="selected">~~ Select ~~</option>
												</select>
											<span id="divddsalary" style="color:red;"></span></div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Salary Description</label>
										<div class="col-md-6">
											<textarea id="txtscaledesc" name="txtscaledesc" class="form-control input-sm empty" placeholder="Enter Description"></textarea>
										<span style="color:red;" id="divtxtscaledesc" > </span> </div>
									</div>
									
									<div class="form-group" >
										<label class="col-md-3 control-label">Allowance(s) </label>
										<div class="col-md-6">
											<div class="checkbox-list">
												<label class="checkbox-inline">
												<input type="checkbox" id="checkda" value="option1" onclick="da()"> DA </label>
												<label class="checkbox-inline">
												<input type="checkbox" id="checkhra" value="option2" onclick="hra()"> HRA </label>
											</div>
										</div>
									</div>
									<div id="da">
										<input type="hidden" id="txtda" name="txtda" class="form-control input-inline input-medium" value="0">
									</div>
									<div id="hra">
										<input type="hidden" id="txthra" name="txthra" class="form-control input-inline input-medium" value="0">
									</div>
									<div class="form-group" id="rank2">
										<label class="col-md-3 control-label">Status</label>
										<div class="col-md-6">
											<div class="radio-list">
												<label class="radio-inline">
													<input type="radio" name="rbranks" id="rbranks1" value="1" checked="checked" onClick="check1()">
												Enable</label>
												<label class="radio-inline">
													<input type="radio" name="rbranks" id="rbranks2" value="0"  onClick="check1()">
												Disable</label>
												<input type="hidden" value="1" id="txtstatus" name="txtstatus" />
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="button" class="btn green" onclick="add_salary()" >Submit</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-reorder font-black"></i>
								<span class="caption-subject bold uppercase"></span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S No.</th>
										<th>Department</th>
										<th>Role</th>
										<th>Salary By</th>
										<th>Salary Scale</th>
										<th>DA</th>
										<th>HRA</th>                  
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										$sn=1;
										foreach($salary->result() as $row)
										{
											$salby='';
											if($row->salby==1)
											{
												$salby='Hour';
											}
											if($row->salby==2)
											{
												$salby='Day';
											}
											if($row->salby==3)
											{
												$salby='Week';
											}
											if($row->salby==4)
											{
												$salby='Month';
											}
										?>
										<tr>
											<td><?php echo $sn;?></td>
											<td><?php echo $row->depart;?></td>
											<td><?php echo $row->role;?></td>
											<td><?php echo $salby;?></td>
											<td><?php echo $row->salarysc;?></td>
											<td><?php echo $row->sal_da;?> %</td>
											<td><?php echo $row->sal_hra;?> %</td>
											<td>
												<div class="btn-group">
													<button class="btn red btn-sm dropdown-toggle" data-toggle="dropdown">Action
														<i class="fa fa-angle-down"></i>
													</button>
													<ul class="dropdown-menu">
														
														<li>
															<a href="javascript:void(0)" onclick="edit_salary(<?php echo $row->sal_id;?>)" rel="facebox"> 
																Edit
															</a>
														</li>
														<?php 
															if($row->sal_status==1)
															{
															?>
															<li>
																
																<a href="javascript:void(0)" onclick="stch_salary(<?php echo $row->sal_id;?>,0)"> 
																	Disable
																</a>
															</li>
															<?php
															}
															if($row->sal_status==0)
															{
															?>
															<li>
																<a href="javascript:void(0)" onclick="stch_salary(<?php echo $row->sal_id;?>,1)">
																	Enable
																</a>
															</li>
															<?php
															}
														?>
													</ul>
												</div>
											</td>
										</tr>
										<?php $sn++;
										} ?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	

<script>
	function check22()
	{
		var collection=$("#rank1");
		var inputs=collection.find("input[type=checkbox],input[type=radio]");
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			var name=inputs[x].name;
			if($("#"+id+"").is(':checked'))
			{
				if(id=="rbrank1")
				{
					$("#txtsalby").val('1');
				}
				if(id=="rbrank2")
				{
					$("#txtsalby").val('2');
				}
				if(id=="rbrank3")
				{
					$("#txtsalby").val('3');
				}
				if(id=="rbrank4")
				{
					$("#txtsalby").val('4');
				}
				
			}
		}
	}
</script>
<script>
	function check1()
	{
		var collection=$("#rank2");
		var inputs=collection.find("input[type=checkbox],input[type=radio]");
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			var name=inputs[x].name;
			if($("#"+id+"").is(':checked'))
			{
				if(id=="rbranks1")
				{
					$("#txtstatus").val('1');
				}
				if(id=="rbranks2")
				{
					$("#txtstatus").val('0');
				}
			}
		}
	}
</script>
<script>
	function get_roles()
	{
		var department = $('#dddepartment').val();
		$("#ddroles").empty();
		$.ajax(
		{
			type:"POST",
			url:'<?php echo base_url();?>index.php/acs/hid_get_roles/'+department,
			dataType:'json',
			data:{'department':department},
			success: function(msg) {
				
				$("#ddroles").append("<option value=-1> ~~ Select ~~</option>");
				$.each(msg,function(i,item)
				{
					$("#ddroles").append("<option value="+item.m_des_id+">"+item.m_des_name+"</option>");
				});
			}
		});
		//alert(department);
		//$('#roles').html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		//$('#roles').load('<?php echo base_url();?>index.php/acs/hid_get_roles/'+department);
	}
</script>
<script>
	function da()
	{
		
		if($('#checkda').prop('checked') === true)
		{
			$('#txtda').val(1);
		}
		else
		{
			$('#txtda').val(0);
		}
		
	}
</script>
<script>
	function hra()
	{
		
		if($('#checkhra').prop('checked') === true)
		{
			$('#txthra').val(1);
		}
		else
		{
			$('#txthra').val(0);
		}
		
	}
</script>
<script>
	function checksal(id)
	{
	   $("#ddsalary").empty();
		$.ajax(
		{
			type:"POST",
			url:'<?php echo base_url();?>index.php/acs/checksal/'+id,
			dataType:'json',
			data:{'id':id},
			success: function(msg) {
				
				$("#ddsalary").append("<option value=-1> ~~ Select ~~</option>");
				$.each(msg,function(i,item)
				{
					$("#ddsalary").append("<option value="+item.m_salsc_id+">"+item.m_salsc_id+"</option>");
				});
			}
		});
		//$('#salary').html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		//$('#salary').load('<?php echo base_url();?>index.php/acs/checksal/'+id);	
	}
</script>

<script>
	function add_salary()
	{
		if(check("myform"))
		{
			bootbox.confirm('Are you sure to Submit form?', function(result){
				if(result==true)
				{
					var data={
						dddepartment:$('#dddepartment').val(),
						ddroles:$('#ddroles').val(),
						txtsalby:$('#txtsalby').val(),
						ddsalary:$('#ddsalary').val(),
						txtda:$('#txtda').val(),
						txthra:$('#txthra').val(),
						txtscaledesc:$('#txtscaledesc').val(),
						txtstatus:$('#txtstatus').val()
					};
					$.ajax(
					{
						type:"POST",
						url:"<?php echo base_url();?>index.php/acs/add_salary",
						dataType:'json',
						data: data,
						success: function(msg) {
							//alert(msg);
							if(msg==1)
							{
								$(".page-content").html("<center><h2>Salary Added Successfully</h2></center>")
								.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
								.hide()
								.fadeIn(1000,function(){
									$('#rank').load('<?php echo base_url();?>index.php/acs/view_add_salary1/');
								});
							}
							else
							{
								//alert('ff');
								alert("Some error occured.");
								$('#rank').html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
								$('#rank').load('<?php echo base_url();?>index.php/acs/view_add_salary1/');
							}
							
						}
					});
				}
			}); 
		}
		else
		{
			return false;
		}
	}
</script>
<script>
	function edit_salary(id)
	{
		$('#rank').html("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>");
		$('#rank').load('<?php echo base_url();?>index.php/acs/edit_salary/'+id);
	}
</script> 
<script>
	function stch_salary(id,status)
	{
		$('#rank').html("<center><h2>Salary Status Altered Successfully</h2></center>")
		.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif' /></center>")
		.hide()
		.fadeIn(1000,function(){
			$('#rank').load('<?php echo base_url();?>index.php/acs/stch_salary/'+id+'/'+status);
		});
	}
</script>																																