<div class="form-group">
  <label class="col-md-3 control-label">Roles </label>
  <div class="col-md-9">
    <select  class="form-control input-inline input-medium opt" name="ddroles" id="ddroles">
      <option value="-1" selected="selected">~~ Select ~~</option>
      <?php foreach($roles->result() as $rowd)
		{
		?>
	  <option value="<?php echo $rowd->m_des_id;?>"><?php echo $rowd->m_des_name;?></option>
	  <?php } ?>
    </select>
    <span class="help-block" id="divddroles" style="color:red;"></span>
  </div>
</div>
