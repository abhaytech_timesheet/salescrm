<script>
	function payment()
	{
		var mode=$("#txtpayment_mode").val();
		if(mode==2 || mode==3 || mode==4)
		{	
			$("#txtcheck_no").val("");
			$("#txtcheck_date").val("");
			$("#txtpayment_type").val("");
			$("#check").removeAttr("style");
		}
		
		if(mode==5)
		{
			$("#txtcheck_no").val("");
			$("#txtcheck_date").val("");
			$("#txtpayment_type").val("");
			$("#payment_type").removeAttr("style");
			$("#check").removeAttr("style");
		}
		
		if(mode==1 || mode==-1)
		{
			$("#txtcheck_no").val("");
			$("#txtcheck_date").val("");
			$("#txtpayment_type").val("");
			$("#payment_type").css("display","none");
			$("#check").css("display","none");
		}
	}
</script>


<script>
	function get_name(id)
	{
		if(id==1)
			var member_id=$("#txtrefer_id").val();
		else
		var member_id=$("#txtmember_id").val();
		if(member_id!='')
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url(); ?>corporate/get_member_name/",
				data: 'txtintuserid='+member_id,
				success: function(data) 
				{
					if(id==1)
					$("#txtrefer_name").val(data.trim());
					else
					$("#txtmember_name").val(data.trim());
				}
			});
		}
		else
		{
			alert('Please First Fill Refer Id');
		}
		
	}
</script>

<script>
	function get_plot_size()
	{
		$("#ddplot_no").empty();
		$("#ddplot_size").empty();
		$("#txtplot_amt").val('');
		var site_id=$("#ddsite").val();
		if(site_id!='-1')
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url(); ?>corporate/getplot_size/",
				dataType: 'json',
				data: {'site_id': site_id},
				success: function(data) 
				{
					$("#ddplot_size").empty();
					$("#ddplot_size").append("<option value=-1>Select</option>");
					$.each(data,function(i,item)
					{
						$("#ddplot_size").append("<option value="+item.m_plot_size_id+">"+item.m_plot_Area+" [ "+item.m_plot_width_ft+"`` "+item.m_plot_width_inch+"` X "+item.m_plot_height_ft+"`` "+item.m_plot_height_inch+"` ] </option>");
					});  
					get_plans(site_id);
				}
			});
		}
		else
		{
			alert("Please First Select The Site");
		}
	}
</script>

<script>
	function get_plot_no()
	{
		$("#txtplot_amt").val('');
		var site_id=$("#ddsite").val();	
		var plot_size=$("#ddplot_size").val();
		if(plot_size!='-1')
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url(); ?>corporate/getplot_no/",
				dataType: 'json',
				data: {'plot_size': plot_size,'site_id':site_id},
				success: function(data) {
					
					$("#ddplot_no").empty();
					$("#ddplot_no").append("<option value=-1>Select</option>");
					
					$.each(data,function(i,item)
					{
						$("#ddplot_no").append("<option value="+item.ID+">"+item.PLOT_NO+"</option>");
					});  
					
				}
			});
		}
		else
		{
			alert("Please First Select The Plot Size");
		}  
	}
</script>

<script>
	function get_plot_amt()
	{
		$("#txtplot_amt").val("");
		var ddplot_no=$("#ddplot_no").val();
		if(ddplot_no!='-1')
		{
			
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url(); ?>corporate/getplot_amt/",
				dataType: 'json',
				data: {'ddplot_no':ddplot_no},
				success: function(data) {
					
					$.each(data,function(i,item)
					{
						$("#txtplot_amt").val(item.PLOT_AMT);
					});  
					
				}
			});
		}
		else
		{
			alert("Please First Select The Plot No");
		}  
	}
</script>

<script>
	function get_plans(id)
	{
		if(id!='-1')
		{
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url(); ?>corporate/getplan/",
				dataType: 'json',
				data: {'site_id':id},
				success: function(data) {
					
					$("#txtpayment_plan").empty();
					$("#txtpayment_plan").append("<option value=-1>Select</option>");
					
					$.each(data,function(i,item)
					{
						$("#txtpayment_plan").append("<option value="+item.m_pm_id+">"+item.m_pm_planname+"</option>");
					});   
					
				}
			});
		}
		else
		{
			alert("Please First Select The Plot No");
		}  
	}
</script>


<script>
	function get_booking_amnt()
	{
		var site_id=$("#ddsite").val();	
		var plan_id=$("#txtpayment_plan").val();	
		
		if(site_id!='-1' && plan_id!='-1')
		{
			
			$.ajax(
			{
				type:"POST",
				url:"<?php echo base_url(); ?>corporate/getbook_amnt/",
				dataType: 'json',
				data: {'site_id':site_id,'plan_id':plan_id},
				success: function(data) {	
					//alert(data)
					$.each(data,function(i,item)
					{
						$("#txtbooking_amt").val(item.m_pm_booking_amount);		//Booking Amount
						
						/* Start Installment Type */
						if(item.m_pm_installment_type =='1')
						{
							$("#txtinstal_type").val("Daily");
						}
						if(item.m_pm_installment_type =='2')
						{
							$("#txtinstal_type").val("Weekly");
						}
						if(item.m_pm_installment_type =='3')
						{
							$("#txtinstal_type").val("Monthly");
						}
						if(item.m_pm_installment_type =='4')
						{
							$("#txtinstal_type").val("Quarterly");
						}
						if(item.m_pm_installment_type =='5')
						{
							$("#txtinstal_type").val("Half Yearly");
						}
						if(item.m_pm_installment_type =='6')
						{
							$("#txtinstal_type").val("Yearly");
						}
						if(item.m_pm_installment_type =='7')
						{
							$("#txtinstal_type").val("One Time");
						}
						/* Start Installment Type */
						
						$("#txtins_no").val(item.m_pm_installment);			// No. of installments
						$("#txtallot_rate").val(item.m_pm_allotment);		// Allotment Rate
						
						var plot_amnt = $('#txtplot_amt').val();			// Allotment Amount
						var allot_amnt = ((plot_amnt) * (item.m_pm_allotment)/100);
						$("#txtallot_amt").val(allot_amnt);
						
						$("#txtinstal_days").val(item.m_pm_install_days);	// No. of days after installment is considered
						
						$("#txtallot_days").val(item.m_pm_allot_days);		// No. of days after allotment is considered  
						
						$("#txtdiscount_rate").val(item.m_pm_discount);		// Discount Rate
						
						$("#txtinterest_rate").val(item.m_pm_interest);		// Interest on intallments
						
						if(item.m_pm_is_registry=='1')
						{
							$("#registry_hid").removeAttr('style');
							$("#txtregistry_rate").val(item.m_pm_interest);
						}
						
						if(item.m_pm_is_refundable=='1')
						{
							$("#refundable_hid").removeAttr('style');
							$("#txtrefund_rate").val(item.m_pm_refund);
						}
						
					});
					$("#hidden_div").removeAttr('style');		
				}			
			});
		}
		else
		{
			$("#hidden_div").css('display','none');
			$("#registry_hid").css('display','none');
			$("#refundable_hid").css('display','none');
			
			$("#txtbooking_amt").val('');
			$("#txtinstal_type").val('');
			$("#txtins_no").val('');	
			$("#txtallot_rate").val('');
			$("#txtallot_amt").val('');
			$("#txtinstal_days").val('');
			$("#txtallot_days").val('');
			$("#txtdiscount_rate").val('');
			$("#txtinterest_rate").val('');
			
			alert("Please First Select The Plot No");
		}  
	}
</script>

<script>
	function get_late()
	{
		var st = $('#ddlate').val();
		if(st=='1')
		{
			$("#late_hid").removeAttr('style');
		}
		else
		{
			$("#late_hid").css('display','none');
		}
	}
</script>


<script>
	function get_bal_allot()
	{
		var txtpay_amt = parseFloat($('#txtpay_amt').val());
		
		var txtallot_amt = parseFloat($('#txtallot_amt').val());
		
		//alert(txtpay_amt+" & "+txtallot_amt);
		if(txtpay_amt < txtallot_amt)
		{
			//alert("if");
			var resul = txtallot_amt - txtpay_amt;
			$('#txtbalallot_amnt').val(resul);
		}
		else
		{
			var resul = txtpay_amt - txtallot_amt;
			$('#txtbalallot_amnt').val("Exceeding "+resul);
		}
	}
</script>


<script>
	function get_balanceamt()
	{
		
		var pay_amt=$("#txtpay_amt").val();
		var alot_amt=$("#txtallot_amt").val();
		var bal_amt=(parseFloat(alot_amt)-parseFloat(pay_amt)).toFixed(2);
		
		if (parseFloat(bal_amt) <= 0)
		{
			$("#txtbal_alot_amt").val("0");
		}
		else
		{
			$("#txtbal_alot_amt").val(bal_amt);
		}
	}
</script>											