<?php
if($resul->num_rows() != 0)
{ 
foreach($resul->result() as $rowsdata)
{
}?>
<script>
window.onload = function() {
numinwrd(<?php echo $rowsdata->paid_amount; ?>);
};
</script>	
<doctype html>
<html>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<?php echo base_url() ?>application/libraries/receipt/css/bootstrap.min.css" type="text/css" rel="stylesheet">
<script src="<?php echo base_url() ?>application/libraries/receipt/js/bootstrap.min.js" type="text/javascript"></script>
<head>
<title>REAL BUSINESS WORLD</title>
</head>
<body>
<table width="950px" height="350px" align="center" border="0" cellpadding="0" cellspacing="0" style="margin-top:30px;">
  <tr>
    <th height="90px">
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <th width="26%" scope="row"><img src="<?php echo base_url(); ?>application/libraries/receipt/logo.png"></th>
          <th width="48%" align="center"> <h4 align="center">REAL BUSINESS WORLD</h4>
            <h6 align="center">392-393, HIND NAGAR, NEAR PURANI CHUNGI KANPUR ROAD</h6>
            <h6 align="center">LANDMARK: ST. MEERA'S DAY COLLEGE, LUCKNOW-12, (UP). </h6>
            <h6 align="center">Phone : +91-9670016064 || Website : www.realbusinessworld.biz</h6>
			<h4 align="center">PAYMENT RECEIPT</h4>
		  </th>
          <th width="26%">
			<div align="right"><h6>Printed Date</h6><h6><?php echo date('d-m-Y'); ?></h6>
            &nbsp;</div>
		  </th>
        </tr>
		</table>
		<table width="100%">
		<tr>
          <th width="25%" scope="row">Receipt No.:</th>
          <th width="25%"> </th>
          <th width="25%"> </th>
          <th width="25%"><div align="right"><h5>OFFICE COPY</h5></div></th>
        </tr>
      </table>
	  <table width="100%" style="font-size:13px;" >
		<tr>
          <td width="20%" height="20">Account No. :</td>
          <td width="20%" height="20"><?php echo $rowsdata->book_no; ?></td>
          <td width="20%" height="20"></td>
          <td width="20%" height="20">Pan No. :</td>
          <td width="20%" height="20"></td>
        </tr>
		<tr>
          <td width="20%" height="20">Plot No. :</td>
          <td width="20%" height="20"><?php echo $rowsdata->siteplot_no; ?></td>
          <td width="20%" height="20"></td>
          <td width="20%" height="20">Area :</td>
          <td width="20%" height="20"><?php echo $rowsdata->plotsize_area." ".$rowsdata->siteplot_unit_name; ?></td>
        </tr>
		<tr>
          <td width="20%" height="20">Project :</td>
          <td width="20%" height="20"><?php echo $rowsdata->site_name; ?></td>
          <td width="20%" height="20"></td>
          <td width="20%" height="20">Dimension :</td>
          <td width="20%" height="20"><?php echo $rowsdata->plotsize_widthf." x ".$rowsdata->plotsize_heightf; ?></td>
        </tr>
        </tr>
      </table>
	  </th>
  </tr>
  <th><img src="<?php echo base_url(); ?>application/libraries/receipt/9.png" width="950px" height="8px"></th>
  <tr>
    <th height="180px"> 	
	<table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-size:13px;">
        <tr>
          <th width="20%" height="25" scope="row"><b>Name of Purchaser</b></th>
          <td width="20%"><?php echo $rowsdata->member_name; ?></td>
          <td width="20%">&nbsp;</td>
          <td width="20%"><b>Mobile No.</b></td>
          <td width="20%"><?php echo $rowsdata->mem_mob_no; ?></td>
        </tr>
        <tr>
          <th height="25" scope="row"><b>Address :</b></th>
          <td><?php echo $rowsdata->mem_address; ?></td>
          <td>&nbsp;</td>
          <td><b>Due in Allotment</b></td>
          <td><?php echo $rowsdata->mem_mob_no; ?></td>
        </tr>
        <tr>
          <th height="25" scope="row"><b>Plot Rate :</b></th>
          <td><?php echo $rowsdata->siteplot_rate." / ".$rowsdata->siteplot_unit_name; ?></td>
          <td>&nbsp;</td>
          <td><b>Pre Location</b>:</td>
          <td><?php echo $rowsdata->site_location; ?></td>
        </tr>
        <tr>
          <th height="25" scope="row"><b>Paid Amount</b></th>
          <td><?php echo $rowsdata->paid_amount; ?></td>
          <td>&nbsp;</td>
           <td><b>(In words) <del>&#2352;</del> :</b></td>
          <td><span id="spaninwords"></span></td>
        </tr>
        <tr>
          <th height="25" scope="row"><b>As</b></th>
          <td>Installment Amount</td>
          <td>&nbsp;</td>
          <td><b>Paid By :</b></td>
          <td><?php if($rowsdata->payment_mode==1)
			  {echo $rowsdata->Payment_mode;}
			  if($rowsdata->payment_mode==2)
			  {
				echo "Cheque No. ".$rowsdata->cheque_num."<br> Bank ".$rowsdata->Payment_to;
			  }?>
		  </td>
        </tr>
        <tr>
          <th height="25" scope="row"><b>Drawn On :</b></th>
          <td><?php echo $rowsdata->Payment_mode; ?></td>
          <td>&nbsp;</td>
          <td><b>Next Due Date :</b></td>
          <td></td>
        </tr>
     </table>
	 </th>
  </tr>
 <th><img src="<?php echo base_url(); ?>application/libraries/receipt/9.png" width="950px" height="8px"></th>
  <tr>
    <th><table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <th width="50%" scope="row"><h6>***Note***</h6>
            <h6>Subject to encashment of Cheque / Draft realization.</h6>
            <h6>All the terms & conditions shall be applicable as per scheme.</h6>
            <h6>Pre location charges will be applicable.</h6>
			
		</th>
          <th width="20%"></th>
          <th width="48%"> <h6><b>Authorised Signatory.</b></h6></th>
        </tr>
      </table>
	  </th>
  </tr>
</table>
<br>
<center><img src="<?php echo base_url(); ?>application/libraries/receipt/9.png" width="950px" height="8px"></center>
<br>
<table width="950px" height="350px" align="center" border="0" cellpadding="0" cellspacing="0" style="margin-top:30px;">
  <tr>
    <th height="90px">
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <th width="26%" scope="row"><img src="<?php echo base_url(); ?>application/libraries/receipt/logo.png"></th>
          <th width="48%" align="center"> <h4 align="center">REAL BUSINESS WORLD</h4>
            <h6 align="center">392-393, HIND NAGAR, NEAR PURANI CHUNGI KANPUR ROAD</h6>
            <h6 align="center">LANDMARK: ST. MEERA'S DAY COLLEGE, LUCKNOW-12, (UP). </h6>
            <h6 align="center">Phone : +91-9670016064 || Website : www.realbusinessworld.biz</h6>
			<h4 align="center">PAYMENT RECEIPT</h4>
		  </th>
          <th width="26%">
			<div align="right"><h6>Printed Date</h6><h6><?php echo date('d-m-Y'); ?></h6>
            &nbsp;</div>
		  </th>
        </tr>
		</table>
		<table width="100%">
		<tr>
          <th width="25%" scope="row">Receipt No.:</th>
          <th width="25%"> </th>
          <th width="25%"> </th>
          <th width="25%"><div align="right"><h5>CUSTOMER COPY</h5></div></th>
        </tr>
      </table>
	  <table width="100%" style="font-size:13px;" >
		<tr>
          <td width="20%" height="20">Account No. :</td>
          <td width="20%" height="20"><?php echo $rowsdata->book_no; ?></td>
          <td width="20%" height="20"></td>
          <td width="20%" height="20">Pan No. :</td>
          <td width="20%" height="20"></td>
        </tr>
		<tr>
          <td width="20%" height="20">Plot No. :</td>
          <td width="20%" height="20"><?php echo $rowsdata->siteplot_no; ?></td>
          <td width="20%" height="20"></td>
          <td width="20%" height="20">Area :</td>
          <td width="20%" height="20"><?php echo $rowsdata->plotsize_area." ".$rowsdata->siteplot_unit_name; ?></td>
        </tr>
		<tr>
          <td width="20%" height="20">Project :</td>
          <td width="20%" height="20"><?php echo $rowsdata->site_name; ?></td>
          <td width="20%" height="20"></td>
          <td width="20%" height="20">Dimension :</td>
          <td width="20%" height="20"><?php echo $rowsdata->plotsize_widthf." x ".$rowsdata->plotsize_heightf; ?></td>
        </tr>
        </tr>
      </table>
	  </th>
  </tr>
  <th><img src="<?php echo base_url(); ?>application/libraries/receipt/9.png" width="950px" height="8px"></th>
  <tr>
    <th height="180px"> 	
	<table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-size:13px;">
        <tr>
          <th width="20%" height="25" scope="row"><b>Name of Purchaser</b></th>
          <td width="20%"><?php echo $rowsdata->member_name; ?></td>
          <td width="20%">&nbsp;</td>
          <td width="20%"><b>Mobile No.</b></td>
          <td width="20%"><?php echo $rowsdata->mem_mob_no; ?></td>
        </tr>
        <tr>
          <th height="25" scope="row"><b>Address :</b></th>
          <td><?php echo $rowsdata->mem_address; ?></td>
          <td>&nbsp;</td>
          <td><b>Due in Allotment</b></td>
          <td><?php echo $rowsdata->mem_mob_no; ?></td>
        </tr>
        <tr>
          <th height="25" scope="row"><b>Plot Rate :</b></th>
          <td><?php echo $rowsdata->siteplot_rate." / ".$rowsdata->siteplot_unit_name; ?></td>
          <td>&nbsp;</td>
          <td><b>Pre Location</b>:</td>
          <td><?php echo $rowsdata->site_location; ?></td>
        </tr>
        <tr>
          <th height="25" scope="row"><b>Paid Amount</b></th>
          <td><?php echo $rowsdata->paid_amount; ?></td>
          <td>&nbsp;</td>
           <td><b>(In words) <del>&#2352;</del> :</b></td>
          <td><span id="spaninwords1"></span></td>
        </tr>
        <tr>
          <th height="25" scope="row"><b>As</b></th>
          <td>Installment Amount</td>
          <td>&nbsp;</td>
          <td><b>Paid By :</b></td>
          <td><?php if($rowsdata->payment_mode==1)
			  {echo $rowsdata->Payment_mode;}
			  if($rowsdata->payment_mode==2)
			  {
				echo "Cheque No. ".$rowsdata->cheque_num."<br> Bank ".$rowsdata->Payment_to;
			  }?>
		  </td>
        </tr>
        <tr>
          <th height="25" scope="row"><b>Drawn On :</b></th>
          <td><?php echo $rowsdata->Payment_mode; ?></td>
          <td>&nbsp;</td>
          <td><b>Next Due Date :</b></td>
          <td></td>
        </tr>
     </table>
	 </th>
  </tr>
 <th><img src="<?php echo base_url(); ?>application/libraries/receipt/9.png" width="950px" height="8px"></th>
  <tr>
    <th><table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <th width="50%" scope="row"><h6>***Note***</h6>
            <h6>Subject to encashment of Cheque / Draft realization.</h6>
            <h6>All the terms & conditions shall be applicable as per scheme.</h6>
            <h6>Pre location charges will be applicable.</h6>
			
		</th>
          <th width="20%"></th>
          <th width="48%"> <h6><b>Authorised Signatory.</b></h6></th>
        </tr>
      </table>
	  </th>
  </tr>
</table>
<?php } ?>
</body>
<script>
	
	function numinwrd(id)
	{
		//var numbr=document.getElementById(id).value;
		var str=new String(id)   
		var splt=str.split(".");
		var sptr=new String(splt[0]);
		var splt1=sptr.split("");
		var rev=splt1.reverse();
		var once=[' Zero ', ' One ', ' Two ', ' Three ', ' Four ',  ' Five ', ' Six ', ' Seven ', ' Eight ', ' Nine '];
		var twos=[' Ten ', ' Eleven ', ' Twelve ', ' Thirteen ', ' Fourteen ', ' Fifteen ', ' Sixteen ', ' Seventeen ', ' Eighteen ', ' Nineteen '];
		var tens=[ '', 'Ten', ' Twenty', ' Thirty', ' Forty', ' Fifty', ' Sixty', ' Seventy', ' Eighty', ' Ninety' ];
		numlen=rev.length;
		var word=new Array();
		var j=0;   
		for(i=0;i<numlen;i++)
		{
			switch(i)
			{
				case 0:
				if((rev[i]==0) || (rev[i+1]==1))
				{
					word[j]='';                    
				}
				else
				{
					word[j]=once[rev[i]];
				}
				word[j]=word[j] ;
				
				break;
				case 1:
                abovetens();  
				break;
				case 2:
                if(rev[i]==0)
                {
					word[j]='';
				} 
				else if((rev[i-1]==0) || (rev[i-2]==0) )
                {
					word[j]=once[rev[i]]+"Hundred ";                
				}
                else 
                {
                    word[j]=once[rev[i]]+"Hundred and ";
				} 
				break;
				case 3:
				if(rev[i]==0 || rev[i+1]==1)
				{
					word[j]='';                    
				} 
				else
				{
					word[j]=once[rev[i]];
				}
                if((rev[i+1]!=0) || (rev[i] > 0))
                {
					word[j]= word[j]+" Thousand ";
				}
				break;  
				case 4:
				abovetens(); 
				break;  
				
				case 5:
				if((rev[i]==0) || (rev[i+1]==1))
				{
					word[j]='';                    
				} 
				else
				{
					word[j]=once[rev[i]];
				}
                word[j]=word[j]+" Lakhs ";
				break;  
				
				case 6:
				abovetens(); 
				break;
				
				case 7:
				if((rev[i]==0) || (rev[i+1]==1))
				{
					word[j]='';                    
				} 
				else
				{
					word[j]=once[rev[i]];
				}
				word[j]= word[j]+" Crore ";
				break;  
				
				case 8:
				abovetens(); 
				break;    
				default:
				break;
			}
			
			j++;  
			
		}   
		
		function abovetens()
		{
			if(rev[i]==0)
			{
				word[j]='';
			} 
			else if(rev[i]==1)
			{
				word[j]=twos[rev[i-1]];
			}
			else
			{
				word[j]=tens[rev[i]];
			}
		}
		word.reverse();
		var finalw='';
		for(i=0;i<numlen;i++)
		{
			finalw= finalw+word[i];			
		}
		//$("#inwords").html(finalw);
		document.getElementById('spaninwords').innerHTML =finalw;
		document.getElementById('spaninwords1').innerHTML =finalw;
	}
</script>
</html>