<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Plot Booking</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Registration</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Plot Booking</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Plot Booking</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form action="<?php echo base_url(); ?>index.php/corporate/insert_plot_book" class="horizontal-form" method="post" enctype="multipart/form-data"  id="myform">
								<div class="form-body">
									<h3 class="form-section">Joining Information</h3>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Refer By <span class="required"> * </span></label>
												<input class="form-control input-sm alpha_numeric" name="txtrefer_id" id="txtrefer_id" type="text" onblur="get_name(1)" />
											    <span id="divtxtrefer_id" style="color:red"></span> </div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Refer By Name </label>
												<input type="text" name="txtrefer_name" id="txtrefer_name" class="form-control input-sm" readonly />
											</div>
										</div>
									</div>
									<!--/span-->
									
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Member Id <span class="required"> * </span></label>
												<input class="form-control input-sm alpha_numeric" name="txtmember_id" id="txtmember_id" type="text" onblur="get_name(2)" />
											<span id="divtxtmember_id"  style="color:red"></span> </div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Member Name</label>
												<input type="text" name="txtmember_name" id="txtmember_name" class="form-control input-sm" readonly />
											</div>
										</div>
									</div>
									<h3 class="form-section">Booking Information</h3>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Select Site <span class="required"> * </span></label>
												<select id="ddsite" name="ddsite" class="form-control input-sm opt" onchange="get_plot_size()">
													<option value="-1">Select</option>
													<?php 
														foreach($site->result() as $row)
														{
														?>
														<option value="<?php echo $row->m_site_id; ?>"><?php echo $row->m_site_name; ?></option>
														<?php
														}
													?>
												</select>
											<span id="divddsite"  style="color:red"></span> </div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Plot Size <span class="required"> * </span></label>
												<select id="ddplot_size" name="ddplot_size" class="form-control input-sm opt" onchange="get_plot_no()">
													<option value='-1'>Select</option>
												</select>
											<span id="divddplot_size"  style="color:red"></span> </div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Select Plot No <span class="required"> * </span></label>
												<select id="ddplot_no" name="ddplot_no" class="form-control input-sm opt" onchange="get_plot_amt()">
													<option value="-1">Select</option>
												</select>
											<span id="divddplot_no"  style="color:red"></span> </div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Plot Amount <span class="required"> * </span></label>
												<input class="form-control input-sm numeric" name="txtplot_amt" id="txtplot_amt" type="text"/>
												<!-- <input class="form-control input-sm numeric" name="txtplot_amt" id="txtplot_amt" type="text" onblur="get_emi_plan()" />  --> 
											<span id="divtxtplot_amt"  style="color:red"></span> </div>
										</div>
									</div>
									<!--/span-->
									
									<h3 class="form-section">Payment Information</h3>
									<div class="row">
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Payment Plan <span class="required"> * </span></label>
												<!--<select name="txtpayment_plan" id="txtpayment_plan" onchange="get_emi_plan()" class="form-control input-sm opt">-->
												<select name="txtpayment_plan" id="txtpayment_plan" onchange="get_booking_amnt()" class="form-control input-sm opt">
													<option value="-1">Select</option>
													
												</select>
											<span id="divtxtpayment_plan"  style="color:red"></span> </div>
										</div>
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Booking Amount <span class="required"> * </span></label>
												<input type="text" class="form-control input-sm numeric" name="txtbooking_amt" id="txtbooking_amt" readonly="readonly" />
											<span id="divtxtbooking_amt"  style="color:red"></span> </div>
										</div>
                                        
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Pay Amount <span class="required"> * </span></label>
												<input type="text" class="form-control input-sm numeric" name="txtpay_amt" id="txtpay_amt" onblur="get_bal_allot()" />
											<span id="divtxtpay_amt"  style="color:red"></span> </div>
										</div>
									</div>
									
									
									<div style="display:none;" id="hidden_div">
										<div class="row">                       
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Installment Type</label>
													<input type="text" class="form-control input-sm" name="txtinstal_type" id="txtinstal_type" readonly="readonly"  />
												</div>
											</div>
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">No. of Installments</label>
													<input type="text" class="form-control input-sm" name="txtins_no" id="txtins_no" readonly="readonly" />
												</div>
											</div>
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Days after intallment is considered </label>
													<input type="text" class="form-control input-sm" name="txtinstal_days" id="txtinstal_days" readonly="readonly"  />
												</div>
											</div>
											
										</div>
										
										
										<div class="row">                       
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Allotment Rate  <small> (In Percent)</small></label>
													<input type="text" class="form-control input-sm" name="txtallot_rate" id="txtallot_rate" readonly="readonly" />
												</div>
											</div>
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Allotment Amount ( In <del>&#2352;</del> )</label>
													<input type="text" class="form-control input-sm" name="txtallot_amt" id="txtallot_amt" readonly="readonly"   />
												</div>
											</div>             
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Days after allotment is considered </label>
													<input type="text" class="form-control input-sm" name="txtallot_days" id="txtallot_days" readonly="readonly" />
												</div>
											</div>
											
										</div>
										
									</div>                  
									
									<div class="row">                    
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Balance Allotment Amount <small> ( In <del>&#2352;</del> )</small></label>
												<input type="text" class="form-control input-sm" name="txtbalallot_amnt" id="txtbalallot_amnt"  />
											</div>
										</div>
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Discount Rate <small> ( In Percent )</small></label>
												<input type="text" class="form-control input-sm" name="txtdiscount_rate" id="txtdiscount_rate"  />
											</div>
										</div>
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Interest Rate on EMI<sub> ( s )</sub> <small> &nbsp;( In Percent )</small></label>
												<input type="text" class="form-control input-sm" name="txtinterest_rate" id="txtinterest_rate"  />
											</div>
										</div>
										
									</div>
									
									<div class="row">              
										<div id="registry_hid" style="display:none">
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Registry Rate  <small> ( In Percent )</small></label>
													<input type="text" class="form-control input-sm" name="txtregistry_rate" id="txtregistry_rate" readonly="readonly" />
												</div>
											</div>
										</div>
										
										<div id="refundable_hid" style="display:none">
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Refund Rate ( In Percent )</label>
													<input type="text" class="form-control input-sm" name="txtrefund_rate" id="txtrefund_rate" readonly="readonly"   />
												</div>
											</div>             
										</div>
									</div>
									
									<div class="row">              
										
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Late Fees Consideration </label>
												<select name="ddlate" id="ddlate" onchange="get_late()" class="form-control input-sm opt">
													<option value="-1">Select</option>
													<option value="1">Yes</option>
													<option value="0">No</option>
												</select>
											</div>
										</div>
										
										<div id="late_hid" style="display:none">
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Days after late fees is considered</label>
													<input type="text" class="form-control input-sm" name="txtlate_day" id="txtlate_day" />
												</div>
											</div>
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Late Fees Rate</label>
													<input type="text" class="form-control input-sm" name="txtlate_rate" id="txtlate_rate" />
												</div>
											</div>
										</div>
										
									</div>
									
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Booking Date</label>
												<input type="text" class="form-control input-sm" name="txtpayment_date" id="txtpayment_date" data-date-format="yyyy-mm-dd" value="<?php echo date('Y-m-d'); ?>" readonly="readonly" />
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Payment Mode</label>
												<select name="txtpayment_mode" id="txtpayment_mode" class="form-control input-sm opt" onchange="payment()">
													<option value="-1">Select</option>
													<option value="1">Cash</option>
													<option value="2">Cheque</option>
													<option value="3">Demand Draft</option>
													<option value="4">Bankers Cheque</option>
													<option value="5">Other</option>
												</select>
											<span id="divtxtpayment_mode"  style="color:red"></span> </div>
										</div>
										<div class="col-md-4" id="payment_type" style="display:none;">
											<div class="form-group">
												<label class="control-label">&nbsp</label>
													<input type="text" class="form-control input-sm" name="txtpayment_type" id="txtpayment_type">
												</div>
											</div>
										</div>
										
										<div class="row" id="check" style="display:none;">
											<div class="form-group">
												<div class="col-md-4">
													<div class="form-group">
														<label class="control-label">Cheque/DD No</label>
														<input type="text" class="form-control input-sm" name="txtcheck_no" id="txtcheck_no">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<label class="control-label">Bank</label>
														<select name="ddbank" id="ddbank" class="form-control input-sm" >
															<option value="-1">Select</option>
															<?php 
																foreach($bank->result() as $b)
																{
																?>
																<option value="<?php echo $b->m_bank_id; ?>"><?php echo $b->m_bank_name; ?></option>
																<?php
																}
															?>
														</select>
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<label class="control-label">Cheque/DD Date</label>
														<input type="text" class="form-control input-sm date-picker" name="txtcheck_date" id="txtcheck_date" data-date-format="yyyy-mm-dd">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group">
												<div class="col-md-8">
													<div class="form-group">
														<label class="control-label">Remark</label>
														<textarea  class="form-control input-sm" name="txtremark" id="txtremark" rows='3'></textarea>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-actions fluid">
										<div class="col-md-offset-4 col-md-8">
											<button type="button" class="btn green m-icon" onclick="return conwv('myform')"> Save <i class="m-icon-swapright m-icon-white"></i></button>
											<button type="reset" class="btn default">Reset <i class="m-icon-swapleft"></i></button>
										</div>
									</div>
								</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->
