<?php
	
	foreach($plotsize->result() as $rowps) 
	{
	}?>
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label">Plot Size <span class="required"> * </span></label>
			<select id="ddplot_size" name="ddplot_size" class="form-control otp" onchange="get_plot_no()">
				<option value='-1'>Select</option>
				<option value="<?php echo $rowps->m_plot_size_id; ?>" selected><?php echo $rowps->m_plot_Area." [ ".$rowps->m_plot_width_ft."`.".$rowps->m_plot_width_inch."`` X ".$rowps->m_plot_height_ft."`.".$rowps->m_plot_height_inch."`` ]"; ?></option>
				
			</select>
		<span id="divddplot_size" class="help-block" style="color:red"></span> </div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label class="control-label">Select Plot No <span class="required"> * </span></label>
			<select id="ddplot_no" name="ddplot_no" class="form-control otp" onchange="get_plot_amt()">
				<option value="-1">Select</option>
				<option value="<?php echo $rowps->Plot_id; ?>" selected><?php echo $rowps->Plot_no; ?></option>     
			</select>
		<span id="divddplot_no" class="help-block" style="color:red"></span> </div>
	</div>
		