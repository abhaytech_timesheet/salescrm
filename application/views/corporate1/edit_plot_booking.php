<?php
	if($resul->num_rows() != 0)
	{ 
		foreach($resul->result() as $rowsdata)
		{
		}
	?>
	<script>
		window.onload = function(){
			get_name(1);
			get_name(2);
			var site_id = $('#ddsite').val();
			var siteplot = "<?php echo $rowsdata->plotsize_id;?>";
			$('#plot_size').load('<?php echo base_url(); ?>corporate/loadplot/'+site_id+'/'+siteplot+'/');
			var payment_plan = <?php echo $rowsdata->book_payment_plan;?>;
			$('#hid_payment_plan').load('<?php echo base_url(); ?>corporate/loadpaymentplan/'+payment_plan+'/'+site_id+'/');
			get_bal_allot();
		};
	</script>
	
	<div id="stylized">
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<!-- BEGIN CONTENT BODY -->
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="icon-globe"></i>
							<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Plot Booking</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<i class="fa fa-location-arrow"></i>
							<span>Registration</span>
							<i class="fa fa-angle-right"></i>
						</li>
					</ul>
					<div class="page-toolbar">
						<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
							<i class="icon-calendar"></i>&nbsp;
							<span class="thin uppercase hidden-xs"></span>&nbsp;
							<i class="fa fa-angle-down"></i>
						</div>
					</div>
				</div>
				<!-- END PAGE BAR -->
				<!-- BEGIN PAGE TITLE-->
				<h3 class="page-title"> PLot Booking</h3>
				<!-- END PAGE TITLE-->
				
				<div class="row">
					<div class="col-md-12">
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-cursor"></i>
									<span class="caption-subject bold uppercase">PLot Booking</span>
								</div>
								
								<div class="actions">
									<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
										<i class="icon-cloud-upload"></i>
									</a>
									<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
										<i class="icon-wrench"></i>
									</a>
									<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
									<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
										<i class="icon-trash"></i>
									</a>
								</div>
							</div>
							<div class="portlet-body form">
								<form action="<?php echo base_url(); ?>corporate/update_plot_book" class="horizontal-form" method="post" enctype="multipart/form-data" id="myform" >
									<div class="form-body">
										<h3 class="form-section">Joining Information</h3>
										
										<!--/span-->
										
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Member Id <span class="required"> * </span></label>
													<input class="form-control alpha_numeric" name="txtmember_id" id="txtmember_id" type="text" onblur="get_name(2)" value="<?php echo $rowsdata->member_user_id; ?>" readonly="readonly"/>
												<span id="divtxtmember_id" style="color:red"></span> </div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Member Name</label>
													<input type="text" name="txtmember_name" id="txtmember_name" class="form-control" readonly />
												</div>
											</div>
										</div>
										<h3 class="form-section">Booking Information</h3>
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Select Site <span class="required"> * </span></label>
													<select id="ddsite" name="ddsite" class="form-control opt" onchange="get_plot_size()">
														<option value="-1">Select</option>
														<?php 
															foreach($site->result() as $row)
															{
															?>
															<option value="<?php echo $row->m_site_id; ?>" <?php echo ($rowsdata->site_id==$row->m_site_id ? "selected='selected'" : "" ); ?> ><?php echo $row->m_site_name; ?></option>
															<?php
															}
														?>
													</select>
												<span id="divddsite" style="color:red"></span> </div>
											</div>
											<div id="plot_size">
												<div class="col-md-4">
													<div class="form-group">
														<label class="control-label">Plot Size <span class="required"> * </span></label>
														<select id="ddplot_size" name="ddplot_size" class="form-control opt" onchange="get_plot_no()">
															<option value='-1'>Select</option>
														</select>
													<span id="divddplot_size" style="color:red"></span> </div>
												</div>
												
												<div class="col-md-4">
													<div class="form-group">
														<label class="control-label">Select Plot No <span class="required"> * </span></label>
														<select id="ddplot_no" name="ddplot_no" class="form-control opt" onchange="get_plot_amt()">
															<option value="-1">Select</option>
														</select>
													<span id="divddplot_no" style="color:red"></span> </div>
												</div>
											</div>
											
										</div>
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Plot Amount <span class="required"> * </span></label>
													<input class="form-control numeric" name="txtplot_amt" id="txtplot_amt" type="text" value="<?php echo $rowsdata->siteplot_amount; ?>"/>
													<!-- <input class="form-control numeric" name="txtplot_amt" id="txtplot_amt" type="text" onblur="get_emi_plan()" />  --> 
												<span id="divtxtplot_amt" style="color:red"></span> </div>
											</div>
										</div>
										<!--/span-->
										
										<h3 class="form-section">Payment Information</h3>
										<div class="row">
											
											<div id="hid_payment_plan">
												<div class="col-md-4">
													<div class="form-group">
														<label class="control-label">Payment Plan <span class="required"> * </span></label>
														<!--<select name="txtpayment_plan" id="txtpayment_plan" onchange="get_emi_plan()" class="form-control opt">-->
														<select name="txtpayment_plan" id="txtpayment_plan" onchange="get_booking_amnt()" class="form-control opt">
															<option value="-1">Select</option>                                                    
														</select>
													<span id="divtxtpayment_plan" style="color:red"></span> </div>
												</div>
											</div>
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Booking Amount <span class="required"> * </span></label>
													<input type="text" class="form-control numeric" name="txtbooking_amt" id="txtbooking_amt" readonly="readonly" value="<?php echo $rowsdata->plan_bookamnt; ?>" />
												<span id="divtxtbooking_amt" style="color:red"></span> </div>
											</div>
											
											<?php 
												foreach($ins_data->result() as $rowinstall) 
												{
													break;
												}
											?> 
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Pay Amount <span class="required"> * </span></label>
													<input type="text" class="form-control numeric" name="txtpay_amt" id="txtpay_amt" value="<?php echo $rowinstall->payment_amt; ?>" onblur="get_bal_allot()" readonly="readonly" />
												<span id="divtxtpay_amt" style="color:red"></span> </div>
											</div>
										</div>
										
										<?php
											$install_type ='';
											if($rowsdata->plan_installment_type =='1')
											{
												$install_type = "Daily";
											}
											if($rowsdata->plan_installment_type =='2')
											{
												$install_type ="Weekly";
											}
											if($rowsdata->plan_installment_type =='3')
											{
												$install_type ="Monthly";
											}
											if($rowsdata->plan_installment_type =='4')
											{
												$install_type ="Quarterly";
											}
											if($rowsdata->plan_installment_type =='5')
											{
												$install_type ="Half Yearly";
											}
											if($rowsdata->plan_installment_type =='6')
											{
												$install_type ="Yearly";
											}
											if($rowsdata->plan_installment_type =='7')
											{
												$install_type ="One Time";
											}
										?>
										<div>
											<div class="row">                       
												<div class="col-md-4">
													<div class="form-group">
														<label class="control-label">Installment Type</label>
														<input type="text" class="form-control" name="txtinstal_type" id="txtinstal_type" readonly="readonly" value="<?php echo $install_type; ?>"  />
													</div>
												</div>
												
												<div class="col-md-4">
													<div class="form-group">
														<label class="control-label">No. of Installments</label>
														<input type="text" class="form-control" name="txtins_no" id="txtins_no" readonly="readonly" value="<?php echo $rowsdata->plan_installment; ?>" />
													</div>
												</div>
												
												<div class="col-md-4">
													<div class="form-group">
														<label class="control-label">Days after intallment is considered </label>
														<input type="text" class="form-control" name="txtinstal_days" id="txtinstal_days" readonly="readonly" value="<?php echo $rowsdata->plan_install_days; ?>"  />
													</div>
												</div>
												
											</div>
											
											
											<div class="row">                       
												
												<div class="col-md-4">
													<div class="form-group">
														<label class="control-label">Allotment Rate  <small> (In Percent)</small></label>
														<input type="text" class="form-control" name="txtallot_rate" id="txtallot_rate" readonly="readonly" value="<?php echo $rowsdata->plan_allot; ?>"  />
													</div>
												</div>
												
												<div class="col-md-4">
													<?php $allot_amnt=0.00;
													$allot_amnt = $rowsdata->siteplot_amount * $rowsdata->plan_allot; ?>
													<div class="form-group">
														<label class="control-label">Allotment Amount ( In <del>&#2352;</del> )</label>
														<input type="text" class="form-control" name="txtallot_amt" id="txtallot_amt" readonly="readonly" value="<?php echo $allot_amnt; ?>"  />
													</div>
												</div>             
												
												<div class="col-md-4">
													<div class="form-group">
														<label class="control-label">Days after allotment is considered </label>
														<input type="text" class="form-control" name="txtallot_days" id="txtallot_days" readonly="readonly" value="<?php echo $rowsdata->plan_allotdays; ?>" />
													</div>
												</div>
												
											</div>
											
										</div>                  
										
										<div class="row">                
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Balance Allotment Amount <small> ( In <del>&#2352;</del> )</small></label>
													<input type="text" class="form-control" name="txtbalallot_amnt" id="txtbalallot_amnt"  />
												</div>
											</div>
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Discount Rate <small> ( In Percent )</small></label>
													<input type="text" class="form-control" name="txtdiscount_rate" id="txtdiscount_rate" value="<?php echo $rowsdata->plan_discount; ?>"  />
												</div>
											</div>
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Interest Rate on EMI<sub> ( s )</sub> <small> &nbsp;( In Percent )</small></label>
													<input type="text" class="form-control" name="txtinterest_rate" id="txtinterest_rate" value="<?php echo $rowsdata->plan_interest; ?>"  />
												</div>
											</div>                    
										</div>
										
										<div class="row"> 
											<?php if($rowsdata->plan_isregistry == 1)
												{ ?>             
												<div id="registry_hid">
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Registry Rate  <small> ( In Percent )</small></label>
															<input type="text" class="form-control" name="txtregistry_rate" id="txtregistry_rate" readonly="readonly" value="<?php echo $rowsdata->plan_registry; ?>" />
														</div>
													</div>
												</div>
												<?php }
												else
												{ ?>
												<div id="registry_hid" style="display:none">
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Registry Rate  <small> ( In Percent )</small></label>
															<input type="text" class="form-control" name="txtregistry_rate" id="txtregistry_rate" readonly="readonly" />
														</div>
													</div>
												</div>
											<?php } ?>
											
											<?php if($rowsdata->plan_refundable == 1)
												{ ?>
												<div id="refundable_hid">
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Refund Rate ( In Percent )</label>
															<input type="text" class="form-control" name="txtrefund_rate" id="txtrefund_rate" readonly="readonly"  value="<?php echo $rowsdata->plan_refund; ?>"  />
														</div>
													</div>             
												</div>
												<?php }
												else
												{ ?>
												
												<div id="refundable_hid" style="display:none">
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Refund Rate ( In Percent )</label>
															<input type="text" class="form-control" name="txtrefund_rate" id="txtrefund_rate" readonly="readonly"   />
														</div>
													</div>             
												</div>
											<?php } ?>
										</div>
										
										<div class="row">              
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Late Fees Consideration </label>
													<select name="ddlate" id="ddlate" onchange="get_late()" class="form-control opt">
														<option value="-1">Select</option>
														<?php if($rowsdata->book_islate==1) 
															{?>                             
															<option value="1" selected="selected">Yes</option>
															<option value="0" >No</option>
															<?php }
															else
															{ ?>
															<option value="1">Yes</option>
															<option value="0" selected="selected">No</option>
														<?php } ?>
													</select>
												</div>
											</div>
											
											<?php if($rowsdata->book_islate==1) 
												{?> 
												<div id="late_hid">
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Days after late fees is considered</label>
															<input type="text" class="form-control" name="txtlate_day" id="txtlate_day" value="<?php echo $rowsdata->book_latedays; ?>" />
														</div>
													</div>
													
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Late Fees Rate</label>
															<input type="text" class="form-control" name="txtlate_rate" id="txtlate_rate" value="<?php echo $rowsdata->book_laterate; ?>" />
														</div>
													</div>
												</div>
												
												<?php } 
												else
												{?>                     
												<div id="late_hid" style="display:none">
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Days after late fees is considered</label>
															<input type="text" class="form-control" name="txtlate_day" id="txtlate_day" />
														</div>
													</div>
													
													<div class="col-md-4">
														<div class="form-group">
															<label class="control-label">Late Fees Rate</label>
															<input type="text" class="form-control" name="txtlate_rate" id="txtlate_rate" />
														</div>
													</div>
												</div>
											<?php } ?>     
										</div>
										
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Booked Date</label>
													<input type="text" class="form-control" name="txtpayment_date" id="txtpayment_date" data-date-format="yyyy-mm-dd" value="<?php echo $rowsdata->book_date; ?>" readonly="readonly" />
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Payment Mode</label>
													<select name="txtpayment_mode" id="txtpayment_mode" class="form-control opt" onchange="payment()">
														<option value="-1" selected="selected">Select</option>
															<option value="1" <?php echo ($rowinstall->payment_mode==1 ? "selected='selected'" : "" );?> >Cash</option>
															<option value="2" <?php echo ($rowinstall->payment_mode==2 ? "selected='selected'" : "" );?> >Cheque</option>
															<option value="3" <?php echo ($rowinstall->payment_mode==3 ? "selected='selected'" : "" );?> >Demand Draft</option>
															<option value="4" <?php echo ($rowinstall->payment_mode==4 ? "selected='selected'" : "" );?> >Bankers Cheque</option>
															<option value="5" <?php echo ($rowinstall->payment_mode==5 ? "selected='selected'" : "" );?> >Other</option>
													</select>
												<span id="divtxtpayment_mode" style="color:red"></span> </div>
											</div>
											
											<div class="col-md-4" id="payment_type" style="display:none;">
												<div class="form-group">
													<label class="control-label">&nbsp</label>
														<input type="text" class="form-control" name="txtpayment_type" id="txtpayment_type">
													</div>
												</div>
											</div>
											
											<?php if($rowinstall->payment_mode!=1)
												{ ?>
												<div class="row" id="check">
													<div class="form-group">
														<div class="col-md-4">
															<div class="form-group">
																<label class="control-label">Cheque/DD No</label>
																<input type="text" class="form-control" name="txtcheck_no" id="txtcheck_no" value="<?php echo $rowinstall->payment_check_no; ?>">
															</div>
														</div>
														<div class="col-md-4">
															<div class="form-group">
																<label class="control-label">Bank</label>
																<select name="ddbank" id="ddbank" class="form-control" >
																	<option value="-1">Select</option>
																	<?php 
																		foreach($bank->result() as $b)
																		{
																		?>
																		<option value="<?php echo $b->m_bank_id; ?>"><?php echo $b->m_bank_name; ?></option>
																		<?php
																		}
																	?>
																</select>
															</div>
														</div>
														<div class="col-md-4">
															<div class="form-group">
																<label class="control-label">Cheque/DD Date</label>
																<input type="text" class="form-control date-picker" name="txtcheck_date" id="txtcheck_date" data-date-format="yyyy-mm-dd" value="<?php echo $rowinstall->payment_check_date;  ?>">
															</div>
														</div>
													</div>
												</div>
												<?php }
												else
												{ ?>
												<div class="row" id="check" style="display:none;">
													<div class="form-group">
														<div class="col-md-4">
															<div class="form-group">
																<label class="control-label">Cheque/DD No</label>
																<input type="text" class="form-control" name="txtcheck_no" id="txtcheck_no">
															</div>
														</div>
														<div class="col-md-4">
															<div class="form-group">
																<label class="control-label">Bank</label>
																<select name="ddbank" id="ddbank" class="form-control" >
																	<option value="-1">Select</option>
																	<?php 
																		foreach($bank->result() as $b)
																		{
																		?>
																		<option value="<?php echo $b->m_bank_id; ?>"><?php echo $b->m_bank_name; ?></option>
																		<?php
																		}
																	?>
																</select>
															</div>
														</div>
														<div class="col-md-4">
															<div class="form-group">
																<label class="control-label">Cheque/DD Date</label>
																<input type="text" class="form-control date-picker" name="txtcheck_date" id="txtcheck_date" data-date-format="yyyy-mm-dd">
															</div>
														</div>
													</div>
												</div>
											<?php } ?>
											<div class="row">
												<div class="form-group">
													<div class="col-md-8">
														<div class="form-group">
															<label class="control-label">Remark</label>
															<textarea  class="form-control" name="txtremark" id="txtremark" rows='3'></textarea>
														</div>
													</div>
												</div>
											</div>
										</div>
										<input type="hidden" name="txtplan_book_id" id="txtplan_book_id" value="<?php echo $rowsdata->book_id; ?>"><br>
										<div class="form-actions fluid">
											<div class="col-md-offset-4 col-md-8">
												<button type="button" class="btn btn-info m-icon" onclick="return conwv('myform')" > Update <i class="m-icon-swapright m-icon-white"></i></button>
												&nbsp;&nbsp;&nbsp;&nbsp;<button type="reset" class="btn btn-danger">Reset <i class="fa fa-m-icon-swapleft"></i></button>
											</div>
										</div>
									</form>
								</div>
							</div>
							
						</div>
						<!-- END PAGE CONTENT-->
						
					</div>
				</div>
				<!-- END CONTENT BODY -->
			</div>
		</div>
		<!-- END CONTENT -->
		
		
	<?php } ?>															