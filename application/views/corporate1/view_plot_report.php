<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
					    <i class="icon-globe"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_mainconfig">Plot Booking</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-location-arrow"></i>
						<span>Registration</span>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Plot Booking</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-cursor"></i>
								<span class="caption-subject bold uppercase">Plot Booking</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>Action</th>
										<th>S No.</th>
										<th class="ignore">Plan No.</th>
										<th>Member Id / Name</th>
										<th>Site</th>
										<th>Plot Size</th>
										<th>Plot No</th>
										<th>Payment Plan</th>
										<th>Plot Amount(BSP)</th>
										<th>Plot Rate</th>                  
										<th>Allotment Rate</th>   
										<th>Booking Amount</th>  
										<th>Allotment Amount</th>                  
										
									</tr>
								</thead>
								<tbody>
									<?php 
										$sn = 1;
										foreach($plot_report->result() as $rowinfo)
										{ 
										?>
										<tr>
											<td>
												<a href="<?php echo base_url();?>corporate/edit_plotbooking/<?php echo $rowinfo->book_id?>" onclick="return confirm('Are you sure want to edit ?')" title="Edit Plot"><i class="fa fa-edit"></i></a>
											</td>
											<td><?php echo $sn; ?></td>
											<td><?php echo $rowinfo->book_no; ?></td>
											<td><?php echo $rowinfo->member_id." / ".$rowinfo->member_name; ?></td>
											<td><?php echo $rowinfo->site_name; ?></td>
											<td><?php echo $rowinfo->plotsize_widthf."`.".$rowinfo->plotsize_widthi."`` X ".$rowinfo->plotsize_heightf."`.".$rowinfo->plotsize_heighti."``"." ".$rowinfo->plotsize_unit_name; ?></td>
											<td><?php echo $rowinfo->siteplot_no; ?></td>
											<td><?php echo $rowinfo->plan_name; ?></td>
											<td><?php echo $rowinfo->siteplot_amount; ?></td>
											<td><del>&#2352;</del> <?php echo $rowinfo->siteplot_rate." Per ".$rowinfo->siteplot_unit_name; ?></td>
											<td><?php echo $rowinfo->plan_allot; ?> %</td>
											<td><del>&#2352;</del> <?php echo $rowinfo->plan_bookamnt; ?> </td>
											<td><del>&#2352;</del> <?php echo $rowinfo->siteplot_amount * $rowinfo->plan_allot; ?> </td>
										</tr>
										<?php $sn++; 
										} ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->

