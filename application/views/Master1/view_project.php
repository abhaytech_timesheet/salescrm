<!-- BEGIN CONTENT -->
<div id="stylized">
	<div class="page-content-wrapper">
		<div class="page-content" >		
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Create Project 
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo base_url();?>index.php/crm/view_Project">CRM</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?php echo base_url();?>index.php/crm/view_Project"> Create Project</a>
						</li>
					<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			
            
            <div class="row">
			<!--row-->
            <div class="col-md-6">
					<!-- BEGIN PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Add New Project
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<!--BEGIN TABS-->
							<div class="tabbable tabbable-custom">
							
                          	<div id="content_email">
								<div id="content_email_temp">
                                <div data-always-visible="1" data-rail-visible="0">	
                                
                                	<div class="form-group">                                     
                                            <label>Project Name</label>
                                            <input class="form-control input-large" placeholder="Enter Project Name" name="txtpname1" id="txtpname" type="text" />
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Date</label>
											<div class="input-group input-large date-picker input-daterange" data-date="2012/11/10/" data-date-format="yyyy/mm/dd">
                                                            <input type="text" class="form-control" name="from" id="txtfrom">
                                                            <span class="input-group-addon">
                                                                 to
                                                            </span>
                                                            <input type="text" class="form-control" name="to" id="txtto">
                                            </div>
									</div>
                                    
                                    <div class="form-group">
										<label>Attactment</label>
                                        <div class="clear"></div>
											<div class="fileinput fileinput-new" data-provides="fileinput">
												<span class="btn default btn-file">
													<span class="fileinput-new">
														 Select file
													</span>
													<span class="fileinput-exists">
														 Change
													</span>
													<input type="file" name="fileatt" id="txtfileatt">
												</span>
												<span class="fileinput-filename">
												</span>
												 &nbsp;
												<a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">
												</a>
											</div>
                                      </div>
                                      
                                      
                                      <div class="form-group">
										<label>Project Description</label>
											<textarea class="form-control" rows="4" PLACEHOLDER="Enter Project Description Information" name="txtProject_des" id="txtProject_des"></textarea>
                                      </div>
                                    
                                    
                             		<div id="function" class="form-actions">
											    <button type="button" onclick="insert()" class="btn blue"><i class="fa fa-check"></i> Save</button>
											    <button type="button" class="btn default">Cancel</button>
												
									</div>
									</div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- END PAGE CONTENT-->
            
            <div class="col-md-6">
                	<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>View Projects</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
                            	<th>
									 S No.
								</th>
								<th>
									Project Name
								</th>
                                <th>
									Start Date
								</th>
                                 <th>
									 End Date
								</th>
                                <th>
									 Action
								</th>
							</tr>
							</thead>
							
                            <tbody>
                            <?php
							$sn=1;
							foreach($rec->result() as $row)
							{
							?>
                               <tr class="odd gradeX">
                                    <td><?php echo $sn; ?></td>
                                    <td><?php echo $row->m_project_name; ?></td>
                                    <td><?php echo $row->m_project_startdate; ?></td>
                                    <td><?php echo $row->m_project_enddate; ?></td>
                                    <td>
                                    <a href="<?php echo base_url();?>index.php/crm/edit_project/<?php echo $row->m_project_id ?>">
                       					<span class="btn btn-xs purple"><i class="fa fa-edit"></i> Edit</span>
                                    </a>
                                    <?php
									if($row->m_project_stauts=='1')
									{
										?>
									<a href="<?php echo base_url();?>index.php/crm/project_disable/<?php echo $row->m_project_id ?>">
                       					<span class="btn red btn-xs"><i class="fa fa-times"></i> Disable</span>
                                    </a>
                                        
                                      <?php }
									  else
									  {
										  ?>
									 <a href="<?php echo base_url();?>index.php/crm/project_enable/<?php echo $row->m_project_id ?>">
                       					<span class="btn btn-xs green"><i class="fa fa-plus"></i> Enable</span>
                                     </a>
									  <?php }
									  ?>
                                      
                                      
                                    </td>
                                </tr>
                            <?php
							$sn++;
							}
							?>
							</tbody>
                           
							</table>
						</div>
					</div>
			</div>
            	
		</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/clockface/js/clockface.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/core/app.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/components-pickers.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
        jQuery(document).ready(function() {       
           // initiate layout and plugins
           App.init();
           ComponentsPickers.init();
        });   
    </script>
<!-- END JAVASCRIPTS -->



<script>
function insert()
{
	alert('fsdfs');
	var project_name = $('#txtpname').val();
    var from = $('#txtfrom').val();
	var to = $('#txtto').val();
    var file1= $('#txtfileatt').val();
	var fileatt = file1.replace(/^.*\\/, "");
	var description = $('#txtProject_des').val();
	$.ajax(
        {
 	type: "POST",
 	url:"<?php echo base_url();?>index.php/crm/insert_project/",
 	data:"txtpname="+project_name+"&txtfrom="+from+"&txtto="+to+"&txtfileatt="+fileatt+"&txtProject_des="+description,
 	success: function(msg) {
 	      if(msg=="true")
		    {
			$(".page-content").html("<center><h2>Project Added Successfully!</h2></center>")
			.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/img/loading-spinner-blue.gif' /></center>")
			.hide()
			.fadeIn(1000,function()
			{
			$("#stylized").load("<?php echo base_url().'index.php/crm/after_view_project'?>");
			}
			);

	}	  
         }
       });
}
</script>



