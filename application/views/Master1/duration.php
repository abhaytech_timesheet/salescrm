<?php
	foreach($month->result() as $row)
	{
		$month=$row->p_mst_pl_depositterm;
		if($row->p_plantype_id=='1')
		{
			$name="Single Installment Plan";
		}
		else
		{
			$name="Multiple Installment Plan";
		}
	}
?>

<div class="col-md-4">
	<div class="form-group">
		<label class="control-label">Plan Name</label>
		<input type="text" class="form-control" name="txtplanname" id="txtplanname" readonly="readonly" value="<?php echo $name; ?>">
	</div>
</div>
<!--/span-->
<div class="col-md-4">
	<div class="form-group">
		<label class="control-label">Term of Plan (months)</label>
		<input type="text" class="form-control" name="txttermofplan" id="txttermofplan" value="<?php echo $month; ?>" readonly="readonly">
	</div>
</div>
