<!-- BEGIN FOOTER -->
<div class="footer">
	<div class="footer-inner">
		 <?php echo date('Y') ?> &copy; Ferry Info Pvt. Ltd.
	</div>
	<div class="footer-tools">
		<span class="go-top">
			<i class="fa fa-angle-up"></i>
		</span>
	</div>
</div>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script type="text/javascript" src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/typeahead/typeahead.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url() ?>application/libraries/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>application/libraries/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>application/libraries/assets/plugins/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>

<script src="<?php echo base_url() ?>application/libraries/assets/scripts/core/app.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/index.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/table-advanced.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/components-form-tools.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/components-pickers.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/scripts/custom/ui-alert-dialog-api.js"></script>
<script>
jQuery(document).ready(function() {       
     // initiate layout and plugins
      App.init();
      Index.initDashboardDaterange();
      TableAdvanced.init();
	  ComponentsPickers.init();
	  ComponentsFormTools.init();
	  UIAlertDialogApi.init();

});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>