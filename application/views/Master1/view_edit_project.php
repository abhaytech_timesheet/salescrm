<?php foreach($rec->result() as $row)
	{
	}
?>
<!-- BEGIN CONTENT -->
<div id="stylized">
	<div class="page-content-wrapper">
		<div class="page-content" >		
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
						Create Project 
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="<?php echo base_url();?>index.php/crm/view_Project">CRM</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?php echo base_url();?>index.php/crm/view_Project"> Create Project</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="">
								<i class="fa fa-calendar"></i>
								<span>
								</span>
								
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			
            <div class="row">
				<!--row-->
				<div class="col-md-6">
					<!-- BEGIN PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Add New Project
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<!--BEGIN TABS-->
							<div class="tabbable tabbable-custom">
								
								<div id="content_email">
									<div id="content_email_temp">
										<div data-always-visible="1" data-rail-visible="0">	
											
											<div class="form-group">                                     
												<label>Project Name</label>
												<input class="form-control input-large" placeholder="Enter Project Name" name="txtpname" id="txtpname" type="text" value="<?php echo $row->m_project_name; ?>" />
												<ul>  
													<div id="result"></div>  
												</ul> 
											</div>
											
											<div class="form-group">
												<div>
													<label>Date</label>
													<div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy/mm/dd">
														<input type="text" class="form-control" name="from" id="txtfrom" value="<?php echo $row->m_project_startdate; ?>">
														<span class="input-group-addon">
															to
														</span>
														<input type="text" class="form-control" name="to" id="txtto" value="<?php echo $row->m_project_enddate; ?>">
													</div>
												</div>
											</div>
											
											
											<div class="form-group">
												<label>Project Description</label>
												<textarea class="form-control" rows="5" PLACEHOLDER="Enter Project Description Information" name="txtProject_des" id="txtProject_des"><?php echo $row->m_project_description; ?></textarea>
											</div>
											
											
											<div id="function" class="form-actions">
												<button type="button" onclick="update()" class="btn blue">Update</button>
												<button type="button" class="btn default">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
            	
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->


<script>
	function update()
	{
		var project_name = $('#txtpname').val();
		var from = $('#txtfrom').val();
		var to = $('#txtto').val();
		var description = $('#txtProject_des').val();
		$.ajax(
        {
			type: "POST",
			url:"<?php echo base_url();?>index.php/crm/update_project/<?php echo $this->uri->segment(3);?>",
			data:"txtpname="+project_name+"&txtfrom="+from+"&txtto="+to+"&txtProject_des="+description,
			success: function(msg) {
				if(msg=="true")
				{
					$(".page-content").html("<center><h2>Project Updated Successfully!</h2></center>")
					.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/img/loading-spinner-blue.gif' /></center>")
					.hide()
					.fadeIn(1000,function()
					{
						$("#stylized").load("<?php echo base_url().'index.php/crm/after_view_project'?>");
					}
					);
					
				}	  
			}
		});
	}
</script>