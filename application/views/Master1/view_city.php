<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="#.">SETTINGS</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Configuration</span>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> City/District</h3>
			<!-- END PAGE TITLE-->
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-globe font-black"></i>
								<span class="caption-subject font-black bold uppercase">City/District</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" action="<?php echo base_url().'index.php/master/add_city/'?>" method="post" id="myform" onsubmit="return check(conwv('myform'))">
								<div class="form-body" id="city">
									<div class="form-group">
										<label class="col-md-3 control-label">Select State</label>
										<div class="col-md-6">
											<select id="ddstate" name="ddstate" class="form-control input-sm opt">
												<option selected="selected" value="-1">Select State</option>
												<?php foreach($city->result() as $row)
													{
													?><option value="<?php echo $row->m_loc_id;?>"><?php echo $row->m_loc_name;?></option>
													<?php
													}
												?>
											</select>
											<span id="divddstate" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">City/District Name</label>
										<div class="col-md-6">
											<input type="text" id="txtcity" name="txtcity" class="form-control input-sm empty" placeholder="Enter City Name">
											<span id="divtxtcity" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Status</label>
										<div class="col-md-6">
											<div class="radio-list">
												<label class="radio-inline">
												<input type="radio" name="rbcity" id="rbcity1" value="1" checked onClick="check()"> Enable</label>
												<label class="radio-inline">
												<input type="radio" name="rbcity" id="rbcity2" value="0"  onClick="check()"> Disable</label>
												<input type="hidden" value="1" id="txtstatus" name="txtstatus" />
											</div>
										</div>
									</div>
									
								</div>
								<div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Submit</button>
										<button type="button" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
							
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="icon-globe font-dark"></i>
								<span class="caption-subject bold uppercase"></span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>S No.</th>
										<th>State Name</th>
										<th>City Name</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										$sn=1;
										foreach($city->result() as $row)
										{
											foreach($allcity->result() as $allrow)
											{
												if($row->m_loc_id==$allrow->m_parent_id)
												{
													$status=0;
													if($allrow->m_status==1)
													{
														$status="Enable";
													}
													if($allrow->m_status==0)
													{
														$status="Disable";
													}
												?>
												<tr><td><?php echo $sn;?></td>
													<td><?php echo $row->m_loc_name?></td>
													<td><?php echo $allrow->m_loc_name?></td>
													<td>
													    <div class="btn-group">
															<button class="btn red btn-sm dropdown-toggle" data-toggle="dropdown">Action
																<i class="fa fa-angle-down"></i>
															</button>
															<ul class="dropdown-menu">
																
																<li>
																	<a href="<?php echo base_url();?>index.php/master/edit_city/<?php echo $allrow->m_loc_id;?>" rel="facebox" >Edit</a>
																</li>
																<?php 
																	if($status=="Enable")
																	{
																	?>
																	<li>
																		<a href="<?php echo base_url();?>index.php/master/update_status_city/<?php echo $allrow->m_loc_id;?>/0">Disable</a>
																	</li>
																	<?php
																	}
																	if($status=="Disable")
																	{
																	?>
																	<li>
																		<a href="<?php echo base_url();?>index.php/master/update_status_city/<?php echo $allrow->m_loc_id?>/1">Enable</a>
																	</li>
																	<?php
																	}
																?>
															</ul>
														</div>
													</td>
												</tr>
												<?php
													$sn++;
												}
											}
											
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->														