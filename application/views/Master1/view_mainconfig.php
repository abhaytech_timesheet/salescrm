<?php
	foreach($config->result() as $row)
	{
		break;
	}
?>
<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="<?php echo base_url();?>index.php/master/index/">Manage Software Configuration</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Master</span>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Software Configuration</h3>
			<!-- END PAGE TITLE-->
			
			
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-cursor font-purple"></i>
								<span class="caption-subject font-purple bold uppercase">Software Configuration</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" role="form" method="post" action="<?php echo base_url().'index.php/master/update_mainconfig/'?>" onsubmit="return conwv()">
								<div class="form-body" id="rank">
									
									<div class="form-group">
										<label class="col-md-3 control-label">Project Name</label>
										<div class="col-md-6">
											<input type="text" id="txtprojname" name="txtprojname"  class="form-control input-sm" placeholder="Enter Project Name." value="<?php echo $row->m00_sitename;?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Email Address</label>
										<div class="col-md-6">
											<input type="text" id="txtemail" name="txtemail" readonly="true" class="form-control input-sm" placeholder="Enter Email Address." value="<?php echo $row->m00_email;?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">User Name</label>
										<div class="col-md-6">
											<input type="text" id="txtusername" name="txtusername" class="form-control input-sm" placeholder="Enter User Name." value="<?php echo $row->m00_username;?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">User Password</label>
										<div class="col-md-6">
											<input type="password" id="txtuserpass" name="txtuserpass" class="form-control input-sm" placeholder="Enter User Password." value="<?php echo $row->m00_password;?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">User Pinpassword</label>
										<div class="col-md-6">
											<input type="password" id="txtuserpinpass" name="txtuserpinpass" class="form-control input-sm" placeholder="Enter User Pin Password." value="<?php echo $row->m00_pinpassword;?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label">Description</label>
										<div class="col-md-6">
											<textarea id="txtdesc" name="txtdesc" rows="3" col="9" placeholder="Enter Project Description." class="form-control input-sm"><?php echo $row->m00_description;?></textarea>
										</div>
									</div>
									
								</div>
								<div class="form-actions">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Update</button>
										<button type="reset" class="btn default">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->