<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="#.">SETTINGS</a>
						<i class="fa fa-circle"></i>
					</li>
					<li>
						<span>Configuration</span>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Sub Apps </h3>
			<!-- END PAGE TITLE-->
			<?php
				foreach($smenu->result() as $row)
				{
				}
			?>
			<div class="row">
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-square font-black"></i>
								<span class="caption-subject font-black bold uppercase"> Manage Sub Apps</span>
							</div>
							
							<div class="actions">
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
								<a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form name="myForm" id="fsunm" action="<?php echo base_url();?>index.php/master/edit_submenu/<?php echo $id; ?>" method="post"  class="form-horizontal" onsubmit="return conwv('fsunm')">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label">Apps Name</label>
										<div class="col-md-9">
											<select id="main_menu" name="main_menu" class="form-control input-sm opt">
												<option value="-1">Select Main Apps</option>
												<?php foreach($menu->result() as $row1)
													{
														if($row->m_menu_id==$row1->m_menu_id)
														{
														?>
														<option value="<?php echo $row1->m_menu_id;?>" selected="selected"><?php echo $row1->m_menu_name;?></option>
														<?php
														}
														else
														{
														?>
														<option value="<?php echo $row1->m_menu_id;?>"><?php echo $row1->m_menu_name;?></option>
														<?php
														}
													}?>
											</select>
											<span id="divmain_menu" style="color:red;"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Sub Apps Name</label>
										<div class="col-md-9">
											<input type="text" id="txtsname" name="txtsname" value="<?php echo $row->m_submenu_name;?>" class="form-control input-sm empty" />
											<span id="divtxtsname" style="color:red;"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Sub Apps Link</label>
										<div class="col-md-9">
											<input type="text" id="txtslink" name="txtslink" value="<?php echo $row->m_sbcatlink;?>" class="form-control input-sm empty" />
											<span id="divtxtslink" style="color:red;"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Sub Apps Description</label>
										<div class="col-md-9">
											<textarea rows="5" id="txtsdesc" name="txtsdesc" class="form-control input-sm empty"><?php echo $row->m_submenu_desc;?></textarea>
											<span id="divtxtsdesc" style="color:red;"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label"> Sub Apps Status</label>
										<div class="col-md-9">
											<div class="radio-list">
												<label class="radio-inline">
												<div class="radio" id="uniform-optionsRadios25"><input type="radio"  name="optionsRadios1" id="optionsRadios1" checked="checked" onClick="check()"></div> Enable </label>
												<label class="radio-inline">
												<div class="radio" id="uniform-optionsRadios26"><input type="radio" name="optionsRadios1" id="optionsRadios2" onClick="check()"></div> Disable </label>
												
											</div>
											<input type="hidden" value="1" id="txtstatus" name="txtstatus" />
										</div>
									</div>
									<div class="form-actions fluid">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn blue">Update</button>
											<button type="button" class="btn">Cancel</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
				
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-square font-dark"></i>
								<span class="caption-subject bold uppercase"></span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th>Apps</th>
										<th>Sub Apps</th>
										<th>Sub Apps Link</th>
										<th>Action</th>
										
									</tr>
								</thead>
								<tbody>
									<?php 
										$mmenu="";
										
										foreach($submenu->result() as $row1)
										{$status=0;
											if($row1->m_submenu_status==1)
											{
												$status="Enable";
											}
											if($row1->m_submenu_status==0)
											{
												$status="Disable";
											}
											foreach($menu->result() as $row)
											{
												if($row->m_menu_id==$row1->m_menu_id)
												{
													$mmenu=$row->m_menu_name;
												}
											}
										?>
										<tr>
											<td><?php echo $mmenu; ?></td>
											<td><?php echo $row1->m_submenu_name;?></td>
											<td><?php echo $row1->m_sbcatlink;?></td>
											<td>
												<div class="btn-group">
													<button class="btn red btn-sm dropdown-toggle" data-toggle="dropdown">Action
														<i class="fa fa-angle-down"></i>
													</button>
													<ul class="dropdown-menu">
														
														<li>
															<a href="<?php echo base_url();?>index.php/master/edit_smenu/<?php echo $row1->m_submenu_id;?>" rel="facebox">Edit</a>
														</li>
														<?php 
															if($status=="Enable")
															{
															?>
															<li>
																<a href="<?php echo base_url();?>index.php/master/st_chsubmst/<?php echo $row1->m_submenu_id;?>/0">Disable	</a>
															</li>
															<?php
															}
															if($status=="Disable")
															{
															?>
															<li>
																<a href="<?php echo base_url();?>index.php/master/st_chsubmst/<?php echo $row1->m_submenu_id;?>/1">Enable</a>
															</li>
															<?php
															}
														?>
													</ul>
												</div>
											</td>
										</tr>
										<?php
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->	
<script>
	function check()
	{
		var collection=$("#fsunm");
		var mark=0;
		var obtainmark=0;
		var inputs=collection.find("input[type=checkbox],input[type=radio]");
		for(var x=0;x<inputs.length;x++)
		{
			var id=inputs[x].id;
			var name=inputs[x].name;
			if($("#"+id+"").is(':checked'))
			{
				if(id=="optionsRadios1")
				{
					$("#txtstatus").val('1');
				}
				if(id=="optionsRadios2")
				{
					$("#txtstatus").val('0');
				}
			}
		}
	}
	function fill_city()
	{
		
		var state_id=$("#txtstate").val();
		$("#city").html('<div><img src ="<?php echo base_url();?>application/lib_images/loading.gif" alt="Loading....." title="Loading...."></div>');
		$("#city").load("<?php echo base_url();?>index.php/master/select_city/"+state_id);
	}
	
</script>
