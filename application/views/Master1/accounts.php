<!-- BEGIN CONTENT -->
<div id="stylized">
	<div class="page-content-wrapper">
		<div class="page-content" >		
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Master  
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="">Account</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href=""> Head</a>
							<i class="fa fa-angle-right"></i>
						</li>
					
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-6">
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>Categories
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div id="tree_2" class="tree-demo">
							</div>
						</div>
					</div>
				</div>
                                
                   <div class="col-md-6">
					<div class="tabbable tabbable-custom boxless tabbable-reversed">
							  <div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption"> <i class="fa fa-reorder"></i>Details</div>
										<div class="tools">
											<a href="javascript:;" class="collapse">
											</a>
											<a href="#portlet-config" data-toggle="modal" class="config">
											</a>
											<a href="javascript:;" class="reload">
											</a>
											<a href="javascript:;" class="remove">
											</a>
										</div>
									</div>
                                    <div id="div_load">
                                    <div class="portlet-body form">
										<div class="form-body">
                       				<div class="form-group">
										<label>General Ledger A/C No.</label>
										<input type="text" class="form-control input-large" id="txttitle" name="txttitle" placeholder="Enter General Ledger A/C No" value="" />
										
									</div>
                                    
                                    <div class="form-group">
										<label>General Ledger A/C Name</label>
										<input type="text" class="form-control input-large" id="txttitle" name="txttitle" placeholder="Enter General Ledger A/C Name" value="" />
										
									</div>
                                    
                                    <div class="form-group">
										<label>Sub Ledger A/C No.</label>
										<input type="text" class="form-control input-large" id="txttitle" name="txttitle" placeholder="Enter Sub Ledger A/C No." value="" />
										
									</div>
                                    
                                    <div class="form-group">
										<label>Sub Ledger A/C Name</label>
										<input type="text" class="form-control input-large" id="txttitle" name="txttitle" placeholder="Enter Sub Ledger A/C Name." value="" />
										
									</div>
                                    
                                  <div class="form-actions">
									<button type="button" class="btn default">Cancel</button>
									<button type="button" class="btn blue"><i class="fa fa-check"></i> Save</button>						
								</div>
							</div>
						</div>
                       </div>
                    </div>
                </div>		
               </div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>

<!-- END CONTAINER -->

<script>
var UITree = function () {
var handleSample2 = function () {
        $('#tree_2').jstree({
            'plugins': ["wholerow", "types"],
            'core': {
                "themes" : {
                    "responsive": false
                },    
                'data': [
                		<?php 
							$mmenu="";
							foreach($menu->result() as $row)
							{
							if($row->m_acc_parent==0)
							{
							$mmenu=$row->m_acc_name;
							$accno=$row->m_acc_num;
							$smenu=$row->m_acc_id;
							$parent=$row->m_acc_parent;
							?>
							
						{
						
                        		"text": "<?php echo "<a href='#' onclick='go1($smenu)'>$mmenu</a> - <a href='#' onclick='go1($smenu)'>$accno</a>" ?>",
                        		"children": 
                            	[
								  <?php 
								  foreach($menu->result() as $row1)
										{
											 if($row1->m_acc_parent==$smenu)
											 {
													$sub=$row1->m_acc_name;
													$sub_id=$row1->m_acc_id;
													$accno=$row1->m_acc_num;
								  ?>
									{
                                    "text": "<?php echo "<a href='#' onclick='go1($sub_id)'>$sub</a>-<a href='#' onclick='go1($sub_id)'>$accno</a>" ?>",
									"children": 
										[
										  <?php 
										  foreach($menu->result() as $row2)
											 {
											   if($row2->m_acc_parent==$sub_id)
												{
													$sub2=$row2->m_acc_name;
													$sub_id2=$row2->m_acc_id;
													$accno=$row2->m_acc_num;
											?>
											{
											"text": "<?php echo "<a href='#' onclick='go1($sub_id2)'>$sub2</a>-<a href='#' onclick='go1($sub_id2)'>$accno</a>" ?>",
											"children": 
											[
											  <?php 
											  foreach($menu->result() as $row3)
												 {
												   if($row3->m_acc_parent==$sub_id2)
													{
														$sub3=$row3->m_acc_name;
														$sub_id3=$row3->m_acc_id;
														$accno=$row3->m_acc_num;
												?>
												{
												"text": "<?php echo "<a href='#' onclick='go1($sub_id3)'>$sub3</a>-<a href='#' onclick='go1($sub_id3)'>$accno</a>" ?>",
												"children": 
												[
												  <?php 
												  foreach($menu->result() as $row4)
													 {
													   if($row4->m_acc_parent==$sub_id3)
														{
															$sub4=$row4->m_acc_name;
															$sub_id4=$row4->m_acc_id;
															$accno=$row4->m_acc_num;
													?>
													{
													"text": "<?php echo "<a href='#' onclick='go1($sub_id4)'>$sub4</a>-<a href='#' onclick='go1($sub_id4)'>$accno</a>"  ?>",
													"children": 
													[
													  <?php 
													  foreach($menu->result() as $row5)
														 {
														   if($row5->m_acc_parent==$sub_id4)
															{
																$sub5=$row5->m_acc_name;
																$sub_id5=$row5->m_acc_id;
																$accno=$row5->m_acc_num;
														?>
														{
														"text": "<?php echo "<a href='#' onclick='go1($sub_id5)'>$sub5</a>-<a href='#' onclick='go1($sub_id5)'>$accno</a>" ?>",
														"children": 
														[
														  <?php 
														  foreach($menu->result() as $row6)
															 {
															   if($row6->m_acc_parent==$sub_id5)
																{
																	$sub6=$row6->m_acc_name;
																	$sub_id6=$row6->m_acc_id;
																	$accno=$row6->m_acc_num;
															?>
															{
															"text": "<?php echo "<a href='#' onclick='go1($sub_id6)'>$sub6</a>-<a href='#' onclick='go1($sub_id6)'>$accno</a>" ?>",
														},
															  <?php
																	}
																}
															  ?>
														]
													},
														  <?php
																}
															}
														  ?>
													]
												},
													  <?php
															}
														}
													  ?>
												]
											},
												  <?php
														}
													}
												  ?>
											]
										},
											  <?php
													}
												}
											  ?>
										]
                                },
									  <?php
											}
										}
									  ?>
                            ]
							
                    	},
						<?php
							}
							}
							?>
                        
                ]
                
                
            },
           
        });
    }
	return {
        //main function to initiate the module
        init: function () {

            handleSample2();

        }

    };

}();
</script>
<script>
function go1(id)
{
 var sid =id;
//alert(sid);
	$('#div_load').load("<?php echo base_url()?>index.php/master/account_load/"+sid+"/");
 //alert ("ff");
}
</script>