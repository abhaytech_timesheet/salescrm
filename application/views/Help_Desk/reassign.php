<form method="post" name="status" id="status"  ">
<input type="hidden" name="hd<?php echo $id; ?>" id="hd<?php echo $id; ?>" value="<?php echo $id; ?>" />
<input type="hidden" name="old_assign_id<?php echo $id; ?>" id="old_assign_id<?php echo $id; ?>" value="<?php echo $assign_id; ?>" />
<select name="ddemp_name<?php echo $id; ?>" id="ddemp_name<?php echo $id; ?>" class="form-control input-sm" style="float:left;">
	<option value="-1">Select</option>
	<?php
		foreach($emp->result() as $row2)
		{
			if(($tickt_type==1||$tickt_type==3) && $row2->user_id!=$assign_id )
			{
				if($row2->order_id==1)
				{
				?>
				<option value="<?php echo $row2->user_id; ?>"><?php echo $row2->user_name; ?></option>
				<?php
				}
			}
			if(($tickt_type==$row2->order_id && $row2->order_id!=1 ) && $row2->user_id!=$assign_id)
			{
			?>                 
			<option value="<?php echo $row2->user_id; ?>"><?php echo $row2->user_name; ?></option>
			<?php
			}
		}
	?>
</select>
<button type="button" class="btn green" style="float:left;" onClick="save(<?php echo $id; ?>)">Submit</button>
</form>

<script>
	function save(id)
	{ 
		if($('#ddemp_name'+id).val()!="-1")
		{
			var hidden = $('#hd'+id).val();
			var empname = $('#ddemp_name'+id).val();
			var oldid = $('#old_assign_id'+id).val();
			$.ajax(
			{
				type: "POST",
				url:"<?php echo base_url();?>helpdesk/task_again_assign/",
				data:"&hd="+hidden+"&ddemp_name="+empname+"&old_assign_id="+oldid,
				success: function(msg) {
					if(msg!="")
					{
						$(".page-content").html("<center><h2>"+msg+"</h2></center>")
						.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/global/img/loading-spinner-blue.gif' /></center>")
						.hide()
						.fadeIn(1000,function()
						{
							$("#stylized").load("<?php echo base_url().'helpdesk/assign/'?>");
						}
						);
					}
					else
					{
						alert("Some Error on this page");
					}
				}
			});
		}
		else
		{
			alert('Please Select Employee!!');
			$('#ddemp_name'+id).focus();
		}
		
	}
	
	
</script>