<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-desktop"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/index/">
							Help Desk
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-user-plus"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_assign_ticket/">
							Assign Ticket
						</a>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title">Assign Ticket</h3>
			<!-- END PAGE TITLE-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-ticket font-dark"></i>
								<span class="caption-subject bold uppercase">Ticket</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								
								<thead>
									<tr>
										<th>
											#
										</th>
										<th>
											Department
										</th>
										<th>
											Subject
										</th>
										<th>
											Status
										</th>
										<th>
											Urgency
										</th>
										<th>
											Date
										</th>
										<th>
											Assign to Employee
										</th>
										<th class="ignore">
											Reassign Ticket
										</th>
									</tr>
								</thead>
								<tbody>
									<?php  
										$sn=1;
										foreach($rec->result() as $row)
										{
										?>
										<tr>
											<td>
												<?php echo $sn; ?>
											</td>
											<td>
												<?php echo $row->Department ?>
											</td>
											<td>
												<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_ticket/<?php echo $row->Ticket_reference_no ?>" style="color:#0066FF; text-decoration:none;">
													<?php echo '#'.($row->Ticket_reference_no).' - '.$row->Subject ?>
													<input type="hidden" name="hd" id="hd" value="<?php echo $row->Ticket_reference_no ?>" />
												</a>
											</td>
											<td>
												<?php echo $row->C_Status; ?>
											</td>
											<td>
												<?php echo $row->Urgency;?>
											</td>
											<td>
												<?php echo substr($row->Sub_date,0,10) ?>
											</td>
											<td>
												<?php echo $row->User_name; ?></td>
											<td>
												<div id="ignore<?php echo $row->Ticket_reference_no; ?>">
													<?php 
														if($row->C_Status=='ACTIVE')
														{
														?>
														<a class="btn green" onclick="popup('<?php echo $row->Ticket_reference_no; ?>',<?php echo $row->Assign_to_id; ?>,<?php echo $row->Dep_id; ?>)">
															Reassign <i class="fa fa-mail-reply-all"></i>
														</a>
														<?php
														}
													?>
												</div>
											</td>
										</tr>
										<?php
											$sn++;
										}
										
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->

<script>
	function popup(id,assign_id,tickt_type)
	{ 
		var id=id;
		$("#ignore"+id).html('<div><img src ="<?php echo base_url();?>application/libraries/assets/layouts/layout/img/loading-spinner-blue.gif" alt="Loading....." title="Loading...."></div>');
		$("#ignore"+id).load("<?php echo base_url();?>helpdesk/view_reassign/"+id+"/"+assign_id+"/"+tickt_type);
	}
</script>																										