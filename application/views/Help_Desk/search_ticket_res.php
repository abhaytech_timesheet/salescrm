<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-desktop"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/index/">
							Help Desk
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
					    <i class="fa fa-search"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/search_ticket/">
							Search Ticket
						</a>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> Search Ticket</h3>
			<!-- END PAGE TITLE-->
			
			
			<div class="row">
				<div class="col-md-12">
					
					<div class="table-responsive" id="data1">
						
						<table class="table table-striped table-bordered table-hover table-full-width" id="datatable_products">
							<thead>
								<tr role="row" class="heading">
									
									
									<th width="15%">
										Department
									</th>
									<th>
										Account ID
									</th>
									<th width="15%">
										Account Name
									</th>
									<th>
										Employee ID
									</th>
									<th width="15%">
										Employee Name
									</th>
									<th width="15%">
										Date&nbsp;Created
									</th>
									<th width="10%">
										Status
									</th>
									<th width="10%">
										Actions
									</th>
								</tr>
								
								<form method="post" action="<?php echo base_url()?>index.php/admin/search_ticket" onsubmit="return check_date()">
									<tr role="row" class="filter">
										<td>
											<select name="dddept" id="dddept" class="form-control form-filter input-sm">
												<option value="">Select...</option>
												<option value="1">Support</option>
												<option value="2">Sales</option>
												<option value="3">Development</option>
												<option value="4">Complaints</option>
											</select>
										</td>
										<td>
											<input type="text" class="form-control form-filter input-sm" name="txtaccid" id="txtaccid">
											
										</td>
										<td>
											<input type="text" class="form-control form-filter input-sm" name="txtaccname" id="txtaccname">
											
										</td>
										<td>
											<input type="text" class="form-control form-filter input-sm" name="txtempid" id="txtempid">
											
										</td>
										<td>
											<input type="text" class="form-control form-filter input-sm" name="txtempname" id="txtempname">
											
										</td>
										
										
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="txtfrom" id="txtfrom" placeholder="From">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="txtto" id="txtto" placeholder="To">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										<td>
											<select name="ddstatus" id="ddstatus" class="form-control form-filter input-sm">
												<option value="">Select...</option>
												<option value="0">DEACTIVE</option>
												<option value="1">ACTIVE<b>/</b>ASSIGNED</option>
												<option value="2">ACTIVE<b>/</b>UNASSIGNED</option>
												
											</select>
										</td>
										<td>
											<div class="margin-bottom-5">
												<input type="submit" name="submit" id="submit" value="Submit" class="btn green"/>
											</div>
											
										</td>
									</tr>
								</form>
							</thead>
						</table>
						
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-ticket font-dark"></i>
								<span class="caption-subject bold uppercase">Ticket</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								
								<thead>
									<tr>
										<th>
											#
										</th>
										<th>
											Department
										</th>
										
										<th>
											Created Date
										</th>
										<th>
											Resolved Date
										</th>
										<th>
											Account Name
										</th>
										<th>
											Assign to Employee
										</th>
										<th>
											Subject
										</th>
										<th>
											Status
										</th>
									</tr>
									
								</thead>
								<tbody>
									<?php 
										$sn=1;
										foreach($ticket->result() as $row)
										{
										?>
										<tr>
											<td>
												<?php echo $sn;?> 
											</td>
											<td>
												
												<?php echo $row->Department;?>
											</td>
											
											<?php 
												$date1 = date_create( $row->Sub_date );
												$date2 = date_create( $row->Resolve_date );
											?>
											
											<td>
												<?php echo date_format($date1, 'Y-m-d');?>
											</td>
											<td>
												<?php echo date_format($date2, 'Y-m-d');?>
											</td>
											<td>
												<?php echo $row->ACCOUNT;?>&nbsp;<b style="color:red;">/</b>&nbsp;<br><?php echo $row->AccountID;?>
											</td>
											<td>
												
												<?php echo $row->User_name;?>&nbsp;<b style="color:red;">/</b>&nbsp;<br><?php echo $row->EmployeeID;?>
												
											</td>
											<td>
												<a href="<?php echo base_url();?>index.php/<?php echo $this->router->fetch_class();?>/view_ticket/<?php echo $row->Ticket_reference_no ?>" style="color:#0066FF; text-decoration:none;">
													<?php echo '#'.($row->Ticket_reference_no).' - '.$row->Subject ?>
													<input type="hidden" name="hd" id="hd" value="<?php echo $row->Ticket_reference_no ?>" />
												</a>
												
											</td>
											<?php
												$st='';
												if($row->C_Status=='ACTIVE')
												{
													$st="<button type='button' class='btn btn-xs green'>ACTIVE</button>";
												}
												if($row->C_Status=='DEACTIVE')
												{
													$st="<button type='button' class='btn btn-xs red'>DEACTIVE</button>";
												}
											?>
											<td>
												<?php echo $st;?> 
											</td>
										</tr>
										<?php
											$sn++;
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>

<script>
	function check_date()
	{
		var txtfrom=$("#txtfrom").val();
		var txtto=$("#txtto").val();
		if(txtfrom=='')
		{
			alert('Please Mention From Date');
			return false;
		}
		if(txtto=='')
		{
			alert('Please Mention To Date');
			return false;
		}
		else
		{
			return true;
		}
	}
</script>															