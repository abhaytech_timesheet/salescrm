<div id="stylized">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			
			<!-- BEGIN PAGE BAR -->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-desktop"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/index/">
							Help Desk
						</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-ticket"></i>
						<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/index/">
							All Ticket
						</a>
					</li>
				</ul>
				<div class="page-toolbar">
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="icon-calendar"></i>&nbsp;
						<span class="thin uppercase hidden-xs"></span>&nbsp;
						<i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<!-- END PAGE BAR -->
			<!-- BEGIN PAGE TITLE-->
			<h3 class="page-title"> View All Ticket</h3>
			<!-- END PAGE TITLE-->
			
			
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive" id="data1">
						<table class="table table-striped table-bordered table-hover table-full-width" id="datatable_products">
							<thead>
								<tr role="row" class="heading">
									
									
									<th width="15%">
										Department
									</th>
									<!--<th>
										Account Name
									</th>
									<th width="15%">
										Account ID
									</th>-->
									<th width="15%">
										Date&nbsp;Created
									</th>
									<th width="10%">
										Actions
									</th>
								</tr>
								
								<form method="post" action="<?php echo base_url()?><?php echo $this->router->fetch_class();?>/index" onsubmit="return conwv1()" >
									<tr role="row" class="filter">
										<td>
											<select name="dddept" id="dddept" class="form-control form-filter input-sm">
												<option value="">Select...</option>
												<option value="1">Support</option>
												<option value="2">Sales</option>
												<option value="3">Development</option>
												<option value="4">Complaints</option>
											</select>
										</td>
										<td>
											<div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="txtfrom" id="txtfrom" placeholder="From">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" class="form-control form-filter input-sm" readonly name="txtto" id="txtto" placeholder="To">
												<span class="input-group-btn">
													<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</td>
										
										<td>
											<div class="margin-bottom-5">
												<input type="submit" name="submit" id="submit" value="Submit" class="btn green"/>
											</div>
											
										</td>
									</tr>
								</form>
							</thead>
						</table>
						
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-dark">
								<i class="fa fa-ticket font-dark" ></i>
								<span class="caption-subject bold uppercase">View All Ticket</span>
							</div>
							<div class="tools"> </div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								
								<thead>
									<tr>
										<th>
											S No.
										</th>
										<th>
											Department
										</th>
										<th>
											Subject
										</th>
										<th>
											Status
										</th>
										<th>
											Urgency
										</th>
										<th>
											Date
										</th>
										<th>
											Assign to
										</th>
										<th>
											Action
										</th>
									</tr>
									
								</thead>
								<tbody>
									<?php  
										$sn=1;
										foreach($rec->result() as $row)
										{
											if(($row->C_Status)!="DEACTIVE")
											{
												
											?>
											<tr>
												<td>
													<?php echo $sn; ?>
												</td>
												<td>
													<?php echo $row->Department ?>
												</td>
												<td>
													<a href="<?php echo base_url();?><?php echo $this->router->fetch_class();?>/view_ticket/<?php echo $row->Ticket_reference_no ?>" style="color:#0066FF; text-decoration:none;">
														<?php echo '#'.($row->Ticket_reference_no).' - '.$row->Subject ?>
														<input type="hidden" name="hd<?php echo $row->Ticket_id; ?>" id="hd<?php echo $row->Ticket_id; ?>" value="<?php echo $row->Ticket_reference_no?>" />
													</a>
												</td>
												<td>
													<?php echo $row->C_Status; ?>
												</td>
												<td>
													<?php 
														echo $row->Urgency;
													?>
												</td>
												<td>
													<?php echo substr($row->Sub_date,0,10) ?>
												</td>
												
												<td>
													<?php 
														if(($row->C_Status)!="DEACTIVE")
														{
														?>
														<select name="ddemp_name<?php echo $row->Ticket_id; ?>" id="ddemp_name<?php echo $row->Ticket_id; ?>" class="form-control input-inline input-medium">
															<option value="-1">Select Employee</option>
															
															<?php
																$ticket_type=$row->Dep_id;
																foreach($emp->result() as $row2)
																{
																	if($row->Dep_id==1||$row->Dep_id==3)
																	{
																		if($row2->order_id==1)
																		{
																		?>
																		<option value="<?php echo $row2->user_id; ?>"><?php echo $row2->user_name; ?></option>
																		<?php
																		}
																	}
																	if($row->Dep_id==$row2->order_id && $row2->order_id!=1 )
																	{
																	?>                 
																	<option value="<?php echo $row2->user_id; ?>"><?php echo $row2->user_name; ?></option>
																	<?php
																	}
																}
																
															?>
														</select>
														<?php
														}?>
												</td>
												
												<td>
													<input type="button" name="submit" class="btn blue" value="Submit" onclick="save('<?php echo $row->Ticket_id;?>')" />
												</td>
											</tr>
											
											
											<?php
												$sn++;
											}
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT BODY -->
	</div>
</div>
<!-- END CONTENT -->
<script>
	function save(id)
	{ 
		
		if($('#ddemp_name'+id).val()!="-1")
		{
			var hidden = $('#hd'+id).val();
			var empname = $('#ddemp_name'+id).val();
			$.ajax(
			{
				type: "POST",
				url:"<?php echo base_url();?>helpdesk/task_assign/",
				data:"&hd="+hidden+"&ddemp_name="+empname+"&old_assign_id=0",
				success: function(msg){
					alert(msg);
					if(msg!=""){
						$(".page-content").html("<center><h2>"+msg+"</h2></center>")
						.append("<center><img id='checkmark' src='<?php echo base_url(); ?>application/libraries/assets/img/loading-spinner-blue.gif' /></center>")
						.hide()
						.fadeIn(1000,function()
						{
							
							$("#stylized").load("<?php echo base_url().'helpdesk/after_index/-1'?>");
							
						}
						);
					}
					else
					{
						alert("Some Error on this page");
					}
				}
			});
		}
		else
		{
			alert('Please Select Employee!!');
			$('#ddemp_name'+id).focus();
		}
	}
</script>																																					