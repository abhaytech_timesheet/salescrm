<script src="<?php echo base_url() ?>application/libraries/js/check.js"></script>
<script src="<?php echo base_url() ?>application/libraries/js/insert_data.js"></script>
<script>
	jQuery(document).ready(function(){
		App.init();
		TableDatatablesButtons.init();
		ComponentsDateTimePickers.init();
		ComponentsBootstrapSelect.init();
		<?php
			if($this->router->fetch_method()=='dashboard')
			{
			?>
			ComponentsBootstrapSwitch.init();
			<?php
			}
		?>
		Dashboard.init();
	});
</script>