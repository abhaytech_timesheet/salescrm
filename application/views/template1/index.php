
<!DOCTYPE HTML>
<html>
<head>
<title>Template 1</title>
<link href="<?php echo base_url() ?>application/libraries/assets/css/template1_css/style.css" rel="stylesheet" type="text/css" media="all" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo base_url() ?>application/libraries/assets/css/template1_css/responsiveslides.css">
<script src="<?php echo base_url() ?>application/libraries/assets/css/template1_js/jquery.min.js"></script>
<script src="<?php echo base_url() ?>application/libraries/assets/css/template1_js/responsiveslides.min.js"></script>
		  <script>
		    // You can also use "$(window).load(function() {"
			    $(function () {
			      // Slideshow 1
			      $("#slider1").responsiveSlides({
			        maxwidth: 2500,
			        speed: 600
			      });
			});
		  </script>
<!--light-box-->
<script type="text/javascript" src="<?php echo base_url() ?>application/libraries/assets/css/template1_js/jquery.lightbox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>application/libraries/assets/css/template1_css/lightbox.css" media="screen">
	<script type="text/javascript">
		$(function() {
			$('.gallery a').lightBox();
		});
   </script>
</head>
<body>
<div class="header">
	<div class="wrap">
		<div class="header-top">
			<div class="logo">
				<a href="index.html"><img src="<?php echo base_url() ?>application/libraries/assets/img/template1_images/logo.png" alt=""/></a>
			</div>
			<div class="telephone">
				<span><img src="<?php echo base_url() ?>application/libraries/assets/img/template1_images/watch.png" "style=margin-right=10px" alt=""/>Timings</span><span class="number">(800) 234-5678</span>
		    </div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="menu">
		<div class="wrap">
			<div class="top-nav">
			      <ul class="nav">
                  <?php foreach($top_menu->result() as $row)
				  { ?>
		            <li><a href="<?php echo base_url(); ?>index.php/cms/template1/<?php echo $row->widget_index ?>"><?php echo $row->widget_title;?></a></li>
					
                    <?php } ?>
					<div class="clear"></div>
				 </ul>
				  <div class="search">
				    		<form>
				    			<input type="text" value="">
				    			<input type="submit" value="">
				    		</form>
					</div>
					<div class="clear"></div>
			</div>
	     </div>
	</div>
</div>
      <!--start-image-slider---->
		<div class="image-slider">
		<!-- Slideshow 1 -->
		  <ul class="rslides" id="slider1">
		    <li><img src="<?php echo base_url() ?>application/libraries/assets/img/template1_images/banner1.jpg" alt=""></li>
			<li><img src="<?php echo base_url() ?>application/libraries/assets/img/template1_images/banner2.jpg" alt=""></li>
			<li><img src="<?php echo base_url() ?>application/libraries/assets/img/template1_images/banner3.jpg" alt=""></li>
		    <li><img src="<?php echo base_url() ?>application/libraries/assets/img/template1_images/banner4.jpg" alt=""></li>
		  </ul>
	    <!-- Slideshow 2 -->
	   </div>
	<!--End-image-slider---->
	<div class="wrap">
	  <div class="main">
		<div class="section group">
        <?php foreach($center_menu->result() as $cen_row)
				  {
					 
				  ?>
				<div class="col_1_of_4 span_1_of_4">
					<img src="<?php echo base_url() ?>application/libraries/assets/img/template1_images/pic.jpg" alt=""/>
						<div class="banner-box3">
							<span class="text20"><?php echo $cen_row->widget_title; ?></span>
							<a href="#" class="link2"></a>
						</div>
						<p class="desc"><?php echo substr($cen_row->widget_content,0,50); ?></p><br>

						<div class="more">
						  <a href="#" class="button">Read More</a>
						</div>
				</div>
                <?php
				  }
				  ?>
				<div class="clear"> </div>
			<div class="bottom-grids">
						<?php foreach($left_menu->result() as $left_row)
						{
						?>
					<div class="bottom-grid1">
                   
							<h3><?php echo $left_row->widget_title; ?></h3>
							<span>consectetur adipisicing elit, sed do eiusmod tempor</span>
							<p><?php echo substr($left_row->widget_content,0,250); ?></p>
							<br>
							<a href="#" class="button">Read More</a>
					</div>
                   
							<?php
							}
							?>
                      <div class="clear"></div>
        
						
						   <?php foreach($right_menu->result() as $right_row)
                                 {
                         
                             ?>
				  			<div class="bottom-grid1">
                   
							<h3><?php echo $right_row->widget_title; ?></h3>
							<span>consectetur adipisicing elit, sed do eiusmod tempor</span>
							<p><?php echo $right_row->widget_content; ?></p>
							<br>
									<a href="#" class="button">Read More</a>
							</div>
								<?php
                                 }
                                 ?>
						
						<div class="clear"></div>
                  </div>
            </div>
        </div>
	</div>
   
     
   <div class="footer">
			<div class="wrap">
			<div class="footer-grids">
				<div class="footer-grid">
					<h3>About Us</h3>
					<p>Ut rutrum neque a mollis laoreet diam enim feuiat dui nec ulacoper quam felis id diam. Nunc ut tortor ligula eu petiu risus. Pelleesque conquat dignissim lacus quis altrcies.</p>
				</div>
				<div class="footer-grid">
					<h3>RECENT POSTS</h3>
					<ul>
						<li><a href="#">Vestibulum felis</a></li>
						<li><a href="#">Mauris at tellus</a></li>
						<li><a href="#">Donec ut lectus</a></li>
						<li><a href="#">vitae interdum</a></li>
					</ul>
				</div>
				<div class="footer-grid">
					<h3>USEFUL INFO</h3>
					<ul>
						<li><a href="#">Hendrerit quam</a></li>
						<li><a href="#">Amet consectetur </a></li>
						<li><a href="#">Iquam hendrerit</a></li>
						<li><a href="#">Donec ut lectus </a></li>
					</ul>
				</div>
				<div class="footer-grid footer-lastgrid">
					<h3>CONTACT US</h3>
					<p>Pelleesque conquat dignissim lacus quis altrcies.</p>
					<div class="footer-grid-address">
						<p>Tel.800-255-9999</p>
						<p>Fax: 1234 568</p>
						<p>Email:<a class="email-link" href="#">email(at)academia.com</a></p>
					</div>
				</div>
				<div class="clear"> </div>
			</div>
			<div class="copy-right">
			 <p>Design by <a href="http://w3layouts.com/">W3layouts</a></p>
		    </div>
		</div>
	</div>
</body>
</html>
