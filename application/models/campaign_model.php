<?php
class Campaign_Model extends CI_Model
	{
		public function insert_sms_temp()
		{
			$aid=0;
			$data=array
			(
			  'affiliate_id'=>$this->session->userdata('affid'),
			  'm_email_temp_title'=>$this->input->post('title'),
			  'm_email_temp_description'=>$this->input->post('content'),
			  'category'=>$this->input->post('category'),
			  'm_template_category'=>'2',
			  'm_status'=>'Pending',
			  'user_id' =>$this->session->userdata('profile_id')
			);
			$this->db->insert('m39_email_temp',$data);
			return "true";
		}
		
		
		
		public function update_sms_template()
		{
			$cont=array(
				'm_email_temp_title'=>$this->input->post('txttitle'),
				'category'=>$this->input->post('ddcategory'),
				'm_email_temp_description'=>$this->input->post('txtdescription'),
				'user_id' =>$this->session->userdata('profile_id')
			);
			$this->db->where('m_email_temp_id',$this->uri->segment(3));
			$this->db->update('m39_email_temp',$cont);
			return 'true';
		}
		
		public function sends_sms($url,$sender_id,$user,$pwd,$mob,$msg)
		{
		   $params = array (
		    'username'=>$user,
		    'password'=>$pwd,
		    'sender'=>$sender_id,
		    'to'=>$mob,
		    'message'=>$msg
		   );
		   $options = array(
		   CURLOPT_SSL_VERIFYHOST => 0,
		   CURLOPT_SSL_VERIFYPEER => 0
		   );
		   
		   $defaults = array(
		   CURLOPT_URL => $url. (strpos($url, '?') 
		   === FALSE ? '?' : ''). http_build_query($params),
		   CURLOPT_HEADER => 0,
		   CURLOPT_RETURNTRANSFER => TRUE,
		   CURLOPT_TIMEOUT =>56
		   );
		   $ch = curl_init();
		   curl_setopt_array($ch, ($options + $defaults));
		   $result = curl_exec($ch);
		   if(!$result)
		   {
			trigger_error(curl_error($ch));
			return $flag=0;
		   }
		   else
		   {                 
			return $flag=1;
		   }
		   curl_close($ch);
		   
		   //echo $result; 
		  }
		  
		  
		public function sms()
		{
			$url = "http://smsport.biz/API/WebSMS/Http/v1.0a/index.php";
		    $params = array (
		      'username'=>'ferry',
		      'password'=>'qwerty',
		      'sender'=>'FERRY',
		      'to'=>'9628281021',
		      'message'=>'HELLOW DEAr'
		   );
		   $options = array(
		   CURLOPT_SSL_VERIFYHOST => 0,
		   CURLOPT_SSL_VERIFYPEER => 0
		   );
		   
		   $defaults = array(
		   CURLOPT_URL => $url. (strpos($url, '?') 
		   === FALSE ? '?' : ''). http_build_query($params),
		   CURLOPT_HEADER => 0,
		   CURLOPT_RETURNTRANSFER => TRUE,
		   CURLOPT_TIMEOUT =>56
		   );
		   $ch = curl_init();
		   curl_setopt_array($ch, ($options + $defaults));
		   $result = curl_exec($ch);
		   if(!$result)
		   {
			trigger_error(curl_error($ch));
			return $flag=0;
		   }
		   else
		   {                 
			return $flag=1;
		   }
		   curl_close($ch);
		   
		   //echo $result; 
		  }
		
		public function update_email_template()
		{
			
			$aid=0;
			$id=$this->uri->segment(3);
			$data=array
			(
			'm_email_temp_title'=>$this->input->post('txttitle'),
			'm_email_temp_description'=>$this->input->post('editor1'),
			'category'=>$this->input->post('category')
			);
			
			$this->db->where('m_email_temp_id',$id);
			$this->db->where('affiliate_id',$aid);
			$this->db->update('m39_email_temp',$data);
			return "true";
		}
		
		
		public function insert_email_temp()
		{
			$aid=0;
			$data=array
			(
			'affiliate_id'=>0,
			'm_email_temp_title'=>$this->input->post('txttitle'),
			'm_email_temp_description'=>$this->input->post('editor1'),
			'category'=>$this->input->post('category'),
			'm_template_category'=>'1',
			'm_status'=>'Pending',
			'user_id'=>$this->session->userdata('profile_id')
			);
			$this->db->insert('m39_email_temp',$data);
			return "true";
		}
		
	}?>