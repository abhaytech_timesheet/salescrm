<?php
class fundrequest extends CI_Model
{

public function fund_request()
{
$this->load->library('session');
$this->load->database();
$this->load->helper('url');
$this->load->helper('string');
$timezone = new DateTimeZone("Asia/Kolkata" );
$date = new DateTime();
$date->setTimezone($timezone );
$ss=random_string('numeric',4);
$d=$date->format('YmdHis');
$d=$d.$ss;
$id=$this->uri->segment(3);
$this->load->model('users_records');
$this->db->where('m_fr_id',$id);
$data['fundinfo']=$this->db->get('tr01_fund_req');
foreach($data['fundinfo']->result() as $row)
{
break;
}
$userid=$row->m_u_id;
$mid=$this->users_records->get_marsuid($row->m_u_id);
$amt=$row->m_fr_amount;


if(trim($mid)!="")
{
$query=$this->db->get_where('m06_user_detail',array('or_m_user_id'=>trim($this->input->post('txtintuserid'))));
$row = $query->row();
if(trim($this->session->userdata('profile_id'))==$row->or_m_intr_id || $this->session->userdata('profile_id')==0)
{

if($this->session->userdata('profile_id')!=0 && $amt>0)
{
$avail_amt=$this->users_records->user_avail_amt1($this->session->userdata('profile_id'));	
$availamt=$this->users_records->user_avail_amt1($userid);
$depositamt=$amt;
$currentbalance=$availamt+$depositamt;
if($avail_amt > $depositamt && trim($userid)!="")
{
//for credit
$requestdata = array(
		 'm_u_id'=>trim($userid),
		 'm_trans_id'=>$d,
		 'm_cramount'=>$amt,
		 'm_dramount'=>"",
		 'm_description'=>"Fund Requset Approv ",
		 'm_transdate'=>$date->format( 'Y-m-d H-i-s' ),
		 'm_deduction'=>"",
		 'm_refrence_id'=>$this->session->userdata('profile_id'),
         'm_ledger_type'=>1,
         'm_current_balance'=>$availamt+$amt,
		 'm_datetime'=>$date->format( 'Y-m-d H-i-s' )
		  ); 
		  $this->db->insert('tr03_manage_ledger', $requestdata);
//for debit		  
		  $requestdata1 = array(
		  		  		 'm_u_id'=>$this->session->userdata('profile_id'),
		  		  		 'm_trans_id'=>$d,
		  		  		 'm_cramount'=>"",
		  		  		 'm_dramount'=>$amt,
		  		  		 'm_description'=>"Fund Request Approv",
		  		  		 'm_transdate'=>$date->format( 'Y-m-d H-i-s' ),
		  		  		 'm_deduction'=>"",
		  		  		 'm_refrence_id'=>trim($userid),
		                 'm_ledger_type'=>2,
		  		  		 'm_current_balance'=>$avail_amt-$amt,
		  		  		 'm_datetime'=>$date->format('Y-m-d H-i-s')
		  		  		  ); 
		  				$this->db->insert('tr03_manage_ledger', $requestdata1);
		  }
		  
}
else
{
$availamt=$this->users_records->user_avail_amt1($userid);
$depositamt=$amt;
$currentbalance=$availamt+$depositamt;
if($this->session->userdata('profile_id')!=""&&$this->session->userdata('profile_id')==0 && $amt>0)
{
if(trim($userid)!="" )
{
$requestdata = array(
		 'm_u_id'=>trim($userid),
		 'm_trans_id'=>$d,
		 'm_cramount'=>$amt,
		 'm_dramount'=>"",
		 'm_description'=>"Fund Request Approv",
		 'm_transdate'=>$date->format( 'Y-m-d H-i-s' ),
		 'm_deduction'=>"",
		 'm_refrence_id'=>$this->session->userdata('profile_id'),
         'm_ledger_type'=>1,
		 'm_current_balance'=>$availamt+$amt,
		 'm_datetime'=>$date->format( 'Y-m-d H-i-s' )
		  ); 
		  $this->db->insert('tr03_manage_ledger', $requestdata);
}
}
}
}

}
}
}
?>