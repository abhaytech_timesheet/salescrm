<?php
	class corporate_model extends CI_Model
	{
		
		//------------------Get User Id--------------------------
		
		public function get_uid($id)
		{
			$aid=$id;
			
			$this->db->where('or_m_user_id',$aid);
			$data['membername']=$this->db->get('m06_user_detail');
			foreach($data['membername']->result() as $row)
			{
				$mname=$row->or_m_reg_id;
				return $mname;
			}
		}
		
		
		//Select State here
		public function select_state($id)
		{
			$this->load->helper('url');
			$this->load->database();
			if($id!="" && $id!=0)
			{
				$this->db->where('m_parent_id',$id);
				return $this->db->get('m02_location');
			}
			else
			{
				//$this->db->where('m_parent_id',1);
				return $this->db->get('m02_location');
			}
		}
		
		//---------------------Insert Plot Booking-----------------------------
		
		public function insert_plot_book()
		{
			
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			
			$pay_status = 0;
			$booking_amnt = $this->input->post('txtbooking_amt');
			$allotment_amnt = $this->input->post('txtallot_amt');
			$payment_amnt = $this->input->post('txtpay_amt');
			
			if($payment_amnt > $allotment_amnt)											// pay status = 1 ( Booking & Allotment Filled)
			{																			// pay status = 2 ( Booking Filled but not allotment)
				$pay_status = 1;													// pay status = 3 ( neither booking nor alltment filled)
			}
			else
			{
				if($payment_amnt > $booking_amnt)
				{
					$pay_status = 2;
				}
				else
				{
					$pay_status = 3;
				}
			}
			
			$data=array(
			'proc'=>'1',
			'plot_book_id'=>'0',			
			'book_ref_by'=>'',
			'or_m_reg_id'=>$this->get_uid($this->input->post('txtmember_id')),
			'site_id'=>$this->input->post('ddsite'),
			'plot_size'=>$this->input->post('ddplot_size'),
			'plot_no'=>$this->input->post('ddplot_no'),
			'payment_plan'=>$this->input->post('txtpayment_plan'),
			'is_late'=>$this->input->post('ddlate'),
			'late_days'=>$this->input->post('txtlate_day'),
			'late_rate'=>$this->input->post('txtlate_rate'),
			'bookdates'=>$date->format('Y-m-d'),				
			'paymentamnt'=>$this->input->post('txtpay_amt'),
			'paymentmode'=>$this->input->post('txtpayment_mode'),
			'paymentother'=>0,				
			'paymentcheckno'=>$this->input->post('txtcheck_no'),
			'paymentbank'=>$this->input->post('ddbank'),
			'paymentcheckdate'=>$this->input->post('txtcheck_date'),
			'paymentcheckstatus'=>0,
			'paystatus'=>$pay_status,
			'booking_status'=>1	
			);
			//var_dump($data);
			//echo count($data);
			$query = " CALL sp_plot_booking(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query,$data);
			
		}
		
		
		public function update_pwd()
		{
			if($this->input->post('txtnew_pwd')=="")
			{
				$pwd=$this->uri->segment(4);
			}
			else
			{
				$pwd=$this->input->post('txtnew_pwd');
			}
			
			if($this->input->post('txtpin_pwd')=="")
			{
				$pin_pwd=$this->uri->segment(5);
			}
			else
			{
				$pin_pwd=$this->input->post('txtpin_pwd');
			}
			
			
			$data=array(
			'user_id'=>$this->uri->segment(3),
			'new_pwd'=>$pwd,
			'new_pin_pwd'=>$pin_pwd,
			'proc'=>'2'
			);
			$query = " CALL sp_change_password(?" . str_repeat(",?", count($data)-1) . ")";
			$query=$this->db->query($query, $data);
		}
		
		public function plancode()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone );
			$doj=$date->format('my'); 
			$query=$this->db->query("SELECT max(book_id) as book_id FROM `m12_booking_details` WHERE `or_m_status`='1'");
			$count=$this->db->count_all_results('m12_booking_details');
			if($count>0)
			{
				$row = $query->row();
				$plan_code=$row->booking_no;
				$emp_code=($plan_code+1);
				$length=strlen($plan_code);
				if($length<3)
				{
					$plan_code='00'.$plan_code;
				}
				else
				{
					$plan_code=($plan_code);
				}
				$code='FIPL'.$doj.'PLN'.$plan_code;
				return $code;
			}
			else
			{
				$code='FIPL'.$doj.'PLN'.'001';
				return $code;
			}
		}		
		
		
	}
?>