<?php
class mastermodel extends CI_Model
	{
		/*Configuration Model function Start Here */
		public function get_settings()
		{
			return 'abc';
		}
		//Select Config Data
		public function select_config()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->db->where('m00_id',1);
			return $this->db->get('m00_setconfig');
		}
		
		// Update Config data
		public function update_config($param)
		{
			$mcf=array();
			$this->load->helper('url');
			$this->load->database();
			if($param==1)
			{
				$config['upload_path']   =   "application/logo/";
				$config['allowed_types'] =   "gif|jpg|jpeg|png"; 
				$config['max_size']      =   "5000";
				$config['max_width']     =   "5000";
				$config['max_height']    =   "5000";
				$this->load->library('upload',$config);
				$this->upload->do_upload();
				$finfo=$this->upload->data();
				$fileupload=($finfo['raw_name'].$finfo['file_ext']);
				$this->project_resize_image($fileupload);
				$mcf['m00_sitename']=$this->input->post('txtprojname');
				$mcf['m00_description']=$this->input->post('txtdesc');
				$mcf['m00_website']=$this->input->post('txtwebsite');
				$mcf['m00_sitelogo'] =$fileupload;
				$this->db->where('m00_id',1);
				$this->db->update('m00_setconfig',$mcf);
			}
			else if($param==2)
			{ 
				$mcf['m00_secondary_emails']=implode(',',$this->input->post('txtemail'));
				$mcf['m00_address']=$this->input->post('txtadd');
                $mcf['m00_email_pwd']=$this->input->post('txtemailpassword');
                $mcf['m00_sms_uername']=$this->input->post('txtsmsuserid');
                $mcf['m00_sms_pwd']=$this->input->post('txtsmspassword');
				$mcf['m00_contact']=implode(',',$this->input->post('txtcontact'));
				$this->db->where('m00_id',1);
				$this->db->update('m00_setconfig',$mcf);
			}
			else if($param==3)
			{
		        $query00 = $this->db->get_where('m00_setconfig',array('m00_password'=>$this->input->post('txtolduserpass'), 'm00_status'=>1));
				$row00 = $query00->row();
				if($query00->num_rows()==1)
				{
					$mcf['m00_username']=$this->input->post('txtusername');
					$mcf['m00_password'] =$this->input->post('txtuserpass');
					$mcf['m00_pinpassword']=$this->input->post('txtuserpinpass');
					$mcf['m00_otp']=($this->input->post('chkotp')!='on')?0:1;
					$mcf['m00_sms_alert']=($this->input->post('chkemail')!='on')?0:1;
					$mcf['m00_email_alert']=($this->input->post('chksms')!='on')?0:1;
					$this->db->where('m00_id',1);
					$this->db->update('m00_setconfig',$mcf);
				}
			}
			
		}
		
		public function project_resize_image($img)
		{
		    $this->load->library('image_lib');
			$configSize1['image_library'] = 'gd2';
			$configSize1['source_image']  = 'application/logo/'.$img;
			$configSize1['new_image']  ='application/logo/221x217/';
			$configSize1['width']	 = '221';
			$configSize1['height']	= '217';
			$this->image_lib->initialize($configSize1);
			$this->image_lib->resize();
			
			$this->image_lib->clear();
			
			$configSize2['image_library'] = 'gd2';
			$configSize2['source_image']  = 'application/logo/'.$img;
			$configSize2['new_image']  = 'application/logo/86x14/';
			$configSize2['width']	 =  '137';
			$configSize2['height']	= '38';
			$this->image_lib->initialize($configSize2);
			$this->image_lib->resize();
		
		}
		/* Configuration Model  function End Here  */
		
		/* City/District Model Function Start Here  */
		
		//Select State here
		public function select_state($id)
		{
			$this->load->helper('url');
			$this->load->database();
			if($id!="" && $id!=0)
			{
				$this->db->where('m_parent_id',$id);
				return $this->db->get('m05_location');
			}
			else
			{
				//$this->db->where('m_parent_id',1);
				return $this->db->get('m05_location');
			}
		}
		//Select City here
		public function select_city($id)
		{
			$this->load->helper('url');
			$this->load->database();
			if($id!="" && $id!=0)
			{
				$this->db->where('m_loc_id',$id);
				return $this->db->get('m05_location');
			}
		}
		//Add City Here
		public function add_city()  
		{
			$this->load->helper('url');
			$this->load->database();
			if(trim($this->input->post('txtcity'))!="")
			{
				if(trim($this->input->post('ddstate'))!="-1")
				{
					$city=array(
					'm_loc_name'=>trim($this->input->post('txtcity')),
					'm_parent_id'=>trim($this->input->post('ddstate')),
					'm_status'=>trim($this->input->post('txtstatus'))
					);
					$this->db->insert('m05_location',$city);
				}
			}
		}
		
		//Update City Here
		public function update_city($id)  
		{
			$this->load->helper('url');
			$this->load->database();
			if(trim($this->input->post('txtcity'))!="")
			{
				if(trim($this->input->post('ddstate'))!="-1")
				{
					$city=array(
					'm_loc_name'=>trim($this->input->post('txtcity')),
					'm_parent_id'=>trim($this->input->post('ddstate')),
					'm_status'=>trim($this->input->post('txtstatus'))
					);
					$this->db->where('m_loc_id',$id);
					$this->db->update('m05_location',$city);
				}
			}
		} 
		/* City/District Model Function End Here  */
		
		/* Designation/Rank Model Function Start Here  */
		
		public function select_designation()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->db->where('m_des_user_id',0);
			$this->db->where('m_des_pat_id',0);
			return $this->db->get('m03_designation');
		}
		public function select_rank($id)
		{
			$this->load->helper('url');
			$this->load->database();
			$this->db->where('m_des_id',$id);
			return $this->db->get('m03_designation');
		}
		//Add Designation Here
		public function add_designation()  
		{
			$this->load->helper('url');
			$this->load->database();
			if(trim($this->input->post('txtdesignation'))!="")
			{
				if(trim($this->input->post('txtdesigorder'))!="")
				{
					$desig=array(
					'm_des_name'=>trim($this->input->post('txtdesignation')), 
					'm_des_pat_id'=>0, 
					'm_des_user_id'=>0, 
					'm_des_orid'=>trim($this->input->post('txtdesigorder')), 
					'm_des_status'=>trim($this->input->post('txtstatus'))
					);
					$this->db->insert('m03_designation',$desig);
				}
			}
		}
		
		//Update Desigantion Here
		public function update_designation($id)  
		{
			$this->load->helper('url');
			$this->load->database();
			if(trim($this->input->post('txtdesignation'))!="")
			{
				if(trim($this->input->post('txtdesigorder'))!="")
				{
					$desig=array(
					'm_des_name'=>trim($this->input->post('txtdesignation')), 
					'm_des_pat_id'=>0, 
					'm_des_user_id'=>0, 
					'm_des_orid'=>trim($this->input->post('txtdesigorder')), 
					'm_des_status'=>trim($this->input->post('txtstatus'))
					);
					$this->db->where('m_des_id',$id);
					$this->db->update('m03_designation',$desig);
				}
			}
		}
		
		
		
		/* Designation/Rank Model Function End Here  */
		
		
		/* Branch/Affilaite Model Function Start Here  */
		public function select_affiliate()
		{
			$this->load->helper('url');
			$this->load->database();
			//$this->db->where('affiliate_status',1);
			return $this->db->get('m22_affiliate');
		}
		
		public function select_branch($id)
		{
			$this->load->helper('url');
			$this->load->database();
			$this->db->where('affiliate_id',$id);
			return $this->db->get('m22_affiliate');
		}
		//Add Branch/Affilaite Here
		public function add_affiliate()  
		{
			$this->load->helper('url');
			$this->load->database();
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			if(trim($this->input->post('txtbrchname'))!="")
			{
				if(trim($this->input->post('txtbrchcommuadd'))!="")
				{
					if(trim($this->input->post('txtbrchcontactper'))!="")
					{
						if(trim($this->input->post('txtbrchcontactno'))!="")
						{
							if(trim($this->input->post('txtbrchemailadd'))!="")
							{
								if(trim($this->input->post('txtbrchloginpwd'))!="")
								{
									$affiliate=array(
									'affiliate_mreg_id'=>1,
									'affiliate_name'=>trim($this->input->post('txtbrchname')), 
									'affiliate_address'=>trim($this->input->post('txtbrchcommuadd')),
									'affiliate_conatct_person'=>trim($this->input->post('txtbrchcontactper')),
									'affiliate_contact_no'=>trim($this->input->post('txtbrchcontactno')),
									'affiliate_email'=>trim($this->input->post('txtbrchemailadd')),
									'affiliate_password'=>trim($this->input->post('txtbrchloginpwd')),
									'affiliate_state'=>trim($this->input->post('ddstate')),
									'affiliate_city'=>trim($this->input->post('ddcity')),
									'affiliate_entry_date'=>$date->format( 'Y-m-d H-i-s' ), 
									'affiliate_status'=>trim($this->input->post('txtstatus'))
									);
									$this->db->insert('m22_affiliate',$affiliate);
									
								}
							}
						}
					}
				}
			}
		}
		
		//Update Branch/Affilaite Here
		public function update_affiliate($id)  
		{
			$this->load->helper('url');
			$this->load->database();
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			if(trim($this->input->post('txtbrchname'))!="")
			{
				if(trim($this->input->post('txtbrchcommuadd'))!="")
				{
					if(trim($this->input->post('txtbrchcontactper'))!="")
					{
						if(trim($this->input->post('txtbrchcontactno'))!="")
						{
							if(trim($this->input->post('txtbrchemailadd'))!="")
							{
								if(trim($this->input->post('txtbrchloginpwd'))!="")
								{
									$affiliate=array(
									'affiliate_mreg_id'=>1,
									'affiliate_name'=>trim($this->input->post('txtbrchname')), 
									'affiliate_address'=>trim($this->input->post('txtbrchcommuadd')),
									'affiliate_conatct_person'=>trim($this->input->post('txtbrchcontactper')),
									'affiliate_contact_no'=>trim($this->input->post('txtbrchcontactno')),
									'affiliate_email'=>trim($this->input->post('txtbrchemailadd')),
									'affiliate_password'=>trim($this->input->post('txtbrchloginpwd')),
									'affiliate_state'=>trim($this->input->post('ddstate')),
									'affiliate_city'=>trim($this->input->post('ddcity')),
									'affiliate_entry_date'=>$date->format( 'Y-m-d H-i-s' ), 
									'affiliate_status'=>trim($this->input->post('txtstatus'))
									);
									$this->db->where('affiliate_id',$id);
									$this->db->update('m22_affiliate',$affiliate);
									
								}
							}
						}
					}
				}
			}
		}
		
		
		/* Branch/Affilaite Model Function End Here  */
		/*Plan */
		//Select City here
		public function select_plan($id)
		{
			$this->load->helper('url');
			$this->load->database();
			if($id!="" && $id!=0)
			{
				$this->db->where('p_plantype_id',$id);
				return $this->db->get('p02_master_plan');
			}
		}
		
		
		//Manage City Add,Update,Status Change Function 
		public function update_status_city()
		{
			$id=$this->uri->segment(3);
			$this->db->where('m_loc_id',$id);
			$city=array(
			'm_status'=>$this->uri->segment(4)
			);
			$this->db->update('m05_location',$city);
			
		}
		
		/*--------------------------Start News Master--------------------------*/
		public function view_news()
		{
			$aid=0;
			$this->db->where('m_affid',$aid);
			$data['content']=$this->db->get('m24_news');
			return $data;
		}
		
		
		public function add_news()
		{
			$aid=0;
			$this->db->where('m_affid',$aid);
			$data['content']=$this->db->get('m24_news');
			return $data;
		}
		
		
		public function insert_news()
		{
			$data=array
			(
			'm_news_id'=>'',
			'm_news_title'=>$this->input->post('txttitle'),
			'm_news_des'=>$this->input->post('txtdescription'),
			'm_news_status'=>$this->input->post('ddstatus'),
			'm_affid'=>$this->session->userdata('profile_id'),
			'proc_id'=>'1'
			);
			$query = " CALL sp_news(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		
		public function edit_news()
		{
			$aid=0;
			$this->db->where('m_affid',$aid);
			$this->db->where('m_news_id',$this->uri->segment(3));
			$data['content']=$this->db->get('m24_news');
			return $data;
		}
		
		
		public function update_news()
		{
			$data=array
			(
			'm_news_id'=>$this->uri->segment(3),
			'm_news_title'=>$this->input->post('txttitle'),
			'm_news_des'=>$this->input->post('txtdescription'),
			'm_news_status'=>$this->input->post('ddstatus'),
			'm_affid'=>$this->session->userdata('profile_id'),
			'proc_id'=>'2'
			);
			$query = " CALL sp_news(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		
		public function delete_news()
		{
			$data=array
			(
			'm_news_id'=>$this->uri->segment(3),
			'm_news_title'=>'',
			'm_news_des'=>'',
			'm_news_status'=>$this->uri->segment(4),
			'm_affid'=>'',
			'proc_id'=>'3'
			);
			$query = " CALL sp_news(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		
		public function account()
		{
			$this->db->where('m_acc_status',1);
			//$this->db->where('m_ic_parent',0);
			$data['menu']=$this->db->get('m47_account');
			return $data;
		}
		
		
		public function account_load()
		{
			$id=$this->uri->segment(3);
			$this->db->where('m_acc_id',$id);
			$data['details']=$this->db->get('m47_account');
			return $data;
		}
		
		
		public function insert_account()
		{
			$account = array
			(
			'm_acc_name'=>$this->input->post('txtsubledname'),
			'm_acc_num'=>$this->input->post('txtsubled'),
			'm_acc_descrip'=>$this->input->post('txtsubledname'),
			'm_acc_parent'=>$this->input->post('m_acc_id'),
			'm_acc_for'=>1,
			'm_acc_status'=>1,
			'm_main_acc_id'=>$this->input->post('m_main_acc_id')
			);
			$this->db->insert('m47_account',$account);
			return "true";
		}
		
		
		public function view_add_apps()
		{
			$this->db->where('tr_assign_userid',$this->uri->segment(3));
            $this->db->where('tr_assign_desig',$this->uri->segment(4));
			$data['user']=$this->db->get('tr27_assign_apps');
			$this->db->where('m_menu_status',1);
			$data['menu']=$this->db->get('m58_apps');
			return $data;
		}
		
		
		public function insert_apps()
		{
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			$smenu=array(
			'm_menu_name'=>$this->input->post('txtname'),
			'm_menu_serial'=>$this->input->post('txtserial'),
			'm_menu_icon'=>$this->input->post('txticon'),
			'm_menu_desc'=>$this->input->post('txtdes'),
			'm_sbcatlink'=>$this->input->post('txtsubcate'),
			'm_menu_url'=>$this->input->post('txturl'),
			'm_menu_parentid'=>$this->input->post('txtparent'),
			'm_entrydate'=>$date->format( 'Y-m-d H-i-s' ),
			'm_menu_status'=>1
			);
			$this->db->insert('m58_apps',$smenu);
		}
		
		
		public function view_change_status_apps()
		{
			$data['menu']=$this->db->get('m58_apps');
			return $data;
		}
		
		
		public function view_edit_apps()
		{
			$this->db->where('m_menu_id',$this->uri->segment(3));
			$data['menu']=$this->db->get('m58_apps');
			return $data;
		}
		
		
		public function update_apps()
		{
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			$smenu=array(
			'm_menu_name'=>$this->input->post('txtname'),
			'm_menu_serial'=>$this->input->post('txtserial'),
			'm_menu_icon'=>$this->input->post('txticon'),
			'm_menu_desc'=>$this->input->post('txtdes'),
			'm_sbcatlink'=>$this->input->post('txtsubcate'),
			'm_menu_url'=>$this->input->post('txturl'),
			'm_menu_id'=>$this->input->post('txtparent'),
			'm_entrydate'=>$date->format( 'Y-m-d H-i-s' ),
			'm_menu_status'=>1
			);
			$this->db->where('m_menu_id',$this->uri->segment(3));
			$this->db->update('m58_apps',$smenu);
		}
		
		public function update_status_apps()
		{
			$smenu=array(
			'm_menu_status'=>$this->uri->segment(4)
			);
			$this->db->where('m_menu_id',$this->uri->segment(3));
			$this->db->update('m58_apps',$smenu);
		}
		
		
		public function update_menu()
		{
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			$data=array(
			'm_menu_name'=>$this->input->post('txtname'),
			'm_menu_serial'=>$this->input->post('txtserial'),
			'm_menu_icon'=>$this->input->post('txticon'),
			'm_menu_desc'=>$this->input->post('txtdesc'),
			'm_menu_url'=>$this->input->post('txturl'),
			'm_entrydate'=>$date->format( 'Y-m-d H-i-s' )
			);
			$this->db->where('m_menu_id',$this->input->post('hdid'));
			$this->db->update('m58_apps',$data);
			return "1";
		}
		
		
		public function assign_apps_to_user()
		{
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			
			$menus=$this->input->post('txtquid');
			$c = explode(',', $menus);
			$no=count($c);
		    $this->db->query("CALL sp_delete_assign_menu('".$this->input->post('txtemail')."',".$this->session->userdata('affid').")");
            
            $this->db->free_db_resource();
			for($i=0; $i<$no-1; $i++)
			{
				$co = explode(',', $menus);
				$data=array(
				'tr_assign_userid'=>$this->input->post('txtemail'),
				'tr_assign_menuid'=>$co[$i],
				'tr_assign_view'=>$this->input->post('txtview'),
				'tr_assign_add'=>$this->input->post('txtadd'),
				'tr_assign_update'=>$this->input->post('txtupdate'),
				'tr_assign_delete'=>$this->input->post('txtdelete'),
				'tr_assign_status'=>1,
				'tr_assign_date'=>$date->format( 'Y-m-d H-i-s' ),
                'affid'=>$this->session->userdata('affid')
				);
				//print_r($data);
				//die();
				$query = " CALL sp_assign_menu_to_user(?" . str_repeat(",?", count($data)-1) . ")";
				$this->db->query($query, $data);
               
			}
		}
		
		
		public function view_assign_apps_status()
		{
			$query = " CALL sp_assign_apps()";
			$data['menu']=$this->db->query($query);
			return $data;
		}
		
		
		public function update_assign_status_apps()
		{
			$data=array(
			'tr_assign_status'=>$this->uri->segment(4)
			);
			$this->db->where('tr_assign_id',$this->uri->segment(3));
			$this->db->update('tr27_assign_apps',$data);
		}

	/*04/11/2016*/
		//-------ADD SERVICES------//
		
		public function add_service()
		{
			$serv = $this->input->post('txtservice');
			if($serv == 'PREPAID' || $serv == 'DTH')
			{
				$servfor=2;
			}
			else
			{
				$servfor=3;
			}
			$service=array(
			'Service_type_id'=>'',
			'Service_type'=>$serv,
			'Service_for'=>$servfor,
			'Service_status'=>$this->input->post('rbstatus'),
			'proc'=>1
			);
			$query="CALL sp_services(?" . str_repeat(",?", count($service)-1) . ",@aa)";
			$this->db->query($query,$service);
			
		}
		
		
		public function update_service()
		{
			$service=array(
			'Service_type_id'=>$this->uri->segment(3),
			'Service_type'=>$this->input->post('txtservice'),
			'Service_for'=>'',
			'Service_status'=>'',
			'proc'=>2
			);
			$query="CALL sp_services(?" . str_repeat(",?", count($service)-1) . ",@aa)";
			$this->db->query($query,$service);
		}
		
		public function stch_service()
		{
			$chk=trim($this->input->post('chkid'),',');
			$unchk=trim($this->input->post('unchkid'),',');
			if($chk != '')
			{
			$this->db->query("UPDATE `m10_service_type` SET `m10_service_type`.`m_service_status`=1 WHERE `m10_service_type`.`m_service_type_id` IN (".$chk.") AND `m_service_for` !=1");
			}
			if($unchk != '')
			{
			$this->db->query("UPDATE `m10_service_type` SET `m10_service_type`.`m_service_status`=0 WHERE `m10_service_type`.`m_service_type_id` IN (".$unchk.") AND `m_service_for` !=1");
			}
		}
		//---SERVICE PROVIDER----//
		
		public function add_srv_provider()
		{
			$sp=array(
			'Sp_id'=>'',
			'Sp_type_id'=>$this->input->post('ddservice_prov'),
			'Sp_name'=>$this->input->post('txtoptname'),
			'Sp_code'=>$this->input->post('txtoptcode'),
			'Sp_status'=>$this->input->post('rbstatus'),
			'proc'=>1
			);
			$query="CALL sp_service_provider(?" . str_repeat(",?", count($sp)-1) . ",@aa)";
			$this->db->query($query,$sp);
		}
		
		public function update_srv_provider()
		{
			$sp=array(
			'Sp_id'=>$this->uri->segment(3),
			'Sp_type_id'=>'',
			'Sp_name'=>$this->input->post('txtoptname'),
			'Sp_code'=>$this->input->post('txtoptcode'),
			'Sp_status'=>'',
			'proc'=>2
			);
			$query="CALL sp_service_provider(?" . str_repeat(",?", count($sp)-1) . ",@aa)";
			$this->db->query($query,$sp);
			
		}
		
		
		public function stch_sp()
		{
			$chk=trim($this->input->post('chkid'),',');
			$unchk=trim($this->input->post('unchkid'),',');
			if($chk != '')
			{
				$this->db->query("UPDATE `m17_service_provider` SET `m17_service_provider`.`m_sp_status`=1 WHERE `m17_service_provider`.`m_sp_id` IN (".$chk.")");
			}
			if($unchk != '')
			{
				$this->db->query("UPDATE `m17_service_provider` SET `m17_service_provider`.`m_sp_status`=0 WHERE `m17_service_provider`.`m_sp_id` IN (".$unchk.")");
			}
		}

	}			
?>