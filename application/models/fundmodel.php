<?php
class fundmodel extends CI_Model
{
public function get_availbal($stid)
{
$this->load->database();
$this->load->helper('url');
$id=$stid;
$query=$this->db->get_where('or_registration',array('or_m_user_id'=>trim($id)));
$row = $query->row();
$this->db->where('m_u_id',trim($row->or_m_reg_id));
return $this->db->get('tr03_manage_ledger');
//$row = $query->row();
}

public function get_currentavailbal($stid)
{
$this->load->database();
$this->load->helper('url');
$id=$stid;
$totalbalnce=0;
$this->db->where('m_u_id',trim($id));
$data['bal']= $this->db->get('tr03_manage_ledger');
foreach($data['bal']->result() as $row)
{
$totalbalnce=$totalbalnce+($row->m_cramount+$row->m_dramount);		 
}
return $totalbalnce;
}

public function fund_request()
{
$this->load->database();
$this->load->helper('url');
$timezone = new DateTimeZone("Asia/Kolkata" );
$date = new DateTime();
$date->setTimezone($timezone );
$d=$date->format( 'YmdHis' );
$requestdata = array(
		 'm_fr_tid'=>$d,
		 'm_fr_mid'=>$this->input->post('m_fr_mid'),
		 'm_fr_request_type'=>$this->input->post('m_fr_request_type'),
		 'm_fr_ddno'=>$this->input->post('m_fr_ddno'),
		 'm_fr_bank_name'=>$this->input->post('m_fr_bank_name'),
		 'm_fr_branch_name'=>$this->input->post('m_fr_branch_name'),
		 'm_fr_amount'=>$this->input->post('m_fr_amount'),
		 'm_fr_subdate'=>$date->format( 'Y-m-d H-i-s' ),
		 'm_fr_status'=>$this->input->post('txtstatus'),
		 'm_fr_description'=>"",
		 'm_fr_admindate'=>$date->format( 'Y-m-d H-i-s' )
		  ); 
$this->db->insert('tr03_manage_ledger', $requestdata);
}

public function transfer_bal()
{
$this->load->database();
$this->load->helper('url');
$this->load->helper('string');
$timezone = new DateTimeZone("Asia/Kolkata" );
$date = new DateTime();
$date->setTimezone($timezone );
$ss=random_string('numeric',4);
$d=$date->format('YmdHis');
$d=$d.$ss;
$this->load->model('users_records');
if(trim($this->input->post('txtintuserid'))!="")
{
$query=$this->db->get_where('m06_user_detail',array('or_m_user_id'=>trim($this->input->post('txtintuserid'))));
$row = $query->row();
if(trim($this->session->userdata('profile_id'))==$row->or_m_intr_id || $this->session->userdata('profile_id')==0)
{
if($this->session->userdata('profile_id')!=0 && $this->input->post('txtdepositamt')>0)
{
$avail_amt=$this->users_records->user_avail_amt1($this->session->userdata('profile_id'));	
$availamt=$this->input->post('txtavailamt');
$depositamt=$this->input->post('txtdepositamt');
$currentbalance=$availamt+$depositamt;
if($avail_amt >= $depositamt && $this->input->post('txtstatus')== "1" && trim($row->or_m_reg_id)!="")
{
$requestdata = array(
		 'm_u_id'=>trim($row->or_m_reg_id),
		 'm_trans_id'=>$d,
		 'm_cramount'=>$this->input->post('txtdepositamt'),
		 'm_dramount'=>"",
		 'm_description'=>$this->input->post('txtremark'),
		 'm_transdate'=>$date->format( 'Y-m-d H-i-s' ),
		 'm_deduction'=>"",
		 'm_refrence_id'=>$this->session->userdata('profile_id'),
         'm_ledger_type'=>$this->input->post('txtstatus'),
          'm_current_balance'=>$this->input->post('txtavailamt')+$this->input->post('txtdepositamt'),
		 'm_datetime'=>$date->format( 'Y-m-d H-i-s' )
		  ); 
		  $this->db->insert('tr03_manage_ledger', $requestdata);
		  $requestdata1 = array(
		  		  		 'm_u_id'=>$this->session->userdata('profile_id'),
		  		  		 'm_trans_id'=>$d,
		  		  		 'm_cramount'=>"",
		  		  		 'm_dramount'=>$this->input->post('txtdepositamt'),
		  		  		 'm_description'=>$this->input->post('txtremark'),
		  		  		 'm_transdate'=>$date->format( 'Y-m-d H-i-s' ),
		  		  		 'm_deduction'=>"",
		  		  		 'm_refrence_id'=>trim($row->or_m_reg_id),
		                 'm_ledger_type'=>2,
		  		  		 'm_current_balance'=>$avail_amt-$this->input->post('txtdepositamt'),
		  		  		 'm_datetime'=>$date->format('Y-m-d H-i-s')
		  		  		  ); 
		  				$this->db->insert('tr03_manage_ledger', $requestdata1);
		  }
if($this->input->post('txtstatus')=="2" && trim($row->or_m_reg_id)!="" && ($this->input->post('txtavailamt')>=$this->input->post('txtdepositamt')))
		  {
$avail_amt1=$this->users_records->user_avail_amt1(trim($row->or_m_reg_id));
$ava=($avail_amt1-$this->input->post('txtdepositamt'));
		  if($ava>=0)
		  {
		  $requestdata = array(
		  		 'm_u_id'=>trim($row->or_m_reg_id),
		  		 'm_trans_id'=>$d,
		  		 'm_cramount'=>"",
		  		 'm_dramount'=>$this->input->post('txtdepositamt'),
		  		 'm_description'=>$this->input->post('txtremark'),
		  		 'm_transdate'=>$date->format( 'Y-m-d H-i-s' ),
		  		 'm_deduction'=>$this->input->post('txtdepositamt'),
		  		 'm_refrence_id'=>$this->session->userdata('profile_id'),
                 'm_ledger_type'=>$this->input->post('txtstatus'),
		  		 'm_current_balance'=>$this->input->post('txtavailamt')-$this->input->post('txtdepositamt'),
		  		 'm_datetime'=>$date->format( 'Y-m-d H-i-s' )
		  		  ); 
				    $this->db->insert('tr03_manage_ledger', $requestdata);
		  		 
$requestdata1 = array(
		 'm_u_id'=>$this->session->userdata('profile_id'),
		 'm_trans_id'=>$d,
		 'm_cramount'=>$this->input->post('txtdepositamt'),
		 'm_dramount'=>"",
		 'm_description'=>$this->input->post('txtremark'),
		 'm_transdate'=>$date->format( 'Y-m-d H-i-s' ),
		 'm_deduction'=>"",
		 'm_refrence_id'=>trim($row->or_m_reg_id),
         'm_ledger_type'=>1,
		 'm_current_balance'=>$avail_amt+$this->input->post('txtdepositamt'),
		 'm_datetime'=>$date->format( 'Y-m-d H-i-s' )
		  ); 
		  $this->db->insert('tr03_manage_ledger', $requestdata1);
}

}}
else
{
$availamt=$this->input->post('txtavailamt');
$depositamt=$this->input->post('txtdepositamt');
$currentbalance=$availamt+$depositamt;
if($this->session->userdata('profile_id')!=""&&$this->session->userdata('profile_id')==0 && $this->input->post('txtdepositamt')>0)
{
if($this->input->post('txtstatus')=="1" && trim($row->or_m_reg_id)!="" )
{
$requestdata = array(
		 'm_u_id'=>trim($row->or_m_reg_id),
		 'm_trans_id'=>$d,
		 'm_cramount'=>$this->input->post('txtdepositamt'),
		 'm_dramount'=>"",
		 'm_description'=>$this->input->post('txtremark'),
		 'm_transdate'=>$date->format( 'Y-m-d H-i-s' ),
		 'm_deduction'=>"",
		 'm_refrence_id'=>$this->session->userdata('profile_id'),
         'm_ledger_type'=>$this->input->post('txtstatus'),
		 'm_current_balance'=>$this->input->post('txtavailamt')+$this->input->post('txtdepositamt'),
		 'm_datetime'=>$date->format( 'Y-m-d H-i-s' )
		  ); 
		  $this->db->insert('tr03_manage_ledger', $requestdata);
}
}
if($this->input->post('txtstatus')=="2" && trim($row->or_m_reg_id)!="" && $this->input->post('txtdepositamt')>0 )
{
$requestdata = array(
		  		 'm_u_id'=>trim($row->or_m_reg_id),
		  		 'm_trans_id'=>$d,
		  		 'm_cramount'=>"",
		  		 'm_dramount'=>$this->input->post('txtdepositamt'),
		  		 'm_description'=>$this->input->post('txtremark'),
		  		 'm_transdate'=>$date->format( 'Y-m-d H-i-s' ),
		  		 'm_deduction'=>$this->input->post('txtdepositamt'),
		  		 'm_refrence_id'=>$this->session->userdata('profile_id'),
                 'm_ledger_type'=>$this->input->post('txtstatus'),
		  		 'm_current_balance'=>$this->input->post('txtavailamt')-$this->input->post('txtdepositamt'),
		  		 'm_datetime'=>$date->format( 'Y-m-d H-i-s' )
		  		  ); 
				    $this->db->insert('tr03_manage_ledger', $requestdata);
		  		  }

}
}
}
}

public function select_fund_request()
 {
 $this->load->database();
 $this->load->helper('url');
 $this->db->where('m_ft_status',1);
 return $this->db->get('tr02_fund_transaction');
 }
 
  public function fund_requestapproved()
  {
   $this->load->database();
   $this->load->helper('url');
   $timezone = new DateTimeZone("Asia/Kolkata" );
   $date = new DateTime();
   $date->setTimezone($timezone);
   if(trim($this->input->post('userid'))!="")
   {
   $this->db->where('m_ft_id',trim($this->input->post('freq_id')));
   $this->db->where('m_ft_mid',trim($this->input->post('userid')));
   $data['query']=$this->db->get('tr02_fund_transaction');
   foreach($data['query']->result() as $row )
   {
   break;
   }
   if(trim($row->m_ft_id)!="")
   {
   $requestdata = array(
		 'm_u_id'=>trim($this->input->post('userid')),
		 'm_trans_id'=>1,
		 'm_cramount'=>$row->m_ft_amount,
		 'm_dramount'=>"",
		 'm_description'=>"Grant by admin against fund request",
		 'm_transdate'=>$date->format( 'Y-m-d H-i-s' ),
		 'm_deduction'=>"",
		 'm_refrence_id'=>$this->session->userdata('profile_id'),
                 'm_ledger_type'=>3,
		 'm_current_balance'=>'',
		 'm_datetime'=>$date->format( 'Y-m-d H-i-s' )
		  );
    $this->db->insert('tr03_manage_ledger', $requestdata); 
    if($this->session->userdata('profile_id')!="0")
    {
      $m_u_id=trim($row->or_m_reg_id);
    }
    else
    {
      $m_u_id=trim($this->session->userdata('profile_id'));  
    }
if($this->session->userdata('profile_id')!="0")
{
$query=$this->db->get_where('m06_user_detail',array('or_m_reg_id'=>trim($this->session->userdata('profile_id'))));
$row1 = $query->row();
$ref_id=$row1->or_m_intr_id;
		  }
		  else
		  {
		  $ref_id=0;
		  }
    $responsedata = array(
                 'm_u_id'=>$m_u_id,
		  		 'm_trans_id'=>1,
		  		 'm_cramount'=>"",
		  		 'm_dramount'=>$row->m_ft_amount,
		  		 'm_description'=>"After Grant by admin against fund request",
		  		 'm_transdate'=>$date->format( 'Y-m-d H-i-s' ),
		  		 'm_deduction'=>'',
		  		 'm_refrence_id'=>$this->session->userdata('profile_id'),
                 'm_ledger_type'=>3,
		  		 'm_current_balance'=>'',
		  		 'm_datetime'=>$date->format( 'Y-m-d H-i-s' )
		  		  );
    $this->db->insert('tr03_manage_ledger', $responsedata);
    $updatedata=array(
         'm_ft_status'=>0,
         'm_ft_description'=>"Grant by admin against fund request",
         'm_ft_admindate'=>$date->format( 'Y-m-d H-i-s' )
    );
    $this->db->where('m_ft_id',trim($this->input->post('freq_id')));
    $this->db->where('m_ft_mid',trim($this->input->post('userid')));
    $this->db->update('tr02_fund_transaction', $updatedata);
   }
   }
 else
 {
 echo 'Sorry';
 }
    }



public function transfer_id()
{
$this->load->database();
$this->load->helper('url');
$timezone = new DateTimeZone("Asia/Kolkata" );
$date = new DateTime();
$date->setTimezone($timezone );
$d=$date->format( 'YmdHis' );
$this->load->model('users_records');
if(trim($this->input->post('txtintuserid'))!="")
{
$query=$this->db->get_where('m06_user_detail',array('or_m_user_id'=>trim($this->input->post('txtintuserid'))));
$row = $query->row();
if(trim($this->session->userdata('profile_id'))==$row->or_m_intr_id || $this->session->userdata('profile_id')==0)
{
if($this->session->userdata('profile_id')!=0 && $this->input->post('txtdepositamt')>0)
{
$avail_limit=$this->users_records->user_avail_id($this->session->userdata('profile_id'));	
$availlimit=$this->input->post('txtavailamt');
$depositlimit=$this->input->post('txtdepositamt');
$currentlimit=$availlimit+$depositlimit;
if($avail_limit > $depositlimit && $this->input->post('txtstatus')== "1" && trim($row->or_m_reg_id)!="")
{
$requestdata = array(
		 'm_id_muid'=>trim($row->or_m_reg_id),
		 'm_id_cr'=>$this->input->post('txtdepositamt'),
		 'm_id_dr'=>"",
		 'm_id_refid'=>$this->session->userdata('profile_id'),
		 'm_id_desc'=>$this->input->post('txtremark'),
		 'm_id_tt'=>$this->input->post('txtstatus'),
		 'm_id_tot'=>$this->input->post('txtavailamt')+$this->input->post('txtdepositamt'),
		 'm_id_datetime'=>$date->format( 'Y-m-d H-i-s' )
		  ); 
		  $this->db->insert('tr10_id_ledger', $requestdata);
		  $requestdata1 = array(
		 'm_id_muid'=>$this->session->userdata('profile_id'),
		 'm_id_cr'=>"",
		 'm_id_dr'=>$this->input->post('txtdepositamt'),
		 'm_id_refid'=>trim($row->or_m_reg_id),
		 'm_id_desc'=>$this->input->post('txtremark'),
		 'm_id_tt'=>2,
		 'm_id_tot'=>$avail_limit-$this->input->post('txtdepositamt'),
		 'm_id_datetime'=>$date->format( 'Y-m-d H-i-s' )
		  		  		  ); 
		  				$this->db->insert('tr10_id_ledger', $requestdata1);
		  }
		  if($this->input->post('txtstatus')=="2" && trim($row->or_m_reg_id)!="")
		  {
		  $this->load->model('users_records');
		  $avail_limit01=$this->users_records->user_avail_id(trim($row->or_m_reg_id));	
		  $ava=($avail_limit01-$this->input->post('txtdepositamt'));
		  if($ava >=0)
		  {
		  $requestdata = array(
		 'm_id_muid'=>trim($row->or_m_reg_id),
		 'm_id_cr'=>"",
		 'm_id_dr'=>$this->input->post('txtdepositamt'),
		 'm_id_refid'=>$this->session->userdata('profile_id'),
		 'm_id_desc'=>$this->input->post('txtremark'),
		 'm_id_tt'=>$this->input->post('txtstatus'),
		 'm_id_tot'=>$avail_limit01-$this->input->post('txtdepositamt'),
		 'm_id_datetime'=>$date->format( 'Y-m-d H-i-s' )
		  ); 
		  $this->db->insert('tr10_id_ledger', $requestdata);
		  		 
$requestdata1 = array(
		'm_id_muid'=>$this->session->userdata('profile_id'),
		 'm_id_cr'=>$this->input->post('txtdepositamt'),
		 'm_id_dr'=>"",
		 'm_id_refid'=>trim($row->or_m_reg_id),
		 'm_id_desc'=>$this->input->post('txtremark'),
		 'm_id_tt'=>1,
		 'm_id_tot'=>$avail_limit+$this->input->post('txtdepositamt'),
		 'm_id_datetime'=>$date->format( 'Y-m-d H-i-s' )
		  		  		  ); 
		  				$this->db->insert('tr10_id_ledger', $requestdata1);
			}

}
}
else
{
$availamt=$this->input->post('txtavailamt');
$depositamt=$this->input->post('txtdepositamt');
$currentbalance=$availamt+$depositamt;
$avail_limit11=$this->users_records->user_avail_id($row->or_m_reg_id);
if($this->session->userdata('profile_id')!=""&&$this->session->userdata('profile_id')==0 && $this->input->post('txtdepositamt')>0)
{
if($this->input->post('txtstatus')=="1" && trim($row->or_m_reg_id)!="" )
{
$requestdata = array(
		 'm_id_muid'=>trim($row->or_m_reg_id),
		 'm_id_cr'=>$this->input->post('txtdepositamt'),
		 'm_id_dr'=>"",
		 'm_id_refid'=>$this->session->userdata('profile_id'),
		 'm_id_desc'=>$this->input->post('txtremark'),
		 'm_id_tt'=>$this->input->post('txtstatus'),
		 'm_id_tot'=>$avail_limit11+$this->input->post('txtdepositamt'),
		 'm_id_datetime'=>$date->format( 'Y-m-d H-i-s' )
		  		  		  ); 
		  				$this->db->insert('tr10_id_ledger', $requestdata);
}
}
if($this->input->post('txtstatus')=="2" && trim($row->or_m_reg_id)!="" && $this->input->post('txtdepositamt')>0 )
{
$requestdata = array(
		  		'm_id_muid'=>trim($row->or_m_reg_id),
		 'm_id_cr'=>"",
		 'm_id_dr'=>$this->input->post('txtdepositamt'),
		 'm_id_refid'=>$this->session->userdata('profile_id'),
		 'm_id_desc'=>$this->input->post('txtremark'),
		 'm_id_tt'=>$this->input->post('txtstatus'),
		 'm_id_tot'=>$avail_limit11-$this->input->post('txtdepositamt'),
		 'm_id_datetime'=>$date->format( 'Y-m-d H-i-s' )
		  		  		  ); 
		  				$this->db->insert('tr10_id_ledger', $requestdata);
		  		  }

}
}
}
}
  
  
}
?>