<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Project_model extends CI_Model
	{
		public function __construct() 
		{
			parent::__construct();
			$timezone = new DateTimeZone("Asia/Kolkata" );
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
		}
		
		// --------------------------------Start Insert Project Here-----------------------------------------
		
		public function insert_project()
		{
			$data=array(
			'm_project_id'=>'',
			'm_project_name'=>$this->input->post('txtpname'),
			'm_project_type'=>$this->input->post('txttype'),
			'm_project_description'=>$this->input->post('txtProject_des'),
			'm_project_file'=>$this->input->post('txtfileatt'),
			'm_project_startdate'=>$this->input->post('txtfrom'),
			'm_project_enddate'=>$this->input->post('txtto'),
			'm_project_stauts'=>1,
			'm_project_create'=>$this->curr,
			'proc'=>1
			);
			$query = " CALL sp_development_project(?" . str_repeat(",?", count($data)-1) . " ,@msg) ";
			$this->db->query($query, $data);
			$data=$this->db->query("SELECT @msg as resp");
			$rows=$data->row();
			echo $rows->resp;
		}
		
		
		// --------------------------------Start Update Project Here-----------------------------------------
		
		public function update_project()
		{
			$data=array(
			'm_project_id'=>$this->uri->segment(3),
			'm_project_name'=>$this->input->post('txtpname'),
			'm_project_type'=>$this->input->post('txttype'),
			'm_project_description'=>$this->input->post('txtProject_des'),
			'm_project_file'=>$this->input->post('txtfileatt'),
			'm_project_startdate'=>$this->input->post('txtfrom'),
			'm_project_enddate'=>$this->input->post('txtto'),
			'm_project_stauts'=>1,
			'm_project_create'=>$this->curr,
			'proc'=>2
			);
			$query = " CALL sp_development_project(?" . str_repeat(",?", count($data)-1) . " ,@msg) ";
			$this->db->query($query, $data);
			$data=$this->db->query("SELECT @msg as resp");
			$rows=$data->row();
			echo $rows->resp;
		}
		
		// --------------------------------Start Status Change Project Here-----------------------------------------
		
		public function project_status()
		{
			$data=array(
			'm_project_id'=>$this->uri->segment(3),
			'm_project_name'=>'',
			'm_project_type'=>'',
			'm_project_description'=>'',
			'm_project_file'=>'',
			'm_project_startdate'=>'',
			'm_project_enddate'=>'',
			'm_project_stauts'=>$this->uri->segment(4),
			'm_project_create'=>'',
			'proc'=>3
			);
			$query = " CALL sp_development_project(?" . str_repeat(",?", count($data)-1) . " ,@msg) ";
			$this->db->query($query, $data);
			$data=$this->db->query("SELECT @msg as resp");
			$rows=$data->row();
			echo $rows->resp;
		}
		
		// -------------------Start Add Module Here---------------------------
		
		public function insert_module()
		{
			$data=array(
				'm_module_id'=>'',
				'm_module_project_id'=>$this->uri->segment(3),
				'm_module_name'=>$this->input->post('txtname'),
				'm_module_description'=>$this->input->post('txtdescription'),
				'm_module_doc'=>$this->input->post('txtfileatt'),
				'm_module_parent_id'=>$this->input->post('txtparentid'),
				'm_module_status'=>1,
				'm_module_date'=>$this->curr,
				'proc'=>1
				);
			$query = " CALL sp_project_module(?" . str_repeat(",?", count($data)-1) . " ,@msg) ";
			$this->db->query($query, $data);
			$data=$this->db->query("SELECT @msg as resp");
			$rows=$data->row();
			echo $rows->resp;
		}
		public function update_module()
		{
			$data=array(
			'm_module_id'=>$this->uri->segment(3),
			'm_module_project_id'=>'',
			'm_module_name'=>$this->input->post('txtname'),
			'm_module_description'=>$this->input->post('txtdescription'),
			'm_module_doc'=>$this->input->post('txtfileatt'),
			'm_module_parent_id'=>'',
			'm_module_status'=>'',
			'm_module_date'=>$this->curr,
			'proc'=>2
			);
			$query = " CALL sp_project_module(?" . str_repeat(",?", count($data)-1) . " ,@msg) ";
			$this->db->query($query, $data);
			$data=$this->db->query("SELECT @msg as resp");
			$rows=$data->row();
			echo $rows->resp;
		}
		// -------------------Start Add Participants Here---------------------------
		
		public function participants_add()
		{
		    
		    $pr=explode('-',$this->input->post('txtprojectid'));
			$data=array(
			'parti_id'=>'',
			'parti_account_id'=>$pr[0],
			'parti_project_id'=>$pr[1],
			'parti_participants_id'=>$this->input->post('txtparticipant'),
			'parti_designation'=>$this->input->post('txtdesignation'),
			'parti_admin_right'=>$this->input->post('chadminright'),
			'parti_add_participant'=>$this->input->post('chaddparticipant'),
			'parti_view_right'=>$this->input->post('chviewright'),
			'parti_update_right'=>$this->input->post('chupdateright'),
			'parti_project_noti'=>$this->input->post('chproject_noti'),
			'parti_task_noti'=>$this->input->post('chtask_noti'),
			'parti_add_date'=>$this->curr,
			'parti_status'=>1,
			'proc'=>1
			);
			$query = " CALL sp_project_participants(?" . str_repeat(",?", count($data)-1) . " ,@msg) ";
		    //echo $query;
			$this->db->query($query, $data);
			$data=$this->db->query("SELECT @msg as resp");
			$rows=$data->row();
			echo $rows->resp;
		}
		
		// --------------------------------Start Update Participants Here-----------------------------------------
		
		public function participants_update()
		{
			$data=array(
			'parti_id'=>$this->uri->segment(3),
			'parti_account_id'=>1,
			'parti_project_id'=>$this->input->post('txtprojectid'),
			'parti_participants_id'=>$this->input->post('txtparticipant'),
			'parti_designation'=>$this->input->post('txtdesignation'),
			'parti_admin_right'=>$this->input->post('chadminright'),
			'parti_add_participant'=>$this->input->post('chaddparticipant'),
			'parti_view_right'=>$this->input->post('chviewright'),
			'parti_update_right'=>$this->input->post('chupdateright'),
			'parti_project_noti'=>$this->input->post('chproject_noti'),
			'parti_task_noti'=>$this->input->post('chtask_noti'),
			'parti_add_date'=>$this->curr,
			'parti_status'=>'1',
			'proc'=>2
			);
			$query = " CALL sp_project_participants(?" . str_repeat(",?", count($data)-1) . ",@msg) ";
			$this->db->query($query, $data);
			$data=$this->db->query("SELECT @msg as resp");
			$rows=$data->row();
			return $rows->resp;
		}
		
		// --------------------------------Start Status Change Participants Here-----------------------------------------
		
		public function participants_status()
		{
			$data=array(
			'parti_id'=>$this->uri->segment(3),
			'parti_account_id'=>'',
			'parti_project_id'=>'',
			'parti_participants_id'=>'',
            'parti_designation'=>'',
			'parti_admin_right'=>'',
			'parti_add_participant'=>'',
			'parti_view_right'=>'',
			'parti_update_right'=>'',
			'parti_project_noti'=>'',
			'parti_task_noti'=>'',
			'parti_add_date'=>'',
			'parti_status'=>$this->uri->segment(4),
			'proc'=>3
			);
			$query = "CALL sp_project_participants(?" . str_repeat(",?", count($data)-1) . ",@msg) ";
			$this->db->query($query, $data);
			$data=$this->db->query("SELECT @msg as resp");
			$rows=$data->row();
			//echo $rows->resp;
		}
		
		// -----------------------Start insert Project Task Here------------------------
		
		public function insert_project_task()
		{			
			$priority=$this->input->post('ddpriority');
			$priority1="";
			$name="";
			$email="";
			
			$data=array(
			'm_ptask_id'=>'',
			'm_ptask_title'=>$this->input->post('txtsubject'),
			'm_ptask_project'=>$this->input->post('txtproject'),
			'm_ptask_assign_by'=>$this->input->post('txtassign_id'),
			'm_ptask_priority'=>$priority,
			'm_ptask_start_date'=>$this->input->post('txtfrom'),
			'm_ptask_end_date'=>$this->input->post('txtto'),
			'm_ptask_assign_to'=>$this->input->post('ddassignto'),
			'm_ptask_follower'=>$this->input->post('ddfollower'),
			'm_ptask_recurence'=>$this->input->post('checkrec'),
			'm_ptask_schedule'=>$this->input->post('ddschedule'),
			'm_ptask_file'=>$this->input->post('filename'),
			'm_ptask_description'=>$this->input->post('txtcomment'),
			'm_ptask_status'=>'1',
			'm_ptask_date'=>$this->curr,
            'm_task_usertype'=>$this->session->userdata('user_type'),
			'proc'=>1
			);
			$query = "CALL sp_project_task(?" . str_repeat(",?", count($data)-1) . ",@msg) ";
			$this->db->query($query, $data);
			$data=$this->db->query("SELECT @msg as resp");
			$rows=$data->row();
			
			if($priority=='1')
			{
				$priority1="High";
			}
			if($priority=='2')
			{
				$priority1="Medium";
			}
			if($priority=='3')
			{
				$priority1="Low";
			}
			
			$query=$this->db->query("SELECT get_EmailbyId(".$this->input->post('ddassignto').") AS email");
			$row1=$query->row();
			$email=$row1->email;
			
			$query=$this->db->query("SELECT GetNameById(".$this->input->post('ddassignto').") AS uname");
			$row2=$query->row();
			$name=$row2->uname;
			
			$config['mailtype'] = 'html';
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from('support@ferryipl.com','Admin Ferryipl'); 						// change it to yours
			$this->email->to($email);															// change it to yours
			$this->email->bcc('prog.vivekjaiswal@gmail.com');
			$this->email->subject('Task Assign');
			$this->email->message('<p>Dear '.$name.'<br><br>This is to inform, that your coordination is required in resolving the Task assigned to you. please login your account in http://soft.ferryipl.com/ and check your task.<br><br>Task Subject :'.$this->input->post('txtsubject').'<br><br>Priority : '.$priority1.' </p>');
			//$this->email->send();
			echo $rows->resp;
		}
		
		// --------------------Start Task Update,Enable And Disable Project Task Here-------------------------
		
		public function view_task_complete()
		{
			$data=array(
			'm_ptask_id'=>$this->uri->segment(3),
			'm_ptask_title'=>'',
			'm_ptask_project'=>'',
			'm_ptask_assign_by'=>'',
			'm_ptask_priority'=>'',
			'm_ptask_start_date'=>'',
			'm_ptask_end_date'=>'',
			'm_ptask_assign_to'=>'',
			'm_ptask_follower'=>'',
			'm_ptask_recurence'=>'',
			'm_ptask_schedule'=>'',
			'm_ptask_file'=>'',
			'm_ptask_description'=>'',
			'm_ptask_status'=>$this->uri->segment(4),
			'm_ptask_date'=>$this->curr,
            'm_task_usertype'=>$this->session->userdata('user_type'),
			'proc'=>2
			);
			$query = "CALL sp_project_task(?" . str_repeat(",?", count($data)-1) . ",@msg) ";
			$this->db->query($query, $data);
			$data=$this->db->query("SELECT @msg as resp");
			$rows=$data->row();
			echo $rows->resp;
		}
		
		// --------------------Insert Task Follow up-------------------------
		
		public function insert_follow_up()
		{
			if($this->session->userdata('profile_id')==0)
			{
				$task_response_type=0;
			}
			else
			{
				$task_response_type=1;
			}
			$taskdata=array(
				'm_ptask_id' => $this->input->post('txttaskid'),
				'm_ptask_title' => '',
				'm_ptask_project' => '',
				'm_ptask_assign_by' => $this->session->userdata('profile_id'),
				'm_ptask_priority' => $this->session->userdata('user_type'),
				'm_ptask_start_date' => '',
				'm_ptask_end_date' => '',
				'm_ptask_assign_to' => '',
				'm_ptask_follower' => '',
				'm_ptask_recurence' => '',
				'm_ptask_schedule' => '',
				'm_ptask_file' => '',
				'm_ptask_description' => $this->input->post('txtdescription'),
				'm_ptask_status' => 1,
				'm_ptask_date' => $this->curr,
                'm_task_usertype'=>$this->session->userdata('user_type'),
				'proc' => 4
			);
			$query = "CALL sp_project_task(?" . str_repeat(",?", count($taskdata)-1) . ",@msg) ";
			$this->db->query($query, $taskdata);
			$data=$this->db->query("SELECT @msg as resp");
			$rows=$data->row();
			echo $this->input->post('txttaskid');
		}
		
		// --------------------------------Start update Project Task Here-----------------------------------------
		
		public function update_project_task()
		{
			$data=array(
			'm_ptask_id'=>$this->input->post('txttaskid'),
			'm_ptask_title'=>$this->input->post('txtsubject'),
			'm_ptask_project'=>'',
			'm_ptask_assign_by'=>'',
			'm_ptask_priority'=>$this->input->post('ddpriority'),
			'm_ptask_start_date'=>$this->input->post('txtfrom'),
			'm_ptask_end_date'=>$this->input->post('txtto'),
			'm_ptask_assign_to'=>'',
			'm_ptask_follower'=>'',
			'm_ptask_recurence'=>'',
			'm_ptask_schedule'=>'',
			'm_ptask_file'=>'',
			'm_ptask_description'=>$this->input->post('txtcomment'),
			'm_ptask_status'=>'1',
			'm_ptask_date'=>$this->curr,
            'm_task_usertype'=>$this->session->userdata('user_type'),
			'proc'=>3
			);
			$query = "CALL sp_project_task(?" . str_repeat(",?", count($data)-1) . ",@msg) ";
			$this->db->query($query, $data);
			$data=$this->db->query("SELECT @msg as resp");
			$rows=$data->row();
			echo $rows->resp;
		}
		
		
	}
?>		