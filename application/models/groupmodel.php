<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Groupmodel extends CI_Model
	{
		public function __construct() 
		{
			parent::__construct();
			$timezone = new DateTimeZone("Asia/Kolkata" );
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
		}
		
		// --------------------------------Start Insert API Here-----------------------------------------
		
		public function add_api()
		{
			$api=array(
			'Api_id'=>'',
			'Api_name'=>$this->input->post('txtapi_provname'),
			'Api_url'=>$this->input->post('txtapi_url'),
			'Api_status'=>$this->input->post('rbapi'),
			'proc'=>1
			);
			$query="CALL sp_api(?" . str_repeat(",?", count($api)-1) . ")";
			$this->db->query($query,$api);
		}
		
		
	}
?>