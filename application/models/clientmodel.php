<?php
	class Clientmodel extends CI_Model
	{
		
		/*--------------------Create registration----------------------------*/
		
		public function add_new_client()
		{
			$this->load->database();
			$this->load->library('session');
			$this->load->helper('url');
			if($this->input->post('txtlname')!="")
			{
				$last_name=$this->input->post('txtlname');
			}
			else
			{
				$last_name="";
			}
			if($this->input->post('txtcompany')!="")
			{
				$conpany_name=$this->input->post('txtcompany');
			}
			else
			{
				$conpany_name="";
			}
			if($this->input->post('txtzip')!="")
			{
				$zip_code=$this->input->post('txtzip');
			}
			else
			{
				$zip_code="";
			}
			$data=array(
			'txtfname'=>$this->input->post('txtfname'),
			'txtlname'=>$last_name,
			'txtcompany'=>$conpany_name,
			'txtemail'=>$this->input->post('txtemail'),
			'txtcountry'=>$this->input->post('txtcountry'),
			'txtstate'=>$this->input->post('txtstate'),
			'txtcity'=>$this->input->post('txtcity'),
			'txtlocality'=>$this->input->post('txtlocality'),
			'txtzip'=>$zip_code,
			'txtphone'=>$this->input->post('txtphone'),
			'txtcurrency'=>$this->input->post('txtcurrency'),
			'txtstatus'=>$this->input->post('txtstatus'),
			'txtpaymentmethod'=>'',
			'txtbillingcid'=>''
			);
			$data_log=array(
			'txtemail'=>$this->input->post('txtemail'),
			'password'=>$this->input->post('password'),
			'txtstatus'=>$this->input->post('txtstatus')
			);
			$this->db->insert('m35_user_detail',$data);
			$this->db->insert('tr19_login',$data_log);
		}
		
		/*--------------------Submit ticket----------------------------*/
		public function add_new_client_ticket($fileupload)
		{
			$this->load->database();
			$this->load->library('session');
			$this->load->helper('url');
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			if($fileupload=="")
			{
				$fi="";
			}
			else
			{
				$fi=$fileupload;
			}
			
			$data1=array(
		    'proc'=>1,
			'ticket_no'=>$this->input->post('txtticket'),
			'tkt_person_name'=>$this->input->post('txtname'),
			'tkt_email'=>$this->input->post('txtemail'),
			'tkt_department'=>$this->input->post('txtdepartment'),
			'emp_id'=>0,
			'tkt_subject'=>$this->input->post('txtsubject'),
			'tkt_urgency'=>$this->input->post('txturgency'),
			'tkt_discription'=>$this->input->post('txtdiscription'),
			'tkt_response_type'=>2,
            'response_by'=>$this->input->post('txtemail'),
			'tkt_userfile'=>$fi,
			'tkt_status'=>1,
			'account_id'=>0,
			'affiliate_id'=>$this->session->userdata('affid'),
			'tkt_sub_date'=>$this->curr,
			'trans_description'=>$this->input->post('txtdiscription'),
			'trans_response_date'=>$this->curr,
			'trans_status'=>1
			);
		 	$query = " CALL sp_ticket_submission(?" . str_repeat(",?", count($data1)-1) . ") ";
			$data['bond_data']=$this->db->query($query,$data1);	
			$this->db->free_db_resource();
			//$this->db->insert('tr20_submit_ticket',$data1);
			
		}
		
		
		
		public function add_new_ticket($fileupload)
		{
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			if($fileupload=="")
			{
				$fi="";
			}
			else
			{
				$fi=$fileupload;
			}
			$acc_id='';
			$mail=$this->input->post('txtemail');
			$this->db->where('account_email',$mail);
			$data['acc']=$this->db->get('m33_account');
			foreach($data['acc']->result() as $row)
			{
				$acc_id=$row->account_id;
			}
			$data1=array(
		    'proc'=>1,
			'ticket_no'=>$this->input->post('txtticket'),
			'tkt_person_name'=>$this->input->post('txtname'),
			'tkt_email'=>$this->input->post('txtemail'),
			'tkt_department'=>$this->input->post('txtdepartmentid'),
			'emp_id'=>0,
			'tkt_subject'=>$this->input->post('txtsubject'),
			'tkt_urgency'=>$this->input->post('txturgency'),
			'tkt_discription'=>$this->input->post('txtdiscription'),
			'tkt_response_type'=>3,
            'response_by'=>$this->input->post('txtemail'),
			'tkt_userfile'=>$fi,
			'tkt_status'=>1,
			'account_id'=>$acc_id,
			'affiliate_id'=>$this->session->userdata('affid'),
			'tkt_sub_date'=>$this->curr,
			'trans_description'=>$this->input->post('txtdiscription'),
			'trans_response_date'=>$this->curr,
			'trans_status'=>1
			);
		 	$query = " CALL sp_ticket_submission(?" . str_repeat(",?", count($data1)-1) . ", @a) ";
			$this->db->query($query,$data1);	
			
			//$this->db->insert('tr20_submit_ticket',$data1);
			
		}
		
		
		public function add_new_outer_ticket($fileupload)
		{
			$this->load->database();
			$this->load->library('session');
			$this->load->helper('url');
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			if($fileupload=="")
			{
				$fi="";
			}
			else
			{
				$fi=$fileupload;
			}
			/*$tic=SELECT GetNewTicketNo((SELECT SUBSTRING(RAND() FROM 6 FOR 6) AS ticket_no)) AS tkt_no);*/
			$data1=array(
		    'proc'=>1,
			'tkt_ref_no'=>$this->input->post('txtticket'),
			'tkt_person_name'=>$this->input->post('txtname'),
			'tkt_email'=>$this->input->post('txtemail'),
			'tkt_department'=>$this->input->post('txtdepartment'),
			'emp_id'=>0,
			'tkt_subject'=>$this->input->post('txtsubject'),
			'tkt_urgency'=>$this->input->post('txturgency'),
			'tkt_discription'=>$this->input->post('txtdiscription'),
			'tkt_response_type'=>4,
			'tkt_response_by'=>$this->input->post('txtemail'),
			'tkt_userfile'=>$fi,
			'tkt_status'=>1,
			'account_id'=>0,
			'affiliate_id'=>$this->session->userdata('affid'),
			'tkt_sub_date'=>$this->curr,
			'trans_description'=>$this->input->post('txtdiscription'),
			'trans_response_date'=>$this->curr,
			'trans_status'=>1
			);
			
			$query = " CALL sp_ticket_submission(?" . str_repeat(",?", count($data1)-1) . ",@a) ";
			$data['bond_data']=$this->db->query($query,$data1);	
			$data['response']=$this->db->query("SELECT @a as resp");
			foreach($data['response']->result() as $rows)
			{break;}
			$ids =$rows->resp;
			return $ids;
			//$this->db->insert('tr20_submit_ticket',$data1);
			
			
		}
		
		
	}
?>