<?php
	class users_records extends CI_Model
	{
		
		public function get_autocomplete($search_data,$for) {
			$this->load->database();
			$this->load->library('session');
			if($for==2)
			{
				$this->db->select('contact_id,contact_name');
				$this->db->like('contact_name', $search_data);
				return $this->db->get('m34_contact', 10);
			}
			if($for==1)
			{
				$t=array('0','7');
				$this->db->select('lead_id,lead_name');
				$this->db->like('lead_name', $search_data);
				$this->db->where_not_in('lead_status',$t);
				return $this->db->get('m32_lead', 10);
			}
		}
		
		public function signup($loginpass,$pinpass)
		{
			$this->load->database();
			$this->load->library('session');
			$email=$this->input->post('txtemail');
			$isValidate = true;
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone );
			if($isValidate == true){
				$infodata = array(
				'or_m_user_id'=>trim($this->input->post('txtmobno')),
				'or_m_designation'=>trim($this->input->post('dddesignation')),
				'or_m_name'=>trim($this->input->post('txtname')),
				'or_m_dob'=>trim($this->input->post('txtmemdob')),
				'or_m_gurdian_name'=>trim($this->input->post('txtgurdians')),
				'or_member_joining_date'=>$date->format( 'Y-m-d' ),
				'or_m_gender'=>trim($this->input->post('ddlgender')), 
				'or_m_email'=>trim($this->input->post('txtemail')), 
				'or_m_landline_no'=>trim($this->input->post('txtllno')), 
				'or_m_mobile_no'=>trim($this->input->post('txtmobno')), 
				'or_m_address'=>trim($this->input->post('txtaddress')), 
				'or_m_pincode'=>trim($this->input->post('txtpincode')), 
				'or_m_country'=>trim($this->input->post('txtcountry')), 
				'or_m_state'=>trim($this->input->post('ddstate')),
				'or_m_city'=>trim($this->input->post('ddcity')),
				'or_m_status'=>0,
				'or_m_intr_id'=>trim($this->session->userdata('profile_id')), 
				'or_m_intr_name'=>trim($this->input->post('txtintusername')),
				'or_m_aff_id'=>trim($this->session->userdata('affid')),
				'or_m_regdate'=>$date->format( 'Y-m-d H-i-s')
				);
				$this->db->insert('m06_user_detail', $infodata);
				$query=$this->db->get_where('m06_user_detail',array('or_m_user_id'=>trim($this->input->post('txtmobno'))));
				$row = $query->row();
				$bankdata = array(
				'or_m_id'=>trim($row->or_m_reg_id),
				'or_m_b_ifscode'=>trim($this->input->post('txtifscode')),
				'or_m_b_cbsacno'=>trim($this->input->post('txtcbsacno')), 
				'or_m_b_name'=>trim($this->input->post('ddbankname')), 
				'or_m_b_branch'=>trim($this->input->post('txtbankbrachname')),
				'or_m_b_pancard'=>trim($this->input->post('txtpancard')), 
				'or_m_b_status'=>0
				);
				$this->db->insert('m07_user_bank', $bankdata);
				
				$paymentdata = array(
				'or_m_id'=>trim($row->or_m_reg_id),
				'or_m_pd_mode'=>trim($this->input->post('ddpaymode')),
				'or_m_pd_amt'=>trim($this->input->post('txtamt')),
				'or_m_pd_paydate'=>trim($this->input->post('txtpaydate')),
				'or_m_pd_dd_date'=>trim($this->input->post('txtdddate')),
				'or_m_pd_dd_no'=>trim($this->input->post('txtddno')), 
				'or_m_pd_bankname'=>trim($this->input->post('ddddbank')),
				'or_m_pd_branchname'=>trim($this->input->post('txtddbbrachname')),
				'or_m_pd_accno'=>trim($this->input->post('txtddacno')), 
				'or_m_pd_status'=>0
				); 
				$this->db->insert('m09_user_payment', $paymentdata);
				$logindata = array(
				'or_designation'=>trim($row->or_m_reg_id),
				'or_login_id'=>trim($this->input->post('txtmobno')),
				'or_login_pwd'=>trim($loginpass),
				'or_pin_pwd'=>trim($pinpass),
				'or_affiliateid'=>'0',
				'or_login_type'=>trim($this->input->post('dddesignation')),
				
				); 
				$this->db->insert('tr04_login', $logindata);
				
				echo "true";
			}
			else
			{
				echo "false";
			}
		}
        public function get_uid($id)
		{
			$aid=$id;
			$this->load->library('session');
			$this->load->database();
			$this->load->helper('url');
			$this->db->where('or_m_user_id',$aid);
			$data['membername']=$this->db->get('m06_user_detail');
			foreach($data['membername']->result() as $row)
			{
				$mname=$row->or_m_reg_id;
				return $mname;
			}
		}
		
        public function get_uid1($id)
		{
			$aid=$id;
			$this->load->library('session');
			$this->load->database();
			$this->load->helper('url');
			$this->db->where('or_m_user_id',$aid);
			$this->db->where('or_m_intr_id',$this->session->userdata('profile_id'));
			$data['membername']=$this->db->get('m06_user_detail');
			foreach($data['membername']->result() as $row)
			{
				$mname=$row->or_m_reg_id;
				return $mname;
			}
		}
		
		public function get_intid($id)
		{
			$this->load->database();
			$this->load->helper('url');
			$this->load->library('session');
			$intrid=$id;
			$query=$this->db->get_where('m06_user_detail',array('or_m_reg_id'=>$intrid ));
			$row = $query->row();
			if($query->num_rows()==1)
			{		// validate??
				echo $row->or_m_user_id;	
			}
			else
			{
				echo "false";		// RETURN ARRAY WITH ERROR
			}
		}	
		
		public function get_intname($id)
		{
			$this->load->database();
			$this->load->helper('url');
			$this->load->library('session');
			$intrid=$id;
			$query=$this->db->get_where('m06_user_detail',array('or_m_reg_id'=>$intrid ));
			$row = $query->row();
			if($query->num_rows()==1)
			{		// validate??
				
				echo $row->or_m_name;	
			}
			else
			{
				echo "false";		// RETURN ARRAY WITH ERROR
			}
		}	
		
		public function get_intid1($id,$uid1)
		{
			$this->load->database();
			$this->load->helper('url');
			$this->load->library('session');
			$intrid=$uid1;
			$userid=$id;
			$query1=$this->db->get_where('m06_user_detail',array('or_m_user_id'=>$id ));
			$row1 = $query1->row();
			$query=$this->db->get_where('m06_user_detail',array('or_m_reg_id'=>$intrid ));
			$row = $query->row();
			if($query->num_rows()==1 && $query1->num_rows()==1)
			{		
				if($row->or_m_designation == $row1->or_m_designation	)	
				{	
					echo $row1->or_m_name;	
				}
				else
				{
					echo "false";		// RETURN ARRAY WITH ERROR
				}
			}	
			else
			{
				echo "false";
			}
		}
		public function introducerid()
		{
			$this->load->database();
			$this->load->helper('url');
			$this->load->library('session');
			$validateValue=trim($this->input->post('txtintuserid'));
			$query="";
			if($this->uri->segment(3)==0)
			{
					$query=$this->db->get_where('m34_contact',array('contact_id'=>$validateValue ));
			}
			if($this->uri->segment(3)==1)
			{
				$query=$this->db->get_where('m06_user_detail',array('or_m_reg_id'=>$validateValue ));
			}
			if($this->uri->segment(3)==2)
			{
					if($this->session->userdata('profile_id')==$validateValue && $this->uri->segment(3)==1)
					{
					$query=$this->db->get_where('m06_user_detail',array('or_m_reg_id'=>$validateValue ));
					 }
					if($this->session->userdata('profile_id')!=$validateValue && $this->uri->segment(3)==1)
					{
					$query=$this->db->get_where('m06_user_detail',array('or_m_reg_id'=>$validateValue ));
					 }
					if($this->session->userdata('profile_id')!=$validateValue && $this->uri->segment(3)==2)
					{
					$query=$this->db->get_where('m06_user_detail',array('or_m_reg_id'=>$validateValue ));
					 }
					if($this->session->userdata('profile_id')==$validateValue && $this->uri->segment(3)==2)
					{				
					$query=$this->db->get_where('m34_contact',array('contact_id'=>$validateValue ));
					}         
			}
			if($query->num_rows()==1)
			{	
				$row = $query->row();
				if($this->uri->segment(3)==0)
				{
				echo trim($row->contact_name);
				}
				if($this->uri->segment(3)==1)
				{
					echo  trim($row->or_m_name);	
				}
				if($this->uri->segment(3)==2)
				{
							if($this->session->userdata('profile_id')==$validateValue && $this->uri->segment(3)==1)
							{
							echo  trim($row->or_m_name);
							 }
							if($this->session->userdata('profile_id')!=$validateValue && $this->uri->segment(3)==1)
							{
							echo  trim($row->or_m_name);
							 }
							if($this->session->userdata('profile_id')!=$validateValue && $this->uri->segment(3)==2)
							{
							echo  trim($row->or_m_name);
							 }
							if($this->session->userdata('profile_id')==$validateValue && $this->uri->segment(3)==2)
							{				
							echo $row->contact_name;
							}					
				}
			}
			else
			{
				echo "false";		// RETURN ARRAY WITH ERROR
			}
		}	
		public function introducerid1()
		{
			$this->load->database();
			$this->load->helper('url');
			$this->load->library('session');
			$validateValue=trim($this->input->post('txtintuserid'));
			$query=$this->db->get_where('m06_user_detail',array('or_m_user_id'=>$validateValue ,'or_m_intr_id'=>$this->session->userdata('profile_id')));
			if($query->num_rows()==1)
			{		// validate??
				$row = $query->row();
				echo  $row->or_m_name ;	
			}
			else
			{
				echo "false";		// RETURN ARRAY WITH ERROR
			}
		}	
		// foe Edit Detail
		
		public function introducerid11()
		{
			$this->load->database();
			$this->load->helper('url');
			$this->load->library('session');
			$validateValue=trim($this->input->post('txtintuserid'));
			$query=$this->db->get_where('m06_user_detail',array('or_m_user_id'=>$validateValue ,'or_m_intr_id'=>$this->session->userdata('profile_id'),'or_m_intr_name'=>$this->session->userdata('name'),'or_m_status'=>1));
			if($query->num_rows()==1)
			{		// validate??
				$row = $query->row();
				echo  $row->or_m_name ;	
			}
			else
			{
				echo "false";		// RETURN ARRAY WITH ERROR
			}
		}
		
		public function useravail_amt()
		{
			$this->load->database();
			$this->load->helper('url');
			$id=trim($this->input->post('txtintuserid'));
			$query=$this->db->get_where('m06_user_detail',array('or_m_user_id'=>trim($id)));
			$row = $query->row();
			$query1=$this->db->get_where('tr03_manage_ledger',array('m_u_id'=>trim($row->or_m_reg_id)));
			$availamt=0;
			if($query1->num_rows()>=1)
			{
				foreach($query1->result() as $row1)
				{
					$availamt=$availamt+($row1->m_cramount-$row1->m_dramount) ;	
				}
				echo $availamt;
			}
			else
			{
				echo $availamt;
				//echo $row->or_m_reg_id;
			}
		}
		//For Show on Header Only Profile
		public function user_avail_amt()
		{
			$this->load->database();
			$this->load->helper('url');
			$query1=$this->db->get_where('tr03_manage_ledger',array('m_u_id'=>$this->session->userdata('profile_id')));
			$availamt=0;
			if($query1->num_rows()>0)
			{
				foreach($query1->result() as $row1)
				{
					$availamt=$availamt+($row1->m_cramount-$row1->m_dramount) ;	
				}
				return $availamt;
			}
			else
			{
				return $availamt;
				//echo $row->or_m_reg_id;
			}
		}
		
		public function user_avail_amt1($id)
		{
			$this->load->database();
			$this->load->helper('url');
			if($id!=0)
			{
				$query1=$this->db->get_where('tr03_manage_ledger',array('m_u_id'=>$id));
				$availamt=0;
				if($query1->num_rows()>0)
				{
					foreach($query1->result() as $row1)
					{
						$availamt=$availamt+($row1->m_cramount-$row1->m_dramount) ;	
					}
					return $availamt;
				}
				else
				{
					return $availamt;
					//echo $row->or_m_reg_id;
				}
			}
		}
		
		public function get_memberid($nm)
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$this->db->where('or_m_user_id',$nm);
			$this->db->where('or_m_status',1);
			$data['memberid']=$this->db->get('m06_user_detail');
			foreach($data['memberid']->result() as $row)
			{
				$mmid=$row->or_m_reg_id.','.$row->or_m_aff_id.','.$row->or_m_designation;
				return $mmid;
			}
			
			
			
			
		}
		
		public function get_membername($id)
		{
			$aid=$id;
			$this->load->library('session');
			$this->load->database();
			$this->load->helper('url');
			$this->db->where('or_m_reg_id',$aid);
			$this->db->where('or_m_status',1);
			$this->db->where('or_m_intr_id',$this->session->userdata('profile_id'));
			$this->db->where('or_m_intr_name',$this->session->userdata('name'));
			$data['membername']=$this->db->get('or_registration');
			foreach($data['membername']->result() as $row)
			{
				$mname=$row->or_m_name;
				return $mname;
			}
		}
		
		public function get_memberdesignation($id)
		{
			$aid=$id;
			$this->load->library('session');
			$this->load->database();
			$this->load->helper('url');
			$this->db->where('or_m_reg_id',$aid);
			$this->db->where('or_m_status',1);
			$this->db->where('or_m_intr_id',$this->session->userdata('profile_id'));
			$this->db->where('or_m_intr_name',$this->session->userdata('name'));
			$data['memberdesig']=$this->db->get('or_registration');
			foreach($data['memberdesig']->result() as $row)
			{
				$mdesig=$row->or_m_designation;
				return $mdesig;
			}
		}
		
		public function edit_user()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$this->db->where('or_m_reg_id',$this->uri->segment(4));
			return $this->db->get('m06_user_detail');
		}
		
		public function modaledit_personal_detail()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			if(trim($this->input->post('txtname'))!="")
			{
				if(trim($this->input->post('ddlgender'))!="")
				{
					if(trim($this->input->post('txtemail'))!="")
					{
						if(trim($this->input->post('ddstate'))!="" && trim($this->input->post('ddstate')!=-1))
						{ 
							if(trim($this->input->post('ddcity'))!="" && trim($this->input->post('ddcity')!=-1))
							{
								$infodata = array(
								'or_m_name'=>trim($this->input->post('txtname')),
								'or_m_gender'=>trim($this->input->post('ddlgender')), 
								'or_m_email'=>trim($this->input->post('txtemail')),  
								'or_m_address'=>trim($this->input->post('txtaddress')),  
								'or_m_state'=>trim($this->input->post('ddstate')),
								'or_m_city'=>trim($this->input->post('ddcity'))
								);
								$this->db->where('or_m_reg_id',trim($this->input->post('txtuserid')));
								$this->db->update('m06_user_detail',$infodata);
								$introdata = array(
								'or_m_intr_name'=>trim($this->input->post('txtname'))
								);
								$this->db->where('or_m_intr_id',trim($this->input->post('txtuserid')));
								$this->db->update('m06_user_detail',$introdata);
								echo "true";
							}
						}
					}
				}
			}
			
		}
		public function modaledit_bank_detail()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone );
			$bankdata = array(
			'or_m_b_ifscode'=>trim($this->input->post('txtifscode')),
			'or_m_b_cbsacno'=>trim($this->input->post('txtcbsacno')), 
			'or_m_b_name'=>trim($this->input->post('ddbankname')), 
			'or_m_b_branch'=>trim($this->input->post('txtbankbrachname')),
			'or_m_b_pancard'=>trim($this->input->post('txtpancard'))
			);
			$this->db->where('or_m_id',trim($this->input->post('txtuserid')));
			$this->db->where('or_m_b_status',1);
			$this->db->update('or_member_bank_detail', $bankdata);
		}
		public function modaledit_nominee_detail()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone );
			$nomineedata = array(
			'or_m_n_name'=>trim($this->input->post('txtnomineename')),
			'or_m_n_address'=>trim($this->input->post('txtnomineeaddress')), 
			'or_m_n_dob'=>trim($this->input->post('txtnomineedob')),  
			'or_m_n_gender'=>trim($this->input->post('ddnomgender')), 
			'or_m_n_relation'=>trim($this->input->post('txtrelation')) 
			); 
			$this->db->where('or_m_id',trim($this->input->post('txtuserid')));
			$this->db->where('or_m_n_status',1);
			$this->db->update('or_member_nominee', $nomineedata);
		}
		public function modaledit_payment_detail()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone );
			$paymentdata = array(
			'or_m_pd_mode'=>trim($this->input->post('ddpaymode')),
			'or_m_pd_amt'=>trim($this->input->post('txtamt')),
			'or_m_pd_paydate'=>trim($this->input->post('txtpaydate')),
			'or_m_pd_dd_date'=>trim($this->input->post('txtdddate')),
			'or_m_pd_dd_no'=>trim($this->input->post('txtddno')), 
			'or_m_pd_bankname'=>trim($this->input->post('ddddbank')),
			'or_m_pd_branchname'=>trim($this->input->post('txtddbbrachname')),
			'or_m_pd_accno'=>trim($this->input->post('txtddacno'))
			); 
			$this->db->where('or_m_id',trim($this->input->post('txtuserid')));
			$this->db->where('or_m_pd_status',1);
			$this->db->update('or_member_payment_detail', $paymentdata);
		}
		
		
		
		
		
		public function get_recharge_report()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database(); 
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);	 
			$todate=$this->input->post('txttojoin');
			$fromdate=$this->input->post('txtfromjoin');
			$mid=0;
			$mname="";
			$temid=$this->view_wholeteam($this->session->userdata('profile_id'));
			if($this->input->post('txttojoin')!="")
			{
				$todate=$this->input->post('txttojoin');
			}
			if($this->input->post('txtfromjoin')!="")
			{
				$fromdate=$this->input->post('txtfromjoin');
			}
			if($this->input->post('txtintuserid')!="")
			{
				$mid=$this->input->post('txtintuserid');
			}
			if($this->input->post('txtmemname')!="")
			{
				$mname=$this->input->post('txtmemname');
			}
			if($todate!='0' && $fromdate!='0')
			{
				$query = $this->db->query("select * from tr05_recharge_detail where DATE_FORMAT(`m_urd_requestdate`,'%Y-%m-%d')>=DATE_FORMAT('$fromdate','%Y-%m-%d') and DATE_FORMAT(`m_urd_requestdate`,'%Y-%m-%d')<=DATE_FORMAT('$todate','%Y-%m-%d') and m_urd_mid IN($temid) ORDER BY m_urd_id DESC");
				
			}
			else
			{
				$today = $date->format( 'Y-m-d');
				$query = $this->db->query("select * from tr05_recharge_detail where DATE_FORMAT(`m_urd_requestdate`,'%Y-%m-%d')=DATE_FORMAT('$today','%Y-%m-%d') and m_urd_mid IN($temid) ORDER BY m_urd_id DESC");
			}
			
			return $query;
		}
		
		public function get_ffrecharge_report()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database(); 
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);	 
			$todate=$date->format('Y-m-d');
			$fromdate=date('Y-m-d',strtotime('-45 days'));
			$temid=$this->view_wholeteam($this->session->userdata('profile_id'));
			if($todate!='0' && $fromdate!='0')
			{
				$query = $this->db->query("select * from tr05_recharge_detail where DATE_FORMAT(`m_urd_requestdate`,'%Y-%m-%d')>=DATE_FORMAT('$fromdate','%Y-%m-%d') and DATE_FORMAT(`m_urd_requestdate`,'%Y-%m-%d')<=DATE_FORMAT('$todate','%Y-%m-%d') and m_urd_mid IN($temid) ORDER BY m_urd_id DESC");
			}
			else
			{
				$today = $date->format('Y-m-d');
				$query = $this->db->query("select * from tr05_recharge_detail where DATE_FORMAT(`m_urd_requestdate`,'%Y-%m-%d')=DATE_FORMAT('$today','%Y-%m-%d') and m_urd_mid IN($temid) ORDER BY m_urd_id DESC");
			}
			
			return $query;
		}
		
		// Today Report
		
		public function get_todayrecharge_report()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database(); 
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);	 
			$todate=0;
			$fromdate=0;
			$temid=$this->view_wholeteam($this->session->userdata('profile_id'));
			if($todate!='0' && $fromdate!='0')
			{
				$query = $this->db->query("select * from tr05_recharge_detail where DATE_FORMAT(`m_urd_requestdate`,'%Y-%m-%d')>=DATE_FORMAT('$fromdate','%Y-%m-%d') and DATE_FORMAT(`m_urd_requestdate`,'%Y-%m-%d')<=DATE_FORMAT('$todate','%Y-%m-%d') and m_urd_mid IN($temid) ORDER BY m_urd_id DESC");
			}
			else
			{
				$today = $date->format('Y-m-d');
				$query = $this->db->query("select * from tr05_recharge_detail where DATE_FORMAT(`m_urd_requestdate`,'%Y-%m-%d')=DATE_FORMAT('$today','%Y-%m-%d') and m_urd_mid IN($temid) ORDER BY m_urd_id DESC");
			}
			
			return $query;
		}
		
		
		
		public function modaledit_user_status()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			if($this->uri->segment(4)==1)
			{
				$infodata = array(
				'or_m_status'=>trim($this->uri->segment(4))
				);
				$this->db->where('or_m_reg_id',trim($this->uri->segment(3)));
				$this->db->where('or_m_status',0);
			}
			if($this->uri->segment(4)==0)
			{
				$infodata = array(
				'or_m_status'=>trim($this->uri->segment(4))
				);
				$this->db->where('or_m_reg_id',trim($this->uri->segment(3)));
				$this->db->where('or_m_status',1);
			}
			$this->db->update('m06_user_detail',$infodata);
		}
		
		public function user_avail_introid()
		{
			$this->load->database();
			$this->load->helper('url');
			$query1=$this->db->get_where('tr10_id_ledger',array('m_id_muid'=>$this->session->userdata('profile_id')));
			$availamt=0;
			if($query1->num_rows()>0)
			{
				foreach($query1->result() as $row1)
				{
					$availamt=$availamt+($row1->m_id_cr-$row1->m_id_dr) ;	
				}
				return $availamt;
			}
			else
			{
				return $availamt;
				//echo $row->or_m_reg_id;
			}
		}
		public function user_avail_id($id)
		{
			$this->load->database();
			$this->load->helper('url');
			$query1=$this->db->get_where('tr10_id_ledger',array('m_id_muid'=>$id));
			$availamt=0;
			if($query1->num_rows()>0)
			{
				foreach($query1->result() as $row1)
				{
					$availamt=$availamt+($row1->m_id_cr-$row1->m_id_dr) ;	
				}
				return $availamt;
			}
			else
			{
				return $availamt;
				//echo $row->or_m_reg_id;
			}
		}
		public function user_avail_intid()
		{
			$this->load->database();
			$this->load->helper('url');
			$availamt=0;
			$id=trim($this->input->post('txtintuserid'));
			$query=$this->db->get_where('m06_user_detail',array('or_m_user_id'=>trim($id)));
			$row = $query->row();
			if($row->or_m_reg_id!=0)
			{
				$query1=$this->db->get_where('tr10_id_ledger',array('m_id_muid'=>$row->or_m_reg_id));
				if($query1->num_rows()>0)
				{
					foreach($query1->result() as $row1)
					{
						$availamt=$availamt+($row1->m_id_cr-$row1->m_id_dr) ;	
					}
				}
				echo $availamt;
			}
			
			
		}
		var $uuuid="";  
		
		public function scan_team1($id)
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$this->db->select('or_m_reg_id');
			$this->db->where('or_m_intr_id',$id);
			$data['mem1']=$this->db->get('m06_user_detail');
			foreach($data['mem1']->result() as $row1)
			{
				$intdid1=$row1->or_m_reg_id;
				$this->uuuid=$this->uuuid.$intdid1.',';
				$this->scan_team1($intdid1);
			}
		} 
		
		public function view_wholeteam($regid)
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$this->scan_team1($regid);
			return $this->uuuid.$regid;
		}
		
		public function get_dwr_report($start,$end,$status,$api,$sp,$memid)
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database(); 
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);	 
			$todate=$date->format( 'Y-m-d');
			$dates = array();
			$start1 = $start; //start date
			$end1 = $end; 
			$status1 = $status; 
			$api1 = $api; 
			$sp1 = $sp; 
			$memid1 = $memid; 
			$start1 = $current = strtotime($start1);
			$end1 = strtotime($end1);
			$daily="";
			while ($current <= $end1)
			{
				$dates[] = date('Y-m-d', $current);
				$days[]= date('w',$current);
				$current = strtotime('+1 days', $current);
			}
			for($t=0;count($dates)>$t;$t++)
			{
				$today =$dates[$t];
				$condition="select COUNT( * ) as total , SUM(  `m_urd_amount` ) as sum  from tr05_recharge_detail where DATE_FORMAT(`m_urd_requestdate`,'%Y-%m-%d')=DATE_FORMAT('$today','%Y-%m-%d')";
				if($status1!="" && $status1!=-1)
				{
					$condition=$condition." and m_urd_recharge_status=".$status1." " ;
				}
				if($api1!="" && $api1!=-1)
				{
					$condition=$condition."and m_urd_apiid=".$api1." ";
				}
				if($sp1!="" && $sp1!=-1)
				{
					$condition=$condition." and m_urd_s_id=".$sp1." ";
				}
				$condition=$condition." ORDER BY m_urd_id DESC ";
				$query = $this->db->query($condition );
				$data[$today]=$query;
				foreach($data[$today]->result() as $row)
				{
					$daily=$daily.'-'. $row->total.','.$row->sum;
				}
			} 
			return $daily;
		}
		
		public function get_marsuid($id)
		{
			$aid=$id;
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$this->db->where('or_m_reg_id',$aid);
			$data['mem']=$this->db->get('m06_user_detail');
			foreach($data['mem']->result() as $row)
			{
				$mname=$row->or_m_user_id;
			}
			return $mname;
		}
		
		
		public function get_lead()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$id=1;
			$sql="CALL sp_sel_lead($id)";
			$parameter=array();
			return $this->db->query($sql,$parameter);
		}
		public function get_lead1()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$id=2;
			$sql="CALL sp_sel_lead($id)";
			$parameter=array();
			return $this->db->query($sql,$parameter);
		}
		
	}													