<?php
	class Help_model extends CI_Model
	{
		public function __construct() 
		{
			parent::__construct();
            $timezone = new DateTimeZone("Asia/Kolkata" );
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
		}
		
		
		/*Affiliate Wise Submit Ticket */ 
		public function get_submit_ticket_detail()
		{
			$affid=$this->session->userdata["affid"];
			$condition='';
			$dept="".$this->input->post('dddept')."";
			$acc="".$this->input->post('txtaccname')."";
			$accid="".$this->input->post('txtaccid')."";
			$from="".$this->input->post('txtfrom')."";
			$to="".$this->input->post('txtto')."";
			if($this->input->post('dddept')!='')
			{
				$condition=$condition." `m46_submit_ticket`.`tkt_department`= ".$dept." and ";
			}
			if($this->input->post('txtaccname')!='')
			{
				$condition=$condition." `m46_submit_ticket`.`tkt_person_name` LIKE '%".$acc."%' and ";
			}
			if($this->input->post('txtaccid')!='')
			{
				$condition=$condition." `m33_account`.`account_user_id` LIKE '%".$accid."%' and ";
			}
			if($this->input->post('txtto')!='' && $this->input->post('txtfrom')!='')
			{
				$condition=$condition." date_format(`m46_submit_ticket`.`tkt_sub_date`,'%Y-%m-%d') between date_format('".$from."','%Y-%m-%d') and date_format('". $to ."','%Y-%m-%d') and";
			}
			$condition = $condition." affiliate_id=".$affid." and `m46_submit_ticket`.`emp_id`=0 GROUP BY tkt_ref_no order by tkt_id desc";
			$clsdt= array(
			'person_reg_id'=>$condition
			);
			$query = "CALL sp_view_ticket(?)";
			$data['rec']=$this->db->query($query,$clsdt);
			$this->db->free_db_resource();
			$clsdt1= array(
			'aff_id'=>$affid
			);
			$query1 = "CALL sp_find_desig(?)";
			$data['emp']=$this->db->query($query1,$clsdt1);

			$this->db->free_db_resource();
			return $data;
		}
		
		/*Affiliate Wise Submit Ticket Assign Report*/   
        public function assign($st)
	    {
			$affid=$st;
			$condition='';	
if($this->session->userdata('user_type')==1)
			$condition = $condition." affiliate_id=".$affid." and emp_id=". $this->session->userdata('profile_id') ."";
if($this->session->userdata('user_type')==2)
			$condition = $condition." affiliate_id=".$affid." and emp_id!=0";
			$condition = $condition." and tkt_status=1";
			$condition = $condition." GROUP BY tkt_ref_no order by tkt_id desc";
			$clsdt= array(
			'querey'=>$condition
			);
			$query = "CALL sp_view_ticket(?)";
			$data['rec']=$this->db->query($query,$clsdt);
			$this->db->free_db_resource();
			return $data;
		}
		
		
		public function task_assign()
		{
			$tic_no=$this->input->post('hd');
			$data=array(
			'emp_id'=>$this->input->post('ddemp_name'),
			'tic_no'=>$this->input->post('hd'),
			'assign_time'=>$this->curr,
			'id'=>$this->input->post('old_assign_id')
			);
			$query = "CALL sp_assign_ticket(?" . str_repeat(",?", count($data)-1) .",@a)";
			$data['rec']=$this->db->query($query,$data);
			$this->db->free_db_resource();    
			$data['response']=$this->db->query("SELECT @a as resp");
			foreach($data['response']->result() as $rows)
			{
			break;
			}
           //echo $rows->resp;
			/*Write Mail code here*/
			/*$config['mailtype'] = 'html';
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL,'Support Techaarjavam'); // change it to yours
			$this->email->to($email);// change it to yours
			$this->email->subject('Ticket Assign');
			$this->email->message('<p>Dear '.$name.'<br><br>This is to inform, that your coordination is required in resolving the Ticket ( #'.$tic_no.' ) assigned to you. Kindly follow the link below for further proceedings.</p>');
			if($this->email->send())
			{
				echo 'true';  
			}
			else
			{
				show_error($this->email->print_debugger());
			}*/
		    return $rows->resp;
		}
		
		
		public function aff_emp($id)
		{
			$affid=$id;
			$clsdt1= array(
            'aff_id'=>$affid
			);
			$query1 = "CALL sp_find_desig(?)";
			$data['emp']=$this->db->query($query1,$clsdt1);
			$this->db->free_db_resource();
			return $data;
		}
		
		public function ticket_search_report()
		{
			$affid=$this->session->userdata["affid"];
			$curr="".$this->curr."";		
			$duration=$this->input->post('ddduration');
			$empid="".$this->input->post('txtempid')."";
			$empname="".$this->input->post('txtempname')."";
			$dept="".$this->input->post('dddept')."";
		    //$st="".$this->input->post('ddstatus')."";
			$condition='';
			if($this->input->post('dddept')!='' && $this->input->post('dddept')!=-1)
			{
				$condition=$condition." `m46_submit_ticket`.`tkt_department`= ".$dept." and ";
			}
			if($this->input->post('txtempid')!='')
			{
				$condition=$condition." `m06_user_detail`.`or_m_user_id` LIKE '%".$empid."%' and ";
			}
			if($this->input->post('txtempname')!='')
			{
				$condition=$condition." `m06_user_detail`.`or_m_name` LIKE '%".$empname."%' and ";
			}
			if($this->input->post('ddstatus')!=-1 && $this->input->post('ddstatus')!="")
			{
				echo $this->input->post('ddstatus').'/'.$condition=$condition." `m46_submit_ticket`.`tkt_status` =".$this->input->post('ddstatus')." and  ";
			}
			if($this->input->post('ddstatus')!=-1 && $this->input->post('ddstatus')!="" && $this->input->post('ddduration')!=-1 && $this->input->post('ddduration')!='')
			{
				if($this->input->post('ddduration')==1)
				{
					$condition=$condition."DATEDIFF('".$curr."',date_format(tkt_sub_date,'%Y-%m-%d')) BETWEEN 1 AND 3 AND tkt_status=".$this->input->post('ddstatus')." and ";
				}
				if($this->input->post('ddduration')==2)
				{
					$condition=$condition."DATEDIFF('".$curr."',date_format(tkt_sub_date,'%Y-%m-%d')) BETWEEN 4 AND 7 AND tkt_status=".$this->input->post('ddstatus')." and ";
				}
				if($this->input->post('ddduration')==3)
				{
					$condition=$condition."DATEDIFF('".$curr."',date_format(tkt_sub_date,'%Y-%m-%d')) BETWEEN 8 AND 15 AND tkt_status=".$this->input->post('ddstatus')." and ";
				}
				if($this->input->post('ddduration')==4)
				{
					$condition=$condition."DATEDIFF('".$curr."',date_format(tkt_sub_date,'%Y-%m-%d')) > 15 AND tkt_status=".$this->input->post('ddstatus')." and ";
				}
			}
				if($this->session->userdata('user_type')==1)
					$condition = $condition." affiliate_id=".$affid." and emp_id=". $this->session->userdata('profile_id') ." and ";
			if($this->session->userdata('user_type')==2)
					$condition = $condition." affiliate_id=".$affid." and ";
			$condition=$condition." `m46_submit_ticket`.`emp_id`!= 0 ORDER BY `m46_submit_ticket`.`tkt_id` DESC";
			
			$cls=array(
			'querey'=>$condition
			);
			$query="CALL sp_view_ticket(?)";
			$data['ticket']=$this->db->query($query,$cls);
			$this->db->free_db_resource();
			return $data;
		}
		
		public function ticket_search()
		{ 
			$affid=$this->session->userdata["affid"];
			$condition='';
			$dept="".$this->input->post('dddept')."";
			$emp="".$this->input->post('txtempname')."";
			$empid="".$this->input->post('txtempid')."";
			$acc="".$this->input->post('txtaccname')."";
			$accid="".$this->input->post('txtaccid')."";
			$from="".$this->input->post('txtfrom')."";
			$to="".$this->input->post('txtto')."";
			$st="".$this->input->post('ddstatus')."";
			$data['user']=$this->db->get('m06_user_detail');
			if($this->input->post('dddept')!='')
			{
				$condition=$condition." `m46_submit_ticket`.`tkt_department`= ".$dept." and ";
			}
			if($this->input->post('txtempname')!='')
			{
				$condition=$condition." `m06_user_detail`.`or_m_name` LIKE '%".$emp."%' and ";
			}
			if($this->input->post('txtempid')!='')
			{
				$condition=$condition." `m06_user_detail`.`or_m_user_id` LIKE '%".$empid."%' and ";
			}
			if($this->input->post('txtaccname')!='')
			{
				$condition=$condition." `m46_submit_ticket`.`tkt_person_name` LIKE '%".$acc."%' and ";
			}
			if($this->input->post('txtaccid')!='')
			{
				$condition=$condition." `m33_account`.`account_user_id` LIKE '%".$accid."%' and ";
			}
			if($this->input->post('txtto')!='' && $this->input->post('txtfrom')!='')
			{
				$condition=$condition." date_format(`m46_submit_ticket`.`tkt_sub_date`,'%Y-%m-%d') between date_format('".$from."','%Y-%m-%d') and date_format('". $to ."','%Y-%m-%d') and";
			}
			if($this->input->post('ddstatus')!='' && $this->input->post('ddstatus')!=2 )
			{
				$condition=$condition." `m46_submit_ticket`.`tkt_status` =".$st. " and `m46_submit_ticket`.`emp_id` !=0 and ";
			}
			if($this->input->post('ddstatus')!='' && $this->input->post('ddstatus')==2 )
			{
				$condition=$condition." `m46_submit_ticket`.`emp_id` =0 and ";
			}
			if($this->session->userdata('user_type')==1)
					$condition = $condition." affiliate_id=".$affid." and emp_id=". $this->session->userdata('profile_id') ." and ";
			if($this->session->userdata('user_type')==2)
					$condition = $condition." affiliate_id=".$affid." and ";
			$condition=$condition. " 1 ";
			$cls=array(
			'querey'=>$condition
			);
			$query="CALL sp_view_ticket(?)";
			$data['ticket']=$this->db->query($query,$cls);
			$this->db->free_db_resource();
		    return $data;
		}
		
		
		/** View Ticket**/
		
		public function view_ticket()
		{
			$p_id=$this->uri->segment(3);
			$condition='';
			$condition=$condition."m46_submit_ticket.tkt_ref_no='$p_id'";
			$clsdt1= array(
			'querey'=>$condition
			);
			$query1 = "CALL sp_view_ticket(?)";
			$data['rec']=$this->db->query($query1,$clsdt1);
			$this->db->free_db_resource();
			$this->db->where('tkt_ref_no',$p_id);
			$data['ticket']=$this->db->get('m46_submit_ticket');
			foreach($data['ticket']->result() as $row)
			{
				$id=$row->tkt_id;	
			}
			$condition = "tr24_ticket_transaction.tkt_id='$id'";
			$clsdt= array(
			'querey'=>$condition
			);
			$query = "CALL sp_view_ticket_reply(?)";
			$data['rec1']=$this->db->query($query,$clsdt);
			$this->db->free_db_resource();
			return $data;
		}
		
		
		public function response()
		{
	        $tic_no=$this->input->post('txtticket');
			$txtsubject=$this->input->post('txtsubject');
			$description=$this->input->post('txtdiscription');
			$res=$this->input->post('txtemail');
            $ticket_no=$this->input->post('txtticket');
           
			$data['rec']=$this->db->query("SELECT tkt_email,tkt_person_name FROM m46_submit_ticket WHERE tkt_ref_no='$ticket_no' GROUP BY tkt_email");
			
            foreach($data['rec']->result() as $rec)
			{
				$tic_owner=$rec->tkt_email;
                $txtname=$rec->tkt_person_name;
				break;
			}
			$config['upload_path']   =   "application/uploadimage/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "1907";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$tic=$this->input->post('txtticket');
			$fileupload=$finfo['raw_name'].$finfo['file_ext'];
			if($fileupload=="")
			{
				$fi="";
			}
			else
			{
				$fi=$fileupload;
			}
			$data=array(
			'proc'=>2,'ticket_no'=>$this->input->post('txtticket'),'tkt_person_name'=>'','tkt_email'=>'','tkt_department'=>'','emp_id'=>'','tkt_subject'=>'','tkt_urgency'=>'','tkt_discription'=>'','tkt_response_type'=>1,'response_by'=>$this->session->userdata('name'),'tkt_userfile'=>$fi,'tkt_status'=>1,'account_id'=>'','affiliate_id'=>'','tkt_sub_date'=> $this->curr,'trans_description'=>$this->input->post('txtdiscription'),'trans_response_date'=>$this->curr,'trans_status'=>1
			);
			$query = "CALL sp_ticket_submission(?" . str_repeat(",?", count($data)-1) .",@a)";
			$data['rec']=$this->db->query($query,$data);
			$this->db->free_db_resource();
			$data['response']=$this->db->query("SELECT @a as resp");
			foreach($data['response']->result() as $rows)
			{
			break;
			}
            $ip=$_SERVER['REMOTE_ADDR'];
			$message = '<p>Dear: '.$txtname.',<br><br>
			This is a notification to let you know that we have responded back to your query with reference to the ticket no #'.$this->input->post('txtticket').'. 
			Please re-open the ticket and respond accordingly.<br><br>
			Message: '. $description.'<br><br>
			Priority: High<br><br>
			Status: Open<br><br>
			Your IP: '. $ip.'<br><br>
			For checking your Ticket- '.base_url().'index.php/direct_ticket/view_ticket/'.$ticket_no.'</p>';
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL,'Support Techaarjavam'); // change it to yours
			$this->email->to($tic_owner);// change it to yours
            $this->email->bcc('techaarjavam@gmail.com');
			$this->email->subject('Techaarjavam Support Ticket[ID:'.$ticket_no.']');
			$this->email->message($message);
			if($this->email->send())
			{
				echo 'true';  
			}
			else
			{
				show_error($this->email->print_debugger());
			}
			//return $rows->resp;
		}
		
		/*Closed Ticket Here*/
		
		public function resolve()
		{
            $ticket_no=$this->input->post('ticket_no');		
			$data['rec']=$this->db->query("SELECT tkt_email FROM m46_submit_ticket WHERE tkt_ref_no='$ticket_no' GROUP BY tkt_email");
			foreach($data['rec']->result() as $rec)
			{
				$tic_owner=$rec->tkt_email;
				break;
			}
			$data=array(       
			'proc'=>3,'ticket_no'=>$ticket_no,'tkt_person_name'=>'','tkt_email'=>'','tkt_department'=>'','emp_id'=>'','tkt_subject'=>'','tkt_urgency'=>'','tkt_discription'=>'','tkt_response_type'=>1,'response_by'=>'','tkt_userfile'=>'','tkt_status'=>0,'account_id'=>'','affiliate_id'=>'','tkt_sub_date'=> $this->curr,'trans_description'=>'','trans_response_date'=>'','trans_status'=>''
			);
			$query = "CALL sp_ticket_submission(?" . str_repeat(",?", count($data)-1) .",@a)";
			$data['rec']=$this->db->query($query,$data);
			$this->db->free_db_resource();
			$data['rec']=$this->db->query("SELECT @a as resp");
			foreach($data['rec']->result() as $rows)
			{
			break;
			}
			$ip=$_SERVER['REMOTE_ADDR'];
			$message = '<p>Dear: '.$tic_owner.',<br><br>
			This is in response of your attempt that your all query has been resolved now. So, we are considering it true and here by changing the status of your ticket(5464) to closed .<br><br> Thanking you,<br><br>
			Keep visiting us!<br><br>
			Priority: High<br><br>
			Status: Closed<br><br>
			Your IP: '. $ip.'<br><br>
			For checking your Ticket- '.base_url().'index.php/direct_ticket/view_ticket/'.$ticket_no.'</p>';
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL,'Support Techaarjavam'); // change it to yours
			$this->email->to($tic_owner);// change it to yours
			$this->email->subject('Techaarjavam Support Ticket[ID:'.$ticket_no.']');
			$this->email->message($message);
			if($this->email->send())
			{
				return $rows->resp;
			}
			else
			{
				show_error($this->email->print_debugger());
			}
			
		}
		
	}
?>