<?php
	class Employee_model extends CI_Model
	{
		public function __construct() 
		{
			parent::__construct();
            $timezone = new DateTimeZone("Asia/Kolkata" );
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
		}
		

//Select State here
		public function select_state($id)
		{
			$this->load->helper('url');
			$this->load->database();
			if($id!="" && $id!=0)
			{
				$this->db->where('m_parent_id',$id);
				return $this->db->get('m05_location');
			}
			else
			{
				//$this->db->where('m_parent_id',1);
				return $this->db->get('m05_location');
			}
		}
		//Select City here
		public function select_city($id)
		{
			$this->load->helper('url');
			$this->load->database();
			if($id!="" && $id!=0)
			{
				$this->db->where('m_loc_id',$id);
				return $this->db->get('m05_location');
			}
		}

		//Start Employee Module
		public function emp()
		{
			$this->db->where('m_des_pat_id',0);
			$data['desig']=$this->db->get('m03_designation');
			$this->db->where('quali_status',1);
			$data['quali']=$this->db->get('m27_qualification');
			$this->db->where('m_bank_status',1);
			$data['bank']=$this->db->get('m04_bank');
			$this->db->where('m_parent_id',1);
			$data['loc']=$this->db->get('m05_location');
			$this->db->where('or_m_reg_id',$this->uri->segment(3));
			$data['info']=$this->db->get('m06_user_detail');
			$this->db->where('or_m_id',$this->uri->segment(3));
			$data['bank_detail']=$this->db->get('m07_user_bank');
			$this->db->where('emp_user_id',$this->uri->segment(3));
			$data['specifi']=$this->db->get('m36_specification');
			$data['spcl']=$this->db->get('m06_user_detail');
			$this->db->where('m_shift_status',1);
		    $data['shift']=$this->db->get('m54_shift');
			return $data;
		}
		
		public function index()
		{
            $affid=$this->session->userdata('affid');
			$query ="CALL sp_employee(3,'0','0','0',0,0,0,'0000-00-00','0000-00-00','0','0','0','0','0','0','0',$affid,'0','0','0','0','0','0','0','0','0','','0','0',@i)";
			$data['rec']=$this->db->query($query);
			return $data;
		}
		
		
		public function insert_employee()
		{
			$pwd=random_string('numeric',4);
			if($this->input->post('dddesignation')=='11')
			{
				$spcl="";
				$workexp="";
				$qualific="";
			}
			else
			{
				$spcl=$this->input->post('ddspecialization');
				$workexp=$this->input->post('ddworkexp');
				$qualific=$this->input->post('ddqualification');
			}
			$data=array(
			'proc'=>'1',
			'or_m_user_id'=>$this->input->post('ddshift_type'),/*Send Shift Type due to this column data not used in sp*/
			'or_m_designation'=>$this->input->post('dddesignation'),
			'or_m_name'=>$this->input->post('txtemp_name'),
			'or_m_dob'=>date('Y-m-d',strtotime($this->input->post('txtdob'))),
			'or_member_joining_date'=>$this->curr,
			'or_m_gender'=>$this->input->post('ddgender'),
			'or_m_mobile_no'=>$this->input->post('txtcontact'),
			'or_m_email'=>$this->input->post('txtmail'),
			'or_m_address'=>$this->input->post('txtaddress'),
			'or_m_city'=>$this->input->post('ddcity'),
			'or_m_state'=>$this->input->post('ddstate'),
			'or_m_country'=>'1',
			'or_m_pincode'=>$this->input->post('txtpincode'),
			'or_m_intr_id'=>$this->session->userdata('profile_id'),
			'or_m_intr_name'=>$this->session->userdata('name'),
			'or_m_aff_id'=>$this->session->userdata('affid'),
			'or_m_regdate'=>$this->curr,
			'emp_spcl'=>$spcl,
			'emp_exp'=>$workexp,
			'emp_quali'=>$qualific,
			'or_m_b_name'=>trim($this->input->post('txtacc_name')),
			'or_m_b_cbsacno'=>trim($this->input->post('txtacc_num')), 
			'or_m_bname'=>trim($this->input->post('ddbank_name')), 
			'or_m_b_branch'=>trim($this->input->post('txtbank_branch')),
			'or_m_b_ifscode'=>trim($this->input->post('txtbank')),
			'or_m_b_imps'=>trim($this->input->post('txtmmid')),
			'or_login_pwd'=>trim($pwd),
			'or_pin_pwd'=>trim($this->input->post('ddweekoff'))/*Send Week off due to this column data not used in sp*/
			);
			//var_dump($data);
			$query ="CALL sp_employee(?" . str_repeat(",?", count($data)-1) . ",@i) ";
			$data['cl_dt']=$this->db->query($query,$data);	
			$this->db->free_db_resource();
			$query1 = "select @i as id";
			$data['out'] =$this->db->query($query1);
			//print_r($data['out']);
			foreach($data['out']->result() as $rows)
			{
			}
		    $ids =$rows->id;
			$did=$this->input->post('dddesignation');
			$data['designa']=$this->db->query("(Select GetDesig('$did') as desig)");
			$data['Specilization']=$this->db->query("(Select GetDesig('$spcl') as spcl)");
			foreach($data['designa']->result() as $desr)
			{
				break;
			}
			foreach($data['Specilization']->result() as $spc)
			{
				break;
			}
			$message="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
			<html xmlns='http://www.w3.org/1999/xhtml'>
			<head>
			<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
			</head>
			
			<body style='margin: 0px auto; padding:0px;'>
			<div style='width: 790px; background-color: #FFF; margin: 0px auto; padding:0px; font-size:17px;'>
			<!--header start--> 
			<!--content1 start-->
			<div style='width: 100%;  background-color: #FCFCFC; MARGIN: 0 AUTO; HEIGHT:137PX;'>
			<div style='float:left;width:100%; MARGIN: 0 AUTO;'><img src='".base_url()."application/libraries/assets/layouts/layout/img/company_logo_1582267357-1.png' style='width: 150px; height: 101px; margin-top: 25px;	padding-left: 1px; '></div>
			<div style='float:RIGHT; width:80%;'>

			</div>
			<!--content2 start-->
			<div style='clear:both;'></div>
			<hr / style='    border: 1px solid #D56767;'>
			<div style='width:90%; margin:0 auto;'>
			</div>
			<!--content3 Close--> 
			</div>
			<br />
			<div style='clear:both;'></div>
			<br />
			<br />
			
			<!--content Start-->
			<div style='width: 100%;'>
			<div style='font-size:22px;color:#D56767;' align='center'><strong>WELCOME</strong></div>
			<!--content photo-->
			<div style='width: 90%; margin:0 auto;'>
			<div style='float:left;  font-size:17px;'> Dear ".$this->input->post('txtemp_name').", </div>
			</div>
			<!--content photo Close-->
			<div style='clear:both;'></div>
			<br />
			<div style='width: 90%; margin:0 auto;'>
			<div style='float:left;  font-size:17px;    text-align: justify;'> Your login credentials are made on Abhay Techsolutions LLP's sales tracking portal.			</div>
			<div style='clear:both;'></div>
			<div style=' width: 100%;
			margin: 0 auto;
			float: left;'>
			</div>
			<br />
			<div style='float:left;  font-size:17px; text-align: justify;'>
			Email Id: ".$this->input->post('txtmail')."<br>
			Password: ".trim($pwd)."<br>
			URL: <a href='http://innovativetech.tech/sales/CRM'>Click Here To Login</a><br>
			</div>
			<div style='clear:both;'></div>
			<br />
			<div style='float:left;  font-size:17px;    text-align: justify;'>Should you need any further assistance please feel free to speak to your Manager.</div>
			<br />
			<div style='float:left;  font-size:17px;    text-align: justify;'>Wish you luck!</div>
			</div>
			<br /><br />
			<div style='clear:both;'></div>
			<div style='clear:both;'></div>
			<div style='width: 90%; margin:0 auto;'>
			<div style='float:left;  font-size:17px;'> Best Regards,
			<br />
			Team Abhay TechSolutions LLP</strong> </div>
			</div>
			<!--FOOTER-->
			<div style='clear:both;'></div>
			<br />
			<hr / style='    border: 1px solid #D56767;'>
			</div>
			</body>
			</html>";
			
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL,'ADMINISTRATION ABHAY TECHSOLUTIONS LLP');			 // change it to yours
			$this->email->to($this->input->post('txtmail'));                         // change it to yours
			$this->email->subject('EMPLOYEE REGISTRATION OF ABHAY TECHSOLUTIONS LLP');
			$this->email->message($message);
			$this->email->send();
	        return $ids;  
		}
		
		public function uploadfile()
		{
			$config['upload_path']   =   "application/emp_pics/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "5000";
			$config['max_height']    =   "5000";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$error = $this->upload->display_errors();
			if($error!="")
			{
				return $error;
			}
			else
			{
				$fileupload=($finfo['raw_name'].$finfo['file_ext']);
				return $fileupload;
			}
		}
		
		
		public function insert_profile_pic()		
		{
			$id= $this->input->post('txtpro_id'); 
			$pic = $this->input->post('file1');
			$data = array(
			'or_m_userimage'=>$pic
			); 
			$this->db->where('or_m_reg_id',$id);
			$this->db->update('m06_user_detail',$data);
			return 1;
			
		}
		
		
		public function uploadfile1()
		{
			$config['upload_path']   =   "application/signature_images/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png"; 
			// $config['allowed_types'] =   "pdf|doc|docx|xlsx|txt"; 
			$config['max_size']      =   "5000";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$error = $this->upload->display_errors();
			if($error!="")
			{
				return $error;
			}
			else
			{
				$fileupload=($finfo['raw_name'].$finfo['file_ext']);
				return $fileupload;
			}
		}
		
		public function insert_sign_pic()		
		{			  
			$id= $this->input->post('txtpro_id'); 
			$pic = $this->input->post('file1');
			$data = array(
			'or_m_usersign'=>$pic
			); 
			$this->db->where('or_m_reg_id',$id);
			$this->db->update('m06_user_detail',$data);
			return 1;
			
		}
		
		
		public function view_edit_employee()
		{
		    $data=$this->emp();
			$data['id']=$this->uri->segment(3);
			$id = $this->uri->segment(3);
            $affid=$this->session->userdata('affid');
			$query ="CALL sp_employee(3,'0','$id','0',0,0,0,'0000-00-00','0000-00-00','0','0','0','0','0','0','0',$affid,'0','0','0','0','0','0','0','0','0','','0','0',@i)";
			$data['resul']=$this->db->query($query);
			return $data;
		}
		
		
		public function update_employee()
		{
			if($this->input->post('dddesignation')=='11')
			{
				$spcl="";
				$workexp="";
				$qualific="";
			}
			else
			{
				$spcl=$this->input->post('ddspcl'); /// ddspcl replace ddspecialization
				$workexp=$this->input->post('ddworkexp');
				$qualific=$this->input->post('ddqualification');
			}
			$clsdt = array(
			'proc'=>'2',
			'or_m_user_id'=>$this->input->post('txtemp_code'),
			'or_m_designation'=>$this->input->post('dddesignation'),
			'or_m_name'=>$this->input->post('txtemp_name'),
			'or_m_dob'=>date('Y-m-d',strtotime($this->input->post('txtdob'))),
			'or_member_joining_date'=>$this->curr,
			'or_m_gender'=>$this->input->post('ddgender'),
			'or_m_mobile_no'=>$this->input->post('txtcontact'),
			'or_m_email'=>$this->input->post('txtmail'),
			'or_m_address'=>$this->input->post('txtaddress'),
			'or_m_city'=>$this->input->post('ddcity'),
			'or_m_state'=>$this->input->post('ddstate'),
			'or_m_country'=>'1',
			'or_m_pincode'=>$this->input->post('txtpincode'),
			'or_m_intr_id'=>$this->input->post('txtid'),/*User Registration Id*/
			'or_m_intr_name'=>0,
			'or_m_aff_id'=>0,
			'or_m_regdate'=>$this->curr,
			'emp_spcl'=>$spcl,
			'emp_exp'=>$workexp,
			'emp_quali'=>$qualific,
			'or_m_b_name'=>trim($this->input->post('txtacc_name')),
			'or_m_b_cbsacno'=>trim($this->input->post('txtacc_num')), 
			'or_m_bname'=>trim($this->input->post('ddbank_name')), 
			'or_m_b_branch'=>trim($this->input->post('txtbank_branch')),
			'or_m_b_ifscode'=>trim($this->input->post('txtbank')),
			'or_m_b_imps'=>trim($this->input->post('txtmmid')),
			'or_login_pwd'=>$this->input->post('ddshift_type'),
			'or_pin_pwd'=>trim($this->input->post('ddweekoff'))
			);
			//var_dump($clsdt);
			$query ="CALL sp_employee(?" . str_repeat(",?", count($clsdt)-1) . ",@i) ";
			$data['cl_dt']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			$query1 = "select @i as id";
			$data['out'] =$this->db->query($query1);
			foreach($data['out']->result() as $rows)
			{
			}
			$ids =$rows->id;
			return $ids;
		}
		
		
		public function view_employee_report()
		{	
			$todate='0000-00-00';$fromdate='0000-00-00';$empcode=0;$empregid=0;$empstate=0;$empcity=0;$empdesig=0;$empspecification=0;
			$data['todate']='';$data['fromdate']='';$data['empcode']=0;$data['empregid']=0;$data['empstate']=0;$data['empcity']=0;$data['empdesig']=0;$data['empspecification']=0;
			if($this->input->post('txtfromdate')!="" && $this->input->post('txtfromdate')!="0")
			{
				$fromdate=$this->input->post('txtfromdate');
				$data['fromdate']=$fromdate;
			}
			if($this->input->post('txttodate')!="" && $this->input->post('txttodate')!="0")
			{
				$todate=$this->input->post('txttodate');
				$data['todate']=$todate;
			}
			if($this->input->post('txtempcode')!="" && $this->input->post('txtempcode')!="0")
			{
				$empcode=$this->input->post('txtempcode');
				$data['empcode']=$empcode;
			}
			if($this->input->post('ddname')!="" && $this->input->post('ddname')!="-1")
			{
				$empregid=$this->input->post('ddname');
				$data['empregid']=$empregid;
			}
			if($this->input->post('ddstate')!="" && $this->input->post('ddstate')!="-1")
			{
				$empstate=$this->input->post('ddstate');
				$data['empstate']=$empstate;
			}
			if($this->input->post('ddcity')!="-1" && $this->input->post('ddcity')!="")
			{
				$empcity=$this->input->post('ddcity');
				$data['empcity']=$empcity;
			}
			if($this->input->post('dddesignation')!="-1" && $this->input->post('dddesignation')!="")
			{
				$empdesig=$this->input->post('dddesignation');
				$data['empdesig']=$empdesig;
			}
			if($this->input->post('ddspcl')!="-1" && $this->input->post('ddspcl')!="")
			{
				$empspecification=$this->input->post('ddspcl');
				$data['empspecification']=$empspecification;
			}
		    $affid=$this->session->userdata('affid');
			$query ="CALL sp_employee(3,'$empcode','$empregid','$empstate',$empcity,$empdesig,$empspecification,'$fromdate','$todate','0','0','0','0','0','0','0',$affid,'0','0','0','0','0','0','0','0','0','','0','0',@i) ";
			$data['rec']=$this->db->query($query);
			$this->db->free_db_resource();
            $this->db->where('or_m_aff_id',$this->session->userdata('affid'));
			$this->db->where('or_m_status',1);
			$data['user']=$this->db->get('m06_user_detail');
			$this->db->where('m_parent_id',1);
			$data['loc']=$this->db->get('m05_location');
			$this->db->where('m_des_pat_id',0);
			$data['desig']=$this->db->get('m03_designation');
			return $data;
		}
		
		
		public function view_employee_attendence_report()
		{	
			$empcode=0;$empregid=0;$empdesig=0;$empspecification=0;$year=date('Y');$month=date('m');
			$data['empcode']=0;$data['empregid']=0;$data['empdesig']=0;$data['empspecification']=0;$data['month']=$month;$data['year']=$year;
			if($this->input->post('txtempcode')!="" && $this->input->post('txtempcode')!="0")
			{
				$empcode=$this->input->post('txtempcode');
				$data['empcode']=$empcode;
			}
			if($this->input->post('ddname')!="" && $this->input->post('ddname')!="-1")
			{
				$empregid=$this->input->post('ddname');
				$data['empregid']=$empregid;
			}
			
			if($this->input->post('dddesignation')!="-1" && $this->input->post('dddesignation')!="")
			{
				$empdesig=$this->input->post('dddesignation');
				$data['empdesig']=$empdesig;
			}
			if($this->input->post('ddspcl')!="-1" && $this->input->post('ddspcl')!="")
			{
				$empspecification=$this->input->post('ddspcl');
				$data['empspecification']=$empspecification;
			}
			if($this->input->post('ddyear')!="-1" && $this->input->post('ddyear')!="")
			{
				$year=$this->input->post('ddyear');
				$data['year']=$year;
			}
			if($this->input->post('ddmonth')!="-1" && $this->input->post('ddmonth')!="")
			{
				$month=$this->input->post('ddmonth');
				$data['month']=$month;
			}
            $affid=$this->session->userdata('affid');
			$query ="CALL sp_employee_attendance(1,$affid,'$empcode','$empregid','$empdesig','$empspecification','$year','$month',@i) ";
			$data['rec']=$this->db->query($query);
			$this->db->free_db_resource();
			if($month==date('m'))
			{
				$data['lastday']=date('d');
			}
			else
			{
			foreach($data['rec']->result() as $row)
			{
				$data['lastday']=$row->Total_Day;
			}
			}
			$this->db->free_db_resource();
			$this->db->where('m_leave_status',1);
			$data['leave']=$this->db->get('m53_leave');
			$this->db->free_db_resource();
			$this->db->where('or_m_aff_id',$this->session->userdata('affid'));
			$this->db->where('or_m_status',1);
			$data['user']=$this->db->get('m06_user_detail');
			$this->db->where('m_parent_id',1);
			$data['loc']=$this->db->get('m05_location');
			$this->db->where('m_des_pat_id',0);
			$data['desig']=$this->db->get('m03_designation');
			return $data;
		}
		
		
		public function employee_attendance()
		{
			$empregid=$this->uri->segment(3);
			$year=$this->uri->segment(4);
			$month=$this->uri->segment(5);
			$empcode=$this->uri->segment(6);
            $affid=$this->session->userdata('affid');
			$query ="CALL sp_employee_attendance(2,$affid,'$empcode','$empregid','','','$year','$month',@i) ";
			$data['full_att']=$this->db->query($query);
			return $data;
			
		}
		
		
		public function view_upload_doccument()
	    {
			$data['doc']=$this->db->query("SELECT * FROM `m57_document_type` WHERE `m_type_id` NOT IN(SELECT `m56_documents`.`m_doc_type` FROM `m56_documents` WHERE `m56_documents`.`m_doc_uid`=".$this->uri->segment(3).")");
			$this->db->free_db_resource();
			$id=$this->uri->segment(3);
			$data['id']=$this->uri->segment(3);
			$query ="CALL sp_employee(4,'`m06_user_detail`.`or_m_reg_id`=$id','','','','','','','','','','','','','','','','','','','','','','','','','','','',@i) ";
			$data['rec']=$this->db->query($query);
			return $data;
		}
		
		
		public function upload_document()		
		{
			$config['upload_path']   =   "application/user_document/";
			$config['allowed_types'] =   "pdf|doc|docx|xlsx|txt|gif|jpg|jpeg|png"; 
			$config['max_size']      =   "5000";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo = $this->upload->data();
			$error = $this->upload->display_errors();
			if($error!="")
			{
				return $error;
			}
			else
			{
				$fileupload=($finfo['raw_name'].$finfo['file_ext']);
				return $fileupload;
			}
		}
		
		
		public function insert_uploaded_Document()		
		{
			$data = array(
			'm_doc_uid'=>$this->input->post('txtuserid'),
			'm_doc_upload'=>$this->input->post('txtpicname'),
			'm_doc_type'=>$this->input->post('ddtype'),
			'm_doc_affid'=>$this->session->userdata('profile_id'),
			'm_doc_status'=>1,
			'm_doc_date'=>$this->curr
			);
			$this->db->insert('m56_documents',$data);
			return 1;
		}
		
		
		public function view_upload_profile_image()
	    {
			$data['doc']=$this->db->query("SELECT * FROM `m57_document_type` WHERE `m_type_id` NOT IN(SELECT `m56_documents`.`m_doc_type` FROM `m56_documents` WHERE `m56_documents`.`m_doc_uid`=".$this->uri->segment(3).")");
			$this->db->free_db_resource();
			$id=$this->uri->segment(3);
			$data['id']=$this->uri->segment(3);
			$query ="CALL sp_employee(5,'`m06_user_detail`.`or_m_reg_id`=$id','','','','','','','','','','','','','','','','','','','','','','','','','','','',@i) ";
			$data['rec']=$this->db->query($query);
			return $data;
		}
		
		
		public function upload_image()		
		{ 
		    if($this->uri->segment(3)==1)
		    {
				$config['upload_path']   =   "application/emp_pics/";
			}
			if($this->uri->segment(3)==2)
			{
				$config['upload_path']   =   "application/signature_images/";
			}
			$config['allowed_types'] =   "pdf|doc|docx|xlsx|txt|gif|jpg|jpeg|png"; 
			$config['max_size']      =   "5000";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo = $this->upload->data();
			$error = $this->upload->display_errors();
			if($error!="")
			{
				return $error;
			}
			else
			{
				$fileupload=($finfo['raw_name'].$finfo['file_ext']);
				return $fileupload;
			}
		}
		
		
		public function update_uploaded_image()		
		{
			if($this->input->post('ddtype')==1)
			{
				$data = array(
				'or_m_userimage'=>$this->input->post('txtpicname')
				);
				$this->db->where('or_m_reg_id',$this->input->post('txtuserid'));
				$this->db->update('m06_user_detail',$data);
				return 1;
			}
			if($this->input->post('ddtype')==2)
			{
				$data = array(
				'or_m_usersign'=>$this->input->post('txtpicname')
				);
				$this->db->where('or_m_reg_id',$this->input->post('txtuserid'));
				$this->db->update('m06_user_detail',$data);
				return 1;
			}
		}
		
		
		public function spcl()
		{
			$data['id']=$this->uri->segment(4);
			$id=$this->uri->segment(3);
			$this->db->where('m_des_pat_id',$id);
			$datas=$this->db->get('m03_designation');
			return $datas;
		}
		
		public function select_shift()
		{
			$shift_id = $this->uri->segment(3);
			$query = "Select * from m54_shift where m_shift_id = '$shift_id' and m_shift_status =1";
			$datas= $this->db->query($query);
			return $datas;
		}
		
		public function employee_holiday()
		{
			$clsdt= array(
			'proc'=>2,
			'holiday_id'=>$this->uri->segment(3),
			'affid'=>1
			);
			$query ="CALL sp_holiday_on_session_basis(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['holiday']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			return $data;
			
		}
		
		
		public function view_approve_leave()
		{
			$data['leave']=$this->db->query('SELECT * FROM `m53_leave` WHERE `m_leave_status`=1');
			return $data;
		}
		
		
		public function get_employee()
		{
		    $dates=$this->input->post('employee');
			$datas=$this->db->query("SELECT * FROM `m06_user_detail` WHERE `m06_user_detail`.`or_m_reg_id` NOT IN (SELECT `or_m_reg_id` FROM `m64_user_attendance`  WHERE `m64_user_attendance`.`atten_date`='$dates' )");
			return $datas;
		}
		
		
		public function approve_leave()
		{
			$year=explode('-',$this->input->post('txtddate'));
			$datas=array(
			'Leave_m_id'=>'',
			'M_reg_id'=>$this->input->post('ddemployee'),
			'Leave_year'=>$year[0],
			'Leave_date'=>$this->input->post('txtddate'),
			'Leave_id'=>$this->input->post('ddleavename'),
			'Taken_leave'=>1,
			'proc'=>1
			);
			$query="CALL sp_approve_leave(?" . str_repeat(",?", count($datas)-1) . ")";
			$this->db->query($query,$datas);
			return 'true';
		}
		
		public function load_leave_table()
		{
		    $emp_id=$this->input->post('emp_id');
			$txtddate=explode("-",$this->input->post('txtddate'));
			$dat11=$txtddate[0];
			$condition="`tr28_leave_management`.`or_m_reg_id`='$emp_id' AND `tr28_leave_management`.`tr_leave_year`='$dat11' ";
			$datas=array(
			'proc'=>1,
			'querryy'=>$condition
			);
			
			$query="CALL sp_leave_detail(?,?)";
			$data['load_leave']=$this->db->query($query,$datas);
			return $data;
		}
		
		
		public function load_leave_name()
		{
		    $emp_id=$this->input->post('emp_id');
			$txtddate=explode("-",$this->input->post('txtddate'));
			$dat11=$txtddate[0];
			$condition="`m53_leave`.`m_leave_max`!=COUNT(`tr28_leave_management`.`m_leave_id`) AND `tr28_leave_management`.`or_m_reg_id`='$emp_id' AND `tr28_leave_management`.`tr_leave_year`='$dat11' ";
			$datas=array(
			'proc'=>2,
			'querryy'=>$condition
			);
			$query="CALL sp_leave_detail(?,?)";
			$data=$this->db->query($query,$datas);
			return $data;
		}
		
		
		public function check_leave()
		{
		    $emp_id=$this->input->post('emp_id');
			$txtddate=$this->input->post('txtddate');
			$condition="`tr28_leave_management`.`or_m_reg_id`='$emp_id' AND `tr28_leave_management`.`tr_leave_date`='$txtddate' ";
			$datas=array(
			'proc'=>1,
			'querryy'=>$condition
			);
			$query="CALL sp_leave_detail(?,?)";
			$data=$this->db->query($query,$datas);
			return $data;
		}
		
		
		public function employee_leave()
		{
			$clsdt= array(
			'proc'=>2,
			'leave_id'=>$this->uri->segment(3),
			'affid'=>0
			);
			$query ="CALL sp_view_leave(?" . str_repeat(",?", count($clsdt)-1) . ") ";
			$data['leave1']=$this->db->query($query,$clsdt);	
			$this->db->free_db_resource();
			return $data;
			
		}
	}
?>