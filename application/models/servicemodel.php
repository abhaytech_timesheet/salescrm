<?php
class servicemodel extends CI_Model
{
public function selectservice($stid)
{
$this->load->database();
$this->load->helper('url');
$id=$stid;
$t=0;
$this->db->where('m_servicetype_id',trim($id));
$this->db->where('m_service_status',1);
//$this->db->where('m_api_id',1);
$data['sid']=$this->db->get('m11_services');
foreach($data['sid']->result() as $row)
{
$t++;
break;
}
if($t!=0)
{
$this->db->where('m_service_id',$row->m_service_id);
$this->db->where('m_so_status',1);
$this->db->group_by('m_so_id');
return $this->db->get('m15_service_operator');
}
else
{
return 'NO SERVICE OPERATOR';
}
//$row = $query->row();
}
public function smsggroup_id($mid)
{
$this->load->database();
$this->load->helper('url');
$this->load->library('session');
$pr=explode(',',$mid);
$affid="";
$desid="";
for($t=0;count($pr)>$t;$t++)
{
if($t==1)
$affid=$pr[$t];
if($t==2)
$desid=$pr[$t];
}
$this->db->where('m_aff_id',$affid);
$this->db->where('m_desig_id',$desid);
$data['gid']=$this->db->get('tr08_group_desig');
foreach($data['gid']->result() as $row)
{
$did=$row->m_group_id;
return $did;
}
}

public function get_api_serid($gid,$op)
{
$this->load->database();
$this->load->helper('url');
$this->load->library('session');
//$opc="";
$id=$op;
$this->db->where('m_sp_code',trim($id));
//$this->db->where('m_sp_status',1);
$data['spame']=$this->db->get('m17_service_provider');
foreach($data['spame']->result() as $rowsp)
{
break;
}
if($rowsp->m_sp_status =='1')
{
$this->db->where('m_so_name',$op);
$this->db->where('m_gr_id',$gid);
$data['apiid']=$this->db->get('tr06_group_op');
foreach($data['apiid']->result() as $row1)
{
$opc=$row1->m_so_code.','.$row1->m_api_id.','.$row1->m_service_id;
return $opc;
}
}
else
{
$opc="0,0,0";
return $opc;
}
}

public function selectspname($stid)
{
$this->load->database();
$this->load->helper('url');
$id=$stid;
$this->db->where('m_sp_code',trim($id));
//$this->db->where('m_sp_status',1);
$data['spame']=$this->db->get('m17_service_provider');
foreach($data['spame']->result() as $row)
{
$opname=$row->m_sp_name;
return $opname;
}
}

public function selectserviceprovider($stid)
{
$this->load->database();
$this->load->helper('url');
$id=$stid;
$this->db->where('m_sp_type_id',trim($id));
$this->db->where('m_sp_status',1);
return $this->db->get('m17_service_provider');
//$row = $query->row();
}
public function selectgr_op($stid,$gid)
{
$this->load->database();
$this->load->helper('url');
$id=$stid;
$grid=$gid;
$this->db->where('m_service_id',trim($id));
$this->db->where('m_gr_id',$grid);
return $this->db->get('tr06_group_op');
}
public function select_service()
{
$this->load->database();
$this->load->helper('url');
//$this->db->where('m_s_type_id',trim($id));
return $this->db->get('m10_service_type');
//$row = $query->row();
}

public function fill_service()
{
$this->load->database();
$this->load->helper('url');
$this->db->where('m_service_status',1);
return $this->db->get('m10_service_type');
}

public function get_group_id()
{
$this->load->database();
$this->load->helper('url');
 $this->load->library('session');
$this->db->where('m_aff_id',$this->session->userdata('affid'));
$this->db->where('m_desig_id',$this->session->userdata('designation'));
$data['gid']=$this->db->get('tr08_group_desig');
foreach($data['gid']->result() as $row)
{
$did=$row->m_group_id;
return $did;
}
}
public function rechargerequest()
{
 $this->load->library('session');
 $this->load->database();
 $this->load->helper('url');
$this->load->helper('string');
$this->load->model('users_records');
 $mobno=trim($this->input->post('txtmobno'));
 $amt=trim($this->input->post('txtamt'));
 $op=trim($this->input->post('ddlapiid'));
 if($this->session->userdata('user_id')==$this->input->post('txtmemid'))
 {
$query=$this->db->get_where('m06_user_detail',array('or_m_user_id'=>trim($this->input->post('txtmemid'))));
 $row = $query->row();
 $timezone = new DateTimeZone("Asia/Kolkata" );
 $date = new DateTime();
 $date->setTimezone($timezone );
$ss=random_string('numeric',4);
$d=$date->format('YmdHis');
$d=$d.$ss;
 $oper=explode(',',$this->input->post('ddlapiid'));
 $rechargedata = array(
 'm_urd_request_id'=>$d,
 'm_urd_trans_id'=>$d,
 'm_urd_mid'=>trim($this->session->userdata('profile_id')),
 'm_urd_requ_type'=>3,
 'm_urd_stype_id'=>trim($this->input->post('servicetype')),
 'm_urd_s_id'=>trim($oper[0]),
 'm_urd_rech_number'=>trim($this->input->post('txtmobno')),
 'm_urd_circle_id'=>0,
 'm_urd_amount'=>trim($this->input->post('txtamt')),
 'm_urd_refre_id'=>$d,
 'm_urd_rech_mode'=>trim($this->input->post('servicemode')),
 'm_urd_apiid'=>trim($oper[1]),
 'm_urd_requestdate'=>$date->format( 'Y-m-d H-i-s' ),
 'm_urd_admindate'=>'',
 'm_urd_recharge_status'=>2
);
$this->db->insert('tr05_recharge_detail', $rechargedata);
if($this->session->userdata('profile_id')!=0)
{
$availamt=$this->users_records->user_avail_amt1($this->session->userdata('profile_id'));
}
$ledgerdata =array(
'm_u_id'=>trim($this->session->userdata('profile_id')),
'm_trans_id'=>$d,
'm_cramount'=>"",
'm_description'=>"Recharge Service Request.Mobile No-".'&nbsp;'.trim($this->input->post('txtmobno')).'&nbsp'."Amount of".'&nbsp'.trim($this->input->post('txtamt')),
'm_dramount'=>trim($this->input->post('txtamt')),
'm_transdate'=>$date->format( 'Y-m-d H-i-s' ),
'm_deduction'=>"",
'm_refrence_id'=>$d,
'm_ledger_type'=>3,
'm_current_balance'=>($availamt-trim($this->input->post('txtamt'))),
'm_datetime'=>$date->format( 'Y-m-d H-i-s' )
);
$this->db->insert('tr03_manage_ledger', $ledgerdata);
return $d;
}
else
{
return $d;
}
}
 
public function set_tid($tid,$rqd,$status)
{
$this->load->library('session');
$this->load->database();
$this->load->helper('url');
$timezone = new DateTimeZone("Asia/Kolkata" );
$date = new DateTime();
$date->setTimezone($timezone );
$trnasction_id=$tid;
$responsedata =array(
'm_urd_request_id'=>$trnasction_id,
'm_urd_admindate'=>$date->format( 'Y-m-d H-i-s' ),
'm_urd_recharge_status'=>$status
);
$this->db->where('m_urd_request_id',$rqd);
$this->db->where('m_urd_recharge_status',2);
$this->db->where('m_urd_mid',trim($this->session->userdata('profile_id')));
$this->db->where('m_urd_admindate',"0000-00-00 00:00:00	");
$this->db->update('tr05_recharge_detail', $responsedata);
} 
public function set_margin($tid,$rqd,$margin)
{
$this->load->library('session');
$this->load->database();
$this->load->helper('url');
$this->load->model('users_records');
$timezone = new DateTimeZone("Asia/Kolkata" );
$date = new DateTime();
$date->setTimezone($timezone );
$trnasction_id=$tid;
if($margin!=0)
{
if($this->session->userdata('profile_id')!=0)
{
$availamt=$this->users_records->user_avail_amt1($this->session->userdata('profile_id'));
}
$ledgerdata =array(
'm_u_id'=>trim($this->session->userdata('profile_id')),
'm_trans_id'=>$trnasction_id,
'm_cramount'=>$margin,
'm_description'=>"Recharge Service Request Failed.TID-".'&nbsp;'.$tid,
'm_dramount'=>'',
'm_transdate'=>$date->format( 'Y-m-d H-i-s' ),
'm_deduction'=>"",
'm_refrence_id'=>$rqd,
'm_ledger_type'=>4,
'm_current_balance'=>($availamt+$margin),
'm_datetime'=>$date->format( 'Y-m-d H-i-s' )
);
$this->db->insert('tr03_manage_ledger', $ledgerdata);
}
}
public function smsset_tid($tid,$rqd,$mid,$status)
{
$this->load->library('session');
$this->load->database();
$this->load->helper('url');
$timezone = new DateTimeZone("Asia/Kolkata" );
$date = new DateTime();
$date->setTimezone($timezone );
$trnasction_id=$tid;
$responsedata =array(
'm_urd_request_id'=>$trnasction_id,
'm_urd_admindate'=>$date->format( 'Y-m-d H-i-s' ),
'm_urd_recharge_status'=>$status
);
$this->db->where('m_urd_request_id',$rqd);
$this->db->where('m_urd_recharge_status',2);
$this->db->where('m_urd_mid',trim($mid));
$this->db->where('m_urd_admindate',"0000-00-00 00:00:00	");
$this->db->update('tr05_recharge_detail', $responsedata);
} 
public function smsset_margin($tid,$rqd,$margin,$mid)
{
$this->load->library('session');
$this->load->database();
$this->load->helper('url');
$this->load->model('users_records');
$timezone = new DateTimeZone("Asia/Kolkata" );
$date = new DateTime();
$date->setTimezone($timezone );
$trnasction_id=$tid;
if($margin!=0)
{
if($mid!=0)
{
$availamt=$this->users_records->user_avail_amt1($mid);
}
$ledgerdata =array(
'm_u_id'=>$mid,
'm_trans_id'=>$trnasction_id,
'm_cramount'=>$margin,
'm_description'=>"Recharge Service RequestBy SMS Failed.TID-".'&nbsp;'.$tid,
'm_dramount'=>'',
'm_transdate'=>$date->format( 'Y-m-d H-i-s' ),
'm_deduction'=>"",
'm_refrence_id'=>$rqd,
'm_ledger_type'=>4,
'm_current_balance'=>($availamt+$margin),
'm_datetime'=>$date->format( 'Y-m-d H-i-s' )
);
$this->db->insert('tr03_manage_ledger', $ledgerdata);
}
}


public function set_refundtid($tid,$rqd,$status,$mid)
{
$this->load->library('session');
$this->load->database();
$this->load->helper('url');
$timezone = new DateTimeZone("Asia/Kolkata" );
$date = new DateTime();
$date->setTimezone($timezone );
$trnasction_id=$tid;
$id=$mid;
$sta=$status;
$responsedata =array(
'm_urd_request_id'=>$trnasction_id,
'm_urd_admindate'=>$date->format( 'Y-m-d H-i-s' ),
'm_urd_recharge_status'=>$sta
);
$this->db->where('m_urd_trans_id',$rqd);
$this->db->where('m_urd_recharge_status',2);
$this->db->where('m_urd_mid',trim($id));
$this->db->update('tr05_recharge_detail', $responsedata);
} 
public function set_refundmargin($tid,$rqd,$margin,$mid)
{
$this->load->library('session');
$this->load->database();
$this->load->helper('url');
$this->load->model('users_records');
$timezone = new DateTimeZone("Asia/Kolkata" );
$date = new DateTime();
$date->setTimezone($timezone );
$trnasction_id=$tid;
$id=$mid;
if($margin!=0)
{
if($id!=0)
{
$availamt=$this->users_records->user_avail_amt1($id);
}
$ledgerdata =array(
'm_u_id'=>trim($id),
'm_trans_id'=>$trnasction_id,
'm_cramount'=>$margin,
'm_description'=>"Refund -Recharge Service Request Failed.TID-".'&nbsp;'.$tid,
'm_dramount'=>'',
'm_transdate'=>$date->format( 'Y-m-d H-i-s' ),
'm_deduction'=>"",
'm_refrence_id'=>$rqd,
'm_ledger_type'=>4,
'm_current_balance'=>($availamt+$margin),
'm_datetime'=>$date->format( 'Y-m-d H-i-s' )
);
$this->db->insert('tr03_manage_ledger', $ledgerdata);
}
}



public function st_op_margin($stid,$op,$gid)
{
$this->load->database();
$this->load->helper('url');
$id=$stid;
$grid=$gid;
$opid=$op;
$this->db->where('m_service_id',trim($id));
$this->db->where('m_so_name',trim($opid));
$this->db->where('m_gr_id',$grid);
$data['mar']=$this->db->get('tr07_op_margin');
foreach($data['mar']->result() as $row)
{
$per=$row->m_op_margin;
return $per;
}
}

public function set_marstid($tid,$rqd,$status,$mid)
{
$this->load->library('session');
$this->load->database();
$this->load->helper('url');
$timezone = new DateTimeZone("Asia/Kolkata" );
$date = new DateTime();
$date->setTimezone($timezone );
$trnasction_id=$tid;
$id=$mid;
$sta=$status;
$responsedata =array(
'm_urd_admindate'=>$date->format( 'Y-m-d H-i-s' ),
'm_urd_recharge_status'=>$sta
);
$this->db->where('m_urd_trans_id',$rqd);
$this->db->where('m_urd_recharge_status',2);
$this->db->where('m_urd_mid',trim($id));
$this->db->update('tr05_recharge_detail', $responsedata);
}

}
?>