<?php
	class Support_model extends CI_Model
	{
		public function __construct() 
		{
			parent::__construct();
            $timezone = new DateTimeZone("Asia/Kolkata" );
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
		}
		public function assign_menu()
		{
			//$desig=$this->session->userdata('designation');
			//$id=$this->session->userdata('profile_id');
			$desig=$this->session->userdata('designation');
			$data['menu']=$this->db->query("SELECT * FROM `view_menu` WHERE tr_assign_desig=$desig AND m_menu_status=1 GROUP BY m_menu_name ORDER BY tr_assign_menuid ");
			return $data;
		}
		
		public function get_submit_ticket_detail()
		{
			$affid=$this->session->userdata["affid"];
			$empid=$this->session->userdata["profile_id"];
			$condition='';
			$txtdepartment="-1";//replace $txtdepartment='-1';
			if($this->input->post('txtdepartment')!="" && $this->input->post('txtdepartment')!="-1")
			{
				$txtdepartment=$this->input->post('txtdepartment');
				$data['depart']=$txtdepartment;
			}
			if($txtdepartment=="" || $txtdepartment=="-1")
			{
				$condition = $condition." affiliate_id=".$affid." and emp_id=".$empid." GROUP BY tkt_ref_no order by tkt_id desc";
			}
			else
			{
				$condition = $condition." tkt_department=".$txtdepartment." and affiliate_id=".$affid." and emp_id=".$empid." GROUP BY tkt_ref_no order by tkt_id desc";
			}
			
			$clsdt= array(
			'person_reg_id'=>$condition
			);
			$query = "CALL sp_view_ticket(?)";
			$data['rec']=$this->db->query($query,$clsdt);
			$this->db->free_db_resource();
			$clsdt1= array(
			'aff_id'=>$condition
			);
			$query1 = "CALL sp_find_desig(?)";
			$data['emp']=$this->db->query($query1,  $clsdt1);
			$this->db->free_db_resource();
			return $data;
		}
		
		
		public function view_ticket()
		{
			$p_id=$this->uri->segment(3);
			$condition='';
			$condition=$condition."m46_submit_ticket.tkt_ref_no='$p_id'";
			$clsdt1= array(
			'querey'=>$condition
			);
			$query1 = "CALL sp_view_ticket(?)";
			$data['rec']=$this->db->query($query1,$clsdt1);
			$this->db->free_db_resource();
			$this->db->where('tkt_ref_no',$p_id);
			$data['ticket']=$this->db->get('m46_submit_ticket');
			foreach($data['ticket']->result() as $row)
			{
				$id=$row->tkt_id;	
			}
			$condition = "tr24_ticket_transaction.tkt_id='$id'";
			$clsdt= array(
			'querey'=>$condition
			);
			$query = "CALL sp_view_ticket_reply(?)";
			$data['rec1']=$this->db->query($query,$clsdt);
			$this->db->free_db_resource();
			return $data;
		}
		
		
		public function affiliates_response()
		{
			$tic_no=$this->input->post('txtticket');
            $ticket_no=$this->input->post('txtticket');	
			$description=$this->input->post('txtdiscription');
			$data['rec']=$this->db->query("SELECT tkt_email,tkt_person_name FROM m46_submit_ticket WHERE tkt_ref_no='$ticket_no' GROUP BY tkt_email");
			foreach($data['rec']->result() as $rec)
			{
				$tic_owner=$rec->tkt_email;
                $txtname=$rec->tkt_person_name;
				break;
			}
			$config['upload_path']     =  "application/uploadimage/";
			$config['allowed_types']  =  "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']         =   "5000";
			$config['max_width']     =   "1907";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$tic=$this->input->post('txtticket');
            $ip=$_SERVER['REMOTE_ADDR'];
			$fileupload=$finfo['raw_name'].$finfo['file_ext'];
			if($fileupload=="")
			{
				$fi="";
			}
			else
			{
				$fi=$fileupload;
			}
			$data=array(
			'proc'=>2,'ticket_no'=>$this->input->post('txtticket'),'tkt_person_name'=>'','tkt_email'=>'','tkt_department'=>'','emp_id'=>'','tkt_subject'=>'','tkt_urgency'=>'','tkt_discription'=>'','tkt_response_type'=>1,'response_by'=>$this->session->userdata('name'),'tkt_userfile'=>$fi,'tkt_status'=>1,'account_id'=>'','affiliate_id'=>'','tkt_sub_date'=>$this->curr,'trans_description'=>$this->input->post('txtdiscription'),'trans_response_date'=>$this->curr,'trans_status'=>1
			);
			$query = "CALL sp_ticket_submission(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,@a)";
			$data['rec']=$this->db->query($query,$data);
			$this->db->free_db_resource();
            $data['rec']=$this->db->query("SELECT @a as resp");
			foreach($data['rec']->result() as $rows)
			{
				break;
			}
			$message = '<p>Dear: '.$txtname.',<br><br>
			This is a notification to let you know that we have responded back to your query with reference to the ticket no #'.$this->input->post('txtticket').'. 
			Please re-open the ticket and respond accordingly.<br><br>
			Message: '. $description.'<br><br>
			Priority: High<br><br>
			Status: Open<br><br>
			Your IP: '. $ip.'<br><br>
			For checking your Ticket- '.base_url().'index.php/direct_ticket/view_ticket/'.$ticket_no.'</p>';
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL,'Support Techaarjavam'); // change it to yours
			$this->email->to($tic_owner);// change it to yours
			$this->email->subject('Techaarjavam Support Ticket[ID:'.$ticket_no.']');
			$this->email->message($message);
			if($this->email->send())
			{
				return $tic; 
			}
			else
			{
				show_error($this->email->print_debugger());
			}
		}
		
		
		public function resolve()
		{
			$ticket_no=$this->input->post('ticket_no');
            $data['rec']=$this->db->query("SELECT tkt_email,tkt_person_name FROM m46_submit_ticket WHERE tkt_ref_no='$ticket_no' GROUP BY tkt_email");
			foreach($data['rec']->result() as $rec)
			{
				$tic_owner=$rec->tkt_email;
				$txtname=$rec->tkt_person_name;
				break;
			}
			$data=array(       
			'proc'=>3,'ticket_no'=>$ticket_no,'tkt_person_name'=>'','tkt_email'=>'','tkt_department'=>'','emp_id'=>'','tkt_subject'=>'','tkt_urgency'=>'','tkt_discription'=>'','tkt_response_type'=>1,'response_by'=>'','tkt_userfile'=>'','tkt_status'=>0,'account_id'=>'','affiliate_id'=>'','tkt_sub_date'=>$this->curr,'trans_description'=>'','trans_response_date'=>'','trans_status'=>''
			);
			$query = "CALL sp_ticket_submission(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,@a)";
			$data['rec']=$this->db->query($query,$data);
			$this->db->free_db_resource();
			$ip=$_SERVER['REMOTE_ADDR'];
			$message = '<p>Dear: '.$tic_owner.',<br><br>
			This is in response of your attempt that your all query has been resolved now. So, we are considering it true and here by changing the status of your ticket(#'.$this->input->post('ticket_no').') to closed .<br><br> Thanking you,<br><br>
			Keep visiting us!<br><br>
			Priority: High<br><br>
			Status: Closed<br><br>
			Your IP: '. $ip.'<br><br>
			For checking your Ticket- '.base_url().'index.php/direct_ticket/view_ticket/'.$ticket_no.'</p>';
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from(EMAIL,'Support Techaarjavam'); // change it to yours
			$this->email->to($tic_owner);// change it to yours
			$this->email->subject('Techaarjavam Support Ticket[ID:'.$ticket_no.']');
			$this->email->message($message);
			if($this->email->send())
			{
				return $ticket_no;
			}
			else
			{
				show_error($this->email->print_debugger());
			}
		}
		
		
		public function announcements()
		{
			$t=array('1','3');
			$this->db->where_in('for',$t);
			$data['rec']=$this->db->get('tr19_anoncement');
			$this->db->free_db_resource();
			return $data;
		}
		
		
		public function full_Announcements()
		{
			$p_id=$this->uri->segment(3);
			$this->db->where('id',$p_id);
			$data['rec']=$this->db->get('tr19_anoncement');
			$this->db->where('m_des_id',$this->session->userdata('designation'));
			return $data;
			
		}
		
		
		
		public function view_lead()
		{
			$data['id']=$this->uri->segment(3);
			$smenu=array(
			'proc'=>2,
			'lead_for'=>'',
			'lead_owner'=>$this->session->userdata('profile_id'),
			'lead_created_by'=>$this->session->userdata('user_type'),
			'lead_company'=>'',
			'lead_prefix'=>'',
			'lead_name'=>'',
			'lead_title'=>'',
			'lead_industry'=>'',
			'lead_emp'=>'',
			'lead_email'=>'',
			'lead_mobile'=>'',
			'lead_source'=>'',
			'lead_current_status'=>'',
			'lead_description'=>'',
			'lead_address'=>'',
			'lead_city'=>'',
			'lead_state'=>'',
			'lead_zipcode'=>'',
			'lead_country'=>'',
			'lead_status'=>'',
			'lead_logintype'=>'',
			'lead_regdate'=>''
			);
			$query = " CALL sp_lead(?" . str_repeat(",?", count($smenu)-1) ." ,@a) ";
			$data['rec']=$this->db->query($query, $smenu);
			$this->db->free_db_resource();
			$data['response']=$this->db->query("SELECT @a as resp");
			$this->db->where('or_m_designation',4);
			$this->db->where('or_m_status',1);
			$data['user']=$this->db->get('m06_user_detail');
			return $data;
			
		}
		
		
		public function view_opportunity()
		{
			$this->db->where('opportunity_owner',$this->session->userdata('profile_id'));
			$this->db->where('opportunity_owned_by',$this->session->userdata('user_type'));
			$data['info']=$this->db->get('m35_opportunity');
			return $data;
		}
		
		
		public function view_user_profile()
		{
			$condition ="";
			$affid=$this->session->userdata('affid');
			$empid=$this->session->userdata('profile_id');
			$data['user']=$this->db->query("CALL sp_user(1,'`m06_user_detail`.`or_m_reg_id`=".$this->session->userdata('profile_id')." ')");
			$this->db->free_db_resource();
			$condition = $condition." affiliate_id=".$affid." and emp_id=".$empid." GROUP BY tkt_ref_no order by tkt_id desc";
			$clsdt= array(
			'person_reg_id'=>$condition
			);
			$query = "CALL sp_view_ticket(?)";
			$data['rec']=$this->db->query($query,$clsdt);
			$this->db->free_db_resource();
			$t=array('1','3');
			$this->db->where_in('for',$t);
			$data['rec12']=$this->db->get('tr19_anoncement');
			$this->db->free_db_resource();
			$empcode=0;$empregid=$this->session->userdata('profile_id');$empdesig=0;$empspecification=0;$year=date('Y');$month=date('m');
			$data['empcode']=0;$data['empregid']=0;$data['empdesig']=0;$data['empspecification']=0;$data['month']=$month;$data['year']=$year;
			$query ="CALL sp_employee_attendance(1,'$empcode','$empregid','$empdesig','$empspecification','$year','$month',@i) ";
			$data['atten']=$this->db->query($query);
			$this->db->free_db_resource();
			$empregid=$this->session->userdata('profile_id');
			$year=date('Y');
			$month=date('m');
			$empcode=date('d');
			$query ="CALL sp_employee_attendance(2,'$empcode','$empregid','','','$year','$month',@i) ";
			$data['full_att']=$this->db->query($query);
			$this->db->free_db_resource();
			$data['leave']=$this->db->query("SELECT COUNT(`tr_leave_m_id`) as leaves FROM `tr28_leave_management` WHERE DATE_FORMAT(`tr_leave_date`,'%Y-%m')=DATE_FORMAT('".date('Y-m')."','%Y-%m') AND `or_m_reg_id`='".$this->session->userdata('profile_id')."' ")->row();
			$data['task']=$this->db->query("SELECT COUNT(`m_ptask_id`) AS taskk FROM `m48_task_on_project` WHERE `m_ptask_assign_to`='".$this->session->userdata('profile_id')."' ")->row();
			$this->db->free_db_resource();
			$query=$this->db->query("SELECT GetProjectByParticipantsId(".$this->session->userdata('profile_id').") as total_project");
			$row=$query->row();
			$data['total_project']=$row->total_project;
			$this->db->free_db_resource();
			$query = " CALL sp_view_all_project('`tr22_project_participants`.`parti_participants_id`=".$this->session->userdata('profile_id')."')";
			$data['project']=$this->db->query($query);
			$this->db->free_db_resource();
			return $data;
		}
		
		public function after_profile_update()
		{
			
			$this->db->where('or_m_reg_id',$this->session->userdata('profile_id'));
			//$this->db->where('or_m_reg_id','1');
			$data['user']=$this->db->get('m06_user_detail');
			$data['desig']=$this->db->get('m03_designation');
			$this->db->where('assign_emp_id',$this->session->userdata('profile_id'));
			$this->db->order_by("ticket_id", "desc");
			$this->db->group_by('ticket_no');
			$data['rec']=$this->db->get('tr20_submit_ticket');
			$t=array('1','3');
			$this->db->where_in('for',$t);
			$data['rec12']=$this->db->get('tr19_anoncement');
			return $data;
		}
		
		public function change_profile_image()
		{
			$config['upload_path']    =   "application/emp_pics/";
			$config['allowed_types']  =   "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']        =   "5000";
			$config['max_width']      =   "5000";
			$config['max_height']    =   "5000";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$tic=$this->input->post('txtticket');
			$fileupload=$finfo['raw_name'].$finfo['file_ext'];
			if($fileupload=="")
			{
				$fi="";
			}
			else
			{
				$fi=$fileupload;
			}
			$data=array(
			'or_m_userimage'=>$fi,
			);
			$this->db->where('or_m_reg_id',$this->session->userdata('profile_id'));
			$this->db->update('m06_user_detail',$data);
			$sessiondata=array(
			'image' =>$fi
			);
			$this->session->set_userdata($sessiondata);
		}
		
		
		public function update_profile_details()
		{
			$data= array(
			'or_m_name'=>$this->input->post('txtname'),
			'or_m_email'=>$this->input->post('txtemail'),
			'or_m_mobile_no'=>$this->input->post('txtmobile'),
			'or_m_dob'=>$this->input->post('txtdob'),
			'or_m_address'=>$this->input->post('txtaddress'),
			);
			$this->db->where('or_m_reg_id',$this->session->userdata('profile_id'));
			$this->db->update('m06_user_detail',$data);
			$data1= array(
			'or_login_id'=>$this->input->post('txtemail'),
			);
			$this->db->where('or_user_id',$this->session->userdata('profile_id'));
			$this->db->where('or_login_type',$this->session->userdata('designation'));
			$this->db->update('tr04_login',$data1);
			return "true";
		}
		
		
		public function update_profile_password()
		{
			$data= array(
			'or_login_pwd'=>$this->input->post('txtpassword'),
			'or_pin_pwd'=>$this->input->post('txtpassword')
			);
			$this->db->where('or_user_id',$this->session->userdata('profile_id'));
			$this->db->where('or_login_type',0);
			$this->db->update('tr04_login',$data);
			return "true";
		}	
		
		
		public function account()
		{
			$this->db->order_by('account_id','desc');
			$data['rec']=$this->db->get('m33_account');
			$data['ind']=$this->db->get('m30_industry');
			$data['acc_own']=$this->db->get('m31_ac_ownership');
			$this->db->where('m_des_pat_id',-1);
			$data['type']=$this->db->get('m03_designation');
			$this->db->where('m_parent_id',-1);
			$data['loc']=$this->db->get('m05_location');
			$data['con']=$this->db->get('m34_contact');
			$data['emp']=$this->db->get('m06_user_detail');
			$data['id']=$this->uri->segment(3);
			return $data;
		}
		
		
		public function project()
		{
			$this->db->order_by('account_id','desc');
			$data['rec']=$this->db->get('m33_account');
			$data['ind']=$this->db->get('m30_industry');
			$data['acc_own']=$this->db->get('m31_ac_ownership');
			$this->db->where('m_des_pat_id',-1);
			$data['type']=$this->db->get('m03_designation');
			$this->db->where('m_parent_id',-1);
			$data['loc']=$this->db->get('m05_location');
			$data['con']=$this->db->get('m34_contact');
			$data['emp']=$this->db->get('m06_user_detail');
			$data['id']=$this->uri->segment(3);
			$this->db->where('m_acc_id',$this->uri->segment(3));
			$data['pro']=$this->db->get('m43_acc_project');
			return $data;
			
		}
		
		
		public function project_cost()
		{
			$project_id=$this->uri->segment(3);
			$this->db->order_by('account_id','desc');
			$data['rec']=$this->db->get('m33_account');
			$data['acc_own']=$this->db->get('m31_ac_ownership');
			$this->db->where('m_acc_project',$project_id);
			$data['pro']=$this->db->get('m43_acc_project');
			$data['tax']=$this->db->get('m44_tax');
			$this->db->where('m_project_id',$project_id);
			$this->db->where('m_pay_type','1');
			$data['cost']=$this->db->get('tr21_project_payment');
			$data['count']=$this->db->query("SELECT COUNT(*) as co FROM tr21_project_payment WHERE m_project_id='$project_id'");
			return $data;
		}
		
		
		public function update_project_cost()
		{
			$smenu=array(
			'm_unit_price'=>$this->input->post('txtunitprc'),
			'm_actual_price'=>$this->input->post('txtact_prc'),
			'm_commision'=>$this->input->post('txtcommission'),
			'm_tax'=>$this->input->post('ddtax')
			
			);
			$this->db->where('m_acc_project',$this->uri->segment(3));
			$this->db->update('m43_acc_project',$smenu);
		}	
		
		
		public function payment()
		{
			$project_id=$this->uri->segment(3);
			$data['bank']=$this->db->get('m04_bank');
			$data['user_bank']=$this->db->get('m25_user_bank');
			$this->db->where('m_pm_parent','0');
			$data['payment_mode']=$this->db->get('m23_payment_mode');
			$this->db->where('m_acc_project',$this->uri->segment(3));
			$data['pro']=$this->db->get('m43_acc_project');
			$data['count']=$this->db->query("SELECT COUNT(*) as co FROM tr21_project_payment WHERE m_project_id='$project_id'");
			
			$data['installment']=$this->db->query("SELECT `m_amonut` FROM `tr21_project_payment` WHERE m_pay_id=(SELECT MIN(m_pay_id) FROM tr21_project_payment WHERE m_pay_status='0' AND m_project_id='$project_id')");
			$data['remain']=$this->db->query("SELECT `m_due_amount` FROM `tr21_project_payment` WHERE m_pay_id=(SELECT MAX(m_pay_id) FROM tr21_project_payment WHERE m_pay_status='1' AND m_project_id='$project_id')");
			return $data;
		}
		
		
		public function select_category()
		{
			$this->db->where('m_pm_status',1);
			$this->db->where('m_pm_parent',$this->uri->segment(3));
			$datas=$this->db->get('m23_payment_mode');
			$json=json_encode($datas->result());
			return $json;
		}
		
		
		public function insert_payment()
		{
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			$rem_amt=$this->input->post('txtrem_amt');
			$pay_amt=$this->input->post('txtpay_amt');
			$new_rem_bal=$rem_amt-$pay_amt;
			$data=array(
			'm_project_id'=>$this->uri->segment(3),
			'm_bank_id'=>$this->input->post('bank'),
			'm_paymode'=>$this->input->post('mode'),
			'm_account_no'=>$this->input->post('detail'),
			'm_ddno'=>$this->input->post('transid'),
			'm_amonut'=>$this->input->post('txtpay_amt'),
			'm_due_amount'=>$new_rem_bal,	
			'm_subdate'=>$this->curr,
			'm_pay_description'=>$this->input->post('txtdescription'),	
			'm_pay_status'=>'1',
			'm_pay_reminder'=>'0',
			'm_pay_user_id'=>$this->session->userdata('profile_id'),
			'm_pay_date'=>$this->curr,
			'm_pay_type'=>'1'
			);
			$this->db->insert('tr21_project_payment',$data);
			$sub_date="";
			$no_of_emi=$this->input->post('txtno_emi');
			$curdate=$date->format( 'Y-m-d' );
			
			$emi_charge=$this->input->post('txtemi_charge');
			$due=$new_rem_bal;
			for($i=1;$i<=$no_of_emi;$i++)
			{
				$due=$due-$emi_charge;
				
				$fromdate=date('Y-m-d',strtotime('-1 days'));
				$mod_date = strtotime($fromdate.$i.' months');
				
				$data1=array(
				'm_project_id'=>$this->uri->segment(3),
				'm_bank_id'=>"",
				'm_paymode'=>"",
				'm_account_no'=>"",
				'm_ddno'=>"",
				'm_amonut'=>$this->input->post('txtemi_charge'),
				'm_due_amount'=>$due,	
				'm_subdate'=>date("Y-m-d",$mod_date),
				'm_pay_description'=>"",	
				'm_pay_status'=>'0',
				'm_pay_reminder'=>$this->input->post('txtreminder_date'),
				'm_pay_user_id'=>$this->session->userdata('profile_id'),
				'm_pay_date'=>$this->curr,
				'm_pay_type'=>'1'
				);
				$this->db->insert('tr21_project_payment',$data1);
			}
		}
		
		
		public function update_pay()
		{
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			
			$smenu=array(
			'm_bank_id'=>$this->input->post('bank'),
			'm_paymode'=>$this->input->post('mode'),
			'm_account_no'=>$this->input->post('detail'),
			'm_ddno'=>$this->input->post('transid'),
			'm_pay_description'=>$this->input->post('txtdescription'),
			'm_pay_status'=>1,
			'm_pay_date'=>$this->curr
			);
			$this->db->where('m_pay_id',$this->uri->segment(3));
			$this->db->update('tr21_project_payment',$smenu);
			return 'true';
			
		}
		
		
		public function view_services()
		{
			$data['tax']=$this->db->get('m44_tax');
			$data['servicetype']=$this->db->get('m10_service_type');
			
			$this->db->where('account_type','21');
			$data['acc']=$this->db->get('m33_account');
			$this->db->where('m_acc_id',$this->uri->segment(3));
			$data['service']=$this->db->get('m45_acc_rel_service');
			
			$this->db->where('account_id',$this->uri->segment(3));
			$data['acccount']=$this->db->get('m33_account');
			
			$this->db->where('m_project_id',$this->uri->segment(3));
			$data['payment']=$this->db->get('tr21_project_payment');
			return $data;
		}
		
		
		public function insert_service()
		{
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			$expiry=$this->input->post('txtexpiry');
			$data=array(
			'm_acc_id'=>$this->uri->segment(3),
			'm_project_id'=>"",
			'm_service_id'=>$this->input->post('ddservice'),
			'm_vendor_acc_id'=>$this->input->post('ddvendor'),
			'm_unit'=>$this->input->post('txtunit'),
			'm_unit_price'=>$this->input->post('txtprice'),
			'm_tax'=>$this->input->post('ddtax'),	
			'm_payment_type'=>$this->input->post('ddpayment_type'),	
			'm_expiry_date'=>$expiry,	
			'm_total'=>$this->input->post('txttotal')
			);
			//var_dump($data);
			$this->db->insert('m45_acc_rel_service',$data);
		}
		
		
		public function service_payment()
		{
			$data['bank']=$this->db->get('m04_bank');
			$data['user_bank']=$this->db->get('m25_user_bank');
			$this->db->where('m_pm_parent','0');
			$data['payment_mode']=$this->db->get('m23_payment_mode');
			$this->db->where('m_acc_id',$this->uri->segment(3));
			$data['service']=$this->db->get('m45_acc_rel_service');
			$data['service_type']=$this->db->get('m10_service_type');
			return $data;
		}
		
		
		public function insert_service_payment()
		{
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			$rem_amt=$this->input->post('txtrem_amt');
			$pay_amt=$this->input->post('txtpay_amt');
			$new_rem_bal=$rem_amt-$pay_amt;
			$data=array(
			'm_project_id'=>$this->uri->segment(3),
			'm_bank_id'=>$this->input->post('bank'),
			'm_paymode'=>$this->input->post('mode'),
			'm_account_no'=>$this->input->post('detail'),
			'm_ddno'=>$this->input->post('transid'),
			'm_amonut'=>$this->input->post('txtpay_amt'),
			'm_due_amount'=>$new_rem_bal,
			'm_subdate'=>$date->format( 'Y-m-d H-i-s' ),
			'm_pay_description'=>$this->input->post('txtdescription'),
			'm_pay_status'=>'1',
			'm_pay_reminder'=>'0',
			'm_pay_user_id'=>$this->session->userdata('profile_id'),
			'm_pay_date'=>$this->curr,
			'm_pay_type'=>'2'
			);
			$this->db->insert('tr21_project_payment',$data);
			
		}
		
		
	}
?>