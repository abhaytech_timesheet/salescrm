<?php 
class Acs_model extends CI_Model
	{
		/*Configuration Model function Start Here */
		
		//Select Config Data
		public function select_config()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->db->where('m00_id',1);
			return $this->db->get('m00_setconfig');
		}
		
		// Update Config data
		public function update_config()
		{
			$this->load->helper('url');
			$this->load->database();
			$mcf=array(
			'm00_sitename'=>$this->input->post('txtprojname'),
			'm00_description'=>$this->input->post('txtdesc'),
			'm00_username'=>$this->input->post('txtusername'),
			'm00_password' =>$this->input->post('txtuserpass'),
			'm00_pinpassword'=>$this->input->post('txtuserpinpass')
			);
			$this->db->where('m00_id',1);
			$this->db->update('m00_setconfig',$mcf);
		}
		/* Configuration Model  function End Here  */
		
		/* City/District Model Function Start Here  */
		
		//Select State here
		public function select_state($id)
		{
			$this->load->helper('url');
			$this->load->database();
			if($id!="" && $id!=0)
			{
				$this->db->where('m_parent_id',$id);
				return $this->db->get('m05_location');
			}
			else
			{
				//$this->db->where('m_parent_id',1);
				return $this->db->get('m05_location');
			}
		}
		//Select City here
		public function select_city($id)
		{
			$this->load->helper('url');
			$this->load->database();
			if($id!="" && $id!=0)
			{
				$this->db->where('m_loc_id',$id);
				return $this->db->get('m05_location');
			}
		}
		//Add City Here
		public function add_city()  
		{
			$this->load->helper('url');
			$this->load->database();
			if(trim($this->input->post('txtcity'))!="")
			{
				if(trim($this->input->post('ddstate'))!="-1")
				{
					$city=array(
					'm_loc_name'=>trim($this->input->post('txtcity')),
					'm_parent_id'=>trim($this->input->post('ddstate')),
					'm_status'=>trim($this->input->post('txtstatus'))
					);
					$this->db->insert('m05_location',$city);
				}
			}
		}
		
		//Update City Here
		public function update_city($id)  
		{
			$this->load->helper('url');
			$this->load->database();
			if(trim($this->input->post('txtcity'))!="")
			{
				if(trim($this->input->post('ddstate'))!="-1")
				{
					$city=array(
					'm_loc_name'=>trim($this->input->post('txtcity')),
					'm_parent_id'=>trim($this->input->post('ddstate')),
					'm_status'=>trim($this->input->post('txtstatus'))
					);
					$this->db->where('m_loc_id',$id);
					$this->db->update('m05_location',$city);
				}
			}
		} 
		/* City/District Model Function End Here  */
		
		/* Designation/Rank Model Function Start Here  */
		
		public function select_designation()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->db->where('m_des_user_id',0);
			$this->db->where('m_des_pat_id',0);
			return $this->db->get('m03_designation');
		}
		public function select_rank($id)
		{
			$this->load->helper('url');
			$this->load->database();
			$this->db->where('m_des_id',$id);
			return $this->db->get('m03_designation');
		}
		//Add Designation Here
		public function add_designation()  
		{
			$this->load->helper('url');
			$this->load->database();
			if(trim($this->input->post('txtdesignation'))!="")
			{
				$desig=array(
				'm_des_name'=>trim($this->input->post('txtdesignation')), 
				'm_des_pat_id'=>0, 
				'm_des_user_id'=>0,
				'm_des_description'=>trim($this->input->post('txtdesigdesc')),
				'm_des_orid'=>0, 
				'm_des_status'=>trim($this->input->post('txtstatus'))
				);
				$this->db->insert('m03_designation',$desig);
			}
		}
		
		//Update Desigantion Here
		public function update_designation($id)  
		{
			$this->load->helper('url');
			$this->load->database();
			if(trim($this->input->post('txtdesignation'))!="")
			{
				$desig=array(
				'm_des_name'=>trim($this->input->post('txtdesignation')), 
				'm_des_pat_id'=>0, 
				'm_des_user_id'=>0,
				'm_des_description'=>trim($this->input->post('txtdesigdesc')),
				'm_des_orid'=>0, 
				'm_des_status'=>trim($this->input->post('txtstatus'))
				);
				$this->db->where('m_des_id',$id);
				$this->db->update('m03_designation',$desig);
			}
		}
		/* Roles Function Start Here  */
		public function add_roles()  
		{
			$this->load->helper('url');
			$this->load->database();
			if(trim($this->input->post('txtrole'))!="")
			{
				if(trim($this->input->post('dddepartment'))!="-1")
				{
					$desig=array(
					'm_des_name'=>trim($this->input->post('txtrole')), 
					'm_des_pat_id'=>trim($this->input->post('dddepartment')), 
					'm_des_user_id'=>0,
					'm_des_orid'=>0, 
					'm_des_status'=>trim($this->input->post('txtstatus'))
					);
					$this->db->insert('m03_designation',$desig);
				}
			}
		}
		public function select_roles()
		{
			$this->load->helper('url');
			$this->load->database();
			$query="SELECT
			`m03_designation`.`m_des_id` 	AS 	Parent_id,
			`m03_designation`.`m_des_name`	AS 	Parent_name,
			`m03_designation_1`.`m_des_id` 	AS 	Role_id,
			`m03_designation_1`.`m_des_name`	AS 	Role_name,
			`m03_designation_1`.`m_des_status`	AS 	Role_status,
			`m03_designation_1`.`m_des_pat_id` 	AS 	Role_parent
			FROM
			`appworks_erp`.`m03_designation`
			INNER JOIN `appworks_erp`.`m03_designation` AS `m03_designation_1` 
			ON (`m03_designation`.`m_des_id` = `m03_designation_1`.`m_des_pat_id`);";
			return $this->db->query($query);
		}
		
		//Update Roles Here
		public function update_roles($id)  
		{
			$this->load->helper('url');
			$this->load->database();
			if(trim($this->input->post('txtrole'))!="")
			{
				if(trim($this->input->post('dddepartment'))!="")
				{
					$desig=array(
					'm_des_name'=>trim($this->input->post('txtrole')), 
					'm_des_pat_id'=>trim($this->input->post('dddepartment')), 
					'm_des_user_id'=>0,
					'm_des_description'=>trim($this->input->post('txtroledesc')),
					'm_des_orid'=>0, 
					'm_des_status'=>trim($this->input->post('txtstatus'))
					);
					$this->db->where('m_des_id',$id);
					$this->db->update('m03_designation',$desig);
				}
			}
		}
		
		/* Designation/Rank Model Function End Here  */
		
		
		/* Branch/Affilaite Model Function Start Here  */
		public function select_affiliate()
		{
			$this->load->helper('url');
			$this->load->database();
			//$this->db->where('affiliate_status',1);
			return $this->db->get('m22_affiliate');
		}
		
		public function select_branch($id)
		{
			$this->load->helper('url');
			$this->load->database();
			$this->db->where('affiliate_id',$id);
			return $this->db->get('m22_affiliate');
		}
		//Add Branch/Affilaite Here
		public function add_affiliate()  
		{
			$this->load->helper('url');
			$this->load->database();
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			$branch_code=0;
			$query=$this->db->query("SELECT max(affiliate_id) as affiliate_id FROM `m22_affiliate`");
			$count=$this->db->count_all_results('m22_affiliate');
			if($count>0)
			{
				$row = $query->row();
				$branch_code=$row->affiliate_id ;
				$branch_code='10'.($branch_code+1);
			}
			else
			{
				$branch_code='10'.($branch_code+1);
			}
			if(trim($this->input->post('txtbrchname'))!="")
			{
				if(trim($this->input->post('txtbrchcommuadd'))!="")
				{
					if(trim($this->input->post('txtbrchcontactper'))!="")
					{
						if(trim($this->input->post('txtbrchcontactno'))!="")
						{
							if(trim($this->input->post('txtbrchemailadd'))!="")
							{
								if(trim($this->input->post('txtbrchloginpwd'))!="")
								{
									$affiliate=array(
									'affiliate_code'=>$branch_code,
									'affiliate_name'=>trim($this->input->post('txtbrchname')), 
									'affiliate_address'=>trim($this->input->post('txtbrchcommuadd')),
									'affiliate_conatct_person'=>trim($this->input->post('txtbrchcontactper')),
									'affiliate_contact_no'=>trim($this->input->post('txtbrchcontactno')),
									'affiliate_email'=>trim($this->input->post('txtbrchemailadd')),
									'affiliate_password'=>trim($this->input->post('txtbrchloginpwd')),
									'affiliate_state'=>trim($this->input->post('ddstate')),
									'affiliate_city'=>trim($this->input->post('ddcity')),
									'affiliate_entry_date'=>$date->format( 'Y-m-d H-i-s' ), 
									'affiliate_status'=>trim($this->input->post('txtstatus'))
									);
									$this->db->insert('m22_affiliate',$affiliate);
									
								}
							}
						}
					}
				}
			}
		}
		
		//Update Branch/Affilaite Here
		public function update_affiliate($id)  
		{
			$this->load->helper('url');
			$this->load->database();
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			if(trim($this->input->post('txtbrchname'))!="")
			{
				if(trim($this->input->post('txtbrchcommuadd'))!="")
				{
					if(trim($this->input->post('txtbrchcontactper'))!="")
					{
						if(trim($this->input->post('txtbrchcontactno'))!="")
						{
							if(trim($this->input->post('txtbrchemailadd'))!="")
							{
								if(trim($this->input->post('txtbrchloginpwd'))!="")
								{
									$affiliate=array(
									
									'affiliate_name'=>trim($this->input->post('txtbrchname')), 
									'affiliate_address'=>trim($this->input->post('txtbrchcommuadd')),
									'affiliate_conatct_person'=>trim($this->input->post('txtbrchcontactper')),
									'affiliate_contact_no'=>trim($this->input->post('txtbrchcontactno')),
									'affiliate_email'=>trim($this->input->post('txtbrchemailadd')),
									'affiliate_password'=>trim($this->input->post('txtbrchloginpwd')),
									'affiliate_state'=>trim($this->input->post('ddstate')),
									'affiliate_city'=>trim($this->input->post('ddcity')),
									'affiliate_entry_date'=>$date->format( 'Y-m-d H-i-s' ), 
									'affiliate_status'=>trim($this->input->post('txtstatus'))
									);
									$this->db->where('affiliate_id',$id);
									$this->db->update('m22_affiliate',$affiliate);
									
								}
							}
						}
					}
				}
			}
		}
		
		
		/* Branch/Affilaite Model Function End Here  */
		/*Plan */
		//Select City here
		public function select_plan($id)
		{
			$this->load->helper('url');
			$this->load->database();
			if($id!="" && $id!=0)
			{
				$this->db->where('p_plantype_id',$id);
				return $this->db->get('p02_master_plan');
			}
		}
		public function check_code($code)
		{
			$code=0;
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$this->db->where('or_m_user_id',$code);
			$count1=$this->db->count_all_results('m06_user_detail');
			/*if($count1>1)
			{
				$code=$code+1;
				$this->check_code($code);
				break;
			}*/
			return $code;
		}
		//CODE
		/*public function empcode($type)
			{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone );
			$doj=$date->format('my'); 
			$emp_code=0;
			$query=$this->db->query("SELECT max(or_m_reg_id) as or_m_reg_id FROM `m06_user_detail`");
			$count=$this->db->count_all_results('m06_user_detail');
			if($count>0)
			{
			$row = $query->row();
			$emp_code=$row->or_m_reg_id;
			$emp_code=($emp_code+1);
			$length=strlen($emp_code);
			if($length<3)
			{
			$emp_code='00'.$emp_code;
			}
			else
			{
			$emp_code=($emp_code);
			}
			if($type==1)
			{$code='3030'.$doj.$emp_code;}
			if($type==2)
			{$code='71520'.$doj.$emp_code;}
			
			$this->db->where('or_m_user_id',$code);
			$count1=$this->db->count_all_results('m06_user_detail');
			if($count1>1)
			{
			$code=$this->check_code($code);
			return $code;
			}
			else
			{
			return $code;
			}
			}
			else
			{
			if($type==1)
			{$code='3030'.$doj.'001';}
			if($type==2)
			{$code='71520'.$doj.'001';}
			$this->db->where('or_m_user_id',$code);
			$count1=$this->db->count_all_results('m06_user_detail');
			if($count1>1)
			{
			$code=$this->check_code($code);
			return $code;
			}
			else
			{
			return $code;
			}
			}
		}*/
		public function get_uid($id)
		{
			$aid=$id;
			$this->load->library('session');
			$this->load->database();
			$this->load->helper('url');
			$this->db->where('or_m_user_id',$aid);
			$data['membername']=$this->db->get('m06_user_detail');
			foreach($data['membername']->result() as $row)
			{
				$mname=$row->or_m_reg_id;
				return $mname;
			}
		}
		public function get_associate_id($id)
		{
			$aid=$id;
			$this->load->library('session');
			$this->load->database();
			$this->load->helper('url');
			$data['membername']=$this->db->query("select * from m06_user_detail where or_m_designation!=11 and or_m_user_id='$aid' ");
			foreach($data['membername']->result() as $row)
			{
				$mname=$row->or_m_reg_id;
				return $mname;
			}
		}
		public function get_customer_id($id)
		{
			$aid=$id;
			$this->load->library('session');
			$this->load->database();
			$this->load->helper('url');
			$this->db->where('or_m_designation',11);
			$this->db->where('or_m_user_id',$aid);
			$data['membername']=$this->db->get('m06_user_detail');
			foreach($data['membername']->result() as $row)
			{
				$mname=$row->or_m_reg_id;
				return $mname;
			}
		}
		
		//Associate Registration here 
		public function signup($loginpwd,$pinpwd,$type)
		{
			$this->load->database();
			$this->load->library('session');
			$email=$this->input->post('txtemail');
			$isValidate = true;
			
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			$userid_code=trim($this->empcode($type));
			$info=array(
			'or_m_user_id'=>trim($userid_code),
			'or_m_designation'=> trim($this->input->post('ddrank')),
			'or_m_name'=>trim($this->input->post('txtassociate_name')),
			'or_m_dob'=>date('Y-m-d', strtotime($this->input->post('txtdob'))),
			'or_m_gurdian_name'=>trim($this->input->post('txtparent')),
			'or_member_joining_date'=>$date->format( 'Y-m-d' ),
			'or_m_gender'=>trim($this->input->post('rbgender')),
			'or_m_email'=>trim($this->input->post('txtemail')),
			'or_m_landline_no'=>'',
			'or_m_mobile_no'=>trim($this->input->post('txtphone')),
			'or_m_address'=>trim($this->input->post('txtaddress')),
			'or_m_pincode'=>trim($this->input->post('txtpincode')),
			'or_m_country'=>1,
			'or_m_state'=>trim($this->input->post('txtstate')),
			'or_m_userimage'=>'',
			'or_m_city'=>trim($this->input->post('txtcity')),
			'or_m_status'=>2,
			'or_m_intr_id'=>$this->get_uid($this->input->post('txtintroducer_id')),
			'or_m_intr_name'=>trim($this->input->post('txtintroducer_name')),
			'or_m_aff_id'=>trim($this->input->post('ddbranch')),
			'or_m_upliner_id'=>'',
			'or_m_position'=>'',
			'm_pin_id'=>'',
			'or_m_regdate'=>$date->format( 'Y-m-d H-i-s'),
			'or_m_n_name'=>trim($this->input->post('txtnominee_name')),
			'or_m_n_parent_name'=>trim($this->input->post('txtnominee_parent')),
			'or_m_n_address'=>trim($this->input->post('txtnominee_paddress')),
			'or_m_n_dob'=>trim($this->input->post('txtnominee_age')),
			'or_m_n_gender'=>trim($this->input->post('ddnominee_gender')),
			'or_m_n_relation'=>trim($this->input->post('txtrelation')),
			'or_m_n_state'=>trim($this->input->post('txtstate1')),
			'or_m_n_city'=>trim($this->input->post('txtcity1')),
			'or_m_n_status'=>1,
			'or_m_b_ifscode'=>trim($this->input->post('txtifscode')),
			'or_m_b_cbsacno'=>trim($this->input->post('txtaccount')), 
			'or_m_b_name'=>trim($this->input->post('ddbank_name')), 
			'or_m_b_branch'=>trim($this->input->post('txtbranch_name')),
			'or_m_b_pancard'=>trim($this->input->post('txtpancard')),
			'or_login_pwd'=>trim($loginpwd),
			'or_pin_pwd'=>trim($pinpwd),
			'proc'=>1
			);
			$query = " CALL sp_member(?" . str_repeat(",?", count($info)-1) . ")";
			$this->db->query($query, $info);
			return trim($userid_code);
		}
		
		public function edit_signup($type,$userid)
		{
			$this->load->database();
			$this->load->library('session');
			$email=$this->input->post('txtemail');
			$isValidate = true;
			
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			$info=array(
			'or_m_user_id'=>'',
			'or_m_designation'=> trim($this->input->post('ddrank')),
			'or_m_name'=>trim($this->input->post('txtassociate_name')),
			'or_m_dob'=>date('Y-m-d', strtotime($this->input->post('txtdob'))),
			'or_m_gurdian_name'=>trim($this->input->post('txtparent')),
			'or_member_joining_date'=>$date->format( 'Y-m-d' ),
			'or_m_gender'=>trim($this->input->post('rbgender')),
			'or_m_email'=>trim($this->input->post('txtemail')),
			'or_m_landline_no'=>'',
			'or_m_mobile_no'=>trim($this->input->post('txtphone')),
			'or_m_address'=>trim($this->input->post('txtaddress')),
			'or_m_pincode'=>trim($this->input->post('txtpincode')),
			'or_m_country'=>1,
			'or_m_state'=>trim($this->input->post('txtstate')),
			'or_m_userimage'=>'',
			'or_m_city'=>trim($this->input->post('txtcity')),
			'or_m_status'=>2,
			'or_m_intr_id'=>$this->get_uid($this->input->post('txtintroducer_id')),
			'or_m_intr_name'=>trim($this->input->post('txtintroducer_name')),
			'or_m_aff_id'=>trim($this->input->post('ddbranch')),
			'or_m_upliner_id'=>'',
			'or_m_position'=>'',
			'm_pin_id'=>'',
			'or_m_regdate'=>$date->format( 'Y-m-d H-i-s'),
			'or_m_n_name'=>trim($this->input->post('txtnominee_name')),
			'or_m_n_parent_name'=>trim($this->input->post('txtnominee_parent')),
			'or_m_n_address'=>trim($this->input->post('txtnominee_paddress')),
			'or_m_n_dob'=>trim($this->input->post('txtnominee_age')),
			'or_m_n_gender'=>trim($this->input->post('ddnominee_gender')),
			'or_m_n_relation'=>trim($this->input->post('txtrelation')),
			'or_m_n_state'=>trim($this->input->post('txtstate1')),
			'or_m_n_city'=>trim($this->input->post('txtcity1')),
			'or_m_n_status'=>1,
			'or_m_b_ifscode'=>trim($this->input->post('txtifscode')),
			'or_m_b_cbsacno'=>trim($this->input->post('txtaccount')), 
			'or_m_b_name'=>trim($this->input->post('ddbank_name')), 
			'or_m_b_branch'=>trim($this->input->post('txtbranch_name')),
			'or_m_b_pancard'=>trim($this->input->post('txtpancard')),
			'or_login_pwd'=>'',
			'or_m_reg_id'=>trim($userid),
			'proc'=>2
			);
			$query = " CALL sp_member(?" . str_repeat(",?", count($info)-1) . ")";
			$this->db->query($query, $info);
			return "true";
		}
		
		//Get regi_id from contrat id
		public function get_id_contractid($id)
		{
			$aid=$id;
			$this->load->library('session');
			$this->load->database();
			$this->load->helper('url');
			$this->db->where('plan_contract_id',$aid);
			$data['membername']=$this->db->get('m46_plan_booking');
			foreach($data['membername']->result() as $row)
			{
				$mname=$row->plan_book_id;
				return $mname;
			}
		}
		
		//select emp code
		public function empcode()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('session');
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone );
			$doj=$date->format('my'); 
			$query=$this->db->query("SELECT max(or_m_reg_id) as or_m_reg_id FROM `m06_user_detail` WHERE `or_m_status`='1'");
			$count=$this->db->count_all_results('m06_user_detail');
			if($count>0)
			{
				$row = $query->row();
				$emp_code=$row->or_m_reg_id;
				$emp_code=($emp_code+1);
				$length=strlen($emp_code);
				if($length<3)
				{
					$emp_code='00'.$emp_code;
				}
				else
				{
					$emp_code=($emp_code);
				}
				$code='FIPL'.$doj.'EMP'.$emp_code;
				return $code;
			}
			else
			{
				$code='FIPL'.$doj.'EMP'.'001';
				return $code;
			}
		}	
		
	}
?>