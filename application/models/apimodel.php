<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Apimodel extends CI_Model
	{
		public function __construct() 
		{
			parent::__construct();
			$timezone = new DateTimeZone("Asia/Kolkata" );
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
		}
		
		// --------------------------------Start Insert API Here-----------------------------------------
		
		public function add_api()
		{
			$api=array(
			'Api_id'=>'',
			'Api_name'=>$this->input->post('txtapi_provname'),
			'Api_url'=>$this->input->post('txtapi_url'),
			'Api_status'=>$this->input->post('rbapi'),
			'proc'=>1
			);
			$query="CALL sp_api(?" . str_repeat(",?", count($api)-1) . ")";
			$this->db->query($query,$api);
		}
		// --------------------------------Start status change API Here-----------------------------------------
		
		public function change_status()
		{
			$chk=trim($this->input->post('chkid'),',');
			$unchk=trim($this->input->post('unchkid'),',');
			if($chk != '')
			{
				$this->db->query("UPDATE `m12_api` SET `m12_api`.`m_api_status`=1 WHERE `m12_api`.`m_api_id` IN (".$chk.")");
			}
			if($unchk != '')
			{
				$this->db->query("UPDATE `m12_api` SET `m12_api`.`m_api_status`=0 WHERE `m12_api`.`m_api_id` IN (".$unchk.")");
			}
		}
		
		// --------------------------------Start update API Here-----------------------------------------
		
		public function update_api()
		{
			$api=array(
			'Api_id'=>$this->uri->segment(3),
			'Api_name'=>$this->input->post('txtapi_provname'),
			'Api_url'=>$this->input->post('txtapi_url'),
			'Api_status'=>$this->uri->segment(3),
			'proc'=>2
			);
			$query="CALL sp_api(?" . str_repeat(",?", count($api)-1) . ")";
			$this->db->query($query,$api);
		}
		
		public function add_api_url()
		{
			$api_url=array(
			'Api_url_id'=>'',
			'Api_id'=>$this->input->post('ddapi'),
			'Api_url_name'=>$this->input->post('txtapi_name'),
			'Api_url_address'=>$this->input->post('txtapi_url'),
			'Api_url_prm'=>$this->input->post('txtapiprm'),
			'Api_url_for'=>$this->input->post('rb_apifor'),
			'Api_url_status'=>$this->input->post('rbapi'),
			'proc'=>1
			);
			$query="CALL sp_api_url(?" . str_repeat(",?", count($api_url)-1) . ",@aa)";
			$this->db->query($query,$api_url);
			
		}
		
		public function stch_api_url()
		{
			$chk=trim($this->input->post('chkid'),',');
			$unchk=trim($this->input->post('unchkid'),',');
			if($chk != '')
			{
				$this->db->query("UPDATE `m16_api_url` SET `m16_api_url`.`m_api_url_status`=1 WHERE `m16_api_url`.`m_api_id` IN (".$chk.")");
			}
			if($unchk != '')
			{
				$this->db->query("UPDATE `m16_api_url` SET `m16_api_url`.`m_api_url_status`=0 WHERE `m16_api_url`.`m_api_id` IN (".$unchk.")");
			}
		}
		public function update_api_url()
		{
			$api_url=array(
			'Api_url_id'=>$this->uri->segment(3),
			'Api_id'=>$this->input->post('ddapi'),
			'Api_url_name'=>$this->input->post('txtapi_name'),
			'Api_url_address'=>$this->input->post('txtapi_url'),
			'Api_url_prm'=>$this->input->post('txtapiprm'),
			'Api_url_for'=>'',
			'Api_url_status'=>'',
			'proc'=>2
			);
			$query="CALL sp_api_url(?" . str_repeat(",?", count($api_url)-1) . ",@aa)";
			$this->db->query($query,$api_url);
			
		}
		
		public function add_api_service()
		{ 
			//echo($this->input->post('rbapi'));die;
			$api_ser=array(
			'Service_id'=>'',
			'Servicetype_id'=>$this->input->post('ddapi_service'),
			'Api_id'=>$this->input->post('ddapi'),
			'Service_status'=>$this->input->post('rbapi'),
			'proc'=>1
			);
			$query="CALL sp_api_service(?" . str_repeat(",?", count($api_ser)-1) . ",@a)";
			$this->db->query($query,$api_ser);
			$resp='';
			$data['response']=$this->db->query("SELECT @a as resp");
			foreach($data['response']->result() as $r)
			{
				$resp=$r->resp;
			}
			return $resp;
		}
		
		public function update_api_service()
		{
			$api_ser=array(
			'Service_id'=>$this->uri->segment(3),
			'Servicetype_id'=>$this->input->post('ddapi_service'),
			'Api_id'=>$this->input->post('ddapi'),
			'Service_status'=>'',
			'proc'=>2
			);
			$query="CALL sp_api_service(?" . str_repeat(",?", count($api_ser)-1) . ",@a)";
			$this->db->query($query,$api_ser);
			$resp='';
			$data['response']=$this->db->query("SELECT @a as resp");
			foreach($data['response']->result() as $r)
			{
				$resp=$r->resp;
			}
			return $resp;
		}
		public function stch_api_services()
		{
			$chk=trim($this->input->post('chkid'),',');
			$unchk=trim($this->input->post('unchkid'),',');
			if($chk != '')
			{
				$this->db->query("UPDATE `m11_services` SET `m11_services`.`m_service_status`=1 WHERE `m11_services`.`m_service_id` IN (".$chk.")");
			}
			if($unchk != '')
			{
				$this->db->query("UPDATE `m11_services` SET `m11_services`.`m_service_status`=0 WHERE `m11_services`.`m_service_id` IN (".$unchk.")");
			}
			
		}
		
		public function select_st()
		{
			$this->db->where('Api_id1',$this->uri->segment(3));
			$this->db->where('Service_status',1);
			$data=$this->db->get('view_api_service'); 
			$json=json_encode($data->result());
			return $json;
		}
		public function select_srv_provider()
		{
			$ques=$this->uri->segment(3);
		$qui=explode('_',$ques);
		$data['id']=$qui[0];
		$this->db->where('Sp_type_id',$qui[1]);
		$data=$this->db->get('view_service_provider');
		$json=json_encode($data->result());
		return $json;
		}
		
		public function add_api_operator()
		{
		$ques=$this->input->post('ddapi_service');
		$qui=explode('_',$ques);
		$apiop=array(
		'So_id'=>'',
		'Service_id'=>$qui[0],
		'So_name'=>$this->input->post('ddsp'),
		'So_code'=>$this->input->post('txtoptcode'),
		'So_status'=>$this->input->post('rbapi'),
		'proc'=>1
		);
		$query="CALL sp_api_oprator(?" . str_repeat(",?", count($apiop)-1) . ",@aa)";
		$this->db->query($query,$apiop);
		}
		
		public function st_chserop()
		{
			$chk=trim($this->input->post('chkid'),',');
			$unchk=trim($this->input->post('unchkid'),',');
			if($chk != '')
			{
				$this->db->query("UPDATE `m15_service_operator` SET `m15_service_operator`.`m_so_status`=1 WHERE `m15_service_operator`.`m_so_id` IN (".$chk.")");
			}
			if($unchk != '')
			{
				$this->db->query("UPDATE `m15_service_operator` SET `m15_service_operator`.`m_so_status`=0 WHERE `m15_service_operator`.`m_so_id` IN (".$unchk.")");
			}
		}
		
		public function update_service_opt()
		{
		$id=$this->uri->segment(3);
		$apiop=array(
		'So_id'=>$id,
		'Service_id'=>'',
		'So_name'=>'',
		'So_code'=>$this->input->post('txtoptcode1'),
		'So_status'=>'',
		'proc'=>2
		);
		$query="CALL sp_api_oprator(?" . str_repeat(",?", count($apiop)-1) . ",@aa)";
		$this->db->query($query,$apiop);
		$resp='';
		$data['response']=$this->db->query("SELECT @aa as resp");
		foreach($data['response']->result() as $r)
		{
		$resp=$r->resp;
		}
		return $resp;
		}
		
		}
?>