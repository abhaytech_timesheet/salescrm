<?php
	class Real_mastermodel extends CI_Model
	{
		
		
		//Select Config Data
		public function select_config()
		{
			$this->load->helper('url');
			$this->load->database();
			$this->db->where('m00_id',1);
			return $this->db->get('m00_setconfig');
		}
		
		
		//------------Gret User Id---------------
		public function get_uid($id)
		{
			$aid=$id;
			$this->load->library('session');
			$this->load->database();
			$this->load->helper('url');
			$this->db->where('or_m_user_id',$aid);
			$data['membername']=$this->db->get('m03_user_detail');
			foreach($data['membername']->result() as $row)
			{
				$mname=$row->or_m_reg_id;
				return $mname;
			}
		}
		
		
		// Update Config data
		public function update_config()
		{
			$this->load->helper('url');
			$this->load->database();
			$mcf=array(
			'm00_sitename'=>$this->input->post('txtprojname'),
			'm00_description'=>$this->input->post('txtdesc'),
			'm00_username'=>$this->input->post('txtusername'),
			'm00_password' =>$this->input->post('txtuserpass'),
			'm00_pinpassword'=>$this->input->post('txtuserpinpass')
			);
			$this->db->where('m00_id',1);
			$this->db->update('m00_setconfig',$mcf);
		}
		/* Configuration Model  function End Here  */
		
		//Select State here
		public function select_state($id)
		{
			$this->load->helper('url');
			$this->load->database();
			if($id!="" && $id!=0)
			{
				$this->db->where('m_parent_id',$id);
				return $this->db->get('m02_location');
			}
			else
			{
				//$this->db->where('m_parent_id',1);
				return $this->db->get('m02_location');
			}
		}
		
		
		//Select City here
		public function select_city($id)
		{
			$this->load->helper('url');
			$this->load->database();
			if($id!="" && $id!=0)
			{
				$this->db->where('m_loc_id',$id);
				return $this->db->get('m02_location');
			}
		}
		
		
		//Add City Here
		public function add_city()  
		{
			$this->load->helper('url');
			$this->load->database();
			if(trim($this->input->post('txtcity'))!="")
			{
				if(trim($this->input->post('ddstate'))!="-1")
				{
					$city=array(
					'm_loc_name'=>trim($this->input->post('txtcity')),
					'm_parent_id'=>trim($this->input->post('ddstate')),
					'm_status'=>trim($this->input->post('txtstatus'))
					);
					$this->db->insert('m02_location',$city);
				}
			}
		}
		
		
		//Update City Here
		public function update_city($id)  
		{
			$this->load->helper('url');
			$this->load->database();
			if(trim($this->input->post('txtcity'))!="")
			{
				if(trim($this->input->post('ddstate'))!="-1")
				{
					$city=array(
					'm_loc_name'=>trim($this->input->post('txtcity')),
					'm_parent_id'=>trim($this->input->post('ddstate')),
					'm_status'=>trim($this->input->post('txtstatus'))
					);
					$this->db->where('m_loc_id',$id);
					$this->db->update('m02_location',$city);
				}
			}
		} 
		/* City/District Model Function End Here  */
		
		
		
		//---------------------Start Bank Master-----------------------------
		
		
		public function insert_bank()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$data=array(
			'm_bank_id'=>'',
			'm_bank_name'=>$this->input->post('txtbank'),
			'm_bank_status'=>'1',
			'proc'=>'1'
			);
			$query = " CALL sp_bank(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		
		public function view_update_bank()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$data=array(
			'm_bank_id'=>$this->uri->segment(3),
			'm_bank_name'=>$this->input->post('txtbank'),
			'm_bank_status'=>'',
			'proc'=>'3'
			);
			$query = " CALL sp_bank(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		
		public function bank_status()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$data=array(
			'm_bank_id'=>$this->uri->segment(3),
			'm_bank_name'=>'',
			'm_bank_status'=>$this->uri->segment(4),
			'proc'=>'4'
			);
			$query = " CALL sp_bank(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		//---------------------End Bank Master-----------------------------
		
		
		//---------------------Unit Master-----------------------------
		public function insert_unit_master()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$data=array(
			'm_unit_id'=>'',
			'm_unit_name'=>$this->input->post('txtunit'),
			'm_unit_description'=>$this->input->post('txtdescription'),
			'm_unit_status'=>'1',
			'm_unit_value'=>$this->input->post('txtunitvalue'),
			'proc'=>'1'
			);
			var_dump($data);
			$query = " CALL sp_unit_master(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		
		
		public function update_unit_master()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$data=array(
			'm_unit_id'=>$this->uri->segment(3),
			'm_unit_name'=>$this->input->post('txtunit'),
			'm_unit_description'=>$this->input->post('txtdescription'),
			'm_unit_status'=>'',
			'm_unit_value'=>$this->input->post('txtunitvalue'),
			'proc'=>'3'
			);
			$query = " CALL sp_unit_master(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		
		public function unit_master_status()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$data=array(
			'm_unit_id'=>$this->uri->segment(3),
			'm_unit_name'=>'',
			'm_unit_description'=>'',
			'm_unit_status'=>$this->uri->segment(4),
            'm_unit_value'=>'',
			'proc'=>'4'
			);
			$query = " CALL sp_unit_master(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		
		//---------------------Site Master-----------------------------
		
		
		public function insert_site_master()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$data=array(
			'm_site_id'=>'',
			'm_site_name'=>$this->input->post('txtsitename'),
			'm_site_location'=>$this->input->post('txtlocation'),
			'm_site_rate'=>$this->input->post('txtrate'),
			'm_site_rate_per'=>$this->input->post('txtper'),
			'm_site_corner_rate'=>$this->input->post('txtcorner_rate'),
			'm_site_park_facing'=>$this->input->post('txtpark_rate'),
			'm_site_corner_park'=>$this->input->post('txtcorner_park'),
			'm_site_commercial'=>$this->input->post('txtcommercial_rate'),
			'm_site_main_raod'=>$this->input->post('txtmain_road_rate'),
			'm_site_other'=>$this->input->post('txtother'),
			'm_site_commercial_facing'=>$this->input->post('txtcommercial_frate'),
			'm_site_dev_charge_method'=>$this->input->post('optionsRadios'),
			'm_site_dev_charge'=>$this->input->post('txtdevelopment_chrg'),
			'm_site_dev_per'=>$this->input->post('txtper1'),
			'm_site_point_value'=>$this->input->post('txtplot_value'),
			'm_site_point_status'=>'',
			'proc'=>'1'
			);
			$query = " CALL sp_site_master(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		
		
		
		public function update_site_master()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$data=array(
			'm_site_id'=>$this->uri->segment(3),
			'm_site_name'=>$this->input->post('txtsitename'),
			'm_site_location'=>$this->input->post('txtlocation'),
			'm_site_rate'=>$this->input->post('txtrate'),
			'm_site_rate_per'=>$this->input->post('txtper'),
			'm_site_corner_rate'=>$this->input->post('txtcorner_rate'),
			'm_site_park_facing'=>$this->input->post('txtpark_rate'),
			'm_site_corner_park'=>$this->input->post('txtcorner_park'),
			'm_site_commercial'=>$this->input->post('txtcommercial_rate'),
			'm_site_main_raod'=>$this->input->post('txtmain_road_rate'),
			'm_site_other'=>$this->input->post('txtother'),
			'm_site_commercial_facing'=>$this->input->post('txtcommercial_frate'),
			'm_site_dev_charge_method'=>$this->input->post('optionsRadios'),
			'm_site_dev_charge'=>$this->input->post('txtdevelopment_chrg'),
			'm_site_dev_per'=>$this->input->post('txtper1'),
			'm_site_point_value'=>$this->input->post('txtplot_value'),
			'm_site_point_status'=>'',
			'proc'=>'3'
			);
			$query = " CALL sp_site_master(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		
		
		public function site_master_status()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$data=array(
			'm_site_id'=>$this->uri->segment(3),
			'm_site_name'=>'',
			'm_site_location'=>'',
			'm_site_rate'=>'',
			'm_site_rate_per'=>'',
			'm_site_corner_rate'=>'',
			'm_site_park_facing'=>'',
			'm_site_corner_park'=>'',
			'm_site_commercial'=>'',
			'm_site_main_raod'=>'',
			'm_site_other'=>'',
			'm_site_commercial_facing'=>'',
			'm_site_dev_charge_method'=>'',
			'm_site_dev_charge'=>'',
			'm_site_dev_per'=>'',
			'm_site_point_value'=>'',
			'm_site_point_status'=>$this->uri->segment(4),
			'proc'=>'4'
			);
			$query = " CALL sp_site_master(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		
		//---------------------Plot Size Master-----------------------------
		
		public function insert_plot_size()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$data=array(
			'm_plot_size_id'=>'',
			'm_plot_width_ft'=>$this->input->post('txtdimension'),
			'm_plot_width_inch'=>$this->input->post('txtinches'),
			'm_plot_height_ft'=>$this->input->post('txtplot_height'),
			'm_plot_height_inch'=>$this->input->post('txtinches1'),
			'm_plot_Area'=>$this->input->post('txtplot_area'),
			'm_plot_per'=>$this->input->post('txtper'),
			'm_plot_booking_amt'=>$this->input->post('txtbooking_amount'),
			'm_plot_status'=>'1',
			'proc'=>'1'
			);
			$query = " CALL sp_plot_size_master(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		
		public function update_plot_size()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$data=array(
			'm_plot_size_id'=>$this->uri->segment(3),
			'm_plot_width_ft'=>$this->input->post('txtdimension'),
			'm_plot_width_inch'=>$this->input->post('txtinches'),
			'm_plot_height_ft'=>$this->input->post('txtplot_height'),
			'm_plot_height_inch'=>$this->input->post('txtinches1'),
			'm_plot_Area'=>$this->input->post('txtplot_area'),
			'm_plot_per'=>$this->input->post('txtper'),
			'm_plot_booking_amt'=>$this->input->post('txtbooking_amount'),
			'm_plot_status'=>'',
			'proc'=>'3'
			);
			$query = " CALL sp_plot_size_master(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		
		public function plot_size_status()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$data=array(
			'm_plot_size_id'=>$this->uri->segment(3),
			'm_plot_width_ft'=>'',
			'm_plot_width_inch'=>'',
			'm_plot_height_ft'=>'',
			'm_plot_height_inch'=>'',
			'm_plot_Area'=>'',
			'm_plot_per'=>'',
			'm_plot_booking_amt'=>'',
			'm_plot_status'=>$this->uri->segment(4),
			'proc'=>'4'
			);
			$query = " CALL sp_plot_size_master(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		
		//---------------------Site Plot Master-----------------------------
		
		public function insert_site_plot()
		{
			$this->load->library('session');
			$this->load->helper('url');
			$this->load->database();
			$data=array(
			'm_sp_id'=>'',
			'm_sp_site'=>$this->input->post('siteid'),
			'm_sp_plot_no'=>$this->input->post('txtplot_no'),
			'm_sp_plot_rate'=>$this->input->post('txtplot_rate'),
			'm_sp_plot_rate_per'=>$this->input->post('txtper'),
			'm_sp_plot_type'=>$this->input->post('chnormal'),
			'm_sp_dev_method'=>$this->input->post('optionsRadios'),
			'm_sp_dev_chrg'=>$this->input->post('txtdev_charge'),
			'm_sp_dev_chrg_per'=>$this->input->post('txtper1'),
			'm_sp_plot_size'=>$this->input->post('txtplot_size'),
			'm_sp_plot_amount'=>$this->input->post('txtplot_amount'),
			'm_sp_booking_amount'=>$this->input->post('txtbooking_ammount'),
			'm_sp_status'=>'1',
			'proc'=>'1'
			);
			$query = " CALL sp_site_plot_matser(?" . str_repeat(",?", count($data)-1) . ")";
			$this->db->query($query, $data);
		}
		
		
		
		
	}
?>		