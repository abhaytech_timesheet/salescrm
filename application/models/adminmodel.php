<?php
	class Adminmodel extends CI_Model
	{
        /*Affiliate Wise Submit Ticket */ 
		public function get_submit_ticket_detail()
		{
			$affid=$this->session->userdata["affid"];
			$condition='';
			$dept="".$this->input->post('dddept')."";
			$acc="".$this->input->post('txtaccname')."";
			$accid="".$this->input->post('txtaccid')."";
			$from="".$this->input->post('txtfrom')."";
			$to="".$this->input->post('txtto')."";
			if($this->input->post('dddept')!='')
			{
				$condition=$condition." `m46_submit_ticket`.`tkt_department`= ".$dept." and ";
			}
			if($this->input->post('txtaccname')!='')
			{
				$condition=$condition." `m46_submit_ticket`.`tkt_person_name` LIKE '%".$acc."%' and ";
			}
			if($this->input->post('txtaccid')!='')
			{
				$condition=$condition." `m33_account`.`account_user_id` LIKE '%".$accid."%' and ";
			}
			if($this->input->post('txtto')!='' && $this->input->post('txtfrom')!='')
			{
				$condition=$condition." date_format(`m46_submit_ticket`.`tkt_sub_date`,'%Y-%m-%d') between date_format('".$from."','%Y-%m-%d') and date_format('". $to ."','%Y-%m-%d') and";
			}
			$condition = $condition." affiliate_id=".$affid." and `m46_submit_ticket`.`emp_id` =0 GROUP BY tkt_ref_no order by tkt_id desc";
			$clsdt= array(
			'person_reg_id'=>$condition
			);
			$query = "CALL sp_view_ticket(?)";
			$data['rec']=$this->db->query($query,$clsdt);
			$this->db->free_db_resource();
			$clsdt1= array(
			'aff_id'=>$affid
			);
			$query1 = "CALL sp_find_desig(?)";
			$data['emp']=$this->db->query($query1,$clsdt1);
			$this->db->free_db_resource();
			return $data;
		}
		
		/*Affiliate Wise Submit Ticket Assign*/   
        public function assign($st)
	    {
			$affid=$this->session->userdata["affid"];
			$condition='';	
			$condition = $condition." affiliate_id='$affid' and  (emp_id!=''||emp_id!=0) ";
			if($st!=-1)
			$condition = $condition." and tkt_status='$st'";
			$condition = $condition." GROUP BY tkt_ref_no order by tkt_id desc";
			$clsdt= array(
			'querey'=>$condition
			);
			$query = "CALL sp_view_ticket(?)";
			$data['rec']=$this->db->query($query,$clsdt);
			$this->db->free_db_resource();
			$data['emp']=$this->db->get('m06_user_detail');
			return $data;
		}

		/*Affiliate Wise Submit Ticket Assign to their employees according their designation*/ 
	    public function task_assign()
		{
			$name="";$email="";
			$emp_id=$this->input->post('ddemp_name');
			$data['id']=$this->db->query("SELECT * FROM m06_user_detail WHERE or_m_reg_id='$emp_id'");
			foreach($data['id']->result() as $row)
			{
				$name=$row->or_m_name;
				$email=$row->or_m_email;
				break;
			}
			$tic_no=$this->input->post('hd');
			$data=array(
			'emp_id'=>$this->input->post('ddemp_name'),
			'tic_no'=>$this->input->post('hd'),
			'id'=>$this->session->userdata('profile_id')
			);
			$query = "CALL sp_assign_ticket(?" . str_repeat(",?", count($data)-1) .")";
			$data['rec']=$this->db->query($query,$data);
			$this->db->free_db_resource();
			$config['mailtype'] = 'html';
			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from('support@ferryipl.com','Admin Ferryipl'); // change it to yours
			$this->email->to($email);// change it to yours
			$this->email->subject('Ticket Assign');
			$this->email->message('<p>Dear '.$name.'<br><br>This is to inform, that your coordination is required in resolving the Ticket ( #'.$tic_no.' ) assigned to you. Kindly follow the link below for further proceedings.</p>');
			if($this->email->send())
			{
				echo 'true';  
			}
			else
			{	
				show_error($this->email->print_debugger());
			}
		}
		
	}
?>