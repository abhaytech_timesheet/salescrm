<?php
	class Affiliates_model extends CI_Model
	{
		public function __construct() 
		{
			parent::__construct();
            $timezone = new DateTimeZone("Asia/Kolkata" );
		    $date = new DateTime();
		    $date->setTimezone($timezone);
		    $this->curr=$date->format('Y-m-d H:i:s');
		}
		
		public function assign_menu()
		{
			$desig=$this->session->userdata('designation');
			$id=$this->session->userdata('profile_id');
			$data['menu']=$this->db->query("SELECT * FROM view_menu WHERE tr_assign_userid=1 AND m_submenu_status=1 AND tr_assign_status=1 ORDER BY tr_assign_id DESC");
			return $data;
		}
		
		/*Affiliate Wise Submit Ticket */ 
		public function get_submit_ticket_detail()
		{
			$affid=$this->session->userdata["affid"];
			$condition='';
			$txtdepartment='-1';
			if($this->input->post('txtdepartment')!="" && $this->input->post('txtdepartment')!="-1")
			$txtdepartment=$this->input->post('txtdepartment');
			$data['depart']=$txtdepartment;
			if($txtdepartment=="" || $txtdepartment=="-1")
			{
				$condition = $condition." affiliate_id='$affid' and emp_id='' GROUP BY tkt_ref_no order by tkt_id desc";
			}
			else
			{
				$condition = $condition." tkt_department='$txtdepartment' and affiliate_id='$affid' and emp_id='' GROUP BY tkt_ref_no order by tkt_id desc";
			}
			$clsdt= array(
			'person_reg_id'=>$condition
			);
			$query = "CALL sp_view_ticket(?)";
			$data['rec']=$this->db->query($query,$clsdt);
			$this->db->free_db_resource();
			$clsdt1= array(
			'aff_id'=>$condition
			);
			$query1 = "CALL sp_find_desig(?)";
			$data['emp']=$this->db->query($query1,  $clsdt1);
			$this->db->free_db_resource();
			return $data;
		}
		
		/*Affiliate Wise Submit Ticket Assign*/   
		public function assign()
		{
			$affid=$this->session->userdata["affid"];
			$condition='';
			$condition = $condition." affiliate_id='$affid' and  (emp_id!=''||emp_id!=0) GROUP BY tkt_ref_no order by tkt_id desc";
			$clsdt= array(
			'querey'=>$condition
			);
			$query = "CALL sp_view_ticket(?)";
			$data['rec']=$this->db->query($query,$clsdt);
			$this->db->free_db_resource();
			$data['emp']=$this->db->get('m06_user_detail');
			return $data;
		}
		
		
		/*Affiliate Wise Submit Ticket Assign to their employees according their designation*/ 
	    public function task_assign()
		{
			$tic_no=$this->input->post('hd');
			$data=array(
			'emp_id'=>$this->input->post('ddemp_name'),
			'tic_no'=>$this->input->post('hd'),
			'id'=>$this->session->userdata('profile_id')
			);
			$query = "CALL sp_assign_ticket(?,?,?)";
			$data['rec']=$this->db->query($query,$data);
			$this->db->free_db_resource();
			return "true";
		}
		
		
		public function view_reassign()
		{
			$data['id']=$this->uri->segment(3);
			$data['assign_id']=$this->uri->segment(4);
			$data['tickt_type']=$this->uri->segment(5);
			$query1 = "CALL sp_find_desig()";
			$data['emp']=$this->db->query($query1);
			$this->db->free_db_resource();
			return $data;
		}
		
		
		public function task_again_assign()
	    {
			$tic_no=$this->input->post('hd');
			$assign_id=$this->input->post('old_assign_id');
			$data=array(
			'assign_emp_id'=>$this->input->post('ddemp_name'),
			'tic_no'=>$this->input->post('hd'),
			'id'=>$this->input->post('old_assign_id')
			);
		    $query = "CALL sp_assign_ticket(?,?,?)";
	        $data['rec']=$this->db->query($query,$data);
		    $this->db->free_db_resource();
		}
		
		
		public function view_ticket()
		{
			$p_id=$this->uri->segment(3);
			$condition='';
			$condition=$condition."m46_submit_ticket.tkt_ref_no='$p_id'";
			$clsdt1= array(
			'querey'=>$condition
			);
			$query1 = "CALL sp_view_ticket(?)";
			$data['rec']=$this->db->query($query1,$clsdt1);
			$this->db->free_db_resource();
			$this->db->where('tkt_ref_no',$p_id);
			$data['ticket']=$this->db->get('m46_submit_ticket');
			foreach($data['ticket']->result() as $row)
			{
				$id=$row->tkt_id;	
			}
			$condition = "tr24_ticket_transaction.tkt_id='$id'";
			$clsdt= array(
			'querey'=>$condition
			);
			$query = "CALL sp_view_ticket_reply(?)";
			$data['rec1']=$this->db->query($query,$clsdt);
			$this->db->free_db_resource();
			return $data;
		}
		
		
		public function affiliates_response()
		{
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			$config['upload_path']   =   "application/uploadimage/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "1907";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
			$this->upload->do_upload();
			$finfo=$this->upload->data();
		    $tic=$this->input->post('txtticket');
		    $fileupload=$finfo['raw_name'].$finfo['file_ext'];
			if($fileupload=="")
			{
				$fi="";
			}
			else
			{
				$fi=$fileupload;
			}
			$data=array(
			'proc'=>2,'ticket_no'=>$this->input->post('txtticket'),'tkt_person_name'=>'','tkt_email'=>'','tkt_department'=>'','emp_id'=>'','tkt_subject'=>'','tkt_urgency'=>'','tkt_discription'=>'','tkt_response_type'=>1,'response_by'=>$this->session->userdata('name'),'tkt_userfile'=>$fi,'tkt_status'=>1,'account_id'=>'','affiliate_id'=>'','tkt_sub_date'=>$date->format('Y-m-d H:i:s'),'trans_description'=>$this->input->post('txtdiscription'),'trans_response_date'=>$date->format('Y-m-d H:i:s'),'trans_status'=>1
			);
			$query = "CALL sp_ticket_submission(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			$data['rec']=$this->db->query($query,$data);
			$this->db->free_db_resource();
			return $tic;
		}
		
		
		/* ----------------Resolve Problem on btn Click--------------- */
		public function resolve()
		{
			$timezone = new DateTimeZone("Asia/Kolkata" );
			$date = new DateTime();
			$date->setTimezone($timezone);
			$ticket_no=$this->input->post('ticket_no');
			$data=array(       
		    'proc'=>3,'ticket_no'=>$ticket_no,'tkt_person_name'=>'','tkt_email'=>'','tkt_department'=>'','emp_id'=>'','tkt_subject'=>'','tkt_urgency'=>'','tkt_discription'=>'','tkt_response_type'=>1,'response_by'=>'','tkt_userfile'=>'','tkt_status'=>0,'account_id'=>'','affiliate_id'=>'','tkt_sub_date'=>$date->format('Y-m-d H:i:s'),'trans_description'=>'','trans_response_date'=>'','trans_status'=>''
			);
			$query = "CALL sp_ticket_submission(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			$data['rec']=$this->db->query($query,$data);
			$this->db->free_db_resource();
			return $ticket_no;
		}
		
		
		public function submit_ticket()
		{
			$p_id=$this->uri->segment(4);
			$this->db->where('account_id',$p_id);
			$data['rec']=$this->db->get('m33_account');
			$dep=$this->uri->segment(3);
			$data['dep']=$dep;
			return $data;
		}
		
		
		public function reassign()
		{
			$tic_no=$this->uri->segment(3);
			$data=array(
			'assign_emp_id'=>'',
			);
			$this->db->where('ticket_no',$tic_no);
			$this->db->update('tr20_submit_ticket',$data);
		}
		
		
		public function view_account_profile()
		{
			$this->db->where('account_id',$this->session->userdata('affid'));
			$data['acc']=$this->db->get('m33_account');
			$data['loc']=$this->db->get('m05_location');
			$this->db->where('m_acc_id',$this->session->userdata('affid'));
			$data['pro']=$this->db->get('m43_acc_project');
			
			$data['ticket']=$this->db->get('tr20_submit_ticket');
			$t=array('2','3');
			$this->db->where_in('for',$t);
			$data['anonce']=$this->db->get('tr19_anoncement');
			return $data;
		}
		
		
		public function change_account_image()
		{
			$config['upload_path']   =   "application/uploadimage/";
			$config['allowed_types'] =   "gif|jpg|jpeg|png|pdf|doc|xlsx|xml|zip|txt"; 
			$config['max_size']      =   "5000";
			$config['max_width']     =   "1907";
			$config['max_height']    =   "1280";
			$this->load->library('upload',$config);
			
			$this->upload->do_upload();
			$finfo=$this->upload->data();
			$this->load->helper('url');
			$this->load->library('session');
			$this->load->database();
			$fileupload=$finfo['raw_name'].$finfo['file_ext'];
			$id=$this->uri->segment(3);
			if($fileupload=="")
			{
				$fi="";
			}
			else
			{
				
				$fi=$fileupload;
			}
			$data=array(
			'account_image'=>$fi,
			);
			$this->db->where('account_id',$this->session->userdata('affid'));
			$this->db->update('m33_account',$data);
			$sessiondata=array(
			'image' =>$fi
			);
			$this->session->set_userdata($sessiondata);
			return $id;
			
		}
		
		
		public function details()
		{
			$id = $this->session->userdata('profile_id');
			$this->db->where('contact_id',$id);
			$data['rec2']=$this->db->get('m34_contact');
			$data['rec1']=$this->db->get('m05_location');
			$this->db->where('m_parent_id',0);
			$data['rec']=$this->db->get('m05_location');
			return $data;
			
		}
		
		
		public function change_password()
		{
			$id=$this->session->userdata('profile_id');
			$this->db->where('or_user_id',$id);
			$data['rec']=$this->db->get('tr04_login');
			$data['p']=$this->session->userdata('profile_id');
			//$this->db->where('m_des_id',$this->session->userdata('designation'));
			return $data;
		}
		
		
		public function password_update()
		{
			$pid=$this->session->userdata('profile_id');
			$login=array(
			'or_login_pwd'=>trim($this->input->post('txtpassword'))
			);
			$this->db->where("or_login_type",$this->session->userdata('designation'));
			$this->db->where("or_user_id",$pid);
			//$this->db->update("tr04_login",$login);
		}
		
		
		public function mydetail_update()
		{
			$pid=$this->session->userdata('profile_id');
			$data=array(
			'txtfname'=>$this->input->post('txtfname'),
			'txtlname'=>$this->input->post('txtlname'),
			'txtcompany'=>$this->input->post('txtcompany'),
			'txtemail'=>$this->input->post('txtemail'),
			'txtcountry'=>$this->input->post('txtcountry'),
			'txtstate'=>$this->input->post('txtstate'),
			'txtcity'=>$this->input->post('txtcity'),
			'txtlocality'=>$this->input->post('txtlocality'),
			'txtzip'=>$this->input->post('txtzip'),
			'txtphone'=>$this->input->post('txtphone'),
			'txtcurrency'=>$this->input->post('txtcurrency'),
			'txtpaymentmethod'=>$this->input->post('txtpaymentmethod'),
			'txtbillingcid'=>$this->input->post('txtbillingcid')
			);
			$data1=array(
			'txtemail'=>$this->input->post('txtemail')
			);
			$this->db->where('m02_id',$pid);
			$this->db->update('m06_user_detail',$data);
			$this->db->where('tr01_id',$pid);
			//$this->db->update('tr04_login',$data1);
		}
		
		
		public function view_lead()
		{
			
			$smenu=array(
			'proc'=>2,
			'lead_for'=>'',
			'lead_owner'=>$this->session->userdata('profile_id'),
			'lead_created_by'=>$this->uri->segment(3),
			'lead_company'=>'',
			'lead_prefix'=>'',
			'lead_name'=>'',
			'lead_title'=>'',
			'lead_industry'=>'',
			'lead_emp'=>'',
			'lead_email'=>'',
			'lead_mobile'=>'',
			'lead_source'=>'',
			'lead_current_status'=>'',
			'lead_description'=>'',
			'lead_address'=>'',
			'lead_city'=>'',
			'lead_state'=>'',
			'lead_zipcode'=>'',
			'lead_country'=>'',
			'lead_status'=>'',
			'lead_logintype'=>'',
			'lead_regdate'=>''
			);
			$query = " CALL sp_lead(?" . str_repeat(",?", count($smenu)-1) .",@) ";
			$data['rec']=$this->db->query($query, $smenu);
			$this->db->free_db_resource();
			$data['response']=$this->db->query("SELECT @a as resp");
			$data['id']=$this->uri->segment(3);
			$this->db->where('or_m_designation',4);
			$this->db->where('or_m_status',1);
			$data['user']=$this->db->get('m06_user_detail');
			return $data;
		}
		
		
		public function view_account()
		{
			$data['id']=$this->uri->segment(3);
			$smenu=array(
			'proc'=>2,
			'account_owner'=>$this->session->userdata('profile_id'),
			'account_owned_by'=>$this->uri->segment(3),
			'account_name'=>'',
			'account_industry'=>'',
			'account_emp'=>'',
			'account_website'=>'',
			'account_phone'=>'',
			'account_email'=>'',
			'account_image'=>'',
			'account_fax'=>'',
			'account_type'=>'',
			'account_ownership'=>'',
			'account_city'=>'',
			'account_state'=>'',
			'account_zipcode'=>'',
			'account_desc'=>'',
			'account_country'=>'',
			'account_address'=>'',
			'account_status'=>'',
			'logintype'=>'',
			'account_regdate'=>'',
			'account_pwd'=>'',
			'account_pinpwd'=>''
			);
			$query ="CALL sp_account(?" . str_repeat(",?", count($smenu)-1) .",@a) ";
			$data['account']=$this->db->query($query, $smenu);
			$this->db->free_db_resource();
			$data['id']=$this->uri->segment(3);
			return $data;
		}
		
		
		public function view_contact()
		{
			$contact=array(
			'proc'=>2,'account_id'=>$this->session->userdata('affid'),'contact_title'=>'','contact_name'=>'','contact_designation'=>'','contact_mobile'=>'','contact_email'=>'','contact_address'=>'','contact_city'=>'','contact_state'=>'','contact_country'=>'','contact_zipcode'=>'','contact_status'=>1,'contact_reg_date'=>'','is_account'=>1
			);
			$query = "CALL sp_contact(?" . str_repeat(",?", count($contact)-1) .",@) ";
			$data['rec']=$this->db->query($query,$contact);
			$this->db->free_db_resource();
			$data['id']=$this->uri->segment(3);
			return $data;
		}
		
	}
?>