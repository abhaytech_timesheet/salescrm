<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'auth/index';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['ticket(:any)']="direct_ticket/submit_ticket/$1";
$route['news.html'] = "direct_ticket/announcements";
$route['support.html'] = "direct_ticket/index";
$route['logout(:any)']="auth/index/$1";
$route['helpdesk.html'] = "welcome/help_desk";
$route['ticket'] = "admin/view_ticket";
$route['dashboard'] = "report/dashboard";
$route['tickets'] = "admin/index";
$route['allocate'] = "admin/view_assign_ticket";
$route['explore'] = "admin/search_ticket";
$route['employee_delineate'] = "admin/employee_ticket_report";
$route['declaration'] = "admin/announcements";
$route['employee_outlook'] = "employee/view_employee";
$route['employee_referal'] = "employee/view_employee_report";
$route['employee_presence'] = "employee/view_employee_attendence_report/";
$route['leads(:any)'] = "crm/view_lead/$1";
$route['converted_leads(:any)'] = "crm/view_opportunity/$1";
$route['profiles(:any)'] = "crm/view_account/$1";
$route['associates(:any)'] = "crm/view_contact/$1";
$route['to_do(:any)'] = "crm/view_task/$1";
$route['episodes(:any)'] = "crm/view_event/$1";
$route['text_campaign'] = "crm/create_sms_campaign";
$route['creative_templates'] = "campaign/view_create_email_template";
$route['sms_marketing'] = "campaign/view_send_sms_template";
$route['email_marketing'] = "campaign/view_send_email_template";
$route['clients'] = "account/account_details";
$route['projects'] = "project/view_project";
$route['team'] = "project/view_paricipants";
$route['team_todo'] = "project/view_all_task";
$route['project_detail'] = "project/view_project_report";
$route['todo_list'] = "project/view_task_report";
$route['follow-ups'] = "project/current_followup_report";
$route['sessions'] = "acs/view_session";
$route['departments'] = "acs/view_manage_department";
$route['roles'] = "acs/view_manage_role/";
$route['salary_evaluation'] = "acs/view_define_salary_scale";
$route['allowances'] = "acs/view_add_allowance";
$route['holidays'] = "acs/view_add_holiday";
$route['salary'] = "acs/view_add_salary";
$route['work_schedule'] = "acs/view_work_parameter";
$route['leave_management'] = "acs/view_leave_master";
$route['shifts'] = "acs/view_shift_master";

/*For Configuration */

$route['configuration'] = "master/view_mainconfig";
$route['city'] = "master/view_city";
$route['news'] = "master/view_news";
$route['applications'] = "master/view_add_apps";
$route['application_status'] = "master/view_change_status_apps";
$route['app_allocation'] = "master/view_user_assign";
$route['review-app-status'] = "master/view_assign_apps_status";
$route['accounts'] = "master/account";
$route['services'] = "master/manage_services";
$route['service_provider'] = "master/manage_service_provider";

/*This is for Front Site */
$route['Aarjavam'] = "welcome/aarjavam";
$route['who-we-are'] = "welcome/about";
$route['what-we-do'] = "welcome/services";
$route['solution-services'] = "welcome/solution";
$route['outsourcing'] = "welcome/outsourcing";
$route['techaarjavam'] = "welcome/techaarjavam";
$route['leadership'] = "welcome/leadership";
$route['mobile-application'] = "welcome/mobolitysolution";
$route['analytics'] = "welcome/analytics";
$route['erp'] = "welcome/erp";
$route['enterprise'] = "welcome/enterprises";
$route['digitalisation'] = "welcome/digitaltrans";
$route['service-group'] = "welcome/servicegroup";
$route['salesbooster'] = "welcome/salesbooster";
$route['brandmaker'] = "welcome/braandmaker";
$route['metroheights'] = "welcome/metroheights";
$route['onebox'] = "welcome/onebox";
$route['banking'] = "welcome/banking";
$route['retail'] = "welcome/retail";
$route['manufacturing'] = "welcome/manufacturing";
$route['entertainment'] = "welcome/technomedia";
$route['travel'] = "welcome/travel";
$route['psu'] = "welcome/psu";
$route['term-of-use'] = "welcome/termofuse";
$route['privacy-policy'] = "welcome/privacypolicy";
$route['contact'] = "welcome/contact";
//new assign menus
$route['manage_api'] = "api/manage_api";
$route['manage_api_uri'] = "api/manage_api_uri";
$route['manage_api_services'] = "api/manage_api_services";
$route['manage_api_service_op'] = "api/manage_api_service_op";
$route['genrate_api'] = "api/genrate_api";


$route['manage_services'] = "master/manage_services";
$route['manage_service_provider'] = "master/manage_service_provider";

$route['change_password'] = "action/change_password";
$route['change_mobile'] = "action/change_mobile";
$route['change_intro'] = "action/change_intro";
$route['active_mem'] = "action/active_mem";
$route['deactive_mem'] = "action/deactive_mem";
$route['allow_capping'] = "action/allow_capping";

$route['assign_group'] = "group/assign_group";
$route['manage_group'] = "group/manage_group";
$route['assign_margin'] = "group/assign_margin";

$route['default_controller'] = "welcome";
$route['404_override'] = 'welcome/error_404';

/* End of file routes.php */
/* Location: ./application/config/routes.php */