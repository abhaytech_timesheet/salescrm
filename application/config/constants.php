<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
require_once( BASEPATH.'database/DB'.EXT );
$db =& DB();
$result=$db->get('m00_setconfig')->row();
define('sitelogo',$result->m00_sitelogo);
define('sitename',$result->m00_sitename);
define('website',$result->m00_website);
define('sendotp',$result->m00_otp);
define('sendsms',$result->m00_sms_alert);
define('sendemail',$result->m00_email_alert);
define('smsusername',$result->m00_sms_uername);
define('smspwd',$result->m00_sms_pwd);
define('EMAIL',$result->m00_email);
define('EMAIL_PWD',$result->m00_email_pwd);

define('POP3_HOST',$result->m00_pop3_host);
define('POP3_PORT',$result->m00_pop3_port);
define('PO3_EMAIL',$result->m00_pop3_email);
define('POP3_PWD',$result->m00_pop3_pwd);

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */