  <!-- End Header --> 
  
  <!--======= SUB BANNER =========-->
  <section class="sub-banner about">
    <div class="container">
      <div class="position-center-center">
        <h2>Analytics</h2>
        <span style="color:#fff;"><q style="font-size:20px;">TechAarjavam where simplicity is the ultimate sophistication....................</q></span>
	</div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    
    <!-- Intro -->
    
    
    <!-- Google Front Page -->
    <section class="front-page padding-top-100 padding-bottom-100">
      <div class="container">
        <div class="row">
          <div class="col-md-4"> <img class="img-responsive margin-top-30" src="<?php echo base_url()?>application/libraries/front-lib/images/analytics.png" alt="mobile-app "> </div>
          <div class="col-md-8"> 
            <!-- Tittle -->
            <div class="heading-block text-left margin-bottom-20">
              <h2>Analytics Data</h2>
              <h4>You create a valuable digital presence.</h4>
              
              <p align="justify"> Techaarjavam is a market leading Mobile application Development Company. With the evident and inevitable migration to mobile devices, mobile apps have almost become a necessity for businesses to excel. Our custom mobile application development services will give you that much needed competitive advantage. We house mobile application developers who are very passionate about mobile development and are always keen to hear your ideas and help you bring them to life. As a mobile application development company, we find joy in developing highly interactive mobile apps that yield a positive return on investment. </p>
           <p align="justify">Mobile applications help businesses improve and accelerate their online work process. Our mobile application developers help you create feasible app strategies and help you bring the vision to reality. As a mobile application development company, we take pride in our ability to develop functional, highly engaging mobile apps that helps our clients achieve a desired outcome. Our Mobile Application Development team focusses on developing mobile apps that surpasses screen and bandwidth limitations making browsing and purchasing processes easier, faster and more enjoyable. Our consistency in delivery and commitment to value creation makes us one of the best mobile application development companies in India, England, America, Canada, Malaysia, Australia and other countries in world.</p>
          
          
            </div>
            
            <!-- List Style -->
            
            
           
              </div>
        </div>
      </div>
    </section>
    
    <!-- Our Clients  -->
   <?php /*<section class="clients padding-bottom-100 padding-top-100">
      <div class="container"> 
        
        <!-- Tittle -->
        <div class="heading-block white-text text-center margin-bottom-80">
          <h2>Our Clients </h2>
          <span class="intro-style">Do you want to be part of this client list? </span> </div>
        
        <!-- Clients Images -->
        <ul class="col-5 text-center">
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-1.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-2.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-3.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-4.png" alt=""></li>
          <li> <img class="img-responsive" src="<?php echo base_url()?>application/libraries/front-lib/images/client-img-5.png" alt=""></li>
        </ul>
      </div>
    </section>
    */?>
    <!-- Team  -->
   
  </div>
  <!-- End Content --> 
  
  <!-- Footer -->
 
</div>
<!-- End Page Wrapper --> 

<!-- JavaScripts --> 
</body>


</html>
